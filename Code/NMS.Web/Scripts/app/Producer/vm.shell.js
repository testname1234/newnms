﻿define('vm.shell',
     [
         'ko',
         'config',
         'router',
         'enum',
         'datacontext',
         'model',
         'manager',
         'appdata',
         'vm.home',
         'vm.production',
         'presenter',
         'utils',
         'model.user',
         'helper',
         'vm.headline',
         'vm.ticker',
         'vm.contentviewer'
     ],
    function (ko, config, router, e, dc, model, manager, appdata, homeVM, productionVM, presenter, utils, usermodel, helper, headlineVm, tickerVm, contentviewerVM) {
        var logger = config.logger;

        var
            // Properties
            //-------------------

            menuHashes = config.hashes,
            templates = config.templateNames,
            sourceFilterControl = new model.SourceFilterControl(),
            generalFilterControl = new model.GeneralFilterControl(),
            generalFilterTickerControl = new model.GeneralFilterTickerControl(),
            calendarControl = new model.CalendarControl(),
            currentTopNews = ko.observable(new model.News()),
            currentTopNotification = ko.observable(),
            isStoriesChanged = ko.observable(false),
            currentPassword = ko.observable(''),
            newPassword = ko.observable(''),
            confirmPassword = ko.observable(''),

            isPasswordChanged = ko.observable(false),
            setTimeOutArrayNewsAlerts = [],
            setTimeOutArrayNotificationsAlerts = [],

            // Computed
            //-------------------

            channels = ko.computed({
                read: function () {
                    return dc.channels.getObservableList();
                },
                deferEvaluation: true
            }),
            programs = ko.computed({
                read: function () {
                    var allPrograms = dc.programs.getObservableList();
                    var arr = _.filter(allPrograms, function (program) {
                        return program.channelId() === appdata.currentChannel();
                    });
                    isStoriesChanged(true);
                    return arr;
                },
                deferEvaluation: true
            }),
            episodes = ko.computed({
                read: function () {
                    var arr = [];
                    var programEpisodes = dc.episodes.getByProgramId(appdata.currentProgram().id)();
                    for (var i = 0; i < programEpisodes.length; i++) {
                        var diffDays = utils.getDifferenceBetweenDates(appdata.programSelectedDate(), programEpisodes[i].startTime());
                        if (diffDays === 0) {
                            arr.push(programEpisodes[i]);
                        }
                    }
                    isStoriesChanged(true);
                    return arr;
                },
                deferEvaluation: true
            }),
            shellRightVisible = ko.computed({
                read: function () {
                    if (router.currentHash() === config.hashes.production.production ||
                        router.currentHash() === config.hashes.production.sequence ||
                        router.currentHash() === config.hashes.production.myheadline ||
                        router.currentHash() === config.hashes.production.report ||
                        router.currentHash() === config.hashes.production.filterPreference ||
                        router.currentHash() == config.views.fieldreporter.reportNews.url ||
                        router.currentHash() === config.hashes.production.XMLParser || 
                        router.currentHash() === config.hashes.production.channelTicker ||
                        router.currentHash() === config.hashes.production.alertTicker ||
                        router.currentHash() === config.hashes.production.breakingTicker ||
                        router.currentHash() === config.hashes.production.categoryTicker ||
                        router.currentHash() === config.hashes.production.producerArchivalMedia ||
                        appdata.currentUser().userType === e.UserType.FReporter ||
                        router.currentHash() === config.hashes.production.newsFlow
                        )
                        return false;
                    else
                        return true;
                },
                deferEvaluation: true
            }),
            newsbucketList = ko.computed({
                read: function () {
                    var temp = dc.newsBucketItems.getObservableList();
                    temp = temp.sort(function (storyA, storyB) {
                        return new Date(storyB.creationDate()) - new Date(storyA.creationDate());
                    });
                    return temp;
                },
                deferEvaluation: true
            }),
            segments = ko.computed({
                read: function () {
                    appdata.arrangeNewsFlag();

                    var programElements = dc.programElements.getByEpisodeId(appdata.currentEpisode().id)();
                    var segmentArray = _.filter(programElements, function (programElement) {
                        return programElement.programElementType === e.ProgramElementType.Segment;
                    });

                    if (segmentArray.length > 0) {
                        segmentArray = segmentArray.sort(function (entity1, entity2) {
                            return utils.sortNumeric(entity1.sequenceId, entity2.sequenceId, 'asc');
                        });
                    }
                    if (isStoriesChanged()) {
                        if (segmentArray.length > 0) {
                            _.filter(dc.bunches.getObservableList(), function (item) {
                                item.topNews().isUsed(false);
                            });
                            for (var i = 0; i < segmentArray.length; i++) {
                                for (var j = 0; j < segmentArray[i].stories().length; j++) {
                                    _.filter(dc.bunches.getObservableList(), function (item) {
                                        if (item.topNews().id == segmentArray[i].stories()[j].news.id) {
                                            item.topNews().isUsed(true);
                                            segmentArray[i].stories()[j].isUsed(true);
                                        }
                                    });
                                }
                            }
                            isStoriesChanged(false);
                        }
                    }
                    return segmentArray;
                },
                deferEvaluation: true
            }),
            notifications = ko.computed({
                read: function () {
                    var arr = dc.notifications.getObservableList();
                    arr = arr.sort(function (entity1, entity2) {
                        return utils.sortDateTime(entity1.creationDate(), entity2.creationDate(), 'desc');
                    });
                    return arr;
                },
                deferEvaluation: true
            }),
            unreadNotificationsCount = ko.computed({
                read: function () {
                    var arr = [];
                    arr = _.filter(dc.notifications.getObservableList(), function (tempObj) {
                        return !tempObj.isRead();
                    });

                    return arr ? arr.length : 0;
                },
                deferEvaluation: true
            }),
            shellLeftVisible = ko.computed({
                read: function () {
                    if (router.currentHash() === config.hashes.production.production ||
                        router.currentHash() === config.hashes.production.productionRundown ||
                        router.currentHash().indexOf('topicselection') !== -1 ||
                        router.currentHash() === config.hashes.production.sequence ||
                        router.currentHash() === config.hashes.production.myTicker ||
                        router.currentHash() === config.hashes.production.report ||
                        router.currentHash() === config.views.fieldreporter.reportNews.url ||
                        router.currentHash() === config.hashes.production.bureauTicker ||
                        router.currentHash() === config.hashes.production.filterPreference ||
                        router.currentHash() === config.hashes.production.uploadMedia ||
                        router.currentHash() === config.hashes.production.XMLParser||
                        router.currentHash() === config.hashes.production.channelTicker ||
                        router.currentHash() === config.hashes.production.alertTicker ||
                        router.currentHash() === config.hashes.production.breakingTicker ||
                        router.currentHash() === config.hashes.production.categoryTicker ||
                        router.currentHash() === config.hashes.production.producerArchivalMedia
                        || router.currentHash() === config.hashes.production.newsFlow)
                        return false;
                    else
                        return true;
                },
                deferEvaluation: true
            }),
            tickershellLeftVisible = ko.computed({
                read: function () {
                    if (router.currentHash() === config.hashes.production.myTicker || router.currentHash() === config.hashes.production.rundown)
                        return true;
                    else
                        return false;
                },
                deferEvaluation: true
            }),
            currentHash = ko.computed({
                read: function () {
                    if (router.currentHash() === config.hashes.production.alertTicker)
                    {
                        appdata.currentCommand('lat');
                    }
                    if (router.currentHash() === config.hashes.production.breakingTicker)
                    {
                        appdata.currentCommand('brk');
                    }
                    if (router.currentHash() === config.hashes.production.categoryTicker)
                    {
                        appdata.currentCommand('nrm');
                    }
                    
                    return router.currentHash();
                },
                deferEvaluation: true
            }),
            activeHash = ko.computed({
                read: function () {
                    if (currentHash().indexOf('topicselection') !== -1) {
                        return menuHashes.production.topicSelection;
                    } else if (currentHash() == menuHashes.production.home) {
                        return menuHashes.production.home;
                    } else if (currentHash() == menuHashes.production.sequence) {
                        return menuHashes.production.sequence;
                    } else if (currentHash() == menuHashes.production.production) {
                        return menuHashes.production.production;
                    } else if (currentHash() == menuHashes.production.pendingStories) {
                        return menuHashes.production.pendingStories;
                    } else if (currentHash() == menuHashes.production.report) {
                        return menuHashes.production.report;
                    }
                    else if (currentHash() == menuHashes.production.myheadline) {
                        return menuHashes.production.myheadline;
                    }
                    else if (currentHash() == config.hashes.production.headlineUpdates) {
                        return config.hashes.production.headlineUpdates;
                    }
                    else if (currentHash() == menuHashes.production.myTicker) {
                        return menuHashes.production.myTicker;
                    }
                    else if (currentHash() == menuHashes.production.rundown) {
                        return menuHashes.production.rundown;
                    }
                    else if (currentHash() == config.views.fieldreporter.reportNews.url) {
                        return config.views.fieldreporter.reportNews.url;
                    }
                    else if (currentHash() == config.hashes.production.bureauTicker) {
                        return config.hashes.production.bureauTicker;
                    }
                    else if (currentHash() == config.hashes.production.uploadMedia) {
                        return config.hashes.production.uploadMedia;
                    }
                    else if (currentHash() == config.hashes.production.channelTicker) {
                        return config.hashes.production.channelTicker;
                    }
                    else if (currentHash() == config.hashes.production.alertTicker) {
                        return config.hashes.production.alertTicker;
                    }
                    else if (currentHash() == config.hashes.production.breakingTicker) {
                        return config.hashes.production.breakingTicker;
                    }
                    else if (currentHash() == config.hashes.production.categoryTicker) {
                        return config.hashes.production.categoryTicker;
                    }
                    else if (currentHash() == config.hashes.production.producerArchivalMedia) {
                        return config.hashes.production.producerArchivalMedia;
                    }
                },
                deferEvaluation: true
            }),

            // Methods
            //-------------------
            openChat = function () {
                var encryptedName = helper.getCookie('EncriptedName');
                window.open('/chat.html?name=' + encryptedName, '_blank');
            },
            fileManager = function () {
                var encryptedName = helper.getCookie('EncriptedUserId');
                window.open('http://10.3.12.119/FileManagementSystem.html?name=' + encryptedName, '_blank');
            },
            clearCreadentials = function () {
                currentPassword('');
                newPassword('');
                confirmPassword('');
            },

            submitPassword = function () {
                //if (currentPassword() == $.trim(helper.getCookie('Password'))) {
                //    var reqObj = {
                //        userId: $.trim(helper.getCookie('user-id')),
                //        password: newPassword()
                //    }
                //    if (newPassword() == confirmPassword()) {
                //        $.when(manager.production.changeUserPassword(reqObj))
                //        .done(function (responseObj) {
                //            if (responseObj.IsSuccess) {
                //                document.cookie = "Password" + '=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';
                //                document.cookie = "Password" + "=" + $.trim(newPassword());
                //                config.logger.success("Your Password Changed Successfully!");
                //                clearCreadentials();
                //            }
                //            hidePasswordChange();
                //        })
                //       .fail(function (responseObj) {
                //           hidePasswordChange();
                //       });
                //    }
                //    else {
                //        config.logger.success("Your Password Changed Successfully!");
                //    }
                //}
                //else {
                //    config.logger.error('Please Provide Valid Credentials');
                //}
                homeVM.submitPassword();

            },
            createNews = function () {
                  $("#content-viewer-main").children('#contentviewer-overlay').addClass('warningOverlay');
                  contentviewerVM.isCreateNewsVisible(true);
                  $("#createNewsPopup").addClass('isNews');
                  appdata.isNewsCreated(true);
            },
            setCurrentNews = function (data, isRelated) {
                presenter.showAlert(false);
                homeVM.setCurrentNews(data, isRelated);
            },
            toggleFilter = function (data) {
                homeVM.toggleFilter(data);
            },
            toggleStoryFilter = function (data) {
                if (router.currentHash() === config.hashes.production.headlineUpdates || router.currentHash() === config.hashes.production.myheadline) {
                    headlineVm.toggleHeadlineFilter(data);
                }
                else
                    homeVM.toggleStoryFilter(data);
            },
            toggleFilterInsertion = function (data) {
                tickerVm.toggleFilterInsertion(data);
            },
            proceedToSequence = function () {
                appdata.currentProgramStoryCount.valueHasMutated();
                router.navigateTo(config.hashes.production.sequence);
            },
            removeStoryFromSegment = function (story) {
                if (story) {
                    manager.production.removeStoryOnServer(story)
                    appdata.arrangeNewsFlag(!appdata.arrangeNewsFlag());
                }
            },
            checkForUpdates = function () {
                if (app) {
                }
            },
            logOut = function () {
                window.location.href = '/login#/index';
                document.cookie = config.sessionKeyName + '=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';
                document.cookie = config.roleid + '=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';
                document.cookie = config.userid + '=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';
                document.cookie = config.locationId + '=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';
                document.cookie = config.newsTypeId + '=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';
                document.cookie = config.UserName + '=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';
                document.cookie = "Password" + '=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';
                document.cookie = "EncriptedName" + '=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';



            },
            changePassword = function () {
                //isPasswordChanged(true);
                //$("#content-viewer-main").children('#contentviewer-overlay').addClass('warningOverlay');
                contentviewerVM.changePassword();
            },
            hidePasswordChange = function () {
                //isPasswordChanged(false);
                //$("#content-viewer-main").children('#contentviewer-overlay').removeClass('warningOverlay');
                contentviewerVM.hidePasswordChange();
                //clearCreadentials();
            },
            markNotificationRead = function (data) {
                if (!data.isRead()) {
                    var arr = _.filter(dc.notifications.getObservableList(), function (tempObj) {
                        if (tempObj.id == data.id) {
                            tempObj.isRead(!tempObj.isRead());
                            return tempObj;
                        };
                    });
                    var reqObj = [];
                    reqObj.nid = arr[0].id,
                    reqObj.uid = parseInt($.trim(helper.getCookie('user-id')));
                    sendToServer(reqObj);
                }
                HideNotificationSlide();
                if (data.argument) {
                    try {
                        var arg = JSON.parse(data.argument);
                        if (arg && arg.SlotId && arg.SlotScreenTemplateId) {
                            appdata.isNotifying = true;
                            productionVM.navigateToTemplateDesigner(arg.SlotId, arg.SlotScreenTemplateId);
                        }
                    } catch (e) {
                        //console.log(e.message);
                    }
                }
            },
            markNotificationAlertAsRead = function (data) {
                markNotificationRead(data);
                presenter.showNotificaiton(false);
            },
            deleteNotificationAlert = function (data) {
                if (!data.isRead()) {
                    var arr = _.filter(dc.notifications.getObservableList(), function (tempObj) {
                        if (tempObj.id == data.id) {
                            tempObj.isRead(!tempObj.isRead());
                            return tempObj;
                        };
                    });
                    var reqObj = [];
                    reqObj.nid = arr[0].id,
                    reqObj.uid = parseInt($.trim(helper.getCookie('user-id')));
                    sendToServer(reqObj);
                    presenter.showNotificaiton(false);
                }
            },
            markAllRead = function () {
                var arr = _.filter(dc.notifications.getObservableList(), function (tempObj) {
                    if (!tempObj.isRead()) {
                        tempObj.isRead(true)
                        return tempObj;
                    };
                });
                HideNotificationSlide();
                var reqObj = [];
                reqObj.nid = -1,
                reqObj.uid = parseInt($.trim(helper.getCookie('user-id')));
                sendToServer(reqObj);
            },
            sendToServer = function (reqObj) {
                manager.production.markNotificaitonAsRead(reqObj);
            },
            displayHelp = function () {
                router.navigateTo(config.hashes.production.report);
            },
            HideNotificationSlide = function () {
                setTimeout(function () {
                    $('.notifications').parent('div').hide();
                }, 500);
            },
            changeTickerViewId = function (value) {
                if (value === config.hashes.production.rundown)
                    appdata.currentViewId(e.UserType.TickerManager);
                else if (value === config.hashes.production.myTicker && appdata.currentUser().UserType !== e.UserType.TickerWriter)
                    appdata.currentViewId(e.UserType.TickerProducer);
            },
            addReporterCss = function () {
                $('body').addClass("reporterNews");
            },
            removeBodyCss = function () {
                $('body').removeClass("reporterNews");
                $('body').removeClass("scrollpadding");
                $('html').removeClass("scrolstart scrolstart1");
            },
            displaySettings = function () {
                router.navigateTo(config.hashes.production.filterPreference);
            },

            activate = function (routeData) {
            },
            init = function () {
                appdata.currentAlertNewsId.subscribe(function (value) {
                    var news = dc.news.getLocalById(value);
                    if (news) {
                        currentTopNews(news);
                        if (setTimeOutArrayNewsAlerts.length > 0) {
                            clearTimeout(setTimeOutArrayNewsAlerts[0]);
                        }
                        presenter.showAlert(true);
                        var newsAlertIds = setTimeout(function () {
                            presenter.showAlert(false);
                        }, 5000);

                        setTimeOutArrayNewsAlerts = [];
                        setTimeOutArrayNewsAlerts.push(newsAlertIds);
                    }
                });
                appdata.currentNotificationAlertId.subscribe(function (currentNotification) {
                    var notification = dc.notifications.getLocalById(currentNotification.id);
                    if (notification) {
                        if (!currentTopNotification())
                        { presenter.showNotificaiton(true); }
                        currentTopNotification(notification);
                        if (setTimeOutArrayNotificationsAlerts.length > 0) {
                            clearTimeout(setTimeOutArrayNotificationsAlerts[0]);
                        }
                        var ids = setTimeout(function () {
                            presenter.showNotificaiton(false);
                        }, 5000);
                        setTimeOutArrayNotificationsAlerts = [];
                        setTimeOutArrayNotificationsAlerts.push(ids);
                    }
                });

                if (currentHash() === "#/report-news") {
                    addReporterCss();
                }
                currentHash.subscribe(function (value) {
                    removeBodyCss();
                    if (value === "#/home" || value === "#/myheadline" || value === "#/headlineUpdates") {
                        $('body').addClass("scrollpadding");
                    }
                    else if (value === "#/report-news") {
                        addReporterCss();
                    }

                    changeTickerViewId(value);
                });
            };

        return {
            currentHash: currentHash,
            activeHash: activeHash,
            activate: activate,
            menuHashes: menuHashes,
            shellRightVisible: shellRightVisible,
            shellLeftVisible: shellLeftVisible,
            channels: channels,
            segments: segments,
            templates: templates,
            init: init,
            sourceFilterControl: sourceFilterControl,
            calendarControl: calendarControl,
            appdata: appdata,
            programs: programs,
            episodes: episodes,
            toggleFilter: toggleFilter,
            proceedToSequence: proceedToSequence,
            checkForUpdates: checkForUpdates,
            toggleStoryFilter: toggleStoryFilter,
            currentTopNews: currentTopNews,
            setCurrentNews: setCurrentNews,
            presenter: presenter,
            removeStoryFromSegment: removeStoryFromSegment,
            logOut: logOut,
            unreadNotificationsCount: unreadNotificationsCount,
            notifications: notifications,
            markNotificationRead: markNotificationRead,
            markAllRead: markAllRead,
            currentTopNotification: currentTopNotification,
            markNotificationAlertAsRead: markNotificationAlertAsRead,
            displayHelp: displayHelp,
            deleteNotificationAlert: deleteNotificationAlert,
            config: config,
            router: router,
            generalFilterControl: generalFilterControl,
            generalFilterTickerControl: generalFilterTickerControl,
            e: e,
            tickershellLeftVisible: tickershellLeftVisible,
            toggleFilterInsertion: toggleFilterInsertion,
            changeTickerViewId: changeTickerViewId,
            displaySettings: displaySettings,
            newsbucketList: newsbucketList,
            fileManager:fileManager,
            changePassword: changePassword,
            hidePasswordChange: hidePasswordChange,
            submitPassword: submitPassword,
            openChat: openChat,
            createNews: createNews
        };
    });