﻿define('vm.ticker',
    ['config', 'messenger', 'model', 'datacontext', 'model.mapper', 'moment', 'appdata', 'enum', 'presenter', 'manager', 'underscore', 'utils', 'router', 'vm.contentviewer'],
    function (config, messenger, model, dc, mapper, moment, appdata, e, presenter, manager, _, utils, router, vmContentViewer) {
        var
            hashes = config.hashes.production,
            calendarControl = new model.CalendarControl(),
            templates = config.templateNames,
            tickers = ko.observableArray([]),
            pageIndex = ko.observable(1),
            searchKeywords = ko.observable('').extend({ throttle: config.throttle }),
            tickerCountByStatus = ko.observable({ pendingTickerCount: 0, approvedTickerCount: 0, onHoldTickerCount: 0, rejectedTickerCount: 0 }),
            currentRepeatedCount = ko.observable(0),
            tickerList = ko.observableArray([]),
            isLineInsert = ko.observable(false),
            isLineEdited = ko.observable(false),
            alltickersCount = ko.observable(0),
            textBuffer = ko.observable(''),
            currentSelectedBureau = ko.observable(0),
            bureauTeamCount = ko.observable(0),
            currentTickerStatusId = ko.observable(e.TickerStatus.Pending),
           

            // Computed Properties
            // ------------------------
            isBureau = ko.computed({
                read: function () {
                    return router.currentHash() === hashes.bureauTicker;
                },
                deferEvaluation: true
            }),
            isProducerScreen = ko.computed({
                read: function () {
                    return router.currentHash() === hashes.TickerProducer;
                },
                deferEvaluation: true

            }),
             sendToTwitter = function (data) {
                 alert(data);
             },
            groupedTickers = ko.computed({
                read: function () {
                    var groupedTickers = [];
                    var arr = [];

                    
                    var dataArray = [];

                    var dataArray = manager.ticker.getFilteredTickers();
                    
                    if (isBureau()) {
                        if (currentSelectedBureau().id)
                            dataArray = dc.tickers.getByLocationId(currentSelectedBureau().id)();

                        else if (currentSelectedBureau() === 'AllBureau' || currentSelectedBureau() === 0) {
                            dataArray = dc.tickers.getObservableList();
                            var tempArray = [];
                            if (currentSelectedBureau() === 0) {
                                for (var i = 0, len = dataArray.length  ; i < len; i++) {
                                    if (dataArray[i].locationId() === 0) {
                                        tempArray.push(dataArray[i]);
                                    }
                                }
                                dataArray = tempArray;
                            }
                            else if (currentSelectedBureau() === 'AllBureau') {
                                dataArray = _.filter(dataArray, function (item) {
                                    if (item.locationId()) {
                                        return true;
                                    }
                                    return false;
                                });
                            }
                        }
                    }

                    if (dataArray && dataArray.length > 0) {
                         if (appdata.currentUser().userType == e.UserType.TickerProducer && !isBureau()) {
                            for (var i = 0, len = dataArray.length  ; i < len; i++) {
                                var flag = false;
                                for (var j = 0, len2 = dataArray[i].tickerLines().length ; j < len2; j++) {
                                    var line = dataArray[i].tickerLines()[j];
                                    if (line.categoryStatusId() === currentTickerStatusId()) {
                                        flag = true;
                                    }
                                }
                                if (flag)
                                    arr.push(dataArray[i]);
                            }
                         }
                         else
                             arr = dataArray;

                        arr = utils.arrayOrderBy(arr, "creationDate", "desc");

                        for (var i = 0, len = arr.length; i < len; i++) {
                            var objMap = {};

                            var groupName = moment(arr[i].creationDate()).format("LL hh") + ":00 " + moment(arr[i].creationDate()).format("a");

                            objMap.groupName = groupName;
                            objMap.groupElements = [];
                            objMap.groupElements.count = 0;
                            objMap.groupElements.tickerLines = 0;
                            objMap.type = e.TickerType.Category.Value;
                            var object = utils.arrayIsContainObject(groupedTickers, objMap, "groupName");


                            var tempCount = _.filter(arr[i].tickerLines(), function (line) {
                                if (currentTickerStatusId() === e.TickerStatus.Approved && line.categoryStatusId() === e.TickerStatus.Approved) {
                                    return line;
                                }
                                if (currentTickerStatusId() === e.TickerStatus.Pending && line.categoryStatusId() === e.TickerStatus.Pending) {
                                    return line;
                                }
                                if (currentTickerStatusId() === e.TickerStatus.OnHold && line.categoryStatusId() === e.TickerStatus.OnHold) {
                                    return line;
                                }
                                if (currentTickerStatusId() === e.TickerStatus.OnAired && line.categoryStatusId() === e.TickerStatus.OnAired) {
                                    return line;
                                }

                                if (currentTickerStatusId() === e.TickerStatus.Rejected && line.categoryStatusId() === e.TickerStatus.Rejected) {
                                    return line;
                                }

                            });
                            arr[i].tickerLinesCount(tempCount.length);


                            if (!object.flag) {
                                objMap.groupElements.push(arr[i]);
                                objMap.groupElements.tickerLines = arr[i].tickerLines();
                                objMap.groupElements.count = objMap.groupElements.length;
                                groupedTickers.push(objMap);
                            }
                            else {
                                groupedTickers[object.objectIndex].groupElements.push(arr[i]);
                                groupedTickers[object.objectIndex].groupElements.count = groupedTickers[object.objectIndex].groupElements.length;
                            }
                        }
                        updateTickerStatusCount();
                    }
                    if(groupedTickers.length > 0)
                        return groupedTickers;
                    return [];
                },
                deferEvaluation: true
            }),
            bureauLocations = ko.computed({
                read: function () {
                   
                    var arr = dc.newsLocation.getObservableList();
                    var tempArray = [];
                    for (var i = 0, len = arr.length; i < len; i++) {
                        var location = arr[i];
                        var ticker = dc.tickers.getByLocationId(location.id)();
                        if (ticker && ticker.length > 0) {
                            tempArray.push(location);
                        }
                    }
                    return tempArray;
                },
                deferEvaluation: true
            }),

            isEditorSectionVisible = ko.computed({
                read: function () {
                    return appdata.currentUser().userType !== e.UserType.TickerWriter;
                },
                deferEvaluation: true
            }),
            isStatusInfoVisible = ko.computed({
                read: function () {
                    return appdata.currentUser().userType === e.UserType.TickerWriter;
                },
                deferEvaluation: true
            }),
            isProducerHeaderVisible = ko.computed({
                read: function () {
                    if (router.currentHash() === config.hashes.production.myTicker && appdata.currentUser().userType !== e.UserType.TickerWriter) {
                        return true;
                    }
                    return false;
                },
                deferEvaluation: true
            }),

            // Methods
            // ------------------------

            selectTicker = function (data) {
                manager.news.selectNews(data, true);
                appdata.isStorySavedDirty(true);
            },
            isRundownView = ko.computed({
                read: function () {
                    return router.currentHash() === config.hashes.production.rundown;
                },
                deferEvaluation: true

            }),
            updateTickerStatusCount = function () {
                var arr = dc.tickers.getObservableList();
                var pendingCount = 0;
                var approvedCount = 0;
                var onHoldCount = 0;
                var rejectedCount = 0;
                alltickersCount(arr.length);
                for (var i = 0, len = arr.length; i < len; i++) {
                    var ticker = arr[i];

                    for (var j = 0; j <  arr[i].tickerLines().length; j++) {
                        switch (arr[i].tickerLines()[j].categoryStatusId()) {
                            case e.TickerStatus.Pending:
                                pendingCount++;
                                break;
                            case e.TickerStatus.Approved:
                            case e.TickerStatus.Freezed:
                                approvedCount++;
                                break;
                            case e.TickerStatus.OnHold:
                                onHoldCount++;
                                break;
                            case e.TickerStatus.Rejected:
                                rejectedCount++;
                                break;
                        }
                    }
                }

                var obj = {};

                obj.pendingTickerCount = pendingCount;
                obj.approvedTickerCount = approvedCount;
                obj.onHoldTickerCount = onHoldCount;
                obj.rejectedTickerCount = rejectedCount;

                tickerCountByStatus(obj);
            },
            toggleFreeze = function (data, type) {
                if (type === e.TickerType.Breaking.Value) {
                    if (data.breakingStatusId() === e.TickerStatus.Freezed) {
                        updateTickerStatus(data, e.TickerStatus.Approved, type)
                    }
                    else {
                        updateTickerStatus(data, e.TickerStatus.Freezed, type);
                    }
                }
                else if (type === e.TickerType.Latest.Value) {
                    if (data.latestStatusId() === e.TickerStatus.Freezed) {
                        updateTickerStatus(data, e.TickerStatus.Approved, type)
                    }
                    else {
                        updateTickerStatus(data, e.TickerStatus.Freezed, type);
                    }
                }
                else if (type === e.TickerType.Category.Value) {
                    if (data.categoryStatusId() === e.TickerStatus.Freezed) {
                        updateTickerStatus(data, e.TickerStatus.Approved, type)
                    }
                    else {
                        updateTickerStatus(data, e.TickerStatus.Freezed, type);
                    }
                }
            },
            toggleFilterInsertion = function (data) {
                manager.ticker.toggleFilterInsertion(data);
                appdata.tickerRefresh(!appdata.tickerRefresh());
            },
            insertTickerLine = function (data) {
                textBuffer('');
                isLineInsert(true);
            },
            editTickerLine = function (data) {
                isLineEdited(true);
            },
            openCreateTicker = function () {
                var ticker = new model.Ticker();
                vmContentViewer.twc.cssClass('hideLeftpanel');
                vmContentViewer.setCurrentTicker(ticker);
                vmContentViewer.twc.isEdit(false);
                vmContentViewer.twc.tickerPopupInputVisible(false);
            },
            getTickerLineRequestObject = function (data, type) {
                if (isLineInsert()) {
                    var tempObj = {
                        TickerId: data.id,
                        Text: textBuffer(),
                        LanguageCode: 'ur',
                        SequenceId: data.tickerLines().length + 1,
                    }
                    return tempObj;
                }
                else if (data && data instanceof model.TickerLine) {
                    var tempObj = {
                        TickerId: data.tickerId,
                        Text: data.title(),
                        LanguageCode: 'ur',
                        TickerLineId: data.id,
                        TickerTypeId: type
                    }
                    return tempObj;
                }
                else {
                    var tempObj = {
                        TickerId: data.id,
                        Text: textBuffer(),
                        LanguageCode: 'ur',
                        TickerLineId: parseInt($("#tickerInput" + data.id)[0].name),
                        TickerTypeId: type
                    }
                    return tempObj;
                }
                textBuffer('');
            },
            updateTitle = function (data) {
                if (isLineInsert()) {
                    var tempObj = getTickerLineRequestObject(data);

                    if (!tempObj.Text.trim()) {
                        config.logger.error("Title required!")
                        return;
                    }
                    var reqObj = {
                        TickerLines: [tempObj]
                    };
                    manager.ticker.saveTickerLinesOnServer(reqObj);
                    isLineInsert(false);
                }
                else if (isLineEdited()) {
                    var tempObj = getTickerLineRequestObject(data);
                    if (!tempObj.Text.trim()) {
                        config.logger.error("Title required!")
                        return;
                    }
                    manager.ticker.updateTickerLines(tempObj);
                    isLineEdited(false);
                }
                appdata.ticker
            },
            updateTickerStatus = function (data, status, type) {
                manager.ticker.updateTickerStatus(data, status, type);
            },
            switchTab = function (view) {
                if (view == e.TickerStatus.Pending) {
                    
                    currentTickerStatusId(e.TickerStatus.Pending);
                }
                else if (view == e.TickerStatus.Approved) {
                    currentTickerStatusId(e.TickerStatus.Approved);
                    
                    
                }
                else if (view == e.TickerStatus.OnHold) {
                    currentTickerStatusId(e.TickerStatus.OnHold);
                }
                else if (view == e.TickerStatus.Rejected) {
                    currentTickerStatusId(e.TickerStatus.Rejected);
                }
                appdata.currentTickerStatus(currentTickerStatusId());
            },
            setCurrentSelectedBureau = function (value) {
                if (value && value.id) {
                    currentSelectedBureau(value);
                    appdata.currentSelectedBureau(value.id);
                    dc.newsLocation.getLocalById(value.id).tickerUnreadCount(0);
                }
                else if (value === 0) {
                    currentSelectedBureau(value);
                    appdata.currentSelectedBureau(value);
                    appdata.bureauTeamCount(0);
                }
                else if (value === 'AllBureau') {
                    currentSelectedBureau(value);
                    appdata.currentSelectedBureau(value);
                    appdata.bureauAllTickerCount(0);
                }
            },
            setCalendarDate = function () {
                appdata.lpTickerFromDate = calendarControl.fromDate();
                appdata.lpTickerToDate = calendarControl.toDate();
                appdata.tickerRefresh(!appdata.tickerRefresh());
                manager.ticker.getMoreTickers();
            },
            loadNextPage = function () {
                presenter.toggleActivity(true);
                $.when(manager.ticker.getMoreTickers())
                .done(function () {
                    //var pIndex = pageIndex() + 1;
                    //pageIndex(pIndex);
                    presenter.toggleActivity(false);
                })
                .fail(function () {
                    presenter.toggleActivity(false);
                });
            },
            subscribeEvents = function () {
                searchKeywords.subscribe(function (value) {
                    appdata.searchTickerKeywords = value;
                    appdata.lpStartIndexTicker = 0;
                    appdata.tickerRefresh(!appdata.tickerRefresh());
                    manager.ticker.getMoreTickers();
                });
                pageIndex.subscribe(function (value) {
                    appdata.tickerRefresh(!appdata.tickerRefresh());
                });
                textBuffer.subscribe(function (value) {
                    //console.log("asdasd");
                });
                currentSelectedBureau.subscribe(function (value) {

                });
            },
           

            editTicker = function (data, isFromBureau) {
               
                if(appdata.currentUser().userType === e.UserType.HeadlineProducer){
                    return false;
                }
                if (isFromBureau == true) {
                    vmContentViewer.twc.isFromBureau(isFromBureau);
                }
               
                appdata.istickerPopupVisible(true);
                vmContentViewer.twc.tickerPopupInputVisible(true);
                vmContentViewer.twc.currentTicker(data);
                if (data.newsGuid() && dc.news.getLocalById(data.newsGuid())) {
                    vmContentViewer.twc.currentTicker().news(dc.news.getLocalById(data.newsGuid()));
                    vmContentViewer.twc.cssClass('showleftpanel');
                }
                else {
                    vmContentViewer.twc.cssClass('hideLeftpanel');
                    vmContentViewer.twc.currentTicker().news(new model.News())
                }
                vmContentViewer.twc.tickerLineList(data.tickerLines());
                vmContentViewer.twc.isEdit(true);
                if (!isRundownView() && vmContentViewer.twc.isEdit() && currentTickerStatusId() === e.TickerStatus.Approved) {
                    vmContentViewer.twc.isDisabled(true);
                }
                vmContentViewer.twc.manager = manager;
                vmContentViewer.twc.currentTickerCategory(data.categoryId);
                vmContentViewer.twc.removedTickers([]);
            },

            activate = function (routeData, callback) {
                messenger.publish.viewModelActivated({ canleaveCallback: canLeave });
                $('body').addClass('tickerspage');
                $('html').removeClass('scrolstart scrolstart1');
            },
            canLeave = function () {
                $('body').removeClass('tickerspage');
                return true;
            },
            init = function () {
                calendarControl.currentOption('lastmonth');
                appdata.tickerCalendarControlReference(calendarControl);
                subscribeEvents();
                switchTab(e.TickerStatus.Pending);
                if (appdata.currentUser().userType === e.UserType.TickerProducer) {
                    currentSelectedBureau('AllBureau')
                }
            };

        return {
            activate: activate,
            canLeave: canLeave,
            calendarControl: calendarControl,
            init: init,
            templates: templates,
            groupedTickers: groupedTickers,
            appdata: appdata,
            e: e,
            searchKeywords: searchKeywords,
            loadNextPage: loadNextPage,
            updateTitle: updateTitle,
            currentRepeatedCount: currentRepeatedCount,
            switchTab: switchTab,
            insertTickerLine: insertTickerLine,
            updateTickerStatus: updateTickerStatus,
            toggleFilterInsertion: toggleFilterInsertion,
            setCalendarDate: setCalendarDate,
            tickerCountByStatus: tickerCountByStatus,
            editTickerLine: editTickerLine,
            utils: utils,
            alltickersCount: alltickersCount,
            router: router,
            config: config,
            model: model,
            isEditorSectionVisible: isEditorSectionVisible,
            isStatusInfoVisible: isStatusInfoVisible,
            openCreateTicker: openCreateTicker,
            isProducerHeaderVisible: isProducerHeaderVisible,
            tickerList: tickerList,
            toggleFreeze: toggleFreeze,
            dc: dc,
            textBuffer: textBuffer,
            selectTicker: selectTicker,
            hashes: hashes,
            currentTickerStatusId: currentTickerStatusId,
            currentSelectedBureau: currentSelectedBureau,
            bureauLocations: bureauLocations,
            bureauTeamCount: bureauTeamCount,
            setCurrentSelectedBureau: setCurrentSelectedBureau,
            editTicker: editTicker,
            isRundownView: isRundownView,
            isProducerScreen: isProducerScreen,
            sendToTwitter: sendToTwitter
            
        };
    });