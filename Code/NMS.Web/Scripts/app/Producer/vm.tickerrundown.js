﻿define('vm.tickerrundown',
     ['config', 'messenger', 'model', 'datacontext', 'model.mapper', 'moment', 'appdata', 'enum', 'presenter', 'manager', 'underscore', 'utils', 'router', 'vm.contentviewer', 'vm.ticker', 'store'],
     function (config, messenger, model, dc, mapper, moment, appdata, e, presenter, manager, _, utils, router, vmContentViewer, tickerVm, store) {
         var
              templates = config.templateNames,
              hashes = config.hashes.production,
              mcrBreakingTickerList = ko.observableArray([]),
              mcrLatestTickerList = ko.observableArray([]),
              mcrCategoryTickerList = ko.observableArray([]),
              currentView = ko.observable('onair'),
              operatorDisplayCount = ko.observableArray([]),
              currentTickerTemplateType = ko.observable(''),
              // Computed Properties}
             // ------------------------

              breakingNewsTicker = ko.computed({
                  read: function () {
                      var arr = [];
                      arr = dc.tickers.getObservableList();
                      arr = _.filter(arr, function (item) {

                          return item.tickerTypeId === e.TickerType.Breaking.Value;
                      });
                      arr = arr.sort(function (tickerA, tickerB) {
                          return new Date(tickerA.sequenceId) - new Date(tickerB.sequenceId);
                      });


                      return arr;
                  },
                  deferEvaluation: true
              }),

              latestNewsTicker = ko.computed({
                  read: function () {
                      var arr = [];
                      arr = dc.tickers.getObservableList();
                      arr = _.filter(arr, function (item) {
                          return item.tickerTypeId === e.TickerType.Latest.Value;
                      });
                      arr = arr.sort(function (tickerA, tickerB) {
                          return new Date(tickerA.creationDate()) - new Date(tickerB.creationDate());
                      });
                      return arr;
                  },
                  deferEvaluation: true
              }),
              categoryNewsTicker = ko.computed({
                  read: function () {
                      var arr = [];
                      arr = dc.tickers.getObservableList();
                      arr = _.filter(arr, function (item) {
                          return item.tickerTypeId === e.TickerType.Category.Value;
                      });
                      arr = arr.sort(function (tickerA, tickerB) {
                          return new Date(tickerA.sequenceId) - new Date(tickerB.sequenceId);
                      });
                      return arr;
                  },
                  deferEvaluation: true
              }),
              currentTickerType = ko.computed({
                  read: function () {
                      if (router.currentHash() === hashes.breakingTicker)
                          return e.TickerType.Breaking.Value;
                      if (router.currentHash() === hashes.alertTicker)
                          return e.TickerType.Latest.Value;
                      if (router.currentHash() === hashes.categoryTicker)
                          return e.TickerType.Category.Value;
                  },
                  deferEvaluation: true
              }),
              isRepoContainEmptyTicker = ko.computed({
                  read: function () {
                      var arr = dc.tickers.getObservableList();
                      for (var i = 0; i < arr.length; i++) {
                          var ticker = arr[i];
                          if (ticker.emptyTickerBit()) {
                              return true;
                          }
                      }
                      return false;
                  },
                  deferEvaluation: true
              }),
              currentOnAirTickerCount = ko.computed({
                  read: function () {
                      var arr = dc.tickers.getObservableList();
                      arr = _.filter(arr, function (item) {
                          return item.tickerTypeId === currentTickerType();
                      });
                      var count = 0;
                      if (arr && arr.length > 0) {
                          for (var i = 0; i < arr.length; i++) {
                              var tempTickerLines = _.filter(arr[i].tickerLines(), function (item) {
                                  return item.isShow() === true;
                              });
                              count = count + tempTickerLines.length;
                          }
                      }
                      return count;
                  }
              }),
                // Methods
                // ------------------------

              openCreateSegment = function () {
                  appdata.currentWorkingTicker(0);
                  vmContentViewer.isCreateSegmentVisible(true);
              },
              editSegment = function (data) {
                  appdata.currentWorkingTicker(data.id);
                  vmContentViewer.newTickerGroupName(data.tickerGroupName());
                  vmContentViewer.editTickerSegmentId(data.id);
                  vmContentViewer.isCreateSegmentVisible(true);
              },
              setCurrentTickerLineOperator = function (data, event) {
                  data.currentTickerOperator(parseInt(event.target.value));

                  if (data.currentTickerOperator() === -1) {
                      data.title('');
                      return;
                  }


                  var ticker = _.filter(dc.tickers.getAllLocal(), function (item) {
                      return item.id == data.id;
                  });
                  if (ticker && ticker.length > 0) {
                      var arr = _.filter(ticker[0].tickerLines(), function (item) {
                          return item.operatorNo == data.currentTickerOperator();
                      });
                      if (arr && arr.length > 0) {
                          data.title(arr[0].title());
                      }
                      else
                          data.title('');
                  }
              },
              insertOnAirTickerLine = function (data) {
                  if (data.title && !data.title().length > 0 && !data.title().trim()) {
                      config.logger.error("Please write a line!");
                      return;
                  }
                  if (data.currentTickerOperator() === -1 || isNaN(data.currentTickerOperator())) {
                      config.logger.error("Please select an operator!");
                      return;
                  }
                  appdata.currentWorkingTicker(data.id);
                  $.when(manager.ticker.insertOnAirTickerLine(data, currentTickerType()))
                  .done(function (responseData) {
                      data.currentTickerOperator(-1);
                      data.title('');
                      sendTemplateCommand();
                  })
                  .fail(function () {

                  });
                  

              },
              sendTemplateCommand = function () {
                  if (appdata.lastCommand()) {
                      if (appdata.lastCommand() != appdata.currentCommand()) {
                          var req = {
                              Type: appdata.currentCommand()
                          }
                          $.when(manager.ticker.setTickerTemplate(req))
                            .done(function (responseData) {
                                appdata.lastCommand(appdata.currentCommand());
                            })
                            .fail(function () {
                            });

                      }
                  }
                  else {
                      var req = {
                          Type: appdata.currentCommand()
                      }
                      $.when(manager.ticker.setTickerTemplate(req))
              .done(function (responseData) {
                  appdata.lastCommand(appdata.currentCommand());
              })
              .fail(function () {
              });
                  }
              },
              markOnAirTickerStatus = function (data, type) {
                  manager.ticker.markOnAirTickerStatus(data, currentTickerType());
              },
              deleteCompleteGroup = function (data) {
                  if (confirm('Are you sure you want to delete this segment?')) {
                      manager.ticker.deleteCompleteGroup(data);
                  }
              },
              sendToTwitter = function (data) {
                  console.log(data);
              },
              editBeforeSendToTwitter = function (data) {
                  vmContentViewer.twitterMessage(data.title());
                  vmContentViewer.isTwitterVisible(true);
              },
              setTickerTemplateType = function (event) {
                  if (parseInt(event.target.value) != -1) {
                      currentTickerTemplateType(event.target.value);
                  }
                  else {
                      config.logger.error("Please Select Type!");
                  }
              },
              sendCommandToPCR = function () {
                  if (currentTickerTemplateType() && currentTickerTemplateType() != -1) {
                      var req = {
                          Type: currentTickerTemplateType()
                      }
                      $.when(manager.ticker.setTickerTemplate(req))
              .done(function (responseData) {
              })
              .fail(function () {
              });
                  }
                  else {
                      config.logger.error("Please Select Type!");
                  }

              },
              switchTab = function (view) {
                  switch (view) {
                      case 'mcrview':
                          currentView('mcrview');
                          getMCRLatestView();
                          break;
                      case 'onair':
                          currentView('onair');
                          break;
                  }
              },
              getMCRLatestView = function () {
                  if (currentTickerType() === e.TickerType.Breaking.Value) {
                      mcrBreakingTickerList([]);
                      manager.ticker.getMCRBreakingTickers(mcrBreakingTickerList);
                  }
                  else if (currentTickerType() === e.TickerType.Latest.Value) {
                      mcrLatestTickerList([]);
                      manager.ticker.getMCRLatestTickers(mcrLatestTickerList);
                  }
                  else if (currentTickerType() === e.TickerType.Category.Value) {
                      mcrCategoryTickerList([]);
                      manager.ticker.getMCRCategoryTickers(mcrCategoryTickerList);
                  }
              },
              refreshSortable = function (data, event) {
                  if ($(event.target).children().length === data.tickerLines().length) {
                      $(event.target).sortable("refresh");
                  }
              },
              updateTickerSequence = function (arrayPositions) {
                  var type = parseInt(arrayPositions[0].split("_")[1]);
                  var arr = [];
                  if (arrayPositions) {
                      for (var i = 0, len = arrayPositions.length ; i < len; i++) {
                          var ticker = dc.tickers.getLocalById(arrayPositions[i].split("_")[0]);
                          ticker.sequenceId = i + 1;
                          arr.push(ticker);
                      }
                      manager.ticker.updateTickerSequence(arr, currentTickerType());
                  }
              },
              updateTickerLineSequence = function (arrayPositions) {
                  var type = parseInt(arrayPositions[0].split("_")[1]);
                  var arr = [];
                  if (arrayPositions) {
                      for (var i = 0, len = arrayPositions.length ; i < len; i++) {
                          var ticker = dc.tickerLines.getLocalById(arrayPositions[i].split("_")[0]);
                          ticker.sequenceId = i + 1;
                          arr.push(ticker);
                      }
                      manager.ticker.updateTickerLineSequence(arr, currentTickerType());
                  }
              },

              updateToMCR = function (data) {
                  manager.ticker.updateToMCR(currentTickerType());
              },
              setTickerText = function (data, event) {
                  var text = event.target.value;
                  data.title(text);
              },
              createEmptyGroup = function () {
                  manager.ticker.createSegment(' ', currentTickerType());
              },
              activate = function (routeData, callback) {
                  messenger.publish.viewModelActivated({ canleaveCallback: canLeave });
                  $('body').addClass('tickerspage');
                  $('html').removeClass('scrolstart scrolstart1');
                  switchTab(currentView());
              },
              canLeave = function () {
                  $('body').removeClass('tickerspage');

                  return true;
              },
             init = function () {
                 for (var i = 0; i < 101; i++) {

                     if (i == 0)
                         operatorDisplayCount.push({ text: '-- Select an operator --', operatorId: -1 });
                     else
                         operatorDisplayCount.push({ text: 'Operator ' + i, operatorId: i });

                 }
             };

         return {
             activate: activate,
             canLeave: canLeave,
             init: init,
             templates: templates,
             appdata: appdata,
             e: e,
             appdata: appdata,
             templates: templates,
             hashes: hashes,
             breakingNewsTicker: breakingNewsTicker,
             latestNewsTicker: latestNewsTicker,
             categoryNewsTicker: categoryNewsTicker,
             openCreateSegment: openCreateSegment,
             setCurrentTickerLineOperator: setCurrentTickerLineOperator,
             insertOnAirTickerLine: insertOnAirTickerLine,
             markOnAirTickerStatus: markOnAirTickerStatus,
             deleteCompleteGroup: deleteCompleteGroup,
             currentOnAirTickerCount: currentOnAirTickerCount,
             mcrBreakingTickerList: mcrBreakingTickerList,
             mcrLatestTickerList: mcrLatestTickerList,
             mcrCategoryTickerList: mcrCategoryTickerList,
             getMCRLatestView: getMCRLatestView,
             switchTab: switchTab,
             currentView: currentView,
             currentTickerType: currentTickerType,
             updateTickerSequence: updateTickerSequence,
             updateToMCR: updateToMCR,
             setTickerText: setTickerText,
             editSegment: editSegment,
             operatorDisplayCount: operatorDisplayCount,
             updateTickerLineSequence: updateTickerLineSequence,
             refreshSortable: refreshSortable,
             createEmptyGroup: createEmptyGroup,
             isRepoContainEmptyTicker: isRepoContainEmptyTicker,
             sendToTwitter: sendToTwitter,
             editBeforeSendToTwitter: editBeforeSendToTwitter,
             setTickerTemplateType: setTickerTemplateType,
             sendCommandToPCR: sendCommandToPCR

         };
     });