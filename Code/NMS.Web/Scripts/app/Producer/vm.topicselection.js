﻿define('vm.topicselection',
    [
        'underscore',
        'ko',
        'enum',
        'datacontext',
        'model',
        'config',
        'manager',
        'vm.contentviewer',
        'messenger',
        'utils',
        'appdata',
        'presenter',
        'router',
        'helper',
        'model.mapper'
    ],
    function (_, ko, e, dc, model, config, manager, contentViewerVM, messenger, utils, appdata, presenter, router, helper, mapper) {
        var
            // Properties
            // ------------------------

            currentNews = ko.observable(new model.News()),
            currentBunch = ko.observable(new model.Bunch()),
            currentGuest = ko.observable(new model.Guest()),
            sourceFilterControl = new model.SourceFilterControl(),
            views = config.viewIds.production,
            hashes = config.hashes.production,
            templates = config.templateNames,
            newsCount = ko.observable(0),
            searchKeywords = ko.observable('').extend({ throttle: config.throttle }),
            searchKeywordsResource = ko.observable('').extend({ throttle: config.throttle }),
            selectedFilters = ko.observableArray([]),
            selectedParentFilterId,
            isContentBucketEnabled = ko.observable(false),
            isContentReportEnable = ko.observable(false),
            isContentDraggingEnabled = ko.observable(false),
            iframeSrc = ko.observable(''),
            isIframeVisible = ko.observable(false),
            searchTags = ko.observableArray([]),
            isAdvanceSearchVisible = ko.observable(false),
            FEResource = ko.observable(''),
            suggestedKeywords = ko.observableArray([]),
            resourceFilter = ko.observableArray([]),
            guestSearchKeywords = ko.observable(),
            archivalResources = ko.observableArray([]).extend({ rateLimit: { timeout: 200, method: "notifyWhenChangesStop" } }),
            GoogleSearchText = ko.observable("Enable Google Search"),
            archivalServerCount = ko.observable(0),
            pageSize = ko.observable(50),
            mediaSearchGoogleList = ko.observableArray([]),
            pageNumber = ko.observable(1),
            isSelectedKeyword = ko.observable(false),
            isIframeDiv = ko.observable(true),
            googleSearchResult = [],
            start = 0,
            videosPageNo = 0,
            isGoogleSearchEnabled = ko.observable(false),
            isFavouriteSearchEnable = ko.observable(false),
            isTelevisionNews = ko.observable(true),
            isTickerNews = ko.observable(false),
            isNewsPaperNews = ko.observable(false),
            isAllSearchEnable = ko.observable(true),
            userFavouriteResources = ko.observableArray([]),
            bufferRequestObject = {},
            noAjaxRequestIsInProgress = true,
            resourceFilterChange = ko.observable(false),
            isMediaView = ko.observable(true),
            currentMetaSearchKeyword = ko.observable(),
            isSetTop = ko.observable(false),

            // Computed Properties
            // ------------------------

            relatedNews = ko.computed({
                read: function () {
                    var arr = [];

                    if (selectedFilters().length > 0 && selectedFilters().indexOf(78) === -1) {
                        for (var i = 0; i < currentBunch().relatedNews().length; i++) {
                            var filterIds = currentBunch().relatedNews()[i].filterIds();
                            for (var j = 0 ; j < selectedFilters().length; j++) {
                                if (filterIds.indexOf(selectedFilters()[j]) !== -1)
                                    arr.push(currentBunch().relatedNews()[i]);
                            }
                        }
                    } else {
                        var newsArray = _.filter(currentBunch().bunchNews(), function (newsObj) {
                            return newsObj.id !== currentNews().id;
                        });
                        arr = newsArray;
                    }

                    newsCount(arr.length);

                    return arr;
                },
                deferEvaluation: true
            }),
            totalGuestCount = ko.computed({
                read: function () {
                    return dc.guests.getObservableList().length;
                },
                deferEvaluation: true
            }),
            internalGuests = ko.computed({
                read: function () {
                    var arr = _.filter(dc.guests.getObservableList(), function (guest) {
                        return guest.guestType === e.GuestType.Internal;
                    });

                    if (guestSearchKeywords() && guestSearchKeywords().length > 0) {
                        var srch = utils.regExEscape(guestSearchKeywords().toLowerCase().trim());
                        arr = _.filter(arr, function (guest) {
                            return guest.name.toLowerCase().search(srch) !== -1;
                        });
                    }

                    return arr;
                },
                deferEvaluation: true
            }),
            externalGuests = ko.computed({
                read: function () {
                    var arr = _.filter(dc.guests.getObservableList(), function (guest) {
                        return guest.guestType === e.GuestType.External;
                    });

                    if (guestSearchKeywords() && guestSearchKeywords().length > 0) {
                        var srch = utils.regExEscape(guestSearchKeywords().toLowerCase().trim());
                        arr = _.filter(arr, function (guest) {
                            return guest.name.toLowerCase().search(srch) !== -1;
                        });
                    }

                    return arr;
                },
                deferEvaluation: true
            }),
            activeSideView = ko.computed({
                read: function () {
                    if (sourceFilterControl.currentFilter() === 'media')
                        return views.topicSelectionMediaView;
                    else if (sourceFilterControl.currentFilter() === 'guest')
                        return views.topicSelectionGuestsView;
                    else
                        return views.topicSelectionNewsFilterView;
                },
                deferEvaluation: true
            }),

            mediaArchivalResources = ko.computed({
                read: function () {
                    var arr = []
                    resourceFilterChange();
                    if (archivalResources() && archivalResources().length > 0) {
                        if (currentBunch().resourceFilter().length > 0) {
                            for (var i = 0, len = archivalResources().length ; i < len; i++) {
                                var item = archivalResources()[i];
                                if (item.type === currentBunch().resourceFilter()[0])
                                    arr.push(item)
                            }
                        }
                        else
                            arr = archivalResources();
                    }
                    arr = arr.sort(function (entity1,entity2) {
                        return new Date(entity2.lastUpdateDate()) - new Date(entity1.lastUpdateDate());
                    });
                    return arr;
                },
                deferEvaluation: true
            }),
            internalGuests = ko.computed({
                read: function () {
                    var arr = _.filter(dc.guests.getObservableList(), function (guest) {
                        return guest.guestType === e.GuestType.Internal;
                    });

                    if (guestSearchKeywords() && guestSearchKeywords().length > 0) {
                        var srch = utils.regExEscape(guestSearchKeywords().toLowerCase().trim());
                        arr = _.filter(arr, function (guest) {
                            return guest.name.toLowerCase().search(srch) !== -1;
                        });
                    }

                    return arr;
                },
                deferEvaluation: true
            }),

            isSelectNewsVisible = ko.computed({
                read: function () {
                    var flag = (appdata.currentUser().userType === e.UserType.TickerProducer || router.currentHash() === "#/topicselection?isiframe");
                    if (flag) {
                        return false;
                    }
                    else
                        return true;
                },
                deferEvaluation: true
            }),

            // Methods
            // ------------------------

            selectGuest = function (data) {
                if (data) {
                    currentGuest(data);
                }
            },
            resetFilters = function () {
                selectedFilters([78]);
            },
            toggleFilter = function (data) {
                var filters = selectedFilters();
                if (data.parentId === -1) {
                    var index = filters.indexOf(selectedParentFilterId);
                    if (index !== -1) {
                        filters[index] = data.id;
                    } else {
                        filters.push(data.id);
                    }
                    selectedParentFilterId = data.id;
                    selectedFilters(filters);
                } else {
                    var index = filters.indexOf(data.id);
                    if (index >= 0) {
                        filters.splice(index, 1);
                    } else {
                        filters.push(data.id);
                    }

                    index = filters.indexOf(data.parentId);

                    if (index !== -1) {
                        filters.splice(index, 1);
                    }

                    selectedFilters(filters);
                }

                if (selectedFilters().length <= 0) {
                    selectedFilters([78]);
                } else if (selectedFilters().length > 1 && selectedFilters()[0] === 78) {
                    selectedFilters.shift();
                }
            },

            setCurrentNews = function (data) {
                if (data) {
                    var requestObj = {
                        bunchId: data.bunchId
                    }
                    $.when(manager.news.getNewsBunchWithCount(requestObj, data.id, data.score))
                      .done(function (responseObj) {
                          if (responseObj && responseObj.Data && responseObj.Data.News && responseObj.Data.News.length > 1) {
                              if (currentNews().bunchTags && currentNews().bunchTags().length > 0) {
                                  searchOnSuggestedKeyword(currentNews().bunchTags()[0].Tag);
                              }
                              else {

                                  searchKeywordsResource('');
                              }
                              sourceFilterControl.currentFilter(-1);
                          }
                          else {
                              sourceFilterControl.currentFilter('');
                              sourceFilterControl.currentFilter('media');
                          }
                      })
                      .fail(function (responseObj) {
                      });
                    currentNews(data);
                    if (currentNews().bunchTags && currentNews().bunchTags().length > 0) {
                        searchOnSuggestedKeyword(currentNews().bunchTags()[0].Tag);
                    }
                    else {

                        searchKeywordsResource('');
                    }
                    console.log(currentNews());
                    var bunch = dc.bunches.getLocalById(currentNews().bunchId);
                    if (bunch) {
                        currentBunch(bunch);
                        sourceFilterControl.currentBunch(bunch);
                        sourceFilterControl.applyBunchFilters(true);
                    }
                }
            },
            searchByTag = function (selectedTag) {
                searchOnSuggestedKeyword(selectedTag);
            },
            setCurrentContent = function (data, options) {
                if (isMediaView()) {
                    if (data && options) {
                        if (options.isArchiveContent) {
                            contentViewerVM.setCurrentContent(archivalResources(), data);
                        } else if (options.isReportedContent) {
                            contentViewerVM.setCurrentContent(currentNews().resources(), data);
                        } else if (options.isRelatedContent) {
                            contentViewerVM.setCurrentContent(currentBunch().relatedNewsResources(), data);
                        }
                        else if (options.isGoogleContent) {
                            var isVideo = (currentBunch().resourceFilter()[0] == e.ContentType.Video || currentBunch().resourceFilter().length == 0) ? true : false;
                            contentViewerVM.setCurrentContentOfGoogle(mediaSearchGoogleList(), data, isVideo);
                        }
                        else if (options.isUserFavouriteResource) {
                            contentViewerVM.setCurrentContent(appdata.userFavourites(), data);
                        }
                    }  
                }
                isMediaView(true);
            },
            selectNews = function () {
                manager.news.selectNews(currentNews());
            },

            toggleResourceFilter = function (data) {
                currentBunch().resourceFilter([]);
                currentBunch().resourceFilter.push(data);
                resourceFilterChange(!resourceFilterChange());
                //  setArchivalResource();
            },
            markResourceAsFavourite = function (data) {
                var arr = _.filter(appdata.userFavourites(), function (item) {
                    return (item.guid === data.guid);
                });
                if (arr && arr.length > 0) {
                    config.logger.error("Already Marked Favourite!");
                }
                else {
                    data.isMarkedFavourite(true);
                    appdata.userFavourites.push(data);
                    userFavouriteResources.push(data);
                    manager.production.addUserFavouriteResource(data);
                }
            },
            deleteResourceFromFavourite = function (data) {
                if (data && data.isMarkedFavourite() && userFavouriteResources().indexOf(data) != 0) {
                    var index = userFavouriteResources().indexOf(data);
                    data.isMarkedFavourite(false);
                    manager.production.deleteUserFavouriteResource(data);
                    userFavouriteResources().remove(userFavouriteResources()[index]);
                    userFavouriteResources.valueHasMutated();
                }
            },
            filteredFavouriteSearch = function () {
                if (searchKeywordsResource().trim().length > 0) {
                    var arr = [];
                    for (var i = 0, len = appdata.userFavourites().length ; i < len; i++) {
                        var item = appdata.userFavourites()[i];
                        if (item.slug && item.slug.toLowerCase().trim().indexOf(searchKeywordsResource().toLowerCase().trim()) != -1) {
                            arr.push(item);
                        }
                    }
                    if (currentBunch().resourceFilter().length > 0) {
                        for (var i = 0, len = arr.length; i < len; i++) {
                            if (arr[i].type !== currentBunch().resourceFilter()[0]) {
                                arr.splice(i, 1);
                            }
                        }
                    }
                    userFavouriteResources(arr);
                }
                else {
                    userFavouriteResources(appdata.userFavourites());
                }
            },
            showArchivalDataForSuggestedkeyword = function (keyword) {
                pageNumber(1);
                var programBucketId = 0;
                var resourceFiterId = currentBunch().resourceFilter().length > 0 ? currentBunch().resourceFilter()[0] : 0;
                var requestObj = getArchivalRequestObject(keyword, pageSize(), pageNumber(), programBucketId, resourceFiterId, isAllSearchEnable());
                getArchivalDataFromServer(requestObj, false, true);
            },
            getGoogleLst = function (value) {
                if (value)
                    showloader(true);
                if (currentBunch().resourceFilter()[0] == e.ContentType.Video || currentBunch().resourceFilter().length == 0) {
                    videosPageNo = videosPageNo + 1;
                    $.ajax({
                        url: config.GoogleVideosApiUrl + value + "&page=" + videosPageNo + "&limit=50",
                        dataType: 'jsonp',
                        success: function (results) {
                            showloader(false);
                            if (results && results.list && results.list.length > 0) {
                                googleSearchResult = results.list;
                                for (var k = 0; k < googleSearchResult.length; k++) {
                                    var resource = googleSearchResult[k];
                                    if (resource.duration) {
                                        var tempDuration = moment.duration(parseInt(resource.duration), 'seconds');
                                        tempDuration = S(tempDuration.hours()).padLeft(2, '0') + ':' + S(tempDuration.minutes()).padLeft(2, '0') + ':' + S(tempDuration.seconds()).padLeft(2, '0');
                                        resource["tbUrl"] = resource.thumbnail_url;
                                        resource["isSelected"] = ko.observable(false),
                                        resource["guid"] = '';
                                        resource.duration = tempDuration;
                                    }
                                    mediaSearchGoogleList.push(resource);
                                }
                            }
                        },
                        error: function () {
                            showloader(false);
                        }
                    });
                }
                else {
                    $.getJSON(config.GoogleImageApiUrl + "&q=" + value + "&start=" + start + "&rsz=8&safe=active&callback=?", function (results) {
                        showloader(false);
                        googleSearchResult.push(results.responseData.results);
                        if (googleSearchResult.length < 7) {
                            start = start + 8;
                            console.log(googleSearchResult);
                            getGoogleLst(value);
                        } else {
                            for (var k = 0; k < googleSearchResult.length; k++) {
                                for (var j = 0; j < googleSearchResult[k].length; j++) {
                                    var resource = googleSearchResult[k][j];
                                    if (resource.duration) {
                                        var tempDuration = moment.duration(parseInt(resource.duration), 'seconds');
                                        tempDuration = S(tempDuration.hours()).padLeft(2, '0') + ':' + S(tempDuration.minutes()).padLeft(2, '0') + ':' + S(tempDuration.seconds()).padLeft(2, '0');
                                        resource.duration = tempDuration;
                                    }
                                    resource["isSelected"] = ko.observable(false),
                                    resource["guid"] = '';
                                    resource["id"] = resource.imageId;
                                    mediaSearchGoogleList.push(resource);
                                }
                            }
                        }
                    });
                }
            },
            getGoogleData = function (value) {
                googleSearchResult = [];
                mediaSearchGoogleList([]);
                start = 0;
                videosPageNo = 0;
                getGoogleLst(value);
            },
            searchOnSuggestedKeyword = function (keyword) {
                showArchivalDataForSuggestedkeyword(keyword);
                searchKeywordsResource(keyword);
                setTimeout(function () {
                    suggestedKeywords([]);
                }, 600);
            },
            loadMoreArchivalData = function () {
                if (appdata.currentUser().userType == e.UserType.Producer || appdata.currentUser().userType == e.UserType.FReporter || appdata.currentUser().userType === e.UserType.HeadlineProducer) {
                    var pageNo = pageNumber() + 1;

                    var term = searchKeywordsResource();

                    var programBucketId = 0;
                    var resourceFiterId = currentBunch().resourceFilter().length > 0 ? currentBunch().resourceFilter()[0] : 0;
                    var requestObj = getArchivalRequestObject(term, pageSize(), pageNo, programBucketId, resourceFiterId, isAllSearchEnable());
                    getArchivalDataFromServer(requestObj, true, false);

                    if (isGoogleSearchEnabled())
                        loadMoreGoogleData();
                }
            },
            loadMoreGoogleData = function () {
                showloader(true);
                presenter.toggleActivity(true);
                if (currentBunch().resourceFilter()[0] == e.ContentType.Video || currentBunch().resourceFilter().length == 0) {
                    googleSearchResult = [];
                    videosPageNo = videosPageNo + 1;
                    $.ajax({
                        url: config.GoogleVideosApiUrl + searchKeywordsResource().trim() + "&page=" + videosPageNo + "&limit=10",
                        dataType: 'jsonp',
                        success: function (results) {
                            if (results && results.list && results.list.length > 0) {
                                googleSearchResult = results.list;
                                for (var k = 0; k < googleSearchResult.length; k++) {
                                    var resource = googleSearchResult[k];
                                    if (resource.duration) {
                                        var tempDuration = moment.duration(parseInt(resource.duration), 'seconds');
                                        tempDuration = S(tempDuration.hours()).padLeft(2, '0') + ':' + S(tempDuration.minutes()).padLeft(2, '0') + ':' + S(tempDuration.seconds()).padLeft(2, '0');
                                        resource["tbUrl"] = resource.thumbnail_url;
                                        resource.duration = tempDuration;
                                    }
                                    mediaSearchGoogleList.push(resource);
                                }
                                var arr = _.uniq(mediaSearchGoogleList());
                                mediaSearchGoogleList(arr);
                                $('#busyindicator').removeClass('margin-indicator');
                                presenter.toggleActivity(false);
                            }
                        },
                        error: function () {
                            showloader(false);
                            presenter.toggleActivity(true);
                        }
                    });
                }
                else {
                    googleSearchResult = [];
                    start = start + 8;
                    $.getJSON(config.GoogleImageApiUrl + "&q=" + searchKeywordsResource().trim() + "&start=" + start + "&rsz=8&safe=active&callback=?", function (results) {
                        if (results && results.responseData && results.responseData.results) {
                            if ((results.responseData.results)) {
                                googleSearchResult.push(results.responseData.results);
                                for (var k = 0; k < googleSearchResult.length; k++) {
                                    for (var j = 0; j < googleSearchResult[k].length; j++) {
                                        var resource = googleSearchResult[k][j];
                                        if (resource.duration) {
                                            var tempDuration = moment.duration(parseInt(resource.duration), 'seconds');
                                            tempDuration = S(tempDuration.hours()).padLeft(2, '0') + ':' + S(tempDuration.minutes()).padLeft(2, '0') + ':' + S(tempDuration.seconds()).padLeft(2, '0');
                                            resource.duration = tempDuration;
                                        }
                                        mediaSearchGoogleList.push(resource);
                                    }
                                }
                                var arr = _.uniq(mediaSearchGoogleList());
                                mediaSearchGoogleList(arr);
                            }
                        }
                    });
                }
            },
            enableGoogleSearch = function () {
                $("#iframeExp").css({ "z-index": "" });
                isGoogleSearchEnabled(true);
                getGoogleData(searchKeywordsResource());
                isAdvanceSearchVisible(false);
                isFavouriteSearchEnable(false);
                isIframeVisible(false);
            },
            enableTeamSearch = function () {
                $("#iframeExp").css({ "z-index": "" });
                isAllSearchEnable(false);
                isGoogleSearchEnabled(false);
                isAdvanceSearchVisible(false);
                isFavouriteSearchEnable(false);
                isIframeVisible(false);
                pageNumber(1);
                var programBucketId = 0;
                var resourceFiterId = currentBunch().resourceFilter().length > 0 ? currentBunch().resourceFilter()[0] : 0;
                var requestObj = getArchivalRequestObject(searchKeywordsResource(), pageSize(), pageNumber(), programBucketId, resourceFiterId, isAllSearchEnable());
                getArchivalDataFromServer(requestObj, false, true);
            },
            enableAdvanceSearch = function () {
                $("#iframeExp").css({ "z-index": "" });
                isAllSearchEnable(false);
                isGoogleSearchEnabled(false);
                isFavouriteSearchEnable(false);
                isAdvanceSearchVisible(true);
                isIframeVisible(false);
                pageNumber(1);
                var programBucketId = 0;
                var resourceFiterId = currentBunch().resourceFilter().length > 0 ? currentBunch().resourceFilter()[0] : 0;
                var requestObj = getArchivalRequestObject(searchKeywordsResource(), pageSize(), pageNumber(), programBucketId, resourceFiterId, isAllSearchEnable());
                getArchivalDataFromServer(requestObj, false, true);
            },
            enableUserFavouriteSearch = function () {
                $("#iframeExp").css({ "z-index": "" });
                if (router.currentHash() == "#/topicselection?isiframe") {
                    isAllSearchEnable(false);
                    isGoogleSearchEnabled(false);
                    isIframeVisible(false);
                    isFavouriteSearchEnable(true);
                    $.ajax({
                        url: '/api/News/GetUserFavourites?UserId=' + appdata.currentUser().id,
                        type: "GET",
                    }).done(function (data) {
                        if (data != null && data.length > 0) {
                            var tempResources = [];
                            for (var i = 0; i < data.length; i++) {
                                var item = mapper.resource.fromDto(data[i]);
                                item.isMarkedFavourite(true);
                                tempResources.push(item);
                            }
                            tempResources.reverse();
                            appdata.userFavourites([]);
                            appdata.userFavourites(tempResources);
                            isFavouriteSearchEnable(true);
                            filteredFavouriteSearch();
                        }
                    }).fail(function (jqXHR, textStatus) {

                    });
                }
                else {
                    isAllSearchEnable(false);
                    isGoogleSearchEnabled(false);
                    isIframeVisible(false);
                    isFavouriteSearchEnable(true);
                    filteredFavouriteSearch();
                }
            },
            enableAllSearch = function () {
                $("#iframeExp").css({ "z-index": "" });
                isAllSearchEnable(true);
                isGoogleSearchEnabled(false);
                isFavouriteSearchEnable(false);
                isAdvanceSearchVisible(false);
                isIframeVisible(false);
                pageNumber(1);
                var programBucketId = 0;
                var resourceFiterId = currentBunch().resourceFilter().length > 0 ? currentBunch().resourceFilter()[0] : 0;
                var requestObj = getArchivalRequestObject(searchKeywordsResource(), pageSize(), pageNumber(), programBucketId, resourceFiterId, isAllSearchEnable());
                getArchivalDataFromServer(requestObj, false, true);
            },
            showloader = function (flag) {
                if (flag) {
                    $('.archiveSearch').show();
                }
                else {
                    $('.archiveSearch').hide();
                }

            },
            getCelebrity = function (term, pageSize, pageNumber) {
                if (term && term.length > 0) {
                    showloader(true);
                    var requestObj = { term: term, pageSize: pageSize, pageNumber: pageNumber };
                    $.when(manager.production.searchCelebrity(requestObj))
                   .done(function (responseObj) {
                       var obj = responseObj.Data;
                       suggestedKeywords(responseObj.Data);
                       showloader(false);
                   })
                   .fail(function (responseObj) {
                   });
                }
            },
            isNotSameRequest = function (newRequest, bufferedRequest) {
                if (!bufferedRequest)
                    return true;
                return JSON.stringify(newRequest) !== JSON.stringify(bufferedRequest)
            },

            subscribeEvents = function () {
                searchKeywords.subscribe(function (value) {
                    guestSearchKeywords(value);
                });

                currentMetaSearchKeyword.subscribe(function (value) {
                    noAjaxRequestIsInProgress = true;
                    pageNumber(1);
                    if (value) {
                        var programBucketId = 0;
                        var resourceFiterId = currentBunch().resourceFilter().length > 0 ? currentBunch().resourceFilter()[0] : 0;

                        var requestObj = getArchivalRequestObject(searchKeywordsResource(), pageSize(), pageNumber(), programBucketId, resourceFiterId, isAllSearchEnable());
                        getArchivalDataFromServer(requestObj, false, false);
                    }
                    else {
                        bufferRequestObject = {};
                    }
                });

                searchKeywordsResource.subscribe(function (value) {

                    pageNumber(1);
                    noAjaxRequestIsInProgress = true;
                    getCelebrity(value, 10, pageNumber());
                    var programBucketId = 0;
                    var resourceFiterId = currentBunch().resourceFilter().length > 0 ? currentBunch().resourceFilter()[0] : 0;

                    if (isFavouriteSearchEnable()) {
                        filteredFavouriteSearch();
                    }
                    else if (isGoogleSearchEnabled()) {
                        getGoogleData(value);
                    }
                    else {

                        var requestObj = getArchivalRequestObject(value, pageSize(), pageNumber(), programBucketId, resourceFiterId, isAllSearchEnable());
                        getArchivalDataFromServer(requestObj, false, false);
                    }
                    if (!value.trim()) {
                        archivalServerCount(0);
                        archivalResources([]);
                        mediaSearchGoogleList([]);
                        googleSearchResult = [];
                        suggestedKeywords([]);
                        bufferRequestObject = {};
                        start = 0;
                        getResourceByDate(null, false, false);
                    }
                });

                resourceFilterChange.subscribe(function (value) {
                    noAjaxRequestIsInProgress = true;
                    pageNumber(1);
                    if (isFavouriteSearchEnable()) {
                        filteredFavouriteSearch();
                    }
                    else if (isGoogleSearchEnabled()) {
                        getGoogleData(searchKeywordsResource());
                    }
                    else if(searchKeywordsResource && searchKeywordsResource().length > 0)  {
                        var programBucketId = 0;
                        var resourceFiterId = currentBunch().resourceFilter().length > 0 ? currentBunch().resourceFilter()[0] : 0;
                        var requestObj = getArchivalRequestObject(searchKeywordsResource(), pageSize(), pageNumber(), programBucketId, resourceFiterId, isAllSearchEnable());
                        getArchivalDataFromServer(requestObj, false, false);
                    }
                    showloader(false);
                });
            },
            SwitchNewsTab = function (data) {
                isTelevisionNews(false);
                isTickerNews(false);
                isNewsPaperNews(false);
                if (data == e.NewsFilterType.TelevisionNews) {
                    isTelevisionNews(true);
                }
                if (data == e.NewsFilterType.NewsPaperNews) {
                    isNewsPaperNews(true);
                }
                if (data == e.NewsFilterType.TickerNews) {
                    isTickerNews(true);
                }
            },

            // archival server hits 
            getArchivalRequestObject = function (term, pageSize, pageNumber, bucketId, resourceTypeId, alldata) {

                var tempBucketId;

                if (appdata.currentEpisode && appdata.currentEpisode().id) {
                    tempBucketId = dc.programs.getLocalById(appdata.currentEpisode().programId).bucketId;
                }
                else if (!appdata.currentEpisode) {
                    tempBucketId = dc.programs.getAllLocal()[0];
                }
                else
                    tempBucketId = 0;

                var requestObject = {
                    term: term ? term.trim() : ' ',
                    pageSize: pageSize,
                    pageNumber: pageNumber,
                    bucketId: tempBucketId,
                    resourceTypeId: resourceTypeId,
                    alldata: alldata
                };
                return requestObject;
            },

            getArchivalDataFromServer = function (requestObj, isFromGetMore, emptyExisting) {


                showloader(true);
                presenter.toggleActivity(true);
                if (isNotSameRequest(requestObj, bufferRequestObject) && noAjaxRequestIsInProgress) {
                    noAjaxRequestIsInProgress = false;
                    $.when(manager.production.searchContentArchive(requestObj, archivalServerCount, searchTags(), isAdvanceSearchVisible()))
                        .done(function (responseObj) {

                            noAjaxRequestIsInProgress = true;
                            var temparr = responseObj;

                            if (!isFromGetMore)
                                archivalResources([]);



                            if (isFromGetMore) {
                                for (var i = 0; i < responseObj.length; i++) {
                                    var resource = responseObj[i];
                                    archivalResources.push(resource);

                                }
                                pageNumber(requestObj.pageNumber);
                            }
                            else
                                archivalResources(temparr);

                            showloader(false);
                            presenter.toggleActivity(false);
                            bufferRequestObject = requestObj;

                        })
                        .fail(function (responseObj) {
                            showloader(false);
                            presenter.toggleActivity(false);
                        });
                }
                else {
                    showloader(false);
                    presenter.toggleActivity(false);
                }

            },
            isProducerMediaArchival = ko.computed({
                read: function () {
                    return router.currentHash().indexOf(hashes.producerArchivalMedia) !== -1
                },

                deferEvaluation: true

            }),
            fillLatestResourceData = ko.computed({
                read: function () {
                    if (isProducerMediaArchival() && !searchKeywordsResource()) {
                        var arr = _.filter(dc.resources.getObservableList(), function (item) {
                            return item.isLatest == true;
                        });

                        if (arr && arr.length > 0) {
                            archivalResources(arr);
                            archivalServerCount(archivalResources().length);
                           
                        }
                    }
                }
            }),
            getResourceByDate = function (requestObj, isFromGetMore, emptyExisting) {
               searchKeywordsResource('');
                var requestObject = {
                    ResourceDate: moment().add(-1, 'days').toISOString(),
                };
                showloader(true);
                presenter.toggleActivity(true);
                $.when(manager.production.getResourceByDate(requestObject, archivalServerCount, searchTags(), isAdvanceSearchVisible(), showloader))
                    .done(function (responseObj) {
                        showloader(false);
                        toggleResourceFilter(e.ContentType.Video);
                        presenter.toggleActivity(false);
                    })
                    .fail(function (responseObj) {
                        showloader(false);
                        presenter.toggleActivity(false);
                    });

            },
            openFileExplorer = function () {
                
                $("#iframeExp").css({ "z-index": "9999" });
                  if (router.currentHash() == "#/topicselection?isiframe") {
                      if ($("#iframeExp").hasClass('topDecreasing')) {
                      }
                      else {
                          $("#iframeExp").addClass('topDecreasing');

                      }

                  } iframeSrc('http://10.3.12.119/FileManagementSystem.html?name=' + helper.getCookie('EncriptedUserId'));
                  isIframeVisible(true);
                  isSetTop(true);
                  isAllSearchEnable(false);
                  isGoogleSearchEnabled(false);
                  isAdvanceSearchVisible(false);
                  isFavouriteSearchEnable(false);
              },
              addToReportNews = function (data) {
                  isMediaView(false);
                  if (data.isSelected()) {
                      data.isSelected(false);
                  }
                  else {                     
                      data.isSelected(true);
                  }
                  window.parent.postMessage(JSON.stringify(data), "*");
                 
              },
          
            // ends archival server hits  

            activate = function (routeData, callback) {
                var url = window.location.href;
                if (url.indexOf("isiframe") != -1) {
                    setTimeout(function () {
                        $('.asideExplosives').hide();
                        $('#shell-right-view').hide();
                        $('#shell-top-nav-view').hide();
                        $('#checkedIcon').hide();
                        $('#homeIcon').hide();
                        $('#homeIcon').remove();
                        $('.proceedBtn').hide();
                        $('.reportedSec').addClass('reportedSecWidth');
                        $('.FileExplorerFilters').hide();
                        $("#mediaFilter").click();
                        isContentReportEnable(true);
                        //  $('.folderIcon').hide();
                        $('#topicselection-filters-view').hide();
                        $("#mediaFilter").click();
                        $('.topic-content').addClass('setIframeCss');
                        sourceFilterControl.currentFilter(-1);
                        sourceFilterControl.currentFilter("media");
                        $("#popupIframeFExp").addClass('popupIframeFExp');
                        // isSelectNewsVisible(false);
                        $("#popupIframeFExp").contents().find('body').addClass('popupIframeFExp');
                        $("#childIframe1").contents().find('body').addClass('childIframe');
                        
                        // isSelectNewsVisible(false);
                        isSetTop(true);
                    }, 3000);
                    setTimeout(function () {
                        $("#mediaFilter").click();
                        sourceFilterControl.currentFilter(-1);
                        sourceFilterControl.currentFilter("media");
                        sourceFilterControl.isMediaSectionVisible(true);
                    }, 6000);
                }
                else {
                    isIframeDiv(false);
                    //if (appdata.lastRoute() != config.hashes.production.topicSelection) {
                    //    setTimeout(function () {
                    //        router.navigateTo(config.hashes.production.home);
                    //    }, 300);
                    //}
                }
               

                messenger.publish.viewModelActivated({ canleaveCallback: canLeave });
            },

            canLeave = function () {
                archivalServerCount(0);
                archivalResources([]);
                suggestedKeywords([]);
                isAdvanceSearchVisible(false);
                return true;
            },
            init = function () {
                if (router.currentHash().indexOf(hashes.producerArchivalMedia) !== -1) {
                    sourceFilterControl.currentFilter('media');
                    
                }

                sourceFilterControl.currentFilter(-1);
                sourceFilterControl.isMediaSectionVisible(true);
                sourceFilterControl.isGuestSectionVisible(false);
                subscribeEvents();
                appdata.selectPopUpResource.subscribe(function (data) {
                    if (data && router.currentHash() === config.hashes.production.topicSelection) {
                        addToContentBucket(data);
                    }
                });
                appdata.selectedFavouriteResource.subscribe(function (data) {
                    if (data && router.currentHash() === config.hashes.production.topicSelection || router.currentHash() === config.hashes.production.producerArchivalMedia) {
                        markResourceAsFavourite(data);
                        config.logger.success("Mark Favourite Successfully!");
                    }

                });
                appdata.isDefaultTab.subscribe(function (value) {
                    sourceFilterControl.currentFilter("media");
                    $("#mediaFilter").click();
                    isIframeDiv(true);
                    // $('.folderIcon').show();
                });

                appdata.unmarkSelectedResource.subscribe(function (data) {
                    if (data && router.currentHash() === config.hashes.production.topicSelection) {
                        deleteResourceFromFavourite(data);
                        config.logger.success("UnMark Successfully!");
                    }

                });

                var url = window.location.href;
                if (url.indexOf("isiframe") != -1) {
                    setTimeout(function () {
                        $("#mediaFilter").click();
                        sourceFilterControl.currentFilter(-1);
                        sourceFilterControl.currentFilter("media");
                        sourceFilterControl.isMediaSectionVisible(true);
                    }, 6000);
                }
            };


        return {
            activate: activate,
            canLeave: canLeave,
            setCurrentNews: setCurrentNews,
            currentNews: currentNews,
            currentBunch: currentBunch,
            activeSideView: activeSideView,
            views: views,
            searchKeywords: searchKeywords,
            internalGuests: internalGuests,
            externalGuests: externalGuests,
            totalGuestCount: totalGuestCount,
            sourceFilterControl: sourceFilterControl,
            toggleFilter: toggleFilter,
            init: init,
            templates: templates,
            hashes: hashes,
            relatedNews: relatedNews,
            newsCount: newsCount,
            resetFilters: resetFilters,
            selectNews: selectNews,
            setCurrentContent: setCurrentContent,
            isContentBucketEnabled: isContentBucketEnabled,
            isContentDraggingEnabled: isContentDraggingEnabled,
            currentGuest: currentGuest,
            selectGuest: selectGuest,
            toggleResourceFilter: toggleResourceFilter,
            searchKeywordsResource: searchKeywordsResource,
            archivalResources: archivalResources,
            mediaArchivalResources: mediaArchivalResources,
            loadMoreArchivalData: loadMoreArchivalData,
            archivalServerCount: archivalServerCount,
            utils: utils,
            isSelectNewsVisible: isSelectNewsVisible,
            e: e,
            suggestedKeywords: suggestedKeywords,
            showArchivalDataForSuggestedkeyword: showArchivalDataForSuggestedkeyword,
            searchByTag: searchByTag,
            searchOnSuggestedKeyword: searchOnSuggestedKeyword,
            mediaSearchGoogleList: mediaSearchGoogleList,
            isGoogleSearchEnabled: isGoogleSearchEnabled,
            GoogleSearchText: GoogleSearchText,
            router: router,
            appdata: appdata,
            enableGoogleSearch: enableGoogleSearch,
            enableTeamSearch: enableTeamSearch,
            enableAllSearch: enableAllSearch,
            enableUserFavouriteSearch: enableUserFavouriteSearch,
            isAllSearchEnable: isAllSearchEnable,
            userFavouriteResources: userFavouriteResources,
            isFavouriteSearchEnable: isFavouriteSearchEnable,
            searchTags: searchTags,
            isAdvanceSearchVisible: isAdvanceSearchVisible,
            enableAdvanceSearch: enableAdvanceSearch,
            currentMetaSearchKeyword: currentMetaSearchKeyword,
            SwitchNewsTab: SwitchNewsTab,
            isTelevisionNews: isTelevisionNews,
            isNewsPaperNews: isNewsPaperNews,
            isTickerNews: isTickerNews,
            config: config,
            openFileExplorer: openFileExplorer,
            iframeSrc: iframeSrc,
            isIframeVisible: isIframeVisible,
            FEResource: FEResource,
            isContentReportEnable: isContentReportEnable,
            addToReportNews: addToReportNews,
            isMediaView: isMediaView,
            isIframeDiv: isIframeDiv,
            isProducerMediaArchival: isProducerMediaArchival,
            isSetTop: isSetTop,
            getResourceByDate: getResourceByDate
        };
    });