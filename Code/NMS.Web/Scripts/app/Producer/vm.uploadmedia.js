﻿define('vm.uploadmedia',
    ['control.tag', 'control.uploader', 'appdata', 'datacontext', 'config', 'enum', 'router', 'manager'],
    function (Tag, Uploader, appdata, dc, config, e, router, manager) {

        var
            // Properties
            // ------------------------
            programs = ko.observableArray(),
            slug = ko.observable(),
            selectedProgram = ko.observable(),
            tags = ko.observable(new Tag('/api/tag/gettagbyterm/')),
            uploader = ko.observable(new Uploader()),

            // Computed Properties
            // ------------------------
            getPrograms = ko.computed({
                read: function () {
                    programs(dc.programs.getObservableList());
                    if (programs().length > 0)
                        selectedProgram(programs()[0]);
                },
                deferEvaluation: false
            }),

            // Methods
            // ------------------------
            uploadMedia = function () {
                $('#divUploader').children('form').children('input').get(0).click();
            },

            nleUploadResouces = function () {

            },

            submitMedia = function () {
                if (!slug || !slug() || !slug().trim()) {
                    config.logger.error('Enter Slug');
                }
                else if (!selectedProgram || !selectedProgram()) {
                    config.logger.error('Select Program');
                }
                else if (!tags || !tags() || tags().tags() <= 0) {
                    config.logger.error('No Media Selected');
                }
                else if (!uploader || !uploader().resources || uploader().resources() <= 0) {
                    config.logger.error('No Media Selected');
                }
                else {
                    var guids = '';
                    for (var i = 0; i < uploader().resources().length; i++) {
                        guids = i == 0 ? uploader().resources()[i].Guid : guids + ',' + uploader().resources()[i].Guid;
                    }
                    var metatags = '';
                    for (var j = 0; j < tags().tags().length; j++) {
                        metatags = i == 0 ? tags().tags()[j].trim() : metatags + ',' + tags().tags()[j].trim();
                    }

                    var reqObj = {
                        ListGuids: guids,
                        Slug: slug(),
                        MetaTags: metatags,
                        ProgramId: selectedProgram().id,
                        BucketId: selectedProgram().bucketId,
                        ApiKey: selectedProgram().apiKey
                    }
                    $.when(manager.production.updateResource(reqObj))
                        .done(function (responseData) {
                            if (responseData.IsSuccess) {
                                config.logger.success('Successfully Uploaded');
                                clearForm();
                            } else {
                                config.logger.error('Error Occured');
                            }
                        })
                        .fail(function () {
                            config.logger.error('Error Occured');
                        });

                }
            },

            clearForm = function () {
                slug('');
                selectedProgram(null);
                tags().tags([]);
                uploader().started(false);
                uploader().finished(false);
                uploader().src('');
                uploader().resources([]);
                uploader().uploadedResources([]);
            },

            deleteThumbnail = function (a, b) {
                if (b) {
                    var id = b.currentTarget.id;
                    removeResourcesFromUploader(id);
                    $.when(manager.news.deletevideofile(id))
                      .done(function (responseData) {
                      })
                      .fail(function () {
                      });

                }
            },

            removeResourcesFromUploader = function (id) {
                var list = uploader().uploadedResources();
                var resources = uploader().resources();
                for (var i = 0; i < list.length; i++) {
                    if (list[i].Guid == id) {
                        list.remove(list[i]);
                        for (var j = 0; j < resources.length; j++) {
                            if (resources[j].Guid == id) {
                                resources.remove(resources[j]);
                            }
                        }
                    }

                }
                uploader().uploadedResources(list);
            },

            activate = function (routeData, callback) {

            },

            canLeave = function () {
                return true;
            };

        return {
            activate: activate,
            canLeave: canLeave,
            appdata: appdata,
            e: e,
            config: config,
            router: router,
            slug: slug,
            tags: tags,
            programs: programs,
            selectedProgram: selectedProgram,
            uploader: uploader,
            uploadMedia: uploadMedia,
            nleUploadResouces: nleUploadResouces,
            submitMedia: submitMedia,
            deleteThumbnail: deleteThumbnail
        };
    });