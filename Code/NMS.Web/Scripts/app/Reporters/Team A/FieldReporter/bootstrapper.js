﻿define('bootstrapper',
    [
        'config',
        'binder',
        'route-config',
        'manager',
        'timer',
        'reporter.appdata',
        'datacontext',
        'model.reportnews',
        'enum'
    ],
    function (config, binder, routeConfig, manager, timer, appdata, dc, modelReportnews, e) {

        var
            run = function () {

                var views = config.views.fieldreporter;

                appdata.views = views;
                appdata.listHeader = 'My News';
                config.dataserviceInit();
                appdata.extractUserInformation();
                binder.bindStartUpEvents(views);
               
                routeConfig.register(views);

                $.when(manager.news.reporterLoadInitialData())
                    .done(function (responseData) {
                    
                        binder.bindPreLoginViews(views);
                        timer.start();
                    })
                     .fail(function (responseData) {

                     });



                $.when(manager.categories.loadCategories())
                   .done(function (responseData) {
                       $.when(manager.locations.loadLocations())
                            .done(function (responseData) {

                            })
                             .fail(function (responseData) {

             });

                    })
                    .fail(function (responseData) {

 });

                $.when(manager.usermanagement.getUsersByWorkRoleId(e.UserType.CameraMan))
                           .done(function (responseData) {
                               dc.users.fillData(responseData.Data);
                               appdata.isCameramanFill(true);
                           })
                           .fail(function () {
                               config.logger.error("No Data Fetch");
                           });

                amplify.subscribe(config.eventIds.onLogIn, function (data) {

                });
            };

        return {
            run: run
        };
    });
