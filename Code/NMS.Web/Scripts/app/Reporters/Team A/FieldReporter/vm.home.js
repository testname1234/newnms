﻿define('vm.home',
    ['router','config'],
    function (router,config) {

        var
            // Properties
            // ------------------------


            // Computed Properties
            // ------------------------


            // Methods
            // ------------------------
          
            activate = function (routeData, callback) {
                router.navigateTo(config.views.fieldreporter.reportNews.url);
            },
            canLeave = function () {
                return true;
            };

        return {
            activate: activate,
            canLeave: canLeave,
           
        };
    });