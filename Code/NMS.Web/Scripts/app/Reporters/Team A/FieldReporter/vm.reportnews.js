﻿define('vm.reportnews',
    ['config', 'enum', 'model.reportnews', 'model', 'reporter.appdata', 'router'],
    function (config, e, ReportNews, parentModel, appdata, router) {

        var model = new ReportNews('urduRN', 'englishRN', ['.leftPanel .editor input[type=text]', '.leftPanel .editor .jqte_editor', '.titleStyle', '.reportNewsEditor'], e.NewsType.Story, 'Report News', null, e.NewsFilterType.FieldReporter),

            tickerReporterControl = new parentModel.TickerReporterControl(),

            submitNews = function () {
                model.tickerLines(tickerReporterControl.tickerLineList());
                model.currentTicker(tickerReporterControl.currentTicker());
                if (tickerReporterControl.lastTicker && tickerReporterControl.lastTicker())
                {
                    tickerReporterControl.tickerLineList.push({ Text: tickerReporterControl.lastTicker(), LanguageCode: "ur", SequenceId: tickerReporterControl.tickerLineList().length == 0 ? 1 : tickerReporterControl.tickerLineList().length + 1 });
                }
                model.submit();
                tickerReporterControl.tickerLineList([])
            },

           


        activate = function (routeData, callback) {

        };

        return {
            activate: activate,
            config: config,
            e: e,
            submitNews: submitNews,
            model: model,
            tickerReporterControl: tickerReporterControl,
            appdata: appdata,
          
            router: router
        };
    });