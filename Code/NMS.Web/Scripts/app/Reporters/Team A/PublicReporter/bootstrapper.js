﻿define('bootstrapper',
    [
        'config',
        'binder',
        'route-config',
        'manager',
        'timer',
        'reporter.appdata'
    ],
    function (config, binder, routeConfig, manager, timer,appdata) {

        var
            run = function () {

                var views = config.views.publicreporter;

                appdata.views = views;
                appdata.listHeader = 'My News';
                config.dataserviceInit();

                binder.bindStartUpEvents(views);
                routeConfig.register(views);

                $.when(manager.news.reporterLoadInitialData())
                    .done(function (responseData) {
                        binder.bindPreLoginViews(views);
                        timer.start();
                    })
                     .fail(function (responseData) {

                     });

                $.when(manager.categories.loadCategories())
   .done(function (responseData) {
       $.when(manager.locations.loadLocations())
            .done(function (responseData) {

            })
             .fail(function (responseData) {

             });

   })
 .fail(function (responseData) {

 });


                amplify.subscribe(config.eventIds.onLogIn, function (data) {

                });
            };

        return {
            run: run
        };
    });
