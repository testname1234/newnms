﻿define('vm',
    [
        'vm.shell',
        'vm.submitpackage',
        'vm.home',
        'vm.mynews',
        'vm.report',
        'vm.updatenews'
    ],

    function (shell, publicreportNews, home, mynews, reports, updatenews)
    {
        var vmDictionary = {};

        vmDictionary['#home-view'] = home;
        vmDictionary['#submitpackage-view'] = publicreportNews;
        vmDictionary['.myNewsPanelHeader'] = mynews;
        vmDictionary['#reports-view'] = reports;
        vmDictionary['#shell-top-nav-view'] = shell;
        vmDictionary['.updateNewsLeftRight'] = updatenews;
        vmDictionary['#shell-top'] = shell;

        return {
            dictionary: vmDictionary
        };
    });