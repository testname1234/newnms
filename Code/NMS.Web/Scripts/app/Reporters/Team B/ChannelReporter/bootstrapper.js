﻿define('bootstrapper',
    [
        'config',
        'binder',
        'route-config',
        'manager',
        'timer',
        'reporter.appdata',
        'appdata',
        'enum',
        'vm',
        'ko'
    ],
    function (config, binder, routeConfig, manager, timer, appdata, commonAppdata, e, vm, ko) {

        var
            run = function () {

                var views = config.views.channelreporter;

                appdata.views = views;

                appdata.listHeader = 'My News';
                config.dataserviceInit();
                appdata.extractUserInformation();
                commonAppdata.extractUserInformation();

                if (appdata.currentUser().userType == e.UserType.Creporter) {
                    config.roleid = appdata.currentUser().userType;
                } else {
                    window.location.href = 'login#/index';
                }

                binder.bindStartUpEvents(views);
                routeConfig.register(views);
                var data = { id: appdata.lastUpdateTime() };


                $.when(manager.news.reporterLoadInitialData(true))
                    .done(function (responseData) {
                                    $.when(manager.channels.channelReporterloadInitialData(data))
                                        .done(function (responseData) {
                                            ko.applyBindings(vm.dictionary['#content-viewer-main'], $('#content-viewer-main')[0]);
                                            binder.bindPreLoginViews(views);
                                            //
                                           
                                            //timer.start();
                                        })
                                         .fail(function (responseData) {

                                         });
                 })
                    .fail(function (responseData) {

     });

                $.when(manager.categories.loadCategories())
                    .done(function (responseData) {
                        $.when(manager.locations.loadLocations())
                            .done(function (responseData) {

                            })
                             .fail(function (responseData) {

                             });
                    })
                     .fail(function (responseData) {

                     });

                var obj = { rid: appdata.userId, pageCount: 20, startIndex: 0 };
                $.when(manager.news.LoadMyNewsList(obj))
                .done(function (responseData) {
                })


                amplify.subscribe(config.eventIds.onLogIn, function (data) {

                });
            };

        return {
            run: run
        };
    });