﻿define('vm.reportnews',
    ['config',
     'manager',
     'control.tag',
     'control.multiselect',
     'control.editorcontrol',
     'datacontext',
     'enum',
     'extensions',
     'moment',
     'control.templatepaging',
     'router',
     'model.reportnews',
     'messenger',
     'reporter.appdata'
    ],
    function (config, manager, Tag, MultiSelect, EditorControl, dc, e, ext, moment, TemplatePaging, router, ReportNews, messenger, appdata) {

        var model = new ReportNews(
            'urduRN',
            'englishRN',
            ['.reportNewsChannelReporter .editingSection .editor input[type=text]', '.urduFontFamily', '.textboxHighLight'],
            e.NewsType.Story,
            'Report News',
            e.ContentType.Video,
            e.NewsFilterType.Channel,
            null,
            true,
            'No clip selected yet.Mark the video area to select your desire portion of Program for the news.'
            ),


        seekval = 200,
        seekchangeVal = 0.5,
        fastseekchangeVal = 5,
        v = $("#videoplayer").get(0),
        videoUrl = ko.observable(model.mediaEditor.src()),
        isPlaying = ko.observable(false),
        videoTime = ko.observable(),
        VDonlyerify = ko.observable(false),
        VDcampaigns = ko.observableArray([]),
        VDAllCampaigns = ko.observableArray([]),
        VDselectetdCampaign = ko.observable(null),
        VDselectedBrand = ko.observable(),
        VDallBrands = ko.observable(),
        forwardOption = ko.observable(),
        Starting = ko.observable(),
        listFromTos = ko.observableArray([]),
        From = ko.observable(0),
        TOs = ko.observable(0),
        Ending = ko.observable(),
        option1 = ko.observable("false"),
        StartTime = ko.observable('--:--'),
        EndTime = ko.observable('--:--'),
        browserCss = ko.observable('chromeVPlayer'),
        lastScreen = ko.observable(config.views.channelreporter.home.url),
        vlcTime = ko.observable(0),

       // videoData = ko.observable({ SlotName: "", Title: "", ChannelName: "", CampaignTitle: "", Start: "", End: "", StartDate: "" }),
        VDslots = ko.observableArray([]),

        markTurn = ko.observable(0),

        prevPage = function () {
            ResetMarking();
            router.navigateTo(lastScreen());
        },


        submitNews = function () {

            //if (listFromTos().length > 0 && listFromTos()[0].From > 0 && listFromTos()[0].To > 0) {
            model.channelId = model.templatePaging.currentItem().channelId();
            var PId = model.templatePaging.currentItem().programId();
            var channName = _dc.channels.getAllLocalByIds([model.channelId])[0].name;
            if (listFromTos().length > 0) {
                model.resourceEdit = { ResourceTypeId: e.ContentType.Video, Id: model.templatePaging.currentItem().id, FileName: model.templatePaging.currentItem().fileName(), FromTos: listFromTos(), ProgramId: PId, Url: videoUrl() };
            }

            model.submit(ResetAfterSubmit)
            //}
            //    else
            //        config.logger.error("Please Mark Start and End Time");
        },

        ResetAfterSubmit = function () {
            ResetMarking();
            listFromTos([]);
            //config.logger.success("News Reported");
        },

        forwa = function (time) {
            if (option1() == "true")
                forward1();
            else
                forward2(time);
        },

        forward1 = function () {
            $('#videoplayer').get(0).play();
            isPlaying(true);
            //if (From() == 0) {
            //    From($('#videoplayer').get(0).currentTime);
            //}

            setTimeout(function () {
                $('#videoplayer').get(0).pause();
                makeTime($('#videoplayer').get(0).currentTime);
            }, 50);
        },

        forward2 = function (time) {

            $('#videoplayer').get(0).currentTime = $('#videoplayer').get(0).currentTime + time;
            makeTime($('#videoplayer').get(0).currentTime);
            //if (From() == 0) {
            //    From($('#videoplayer').get(0).currentTime);
            //}
            //if (TOs() == 0) {
            //    TOs($('#videoplayer').get(0).currentTime);
            //}
        },

        backwa = function (time) {
            $('#videoplayer').get(0).currentTime = $('#videoplayer').get(0).currentTime - time;
            makeTime($('#videoplayer').get(0).currentTime);
        },

        makeTime = function (time) {
            var t = time;
            videoTime(toHHmmssff(t));
        },

        toHHmmssff = function (t) {
            //return parseInt(t / 86400) + (new Date(t % 86400 * 1000)).toUTCString().replace(/.*(\d{2}):(\d{2}):(\d{2}).*/, "$1:$2:$3") + " " + parseFloat(t % 1).toFixed(2);
            return (new Date(t % 86400 * 1000)).toUTCString().replace(/.*(\d{2}):(\d{2}):(\d{2}).*/, "$2:$3");
        },

        getSeconds = function (time) {
            if (time) {
                tt = time.split(":");
                sec = tt[0] * 3600 + tt[1] * 60 + tt[2] * 1;
                return sec;
            }
        },

         markBothTimes = function () {
             $('.marker').remove();
             markStartPointer();
             markEndPointer();
         },

        markStartPointer = function () {
            $("#startPoint").remove();
            var val = From();// getSeconds(Starting());
            creatPointer(From(), "startPoint");
        },

        markEndPointer = function () {
            $("#endPoint").remove();
            var val = TOs();//getSeconds(Ending());
            creatPointer(val, "endPoint");
        },

        creatPointer = function (pointOfTime, id) {
            var width = $("#pointerArea").width();
            var time = $('#videoplayer').get(0).duration;

            if (isNaN(time)) {
                time = 3600;
            }

            var percent = width / time;
            var left = parseInt(percent * pointOfTime);
            var idname = id;
            $("#pointerArea").append("<div class='marker' id='" + idname + "' style='width:auto;position:absolute;margin-left:" + left + "px;cursor:pointer;margin-top:-25px;'><div class='arrow-up'></div></div>");
        },

        clearVideoData = function () {
            $('.marker').remove();
            $("#startPoint").remove();
            $("#endPoint").remove();
            markTurn(0);
            Starting("");
            Ending("");
        },


        ChangeMarkPos = function () {
            var t = 0;
            if (markTurn() == 0) {
                markTurn(1);
                t = getSeconds(Ending());
            } else if (markTurn() == 1) {
                t = getSeconds(Starting());
                markTurn(0);
            }
            if (t != 0) {
                if (t) {
                    $('#videoplayer').get(0).currentTime = t - getSeconds(Starting());
                    makeTime($('#videoplayer').get(0).currentTime);
                }
            }

        },

        preciseCurrentTime = function () {
            var t;
            t = videoTime().substr(1, 8) + videoTime().substr(11, 3);
            return t;
        },

        enterMark = function () {
            if (markTurn() == 0) {
                Starting(preciseCurrentTime());
                markStartPointer();
                if (TOs() != 0) {
                    markEndPointer();
                }
            }
            else if (markTurn() == 1) {
                Ending(preciseCurrentTime());
                markStartPointer()
                if (TOs() != 0) {
                    markEndPointer();
                }
                setTimeout(function () {
                    From(0);
                    TOs(0);
                }, 2000);
            }
            listFromTos([]);
            var tempFrom = From();
            var tempTos = TOs();
            if (tempFrom > tempTos)
            { listFromTos.push({ From: TOs(), To: From(), Left: '', width: '' }); }
            else
            { listFromTos.push({ From: From(), To: TOs(), Left: '', width: '' }); }
        },

        //getObj = function () {
        //    var obj = {
        //        AddedToRundownCount: 0,
        //        AssignedTo: 0,
        //        BunchGuid: "360cd601-b929-4b8b-86cc-8a15fea1ddc7",
        //        Categories: [{ CategoryId: 2, Category: "Sports", CreationDateStr: "2014-05-19T13:43:23.773Z" }],
        //        CreationDateStr: "2016-10-26T14:10:52.023Z",
        //        Filters: [],
        //        Highlight: null,
        //        IsAired: false,
        //        IsLatestNews: "HighlightNews",
        //        IsRecommended: false,
        //        LanguageCode: "ur",
        //        LastUpdateDateStr: "2016-10-26T14:10:52.023Z",
        //        NewsCameraMen: null,
        //        NewsPaperdescription: null,
        //        NewsTickerCount: 0,
        //        NewsTypeId: 1,
        //        OnAirCount: 0,
        //        OtherChannelExecutionCount: 0,
        //        ProgramId: 0,
        //        PublishTimeStr: "2016-10-26T14:10:01.000Z",
        //        ReporterId: 6,
        //        ResourceEdit: {
        //            Bottom: 0,
        //            FromTos: [{ From: 1.897574, To: 10.087383 }],
        //            Id: 42,
        //            Left: 0,
        //            ProgramId: 0,
        //            ResourceGuid: "ce9c3dcf-d48a-4162-a464-a566982fba03",
        //            ResourceTypeId: 0,
        //            Right: 0,
        //            Top: 0,
        //            Url: "http://10.1.17.252/shah share/test03-31-02_03-31-34_News Update.mp4"
        //        },
        //        ResourceGuid: null,
        //        Resources: [{
        //            CreationDateStr: "0001-01-01T00:00:00.000Z",
        //            Guid: "ce9c3dcf-d48a-4162-a464-a566982fba03",
        //            LastUpdateDateStr: "0001-01-01T00:00:00.000Z",
        //            ResourceTypeId: 2
        //        }],
        //        Score: 0,
        //        Slug: "No Slug",
        //        Source: "ARY News",
        //        SourceTypeId: 0,
        //        Suggestions: null,
        //        Tickers: null,
        //        Title: "اسدساداسدا",
        //        Version: 0,
        //        _id: "169",
        //        newsUpdateHistories: null,
        //        defaultImageUrl: ""
        //    };
        //    return obj;
        //},


        setThisCurrent = function (data) {

            if (data) {
                loadFromServerReporter(data.id);
            }
        },

        convertObj = function (data) {

            var retObj = data;
            retObj._id = data.id;
            retObj.Description = data.description();
            retObj.Title = data.title();
            retObj.Category = data.categories;//data.categories()[0].Category;
            retObj.LanguageCode = data.languageCode;
            retObj.Locations = data.source(); //[{ LocationId: 0, Location: data.source() }];
            retObj.Locations = [{ LocationId: 0, Location: data.Locations }];
            retObj.Tags = data.tags();
            retObj.PublishTimeStr = data.creationDate;

            return retObj;
        },

         loadFromServerReporter = function (id) {
             model.parentNewsId = id;
             var obj = {
                 id: id
             };
             $.when(manager.news.GetNews(obj))
                 .done(function (responseData) {
                     updateModelReporter(id)
                     if (responseData) {

                     } else {

                     }
                 })
             .fail(function () {
             });
         };

        updateModelReporter = function (id) {
            if (model.newsList()[0].newsId == undefined) {
                newsId = id;
            }
            var chanID = model.templatePaging.currentItem().channelId();
            model.programs(dc.programs.getByChannelId(chanID)());
            
            model.reporters(dc.users.getAllLocal());

            var news = dc.news.getAllLocalByIds([newsId])[0];
            model.isAssignment(news.assignedTo() && news.assignedTo() > 0);
            model.reportedMediaUrl(news.resources()[0].url());
            model.fillViewModelProducer(news);
            From(news.resourceEdits.FromTos[0].From);
            TOs(news.resourceEdits.FromTos[0].To);
            markBothTimes();

        };

        activeVideo = function () {
            //presenter.showloader(true);
            Starting(0);
            Ending(0);

            if (videoUrl().indexOf('.ts') == -1) {
                videoUrl(model.mediaEditor.src());
                $('#mp4PlayArea').show();
                $('#tsPlayArea').hide();
                //model.VidUrl = videoUrl();
                var video = document.getElementsByTagName('video')[0];
                var sources = video.getElementsByTagName('source');
                sources[0].src = videoUrl(model.mediaEditor.src());
                video.load();
                seekval = 0;
                //$('#videoplayer').get(0).currentTime = seekval;

                makeTime($('#videoplayer').get(0).currentTime);
            } else {
                $('#tsPlayArea').show();
                $('#mp4PlayArea').hide();

                var vidUrl = model.mediaEditor.src();
                vidUrl = vidUrl.replace("/Recording", ":85/Recording");//http://10.3.18.15/Recording/MUX2/1002/ARYNewsAsia/1_$2016-11-10%2000-00-33.ts
                videoUrl(vidUrl);
                //var tsPlay = "<embed height='100%' width='100%' name='plugin' src='" + vidUrl + "' type='video/mpeg'></embed>";
                //$("#tsPlayArea").append(tsPlay);
                //$("#ifrm")
                loadIframe("ifrm", model.duration(), vidUrl);
                //$("#embededPlayer").attr('src', vidUrl);
            }

            if ($.browser.chrome) {
                browserCss('chromeVPlayer');
            }
            else if ($.browser.mozilla) {
                browserCss('firefoxVPlayer');
                addEventListener("message", receivedTimeDuration, false);
            }

        },

        receivedTimeDuration = function (event) {
            if (event.data) {
                var a = JSON.parse(event.data);
                console.log(a.Time);
                vlcTime(a.Time);
                isPlaying(false);
                if (From() == 0) {
                    From(a.Time);
                    StartTime(toHHmmssff(a.Time));
                }
                else {
                    TOs(a.Time);
                    EndTime(toHHmmssff(a.Time));
                }
                makeTime(a.Time);
                enterMark();

            }
        },

        loadIframe = function (iframeName, totalvideotime, url) {
            if (url && url.length > 0) {
                var videoUrl = JSON.parse(JSON.stringify(url));
                videoUrl = videoUrl.replace(":85/Recording", "/Recording");
                var obje = {
                    URL: videoUrl,
                    ChannelVideoId: 0,
                    ChannelId: 0,
                    From: 0,
                    To: 0,
                    ProgramId: 0
                };


                $.when(manager.channels.GetTotalTime(obje))
                        .done(function (responseData) {
                            if (responseData) {
                                var duration = responseData.Data.DurationSeconds;

                                var $iframe = $('#' + iframeName);
                                if ($iframe.length) {

                                    $iframe.attr('src', '../Views/Reporters/Team B/ChannelReporter/VideoTS.cshtml?' + duration + '|' + url);
                                    return false;
                                }
                                return true;

                            }
                        });
            } else {
                var $iframe = $('#' + iframeName);
                $iframe.attr('src', '|');
            }
          
        },



        ResetMarking = function () {
            clearVideoData();
            From(0);
            TOs(0);
            listFromTos([]);
            StartTime('--:--');
            EndTime('--:--');
        },

        activate = function (routeData, callback) {
            if (model.templatePaging && model.templatePaging.currentItem && model.templatePaging.currentItem().id) {

            } else {
                setTimeout(function () {
                    router.navigateTo(config.views.channelreporter.home.url);
                }, 500);

                return false;
            }
            model.clearViewModel();
            ResetMarking();

            if (model.mediaEditor.src() && model.mediaEditor.src().length > 0) {
                videoUrl(model.mediaEditor.src())
                activeVideo();
            }

            model.toggleLanguage();

            messenger.publish.viewModelActivated({
                canleaveCallback: canLeave
            });


        },

        refreshVid = function () {
            loadIframe("ifrm",0, "");
            activeVideo();
        },

        canLeave = function () {
            model.newsList('');
            $("#videoplayer")[0].pause();
            loadIframe("ifrm", 0,"");
            return true;
        };

        $(document).keydown(function (e) {

            if (e.target.localName == 'body' || e.target.localName == 'video') {
                videoUrl(model.mediaEditor.src())
                if (videoUrl() && videoUrl().length > 0 && videoUrl().indexOf(".mp4") != -1) {
                    if (e.ctrlKey) {  // ctrl + '?'
                        if (e.keyCode == 39) {   // right arrow
                            forwa(fastseekchangeVal);
                        }
                        else if (e.keyCode == 37) {  // left arrow
                            backwa(fastseekchangeVal);
                        }
                    }
                    else {
                        if (e.keyCode == 39) {  // right arrow
                            forwa(seekchangeVal);
                        }
                        else if (e.keyCode == 37) {  // left arrow
                            backwa(seekchangeVal);
                        }
                        else if (e.keyCode == 32) {  // space
                            if (isPlaying() == false) {
                                $('#videoplayer').get(0).play();
                                isPlaying(true);
                            } else if (isPlaying() == true) {
                                $('#videoplayer').get(0).pause();
                                isPlaying(false);
                            }
                        }
                        else if (e.keyCode == 13) {    // enter
                            $('#videoplayer').get(0).pause();
                            isPlaying(false);
                            if (From() == 0) {
                                From($('#videoplayer').get(0).currentTime);
                                StartTime(toHHmmssff($('#videoplayer').get(0).currentTime));
                            }
                            else {
                                TOs($('#videoplayer').get(0).currentTime);
                                EndTime(toHHmmssff($('#videoplayer').get(0).currentTime));
                            }
                            makeTime($('#videoplayer').get(0).currentTime);
                            enterMark();
                        }
                    }
                }
            }
            else {

            }
        });

        return {
            activate: activate,
            config: config,
            e: e,
            submitNews: submitNews,
            model: model,
            canLeave: canLeave,
            setThisCurrent: setThisCurrent,
            ResetMarking: ResetMarking,
            StartTime: StartTime,
            EndTime: EndTime,
            router: router,
            appdata: appdata,
            videoUrl: videoUrl,
            refreshVid: refreshVid,
            prevPage: prevPage,
            lastScreen: lastScreen,
            browserCss: browserCss
            // SetThisNews: SetThisNews

        };
    });