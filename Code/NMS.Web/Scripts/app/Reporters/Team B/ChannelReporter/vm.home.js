﻿define('vm.home',
    [ 'jquery','datacontext', 'presenter', 'control.listheader', 'manager', 'config', 'vm.reportnews','router','messenger','reporter.appdata','underscore'],
    function ($, dc, presenter, ListHeader, manager, config, vm_reportNews, router, messenger, appdata, _) {

        var
            listHeader = new ListHeader('single'),
            message = ko.observable(""),
            lastObj = ko.observable({date : null , url : null}),
            groupBy = ko.observable(1),
            channelPrograms = ko.observable(),

            channels = ko.computed({
                read: function () {
                    return dc.channels.getObservableList();
                },
                deferEvaluation: true
            }),

            programs = ko.computed({
                read: function () {
                    return dc.programs.getObservableList();
                },
                deferEvaluation: true
            }),

            hourlySlots = ko.computed({
                read: function () {
                    return dc.hourlySlots.getObservableList();
                },
                deferEvaluation: true
            }),

            leftNav = [
                { name: 'Time Wise', cssClass: 'icon-time', groupId: 1, isSelected: ko.observable(true), count: ko.observable(0) },
                { name: 'Channel Wise', cssClass: 'icon-channel', groupId: 2, isSelected: ko.observable(false), count: ko.observable(0) },
                { name: 'Program Wise', cssClass: 'icon-program', groupId: 3, isSelected: ko.observable(false), count: ko.observable(0) }],

            fillResults = function (list, searchText, date) {

                var obj = { pending: 0, result: [] };
                var pending = [];
                searchText = $.trim(searchText);
                //console.log(JSON.stringify( list));
                for (var i = 0; i < list.length; i++) {
                    var channelVideos = list[i].channelVideos();
                    if (channelVideos.length > 0) {
                        for (var j = 0; j < channelVideos.length; j++) {

                            if (date === channelVideos[j].episodeDate) {

                                if ((searchText.length > 3 && (channelVideos[j].channel().name.toLowerCase().indexOf(searchText.toLowerCase()) > -1 ||
                                    channelVideos[j].program().name.toLowerCase().indexOf(searchText.toLowerCase()) > -1)) ||
                                    searchText.length <= 3) {

                                    obj.result.push(list[i]);

                                    if (!channelVideos[j].isProcessed()) {
                                        if (pending.indexOf(i) == -1) {
                                            pending.push(i);
                                        }
                                    }
                                    break;
                                }
                            }
                        }
                    
                    }
                  
                }
                //console.log(JSON.stringify(channelVideos));
                obj.pending = pending.length;
                return obj;
            },
            pendingObject = ko.observable(),

            list = ko.computed({
                read: function () {
                    var group = groupBy();
                    //var searchText = listHeader.searchText();
                    var searchText = '';
                    var date = listHeader.calendar.fromDate();

                    var result = [];
                    var list = [];
                    if (group == 1) {
                        list = hourlySlots();
                        listHeader.heading('Hours');
                    }
                    else if (group == 2) {
                        list = channels();
                        listHeader.heading('Channels');
                    }
                    else if (group == 3) {
                        list = programs();
                        listHeader.heading('Programs');
                    }
                    var obj = fillResults(list, searchText, date);
                    result = obj.result;

                    var pendingObj = { heading: 'Total Pending ' + listHeader.heading() + ' ', pendingCount: obj.pending }
                    pendingObject(pendingObj);

                    listHeader.count(result.length);
                    presenter.toggleActivity(false);
                    return result;
                },
                deferEvaluation: true
            }).extend({ throttle: 200 }),

            loadVideosFromServer = function (data) {
                if (router.currentHash() == config.views.channelreporter.live.url) {
                    message("Please Wait...");
                    $.when(manager.channels.getChannelVideosByDate(data,true))
                                       .done(function (data) {
                                           lastObj({ date: listHeader.calendar.from(), url: router.currentHash() });
                                           message("");
                                       })
                                       .fail(function () {
                                           config.logger.error("Could not load data, Please check your internet connection.");
                                           message("");
                                       });
                } else {
                    message("Please Wait...");
                    $.when(manager.channels.getChannelVideosByDate(data))
                                       .done(function (data) {
                                           lastObj({ date: listHeader.calendar.from(), url: router.currentHash() });
                                           message("");
                                       })
                                       .fail(function () {
                                           config.logger.error("Could not load data, Please check your internet connection.");
                                           message("");
                                       });
                }


               
            },
            selectedDate = ko.computed({
                read: function () {
                    var date = listHeader.calendar.from();
                    if (date != lastObj().date || router.currentHash() != lastObj().url) {
                        loadVideosFromServer({ id: date });
                    }
                },
                deferEvaluation: true
            }).extend({ throttle: 200 }),



            setGroupBy = function (data) {
                
                presenter.toggleActivity(true);
                for (var i = 0; i < leftNav.length; i++) {
                    leftNav[i].isSelected(false);
                }
                data.isSelected(true);
                groupBy(data.groupId);
            },
              activate = function (routeData, callback) {
                  $(".nav-step1").hide();
                  listHeader.isVisible(false);
                  messenger.publish.viewModelActivated({ canleaveCallback: canLeave });
                  if (router.currentHash() == config.views.channelreporter.live.url) {
                      var date = listHeader.calendar.from();
                      loadVideosFromServer({ id: date });
                  } else {
                      if (dc.hourlySlots.getObservableList().length > 0 && dc.hourlySlots.getObservableList()[0].programId==-1) {
                          var date = listHeader.calendar.from();
                          loadVideosFromServer({ id: date });
                      }
                  }
                  
              },

        canLeave = function () {
            return true
        },
            init = function () {
                listHeader.calendar.isSetToday(true);
            },

            getselectedProgram = function (data) {
                debugger
                var a = data;
            }


            itemClicked = function (item) {
                var channelprograms = [];
                var sortedPrograms = [];
                channelprograms = dc.programs.getByChannelId(item.channelId())();
                if (channelprograms.length > 0) {
                    sortedPrograms = _.sortBy(channelprograms, 'name');
                }
                sortedPrograms.push({ id: -2, name: "Program not found" });
                vm_reportNews.model.programs(sortedPrograms);
                
                var data = [];
                if (item.channelVideos) {
                    data = item.channelVideos();

                    var arr = _.filter(data, function (item) {
                        return item.episodeDate === listHeader.calendar.fromDate();
                    });
                    if (arr && arr.length > 0)
                        data = arr;
                }
                else {
                    data = [item];
                }
                vm_reportNews.model.setCurrentItem(data);
                if (router.currentHash() == config.views.channelreporter.live.url) {
                    vm_reportNews.lastScreen(config.views.channelreporter.live.url);
                } else {
                    vm_reportNews.lastScreen(config.views.channelreporter.home.url);
                }
                router.navigateTo(config.views.channelreporter.reportNews.url);
                
            };

        init();

        return {
            activate: activate,
            list: list,
            leftNav: leftNav,
            setGroupBy: setGroupBy,
            listHeader: listHeader,
            pendingObject: pendingObject,
            itemClicked: itemClicked,
            canLeave: canLeave,
            config:config,
            router: router,
            message:message,
            appdata:appdata

        };
    });