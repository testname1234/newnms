﻿define('vm',
    [
        'vm.shell',
        'vm.home',
        'vm.reportnews',
        'vm.mynews',
        'vm.report',
        'vm.updatenews',
        'vm.contentviewer'
    ],

    function (shell, home, channelreportNews, mynews, report, updatenews, contentviewer) {

        var vmDictionary = {};
        vmDictionary['.channelHomePanel']=home;
        vmDictionary['.reportNewsChannelReporter']=channelreportNews;
        vmDictionary['.myNewsPanelHeader']=mynews;
        vmDictionary['#reports-view']=report;
        vmDictionary['#shell-top-nav-view'] = shell;
        vmDictionary['#shell-top'] = shell;
        vmDictionary['.updateNewsLeftRight'] = updatenews;
        vmDictionary['.channelHomePanel2'] = home;
        vmDictionary['#content-viewer-main'] = contentviewer;
        


        return {
            dictionary: vmDictionary
        };
    });