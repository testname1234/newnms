﻿define('bootstrapper',
    [
        'config',
        'binder',
        'route-config',
        'manager',
        'timer',
        'reporter.appdata'
    ],
    function (config, binder, routeConfig, manager,timer,appdata) {

        var
            run = function () {

                var views = config.views.newspaperreporter;

                appdata.views = views;

                appdata.listHeader = 'My News';
                appdata.extractUserInformation();
                config.dataserviceInit();

                binder.bindStartUpEvents(views);
                routeConfig.register(views);
                var data = { id: appdata.lastUpdateTime() };


                $.when(manager.news.reporterLoadInitialData())
                    .done(function (responseData) {
                        $.when(manager.newspaper.newspaperReporterloadInitialData(data))
            .done(function (responseData) {
                binder.bindPreLoginViews(views);
                timer.start();
            })
             .fail(function (responseData) {

             });
    })
                    .fail(function (responseData) {

     });

                $.when(manager.categories.loadCategories())
                    .done(function (responseData) {
                        $.when(manager.locations.loadLocations())
                            .done(function (responseData) {

                            })
                             .fail(function (responseData) {

                             });
                    })
                     .fail(function (responseData) {

                     });


                amplify.subscribe(config.eventIds.onLogIn, function (data) {

                });
            };

        return {
            run: run
        };
    });