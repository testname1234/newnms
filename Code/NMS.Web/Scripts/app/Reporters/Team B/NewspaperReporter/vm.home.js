﻿define('vm.home',
    ['jquery', 'datacontext', 'presenter', 'control.listheader', 'manager', 'config', 'vm.reportnews', 'router'],
    function ($, dc, presenter, ListHeader, manager, config, vm_reportNews, router) {

        var
            listHeader = new ListHeader('range'),

            groupBy = ko.observable(2),

            dateWise = function (from, to) {
                var fromDate = new Date(from.getTime());
                var toDate = new Date(to.getTime());

                  var array = [];

                  for (var i = 0; fromDate <= toDate; fromDate.setDate(fromDate.getDate() + 1), i++) {
                      var obj = {};
                      obj.showNewsPaperName = true;
                      obj.name = moment(fromDate).format(config.DateFormat);
                      obj.dailyNewsPapers = dc.dailyNewsPapers.getByDailyNewsPaperDate(obj.name);

                      var list = obj.dailyNewsPapers();
                      var pending = 0;
                      var processed = 0;
                      var total = 0;

                      for (var j = 0; j < list.length; j++) {
                          if (list[j].processedPercentage() != '100%') {
                              pending++;
                          }
                          processed += list[j].processedPages();
                          total += list[j].newsPaperPages().length;
                      }

                      obj.processedPercentage = ko.observable(Math.round(((processed / total) * 100)) + '%');
                      obj.pending = ko.observable(pending);
                      array.push(obj);
                  }

                  return array;
              },

            newsPapers = ko.computed({
                read: function () {
                    return dc.newsPapers.getObservableList();
                },
                deferEvaluation: true
            }),


            leftNav = [
                    { name: 'Date', cssClass: 'icon-calendar', groupId: 1, isSelected: ko.observable(false), count: ko.observable(0) },
                    { name: 'Newspaper', cssClass: 'icon-newspaper', groupId: 2, isSelected: ko.observable(true), count: ko.observable(0) }],

            fillResults = function (list, searchText,from,to) {
                 var obj = { pending: 0, result: [] };

                 var pending = [];
                 searchText = $.trim(searchText);
                 for (var i = 0; i < list.length; i++) {

                     var dailyNewsPapers = list[i].dailyNewsPapers();
                     if (dailyNewsPapers.length > 0) {
                         if (!list[i].dailyPapers) {
                             list[i].dailyPapers = ko.observableArray([]);
                         }
                         var temp = [];
                         for (var j = 0; j < dailyNewsPapers.length; j++) {

                             var ticks = dailyNewsPapers[j].newsPaperDateTime.getTime();

                             if (ticks >= from.getTime() && ticks <= to.getTime()) {

                                 if ((searchText.length > 3 && (dailyNewsPapers[j].newsPaper().name.toLowerCase().indexOf(searchText.toLowerCase()) > -1)) ||
                                         searchText.length <= 3) {
                                     temp.push(dailyNewsPapers[j]);
                                 }
                             }

                         }
                         list[i].dailyPapers(temp);
                         
                         if (temp.length > 0) {
                             obj.result.push(list[i]);
                         }
                     }
                 }
                 obj.pending = pending.length;
                 return obj;
             },

            pendingObject = ko.observable(),

            list = ko.computed({
                read: function () {
                    var from = listHeader.calendar.from();
                    var to = listHeader.calendar.to();

                    var group = groupBy();
                    var searchText = listHeader.searchText();
                    var date = listHeader.calendar.fromDate();

                    var result = [];
                    var list = [];
                    if (group == 1) {
                        list = dateWise(from,to);
                        listHeader.heading('Date');
                    }
                    else if (group == 2) {
                        list = newsPapers();
                        listHeader.heading('Newspaper');
                    }

                    var obj = fillResults(list, searchText, from,to);
                    result = obj.result;

                    var pendingObj = { heading: 'Total Pending ' + listHeader.heading() + ' ', pendingCount: obj.pending }
                    pendingObject(pendingObj);

                    listHeader.count(result.length);
                    presenter.toggleActivity(false);

                    return result;
                },
                deferEvaluation: true
            }).extend({ throttle: 200 }),

            loadVideosFromServer = function (data) {
                $.when(manager.newspaper.getNewsPaperByDateRange(data))
                    .done(function (data) {
                    })
                    .fail(function () {
                        config.logger.error("Could not load data, Please check your internet connection.");
                    });
            },
            selectedDate = ko.computed({
                read: function () {
                    var fdate = listHeader.calendar.from();
                    var tdate = listHeader.calendar.to();
                    //if (date.toDateString() !== new Date().toDateString()) {
                        loadVideosFromServer({ fromDate: fdate, toDate: tdate });
                    //}
                },
                deferEvaluation: true
            }).extend({ throttle: 200 }),

            setGroupBy = function (data) {
                presenter.toggleActivity(true);
                for (var i = 0; i < leftNav.length; i++) {
                    leftNav[i].isSelected(false);
                }
                data.isSelected(true);
                groupBy(data.groupId);
            },

            itemClicked = function (item) {
                var data = [];
                if (item.newsPaperPages) {
                    data = item.newsPaperPages();
                    if (data)
                    {
                        for(var i=0; i<data.length; i++)
                        {
                            pagePart = data[i].newsPaperPagesParts()
                            for (var j = 0; j < pagePart.length; j++) {
                                var style = "width:" + (pagePart[j].Bottom - pagePart[j].Left - pagePart[j].Left) + "px;";
                                style += "height:" + (pagePart[j].Right - pagePart[j].Top - pagePart[j].Top) + "px;";
                                style += "top:" + (pagePart[j].Top) + "px;";
                                style += "left:" + (pagePart[j].Left) + "px;";
                                data[i].newsPaperPagesParts()[j]['newsAreaStyle'] = style;
                            }
                        }
                    }
                }
                else {
                    data = [item];
                }
                vm_reportNews.model.setCurrentItem(data);
                vm_reportNews.model.isNewsPaperDetail(false);
                router.navigateTo(config.views.channelreporter.reportNews.url);
            },


        activate = function (routeData, callback) {
        },

        init = function () {
            listHeader.calendar.isSetToday(true);
        };

        init();

        return {
            activate: activate,
            listHeader: listHeader,
            leftNav: leftNav,
            pendingObject: pendingObject,
            list: list,
            setGroupBy: setGroupBy,
            groupBy: groupBy,
            itemClicked: itemClicked
        };
    });