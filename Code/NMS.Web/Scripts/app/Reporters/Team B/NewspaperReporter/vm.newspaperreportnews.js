﻿define('vm.reportnews',
    ['config',
     'manager',
     'control.tag',
     'control.multiselect',
     'control.editorcontrol',
     'datacontext',
     'enum',
     'extensions',
     'moment',
     'control.videoeditor',
     'control.templatepaging',
     'router',
     'model.reportnews',
     'messenger',
     'reporter.appdata'
    ],
    function (config, manager, Tag, MultiSelect, EditorControl, dc, e, ext, moment, VideoEditor, TemplatePaging, router, ReportNews, messenger, appdata) {

        var model = new ReportNews(
            'urduRN',
            'englishRN',
            ['.reportNewsChannelReporter .editingSection .editor input[type=text]', '.reportNewsChannelReporter .editingSection .editor .jqte_editor'],
            e.NewsType.Story,
            'Report News',
            e.ContentType.Image,
            e.NewsFilterType.NewsPaper,
            null,
            true,
            'No portion cropped yet.Mark the image area to select your desire portion of image for the news.'
            ),

            setThisCurrent = model.templatePaging.setThisCurrent,
            submitNews = function () {
                model.dailyNewsPaperId = model.templatePaging.currentItem().dailyNewsPaper().newsPaperId;
                if (model.coordinates()) {
                    model.resourceEdit = {
                        ResourceTypeId: e.ContentType.Image, Id: model.templatePaging.currentItem().id, FileName: model.templatePaging.currentItem().fileName(),
                        Left: model.coordinates().Left, Right: model.coordinates().Right, Bottom: model.coordinates().Bottom, Top: model.coordinates().Top
                    };
                }
                model.submit();
            },


            pagePartClick = function (item) {
                partDetail = item.NewsPaperPagesPartsDetail;
                for (i = 0; i < partDetail.length; i++) {
                    item.NewsPaperPagesPartsDetail[i]['url'] = config.MediaServerUrl + 'getresource/' + partDetail[i].ImageGuid
                }
                model.NewsPaperDetail(item.NewsPaperPagesPartsDetail);
                model.isNewsPaperDetail(true);
            },

            Back = function () {
                model.NewsPaperDetail([]);
                model.isNewsPaperDetail(false);
            },

            activate = function (routeData, callback) {
            model.clearViewModel();
            messenger.publish.viewModelActivated({ canleaveCallback: canLeave });

        },

            canLeave = function () {
              model.newsList('');
              return true
          };

        return {
            activate: activate,
            config: config,
            e: e,
            submitNews: submitNews,
            model: model,
            setThisCurrent: setThisCurrent,
            router: router,
            appdata: appdata,
            pagePartClick: pagePartClick,
            Back: Back
        };
    });