﻿define('vm.home',
    ['jquery', 'datacontext', 'presenter', 'control.listheader', 'manager', 'config', 'vm.reportnews', 'router'],
    function ($, dc, presenter, ListHeader, manager, config, vm_reportNews, router) {

        var
            listHeader = new ListHeader(),

            groupBy = ko.observable(2),

            hourlySlots = ko.computed({
                read: function () {
                    return dc.hourlySlots.getObservableList();
                },
                deferEvaluation: true
            }),

            radioStations = ko.computed({
                read: function () {

                    return dc.radioStations.getObservableList();
                },
                deferEvaluation: true
            }),

            leftNav = [
                    { name: 'Hours', cssClass: 'icon-time', groupId: 1, isSelected: ko.observable(false), count: ko.observable(0) },
                    { name: 'Channels', cssClass: 'icon-radio', groupId: 2, isSelected: ko.observable(true), count: ko.observable(0) }],

            fillResults = function (list, searchText, date) {

                var obj = { pending: 0, result: [] };
                var pending = [];
                searchText = $.trim(searchText);
                for (var i = 0; i < list.length; i++) {

                    var radioStreams = list[i].radioStreams();
                    if (radioStreams.length > 0) {

                        for (var j = 0; j < radioStreams.length; j++) {

                            if (date === radioStreams[j].episodeDate) {

                                if ((searchText.length > 3 && (radioStreams[j].radioStation().name.toLowerCase().indexOf(searchText.toLowerCase()) > -1)) ||
                                    searchText.length <= 3) {

                                    obj.result.push(list[i]);

                                    if (list[i].processedPercentage() !== '100%') {
                                        if (pending.indexOf(i) == -1) {
                                            pending.push(i);
                                        }
                                    }
                                    break;
                                }
                            }
                        }
                    }
                }
                obj.pending = pending.length;
                return obj;
            },

            pendingObject = ko.observable(),

            list = ko.computed({
                read: function () {
                    var group = groupBy();
                    var searchText = listHeader.searchText();
                    var date = listHeader.calendar.fromDate();

                    var result = [];
                    var list = [];
                    if (group == 1) {
                        list = hourlySlots();
                        listHeader.heading('Hours');
                    }
                    else if (group == 2) {
                        list = radioStations();
                        listHeader.heading('Channels');
                    }

                    var obj = fillResults(list, searchText, date);
                    result = obj.result;

                    var pendingObj = { heading: 'Total Pending ' + listHeader.heading() + ' ', pendingCount: obj.pending }

                    if (group == 1) {
                        pendingObj.pendingCount = pendingObj.pendingCount.length == 1 ? ('0' + pendingObj.pendingCount) : pendingObj.pendingCount;
                        pendingObj.pendingCount = pendingObj.pendingCount + ':00' + ':00';
                    }
                    pendingObject(pendingObj);

                    listHeader.count(result.length);
                    presenter.toggleActivity(false);




                    if (result.length > 0) {

                        for (var i = 0; i < result.length; i++) {

                            for (var j = 0; j < result[i].radioStreams().length; j++) {


                                if (result[i].radioStreams()[j].episodeDate != date) {


                                    result[i].radioStreams().splice(j, 1);

                                }

                                if (result[i].radioStreams()[j].episodeDate != date) {


                                    result[i].radioStreams().splice(j, 1);

                                }

                            }

                        }


                    }


                    return result;
                },
                deferEvaluation: true
            }).extend({ throttle: 200 }),

            loadVideosFromServer = function (data) {
                $.when(manager.radio.getRadioByDateRange(data))
                    .done(function (data) {
                    })
                    .fail(function () {
                        config.logger.error("Could not load data, Please check your internet connection.");
                    });
            },
            selectedDate = ko.computed({
                read: function () {
                    var date = listHeader.calendar.from();
                    if (date.toDateString() !== new Date().toDateString()) {
                        loadVideosFromServer({ fromDate: date, toDate: date });
                    }
                },
                deferEvaluation: true
            }).extend({ throttle: 200 }),

            setGroupBy = function (data) {
                presenter.toggleActivity(true);
                for (var i = 0; i < leftNav.length; i++) {
                    leftNav[i].isSelected(false);
                }
                data.isSelected(true);
                groupBy(data.groupId);
            },

            itemClicked = function (item) {
                var data = [];
                if (item.radioStreams) {
                    data = item.radioStreams();
                }
                else {
                    data = [item];
                }
                vm_reportNews.model.setCurrentItem(data);
                router.navigateTo(config.views.channelreporter.reportNews.url);
            };

        activate = function (routeData, callback) {
        },

        init = function () {
            listHeader.calendar.isSetToday(true);
        };

        init();

        return {
            activate: activate,
            listHeader: listHeader,
            leftNav: leftNav,
            pendingObject: pendingObject,
            list: list,
            setGroupBy: setGroupBy,
            groupBy: groupBy,
            itemClicked: itemClicked

        };
    });