﻿define('vm',
    [
        'vm.shell',
        'vm.home',
        'vm.reportnews',
        'vm.mynews',
        'vm.report',
        'vm.updatenews'
    ],

    function (shell, home, channelreportNews, mynews, report, updatenews) {

        var vmDictionary = {};
        vmDictionary['.channelHomePanel']=home;
        vmDictionary['.reportNewsChannelReporter']=channelreportNews;
        vmDictionary['.myNewsPanelHeader']=mynews;
        vmDictionary['#reports-view']=report;
        vmDictionary['#shell-top-nav-view'] = shell;
        vmDictionary['#shell-top'] = shell;
        vmDictionary['.updateNewsLeftRight'] = updatenews;


        return {
            dictionary: vmDictionary
        };
    });