﻿define('vm.reportnews',
    ['config',
     'manager',
     'control.tag',
     'control.multiselect',
     'control.editorcontrol',
     'datacontext',
     'enum',
     'extensions',
     'moment',
     'control.templatepaging',
     'router',
     'model.reportnews',
     'messenger'
    ],
    function (config, manager, Tag, MultiSelect, EditorControl, dc, e, ext, moment, TemplatePaging, router, ReportNews, messenger) {

        var model = new ReportNews(
            'urduRN',
            'englishRN',
            ['.reportNewsChannelReporter .editingSection .editor input[type=text]', '.reportNewsChannelReporter .editingSection .editor .jqte_editor'],
            e.NewsType.Story,
            'Report News',
            e.ContentType.Audio,
            e.NewsFilterType.Radio,
            null,
            true,
            'No clip selected yet.Mark the audio area to select your desire portion of Program for the news.'
            ),
            setThisCurrent = model.templatePaging.setThisCurrent,
            submitNews = function () {
                model.radioStationId = model.templatePaging.currentItem().radioStation().id;
                if (model.mediaEditor.totalSelections().length > 0) {
                    model.resourceEdit = { ResourceTypeId: e.ContentType.Audio, Id: model.templatePaging.currentItem().id, FileName: model.templatePaging.currentItem().fileName(), FromTos: model.mediaEditor.totalSelections() };
                }
                model.submit();
            };

        activate = function (routeData, callback) {
            model.clearViewModel();
            messenger.publish.viewModelActivated({ canleaveCallback: canLeave });

        },

      canLeave = function () {
          model.newsList('');
          if (model.mediaEditor) {
              model.mediaEditor.stop();
          }
          return true
      };

        return {
            activate: activate,
            config: config,
            e: e,
            submitNews: submitNews,
            model: model,
            canLeave: canLeave,
            setThisCurrent:setThisCurrent
        };
    });