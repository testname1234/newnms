﻿define('vm.reportnews',
        ['config',
     'manager',
     'control.tag',
     'control.multiselect',
     'control.editorcontrol',
     'datacontext',
     'enum',
     'extensions',
     'moment',
     'control.videoeditor',
     'control.templatepaging',
     'router',
     'model.reportnews'
        ],
    function (config, manager, Tag, MultiSelect, EditorControl, dc, e, ext, moment, VideoEditor, TemplatePaging, router, ReportNews) {


        var model = new ReportNews(
               'urduRN',
               'englishRN',
               ['.courtNews .editingSection .editor input[type=text]', '.courtNews .editingSection .editor .jqte_editor'],
               e.NewsType.Story,
               'Report News',
               null,
               e.NewsFilterType.Police
               ),


         description = ko.observable();
        submitNews = function () {
            model.comments();
            console.log(description());
            model.submit();
        };

        opponnents = ko.computed({
            read: function () {

                var list = dc.newsLocation.getObservableList().clone();

                for (var i = 0; i < list.length; i++) {
                    list[i].isSelected = ko.observable(false);
                }
                return list;

            },
            deferEvaluation: true

        }),

         casetype = ko.computed({
             read: function () {

                 var list = dc.newsLocation.getObservableList().clone();

                 for (var i = 0; i < list.length; i++) {
                     list[i].isSelected = ko.observable(false);
                 }
                 return list;

             },
             deferEvaluation: true

         }),


     tmplName = 'case.add',

    opponentsMultiselect = ko.observable(new MultiSelect()),
    casetypeMultiselect = ko.observable(new MultiSelect()),


    activate = function (routeData, callback) {

    };


        return {
            activate: activate,
            config: config,
            e: e,
            submitNews: submitNews,
            model: model,
            opponnents: opponnents,
            casetype: casetype,
            opponentsMultiselect: opponentsMultiselect,
            casetypeMultiselect: casetypeMultiselect,
            description: description

        };
    });