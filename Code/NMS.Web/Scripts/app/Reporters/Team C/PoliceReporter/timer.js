﻿define('timer',
    ['manager', 'moment', 'config', 'reporter.appdata'],
    function (manager, moment, config, appdata) {
        var
            interval = config.reporterPollingInterval,
            start = function () {
                var obj = { rid: appdata.userId, pageCount: 20, startIndex: 0 };
                setTimeout(function longPolling() {
                    $.when(manager.news.LoadMyNewsList(obj))
                    .done(function (data) {
                        if (data.IsSuccess && data.Data.length > 0) {
                            if (data.Data.length < obj.pageCount) {
                                clearInterval(interval);
                                startPolling();
                            }
                            else {
                                obj.startIndex += obj.pageCount;
                                setTimeout(longPolling, interval * 1000);
                            }
                        }
                        else {
                            clearInterval(interval);
                            startPolling();
                        }
                    })
                    .fail(function () {
                        setTimeout(longPolling, interval * 1000);
                    });

                }, interval * 1000);
            },

            startPolling = function () {
                setTimeout(function polling() {
                    $.when(manager.news.reporterPolling())
                        .done(function (data) {
                            setTimeout(polling, 10 * 1000);                            
                        })
                        .fail(function () {
                            setTimeout(polling, 10 * 1000);
                        });

                }, interval * 1000);
            }

        return {
            start: start
        };
    });