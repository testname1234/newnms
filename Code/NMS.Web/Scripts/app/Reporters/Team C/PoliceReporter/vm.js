﻿define('vm',
    [
        'vm.shell',
        'vm.home',
		'vm.reportnews',
        'vm.reports',
        'vm.mynews',
        'vm.updatenews'
    ],

    function (shell, home, reportNews, reports, mynews, updatenews) {

        var vmDictionary = {};

        vmDictionary['#home-view'] = home;
        vmDictionary['#reporter-reportnews-view'] = reportNews;
        vmDictionary['.myNewsPanelHeader'] = mynews;
        vmDictionary['#reports-view'] = reports;
        vmDictionary['#shell-top-nav-view'] = shell;
        vmDictionary['.updateNewsLeftRight'] = updatenews;
        vmDictionary['#shell-top'] = shell;

        return {
            dictionary: vmDictionary
        };
    });