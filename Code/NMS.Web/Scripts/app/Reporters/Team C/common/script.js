$(window).load(function () {
  
    resolutionfix();
});

$(window).resize(function () {
   
    resolutionfix();
});

function resolutionfix() {
    var ht = $(window).height() - $('header.header').outerHeight() - $('.singlebar-menu').outerHeight() - 20;
    $('.innerwrap, .leftnav .mynews-nav-step1, .leftnav .mynews-nav-step1 ul').height(ht);
    $('.innerwrap, .leftnav .nav-step1, .leftnav .nav-step1 ul ').height(ht);
    $('.contentSection.programs').height($('.innerwrap').outerHeight() - $('.newsNav').outerHeight() - 67);
    $('.updateReport .latestNews').height($('.editingSection').outerHeight() + 30);
    $('.latestCol > div').height(ht - 40);
    $('.rightNewsListing').height($('.editingSection').outerHeight() - $('.editingSection .heading').outerHeight());
}