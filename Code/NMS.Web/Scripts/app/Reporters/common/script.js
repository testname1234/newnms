﻿$(window).load(function () {
    resolutionfix();
    $('.innerwrap').mCustomScrollbar({
        autoDraggerLength: true,
        autoHideScrollbar: true,
        scrollInertia: 100,
        advanced: {
            updateOnBrowserResize: true,
            updateOnContentResize: true,
            autoScrollOnFocus: false
        }
    });
});

$(window).resize(function () {
    resolutionfix();
});

function resolutionfix() {
    var ht = $(window).height() - $('header.header').outerHeight() - $('.singlebar-menu').outerHeight() - 20;
    $('.innerwrap, .leftnav .mynews-nav-step1, .leftnav .mynews-nav-step1 ul').height(ht);
    $('.latestCol > div').height(ht - 40);
}

// Popup SlideShow

var nmbrFiles = $('.xPop .mediaSection ul li').length;
$('.imgDetails .total').text(nmbrFiles);
$('body').delegate('.xPop a.next', 'click', function () {
    var imgActive = $(this).closest('.imageSection').children('.slideImg.active');
    var img = $(this).closest('.imageSection').children('.slideImg');
    if (imgActive.next().is('.slideImg')) {
        imgActive.removeClass('active');
        imgActive.next('.slideImg').addClass('active');
    } else {
        imgActive.removeClass('active');
        img.first('.slideImg').addClass('active');
    }
    fileNumber(imgActive);
});
$('body').delegate('.xPop a.prev', 'click', function () {
    var imgActive = $(this).closest('.imageSection').children('.slideImg.active');
    var img = $(this).closest('.imageSection').children('.slideImg');
    if (imgActive.prev().is('.slideImg')) {
        imgActive.removeClass('active');
        imgActive.prev('.slideImg').addClass('active');
    } else {
        imgActive.removeClass('active');
        img.last('.slideImg').addClass('active');
    }
    fileNumber(imgActive);
});
function fileNumber(imgActive) {
    var fileNmbr = $(imgActive).data('number');
    $('.imgDetails .fileNumber').text(fileNmbr);
}


// Header Top DropDowns
$('body').click(function () {
    $('header .topnav > li').siblings().children('ul').hide();
    $('header .topnav > li').siblings().children('div').hide();
    $('.headQuen > ul > li').siblings().children('ul').hide();
    $(".slideDown li").find('ul').stop(true, true).slideUp('fast');
    resetCount = 0;
});
$('header .topnav > li').click(function (event) {
    event.stopPropagation();
    $('header .topnav > li').siblings().children('ul, div').hide();
    $(this).children('ul').show();
    if ($(this).hasClass('alerts')) {
        $(this).parents().siblings().children('> ul').hide();
        $(this).children('div').show();
        $('header .topnav > li.alerts > div > ul').scrollTop(1);
    }
});

// CameraMan
//$(document).keydown(function () {
//    if (event.keyCode == 27)
//        for (var i = 0; i < $('.cameraman').length; i++)
//            $('.cameraman').get(i).classList.remove('active');
//});

//$('body').click(function () {
//    if (event && event.target && event.target.parentElement && event.target.parentElement.className != 'jqte_tool jqte_tool_23 unselectable')
//        $('.cameraman').removeClass('active');

//});

$('.cameraman').click(function (event) {
    event.stopPropagation();
    $('.cameraman').addClass('active');
});

  

// Next Prev News Update Report
//$('.outerArrowRight').click(
//  function () {
//      $(".latestCol.right").stop(true, true).animate({
//          right: '0px'
//      }, 200, 'linear');
//      $(".latestCol.right").show();
//  });
//$('.latestCol.right a.close, .outerArrowLeft').click(function () {
//    $(".latestCol.right").hide();
//    $(".latestCol.right").stop(true, true).animate({
//        right: '-253px'
//    }, 200, 'linear');
//});
//$('.outerArrowLeft').click(
//  function () {
//      $(".latestCol.left").stop(true, true).animate({
//          left: '0px'
//      }, 200, 'linear');
//      $(".latestCol.left").show();
//  });
//$('.latestCol.left a.close, .outerArrowRight').click(function () {
//    $(".latestCol.left").hide();
//    $(".latestCol.left").stop(true, true).animate({
//        left: '-253px'
//    }, 200, 'linear');
//});