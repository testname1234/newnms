﻿define('vm.shell',
     [
         'ko',
         'config',
         'vm',
         'router',
         'reporter.appdata',
         'rabbitMQ'
     ],
    function (ko, config, vm, router, appdata,rabbitMQ) {

        var
            userName = ko.computed({
                read: function () {
                    console.log(appdata.currentUser());
                    return appdata.currentUser().displayName;
                },
                deferEvaluation: false
            }),

            navigations = function () {
                var menuHashes = appdata.views;
                var navs = [];

                for (var item in menuHashes) {
                    var tempItem = menuHashes[item];
                    if (tempItem.isInTopMenu) {
                        var obj = {};
                        obj.title = tempItem.title;
                        obj.href = tempItem.url;
                        obj.view = tempItem.view;
                        navs.push(obj);
                    }
                }

                return navs;

            },

            currentPage = ko.computed({
                read: function () {
                    return router.currentHash();
                },
                deferEvaluation: true
            }),



            // Methods
            //-------------------

            activate = function (routeData) {
            },

             logOut = function () {

                 window.location.href = '/login#/index';
                 document.cookie = config.sessionKeyName + '=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';
                 document.cookie = config.roleid + '=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';
                 document.cookie = config.userid + '=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';
             },
             changePassword = function () {
                 $("#content-viewer-main").children('#contentviewer-overlay').addClass('warningOverlay');
                 $("#content-viewer-main").children('.warning.changePassword').css('display', 'initial')
               },

            init = function () {
                activate();
                var urlID = 'user/changed_' + appdata.userId;
                rabbitMQ.subscribe(urlID, logOut);
            };
        //$(".bdl").click(function () {

        //    window.location.href = '/login#/index';
        //    document.cookie = config.sessionKeyName + '=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';
        //    document.cookie = config.roleid + '=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';
        //    document.cookie = config.userid + '=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';

        //});

        init();

        return {
            currentPage: currentPage,
            activate: activate,
            navigations: navigations,
            logOut: logOut,
            userName: userName,
            changePassword: changePassword
        };
    });
