﻿define('vm.updatenews',
    ['config', 'datacontext', 'manager', 'enum', 'model.reportnews', 'control.templatepaging', 'router', 'reporter.appdata'],
    function (config, dc, manager, e, ReportNews, TemplatePaging, router, appdata) {

        var model = new ReportNews(
            'urduMN',
            'englishMN',
            ['.updateReportnews .editingSection .editor input[type=text]', '.updateReportnews .editingSection .editor .jqte_editor', '.urduFontFamily'],
            e.NewsType.Story,
            'Update News',
            null,
            e.NewsFilterType.FieldReporter,
            'small'
            ),
            submitNews = function () {
                model.submit();
            },
           
            templatePaging = new TemplatePaging(true),
            setThisCurrent = templatePaging.setThisCurrent,
            totalLength = ko.computed({
                read: function () {
                    return allNews().length - 1;
                },
                deferEvaluation: true

            }),

            allNews = ko.computed({
                read: function () {
                    var list = dc.news.observableList();
                    var ticks = new Date().getTime();
                    return _.sortBy(_.filter(list, function (news) {
                        return (typeof news.parentNewsId == 'undefined');
                    }), function (item) {
                        return (ticks - new Date(item.publishTime).getTime());
                    });
                },
                deferEvaluation: true

            }),

            setCurrentNews = function (data) {
                //loadFromServer(data.id);
                //var index = allNews().indexOf(data);
                //templatePaging.list = allNews;
                //templatePaging.currentIndex(index);
                //templatePaging.currentItem(data);
                //templatePaging.loadFromServer = loadFromServer;


                newsId = data.id;
                newsData = data;
                loadFromServer(data.id);
                var index = allNews().indexOf(data);
                templatePaging.list = allNews;
                templatePaging.currentIndex(index);
                templatePaging.currentItem(data);
                
                templatePaging.loadFromServer = loadFromServer;
                //model.isAssignment(data.assignedTo() && data.assignedTo() > 0);
                //fillImages();

            },
            loadFromServer = function (id) {
                model.parentNewsId = id;
                var obj = { id: id };
                $.when(manager.news.GetNews(obj))
                    .done(function (responseData) {
                        updateModel(id)
                        if (responseData) {

                        } else {

                        }
                    })
                .fail(function () {
                });
            };

        updateModel = function (id) {
            if (!newsId) {
                newsId = id;
            }

            model.programs(dc.programs.getByChannelId(168)());
            model.reporters(dc.users.getAllLocal());

            var news = dc.news.getAllLocalByIds([newsId])[0];
            model.isAssignment(news.assignedTo() && news.assignedTo() > 0);
            if (news.resources() && news.resources().length > 0) {
                model.reportedMediaUrl(news.resources()[0].url());
            }
            
            model.fillViewModelProducer(news);

            if (news.languageCode == 'ur') {
                setTimeout(function () {
                    $("#urduMN")[0].click()
                }, 500);
            } else {
                setTimeout(function () {
                    $("#englishMN")[0].click()
                }, 500);
            }
            
        };
           
            model.currentItemReources=ko.computed({
                read: function () {
                    if (templatePaging.currentItem()) {
                        return templatePaging.currentItem().resources();
                       }  
                },
                  deferEvaluation: false

              });

        activate = function (routeData, callback) {

        };

        return {
            activate: activate,
            config: config,
            templatePaging: templatePaging,
            totalLength: totalLength,
            allNews: allNews,
            setCurrentNews: setCurrentNews,
            submitNews: submitNews,
            model: model,
            e: e,
            setThisCurrent: setThisCurrent,
            router: router,
            appdata: appdata

        };
    });