﻿define('vm.reportnews',
    ['config', 'enum', 'model.reportnews', 'model', 'reporter.appdata', 'router', 'vm.contentviewer', 'datacontext', 'control.templatepaging', 'model.mapper'],
    function (config, e, ReportNews, parentModel, appdata, router, contentviewer, datacontext, TemplatePaging, mapper) {

        var model = new ReportNews('urduRN', 'englishRN', ['.leftPanel .editor .textboxCss', '.reportNewsEditor', '.textboxCss .textboxHighLight'], e.NewsType.Story, 'Report News', null, e.NewsFilterType.FieldReporter),
            tickerReporterControl = new parentModel.TickerReporterControl(),
            templatePaging = new TemplatePaging(true),
              currentResource = ko.observable(''),
             // reportnewsView = new ReportNews('urduRN', 'englishRN', ['.leftPanel .editor input[type=text]', '.leftPanel .editor .jqte_editor', '.reportNewsEditor'], e.NewsType.Story, 'Report News', null, e.NewsFilterType.FieldReporter),
                submitNews = function () {
                    model.tickerLines(tickerReporterControl.tickerLineList());
                    model.currentTicker(tickerReporterControl.currentTicker());
                    if (tickerReporterControl.lastTicker && tickerReporterControl.lastTicker()) {
                        tickerReporterControl.tickerLineList.push({ Text: tickerReporterControl.lastTicker(), LanguageCode: "ur", SequenceId: tickerReporterControl.tickerLineList().length == 0 ? 1 : tickerReporterControl.tickerLineList().length + 1 });
                    }
                    
                    model.submitNewsFile();
                    //datacontext.news.clearAllNews(true);
                    //refillNews();
                    tickerReporterControl.tickerLineList([])
                },

                refillNews = function () {
                    var obj = {rid: appdata.userId, pageCount: 20, startIndex: 0 };
                        $.when(manager.news.LoadMyNewsList(obj))
                            .done(function (data) { 
                               
                            })
                },

                //testingUploader = ko.computed({
                //    read: function () {
                //        var arr = model.uploader().resources();
                //        console.log(arr.length + " vm");
                //    },
                //    deferEvaluation: false
                //}),


            clickAttachment = function () {
                if (appdata.currentUser().userType == e.UserType.FReporter) {
                    $('.editorUploader')[0].click();
                }
                else {
                    $('.editorUploader').click();
                }

            },

             setCurrentContent = function (data) {
                 var filterItem = model.uploader().mappedResources().filter(function (e) {
                     return e.guid == data.id
                 })[0];
                 templatePaging.currentItem({ url: '' }); // jugaaar
                 var item = templatePaging;
                 item.currentItem().url = filterItem.url();
                 item.currentItem()["isProcessed"] = ko.observable(false);
                 item.currentItem().type = filterItem.type;
                 model.popuptempPaging.currentItem(item.currentItem());
                 model.showPopup(true);

             },
             clickCameramen = function () {
                 event.stopPropagation();
                 $('.cameraman').toggleClass('active');
             },


              openMediaSelection = function () {
                  contentviewer.mediaSelectionUrl(config.hashes.production.topicSelection + '?isiframe');
                  $("#content-viewer-main").children('#contentviewer-overlay').addClass('warningOverlay');
                  contentviewer.isMediaViewVisibleExplorer(true);
                  //appdata.isDefaultTab(!appdata.isDefaultTab());
                  // document.getElementById('popupIframeFExp').contentWindow.myFunction();
              },





              subscribeEvents = function () {

                  appdata.currentResource.subscribe(function (value) {
                     
                      if (value && value != '') {
                          //if (typeof topicSelectionVM !== 'undefined' && topicSelectionVM.isIframeVisible()) {
                          //    var resource = value;
                          //    //window.parent.window.postMessage(value, "*");
                          //}
                          //else {
                              var value = JSON.parse(value);
                              if (value.Guid) {
                                  var res = mapper.resource.fromDto(value);
                                  value.ResourceTypeId = res.type;
                                  res.ResourceTypeId = res.type;
                                  var arr = _.filter(model.uploader().mappedResources(), function (resource) {
                                      return resource.guid === res.guid;
                                  });
                                  if (arr && arr.length > 0) {
                                    //  if (value && !value.__kg_selected__) { //
                                          model.uploader().mappedResources().remove(arr[0]);
                                          var arr1 = _.filter(model.uploader().uploadedResources(), function (upresource) {
                                              return upresource.Guid === res.guid;
                                          });
                                          if (arr1 && arr1.length > 0) {
                                              model.uploader().uploadedResources().remove(arr1[0]);
                                          }
                                          model.uploader().uploadedResources.valueHasMutated();
                                          model.uploader().resources(model.uploader().uploadedResources());
                                    //  }
                                  }
                                  else {
                                      model.uploader().mappedResources().push(res);
                                      model.uploader().uploadedResources().push(value);
                                      model.uploader().mappedResources()[model.uploader().mappedResources().length - 1].type = res.type;
                                      model.uploader().mappedResources()[model.uploader().mappedResources().length - 1].IgnoreMeta = 1;
                                      model.uploader().uploadedResources.valueHasMutated();
                                      model.uploader().resources(model.uploader().uploadedResources());
                                  }
                              }
                              else {
                                  var obj = {
                                      Guid: value.guid,
                                      ResourceId: value.id,
                                      ResourceTypeId: value.type,
                                      IgnoreMeta: 1
                                  }
                                  var arr = _.filter(model.uploader().mappedResources(), function (resource) {
                                      return resource.guid === value.guid;
                                  });
                                  if (arr && arr.length > 0) {
                                      model.uploader().mappedResources().remove(arr[0]);
                                      var arr1 = _.filter(model.uploader().uploadedResources(), function (upresource) {
                                          return upresource.Guid === value.guid;
                                      });
                                      if (arr1 && arr1.length > 0) {
                                          model.uploader().uploadedResources().remove(arr1[0]);
                                      }
                                      model.uploader().uploadedResources.valueHasMutated();
                                      model.uploader().resources(model.uploader().uploadedResources());
                                  }
                                  else {
                                      model.uploader().mappedResources().push(value);
                                      model.uploader().uploadedResources().push(obj);
                                      model.uploader().mappedResources()[model.uploader().mappedResources().length - 1].type = value.type;
                                      model.uploader().mappedResources()[model.uploader().mappedResources().length - 1].IgnoreMeta = 1;
                                      model.uploader().uploadedResources.valueHasMutated();
                                      model.uploader().resources(model.uploader().uploadedResources());
                                  }
                              }
                        //  }
                      }
                  });

              },


        activate = function (routeData, callback) {
            model.isAssignment(router.currentHash() == config.views.v3.assignment.url);
            model.isTelevisionView(false);
            model.clearViewModel();
        },


                    init = function () {
                            subscribeEvents();
                            model.programs(datacontext.programs.getByChannelId(168)());
                            model.reporters(datacontext.users.getAllLocal());
                           
                    };


        return {
            activate: activate,
            config: config,
            e: e,
            submitNews: submitNews,
            model: model,
            tickerReporterControl: tickerReporterControl,
            appdata: appdata,
            router: router,
            clickAttachment: clickAttachment,
            openMediaSelection: openMediaSelection,
            clickCameramen: clickCameramen,
            currentResource: currentResource,
            setCurrentContent:setCurrentContent,
            templatePaging: templatePaging,
            init: init
        };

    });