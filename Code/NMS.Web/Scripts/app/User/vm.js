﻿define('vm',
    [
       
        'vm.login'
	
    ],

    function (login) {
        var vmDictionary = {};

        vmDictionary['#login-view'] = login;


        return {
            dictionary: vmDictionary
        };
    });