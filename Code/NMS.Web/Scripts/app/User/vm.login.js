﻿define('vm.login',
    ['ko', 'jquery', 'manager', 'datacontext', 'config'],
    function (ko, $, manager, dc, config) {
        var
            username = ko.observable(),
            password = ko.observable(),
            modules = ko.observableArray([]),
            ReporterLocation = ko.observable(''),
            NewsTypeId = ko.observable(''),
            errorMessage = ko.observable(false),
            encryptedName = ko.observable(''),
            initialDate = ko.observable('00:00:00'),
            locData = [],
            login = function () {
                var obj = {};
                obj.Login = $.trim(username());
                obj.Password = $.trim(password());
                $.when(manager.usermanagement.userlogin(obj))
                .done(function (data) {
                    if (data.IsSuccess && data.Data) {
                        errorMessage(false);

                        var id = data.Data.Modules[0].UserId;
                        var foldersMeda = [];
                        modules(dc.UserRole.getModuleUrlBymoduleId(id)());

                        locData = data.Data;
                        if (modules() && modules().length <= 1) {
                            localStorage.setItem('ls.userInfo', JSON.stringify(data.Data));
                        }

                        encryptedName(data.Data.EncriptedName);
                        if (modules().length <= 1) {
                            url = modules()[0].moduleUrl
                            document.cookie = config.userid + "=" + modules()[0].userId;
                            document.cookie = config.sessionKeyName + "=" + modules()[0].sessionkey;
                            document.cookie = "UserName" + "=" + modules()[0].fullName;
                            document.cookie = "Password" + "=" + $.trim(password());
                            document.cookie = "EncriptedName" + "=" + data.Data.EncriptedName;
                            document.cookie = "EncriptedUserId" + "=" + data.Data.EncriptedUserId;
                            if (data.Data.MetaData && data.Data.MetaData) {
                                document.cookie = "Metas" + "=" + JSON.stringify(data.Data.MetaData);
                                for (var i = 0; i < data.Data.MetaData.length; i++) {
                                    if (parseInt(data.Data.MetaData[i].MetaTypeId) == 12) {
                                        document.cookie = "LocationId" + "=" + parseInt(data.Data.MetaData[i].MetaValue);
                                        ReporterLocation(data.Data.MetaData[0].MetaValue);
                                    }
                                    if (parseInt(data.Data.MetaData[i].MetaTypeId) == 7 && (data.Data.MetaData[i].MetaName == "Package" || data.Data.MetaData[i].MetaName == "Story")) {
                                        document.cookie = "NewsTypeId" + "=" + parseInt(data.Data.MetaData[i].MetaValue);
                                    }
                                    if (parseInt(data.Data.MetaData[i].MetaTypeId) == 14) {
                                        document.cookie = "BucketId" + "=" + parseInt(data.Data.MetaData[i].MetaValue);
                                    }
                                    if (parseInt(data.Data.MetaData[i].MetaTypeId) == 15) {
                                        document.cookie = "ApiKey" + "=" + data.Data.MetaData[i].MetaValue;
                                    }
                                    if (parseInt(data.Data.MetaData[i].MetaTypeId) == 19)
                                    {
                                        foldersMeda.push({ 'FolderName': data.Data.MetaData[i].MetaName, 'FolderId': data.Data.MetaData[i].MetaValue , 'programInterval' : '00:00:00'});
                                    }
                                }
                            }

                                      document.cookie = "FoldersInfo" + "=" +JSON.stringify(foldersMeda);
                                      document.cookie = "isFirstTimeLogin" + "= true";
                            //if (data.Data.MetaData && data.Data.MetaData[0].MetaValue) {
                            //    document.cookie = "LocationId" + "=" + parseInt(data.Data.MetaData[0].MetaValue);
                            //    ReporterLocation(data.Data.MetaData[0].MetaValue);
                            //}

                            try {
                                if (app) {
                                    document.cookie = "UserName" + "=" + modules()[0].workRoleName;
                                }
                            }
                            catch (e) {
                            }
                            document.cookie = config.roleid + "=" + modules()[0].workRoleId;
                            window.location.href = url;
                        }
                    }

                    else {
                    }
                })
                .fail(function (ex) {
                    errorMessage(true);
                });
            },
            naviageroute = function (d) {
                for (var i = 0; i < locData.Modules.length;i++)
                {
                    if (locData.Modules[i].WorkRoleId != d.workRoleId)
                    {
                        locData.Modules.remove(locData.Modules[i]);
                    }
                }
                document.cookie = "Metas" + "=" + JSON.stringify(locData.MetaData);
                var foldersMeda = [];
                for (var i = 0; i < locData.MetaData.length; i++) {
                    if (parseInt(locData.MetaData[i].MetaTypeId) == 14) {
                        document.cookie = "BucketId" + "=" + parseInt(locData.MetaData[i].MetaValue);
                    }
                    if (parseInt(locData.MetaData[i].MetaTypeId) == 15) {
                        document.cookie = "ApiKey" + "=" + locData.MetaData[i].MetaValue;
                    }
                    if (parseInt(locData.MetaData[i].MetaTypeId) == 19) {
                        foldersMeda.push({ 'FolderName': locData.MetaData[i].MetaName, 'FolderId': locData.MetaData[i].MetaValue, 'programInterval': '00:00:00' });
                    }
                }

                document.cookie = "FoldersInfo" + "=" + JSON.stringify(foldersMeda);
                document.cookie = "isFirstTimeLogin" + "= true";

                localStorage.setItem('ls.userInfo', JSON.stringify(locData)); 

                url = d.moduleUrl
                document.cookie = config.userid + "=" + d.userId;
                document.cookie = config.sessionKeyName + "=" + d.sessionkey;
                document.cookie = config.roleid + "=" + d.workRoleId;
                if (ReporterLocation()) {
                    document.cookie = "LocationId" + "=" + parseInt(ReporterLocation());
                }
                if (NewsTypeId()) {
                    document.cookie = "NewsTypeId" + "=" + parseInt(NewsTypeId());
                }

                document.cookie = "UserName" + "=" + modules()[0].fullName;
                document.cookie = "Password" + "=" + $.trim(password());
                document.cookie = "EncriptedName" + "=" + encryptedName();

                window.location.href = url;
            };

        $("input").keyup(function (event) {
            if (event.keyCode == 13) {
                login();
            }
        });

        activate = function (routeData, callback) {
        },
        canLeave = function () {
            return true;
        };

        return {
            activate: activate,
            canLeave: canLeave,
            username: username,
            password: password,
            login: login,
            modules: modules,
            errorMessage: errorMessage,
            naviageroute: naviageroute
        };
    });