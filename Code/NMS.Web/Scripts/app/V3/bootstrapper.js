﻿define('bootstrapper',
    [
        'config',
        'binder',
        'route-config',
        'manager',
        'timer',
        'reporter.appdata',
        'appdata',
        'datacontext',
        'model.reportnews',
        'enum',
        'vm',
        'ko'
        
    ],
    function (config, binder, routeConfig, manager, timer, appdata, commonAppdata, dc, modelReportnews, e, vm, ko) {

        var
            run = function () {

                var views = config.views.v3;

                appdata.views = views;
                appdata.listHeader = 'My News';
                config.dataserviceInit();
                appdata.extractUserInformation();
                commonAppdata.extractUserInformation();

                if (appdata.currentUser().userType == e.UserType.FReporter) {
                } else {
                    window.location.href = 'login#/index';
                }

                binder.bindStartUpEvents(views);
               
                routeConfig.register(views);

                $.when(manager.news.reporterLoadInitialData(false))
                    .done(function (responseData) {
                        // alert('abe');
                        //views['shell']= { title: '', url: '', isInTopMenu: false, view: "#shell-top-nav-view" };
                        ko.applyBindings(vm.dictionary['#content-viewer-main'], $('#content-viewer-main')[0]);
                        binder.bindPreLoginViews(views);
                        vm.reportNews.init();
                        timer.start();
                    })
                     .fail(function (responseData) {

                     });

                
                

                var obj = { rid: appdata.userId, pageCount: 20, startIndex: 0 };
                $.when(manager.news.LoadMyNewsList(obj))
                           .done(function (data) {

                           });

               // ko.applyBindings(vm.contentviewer, getView(ids.contentViewer))

                $.when(manager.categories.loadCategories())
                   .done(function (responseData) {
                       $.when(manager.locations.loadLocations())
                            .done(function (responseData) {

                            })
                             .fail(function (responseData) {

             });

                    })
                    .fail(function (responseData) {

 });

                $.when(manager.usermanagement.getUsersByWorkRoleId(e.UserType.CameraMan))
                           .done(function (responseData) {
                               dc.users.fillData(responseData.Data);
                               appdata.isCameramanFill(true);
                           })
                           .fail(function () {
                               config.logger.error("No Data Fetch");
                           });

                amplify.subscribe(config.eventIds.onLogIn, function (data) {

                });
            };

        return {
            run: run
        };
    });
