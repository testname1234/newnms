﻿define('reporter.appdata',
    ['ko','jquery','helper','model.user'],
    function (ko, $, helper, User) {
        if ($.trim(helper.getCookie('user-id')) == '') {
            window.location.href = '/Login#/index';
        }
        var
             isNewsCreated = ko.observable(false),
            pollingRequired = ko.observable(false),
            currentResource = ko.observable(''),
            userId = helper.getCookie('user-id'),
            roleId = helper.getCookie('role-id'),
            locationId = helper.getCookie('LocationId'),
            isDefaultTab = ko.observable(false),
            lastUpdateTime = ko.observable(new Date().toISOString()),
            views = {},
            currentUser = ko.observable(''),
            isCameramanFill = ko.observable(false),
            extractUserInformation = function () {
                var tempUser = new User();

                tempUser.id = parseInt($.trim(helper.getCookie('user-id')));
                tempUser.name = $.trim(helper.getCookie('UserName').toUpperCase());
                tempUser.displayName = $.trim(helper.getCookie('UserName').toUpperCase());
                tempUser.userType = parseInt($.trim(helper.getCookie('role-id')));

                currentUser(tempUser);
            },
            listHeader,
            newsrightitem;
    

        return {
            lastUpdateTime: lastUpdateTime,
            views: views,
            listHeader:listHeader,
            newsrightitem: newsrightitem,
            userId: userId,
            currentUser: currentUser,
            extractUserInformation: extractUserInformation,
            isCameramanFill: isCameramanFill,
            isDefaultTab: isDefaultTab,
            currentResource: currentResource,
            isNewsCreated: isNewsCreated,
            pollingRequired: pollingRequired,
            locationId: locationId
        };
    });