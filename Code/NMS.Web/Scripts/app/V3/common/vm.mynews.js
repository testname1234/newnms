﻿define('vm.mynews',
    [
       'jquery',
       'datacontext',
       'vm.updatenews',
       'router',
       'config',
       'utils',
       'underscore',
       'moment',
       'enum',
       'manager',
       'presenter',
       'control.templatepaging',
       'control.listheader',
       'reporter.appdata',
       'messenger',
       'helper'

    ],
    function ($, dc, updatenews, router, config, utils, _, moment, e, manager, presenter, TemplatePaging, ListHeader, appdata, messenger, helper) {
        var
           listHeader = new ListHeader('range'),
           rightTmplName,
           imageFooterTmplName = 'reporter.mynews',
           contentFooterTmplName = 'reporter.mynews',
           contentTmplName = 'reporter.mynews',
           selectedFilterId = ko.observable(-1),
           sliderCurrentNewsid = ko.observable(''),
           parentId = ko.observable(' '),
           sliderCurrentNewsid = ko.observable(''),
           showPopup = ko.observable(false),
           groupByFilter = ko.observable(2),
           selectedDate = ko.observable(''),
           currentDate = ko.observable(),
           pageIndex = ko.observable(1),
           totalLength = ko.observable(1),
           screenMessage = ko.observable("Please Wait"),
           pageSize = 10,
           news = ko.observable(),
           allNews = ko.observable([]),
           stopPlayer = ko.observable(false),
           templatePaging = new TemplatePaging(false),
           pollingRequestObj = ko.observable({ from: '', to: '', lastUpdate: '', search: '' }),
           lastPollingRequestObj = ko.observable({ from: '', to: '', lastUpdate: '', search: '' }),

            //allNews = ko.computed({
            //    read: function () {
            //        var list = dc.news.observableList();
            //        var ticks = new Date().getTime();
            //        console.log(list.length);
            //        return list;
            //        //return _.sortBy(_.filter(list, function (news) {
            //        //    return (typeof news.parentNewsId == 'undefined');
            //        //}), function (item) {
            //        //    return (ticks - new Date(item.publishTime).getTime());
            //        //});
            //    },
            //    deferEvaluation: true

            //}),

            //totalLength = ko.computed({
            //    read: function () {
            //        return allNews().length - 1;
            //    },
            //    deferEvaluation: true

            //}),
            loadFromServer = function (id) {
                parentNewsId = id;
                var obj = { id: id };
                $.when(manager.news.GetNews(obj))
                    .done(function (responseData) {
                        if (responseData) {

                        } else {

                        }
                    })
                .fail(function () {
                });
            },

            CurrentDisplayedNews = function (data) {
                if (data.resources() && data.resources().length > 0)
                {
                    loadFromServer(data.id);
                    var index = allNews().indexOf(data);
                    templatePaging.list = allNews;
                    templatePaging.currentIndex(index);
                    templatePaging.currentItem(data);
                    templatePaging.loadFromServer = loadFromServer;
                    parentId('');
                    parentId(data.id);
                    showPopup(true);
                    return true;
              }
            
            },

            filters = ko.computed({
                read: function () {
                    return dc.filters.observableList();
                },
                deferEvaluation: true
            }),

        alldates = ko.computed({
            read: function () {
                //clearallFilters();
                var list = allNews();
                var fromDate = new Date(listHeader.calendar.from());
                var toDate = new Date(listHeader.calendar.to());
                var alldatesArray = new Array();
                for (var i = 0; fromDate <= toDate; fromDate.setDate(fromDate.getDate() + 1), i++) {
                    var obj = {};
                    obj.date = moment(fromDate).format('MMMM D, YYYY');
                    var filteredList = _.filter(list, function (item) {
                        return (item.publishDatePart() == obj.date);
                    })
                    obj.count = filteredList.length;
                    alldatesArray.push(obj);
                }
                return alldatesArray;
            },
            deferEvaluation: true
        }),

          hidePopup = function () {
              showPopup(false);
              stopPlayer(true);

              return true;
          },

   //news = ko.computed({
   //    read: function () {
   //        var pageNumber = pageIndex();
   //        var news = allNews();
   //        if (news.length > 0) {
   //            debugger;
   //        }
   //        var term = listHeader.searchText();
   //        var dic = _.chain(news)
   //                  .filter(function (item) {
   //                      if ($.trim(term).length > 3) {
   //                          return item.title().toLowerCase().indexOf(term.toLowerCase()) > -1;
   //                      }
   //                      if (selectedFilterId() > 0) {
   //                          return item.filterIds().indexOf(selectedFilterId()) > -1;
   //                      }
   //                      if (selectedDate() != '') {
   //                          return (item.publishDatePart() == selectedDate());
   //                      }
   //                      if (listHeader.calendar.fromDate() != '' && listHeader.calendar.toDate() != '') {
   //                          var fromDate = new Date(listHeader.calendar.from());
   //                          var toDate = new Date(listHeader.calendar.to());

   //                          fromDate.setHours(0);
   //                          fromDate.setMinutes(0);
   //                          fromDate.setSeconds(0);
   //                          fromDate.setMilliseconds(0);
   //                          toDate.setHours(23);
   //                          toDate.setMinutes(59);
   //                          toDate.setSeconds(59);
   //                          toDate.setMilliseconds(999);

   //                          fromDate = fromDate.getTime();
   //                          toDate = toDate.getTime();

   //                          var itemDate = new Date(item.publishDatePart()).getTime();

   //                          return (itemDate >= fromDate && itemDate <= toDate);
   //                      }
   //                      return true;
   //                  })
   //                     .slice(0, pageSize * pageNumber)
   //                    .groupBy(function (item) {
   //                        if (groupByFilter() == 1) { //time
   //                            return item.publishTimePart();
   //                        }
   //                        else if (groupByFilter() == 2) { //date
   //                            return item.publishDatePart();
   //                        }
   //                        else if (groupByFilter() == 3) {//location
   //                            return _.sortBy(_.uniq(item.locations()), function (item) {
   //                                return item;
   //                            });
   //                        }
   //                        else if (groupByFilter() == 4) {//category
   //                            return _.sortBy(_.uniq(item.parentCategories()), function (item) {
   //                                return item;
   //                            });
   //                        }
   //                        else {
   //                            return item.publishDatePart();
   //                        }
   //                    })
   //                    .map(function (value, key) {
   //                        return {
   //                            type: key.indexOf('2014') < 0 ? (key.replace(/,/g, ' | ')) : key, list: value, count: value.length
   //                        }
   //                    })
   //            .value();
   //        news = _.sortBy(news, function (item) {
   //            return item.publishDate;
   //        }).reverse();
   //        listHeader.count(news.length);
   //        console.log(dic);
   //        return [{ count: news.length, list: news, type : "" }];
   //        return dic;
   //    },
   //    deferEvaluation: true
   //}),


            setCurrentNews = function (data) {
                updatenews.setCurrentNews(data);
                router.navigateTo(config.views.v3.updatenews.url);
            },

            groupNewsByFilter = function (data) {
                groupByFilter(data);
            },

            dateClick = function (data) {
                clearallFilters();
                selectedDate(data.date);
            },

            selectedFiltereClick = function (filter) {
                if (filter.id == selectedFilterId()) {
                    presenter.toggleActivity(true);
                    clearallFilters();
                    selectedFilterId(-1);
                    presenter.toggleActivity(false);
                }
                else {
                    presenter.toggleActivity(true);
                    clearallFilters();
                    selectedFilterId(filter.id);
                    presenter.toggleActivity(false);
                }
            },

        clearallFilters = function () {
            listHeader.searchText('');
            selectedFilterId(-1);
            selectedDate('');
        },

        showMyNews = function () {
            listHeader.calendar.from(new Date());
            listHeader.calendar.to(new Date());
            clearallFilters();
            $('.active-filter').removeClass();
        },

        activate = function (routeData, callback) {
            setTimeout(function () { $('#shell-left-panel').hide(); }, 100);
            listHeader.heading(appdata.listHeader);
            rightTmplName = appdata.newsrightitem;
            messenger.publish.viewModelActivated({
                canleaveCallback: canLeave
            });
            checkOlderPollingObject();
            appdata.pollingRequired(true);
            populateNews();
            //myNewsPolling();
        },

        checkOlderPollingObject = function () {
            if (listHeader.calendar.from().length > 0 || listHeader.calendar.to().length > 0) {

            } else {
                var FromDate = moment(listHeader.calendar.from()).format('YYYY-MM-DD');
                var ToDate = moment(listHeader.calendar.to()).format('YYYY-MM-DD') + " 23:59:59";
                var searchText = listHeader.searchText();
                pollingRequestObj({
                    from: FromDate,
                    to: ToDate,
                    lastUpdate: '',
                    search: searchText
                });
                clonePollingRequestObjects();
            }
            
        },

        formatDate = function (date) {
            var d = new Date(date),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();

            if (month.length < 2) month = '0' + month;
            if (day.length < 2) day = '0' + day;

            return [year, month, day].join('-');
        },



        myNewsPolling = function () {
            if (appdata.pollingRequired()) {
                screenMessage("Please Wait");
                var maxDt = new Date();
                var maxDtStr = maxDt.toISOString();

                    var FromDate = moment(listHeader.calendar.from()).format('YYYY-MM-DD');
                    var ToDate = moment(listHeader.calendar.to()).format('YYYY-MM-DD') + " 23:59:59";
                    var searchText = listHeader.searchText();

                    var datObj = maxDtStr;

                    if (moment(ToDate).format('YYYY-MM-DD') == moment(new Date()).format('YYYY-MM-DD') && searchText.length == 0 && FromDate == lastPollingRequestObj().from && searchText == lastPollingRequestObj().search) {
                        FromDate = '';
                        ToDate = '';
                     
                        var gNews = dc.news.getAllLocal();
                        if (gNews && gNews.length > 0) {
                            maxDtStr = "";
                            maxDt.setDate(0);
                            for (var i = 0; i < gNews.length; i++) {
                                var item = gNews[i];
                                var date = new Date(item.creationDate);
                                if (date > maxDt) {
                                    maxDt = date;
                                    maxDtStr = item.creationDate;
                                }
                            }
                        }
                        datObj = maxDtStr
                    } else {
                        datObj = '';
                    }

                    var obj = {
                        AssignedTo: appdata.userId, LastUpdateDateStr: datObj, CreatedBy: appdata.userId, From: FromDate, To: ToDate, SearchText: searchText
                    };

                    
                    try {
                        if (obj.SearchText && obj.SearchText.trim().length > 0) {
                            obj.LastUpdateDateStr = '';
                        }
                    } catch (ex) {
                    }

                    if (obj.LastUpdateDateStr != '') {
                        FromDate = '';
                        ToDate = '';
                    }

                    pollingRequestObj({
                        from: FromDate,
                        to: ToDate,
                        lastUpdate: datObj,
                        search: searchText
                    });

                    if (pollingRequestObj().lastUpdate == '') {
                        if (pollingRequestObj().from == lastPollingRequestObj().from && pollingRequestObj().to == lastPollingRequestObj().to && pollingRequestObj().search == lastPollingRequestObj().search) {
                            populateNews(); //setTimeout(myNewsPolling, 5000);
                            return true;
                        } else {
                            dc.news.clearAllNews(true);
                            dc.resources.clearAllNews(true);
                            //console.log("cleared");
                            clearNews();
                            clonePollingRequestObjects();
                        }
                    }
                    
                    $.when(manager.news.MyNewsPolling(obj))
                                .done(function (data) {
                                    if (data.IsSuccess) {
                                         populateNews();
                                    } else {
                                        setTimeout(myNewsPolling, 5000);
                                    }
                                })
                                    .fail(function (data) {
                                        setTimeout(myNewsPolling, 5000);
                                    })
            }
        },


        getNewsFromServer = function (obj) {
            $.when(manager.news.LoadMyNewsList(obj))
                    .done(function (data) {
                        if (data.Data && data.Data.length > 0) {
                            screenMessage("Please Wait");
                            populateNews();
                        } else {
                            screenMessage("No data");
                        }
                    });
        },


        canLeave = function () {
            appdata.pollingRequired(false);
            return true
        },

        clonePollingRequestObjects = function () {
            lastPollingRequestObj({
                from: pollingRequestObj().from,
                to: pollingRequestObj().to,
                search: pollingRequestObj().search,
                lastUpdate: pollingRequestObj().lastUpdate
            });
        },


        populateNews = function () {
            var gNews = dc.news.getAllLocal();
            if (gNews != null && gNews.length > 0) {
                gNews = _.sortBy(gNews, function (item) {
                    return item.publishDate;
                }).reverse();
                totalLength(gNews.length - 1);
                var physicalElements = $(".latestNews.clearfix").length;
                if (gNews.length != allNews().length || allNews().length != physicalElements) {
                                    for (var i = 0; i < gNews.length ; i++) {
                                        var item = gNews[i].resources();
                                        if (item && item.length > 0) {
                                            gNews[i].defaultImageUrl = item[0].thumbImageUrl();
                                        }
                                    }
                                    news('');
                                    clearNews();
                                    listHeader.count(gNews.length);
                                    totalLength(gNews.length - 1);
                                    allNews(gNews);
                                    clonePollingRequestObjects();
                        news({
                            count: gNews.length, list: gNews, type: ""
                        });
                        
                }
                
                setTimeout(myNewsPolling, 5000);
            } else {
                if (pollingRequestObj().from == '' && pollingRequestObj().to == '') {
                    var obj = { rid: appdata.userId, pageCount: 20, startIndex: 0 };
                    setTimeout(getNewsFromServer(obj), 5000);
                } else {
                    clearNews();
                    news({ count: 0, list: [], type: "" });
                    screenMessage("No Data!");
                    setTimeout(myNewsPolling, 5000);
                }
            }
        },

        clearNews = function () {
            screenMessage("Please Wait");
            allNews('');
            totalLength(0);
            news({ count: 0, list: [], type: "" });
            listHeader.count(0);
        },

            init = function () {
                $('#shell-left-panel').hide();
                var role = helper.getCookie('role-id');
                if (role == 11 || role == 12) {
                    rightTmplName = "reporter.mycase";
                }
                else {
                    rightTmplName = "reporter.mynews";
                }

                listHeader.onHeadingClicked = showMyNews;
                listHeader.calendar.isSetLastWeek(true);
            },

            loadNextPage = function () {
                presenter.toggleActivity(true);
                var pageNumber = pageIndex();
                var totalNews = allNews().length;

                if (totalNews > pageNumber * pageSize) {
                    pageIndex(pageIndex() + 1);
                }
                presenter.toggleActivity(false);
            };


        init();

        return {
            activate: activate,
            setCurrentNews: setCurrentNews,
            news: news,
            imageFooterTmplName: imageFooterTmplName,
            rightTmplName: rightTmplName,
            contentFooterTmplName: contentFooterTmplName,
            contentTmplName: contentTmplName,
            alldates: alldates,
            filters: filters,
            selectedFilterId: selectedFilterId,
            showPopup: showPopup,
            CurrentDisplayedNews: CurrentDisplayedNews,
            totalLength: totalLength,
            hidePopup: hidePopup,
            selectedFiltereClick: selectedFiltereClick,
            groupNewsByFilter: groupNewsByFilter,
            allNews: allNews,
            dateClick: dateClick,
            e: e,
            clearallFilters: clearallFilters,
            loadNextPage: loadNextPage,
            templatePaging: templatePaging,
            listHeader: listHeader,
            screenMessage:screenMessage,
            stopPlayer: stopPlayer,
            canLeave: canLeave
        };
    });