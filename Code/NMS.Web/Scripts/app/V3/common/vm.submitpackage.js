﻿define('vm.submitpackage',
    ['config', 'enum', 'model.reportnews', 'router'],
    function (config, e, ReportNews, router) {

        var model = new ReportNews(
            'urduSP',
            'englishSP',
            ['.uploadVideo .editingSection .aside input[type=text],textarea'],
            e.NewsType.Package,
            'Submit Package',
            null,
            e.NewsFilterType.PublicReporter
            ),
            submitNews = function () {
                model.videouploadcheck = 'submitpackage';
                model.submit();
            },
        activate = function (routeData, callback) {

        };

        return {
            activate: activate,
            config: config,
            submitNews:submitNews,
            model: model,
            router: router
        };
    });