﻿define('vm.updatenews',
    ['config', 'datacontext', 'manager', 'enum', 'model.reportnews', 'control.templatepaging', 'router', 'vm.contentviewer', 'reporter.appdata' ],
    function (config, dc, manager, e, ReportNews, TemplatePaging, router, contentviewer, appdata) {

        var model = new ReportNews(
            'urduRN',
            'englishRN',
            ['.leftPanel .editor .textboxCss', '.reportNewsEditor', '.textboxCss .textboxHighLight'],
            e.NewsType.Story,
            'Update News',
            null,
            e.NewsFilterType.FieldReporter,
            'small'
            ),
            submitNews = function () {
                model.submitNewsFile();
                //dc.news.clearAllNews(true);
                //refillNews();
            },
            
            newsId,
            newsData,
            templatePaging = new TemplatePaging(true),
            showPopup = ko.observable(false),
            setThisCurrent = templatePaging.setThisCurrent,
            totalLength = ko.computed({
                read: function () {
                    return allNews().length - 1;
                },
                deferEvaluation: true

            }),

              refillNews = function () {
                  var obj = { rid: appdata.userId, pageCount: 20, startIndex: 0 };
                  $.when(manager.news.LoadMyNewsList(obj))
                      .done(function (data) {
                          debugger;
                      })
              },


            allNews = ko.computed({
                read: function () {
                    var list = dc.news.observableList();
                    var ticks = new Date().getTime();
                    return _.sortBy(_.filter(list, function (news) {
                        return (typeof news.parentNewsId == 'undefined');
                    }), function (item) {
                        return (ticks - new Date(item.publishTime).getTime());
                    });
                },
                deferEvaluation: true

            }),

              setCurrentContent = function (data) {
                  //model.displayCurrentThumb();

                  var filterItem = templatePaging.currentItem().resources().filter(function (e) {
                      return e.guid == data.id
                  })[0];
                  

                  var item = templatePaging;
                  item.currentItem().url = filterItem.url();
                  item.currentItem()["isProcessed"] = ko.observable(false);
                  item.currentItem().type = filterItem.type;
                  //model.popuptempPaging.currentItem()




                  model.popuptempPaging.currentItem(item.currentItem());
                  
                  model.showPopup(true);
                  //var data = templatePaging.currentItem();
                  //loadFromServer(data.id);
                  //var index = allNews().indexOf(data);
                  //templatePaging.list = allNews;
                  //templatePaging.currentIndex(index);
                  //templatePaging.currentItem(data);
                  //templatePaging.loadFromServer = loadFromServer;
                  //parentId('');
                  //parentId(data.id);
                 // showPopup(true);

                  //return true;
              },



             clickAttachment = function () {
                 if (appdata.currentUser().userType == e.UserType.FReporter) {
                     $('.editorUploader')[1].click();
                 }
                 else {
                     $('.editorUploader').click();
                 }
             },

           clickCameramen = function () {
               event.stopPropagation();
               $('.cameraman').toggleClass('active');
           },

            openMediaSelection = function () {
                contentviewer.mediaSelectionUrl(config.hashes.production.topicSelection + '?isiframe');
                $("#content-viewer-main").children('#contentviewer-overlay').addClass('warningOverlay');
                contentviewer.isMediaViewVisibleExplorer(true);
                appdata.isDefaultTab(!appdata.isDefaultTab());
                // document.getElementById('popupIframeFExp').contentWindow.myFunction();
            },

            setCurrentNews = function (data) {
                newsId = data.id;
                newsData = data;
                loadFromServer(data.id);
                var index = allNews().indexOf(data);
                templatePaging.list = allNews;
                templatePaging.currentIndex(index);
                templatePaging.currentItem(data);
                templatePaging.loadFromServer = loadFromServer;
                //model.isAssignment(data.assignedTo() && data.assignedTo() > 0);
                fillImages();
            },


            loadFromServer = function (id) {
                model.parentNewsId = id;
                var obj = { id: id };
                $.when(manager.news.GetNews(obj))
                    .done(function (responseData) {
                        updateModel(id);
                        if (responseData) {

                        } else {

                        }
                    })
                .fail(function () {
                });
            },


            updateModel = function (id) {
                if (!newsId) {
                    newsId = id;
                }

                //model.programs(dc.programs.getByChannelId(168)());
                model.reporters(dc.users.getAllLocal());

                var news = dc.news.getAllLocalByIds([newsId])[0];
                model.isAssignment(news.assignedTo() && news.assignedTo() > 0);

                model.fillViewModelProducer(news);
            };

        //model.currentItemReources = ko.computed({
        //    read: function () {
        //        if (templatePaging.currentItem()) {
        //            if (templatePaging.currentItem() && templatePaging.currentItem().resources) {
        //                fillImages();
        //                return templatePaging.currentItem().resources();
        //            }
        //            return [];
        //        }
        //    },
        //    deferEvaluation: false

        //});

        fillImages = function ()
        {
                var index = allNews().indexOf(templatePaging.currentItem());
                var res = templatePaging.currentItem().resources();
                var arr = [];
                for (var i = 0; i < res.length; i++) {
                    res[i].Guid = res[i].guid;
                }
                model.uploader().uploadedResources(res);
            
        },



        activate = function (routeData, callback) {
            $(".arrows ").hide();
        };

        return {
            activate: activate,
            config: config,
            templatePaging: templatePaging,
            totalLength: totalLength,
            allNews: allNews,
            setCurrentNews: setCurrentNews,
            submitNews: submitNews,
            model: model,
            e: e,
            setThisCurrent: setThisCurrent,
            router: router,
            appdata: appdata,
            clickAttachment: clickAttachment,
            clickCameramen: clickCameramen,
            setCurrentContent: setCurrentContent,
            showPopup:showPopup,
            openMediaSelection: openMediaSelection
        };
    });