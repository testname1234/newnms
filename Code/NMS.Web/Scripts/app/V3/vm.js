﻿define('vm',
    [
        'vm.shell',
        'vm.home',
		'vm.reportnews',
        'vm.submitpackage',
        'vm.reports',
        'vm.mynews',
        'vm.updatenews',
        'vm.contentviewer'
    ],

    function (shell, home, reportNews, submitPackage, reports, mynews, updatenews, contentviewer) {

        var vmDictionary = {};

        vmDictionary['#home-view'] = home;
        vmDictionary['.reportNews'] = reportNews;
        //vmDictionary['.reportNews'] = reportNews;
        vmDictionary['#submitpackage-view'] = submitPackage;
        vmDictionary['.myNewsPanelHeader'] = mynews;
        vmDictionary['#reports-view'] = reports;
        vmDictionary['#shell-top-nav-view'] = shell;
        vmDictionary['#shell-top'] = shell;
        vmDictionary['.updateNewsLeftRight'] = updatenews;
        vmDictionary['#content-viewer-main'] = contentviewer;

        return {
            dictionary: vmDictionary,
            reportNews: reportNews
        };
    });