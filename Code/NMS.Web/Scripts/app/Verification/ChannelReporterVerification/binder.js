﻿define('binder',
    [
        'jquery',
        'ko',
        'config',
        'enum',
        'presenter',
        'vm'
    ],
    function ($, ko, config, e, presenter, vm) {

        var
            ids = config.viewIds.channelreporterverification,

            bindPreLoginViews = function () {
                ko.applyBindings(vm.shell, getView(ids.shellTop));
                ko.applyBindings(vm.shell, getView(ids.shellTopNavView));
                ko.applyBindings(vm.shell, getView(ids.shellLeftView));
                ko.applyBindings(vm.home, getView(ids.homeView));
                ko.applyBindings(vm.pending, getView(ids.pendingView));
                ko.applyBindings(vm.newsdetails, getView(ids.getNewsView));
            },

            bindPostLoginViews = function () {
            },

            deleteExtraViews = function () {
            },

            bindStartUpEvents = function () {
            },

            getView = function (viewName) {
                return $(viewName).get(0);
            };

        return {
            bindPreLoginViews: bindPreLoginViews,
            bindPostLoginViews: bindPostLoginViews,
            bindStartUpEvents: bindStartUpEvents,
            deleteExtraViews: deleteExtraViews
        }

    }
);