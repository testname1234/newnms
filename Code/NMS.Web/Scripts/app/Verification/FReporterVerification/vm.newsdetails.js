﻿define('vm.newsdetails',
    ['config',
     'model.news',
     'model.bunch',
     'model.shortnews',
     'manager',
     'datacontext',
     'enum',
     'moment','presenter',
     'control.editorcontrol',
     'control.videoeditor',
     'model.reportnews',
     'control.tag',
     'control.templatepaging',
     'messenger', 'model.resource', 'control.audiorecorder',
     'router'
    ],
function (config, news, bunch, shortNews, manager, dc, e, moment, presenter, EditorControl, VideoEditor, ReportNews, Tag, PopuptempPaging, messenger, resourcemodel, audioRecorder, router) {
    var self = this,
        currentIndex = ko.observable(0),
        currentNews = ko.observable(new news()),
        currentBunch = ko.observable(new bunch()),
        allSelectedNews = ko.observable(false),
        checkBit = ko.observable(false),
        thisNews = ko.observable(new shortNews()),
        model = new ReportNews();
        currentNewsUpdates = ko.observableArray([]),
        selector = ko.observableArray(['.editor input[type=text]', '.jqte_editor']),
        coordinates = ko.observableArray([]),
        model.markMediaProcessed = null,
        isProcessed = ko.observable(),
        popuptempPaging = new PopuptempPaging(false),
        resourceLength = ko.observable(0),
        videoEditor = ko.observable(new VideoEditor()),
        newspaperurl = ko.observable('Newspapersample.jpg'),
        tmplName = ko.observable('loading.bar'),
        fieldtmplName = ko.observable('loading.bar'),
        categorySource = '/api/category/GetCategoryByTerm/',
        locationSource = '/api/Location/GetLocationByTerm/';
        resourceList = ko.observableArray([]),
        resourceListIndex = ko.observable(),
        uploaderflag= ko.observable(''),
        resourceEdit = null,
        deleteingfiles = [],
        deleteingupdatefiles = [],
        AudioRecorder = new audioRecorder(),
        seekval = 200,
        seekchangeVal = 0.1,
        v = $("#videoplayer").get(0),
        videoUrl = ko.observable(model.mediaEditor.src()),
        isPlaying = ko.observable(false),
        videoTime = ko.observable(),
        VDonlyerify = ko.observable(false),
        VDcampaigns = ko.observableArray([]),
        VDAllCampaigns = ko.observableArray([]),
        VDselectetdCampaign = ko.observable(null),
        VDselectedBrand = ko.observable(),
        VDallBrands = ko.observable(),
        forwardOption = ko.observable(),
        listFromTos = ko.observableArray([]),
        From = ko.observable(0),
        TOs = ko.observable(0),
        option1 = ko.observable("false"),
        VDslots = ko.observableArray([]),
        markTurn = ko.observable(0),

        allbunches = ko.computed({
            read: function () {
                return dc.bunches.observableList();
            },
            deferEvaluation: true

        }),

        allRelatedNews = ko.computed({
            read: function () {
                var result = ko.observableArray([]);
                var flag = false;
                for (var i = 0; i < currentBunch().relatedNews().length; i++) {
                    if (currentBunch().relatedNews()[i]) {
                        if (currentNews().id == currentBunch().relatedNews()[i].id) {
                            flag = true;
                            continue;
                        }
                    }
                    result.push(currentBunch().relatedNews()[i]);
                }
                if (flag) {
                    result.push(currentBunch().topNews());
                }
                return result();
            },
            deferEvaluation: true
        }),

        totalLength = ko.computed({
            read: function () {
                return allbunches().length - 1;
            },
            deferEvaluation: true

        }),

        packageVideo = ko.computed({
            read: function () {
                return thisNews().resources[0];
            }
        }),

        Videoselections = ko.computed({
            read: function () {
                var fr, to;
                var selections = {};
                var sel = [];
                var factor = videoEditor().factor;
                if (factor() != "" && currentNews().resourceEdits) {
                    for (var r = 0; r < currentNews().resourceEdits.FromTos.length; r++) {
                        fr = currentNews().resourceEdits.FromTos[r].From;
                        to = currentNews().resourceEdits.FromTos[r].To;
                        sel[r] = { From: fr, To: to, width: (to / factor()) - (fr / factor()), Left: fr / factor() };
                    }

                    selections = sel;
                    videoEditor().totalSelections(selections);
                    return [];

                }
            },

            deferEvaluation: false
        }),

        uploadingcontent = ko.computed({
            read: function () {
                var totalresourcefiles = [];
                if (thisNews().resources().length > 0)
                {
                    totalresourcefiles = thisNews().resources();
                    for (var i = 0; i < deleteingfiles.length ; i++) {
                        for (var a = 0; a < totalresourcefiles.length ; a++) {
                            if (deleteingfiles[i].id == totalresourcefiles[a].id) {
                                totalresourcefiles.splice(a, 1);
                                break;
                            }
                        }
                    }
                }
                //totalresourcefiles.remove(deleteingfiles);
                var resources = new Array(thisNews().uploader().mappedResources().length);
                for (var j = 0; j < thisNews().uploader().mappedResources().length; j++) {
                    resourceObj = new resourcemodel()
                    resourceObj.id = thisNews().uploader().mappedResources()[j].id;
                    resourceObj.newsId = '';
                    resourceObj.type = thisNews().uploader().mappedResources()[j].type;
                    resourceObj.creationDate = thisNews().uploader().mappedResources()[j].creationDate;
                        deleteingfiles.push(resourceObj);
                        totalresourcefiles.push(resourceObj);
                }
                thisNews().resources(totalresourcefiles);
                //thisNews().uploader().uploadedResources([]);
                return "";
            },

            deferEvaluation: true
        }),


        uploadingupdate = ko.computed({
                read: function () {
                    var totalresourceupdate = [];
                    for (var u = 0; u < thisNews().updates().length; u++) {
                        var thisNewsObj = thisNews().updates()[u];
                        if (thisNewsObj.resources().length > 0) {
                            totalresourceupdate = thisNewsObj.resources();
                            for (var i = 0; i < deleteingupdatefiles.length ; i++) {
                                for (var b = 0; b < totalresourceupdate.length ; b++) {
                                    if (deleteingupdatefiles[i].id == totalresourceupdate[b].id) {
                                        totalresourceupdate.splice(b, 1);
                                        break;
                                    }
                                }
                            }
                        }
                        for (var k = 0; k < thisNewsObj.uploader().mappedResources().length; k++) {
                            resourceObj2 = new resourcemodel();
                            resourceObj2.id = thisNewsObj.uploader().mappedResources()[k].id;
                            resourceObj2.newsId = '';
                            resourceObj2.type = thisNewsObj.uploader().mappedResources()[k].type;
                            resourceObj2.creationDate = thisNewsObj.uploader().mappedResources()[k].creationDate;

                            deleteingupdatefiles.push(resourceObj2);
                            totalresourceupdate.push(resourceObj2);
                        }
                        thisNews().updates()[u].resources(totalresourceupdate);
                    }
                    return "";
                },

                deferEvaluation: true
        }),


        pendingNewsCount = ko.computed({
            read: function () {
                var newsCount = 1;
                for (var i = 0; i < allRelatedNews().length; i++) {
                    if (allRelatedNews()[i].isVerified() == false) {
                        newsCount++;
                    }
                }
                return newsCount;
            }
        }),


        prevNews = function () {
            changRecord(0);
        },

        nextNews = function () {
            changRecord(1);
        },

        changRecord = function (data) {
            coordinates([{ left: 0, right: 0, top: 0, bottom: 0 }]);
            videoEditor().totalSelections([]);
            newspaperurl('../content/images/newspaperReporter/imagecropper.png');
            {
                arr = allbunches().sort(function (entity1, entity2) {
                    return entity1.publishTime() < entity2.publishTime() ? 1 : -1;
                });

                if (data == 3) {
                    //dc.bunches.observableList()[currentIndex()].topNews().isNullo = true;
                    //allbunches()[currentIndex()].isNullo = true;
                    //delete dc.bunches.observableList()[currentIndex()];
                    //delete allbunches()[currentIndex()];
                    var news = arr[currentIndex() + 1];
                    
                }
                if (data == 1) {
                    currentIndex(currentIndex() + 1);
                    var news = arr[currentIndex()];
                }
                if (data == 0) {
                    currentIndex(currentIndex() - 1);
                    var news = arr[currentIndex()];
                }

                {
                    for (var i = 0 ; i <= arr.length; i++) {
                        if (typeof (news) !== 'undefined')
                        { break; } else
                            if (data == 0) { currentIndex(currentIndex() - 1); }
                            else { currentIndex(currentIndex() + 1); }
                        news = arr[currentIndex()];
                    }
                    if (typeof (news) !== 'undefined') {
                        setCurrentNews(news.topNews());
                    }
                    else { config.logger.warning("No more news!"); }
                }

            }
          
  
            

            //  $('.thumbnail-big')
            //.stop(true, true)
            //.animate({
            //    width: "toggle",
            //    opacity: "toggle"
            //}, 3000);
            //$(".editingSection").fadeToggle(1500);
            


        },

        populateNewsItems = function () {
            var updateObj = new shortNews();
            AudioRecorder.NewsId = currentNews().id;
            AudioRecorder.UserId = 1;
            AudioRecorder.CommentTypeId = 2;
            AudioRecorder.resetcontroller();

            updateObj.id = currentNews().id;
            updateObj.title = currentNews().title;
            updateObj.selectedLanguage = currentNews().languageCode;
            updateObj.language = currentNews().languageCode;
            updateObj.newsDate = currentNews().newsDate;
            updateObj.isVerified = currentNews().isVerified;
            updateObj.defaultImage = currentNews().defaultImageUrl;
            updateObj.resources = currentNews().resources;
            updateObj.description = ko.observable(JSON.parse(ko.toJSON(currentNews().description)));
            updateObj.newsComments = currentNews().newsComments;                   //"2014-07-19 06:18:15 00:00"
            updateObj.newsDateTime(moment(currentNews().publishTime));
            updateObj.newsTypeId = currentNews().newsTypeId;
            updateObj.newsFilterType = currentNews().newsFilterType;
            updateObj.selectedCategory({ id: currentNews().categories()[0].CategoryId, name: currentNews().categories()[0].Category });
            updateObj.selectedLocation(currentNews().reportedLocations()[0]);
            var thisNewsTags = new Tag("/api/tag/gettagbyterm/");
            if (currentNews().tags().length > 0) {
                var tags = currentNews().tags();
                for (var i = 0; i < tags.length; i++) {
                    thisNewsTags.tags.push(tags[i]);
                }
            }
            updateObj.newsTag(thisNewsTags);


            var objlanguageToggle = new EditorControl();
            if (currentNews().languageCode == 'ur')
                objlanguageToggle.selectedLanguage = ko.observable('Urdu');
            else if (currentNews().languageCode == 'en')
                objlanguageToggle.selectedLanguage = ko.observable('English');
            else
                objlanguageToggle.selectedLanguage = ko.observable('English');
            objlanguageToggle.content = ko.observable(JSON.parse(ko.toJSON(currentNews().description)));
            objlanguageToggle.isContentSet(true);
            updateObj.languageToggle(objlanguageToggle);

            for (var u = 0; u < currentNews().updates().length; u++) {
                var thisNewsObj = currentNews().updates()[u];

                var obj = new shortNews();

                obj.id = thisNewsObj.id;
                obj.title = thisNewsObj.title;
                obj.selectedLanguage = thisNewsObj.languageCode;
                obj.language = thisNewsObj.languageCode;
                obj.newsDate = thisNewsObj.newsDate;
                obj.isVerified = thisNewsObj.isVerified;
                obj.defaultImage = thisNewsObj.defaultImageUrl;
                obj.resources = thisNewsObj.resources;
                obj.description = ko.observable(JSON.parse(ko.toJSON(thisNewsObj.description)));
                obj.newsComments = thisNewsObj.newsComments;
                obj.newsDateTime(moment(new Date(thisNewsObj.publishTime)).format('DD MMMM YYYY HH:MM:SS'));
                obj.newsTypeId = thisNewsObj.newsTypeId;
                obj.newsFilterType = thisNewsObj.newsFilterType;

                obj.selectedCategory({ id: thisNewsObj.categories()[0].CategoryId, name: thisNewsObj.categories()[0].Category });
                obj.selectedLocation(thisNewsObj.reportedLocations()[0]);


                var thisNewsTags = new Tag();
                if (currentNews().tags().length > 0) {
                    var tags = thisNewsObj.tags();
                    for (var i = 0; i < tags.length; i++) {
                        thisNewsTags.tags.push(tags[i]);
                    }
                }
                obj.newsTag(thisNewsTags);

                var objlanguageToggle = new EditorControl();
                objlanguageToggle.selectedLanguage = ko.observable('English');
                objlanguageToggle.content = ko.observable(JSON.parse(ko.toJSON(thisNewsObj.description())));
                objlanguageToggle.isContentSet(true);
                obj.languageToggle = objlanguageToggle;


                updateObj.updates().push(obj);
            }
            
            thisNews(updateObj);
        },

        submitComments = function (data) {
            PostInput = {
                NewsId: data.id,
                UserId: 2,
                CommentTypeId: 1,
                Comment: data.newsComments()
            };

            if (data.newsComments().length > 0) {
                $.when(manager.news.insertNewsComments(PostInput))
                    .done(function () {
                        config.logger.success("Comments has been submitted.");
                        data.newsComments('');
                        $(".comments .dropdown").hide();
                    })
                    .fail(function (ex) {
                        config.logger.error("Failed to submit the comments, please try again later.");
                    });
            } else {
                config.logger.warning("Commentbox is empty");
            }
        },

        clickVerifier = function (data) {
            if (!data.isVerified()) {
                data.isVerified(true);
            } else {
                data.isVerified(false);
            }

        },

        verifyAllNews = function () {
            if (allSelectedNews() == true)
                allSelectedNews(false);
            else
                allSelectedNews(true);


            ko.utils.arrayForEach(this.allRelatedNews(), function (thisNews) {
                if (allSelectedNews() == true)
                    thisNews.isVerified(true);
                else
                    thisNews.isVerified(false);
            });
        },

        setCurrentNews = function (data) {
            currentNews(data);
            checkBit(true);
            /////hiding elements while getting news /////////////
            //$(".editingSection").hide();
            $(".editingSection").css({ opacity: 0 });
            $(".newsupdates").css({ opacity: 0 });
            $(".thumbnail-big").hide();
            presenter.toggleActivity(true);
            /////////////////////////////////////////////////////
           
            //////////////////////////////////////////////////////
            var obj = { id: data.id, bunchId: data.bunchId };
            $.when(manager.news.getNewsWithUpdates(obj))
            .done(function (responseData) {
                currentNews(data);

                if (data.sourceTypeId == 4) {
                    fieldtmplName('news.newspaper');
                }
                else if (data.sourceTypeId == 10) {
                    fieldtmplName('news.channel');
                }
                else if (data.newsTypeId == e.NewsType.Package) { //  data.sourceTypeId == 7
                    fieldtmplName('news.package');
                } else {
                    fieldtmplName('news.view');
                    tmplName('news.view');
                }
                
                //videoEditor().src('http://10.3.18.21/videos/stream/ARY%20News/2015-02-17/1_$2015-02-17%2012-00-23.mp4');
                //if(videoEditor().src() !== "")
                //    activeVideo();
                if (currentNews().resourceEdits) {
                   
                    if (currentNews().resourceEdits.FromTos) {
                        From(currentNews().resourceEdits.FromTos[0].From);
                        TOs(currentNews().resourceEdits.FromTos[0].To);
                        videoEditor().src(currentNews().resourceEdits.Url);
                        //enterMark();
                        //if (videoEditor().src() !== "")
                        //    activeVideo();
                    }
                    if (currentNews().resourceEdits.Left || currentNews().resourceEdits.Right) {
                       
                        //  newspaperurl(null);
                        //From(currentNews().resourceEdits.Left);
                        //TOs(currentNews().resourceEdits.Right);
                        
                        newspaperurl(config.MediaServerUrl + 'getresource/' + currentNews().resourceEdits.Url);
                        //console.log( newspaperurl());
                        markpaper();
                    }
                }
                populateNewsItems();
                if (document.URL.toLowerCase().search("fieldreporter-verification") > 1)
                { uploaderflag('on'); }
                else
                { uploaderflag('off'); $('.editingSectionVideo .jqte_tool_22').hide(); };
                var bunch = dc.bunches.getLocalById(currentNews().bunchId);
                if (bunch) {
                    currentBunch(bunch);
                }

                /////fade in animation /////////////
                presenter.toggleActivity(false);
                $('.editingSection')
                        .stop(true, true)
                        .animate({
                        width: "linear",
                //    width: "toggle",
                        opacity: 1
                        }, 2000);     
                $(".thumbnail-big").fadeToggle(1500);
                $('.newsupdates')
                        .stop(true, true)
                        .animate({
                            width: "linear",
                            //    width: "toggle",
                            opacity: 1
                        }, 2000);
            
            })


        },

        displayCurrentThumb = function (data, r) {
            if (data && r.id) {
                var id = r.id;
                var list = data.resources();
                for (var i = 0; i < list.length; i++) {
                    if (id == list[i].id) {
                        var index = list.indexOf(list[i]);
                        popuptempPaging.list = data.resources;
                        popuptempPaging.currentItem(list[i]);
                        popuptempPaging.currentIndex(index);
                        resourceLength(list.length - 1);
                        $('.imgDetails .fileNumber').text(popuptempPaging.currentIndex()+1);
                    }
                }
            }
        },

        deleteThumbnail = function (a, b) {
            if (b) {
                var id = b.currentTarget.id;
                removeResourcesFromslider(id);
                $.when(manager.news.deletevideofile(id))
                            .done(function (responseData) {
                            })
                            .fail(function () {
                            });
            }
        },

        removeResourcesFromslider = function (id) {
            var list = currentNews().resources();
            for (var i = 0; i < list.length; i++) {
                if (list[i].id == id) {
                    list.remove(list[i]);
                }
            }
            var uploadlist = thisNews().uploader().uploadedResources();
            for (var i = 0; i < uploadlist.length; i++) {
                if (uploadlist[i].Guid == id) {
                    uploadlist.remove(uploadlist[i]);
                }
            }
            currentNews().resources(list);
            thisNews().uploader().uploadedResources(uploadlist);

            for (var u = 0; u < currentNews().updates().length; u++) {
                var thisNewsObj = currentNews().updates()[u];
                var updatelist = thisNewsObj.resources();
                var updateuploadlist = thisNews().updates()[u].uploader().uploadedResources();
                for (var i = 0; i < updatelist.length; i++) {
                    if (updatelist[i].id == id) {
                        updatelist.remove(updatelist[i]);
                    }
                }
                
                for (var i = 0; i < updateuploadlist.length; i++) {
                    if (updateuploadlist[i].Guid == id) {
                        updateuploadlist.remove(updateuploadlist[i]);
                    }
                    currentNews().updates()[u].resources(updatelist);
                    thisNews().updates()[u].uploader().uploadedResources(updateuploadlist);
                }
            }

        },

        markpaper = function () {
            var x = currentNews().resourceEdits.Left;
            var y = currentNews().resourceEdits.Top;
            var x1 = currentNews().resourceEdits.Right;
            var y1 = currentNews().resourceEdits.Bottom;
            coordinates([{ left: x, right: x1, top: y, bottom: y1 }]);
            //coordinates([{ left: 100, right: 300, top: 100, bottom: 500 }]);
        },

        markVerification = function () {
            if (thisNews().categories() && thisNews().locations() && thisNews().newsTag().tags().length>0) {
                MarkVerifyNewsInput = {
                    NewsIds: Array(),
                    bunchId: '',
                    IsVerified: true
                }
                MarkVerifyNewsInput.bunchId = currentNews().bunchId;
                MarkVerifyNewsInput.IsVerified = true;
                if (currentNews().isVerified())
                    MarkVerifyNewsInput.NewsIds.push(currentNews().id);

                for (var i = 0; i < allRelatedNews().length; i++) {
                    if (allRelatedNews()[i].isVerified())
                        MarkVerifyNewsInput.NewsIds.push(allRelatedNews()[i].id);
                }
                if (MarkVerifyNewsInput.NewsIds.length > 0) {
                    updateNewsChanges(MarkVerifyNewsInput);
                    config.logger.info('News is being updated');
                    
                }
                else
                    config.logger.error("No News selected");

            }

            else {
                config.logger.warning("Fields are incomplete");
            }
        },

        markRejection = function () {
            MarkVerifyNewsInput = {
                NewsIds: Array(),
                bunchId: '',
                IsVerified: false
            }

            MarkVerifyNewsInput.bunchId = currentNews().bunchId;
            MarkVerifyNewsInput.IsVerified = false;
            if (currentNews().isVerified())
                MarkVerifyNewsInput.NewsIds.push(currentNews().id);

                if (MarkVerifyNewsInput.NewsIds.length > 0) {
                    for (var i = 0; i < allRelatedNews().length; i++) {
                        if (allRelatedNews()[i].isVerified())
                            MarkVerifyNewsInput.NewsIds.push(allRelatedNews()[i].id);
                    }
                    //console.log(MarkVerifyNewsInput);
                    $.when(manager.news.markNewsVerified(MarkVerifyNewsInput))
                    .done(function () {
                        changRecord(3);
                        config.logger.success("News Rejected");
                })
                .fail(function () {
                    config.logger.error("Unable to Reject");
                });
            }
            else
                config.logger.error("No News selected");
        },

        updateNewsChanges = function (MarkVerifyNewsInput) {
        var categoryIds = [];
        if (thisNews().selectedCategory()) {
            categoryIds[0] = thisNews().selectedCategory().id;
        }
        var locationIds = [];
        if (thisNews().selectedLocation) {
            locationIds[0] = thisNews().selectedLocation().id;
        }
        var newsTagsArray = [];
        for (var i = 0 ; i < thisNews().newsTag().tags().length; i++) {
            var item = { _id: null, Tag: thisNews().newsTag().tags()[i] }
            newsTagsArray.push(item);
        }
        var resources = new Array(thisNews().resources().length);
        for (var j = 0; j < thisNews().resources().length; j++) {
            var resourceObject = {
                Guid: thisNews().resources()[j].id,
                ResourceTypeId: thisNews().resources()[j].type
            }
            resources[j] = resourceObject;
        }
        if (currentNews().resourceEdits) {
            if (currentNews().resourceEdits.FromTos) {
                currentNews().resourceEdits.FromTos = listFromTos(); //videoEditor().totalSelections();
            }
            if (coordinates().Left || coordinates().Right) {
                currentNews().resourceEdits.Top = coordinates().Top;
                currentNews().resourceEdits.Bottom = coordinates().Bottom;
                currentNews().resourceEdits.Left = coordinates().Left;
                currentNews().resourceEdits.Right = coordinates().Right;

            }
        }

        var filterTypeId = 0;
        if (thisNews().newsTypeId == 1)
            filterTypeId = 7;
        else if (thisNews().newsTypeId == 2)
            filterTypeId = 6;


        var requestData = {
            LanguageCode: thisNews().selectedLanguage,
            title: thisNews().title(),
            description: thisNews().languageToggle().content(),
            newsDateStr: new Date(thisNews().newsDateTime()).toISOString(),
            CategoryIds: categoryIds,
            LocationIds: locationIds,
            tags: newsTagsArray,
            filterTypeId: filterTypeId,
            reporterId: 1,
            newsTypeId: thisNews().newsTypeId,
            resources: resources,
            ResourceEdit: currentNews().resourceEdits,
            MarkNewsVerified: MarkVerifyNewsInput,
            LinkedNewsId: currentNews().id
        };

        $.when(manager.news.onsubmit(requestData))
            .done(function (responseData) {
                allSelectedNews(false);
                if (responseData.IsSuccess)
                    dc.resources.fillData(currentNews().resources, { newsId: responseData.Data._id });
                changRecord(3);
                config.logger.success("News Verified");

            })
            .fail(function () {
                config.logger.error("News Update failed");
            });
    },

        makeTime = function (time) {
            var t = time;
            videoTime(toHHmmssff(t));
        },

        toHHmmssff = function (t) {
            return parseInt(t / 86400) + (new Date(t % 86400 * 1000)).toUTCString().replace(/.*(\d{2}):(\d{2}):(\d{2}).*/, "$1:$2:$3") + " " + parseFloat(t % 1).toFixed(2);
        },

        getSeconds = function (time) {
    if (time) {
        tt = time.split(":");
        sec = tt[0] * 3600 + tt[1] * 60 + tt[2] * 1;
        return sec;
    }
},

        preciseCurrentTime = function () {
        var t;
        t = videoTime().substr(1, 8);
        return t;
    },

        ResetMarking = function () {
            $("#startPoint").remove();
            $("#endPoint").remove();
            markTurn(0);
            From(0);
            TOs(0);
            listFromTos([]);
        },

        activate = function () {
            config.verificationscreenflag = 'off';
            if (checkBit()) {
            }
            else {
                window.history.back("#/pending");
                router.navigateTo(config.hashes.fieldreporterverification.pending);
            }
            //messenger.publish.viewModelActivated({ canleaveCallback: canLeave });
        },


        canLeave = function () {
            videoEditor().totalSelections([]);
            //if (videoEditor()) {
            //    videoEditor().stop();
            //}
            return true
        };

    return {
        activate: activate,
        canLeave: canLeave,
        currentIndex: currentIndex,
        tmplName: tmplName,
        fieldtmplName:fieldtmplName,
        allSelectedNews: allSelectedNews,
        pendingNewsCount: pendingNewsCount,
        currentNews: currentNews,
        packageVideo: packageVideo,
        currentBunch: currentBunch,
        allRelatedNews: allRelatedNews,
        thisNews: thisNews,
        setCurrentNews: setCurrentNews,
        currentNewsUpdates: currentNewsUpdates,
        submitComments: submitComments,
        clickVerifier: clickVerifier,
        videoEditor: videoEditor,
        prevNews: prevNews,
        nextNews: nextNews,
        displayCurrentThumb: displayCurrentThumb,
        model: model,
        AudioRecorder: AudioRecorder,
        markVerification: markVerification,
        markRejection: markRejection,
        isProcessed: isProcessed,
        coordinates: coordinates,
        newspaperurl: newspaperurl,
        e: e,
        uploadingupdate:uploadingupdate,
        uploadingcontent:uploadingcontent,
        popuptempPaging: popuptempPaging,
        resourceLength: resourceLength,
        selector: selector,
        ResetMarking: ResetMarking
        }
});