﻿define('vm.pending',
    [
        'underscore',
        'jquery',
        'ko',
        'datacontext',
        'vm.newsdetails',
        'router',
        'config',
        'model',
        'presenter',
        'manager',
        'appdata',
        'presenter',
        'utils',
        'moment',
        'messenger',
        'enum',
        'model.tickerwritercontrol',
        'model.mapper'
    ],
    function (_, $, ko, dc, updatenews, router, config, model, presenter, manager, appdata, presenter, utils, moment, messenger, e, tickerWriterControl, mapper) {
        var logger = config.logger;

        var
            // Properties
            // ------------------------

            hashes = config.hashes.production,
            rightTmplName = 'fieldreporterverification.pendingnews',
            imageFooterTmplName = 'fieldreporterverification.pendingnews',
            contentFooterTmplName = 'fieldreporterverification.pendingnews',
            contentTmplName = 'fieldreporterverification.pendingnews',
            templates = config.templateNames,
            pageIndex = ko.observable(1),
            selectedNewsId = appdata.selectedNewsId,
            searchKeywords = ko.observable('').extend({ throttle: config.throttle }),
            sourceFilterControl = new model.SourceFilterControl(),
            calendarControl = new model.CalendarControl(),
            bunchArray = ko.observableArray(),
            pendingStoriesArray = ko.observableArray(),
            searchGroupName = ko.observable(),
            currentBunch = ko.observable(new model.Bunch()),
            tickerwritercontrol = new model.TickerWriterControl(),
            currentView = ko.observable('listView'),
            //selectedAudioClip = ko.observable(),
            //audioComments = ko.observable(),
            isUpdateScroll = ko.observable(false),
            isStoreScroll = ko.observable(false),
            viewBreakingNews = ko.observable(false),
            currentBreakingNews = ko.observable(),
            scrollPos = 0,

            // Computed Properties
            // ------------------------

            newsCount = ko.computed({
                read: function () {
                    var allFilters = dc.filters.getObservableList();
                    var filter = _.filter(allFilters, function (obj) {
                        return obj.id === 78;
                    });

                    if (filter && filter.length)
                        return filter[0].newsCount();
                },
                deferEvaluation: true
            }),
            newsGroups = ko.computed({
                read: function () {
                    var newsBunchArray = bunch();
                    if (newsBunchArray.length > 0) {
                        var arr = [];
                        for (var i = 0; i < appdata.dateCategories().length; i++) {
                            var tempObj = {};
                            var parsedDate = moment(appdata.dateCategories()[i]).calendar();
                            var bunchArray = _.filter(newsBunchArray, function (obj) {
                                return moment(obj.lastUpdateDate()).format('l') == moment(appdata.dateCategories()[i]).format('l');
                            });
                            tempObj["groupName"] = parsedDate;
                            tempObj["bunch"] = bunchArray;
                            if (bunchArray.length > 0)
                                arr.push(tempObj);
                        }
                        return arr;
                    } else {
                        return [];
                    }
                },
                deferEvaluation: true
            }),
            bunch = ko.computed({
                read: function () {
                    var arr = bunchArray().slice(0, pageIndex() * 5);
                    console.log('Bunch Display Count : ' + arr.length);
                    var currentGroup = '';
                    var groupArray = [];
                    for (var i = 0; i < arr.length; i++) {
                        var temp = moment(moment(arr[i].publishTime()).format('l')).calendar();
                        if (currentGroup !== temp) {
                            currentGroup = temp;
                            var tempGroup = {
                                groupName: currentGroup,
                                index: i
                            };
                            if (i > 0) {
                                groupArray.push(tempGroup);
                            }
                            else {
                                searchGroupName(tempGroup.groupName);
                            }
                        }
                    }
                    for (var i = 0; i < groupArray.length; i++) {
                        var tempObj = new model.Bunch();
                        tempObj.isGroupName = true;
                        tempObj.groupName = groupArray[i].groupName;
                        arr.splice(groupArray[i].index + i, 0, tempObj);
                    }
                    if (arr.length <= 0) {
                        searchGroupName('');
                    }
                    return arr;
                },
                deferEvaluation: true
            }),

            groupedPendingStories = ko.computed({
                read: function () {
                    lastUIRefreshDisplayTime = moment().toISOString();
                    var arr = [];
                    var timeCategories = utils.getTimeSlots(60);
                    if (pendingStoriesArray().length > 0) {
                        for (var i = 0; i < timeCategories.length; i++) {
                            var currentSlotStory = [];
                            for (var j = 0; j < pendingStoriesArray().length; j++) {
                                if ((new Date(pendingStoriesArray()[j].lastUpdateDate()).getHours()) == timeCategories[i].split(":")[0]) {
                                    currentSlotStory.push(pendingStoriesArray()[j]);
                                }
                            }
                            currentSlotStory = currentSlotStory.sort(function (storyA, storyB) {
                                return new Date(storyB.lastUpdateDate()) - new Date(storyA.lastUpdateDate());
                            });
                            var objectMap = {};
                            objectMap.GroupName = timeCategories[i];
                            objectMap.pendingStories = [];
                            objectMap.pendingStories = currentSlotStory;
                            arr.push(objectMap);
                        }
                    }
                    return arr;
                },
                deferEvaluation: true
            }),
            pendingStoryCount = ko.computed({
                read: function () {
                    var count = 0;
                    for (var i = 0; i < groupedPendingStories().length; i++) {
                        count += groupedPendingStories()[i].pendingStories.length;
                    }
                    return count;
                },
                deferEvaluation: true
            }),
            lastUIRefreshDisplayTime = ko.observable(moment(appdata.lastUIRefreshTime).fromNow()),

            // Methods
            // ------------------------

            setCurrentNews = function (data, isRelated) {
                var i = 0;
                if (!isRelated) {
                    for (i = 0; i < bunchArray().length; i++) {
                        if (bunchArray()[i].topNews())
                            if (bunchArray()[i].topNews().id == data.id)
                                break;
                    }

                } else {
                    for (i = 0; i < bunchArray().length; i++) {
                        if (bunchArray()[i])
                            if (bunchArray()[i].id == data.bunchId)
                                break;
                    }
                }
                var index = i;
                if (data.newsTypeId == e.NewsType.Story || data.newsTypeId == e.NewsType.Package) {

                    updatenews.currentIndex(index);
                    updatenews.setCurrentNews(data);

                    router.navigateTo(config.hashes.fieldreporterverification.getnews);
                }

            },

            initializeBunchGenerator = function () {
                setTimeout(function polling() {
                    $.when(manager.news.getFilteredPendingBunches(calendarControl.currentOption()))
                    .done(function (data) {
                        if (data) {
                            if (data.PageIndex)
                                pageIndex(data.PageIndex);
                            if (data.Bunches)
                                bunchArray(data.Bunches);
                        }
                        if (lastUIRefreshDisplayTime)
                            lastUIRefreshDisplayTime(moment(appdata.lastUIRefreshTime).fromNow());
                        setTimeout(polling, config.displayPollingInterval * 1000);
                    })
                    .fail(function () {
                        if (lastUIRefreshDisplayTime)
                            lastUIRefreshDisplayTime(moment(appdata.lastUIRefreshTime).fromNow());
                        setTimeout(polling, config.displayPollingInterval * 1000);
                    });
                }, config.displayPollingInterval * 1000);
            },

            selectNews = function (data) {
                if (appdata.currentUser().userType == e.UserType.TickerProducer || appdata.currentUser().userType == e.UserType.TickerWriter) {
                    fillTicker(data);
                }
                else
                    manager.news.selectNews(data);
            },
            //fillTicker = function (data) {
            //    var reqObj = [];
            //    reqObj.news = ko.observable(data);
            //    reqObj.TickerId = '1001';
            //    var ticker = mapper.ticker.fromDto(reqObj);
            //    //contentviewer.setCurrentTicker(ticker);
            //},
            toggleStoryFilter = function (data) {
                manager.production.toggleStoryFilter(data);
                pageIndex(1);
                appdata.lpStartIndex = 0;
                manager.news.setDataChangeTime();
            },
            toggleFilter = function (data, isExtraFilter) {
                manager.news.toggleFilter(data, isExtraFilter);
                pageIndex(1);
                appdata.lpStartIndex = 0;
                manager.news.getMorePendingNews();
                manager.news.setDataChangeTime();
            },
            resetFilters = function () {
                appdata.selectedSourceFilters([78]);
                appdata.selectedCategoryFilters([]);
                appdata.selectedExtraFilters([]);
                appdata.isAllNewsFilterSelected(true);
                searchKeywords('');
                $('#top-rated-filter').removeClass('active');
                $('.navigationNews li ul li').removeClass('active-filter');
                $('.nav-step1 li a').removeClass('active-category-filter');
                manager.news.setDataChangeTime();
            },
            setCurrentBunchFilters = function (data) {
                if (currentBunch().selectedFilters().indexOf(data) == -1)
                    currentBunch().selectedFilters.push(data);
                else {
                    var index = currentBunch().selectedFilters.indexOf(data);
                    currentBunch().selectedFilters.splice(index, 1);
                    console.log(currentBunch().selectedFilters());
                }
            },
            setCurrentBunch = function (data) {
                currentBunch(data);
                if (currentBunch().showfilters())
                    currentBunch().showfilters(false);
                else
                    currentBunch().showfilters(true);

                manager.news.getNewsBunch(currentBunch());
            },
            loadNextPage = function () {
                presenter.toggleActivity(true);
                var filters = [{ FilterId: 80 }];
                $.when(manager.news.getMoreVerificationPending(filters))
                    .done(function() {
                        var pIndex = pageIndex() + 1;
                        pageIndex(pIndex);
                        presenter.toggleActivity(false);
                    })
                    .fail(function () {
                        presenter.toggleActivity(false);
                    });
            },
            switchView = function myfunction() {
                if (appdata.currentUser().userType == e.UserType.TickerProducer || appdata.currentUser().userType == e.UserType.TickerWriter) {
                    if (router.currentHash().indexOf('home') != -1) {
                        router.navigateTo(config.hashes.production.myTicker);
                    }
                }
                else {
                    if (router.currentHash().indexOf('home') != -1) {
                        router.navigateTo(config.hashes.production.headlineUpdates);
                    }
                    else if (router.currentHash().indexOf('headlineUpdates') != -1) {
                        router.navigateTo(config.hashes.production.home);
                    }
                }
            },
            setCalendarDate = function () {
                appdata.lpToDate = calendarControl.toDate();
                appdata.lpFromDate = calendarControl.fromDate();

                appdata.NLEfromDate(calendarControl.fromDateDisplay());
                appdata.NLEtoDate(calendarControl.toDateDisplay());
                manager.news.setDataChangeTime();
            },
            subscribeEvents = function () {
                searchKeywords.subscribe(function (value) {
                    appdata.searchKeywords = value;
                    appdata.lpStartIndex = 0;
                    manager.news.getMorePendingNews();
                    manager.news.setDataChangeTime();
                });
                pageIndex.subscribe(function (value) {
                    appdata.pageIndex = value;
                    manager.news.setDataChangeTime();
                });
            },
            //displayAudioToolTip = function (data) {
            //    selectedAudioClip('../../wildlife.mp3');
            //    audioComments(data.title());
            //    console.log(data);
            //},
            //closeAudioToolTip = function () {
            //    selectedAudioClip('');
            //},
            resetContentFilter = function () {
                $('#allNews').removeClass('active');
                $('#allNews').addClass('active');
                $('#news').removeClass('active');
                $('#packages').removeClass('active');
                $('#verified-filter').removeClass('active');
                $('#recommended-filter').removeClass('active');
                $('#top-rated-filter').removeClass('active');
                $('#top-executed-filter').removeClass('active');
                $('#most-recent-filter').removeClass('active');

                appdata.selectedExtraFilters([]);
                toggleFilter(-1, true);
            },
            searchEventNews = function (data) {
                if (data) {
                    if (data.isSelected()) {
                        data.isSelected(false);
                        appdata.searchKeywords = '';
                        manager.news.setDataChangeTime();
                    } else {
                        _.filter(sourceFilterControl.events(), function (obj) {
                            return obj.isSelected(false);
                        });
                        data.isSelected(true);
                        appdata.searchKeywords = data.searchTags;
                        manager.news.setDataChangeTime();
                    }
                }
            },
            setCurrenBreakingNews = function (data) {
                //contentviewer.setCurrentAlertContent(data, 2);
            },
            hideBreakingNews = function () {
                viewBreakingNews(false);
                currentBreakingNews('');
            },
            setFullScreenMode = function () {
                var el = document.body;
                var requestMethod = el.requestFullScreen || el.webkitRequestFullScreen
              || el.mozRequestFullScreen || el.msRequestFullScreen;
                if (requestMethod) {
                    requestMethod.call(el);

                } else if (typeof window.ActiveXObject !== "undefined") {
                    var wscript = new ActiveXObject("WScript.Shell");
                    if (wscript !== null) {
                        wscript.SendKeys("{F11}");
                    }
                }
            },
            storeScrollPosition = function () {
                appdata.producerScrollTop($('#home-view').children('div').children('.mCSB_container').css('top'));
            },
            setScrollPosition = function () {
                scrollPos = appdata.producerScrollTop();
                scrollPos = scrollPos.replace('px', '');
                $('#home-view').children('div').children('.mCSB_container').css('top', parseInt(scrollPos));
            },
            hideNextPrevNLE = function () {
                $(".prev").css({ 'display': 'none' });
                $(".next").css({ 'display': 'none' });
            },

            activate = function (routeData, callback) {
                messenger.publish.viewModelActivated({ canleaveCallback: canLeave });

                if (appdata.currentUser().userType === e.UserType.Producer || appdata.currentUser().userType === e.UserType.HeadlineProducer || appdata.currentUser().userType === e.UserType.TickerWriter || appdata.currentUser().userType === e.UserType.TickerProducer) {
                    setScrollPosition();
                }
            },
            canLeave = function () {
                storeScrollPosition();
                return true;
            },
            init = function () {

                sourceFilterControl.currentFilter(-1);
                sourceFilterControl.isPopulated(1);
                //sourceFilterControl.showExtraFilters(true);
                //sourceFilterControl.isMediaSectionVisible(true);
                
                calendarControl.currentOption('lastmonth');

                //if (appdata.currentUser().userType == e.UserType.Producer || appdata.currentUser().userType === e.UserType.HeadlineProducer || appdata.currentUser().userType === e.UserType.TickerWriter || appdata.currentUser().userType === e.UserType.TickerProducer)
                calendarControl.isNewsCropVisible(true);


                subscribeEvents();

                //toggleFilter(450, true);
                toggleFilter(-1, true);

                initializeBunchGenerator();
            };

        return {
            activate: activate,
            canLeave: canLeave,
            appdata: appdata,
            bunch: bunch,
            setCurrentNews: setCurrentNews,
            //selectNews: selectNews,
            templates: templates,
            init: init,
            sourceFilterControl: sourceFilterControl,
            calendarControl: calendarControl,
            hashes: hashes,
            loadNextPage: loadNextPage,
            toggleFilter: toggleFilter,
            toggleStoryFilter: toggleStoryFilter,
            searchKeywords: searchKeywords,
            //selectedNewsId: selectedNewsId,
            newsGroups: newsGroups,
            newsCount: newsCount,
            resetFilters: resetFilters,
            lastUIRefreshDisplayTime: lastUIRefreshDisplayTime,
            searchGroupName: searchGroupName,
            pendingStoryCount: pendingStoryCount,
            groupedPendingStories: groupedPendingStories,
            setCalendarDate: setCalendarDate,
            currentBunch: currentBunch,
            setCurrentBunch: setCurrentBunch,
            setCurrentBunchFilters: setCurrentBunchFilters,
            currentView: currentView,
            //selectedAudioClip: selectedAudioClip,
            //displayAudioToolTip: displayAudioToolTip,
            //closeAudioToolTip: closeAudioToolTip,
            //audioComments: audioComments,
            resetContentFilter: resetContentFilter,
            isUpdateScroll: isUpdateScroll,
            isStoreScroll: isStoreScroll,
            searchEventNews: searchEventNews,
            setCurrenBreakingNews: setCurrenBreakingNews,
            viewBreakingNews: viewBreakingNews,
            currentBreakingNews: currentBreakingNews,
            hideBreakingNews: hideBreakingNews,
            setFullScreenMode: setFullScreenMode,
            switchView: switchView,
            e: e,
            tickerwritercontrol: tickerwritercontrol,
            utils: utils
        };
    });