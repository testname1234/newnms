﻿define('vm.shell',
     [
         'ko',
         'config',
         'router',
         'datacontext',
         'model',
         'manager',
         'vm.home',
         'appdata'
     ],
    function (ko, config, router, dc, model, manager, home, appdata) {

        var
            // Properties
            //-------------------
            menuHashes = config.hashes,
            templates = config.templateNames,
            sourceFilterControl = new model.SourceFilterControl(),

            // Computed
            //-------------------

            shellLeftVisible = ko.computed({
                read: function () {
                    if (router.currentHash() === config.hashes.fieldreporterverification.home)
                        return true;
                    else
                        return false;
                },
                deferEvaluation: true
            }),
             toggleFilter = function (data) {
                 home.toggleFilter(data);
             },

            currentHash = ko.computed({
                read: function () {
                    return router.currentHash();
                },
                deferEvaluation: true
            }),

            activeHash = ko.computed({
                read: function () {

                    if (currentHash() == config.hashes.fieldreporterverification.home) {
                        return config.hashes.fieldreporterverification.home;
                    } else if (currentHash() == config.hashes.fieldreporterverification.reports) {
                        return config.hashes.fieldreporterverification.reports;
                    } else if (currentHash() == config.hashes.fieldreporterverification.pending) {
                        return config.hashes.fieldreporterverification.pending;
                    } else if (currentHash() == config.hashes.fieldreporterverification.getnews) {
                        return config.hashes.fieldreporterverification.getnews;
                    }
                },
                deferEvaluation: true
            }),

            // Methods
            //-------------------

            activate = function (routeData) {
                
            },

             logOut = function () {

                 window.location.href = '/login#/index';
                 document.cookie = config.sessionKeyName + '=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';
                 document.cookie = config.roleid + '=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';
                 document.cookie = config.userid + '=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';
             },
            init = function () {

            };

        return {
            currentHash: currentHash,
            activeHash: activeHash,
            activate: activate,
            menuHashes: menuHashes,
            shellLeftVisible: shellLeftVisible,
            templates: templates,
            init: init,
            toggleFilter: toggleFilter,
            sourceFilterControl: sourceFilterControl,
            appdata: appdata,
            logOut:logOut
        };
    });
