﻿define('vm',
    [
        'vm.shell',
        'vm.home',
        'vm.pending',
        'vm.newsdetails'
    ],

    function (shell, home, pending, newsdetails) {
        return {
            shell: shell,
            home: home,
            pending: pending,
            newsdetails: newsdetails
        };
    });