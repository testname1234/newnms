﻿define('vm.reports',
    [],
    function (config) {

        var
            // Properties
            // ------------------------


            // Computed Properties
            // ------------------------


            // Methods
            // ------------------------

            activate = function (routeData, callback) {

            },
            canLeave = function () {
                return true;
            };

        return {
            activate: activate,
            canLeave: canLeave
        };
    });