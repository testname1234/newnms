﻿define('vm.sequence',
    ['datacontext'],
    function (dc) {
        var
            newsInBucket = ko.computed({
                read: function () {
                    return dc.newsBucket();
                },
                write: function (value) {
                    dc.newsBucket(value);

                },
                deferEvaluation: true
            }),
            activate = function (routeData, callback) {

            },
            canLeave = function () {
                return true;
            };

        return {
            activate: activate,
            canLeave: canLeave,
            newsInBucket: newsInBucket
        };
    });