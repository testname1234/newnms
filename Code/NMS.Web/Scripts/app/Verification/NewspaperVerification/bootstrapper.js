﻿define('bootstrapper',
    [
        'config',
        'binder',
        'route-config',
        'manager',
        'mytimer',
        'vm',
        'presenter',
        'appdata'
    ],
    function (config, binder, routeConfig, manager, timer, vm, presenter, appdata) {

        var
             run = function () {
                 config.dataserviceInit();
                 appdata.extractUserInformation();
                 binder.bindPreLoginViews();
                 binder.bindStartUpEvents();
                 routeConfig.register();
                 presenter.toggleActivity(true);


                 var filterArray = [{ FilterTypeId: 4, FilterId: 28 }];
                 $.when(manager.news.verificationLoadInitialData(filterArray))
                     .done(function (responseData) {
                         binder.bindPostLoginViews();

                         vm.pending.init();

                         $.when(manager.categories.loadCategories()).done(function () {
                             $.when(manager.locations.loadLocations()).done(function () { }).fail(function () { });
                         }).fail(function () { });

                         amplify.subscribe(config.eventIds.onLogIn, function (data) {

                         });

                         timer.start(filterArray);

                         setTimeout(function () {
                             presenter.toggleActivity(false);
                         }, 100);
                     })
                     .fail(function (responseData) {
                         presenter.toggleActivity(false);
                     });


             };

        return {
            run: run
        };
    }
);