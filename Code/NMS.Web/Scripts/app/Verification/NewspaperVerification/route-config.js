﻿define('route-config',
    ['config', 'router', 'vm'],
    function (config, router, vm) {
        var
            logger = config.logger,

            register = function () {

                var routeData = [

                    // Home routes
                    {
                        view: config.viewIds.newspaperverification.homeView,
                        routes: [
                            {
                                isDefault: true,
                                route: config.hashes.newspaperverification.home,
                                title: 'Home',
                                callback: vm.home.activate,
                                group: '.route-top'
                            }
                        ]
                    },

                    // Pending News routes
                    {
                        view: config.viewIds.newspaperverification.pendingView,
                        routes:
                            [
                                {
                                    route: config.hashes.newspaperverification.pending,
                                    title: 'Pending News',
                                    callback: vm.pending.activate,
                                    group: '.route-filter'
                                }
                            ]
                    },

                    // News Detail routes
                    {
                        view: config.viewIds.newspaperverification.getNewsView,
                        routes:
                            [{
                                route: config.hashes.newspaperverification.getnews,
                                title: 'News Details',
                                callback: vm.newsdetails.activate,
                                group: '.route-filter'
                            }]
                    },

                    // Invalid routes
                    {
                        view: '',
                        route: /.*/,
                        title: '',
                        callback: function () {
                            logger.error(config.toasts.invalidRoute);
                        }
                    }
                ];

                for (var i = 0; i < routeData.length; i++) {
                    router.register(routeData[i]);
                }

                // Crank up the router
                router.run();
            };


        return {
            register: register
        };
    });