﻿define('vm.shell',
     [
         'ko',
         'config',
         'router',
         'datacontext',
         'model',
         'manager',
         'vm.pending',
         'appdata'
     ],
    function (ko, config, router, dc, model, manager, pending, appdata) {

        var
            // Properties
            //-------------------
            menuHashes = config.hashes,

            templates = config.templateNames,

            sourceFilterControl = new model.SourceFilterControl(),

            // Computed
            //-------------------

            channels = ko.computed({
                read: function () {
                    return dc.channels.getObservableList();
                },
                deferEvaluation: true
            }),

            programs = ko.computed({
                read: function () {
                    var allPrograms = dc.programs.getObservableList();
                    var arr = _.filter(allPrograms, function (program) {
                        return program.channelId === appdata.currentChannel();
                    });
                    return arr;
                },
                deferEvaluation: true
            }),

            newsInBucket = ko.computed({
                read: function () {
                    return dc.newsBucket();
                },
                deferEvaluation: true
            }),

            shellLeftVisible = ko.computed({
                read: function () {
                    if (router.currentHash() === config.hashes.fieldreporterverification.pending ||
                        router.currentHash().indexOf('topicselection') !== -1)
                        return true;
                    else
                        return false;
                },
                deferEvaluation: true
            }),
             toggleFilter = function (data) {
                 pending.toggleFilter(data);
             },

            currentHash = ko.computed({
                read: function () {
                    return router.currentHash();
                },
                deferEvaluation: true
            }),

            activeHash = ko.computed({
                read: function () {

                    if (currentHash().indexOf('topselection') !== -1) {
                        return config.hashes.production.topicSelection;
                    } else if (currentHash() == config.hashes.fieldreporterverification.home) {
                        return config.hashes.fieldreporterverification.home;
                    } else if (currentHash() == config.hashes.fieldreporterverification.reports) {
                        return config.hashes.fieldreporterverification.reports;
                    } else if (currentHash() == config.hashes.fieldreporterverification.pending) {
                        return config.hashes.fieldreporterverification.pending;
                    } else if (currentHash() == config.hashes.fieldreporterverification.getnews) {
                        return config.hashes.fieldreporterverification.getnews;
                    }
                },
                deferEvaluation: true
            }),

            // Methods
            //-------------------

            activate = function (routeData) {
            },
              logOut = function () {

                  window.location.href = '/login#/index';
                  document.cookie = config.sessionKeyName + '=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';
                  document.cookie = config.roleid + '=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';
                  document.cookie = config.userid + '=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';
              },
            init = function () {
                programs.subscribe(function (val) {
                    //console.log(val);
                });
            };

        return {
            currentHash: currentHash,
            activeHash: activeHash,
            activate: activate,
            menuHashes: menuHashes,
            shellLeftVisible: shellLeftVisible,
            channels: channels,
            newsInBucket: newsInBucket,
            templates: templates,
            init: init,
            toggleFilter: toggleFilter,
            sourceFilterControl: sourceFilterControl,
            appdata: appdata,
            programs: programs,
            logOut: logOut

        };
    });
