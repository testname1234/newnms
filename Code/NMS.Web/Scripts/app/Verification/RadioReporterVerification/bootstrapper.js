﻿define('bootstrapper',
    [
        'config',
        'binder',
        'route-config',
        'manager',
        'mytimer',
        'vm',
        'presenter'
    ],
    function (config, binder, routeConfig, manager, timer, vm, presenter) {

        var
             run = function () {
                 config.dataserviceInit();

                 binder.bindPreLoginViews();
                 binder.bindStartUpEvents();
                 routeConfig.register();
                 presenter.toggleActivity(true);



                 $.when(manager.news.verificationLoadInitialData())
                     .done(function (responseData) {
                         binder.bindPostLoginViews();

                         vm.pending.init();

                         $.when(manager.categories.loadCategories()).done(function () {
                             $.when(manager.locations.loadLocations()).done(function () { }).fail(function () { });
                         }).fail(function () { });

                         amplify.subscribe(config.eventIds.onLogIn, function (data) {

                         });

                         timer.start();

                         setTimeout(function () {
                             presenter.toggleActivity(false);
                         }, 100);
                     })
                     .fail(function (responseData) {
                         presenter.toggleActivity(false);
                     });


             };

        return {
            run: run
        };
    }
);