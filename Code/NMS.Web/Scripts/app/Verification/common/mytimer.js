﻿define('mytimer',
    ['manager', 'moment', 'config'],
    function (manager, moment, config) {
        var
            interval = config.pollingInterval,
                   start = function (filterArray) {
                       setTimeout(function longPolling() {
                           //if (document.URL.toLowerCase().search("getnews") == -1) {
                           if (config.verificationscreenflag == 'on') {
                             
                               $.when(manager.news.verificationPolling(filterArray))
                               .done(function () {
                                   setTimeout(longPolling, interval * 1000);
                               })
                               .fail(function () {
                                   setTimeout(longPolling, interval * 1000);
                               });

                           } else { setTimeout(longPolling, interval * 1000); }

                       }

                       , interval * 1000);
                   }

        return {
            start: start
        };
    });