// JavaScript Document
// Global Variables
var eas = 'easeOutExpo',
  loader = '';
// Main Function Sarts-----------------------------------------------------
$(function () {
	
               
    //$('.bdl').click(function () {

    //    window.location.href = '/login#/index';
    //    document.cookie = config.sessionKeyName + '=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';
    //    document.cookie = config.roleid + '=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';
    //    document.cookie = config.userid + '=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';

    //});

	$('textarea').click(function(e){
		$(this).focus();	
	})
	$('.rightSection .cancellationBox').click(function (e) {
	    e.preventDefault();
	    e.stopPropagation();
	});
	
	//$('.rightSection').delegate('a.reject', 'hover', function () {
	//    $('.rightSection .cancellationBox').fadeIn();
	//});
	
    // NEWS NAV FUNCTION

	
	    $(".newsNav > ul > li").hover(function () {
	        $(this).find('ul:first').stop(true,true).fadeIn();
	    },function() {
	        $(this).find('ul:first').stop(true,true).fadeOut();
	    });
	
	$(".contentNew:first").show();
	$(".newsPakages li:first").addClass("active");
	$(".newsPakages li").click(function(){
		$(".newsPakages li").removeClass("active");
		$(this).addClass("active");
	});
	
	//$(".tabs li:first").addClass("active");
	$(".tabs li").click(function(){
		$(".tabs li").removeClass("active");
		$(this).addClass("active");
	});
	
	$(".allStories .icon-allStories").click(function(){ var $this=$(this);
		$this.parents('.storiesColume').stop(true,true).animate({width:'300px'},200,'linear');
		$this.parents('.allStories').find('.storiesIcons').stop(true,true).animate({'right' : 0,},300,'linear');
	});
	$(".allStories .sep").click(function(){
		$(this).parents('.storiesColume').stop(true,true).animate({width:'30px'},200,'linear');
		$(this).parents('.allStories').find('.storiesIcons').stop(true,true).animate({'right' : -195,},300,'linear');
	});
	
	$('.allStories .storiesForm').hide();
	$('.allStories').hover(function(){
		$(this).find('.storiesForm').stop(true,true).fadeIn();	
	},function(){		
		$(".allStories .storiesIcons").stop(true,true).animate({'right' : -195,},300,'linear');
		$(this).find('.storiesForm').stop(true,true).fadeOut();
	});
  
  //Resolution Fix
  resolutionfix();
  
  // Popup SlideShow
  var nmbrFiles = $('.xPop .mediaSection ul li').length;
  $('.imgDetails .total').text(nmbrFiles);
  $('body').delegate('.xPop a.next', 'click', function () {
    var imgActive = $(this).closest('.imageSection').children('.slideImg.active');
    var img = $(this).closest('.imageSection').children('.slideImg');
    if (imgActive.next().is('.slideImg')) {
      imgActive.removeClass('active');
      imgActive.next('.slideImg').addClass('active');
    } else {
      imgActive.removeClass('active');
      img.first('.slideImg').addClass('active');
    }
    fileNumber(this);
    
  });

  $('body').delegate('.xPop a.prev', 'click', function () {
    var imgActive = $(this).closest('.imageSection').children('.slideImg.active');
    var img = $(this).closest('.imageSection').children('.slideImg');
    if (imgActive.prev().is('.slideImg')) {
      imgActive.removeClass('active');
      imgActive.prev('.slideImg').addClass('active');
    } else {
      imgActive.removeClass('active');
      img.last('.slideImg').addClass('active');
    }
    fileNumber(this);
    //fileNumber(imgActive);
  });
  $('body').delegate('.xPop .mediaSection ul li', 'click', function () {
    var img = $('.imageSection').children('.slideImg');
    var imgActive = $(this).index() + 1;
    img.removeClass('active');
    $('.slideImg[data-number="' + imgActive + '"]').addClass('active');
    $('.imgDetails .fileNumber').text(imgActive);
  });
  $('body').delegate('thumbnail-big', 'click', function () {
      $('.slideImg[data-number="' + imgActive + '"]').addClass('active');
      fileNumber(this);
  });
  

  
  // Left Menu
  /*$(".nav-step1").hover(
    function () {
      $(this).stop(true, true).animate({
        width: '230px'
      }, 200, 'linear');
    },
    function () {
      $(this).stop(true, true).animate({
        width: '50px'
      }, 200, 'linear');
    });
  $("ul.nav-step1 li").on('mouseover', function () {
    $(this).find('ul').parent('li').addClass("active");
    $(this).find('ul').stop(true, true).animate({
      left: '50px'
    }, 200, 'linear');
    $(this).on('mouseleave', function () {
      $(this).find('ul').parent('li').removeClass("active");
      $(this).find('ul').stop(true, true).animate({
        left: '-340px'
      }, 200, 'linear');
    });
  });*/
  
  // Video Upload Submit Package
  $('.uploadTxt').delegate('input[type=button]', 'click', function () {
    $(this).parent('.uploadTxt').hide();
    $('.videoUpload .uploadprogress').show();
    var progressBarWidth = $('.videoUpload .uploadprogress .progress .bar').data('width');
    $('.videoUpload .uploadprogress .progress .bar').animate({
      'width': progressBarWidth
    }, 1000);
    setTimeout(function () {
      if ($('.videoUpload .uploadprogress .progress .bar').width('100%')) {
        $('.videoUpload .uploadprogress').hide();
        $('.videoUpload i').hide();
        $('.videoUpload .video').fadeIn();
      }
    }, 2000);
  });
  $('.video').delegate('a.close', 'click', function () {
    $(this).parent('.video').hide();
    $('.videoUpload i').show();
    $('.uploadTxt').fadeIn();
  })
  
  
  // Next Prev News Update Report
  $('.outerArrowRight').click(
    function () {
      $(".latestCol.right").stop(true, true).animate({
        right: '0px'
      }, 200, 'linear');
      $(".latestCol.right").show();
    });
  $('.latestCol.right a.close, .outerArrowLeft').click(function () {
    $(".latestCol.right").hide();
    $(".latestCol.right").stop(true, true).animate({
      right: '-253px'
    }, 200, 'linear');
  });
  $('.outerArrowLeft').click(
    function () {
      $(".latestCol.left").stop(true, true).animate({
        left: '0px'
      }, 200, 'linear');
      $(".latestCol.left").show();
    });
  $('.latestCol.left a.close, .outerArrowRight').click(function () {
    $(".latestCol.left").hide();
    $(".latestCol.left").stop(true, true).animate({
      left: '-253px'
    }, 200, 'linear');
  });
  
  
  // Delete Media Files
  $('.filesList li').delegate(' i.delete', 'click', function () {
    //$(this).closest('li').remove();
  });
  
  
  // New Report Date Time
  var myDate = new Date();
  var hours = myDate.getHours();
  var minutes = myDate.getMinutes();
  var ampm = hours >= 12 ? 'pm' : 'am';
  hours = hours % 12;
  hours = hours ? hours : 12; // the hour '0' should be '12'
  minutes = minutes < 10 ? '0' + minutes : minutes;
  var strTime = hours + ':' + minutes + ' ' + ampm;
  var monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
  var currentDate = myDate.getDate() + ' ' + (monthNames[myDate.getMonth()]) + ' ' + myDate.getFullYear();
  $(".options.dateTime div.head > span:not(.time)").text(currentDate);
  $(".options.dateTime div.head > span.time").text(', ' + strTime);
  $('.timepicker input.hour').val(hours);
  $('.timepicker input.min').val(minutes);
  if (ampm = 'am') {
    $('.timepicker select>option:eq(0)').attr('selected', true);
    $('.timepicker .xSelect p').text('pm')
  } else {
    $('.timepicker select>option:eq(1)').attr('selected', true);
    $('.timepicker .xSelect p').text('am')
  }
  $('.timepicker input[type=button]').click(function () {
    var hours = $('.timepicker input.hour').val();
    hours = hours < 10 ? '0' + hours : hours;
    var minutes = $('.timepicker input.min').val();
    minutes = minutes < 10 ? '0' + minutes : minutes;
    var amPm = $('.timepicker select.ampm').val();
    $(".options.dateTime div.head > span.time").text(', ' + hours + ':' + minutes + ' ' + amPm);
  });
  
  
  // Submit Report Splash
  $('.ReportNews').click(function () {
    $('.splash').show();
    $('.overlay').show();
    $('.latestNews.added').hide();
    setTimeout(function () {
      $('.splash').animate({
        'top': -500
      }, 200);
      $('.splash').fadeOut();
      $('.overlay').fadeOut();
      window.location.href = "news.asp";
    }, 1000);
  });
  
  
  // Open Calendar on Submit Report
  $(".options.dateTime div.head").click(function () {
    $(".options.dateTime .optionsList .datepicker").datepicker({
      showOn: "both",
      inline: true,
      showOtherMonths: true,
      dayNamesMin: ['S', 'M', 'T', 'W', 'T', 'F', 'S'],
      nextText: ' ',
      prevText: ' ',
      dateFormat: "dd MM yy",
      onSelect: function (dateText, inst) {
        var date = $(this).val();
        $(".options.dateTime div.head > span:not(.time)").text(date);
      }
    });
  });
  
  
  // Header Top DropDowns
  $('html').click(function () {
    $('header .topnav > li').siblings().children('ul').hide();
    $('header .topnav > li').siblings().children('div').hide();
    $('.headQuen > ul > li').siblings().children('ul').hide();
    $(".slideDown li").find('ul').stop(true, true).slideUp('fast');
    resetCount = 0;
  });
  $('header .topnav > li').click(function (event) {
    event.stopPropagation();
    $('header .topnav > li').siblings().children('ul, div').hide();
    $(this).children('ul').show();
    if ($(this).hasClass('alerts')) {
      $(this).parents().siblings().children('> ul').hide();
      $(this).children('div').show();
      $('header .topnav > li.alerts > div > ul').scrollTop(1);
    }
  });
  
  // Login Checkbox
  xCheckbox('.lform');
  $('.deleteAll').delegate('a', 'click', function () {
    $('.tagsinput span.tag').remove();
  });
  
  $('.icon-check').click(function(){
  	$(this).addClass('active');
  });
  // Editor Tools Report
  $('.editor-tools').delegate('i.attachment', 'click', function (e) {
    e.preventDefault();
    $('.uploadprogress > div').show();
    var progressBarWidth = $('.uploadprogress .progress .bar').data('width');
    $('.uploadprogress .progress .bar').animate({
      'width': progressBarWidth
    }, 1000);
    setTimeout(function () {
      $('.uploadprogress > div').hide();
      $('.filesList').show();
    }, 2000);
    setTimeout(function () {
        $(".innerwrap").mCustomScrollbar("scrollTo", "bottom");
    }, 2200);
  });

  
  $('.editor-tools').delegate('i', 'click', function () {
    var style = $(this).data('style');
    var attribute = $(this).data('attribute');
    if ($(this).hasClass('active')) {
      $('.editor textarea').css(attribute, '');
      $(this).removeClass('active');
    } else {
      $('.editor textarea').css(attribute, style);
      $('.editor-tools i').removeClass('active');
      $(this).addClass('active');
    }
  });
  
  
  // Keyboard Language Selection
  $('.slctkeybrd').delegate('i', 'click', function (e) {
    $(this).closest('.slctkeybrd').children('.languages').slideToggle();
    $(".languages ul").mCustomScrollbar('destroy');
    $(".languages ul:not(.mCustomScrollbar)").mCustomScrollbar({
      autoDraggerLength: true,
      autoHideScrollbar: true,
      scrollInertia: 100,
      advanced: {
          updateOnBrowserResize: true,
          autoScrollOnFocus: false,
        updateOnContentResize: true
      }
    });
    e.stopPropagation();
  });
  $('.slctkeybrd').delegate('.languages', 'click', function (e) {
    e.stopPropagation();
  });
  $('body, html').click(function () {
    //$('.languages, .dropdown, .rightSection .cancellationBox').hide();
    $('.optionsList').slideUp();
  });



  // Keyboard Language Selection
  $('.comments').delegate('i', 'click', function (e) {
    $(this).closest('.comments').children('.dropdown').slideToggle();
    e.stopPropagation();
  });
  $('.comments').delegate('.dropdown', 'click', function (e) {
    e.stopPropagation();
  });

    // DATE PICKER FUNCTION
  var to = new Date();
  var from = new Date(to.getTime() - 1000 * 60 * 60 * 24 * 14);
  $('#datepicker-calendar').DatePicker({
      inline: true,
      date: [from, to],
      calendars: 3,
      mode: 'range',
      current: new Date(to.getFullYear(), to.getMonth() - 1, 1),
      onChange: function (dates, el) {
          // update the range display
          $('#date-range-field span').text(
            dates[0].getDate() + ' ' + dates[0].getMonthName(true) + ', ' +
            dates[0].getFullYear() + ' - ' +
            dates[1].getDate() + ' ' + dates[1].getMonthName(true) + ', ' +
            dates[1].getFullYear());
          $('.calenderLeft #from').val(
            dates[0].getDate() + ' ' + dates[0].getMonthName(true));
          $('.calenderLeft #to').val(
            dates[1].getDate() + ' ' + dates[1].getMonthName(true));
      }
  });
  $("#datepicker").datepicker();
  $("#datepicker").parent().parent().hide();
  $(".ui-state-default").live("mouseup", function () {
      $("small.monthDate").text($(this).text() + "." + $(".ui-datepicker-month", $(this).parents()).text() + "." + $(".ui-datepicker-year", $(this).parents()).text());
      $(this).closest('#datepicker').parent().parent().hide();
  });

    // News Calendar
  //$('.newsCalender').delegate('.GraphCalendar', 'mouseover', function (e) {
  //    $(this).closest('li').children('.calenderMenu').fadeIn();
  //    $('.modifier ').fadeOut();
  //    e.stopPropagation();
  //});
  $('body, html').click(function () {
      $('.calenderMenu ').fadeOut();
  });
  // Aside dropdowns
  $('aside .options').delegate('div.head', 'click', function (e) {
    $(this).parent().addClass('active');
    $(".optionsList").mCustomScrollbar('destroy');
    $(".optionsList:not(.mCustomScrollbar)").mCustomScrollbar({
      autoDraggerLength: true,
      autoHideScrollbar: true,
      scrollInertia: 100,
      advanced: {
          updateOnBrowserResize: true,
          autoScrollOnFocus: false,
        updateOnContentResize: true
      }
    });
    $(this).closest('.options').siblings('.options').children('.optionsList').slideUp();
    $(this).closest('.options').children('.optionsList').slideToggle();
    $(this).toggleClass('active');
    e.stopPropagation();
  });
  $('.options').delegate('.optionsList', 'click', function (e) {
    e.stopPropagation();
  });
  $('.optionsList ul').delegate('li', 'click', function (e) {
    var headingValue = $(this).closest('.options').children('div.head').children('span');
    var value = $(this).children('label').text();
    if (!$(this).hasClass('active')) {
      $(this).children('input[type=checkbox]').attr("checked", "true");
      $(this).addClass('active');
      if (headingValue.text() == '' || headingValue.text() == ' ') {
        headingValue.prepend(value);
      } else {
        headingValue.prepend(value + ', ');
      }
    } else {
      $(this).children('input[type=checkbox]').removeAttr("checked");
      $(this).removeClass('active');
      headingValue.html(headingValue.text().replace(value + ', ', ''));
      headingValue.html(headingValue.text().replace(', ' + value, ''));
      headingValue.html(headingValue.text().replace(value, ''));
    }
    e.stopPropagation();
  });
  
  // xPop Hit
  $('.mainwrap').delegate('[data-xpop]', 'click', function (e) {
    xpopup($(this));
    //customscroller(".updateReport .latestNews", 100);
    e.preventDefault();
  });
  preloader({
    id: '#Loader',
    path: 'assets/images/loader/loader_s5.png',
    sprites: 5,
    size: 80,
    speed: 500
  });

}); // Main Function End


$(window).load(function () {
  resolutionfix()
  customscroller(".latestCol > div", 100);
  customscroller(".mediaSection > div:not(.heading)", 100);
  //customscroller(".updateReport:not(.xPop) .latestNews", 100);
  customscroller(".topnav .scrollBar", 100);
  customscroller(".allRelated > ul", 100);
  customscroller(".tagsinput", 100);
  customscroller("ui-autocomplete:not(.mCustomScrollbar)", 100);
  


});

/* Text box input field for IE */
if($.browser.msie&&$.browser.version<=9){$('input[type="text"], input[type="password"], input[type="email"], textarea').each(function(){var txtvalue=$(this).attr('placeholder');$(this).attr('value',txtvalue)});$('input[type="text"], input[type="password"], input[type="email"]').focus(function(){var maintxt=$(this).attr('placeholder');if($(this).attr('value')==maintxt){$(this).attr('value','')}}).blur(function(){var maintxt=$(this).attr('placeholder');if($(this).attr('value')==''){$(this).attr('value',maintxt)}})}
/* Text box input field for IE */

$(window).resize(function () {
  resolutionfix();
});





function customscroller(containername, scrollspeed) {
  $(containername).mCustomScrollbar({
      autoDraggerLength: true,
      autoHideScrollbar: true,
    scrollInertia: scrollspeed,
    advanced: {
        updateOnBrowserResize: true,
        autoScrollOnFocus: false,
      updateOnContentResize: true
    }
  });
}

function resolutionfix() {
  var ht = $(window).height() - $('header.header').outerHeight() - $('.singlebar-menu').outerHeight() - 20;
  $('.leftnav .nav-step1, .leftnav .nav-step1 ul').height(ht);
  $('.newsList').height(ht - $('.newsNav').height() - 17);
  $('.contentSection').height('auto');
  $('.latestCol > div').height(ht - 40);
  $('.updateReport .latestNews').height($('.editingSection').outerHeight() + 30);
  //$('.rightSection').height($('.contentSection').height());
  
  
}
/* File: preloader.js */
function preloader(e) {
  for (var t = 0; t < arguments.length; t++) {
    var n = e.id;
    path = e.path;
    spr = e.sprites;
    size = e.size;
    dur = e.speed;
    if (!dur) dur = 100;
    interme(n, path, spr, size, dur)
  }
}

function interme(e, t, n, r, i) {
  setInterval(function () {
    var i = parseInt($(e + " img").css("left")) - r;
    if (!i) i = "0px";
    else if (i == -r * n) i = 0;
    if (!$(e).html()) {
      $(e).css({
        width: r + "px",
        height: r + "px",
        padding: "0px",
        overflow: "hidden",
        position: "relative"
      }).append('<img src="' + t + '" alt="prelaoder" style="height:' + r + "px; position:absolute; top:" + i + '; left:0px;" />')
    } else {
      $(e + " img").css("left", i + "px")
    }
  }, i)
}

function xCheckbox(tgt) {
  // Checkboxes
  $(tgt + ' input[type=checkbox]').each(function () {
    if (!$(this).parent().hasClass('xCheckbox')) {
      $(this).fadeTo(0, 0).wrap('<div class="xCheckbox" style="position:relative;" />');
      $(this).css({
        'position': 'absolute',
        'width': '100%',
        'height': '100%',
        'left': '0',
        'top': '0',
        'z-index': '10'
      });
      $(this).on('change', function () {
        if ($(this).is(':checked')) {
          $(this).parent().addClass('checked');
        } else {
          $(this).parent().removeClass('checked');
        }
      });
    }
  });
}

function url(value) {
  window.location.href = value;
}

function fileNumber(obj) {
    var imgnum = $(obj).closest('.imageSection').children('.slideImg.active').attr('data-number');
    $('.imgDetails .fileNumber').text(imgnum);
}

function xpopup(e, f) {
  var tgt, ht = 0,
    wd = 0,
    htm = 0,
    wdm = 0,
    ovc, wdb = 0,
    htb = 0,
    scrl = 0,
    resz = 0,
    tgtb = '',
    tot;
  if (e == 'close') {
    $('#overlay').fadeOut(300, '', function () {
      $(this).remove();
    });
    $('body > .xPop').fadeOut(1000, eas, function () {
      $(this).remove();
    });
  } else {
    if (f == 'self' || f == 'moveto') {
      tgt = e;
    } else {
      tgt = e.data('xpop');
      htm = e.data('height');
      wdm = e.data('width');
      ovc = e.data('overlayclose');
      ht = htm;
      wd = wdm;
    }
    if (tgt == 'iframe') {
      var lnk = e.attr('href');
      $('body').append('<div id="overlay" /><div class="xPop iframe"><div class="popInner"><iframe frameborder="0" src="' + lnk + '" style="position:absolute; width:100%; height:100%; left:0; top:0;"></iframe></div></div>');
      tgt = $('body > .iframe.xPop');
      $('#overlay').fadeIn(1000);
      tgt.show().fadeTo(0, 0);
      tgt.append(loader);
      tgt.width(50).height(50).fadeTo(1000, 1, eas);
      tgt.children('.popInner').hide();
      tgt.find('iframe').load(function () {
        frameheight('.xPop.iframe');
        if (!wd) {
          wdb = tgt.find('iframe').contents().find('body > div').width() + 30;
          if (!wdb) {
            wd = $(window).width() - 100
          } else {
            wd = wdb;
          }
        }
        if (!ht) {
          htb = tgt.find('iframe').contents().find('body > div').height() + 30;
          if (!htb) {
            ht = $(window).height() - 100
          } else {
            ht = htb
          }
        }
        if (ht >= $(window).height()) {
          ht = $(window).height() - 100;
          scrl = 1;
          resz = 1;
        }
        $('#temploader').fadeOut(1000, '', function () {
          $(this).remove();
        });
        tgt.children('.popInner').css({
          width: (wd),
          height: (ht)
        });
        tgt.animate({
          width: (wd + 20),
          height: (ht + 20)
        }, 1000, eas, function () {
          if (tgt.children('close').length == 0) {
            tgt.append('<div class="close" />');
          }
          tgt.children('.popInner').fadeIn();
          if (scrl == 1) {
            tgt.addClass('scrl');
            if (htb) {
              tgt.animate({
                width: (wd + 10)
              });
              tgt.find('iframe').css({
                height: (htb),
                'position': 'relative'
              });
              if (!tgt.children('.popInner').children().hasClass('mCustomScrollBox')) {
                tgt.children('.popInner').mCustomScrollbar({
                  autoDraggerLength: true,
                  autoHideScrollbar: true,
                  scrollInertia: 800,
                  advanced: {
                    updateOnBrowserResize: true,
                    updateOnContentResize: true
                  }
                });
              }
            }
          }
          tgtb = '#overlay,.xPop .close';
          if (!ovc) {
            tgtb = '.xPop .close';
          }
          $(tgtb).on('click', function () {
            $('#overlay').fadeOut(300, '', function () {
              $(this).remove();
            });
            tgt.fadeOut(1000, eas, function () {
              $(this).remove();
            });
          });
        });
      });
    } else if (tgt == 'image') {
      var lnk = e.attr('href');
      $('body').append('<div id="overlay" /><div class="xPop imageDv"><div class="popInner"><img src="' + lnk + '"></div></div>');
      tgt = $('body > .imageDv.xPop');
      $('#overlay').fadeIn(1000);
      tgt.show().fadeTo(0, 0);
      tgt.append(loader);
      tgt.width(50).height(50).fadeTo(1000, 1, eas);
      tgt.children('.popInner').fadeTo(0, 0).css('position', 'absolute;');
      tgt.find('img').load(function () {
        if (!wd) {
          wdb = tgt.find('.popInner img').outerWidth() + 30;
          if (!wdb) {
            wd = $(window).width() - 100
          } else {
            wd = wdb;
          }
        }
        if (!ht) {
          htb = tgt.find('.popInner img').outerHeight() + 30;
          if (!htb) {
            ht = $(window).height() - 100
          } else {
            ht = htb;
          }
        }
        if (ht >= $(window).height()) {
          ht = $(window).height() - 100;
          scrl = 1;
          resz = 1;
        }
        tgt.find('.popInner img').width(wd).height(ht);
        $('#temploader').fadeOut(1000, '', function () {
          $(this).remove();
        });
        tgt.children('.popInner').css({
          width: (wd),
          height: (ht)
        });
        tgt.animate({
          width: (wd),
          height: (ht)
        }, 1000, eas, function () {
          if (tgt.children('close').length == 0) {
            tgt.append('<div class="close" />');
          }
          tgt.children('.popInner').css('position', 'relative').fadeTo(300, 1);
          if (scrl == 1) {
            tgt.addClass('scrl');
            if (htb) {
              tgt.animate({
                width: (wd + 10),
                height: (ht + 20)
              });
              tgt.find('.popInner img').removeAttr('style').width(wd);
              tgttgt.find('.popInner img').css({
                height: (htb),
                'position': 'relative'
              });
              if (!tgt.children('.popInner').children().hasClass('mCustomScrollBox')) {
                tgt.children('.popInner').mCustomScrollbar({
                  autoDraggerLength: true,
                  autoHideScrollbar: true,
                  scrollInertia: 800,
                  advanced: {
                    updateOnBrowserResize: true,
                    updateOnContentResize: true
                  }
                });
              }
            }
          }
          tgtb = '#overlay,.xPop .close';
          if (!ovc) {
            tgtb = '.xPop .close';
          }
          $(tgtb).on('click', function () {
            $('#overlay').fadeOut(300, '', function () {
              $(this).remove();
            });
            tgt.fadeOut(1000, eas, function () {
              $(this).remove();
            });
          });
        });
      });
    } else {
      tgtb = $(tgt + '.xPop');
      if (f == 'moveto') {
        $('body > .xPop').remove();
      } else {
        $('body').append('<div id="overlay" />');
      }
      tgtb.clone().appendTo('body');
      tgt = $('body > ' + tgt + '.xPop');
      if (!tgt.children().hasClass('popInner')) {
        tgt.wrapInner('<div class="popInner" />');
      }
      if (f != 'moveto') {
        $('#overlay').fadeIn(1000);
      }
      tgt.show().fadeTo(0, 0);
      tgt.addClass('t');
      wdb = tgt.outerWidth();
      htb = tgt.outerHeight();
      tgt.removeClass('t');
      if (!wd) {
        wd = wdb
      }
      if (!ht) {
        ht = htb
      }
      if (ht >= $(window).height()) {
        ht = $(window).height() - 100;
        scrl = 1;
        resz = 1;
      }
      if (ht < htb) {
        scrl = 1;
      }
      tgt.append(loader);
      tgt.width(50).height(50).fadeTo(1000, 1, eas);
      tgt.children('.popInner').hide();
      $('#temploader').fadeOut(1000, '', function () {
        $(this).remove();
      });
      tgt.stop(true, true).animate({
        width: wd,
        height: ht
      }, 1000, eas, function () {
        if (tgt.children('close').length == 0) {
          tgt.append('<div class="close" />');
        }
        tgt.children('.popInner').fadeIn();
        if (scrl == 1) {
          tgt.addClass('scrl');
          tgt.children('.popInner').css({
            width: (wd - 10),
            height: (ht - 20)
          });
          tgt.children('.popInner').mCustomScrollbar({
            autoDraggerLength: true,
            autoHideScrollbar: true,
            scrollInertia: 800,
            advanced: {
              updateOnBrowserResize: true,
              updateOnContentResize: true
            }
          });
        }
        if (resz == 1) {
          $(window).resize(function () {
            var wht = $(window).height(),
              wwd = $(window).width();
            /*			if((wht-20)<=htm){
						tgt.css({height:(wht-100)});
						tgt.children('.popInner').css({height:(wht-100)});
						if(!tgt.children('.popInner').children().hasClass('mCustomScrollBox')){
							tgt.children('.popInner').mCustomScrollbar({
								autoDraggerLength:true,
								autoHideScrollbar:true,
								scrollInertia:800,
								advanced:{
									updateOnBrowserResize: true,
									updateOnContentResize: true
							}});
						}
					} else if((wht-20)>htm){
						tgt.css({width:wd,height:ht});
						tgt.children('.popInner').css({width:(wd-10),height:(ht-20)});
						tgt.children('.popInner').mCustomScrollbar("destroy");
					}*/
          });
        }
        tot = setInterval(function () {
          var mht = $('body > .xPop').height(),
            iht = $('body > .xPop .popInner').height();
          //console.log(mht,iht);
          if (mht != iht) {
            $('body > .xPop').stop(true, true).animate({
              height: iht
            });
          }
          if (!$('body > .xPop').html()) {
            clearTimeout(tot);
          }
        }, 100);
        tgtb = '#overlay,.xPop .close';
        if (ovc) {
          tgtb = '.xPop .close';
        }
        $(tgtb).on('click', function () {
          $('#overlay').fadeOut(300, '', function () {
            $(this).remove();
          });
          tgt.fadeOut(1000, eas, function () {
            $(this).remove();
          });
          clearTimeout(tot);
        });
      });
    }
  }
}

