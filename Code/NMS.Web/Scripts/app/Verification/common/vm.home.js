﻿define('vm.home',
    ['ko', 'datacontext', 'router', 'config', 'model'],
    function (ko, dc, router, config, model) {

        var
            // Properties
            // ------------------------

            hashes = config.hashes.fieldreporterverification,

            // Computed Properties
            // ------------------------


            activate = function (routeData, callback) {

            },

            canLeave = function () {
                return true;
            },

            init = function () {

            };

        return {
            activate: activate,
            canLeave: canLeave,
            init: init,
            hashes: hashes

        };
    });