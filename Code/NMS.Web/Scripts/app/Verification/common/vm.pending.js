﻿define('vm.pending',
    [
        'datacontext',
        'vm.newsdetails',
        'config',
        'model',
        'router',
        'presenter',
        'appdata',
        'manager',
        'enum'
    ],
    function (dc, updatenews, config, model, router, presenter, appdata, manager, e) {

        var
            hashes = config.hashes.fieldreporterverification,
            rightTmplName = 'fieldreporterverification.pendingnews',
            imageFooterTmplName = 'fieldreporterverification.pendingnews',
            contentFooterTmplName = 'fieldreporterverification.pendingnews',
            contentTmplName = 'fieldreporterverification.pendingnews',
            pageIndex = ko.observable(1),
            searchText = ko.observable('').extend({ throttle: config.throttle }),
            templates = config.templateNames,
            pendingNewsCount = ko.observable(0),
            sourceFilterControl = new model.SourceFilterControl(),
            bunchArray = ko.observableArray([]),
            calendarControl = new model.CalendarControl(),
            filters = [],
            
            bunch = ko.computed({
                read: function () {
                  
                    var arr;
                        arr = bunchArray().slice(0, pageIndex() * 5);
                        var temp = [];
                        temp.push({ groupName: null, bunch: [] });
                        
                        for (var i = 0; i <= arr.length - 1 ; i++) {
                            if (typeof(arr[i]) !== 'undefined') {
                                if (arr[i].isNullo !== true && arr[i].topNews()) {
                                    temp[0].bunch.push(arr[i]);
                                }
                            } 
                        } pendingNewsCount(temp[0].bunch.length);
                        
                    return temp;
                },
                deferEvaluation: true
            }),

                 search = ko.computed({
                     read: function () {
                         var sear = searchText();
                         if (sear.length > 2) {
                             appdata.searchKeywords = sear;
                             manager.news.setDataChangeTime();
                         }
                         else {
                             appdata.searchKeywords = '';
                             manager.news.setDataChangeTime();
                         }
                     },
                     deferEvaluation: true

                 }),

         
            setCalendarDate = function () {
                appdata.lpToDate = calendarControl.toDate();
                appdata.lpFromDate = calendarControl.fromDate();
                manager.news.setDataChangeTime();
                },

            initializeBunchGenerator = function () {
               
                setTimeout(function polling() {
                    $.when(manager.news.getFilteredBunches())
                        .done(function (data) {
                          
                            if (data) {
                                if (data.PageIndex)
                                    pageIndex(data.PageIndex);
                                if (data.Bunches)
                                    bunchArray(data.Bunches);
                            }
                            
                            setTimeout(polling, config.displayPollingInterval * 1000);
                        })
                        .fail(function () {
                            setTimeout(polling, config.displayPollingInterval * 1000);
                        });

                }, config.displayPollingInterval * 1000);
            },

            toggleFilter = function (data, isExtraFilter) {

                manager.news.toggleFilterVerification(data, isExtraFilter);
               // manager.news.toggleFilterVerification(data);
                pageIndex(1);
                appdata.lpStartIndex = 0;
                manager.news.setDataChangeTime();
            },

            resetFilters = function () {
                //appdata.selectedFilters([80]);
                //sourceFilterControl.currentFilter(-1);

                toggleFilter(-1, true);
            },

            loadNextPage = function () {
                //presenter.toggleActivity(true);
                ////var filters = [{ FilterTypeId: 7, FilterId: 41 }];
                //$.when(manager.news.getMoreVerification(filters))
                //    .done(function() {
                //        var pIndex = pageIndex() + 1;
                //        pageIndex(pIndex);
                //        presenter.toggleActivity(false);
                //    })
                //    .fail(function () {
                //        presenter.toggleActivity(false);
                //    });
                
            },

            setCurrentNews = function(data, isRelated) {
                var i = 0;
                if (!isRelated) {
                    for (i = 0; i <= bunchArray().length; i++) {
                        if (bunchArray()[i].topNews())
                        if (bunchArray()[i].topNews().id == data.id)
                            break;
                    }
                    
                } else {
                    for (i = 0; i <= bunchArray().length; i++) {
                        if (bunchArray()[i])
                        if (bunchArray()[i].id == data.bunchId)
                            break;
                    }
                }
                var index = i;
                if (data.newsTypeId == e.NewsType.Story || data.newsTypeId == e.NewsType.Package) {
                    
                    updatenews.currentIndex(index);
                    updatenews.setCurrentNews(data);

                    router.navigateTo(config.hashes.fieldreporterverification.getnews);                   
                }
                
            },

            activate = function (routeData, callback) {
                config.verificationscreenflag = 'on';
                //to fix yesterday's date in calender from//
                calendarControl.currentOption('yesterday');
                var date = new Date();
                date.setDate(date.getDate() - 1);
                var f=date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate();
                var f1 = moment(f);
                appdata.lpFromDate = f1.format();
                //appdata.lpFromDate = '2014-07-20T07:54:40.285Z';
                init();
            },

            canLeave = function () {
                return true;
            },

            init = function (curFilters) {
            
                filters = curFilters;
                sourceFilterControl.currentFilter(-1);
                sourceFilterControl.isPopulated(true);
                sourceFilterControl.showExtraFilters(true);
                appdata.isAllNewsFilterSelected(true);

                //sourceFilterControl.currentFilter(-1);
                //sourceFilterControl.isPopulated(true);
                //appdata.isAllNewsFilterSelected(true);
                //sourceFilterControl.showExtraFilters(true);
                toggleFilter(-1);
                initializeBunchGenerator();
            };

        return {
            activate: activate,
            canLeave: canLeave,
            setCurrentNews: setCurrentNews,
            resetFilters: resetFilters,
            imageFooterTmplName: imageFooterTmplName,
            rightTmplName: rightTmplName,
            contentFooterTmplName: contentFooterTmplName,
            contentTmplName: contentTmplName,
            templates: templates,
            pendingNewsCount: pendingNewsCount,
            init: init,
            toggleFilter: toggleFilter,
            sourceFilterControl: sourceFilterControl,
            hashes: hashes,
            bunch: bunch,
            bunchArray: bunchArray,
            searchText: searchText,
            loadNextPage: loadNextPage,
            search: search,
            setCalendarDate:setCalendarDate,
            calendarControl:calendarControl,
            e: e,
        };
    });