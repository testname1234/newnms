﻿define('vm.home',
    [
         'utils',
         'config',
         'enum',
         'control.tool',
         'extensions',
         'manager.bolcg',
         'control.videoeditor',
         'helper',
    ],
    function (utils, config, e, Tool, extensions, managerbolcg, videoEditor, helper) {
        var
        // Properties
        // ------------------------------

        // Arrays
        sourceRoute = ko.observable(0);
        destinationRoute = ko.observable(0);
        currentLoggedInUserName = helper.getCookie('UserName');
        casperTemplates = ko.observableArray([]),
        VideoWalls = ko.observableArray([]),
        casperRundowns = ko.observableArray([]),
        casperMedia = ko.observableArray([])
        localBolCgMedia = ko.observableArray([])
        CasparRundownItems = ko.observableArray([]),
        transitions = ko.observableArray([{ cmd: "CUT", isSelected: ko.observable(false) }]),
        tween = ko.observableArray([{ cmd: "Linear", isSelected: ko.observable(false) }]),
        direction = ko.observableArray([{ cmd: "LEFT", isSelected: ko.observable(false) }]),
        deckLinkInput = ko.observableArray([{ CasparItemId: 7, Name: "Live Stream 1", Device: 5, Thumb: '/Content/images/caspar/livestream.png' }, { CasparItemId: 7, Name: "Live Stream 2", Device: 6, Thumb: '/Content/images/caspar/livestream.png' }, { CasparItemId: 7, Name: "Live Stream 3", Device: 7, Thumb: '/Content/images/caspar/livestream.png' }, { CasparItemId: 7, Name: "Live Stream 4", Device: 8, Thumb: '/Content/images/caspar/livestream.png' }]),
        //current Selected data observables 
        currentItem = ko.observable({ Url: "", Name: "Kamran Khan", VideoLayer: "1", Time: "00:00:00", isSelected: ko.observable(false), target: ".DataPopUp" }),
        currentTemplate = ko.observable({ Name: "Template", isSelected: ko.observable(false) }),
        currentRundown = ko.observable({ Name: "Rundown", isSelected: ko.observable(false) })
        currentMedia = ko.observable({ Name: "Media", isSelected: ko.observable(false) }),
        searchFlag = ko.observable(true),
        searchText = ko.observable("").extend({ throttle: 200 }),
        filteredRundowns = ko.observableArray(),
        filteredFlashTemplates = ko.observableArray(),
        filteredCasparMedia = ko.observableArray(),
        flashTemplateKeys = ko.observableArray([{ "FlashKey": " ", "KeyValue": " " }]),
        currentValue = ko.observable(),
        templateData = ko.observableArray([{ "TemplateDataKey": " ", "TemplateDataValue": " " }]),
        templateWindows = ko.observableArray([]),
        postKeyValuePair = ko.observable(),
        currentKey = ko.observable("Keys"),
        FourWindowDropDown = ko.observable(),
        Tools = [new Tool(6, "Crop"), new Tool(3, "Transform")];
        InputTypes = [{ Name: "", DeviceId: "", isSelected: ko.observable(false) }, { Name: "Deck1", DeviceId: "1", isSelected: ko.observable(false) }, { Name: "Deck2", DeviceId: "2", isSelected: ko.observable(false) }, { Name: "Deck3", DeviceId: "3", isSelected: ko.observable(false) }, { Name: "Deck4", DeviceId: "4", isSelected: ko.observable(false) }],
        popUpDisplayDecision = ko.observable("");
        currentTransition = ko.observable(null),
        currentTween = ko.observable(null),
        currentDirection = ko.observable(null),
        setDuration = ko.observable(),
        setDelay = ko.observable(),
        alertCount = ko.observable(0),
        vidsrc = ko.observable(),
		videoeditorcaspar = new videoEditor(),
		videoselectionfrom = ko.observable(),
		videoselectionto = ko.observable(),
		cl = ko.observable(0),
        serverStatus = ko.observable(false),
        listeningStatus = ko.observable(false),
        CRIDToSkip = ko.observable(),
        emptyPopupFlag = ko.observable(true),
        sanSearchText = ko.observable(null).extend({ throttle: 800 });
        mediaSearchText = ko.observable(null).extend({ throttle: 800 });
        sanResult = ko.observableArray([]),
        loadingScreenFlag = ko.observable(false),
        currentPlaying = ko.observable(),
        previewItem = ko.observable(),
        visibleFourWindowPopupFlag = ko.observable(false),
        sanPath = ko.observable(),
        sanApiUrl = ko.observable("http://10.3.12.119/api/resource/searchresource"),
        sanSearchTypeId = ko.observable(),
        currentPageNumber = ko.observable(1),
        currentTheme = ko.observable(),
        selectedLanguage = ko.observable(""),
        progressBar = ko.observable("00:00:00"),
        audioLevel = ko.observable(),
        connectedServer = ko.observableArray([]),
        notConnectedServer = ko.observableArray([]),
        currentContext = ko.observable(""),
        // ------------------------------


        //ogData
        fourWindowWindowTemplateResolution = { Width: 858, Height: 420 },
        channelResolution = ko.observable(),

        //setting Variables
        mosId = ko.observable(""),
        mosPort = ko.observable(""),
        hostName = ko.observable(""),
        hostPort = ko.observable(""),

        //Paging area
           currentIndex = ko.observable(0),
           currentStoryToDisplay = ko.observable({ "StorySlug": "" }),

           previousStory = function () {

               if (currentRundown()) {
                   if (currentIndex() > 0) {
                       currentIndex(currentIndex() - 1);
                       currentRundown().Stories[currentIndex()].CasparRundownItems.sort(function (a, b) {
                           return a.SequenceOrder - b.SequenceOrder;
                       });
                       currentStoryToDisplay(currentRundown().Stories[currentIndex()]);
                   } else {
                       currentIndex(0);
                       config.logger.error("No previous story");
                   }

               }
           },
           nextStory = function () {
               if (currentRundown()) {
                   if (currentIndex() < currentRundown().Stories.length - 1) {
                       currentIndex(currentIndex() + 1);
                       currentRundown().Stories[currentIndex()].CasparRundownItems.sort(function (a, b) {
                           return a.SequenceOrder - b.SequenceOrder;
                       });
                       currentStoryToDisplay(currentRundown().Stories[currentIndex()]);

                   } else {
                       config.logger.error("No next story");
                   }

               }
           },
        setCurrentStory = function (index) {
            currentIndex(index);
            currentRundown().Stories[currentIndex()].CasparRundownItems.sort(function (a, b) {
                return a.SequenceOrder - b.SequenceOrder;
            });
            currentStoryToDisplay(currentRundown().Stories[currentIndex()]);
        },

        //End paging area

        //reportBug
        reportBugName = ko.observable(),
        reportBugEmail = ko.observable(),
        reportBugContact = ko.observable(),
        reportBugDetail = ko.observable(),

        //Computed Property
        //--------------------------------

        loadFilterInitilay = ko.computed({
            read: function () {
                var fRundowns = casperRundowns();
                var fFlashTemplates = casperTemplates();
                if (fRundowns.length > 0 || fFlashTemplates.length > 0) {
                    filteredRundowns(fRundowns);
                    filteredFlashTemplates(fFlashTemplates);
                }
            },
            deferEvaluation: false
        }),

        //Methods
        //-------------------------------

        //Rundown
        currentSelectedRundown = function (data) {
            clearCurrent();
            if (data.Stories[0].CasparRundownItems && data.Stories[0].CasparRundownItems != null && data.Stories[0].CasparRundownItems.length > 0) {
                data.Stories[0].CasparRundownItems.sort(function (a, b) {
                    return a.SequenceOrder - b.SequenceOrder;
                });
            }
            data.isSelected(true);
            currentRundown(data);
            currentStoryToDisplay(currentRundown().Stories[0]);
            if (app) {
                app.setCurrentRundown(ko.toJSON(currentRundown()));
            }
            //CasparRundownItems(currentRundown().CasparRundownItems);
        },

        //Rundown items
        currentSelectedItem = function (data, event) {
            clearCurrent();

            //templateWindows([]);
            //templateData([]);
            //data.isSelected(true);
            //currentItem(data);
        },

        fillPopup = function (data) {
        },

        //Flashtemplates
        currentSelectedTemplate = function (data) {
            clearCurrent();
            data.isSelected(true);
            currentTemplate(data);
        },



        //media
        currentSelectedMedia = function (data) {
            clearCurrent();
            data.isSelected(true);
            currentMedia(data);
        },

        //addingkeyvalueToDB
        addFlashTemplateKeyValue = function () {
            if (app) {
                for (var i = 0; i < templateData().length; i++) {
                    //var obj = { TemplateDataKey: flashTemplateKeys()[i].FlashKey, TemplateDataValue: flashTemplateKeys()[i].KeyValue, CasparRundownItemId: currentItem().CasparRundownItemId };
                    if (templateData()[i] && templateData()[i].TemplateDataValue && templateData()[i].TemplateDataValue != null) {
                        app.insertTemplateData(ko.toJSON(templateData()[i]));
                    } else {
                        config.logger.error("Fill all fields");
                    }

                }
            }
        },

        //selectedFlashTemplateKey
        selectedFlashTemplateKey = function (data) {
            currentKey(data.FlashKey);
            var obj = { TemplateDataKey: data.FlashKey, TemplateDataValue: currentValue(), CasparRundownItemId: currentItem().CasparRundownItemId };
            postKeyValuePair(obj);
        },

        RemoveTemplateData = function (data) {
            if (app && data.TemplateDataId) {
                app.removeTemplateData(JSON.stringify(data.TemplateDataId));
            }
        },

        setTransition = function (data) {
            if (currentTransition()) {
                currentTransition().isSelected(false);
            }
            data.isSelected(true);
            currentTransition(data);

            //if (currentItem().CasparItemId == e.CasparItemType.Video) {
            currentItem().Video.Transition = data.cmd;

            //} else if (currentItem().CasparItemId == e.CasparItemType.Image) {
            //   currentItem().CasparImage.Transition = data.cmd;
            //}
        },

        setTween = function (data) {
            if (currentTween()) {
                currentTween().isSelected(false);
            }
            data.isSelected(true);
            currentTween(data);
            //if (currentItem().CasparItemId == e.CasparItemType.Video) {
            currentItem().Video.Tween = data.cmd;
            //} else if (currentItem().CasparItemId == e.CasparItemType.Image) {
            //   currentItem().CasparImage.Tween = data.cmd;
            //}
        },

        setDirection = function (data) {
            if (currentDirection()) {
                currentDirection().isSelected(false);
            }

            data.isSelected(true);
            currentDirection(data);
            //  if (currentItem().CasparItemId == e.CasparItemType.Video) {
            currentItem().Video.Direction = data.cmd;
            //} else if (currentItem().CasparItemId == e.CasparItemType.Image) {
            //   currentItem().CasparImage.Direction = data.cmd;
            // }
        },

        clearPopUpFields = function () {
            postKeyValuePair('');
            currentValue('');
            currentKey("Keys");
        },

        clearAlerts = function () {
            alertCount(0);
        }

        refreshLibrary = function () {
            if (app) {
                app.refreshLibrary();
            }
        },

        clearCurrent = function () {
            if (currentRundown && currentRundown().isSelected && currentRundown().isSelected()) {
                currentRundown().isSelected(false);
            }
            currentItem().isSelected(false);
            currentMedia().isSelected(false);
            currentTemplate().isSelected(false);
        },

        playCompleteCurrentRundown = function (index) {
            if (currentRundown && currentRundown().CasparRundownId && serverStatus()) {
                app.playCompleteCurrentRundown(index, currentStoryToDisplay().StoryId);
            } else {
                config.logger.error("Select Rundown and Turn On Server");
            }
        }

        exit = function () {
            if (app) {
                app.exitApplication();
            }
        }

        activate = function (routeData, callback) {
            if (app) {
                app.loadInitialData();
            }
            // managerbolcg.loadInititalData();   
        },

        //Server Call Backs
        window.loadVideoWallsCallBack = function (d) {
            var data = JSON.parse(d);
            VideoWalls(data);
        },
        window.loadConnectedCallBack = function (d) {
            var data = JSON.parse(d);
            connectedServer(data);

        },
        window.loadNotConnectedCallBack = function (d) {
            var data = JSON.parse(d);
            notConnectedServer(data);
        }

        //Server Call Backs
        window.loadRundownDownsCallBack = function (d, Type) {
            d = d.replace(/[\r\n]/g, "");
            var data = JSON.parse(d);
            if (data) {
                for (var i = 0; i < data.length; i++) {
                    data[i]["isSelected"] = ko.observable(false);
                    for (var l = 0; l < data[i].Stories.length; l++) {
                        if (data[i].Stories[l].CasparRundownItems && data[i].Stories[l].CasparRundownItems.length > 0) {
                            for (var j = 0; j < data[i].Stories[l].CasparRundownItems.length; j++) {
                                data[i].Stories[l].CasparRundownItems[j]["isSelected"] = ko.observable(false);
                                data[i].Stories[l].CasparRundownItems[j]["IsSkipped"] = ko.observable(false);
                                data[i].Stories[l].CasparRundownItems[j]["IsBeingPlayed"] = ko.observable(false);
                                data[i].Stories[l].CasparRundownItems[j]["IsFailed"] = ko.observable(false);
                                if (data[i].Stories[l].CasparRundownItems[j].Video != null && data[i].Stories[l].CasparRundownItems[j].Video.CasparRundownItems != null) {
                                    for (var k = 0; k < data[i].Stories[l].CasparRundownItems[j].Video.CasparRundownItems.length; k++) {
                                        data[i].Stories[l].CasparRundownItems[j].Video.CasparRundownItems[k]["isSelected"] = ko.observable(false);
                                        data[i].Stories[l].CasparRundownItems[j].Video.CasparRundownItems[k]["IsSkipped"] = ko.observable(false);
                                        data[i].Stories[l].CasparRundownItems[j].Video.CasparRundownItems[k]["IsBeingPlayed"] = ko.observable(false);
                                        data[i].Stories[l].CasparRundownItems[j].Video.CasparRundownItems[k]["IsFailed"] = ko.observable(false);
                                    }
                                }
                            }
                        }
                    }
                }
                if (casperRundowns().length == 0) {
                    casperRundowns(data);

                } else {
                    casperRundowns.unshift(data[0]);
                    currentRundown(data[0]);
                }
                if (Type && Type == "NEW") {
                    alertCount(alertCount() + 1);
                }

            }
        };

        window.loadFlashTemplatesCallBack = function (d) {

            var data = JSON.parse(d);
            if (data) {
                for (var i = 0; i < data.length; i++) {
                    data[i]["isSelected"] = ko.observable(false);
                }
                casperTemplates(data);
            }
        };

        window.loadMediaCallBack = function (d) {
            var data = JSON.parse(d);
            if (data) {
                if (localBolCgMedia().length > 0) {
                    localBolCgMedia.unshift(data[0]);
                } else {
                    localBolCgMedia(data);
                }
                loadingScreenFlag(false);
            }
        };

        window.insertTemplateDataCallBack = function (d, id) {
            //d = d.replace(/[\r\n]/g, "");
            //var data = JSON.parse(d);
            //if (d) {
            //    if (id) {
            //        for (var i = 0; i < templateData().length; i++) {
            //            if (templateData()[i].TemplateDataId = id) {
            //                templateData.remove(templateData()[i]);
            //            }
            //        }
            //    }
            //    templateData.push(data);
            //    config.logger.success(" Record Inserted");
            //}
        };

        window.removeTemplateDataCallBack = function (d) {
            d = d.replace(/[\r\n]/g, "");
            if (d && d > 0) {
                for (var i = 0; i < templateData().length; i++) {
                    if (templateData()[i].TemplateDataId == d) {

                        templateData.remove(templateData()[i]);
                        templateData.valueHasMutated();

                    }
                }
                config.logger.success(" Record removed");
            }
        };

        window.loadChannelResolution = function (d) {
            d = d.replace(/[\r\n]/g, "");
            var data = JSON.parse(d);
            if (data) {
                channelResolution(data);
            }
        },

        window.itemToAddInRundownCallBack = function (d, targetId) {
             d = d.replace(/[\r\n]/g, "");
             var data = JSON.parse(d);
             if (data && targetId == 0) {
                 var cr = currentStoryToDisplay();
                 data.isSelected = ko.observable(false);
                 data.IsSkipped = ko.observable(false);
                 data.IsBeingPlayed = ko.observable(false);
                 data.IsFailed = ko.observable(false);
                 if (cr.CasparRundownItems == null) {
                     cr.CasparRundownItems = [];
                 }
                 cr.CasparRundownItems.push(data);
                 currentStoryToDisplay(cr);
             } else {
                 data.isSelected = ko.observable(false);
                 data.IsSkipped = ko.observable(false);
                 data.IsBeingPlayed = ko.observable(false);
                 for (var i = 0; i < data.length; i++) {
                     if (data[i].Video && data[i].Video.CasparRundownItems && data[i].Video != null && data[i].Video.CasparRundownItems != null) {
                         for (var k = 0; k < data[i].Video.CasparRundownItems.length; k++) {
                             data[i].Video.CasparRundownItems[k]["isSelected"] = ko.observable(false);
                             data[i].Video.CasparRundownItems[k]["IsSkipped"] = ko.observable(false);
                             data[i].Video.CasparRundownItems[k]["IsBeingPlayed"] = ko.observable(false);
                             data[i].Video.CasparRundownItems[k]["IsFailed"] = ko.observable(false);
                         }
                     }
                 }
                 var cr = currentStoryToDisplay();
                 for (var i = 0; i < cr.CasparRundownItems.length; i++) {
                     if (cr.CasparRundownItems && cr.CasparRundownItems != null) {
                         if (cr.CasparRundownItems[i].CasparRundownItemId == targetId) {
                             if (cr.CasparRundownItems[i].Video && cr.CasparRundownItems[i].Video.CasparRundownItems == null) {
                                 cr.CasparRundownItems[i].Video.CasparRundownItems = [];
                             }
                             cr.CasparRundownItems[i].Video.CasparRundownItems.push(data);
                         }
                     }
                 }
                 currentStoryToDisplay(cr);
             }
             loadingScreenFlag(false);
         }


        window.deleteItemCallBack = function (d) {
            if (d) {

                var cr = currentStoryToDisplay();
                for (var i = 0; i < cr.CasparRundownItems.length; i++) {
                    if (cr.CasparRundownItems[i].CasparRundownItemId == d) {
                        cr.CasparRundownItems.remove(cr.CasparRundownItems[i]);
                        currentStoryToDisplay(cr);
                    }
                    if (cr.CasparRundownItems[i].Video && cr.CasparRundownItems[i].Video.CasparRundownItems) {
                        for (var j = 0; j < cr.CasparRundownItems[i].Video.CasparRundownItems.length; j++) {
                            if (cr.CasparRundownItems[i].Video.CasparRundownItems[j].CasparRundownItemId == d) {
                                cr.CasparRundownItems[i].Video.CasparRundownItems.remove(cr.CasparRundownItems[i].Video.CasparRundownItems[j]);
                                currentStoryToDisplay(cr);
                            }
                        }
                    }
                }
            }
        }

        window.connectCasparCallBack = function (d) {
            if (d == "Success") {

                config.logger.success("Server Connected");
            }

        }

        window.startListeningCallBack = function (d) {
            if (d == "Success") {
                listeningStatus(true);
            } else {
                listeningStatus(false);
            }
        }

        window.toaster = function (msg, flag) {

            if (flag == "True") {
                config.logger.success(msg);
            }
            else {
                config.logger.error(msg);
            }
        }

        window.notifyServerHealthCallBack = function (d) {
            if (d && d == "ON") {
                serverStatus(true);
            } else {
                serverStatus(false)
            }
        }

        window.IsSkippedCallBack = function (d) {

        }

        window.loadCasparTweensCallBack = function (d) {
            var data = JSON.parse(d);
            if (data) {

                var tempObj = [];
                for (var i = 0 ; i < data.length; i++) {
                    tempObj[i] = { "cmd": data[i].Name, "isSelected": ko.observable(false) };
                }
                tween(tempObj);
            }

        }

        window.loadCasparDirectionsCallBack = function (d) {
            var data = JSON.parse(d);
            if (data) {

                var tempObj = [];
                for (var i = 0 ; i < data.length; i++) {
                    tempObj[i] = { "cmd": data[i].Name, "isSelected": ko.observable(false) };
                }
                direction(tempObj);
            }
        }

        window.loadCasparTransitionsCallBack = function (d) {
            var data = JSON.parse(d);
            if (data) {

                var tempObj = [];
                for (var i = 0 ; i < data.length; i++) {
                    tempObj[i] = { "cmd": data[i].Name, "isSelected": ko.observable(false) };
                }
                transitions(tempObj);
            }
        }

        window.loadCSettingsCallBack = function (d) {
            var data = JSON.parse(d);
            if (data) {
                mosId(data.MosId);
                mosPort(data.MosPort);
                hostName(data.HostName);
                hostPort(data.HostPort);
            }
        }

        window.updateTemplateWindowsOnDragCallBack = function (targetName, targetWindowId, DraggedItemCasparItemId) {
            if (targetName && targetWindowId) {
                for (var i = 0; i < templateWindows().length; i++) {
                    if (templateWindows()[i].FlashTemplateWindowId == targetWindowId) {
                        templateWindows()[i].FileName(targetName);
                        templateWindows()[i].Media.CasparItemId = DraggedItemCasparItemId;
                    }
                }
            }
            loadingScreenFlag(false);
        }
        window.Loadtheme = function (d) {
            if (d) {
                currentTheme(d);
            }

        }
        window.notifyRundownStatusCallBack = function (id) {
            if (id) {
                var ids = id.split(',');
                for (var i = 0; i < currentStoryToDisplay().CasparRundownItems.length; i++) {
                    if (ids.indexOf(currentStoryToDisplay().CasparRundownItems[i].CasparRundownItemId.toString()) > -1) {
                        if (currentPlaying()) {
                            currentPlaying().IsBeingPlayed(false);
                        }
                        currentStoryToDisplay().CasparRundownItems[i].IsFailed(false);
                        currentStoryToDisplay().CasparRundownItems[i].IsBeingPlayed(true);
                        //currentPlaying(currentStoryToDisplay().CasparRundownItems[i]);
                    }
                    else {
                        currentStoryToDisplay().CasparRundownItems[i].IsFailed(false);
                        currentStoryToDisplay().CasparRundownItems[i].IsBeingPlayed(false);
                    }
                }
            }
        }

        window.LoadConfigSetting = function (sPath, sApiUrl) {
            sanPath(sPath);
            sanApiUrl(sApiUrl);
        }
        window.LoaderHandle = function (d) {
            if (d == "False") {
                loadingScreenFlag(false);
            } else {
                loadingScreenFlag(true);
            }

        }

        window.rundownItemFailCallBack = function (id) {
            if (id) {
                for (var i = 0; i < currentStoryToDisplay().CasparRundownItems.length; i++) {
                    if (currentStoryToDisplay().CasparRundownItems[i].CasparRundownItemId == id) {
                        currentStoryToDisplay().CasparRundownItems[i].IsFailed(true);
                    }
                }
            }

        }

        //start listening
        startListening = function () {
            if (mosId && mosPort && mosId() != null && mosPort() > 0) {
                obj = { MosId: mosId(), MosPort: mosPort() }
                app.startListening(ko.toJSON(obj));
            }
            else {
                config.logger.error("Fill all Fields");
            }
        }

        //connect t0 caspar server
        connectCaspar = function () {
            if (hostName && hostPort && hostName() != null && hostPort() > 0) {
                obj = { HubIp: hostName(), NcsPort: hostPort() }
                app.connectCaspar(ko.toJSON(obj));
            }
            else {
                config.logger.error("Fill all Fields");
            }
        }

        //End Server CallBacks

        canLeave = function () {
        },

        toggleFullScreen = function () {
            if (app) {
                app.ToggleFullScreen();
            }
        },

        //searchText subscriber
        searchText.subscribe(function () {
            var srchText = searchText();
            var ItemsList = casperRundowns();

            if (ItemsList.length > 0) {
                var arr = _.filter(ItemsList, function (singleItem) {
                    if (filterItems(singleItem, srchText)) {
                        return true;
                    } else {
                        return false;
                    }
                });
                filteredRundowns(arr);
            }
        });
        //subscriber End

        //searchFilterMethod
        filterItems = function (item, srchText) {

            if (!srchText) return true;

            var srch = utils.regExEscape(srchText.toLowerCase());

            if (item.Name.toLowerCase().search(srch) !== -1)
                return true;

            return false;
        },
        //End serachFilters

        //shortCutKeys Sections

         shortcut.add("F1", function () {
             if (currentItem && currentItem() != null && currentItem().CasparRundownItemId) {
                 app.stopCG(ko.toJSON(currentItem()));
             } else {
                 config.logger.error("No item selected");
             }
         });
        shortcut.add("F2", function () {

            if (currentItem && currentItem() != null && currentItem().CasparRundownItemId) {
                app.playWithoutLoad(ko.toJSON(currentItem()));
            } else {
                config.logger.error("No item selected");
            }
        });

        shortcut.add("F3", function () {
            if (currentItem && currentItem() != null && currentItem().CasparRundownItemId) {
                app.load(ko.toJSON(currentItem()));
            } else {
                config.logger.error("No item selected");
            }
        });
        shortcut.add("F4", function () {
            if (currentItem && currentItem() != null && currentItem().CasparRundownItemId) {
                app.play(ko.toJSON(currentItem()));
            } else {
                config.logger.error("No item selected");
            }
        });
        shortcut.add("F5", function () {
            if (currentItem && currentItem() != null && currentItem().CasparRundownItemId) {

                app.pause(ko.toJSON(currentItem()));
            } else {
                config.logger.error("Select item to pause");
            }
        });

        shortcut.add("F6", function () {
            if (currentItem && currentItem() != null && currentItem().CasparRundownItemId) {
                app.resume(ko.toJSON(currentItem()));
            } else {
                config.logger.error("Select item to resume");
            }
        });

        shortcut.add("F7", function () {
            if (currentItem && currentItem() != null && currentItem().CasparRundownItemId) {
                app.stop(ko.toJSON(currentItem()));
            } else {
                config.logger.error("Select item to stop");
            }
        });
        shortcut.add("F8", function () {
            if (currentItem && currentItem() != null && currentItem().CasparRundownItemId) {
                app.update(ko.toJSON(currentItem()));
            } else {
                config.logger.error("No item selected");
            }
        });

        shortcut.add("F10", function () {
            if (currentItem && currentItem() != null && currentItem().CasparRundownItemId) {
                app.next(ko.toJSON(currentItem()));
            } else {
                config.logger.error("No item selected");
            }
        });
        shortcut.add("F12", function () {
            if (currentItem && currentItem() != null && currentItem().CasparRundownItemId) {
                app.clearChannel(JSON.stringify(currentItem()));
            }
        });

        shortcut.add("ctrl+F12", function () {
            if (currentItem && currentItem() != null && currentItem().CasparRundownItemId) {
                app.clear();
            }
        });

        shortcut.add("ctrl+r", function () {
            refreshLibrary();
        });

        shortcut.add("ctrl+delete", function () {
            if (app && currentItem && currentItem().CasparRundownItemId) {
                app.deleteItem(currentItem().CasparRundownItemId, currentStoryToDisplay().StoryId);
            }
        });

        shortcut.add("ctrl+e", function () {
            exit();
        });

        shortcut.add("ctrl+f", function () {
            toggleFullScreen();
        });

        shortcut.add("ctrl+b", function () {
            connectCaspar();
        });

        //shortcut.add("ctrl+v", function () {
        //    startListening();
        //});

        shortcut.add("Esc", function () {
            onclose();
        });

        sanSearchText.subscribe(function (data) {
            if (searchFlag()) {

                var srchText = sanSearchText();
                var ItemsList = casperTemplates();

                if (ItemsList.length > 0) {
                    var arr = _.filter(ItemsList, function (singleItem) {
                        if (filterItems(singleItem, srchText)) {
                            return true;
                        } else {
                            return false;
                        }
                    });
                    filteredFlashTemplates(arr);
                }
            } else {
                var pageNumber = 1;
                var pageSize = 10;
                currentPageNumber(1);
                getSanResult(pageSize, pageNumber, sanSearchText(), false);
            }
        });

        getSanResult = function (pageSize, pageNumber, sanSearchText, scroll) {
            if (sanSearchText == null || sanSearchText == "") {
                sanResult([]);
                return false;
            }
            var pageNumber = pageNumber;
            var pageSize = pageSize;
            console.log("pgN  " + pageNumber);
            console.log("text  " + sanSearchText);
            var cUrl = sanApiUrl() + "?term=" + "\"" + sanSearchText + "\"" + "&pageSize=" + pageSize + "&pageNumber=" + pageNumber + "&resourceTypeId=" + sanSearchTypeId();
            $.ajax({
                url: cUrl,
                type: "GET",
                dataType: "json",
                success: function (result) {
                    console.log(result);
                    if (result && result.Resources && result.Resources.length > 0) {


                        if (!scroll) {
                            sanResult([]);
                        }

                        for (var i = 0; i < result.Resources.length; i++) {
                            if (result.Resources[i].ResourceTypeId == 1) {
                                result.Resources[i].ResourceTypeId = 4;
                            }
                            if (typeof result.Resources[i].Slug === 'undefined') {
                                result.Resources[i].Slug = "Slug is undefined";
                            }

                            if (scroll && scroll == true) {
                                sanResult.push(result.Resources[i]);
                            } else {
                                sanResult.push(result.Resources[i]);
                            }
                        }
                    }
                },
                error: function (a, b) {
                }
            });

        },

        mediaSearchText.subscribe(function (data) {
            var pageNumber = 1;
            var pageSize = 10;
            currentPageNumber(1);
            getSanResult(pageSize, pageNumber, mediaSearchText(), false);
        });


        //local media search::::::::::::
        filterlocalBolCgMedia = ko.computed({
            read: function () {
                var bolCgMedia = localBolCgMedia();
                var srchText;
                if (sanSearchText()) {
                    srchText = sanSearchText();
                } else {
                    srchText = mediaSearchText();
                }

                var arr = _.filter(bolCgMedia, function (media) {
                    if (filterLocalMedia(media, srchText)) {
                        return true;
                    } else {
                        return false;
                    }
                });

                return arr;
            },
            deferEvaluation: true
        }),
        // Methods
        // ------------------------------

       filterLocalMedia = function (media, srchText) {
           if (!srchText) return true;

           var srch = utils.regExEscape(srchText.toLowerCase());

           if (media.Name.toLowerCase().search(srch) !== -1)
               return true;

           return false;
       },

        //End shortCutKeys Sections
       droppedItem = function (item, targetId, targetTypeId, url, isLocalMedia, FileExt, RunOrderId) {
           if (currentRundown().CasparRundownId) {
               item.CasparRundownId = currentRundown().CasparRundownId;
               if (app) {
                   loadingScreenFlag(true);
                   app.itemToAddInRundown(JSON.stringify(item), targetId, targetTypeId, url, isLocalMedia, FileExt, RunOrderId);
               }
           } else {
               config.logger.error("select Rundorder First");
           }
       },

        //FourWindowPopUpEvents
        ResoulutionFix = function (/*it will take rectangles array*/ channelPercentage) {
            var channelWidth = channelResolution().Width;
            var channelHeight = channelResolution().Height;
            var FourWindowWidth = fourWindowWindowTemplateResolution.Width;
            var FourWindowHeight = fourWindowWindowTemplateResolution.Height;
            var rectanglesToDrawOnFourWindow = [];



            var rectangles = new Array();
            //Four window percent To draw 
            for (var i = 0; i < channelPercentage.length; i++) {
                rectangles[i] = channelPercentage[i];
                if (ko.isObservable(rectangles[i].FileName)) {
                    rectangles[i].FileName = ko.observable(rectangles[i].FileName());
                } else {
                    rectangles[i].FileName = ko.observable(rectangles[i].FileName);
                }
                rectangles[i].Tools = new Array();
                for (var k = 0; k < Tools.length; k++) {
                    var newObj = copyObject(Tools[k]);
                    newObj.isSelected = ko.observable(false);
                    newObj.Current = ko.observable();
                    rectangles[i].Tools.push(newObj);
                }
                //prompt("Copy to clipboard: Ctrl+C, Enter", JSON.stringify(channelPercentage[i]));
                //alert(JSON.stringify(channelPercentage[i]))
                rectangles[i].topLeftx = channelPercentage[i].TopLeftx * FourWindowWidth;
                rectangles[i].topLefty = channelPercentage[i].BottomRighty * FourWindowHeight;
                rectangles[i].width = channelPercentage[i].Width * FourWindowWidth;
                rectangles[i].height = channelPercentage[i].Height * FourWindowHeight;

                if (rectangles[i].ToolId == e.CasparItemType.Crop) {
                    rectangles[i].Tools[0].isSelected(true);
                }
                else if (rectangles[i].ToolId == e.CasparItemType.Transformation) {
                    rectangles[i].Tools[1].isSelected(true);
                }
                rectanglesToDrawOnFourWindow.push(rectangles[i]);
            }
            return rectanglesToDrawOnFourWindow;
        }

        selectedTool = function (data, item, element) {
            var cToolId = data.id;
            item.ToolId = cToolId;
            $(element).siblings().removeClass('selected');
            $(element).parent('ul').prev('a').children('.Name').text(data.name);
            data.isSelected(true);
            $(element).addClass('selected');
        },

        setTransitionTab = function (data) {
            currentTransition(null);
            currentDirection(null);
            currentTween(null);

            if (data && data.Delay && data.Delay != null) {
                setDelay(data.Delay);
            }

            if (data.Video && data.Video.Transition && data.Video.Transition !== null) {
                for (var i = 0; i < transitions().length; i++) {
                    transitions()[i].isSelected(false);
                    if (data.Video && data.Video && data.Video.Transition == transitions()[i].cmd) {
                        transitions()[i].isSelected(true);
                        currentTransition(transitions()[i]);
                    }
                }
            }
            if (data.Video && data.Video.Direction && data.Video.Direction !== null) {
                for (var i = 0; i < direction().length; i++) {
                    direction()[i].isSelected(false);
                    if (data.Video && data.Video.Direction == direction()[i].cmd) {
                        direction()[i].isSelected(true);
                        currentDirection(direction()[i]);
                    }
                }
            }
            if (data.Video && data.Video.Tween && data.Video.Tween !== null) {

                for (var i = 0; i < tween().length; i++) {
                    tween()[i].isSelected(false);
                    tween()[i].cmd
                    if (data.Video && data.Video.Tween == tween()[i].cmd) {
                        tween()[i].isSelected(true);
                        currentTween(tween()[i]);
                    }
                }
            }
            if ((data.Video && data.Video.Duration && data.Video.Duration !== null)) {
                if (data.Video && data.Video.Duration && data.Video.Duration != null) {
                    setDuration(data.Video.Duration);
                }
            }

        }

        windowInput = function (data, item, element) {
            //var cToolId = data.id;
            //item.ToolId = cToolId;
            $(element).siblings().removeClass('selected');
            $(element).parent('ul').prev('a').children('.Name').text(data.Name);
            $(element).addClass('selected');
        },

        saveData = function (data) {
            onclose();
        },

        saveTransitionPopupPanel = function () {
            //save transition values to db   
            if (currentTransition() || currentTween() || currentDirection() || currentItem().Delay || currentItem().Video.Duration || currentItem().Video.Loop || currentItem().Video.FreezeOnLoad) {
                var obj = {
                    Transition: currentTransition().cmd,
                    Tween: currentTween().cmd,
                    Direction: currentDirection().cmd,
                    Loop: currentItem().Video.Loop,
                    Duration: currentItem().Video.Duration,
                    CasparRundownItemId: currentItem().CasparRundownItemId,
                    FreezeOnLoad: currentItem().Video.FreezeOnLoad,
                };
                app.setTransitionTab(JSON.stringify(obj), currentItem().Delay, currentStoryToDisplay().StoryId);
            } else {
                config.logger.toaster('fill all fields');
            }
        }

        //End FourWindowPopUpEvents
        checkValue = function (data) {

        },

        copyObject = function (obj) {
            return JSON.parse(JSON.stringify(obj));
        },

        setDuration.subscribe(function (data) {

            if (currentItem().CasparItemId == e.CasparItemType.Video) {
                currentItem().Video.Duration = data;
            }
        });

        setDelay.subscribe(function (data) {
            currentItem().Delay = data;
        });
        //now manually make any desired modifications

        init = function () {

        };
        init();

        getseek = function () {

            var startframe = Math.floor(currentItem().Video.FrameRate * videoselectionfrom());
            var toframe = Math.floor(currentItem().Video.FrameRate * videoselectionto());
            var length = ((toframe - startframe));

            currentItem().Video.Seek = startframe;
            currentItem().Video.Length = length;

        };

        clipvid = function () {

            //            app.clipVideo(videoselectionfrom(), videoselectionto(), vidsrc(), ko.toJSON(currentItem()));
        };

        onclose = function () {
            videoeditorcaspar.totalSelections([]);
            videoeditorcaspar.videoDuration('');
            videoeditorcaspar.videoControlElement().currentTime = 0;
            videoselectionfrom(0);
            videoselectionto(0);
            videoeditorcaspar.stop();
            cl(0);
        };

        onpreview = function () {
            videoeditorcaspar.videoControlElement().currentTime = videoselectionfrom();
            videoeditorcaspar.videoControlElement().play();
        }

        chktime = function () {
            if (videoselectionfrom() && videoselectionto()) {

                if (videoeditorcaspar.videoControlElement().currentTime >= videoselectionto()) {
                    videoeditorcaspar.videoControlElement().pause();
                    videoeditorcaspar.videoControlElement().currentTime = videoselectionto();
                }
            }
        }

        //update RundownItem Sequence
        updateRundownItemSequence = function (data) {
            for (var i = 0; i < data.length; i++) {
                data[i] = parseInt(data[i]);

                for (var j = 0; j < data.length; j++) {

                    if (currentStoryToDisplay().CasparRundownItems[j].CasparRundownItemId == data[i]) {
                        currentStoryToDisplay().CasparRundownItems[j].SequenceOrder = i + 1;
                    }
                }
                currentStoryToDisplay().CasparRundownItems.sort(function (a, b) {
                    return a.SequenceOrder - b.SequenceOrder;
                });
            }
            if (app) {
                app.updateRundownItemSequence(ko.toJSON(data), currentStoryToDisplay().StoryId);
            }
        };

        reportBug = function (obj) {
            if (app) {
                app.reportBug(ko.toJSON(obj));
                reportBugName(null);
                reportBugEmail(null);
                reportBugContact(null);
                reportBugDetail(null);
            }
        };

        updateTemplateWindowsOnDrag = function (targetName, targetWindowId, resourceUrl, draggedItemCasparItemId, fileExtension, isLocalMedia, runOrderId, storyId, WallId) {

            loadingScreenFlag(true);
            if ((targetName && targetWindowId && draggedItemCasparItemId) || (targetName && targetWindowId && resourceUrl && fileExtension)) {
                app.updateResourceToServer(targetName, targetWindowId, resourceUrl, currentItem().CasparRundownItemId, draggedItemCasparItemId, fileExtension, isLocalMedia, runOrderId, storyId, WallId);
            }

        };

        cMenuAction = function (key, id, cindex) {

            if (key && id) {
                if (key == "Skip") {
                    for (var i = 0; i < currentStoryToDisplay().CasparRundownItems.length; i++) {
                        if (id == currentStoryToDisplay().CasparRundownItems[i].CasparRundownItemId) {
                            currentStoryToDisplay().CasparRundownItems[i].IsSkipped(true);
                            app.skipUnSkip(currentStoryToDisplay().CasparRundownItems[i].CasparRundownItemId, 1, currentStoryToDisplay().StoryId);
                        }
                    }
                }
                else {
                    for (var i = 0; i < currentStoryToDisplay().CasparRundownItems.length; i++) {
                        if (id == currentStoryToDisplay().CasparRundownItems[i].CasparRundownItemId) {
                            currentStoryToDisplay().CasparRundownItems[i].IsSkipped(false);
                            app.skipUnSkip(currentStoryToDisplay().CasparRundownItems[i].CasparRundownItemId, 0, currentStoryToDisplay().StoryId);
                        }
                    }
                }
                if (key == "Play") {
                    playCompleteCurrentRundown(cindex);
                }
            }
            //-------------------------------------------------------------------------
        };

        minimizeWindow = function () {
            app.minimizeWindow();
        };

        verifyRundown = function () {
            if (currentRundown().CasparRundownId) {
                app.verifyRundown();
            } else {
                config.logger.error("Select Rundorder First");
            }

        };

        browseFile = function () {
            app.browseFile();
        };

        browseFileWall = function (data) {
            loadingScreenFlag(true);
            setTimeout(function () { app.browseFileWall(data); }, 300);

        };

        getSanPagedData = function () {
            currentPageNumber(currentPageNumber() + 1);
            var pageSize = 10;
            if (sanSearchText()) {
                getSanResult(pageSize, currentPageNumber(), sanSearchText(), true);
            }
            if (mediaSearchText()) {
                getSanResult(pageSize, currentPageNumber(), mediaSearchText(), true);
            }
        };


        pause = function () {
            if (currentItem().CasparItemId == 2) {
                app.Pasue();
            }
        };

        resume = function () {
            if (currentItem().CasparItemId == 2) {
                app.Resume();
            }
        };

        stop = function () {
            if (currentItem().CasparItemId == 2) {
                app.Stop();
            }
        };
        updateRundownItemProperties = function (itemNewName, id, propertyToUpdate) {
            if (app) {
                app.UpdateItemProperties(itemNewName, id, propertyToUpdate);
            }
        };

        window.udpRecieve = function (current, total) {
            progressBar(current);
        };
        window.udpAudioLevelRecieve = function (data) {

            audioLevel(data);

        };
        updateAnchorPoints = function (x, y, VideoLayer) {
            if (app) {
                app.updateAnchorPoints(x, y, VideoLayer);
            }
        };
        setSourceRoute = function (data) {
            $('.btn-S').removeClass('active')
            if (data != sourceRoute()) {
                sourceRoute(data);
            }
            else {
                sourceRoute(0);
            }
        };

        setDestinationRoute = function (data) {
            $('.btn-D').removeClass('active')
            if (data != destinationRoute()) {
                destinationRoute(data);
            }
            else {
                destinationRoute(0);
            }
        };
        takeRoute = function () {
            if (destinationRoute() != 0) {
                if (sourceRoute() != 0) {
                    app.takeRoute(destinationRoute(), sourceRoute());
                }
                else {
                    toaster('Kindly Select a Source', false)
                }
            }
            else {
                toaster('Kindly Select a Destination', false)
            }
        };

        resetRoute = function () {
            app.resetRoute();
        };

        window.ClearRouteSelection = function () {
            $('.btn-D').removeClass('active')
            $('.btn-S').removeClass('active')
            destinationRoute(0); sourceRoute(0);
        };

        return {
            activate: activate,
            canLeave: canLeave,
            currentItem: currentItem,
            currentSelectedItem: currentSelectedItem,
            casperTemplates: casperTemplates,
            currentTemplate: currentTemplate,
            currentSelectedTemplate: currentSelectedTemplate,
            casperRundowns: casperRundowns,
            currentRundown: currentRundown,
            currentSelectedRundown: currentSelectedRundown,
            casperMedia: casperMedia,
            currentMedia: currentMedia,
            currentSelectedMedia: currentSelectedMedia,
            searchFlag: searchFlag,
            searchText: searchText,
            filteredRundowns: filteredRundowns,
            filteredFlashTemplates: filteredFlashTemplates,
            filteredCasparMedia: filteredCasparMedia,
            toggleFullScreen: toggleFullScreen,
            e: e,
            flashTemplateKeys: flashTemplateKeys,
            selectedFlashTemplateKey: selectedFlashTemplateKey,
            addFlashTemplateKeyValue: addFlashTemplateKeyValue,
            currentValue: currentValue,
            templateData: templateData,
            RemoveTemplateData: RemoveTemplateData,
            currentKey: currentKey,
            templateWindows: templateWindows,
            selectedTool: selectedTool,
            windowInput: windowInput,
            FourWindowDropDown: FourWindowDropDown,
            saveData: saveData,
            checkValue: checkValue,
            popUpDisplayDecision: popUpDisplayDecision,
            refreshLibrary: refreshLibrary,
            droppedItem: droppedItem,
            fillPopup: fillPopup,
            clearCurrent: clearCurrent,
            ResoulutionFix: ResoulutionFix,
            exit: exit,
            mosId: mosId,
            mosPort: mosPort,
            hostName: hostName,
            hostPort: hostPort,
            startListening: startListening,
            connectCaspar: connectCaspar,
            transitions: transitions,
            setTransition: setTransition,
            currentTransition: currentTransition,
            tween: tween,
            setTween: setTween,
            direction: direction,
            setDirection: setDirection,
            setDuration: setDuration,
            setDelay: setDelay,
            alertCount: alertCount,
            clearAlerts: clearAlerts,
            vidsrc: vidsrc,
            videoeditorcaspar: videoeditorcaspar,
            videoselectionfrom: videoselectionfrom,
            videoselectionto: videoselectionto,
            onclose: onclose,
            cl: cl,
            updateRundownItemSequence: updateRundownItemSequence,
            serverStatus: serverStatus,
            cMenuAction: cMenuAction,
            playCompleteCurrentRundown: playCompleteCurrentRundown,
            setTransitionTab: setTransitionTab,
            emptyPopupFlag: emptyPopupFlag,
            sanSearchText: sanSearchText,
            sanResult: sanResult,
            reportBug: reportBug,
            reportBugName: reportBugName,
            reportBugEmail: reportBugEmail,
            reportBugContact: reportBugContact,
            reportBugDetail: reportBugDetail,
            mediaSearchText: mediaSearchText,
            updateTemplateWindowsOnDrag: updateTemplateWindowsOnDrag,
            deckLinkInput: deckLinkInput,
            loadingScreenFlag: loadingScreenFlag,
            minimizeWindow: minimizeWindow,
            previewItem: previewItem,
            currentLoggedInUserName: currentLoggedInUserName,
            verifyRundown: verifyRundown,
            browseFile: browseFile,
            visibleFourWindowPopupFlag: visibleFourWindowPopupFlag,
            getSanPagedData: getSanPagedData,
            sanPath: sanPath,
            localBolCgMedia: localBolCgMedia,
            filterlocalBolCgMedia: filterlocalBolCgMedia,
            sanSearchTypeId: sanSearchTypeId,
            getSanResult: getSanResult,
            currentTheme: currentTheme,
            listeningStatus: listeningStatus,
            pause: pause,
            resume: resume,
            stop: stop,
            saveTransitionPopupPanel: saveTransitionPopupPanel,
            updateRundownItemProperties: updateRundownItemProperties,
            selectedLanguage: selectedLanguage,
            progressBar: progressBar,
            audioLevel: audioLevel,
            updateAnchorPoints: updateAnchorPoints,
            currentStoryToDisplay: currentStoryToDisplay,
            previousStory: previousStory,
            nextStory: nextStory,
            setCurrentStory: setCurrentStory,
            VideoWalls: VideoWalls,
            browseFileWall: browseFileWall,
            sourceRoute: sourceRoute,
            setSourceRoute: setSourceRoute,
            destinationRoute: destinationRoute,
            setDestinationRoute: setDestinationRoute,
            takeRoute: takeRoute,
            notConnectedServer: notConnectedServer,
            connectedServer: connectedServer,
            resetRoute: resetRoute
        };
    });