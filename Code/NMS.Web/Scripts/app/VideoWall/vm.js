﻿define('vm',
    [
        'vm.home'
    ],

    function (home) {

        var vmDictionary = {};

        vmDictionary['.main-section'] = home;
   
   
        return {
            dictionary: vmDictionary
        };
    });