﻿define('binder',
    [
        'jquery',
        'ko',
        'config',
        'enum',
        'presenter',
        'vm'
    ],

    function ($, ko, config, e, presenter, vm) {
        
            bindPreLoginViews = function (views) {
                for (var view in views) {
                    var tempView = views[view];
                    
                    if (typeof tempView.bind === 'undefined' || tempView.bind) {
                        bind(vm.dictionary[tempView.view], tempView.view);
                    }
                  
                }
                
            },

            bind = function (vm, view) {

                var elements = getView(view);

                if (elements && elements.length > 0) {
                    for (var i = 0; i < elements.length; i++) {
                        ko.applyBindings(vm, elements[i]);
                    }
                }
            },

            bindPostLoginViews = function () {

            },

            deleteExtraViews = function () {
            },

            bindStartUpEvents = function () {
            },

            getView = function (viewName) {
                return $(viewName).get();
            };

        return {
            bindPreLoginViews: bindPreLoginViews,
            bindPostLoginViews: bindPostLoginViews,
            bindStartUpEvents: bindStartUpEvents,
            deleteExtraViews: deleteExtraViews
        }
    });