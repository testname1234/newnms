﻿define('route-config',
    ['config', 'router', 'vm', 'underscore'],
    function (config, router, vm, _) {
        var
            logger = config.logger,

            register = function (views) {

                var routeData = [];
                var objArr = [];

                for (var view in views) {
                    var tempView = views[view];
                    var existingView = _.find(objArr, function (r) {
                        return r.view == tempView.view;
                    });
                    if (existingView) {
                        existingView.routes.push({
                            route: tempView.url,
                            title: tempView.title,
                            callback: vm.dictionary[tempView.view].activate,
                            group: '.route-top'
                        });
                    } else {
                       


                        var obj = {
                            view: tempView.view,
                            routes:
                            [{
                                route: tempView.url,
                                title: tempView.title,
                                callback: vm.dictionary[tempView.view].activate,
                                group: '.route-top'
                            }]

                        };
                        objArr.push(obj);
                    }
                }
                router.registerMultiple(objArr);
                // Invalid routes
                var obj = {
                    view: '',
                    route: /.*/,
                    title: '',
                    callback: function () {
                        logger.error(config.toasts.invalidRoute);
                    }
                };
                router.register(obj);

                router.run();
                
            };


        return {
            register: register
           
        };
    });