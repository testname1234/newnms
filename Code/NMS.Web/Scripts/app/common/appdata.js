﻿define('appdata',
    ['ko', 'utils', 'moment', 'model.program', 'model.episode', 'model.user', 'helper', 'enum'],
    function (ko, utils, moment, Program, Episode, User, helper, e) {
        // Properties
        // ------------------------------

        var
            sessionKey = helper.getCookie('SessionKey'),
            currentUser = ko.observable(new User()),
            pollingRequired = ko.observable(false),
            currentChannel = ko.observable(),
            currentProgram = ko.observable(new Program()),
            currentEpisode = ko.observable(new Episode()),
            currentScreen = ko.observable(''),
            lastRoute = ko.observable(''),
            pollingRequired = ko.observable(false),
            views = {},
            lastUpdateTime = ko.observable(new Date().toISOString()),
            programSelectedDate = ko.observable(moment(moment().format('l')).toISOString()),
            tickerImagesList = ko.observableArray([]),
            isAllNewsFilterSelected = ko.observable(false),
            isProducerUploadResource = ko.observable(false),
            istickerPopupVisible = ko.observable(false),
            selectedNewsFile = ko.observable(''),
            lastCommand = ko.observable(''),
            currentCommand = ko.observable(''),
            isFilterPollingClick = ko.observable(false),
            checkTemplates = ko.observable(false),
            locationId = helper.getCookie('LocationId'),
            isRefreshStoryStatus = ko.observable(false),
            uploadProducerResources = ko.observableArray([]),
            currentResource = ko.observable(''),
            isDefaultTab = ko.observable(false),
            foldersList = ko.observableArray([]),
            programsList = ko.observableArray(),
            refreshGenericList = ko.observable(false),
            cachedFilters = ko.observable(''),
            currentTickerStatus = ko.observable(),
            selectedCategoryFilters = ko.observableArray(),
            selectedSourceFilters = ko.observableArray(),
            selectedExtraFilters = ko.observableArray(),
            notifyData = ko.observable(),
            isEventBit = ko.observable(false),
            selectedParentFilterIdCategory = ko.observable(0),
            selectedParentFilterIdSource = ko.observable(0),
            selectedParentFilterIdExtra = ko.observable(0),
            refreshNewsBucket = ko.observable(false),
            refreshTickers = ko.observable(false),
            refreshNewsFileList = ko.observable(false),
            selectedStoryFilter = ko.observableArray([]),

            lpPageCount = 50,
            lpStartIndex = 0,

            lpStartOffSet = 0,

            lpStartIndexHeadline = 0,
            lpPageCountHeadline = 5,

            lpPageCountTicker = 30,
            lpStartIndexTicker = 0,

            isTickerImageExist = ko.observable(false),
            isNewsCreated=ko.observable(false),
            tickerImageLastUpdateDate = moment().add('month', 1).toISOString(),

            lpFromDate = utils.getDefaultUTCDate(),
            lpToDate = moment().toISOString(),

            NLEfromDate = ko.observable(),
            NLEtoDate = ko.observable(),

            NLEProgramFilter = ko.observableArray([]),
            NLEDateFilter = ko.observableArray([]),
            NLETimeFilter = ko.observableArray([]),

            headLineDateFilter = ko.observableArray([]),
            headLineTimeFilter = ko.observableArray([]),

            tickerDateFilter = ko.observableArray([]),
            tickerTimeFilter = ko.observableArray([]),
            filterLastUpdateDateStr = ko.observable(''),
            refreshHeadlineFilter = ko.observable(false),
            selectPopUpResource = ko.observable(''),
            selectedFavouriteResource = ko.observable(''),
            unmarkSelectedResource=ko.observable(''),

            newsFilterLastUpdateDateStr = utils.getDefaultUTCDate(),
            newsLastUpdateDate = moment().toISOString(),
            tickerLastUpdateDate = moment().toISOString(),
            tickerLineLastUpdateDate = moment().toISOString(),


            tickerIsInEditMode = ko.observable(false),
            isArchival = ko.observable(false),

            searchKeywords = '',
            searchHeadlineKeywords = '',
            searchTickerKeywords = '',

            lpHeadlineFromDate = utils.getDefaultUTCDate(),
            lpHeadlineToDate = moment().add('month', 1).toISOString(),

            lpTickerFromDate = utils.getDefaultUTCDate(),
            lpTickerToDate = moment().add('month', 1).toISOString(),

            teamCalendarControlReference = ko.observable(),
            headlineCalendarControlReference = ko.observable(),
            tickerCalendarControlReference = ko.observable(),
            cropWidthFactor = 0,
            cropHeightFactor = 0,

            pageIndex = 1,
            lpNewsIndex = 0,

            lastChangeTime = moment().toISOString(),
            lastChangeTimeHeadline = moment().toISOString(),
            lastChangeTimeTicker = moment().toISOString(),

            lastUIRefreshTimeTicker = utils.getDefaultUTCDate(),
            lastUIRefreshTimeHeadline = utils.getDefaultUTCDate(),
            lastUIRefreshTime = utils.getDefaultUTCDate(),

            currentStoryTemplateId = 0,
            storyTemplateCommentLastUpdateDate = utils.getDefaultUTCDate(),
            lpNotificationsLastUpdateDate = moment().add(-1, 'days').toISOString(),
            lpAlertsLastUpdateDate = moment().add(-1, 'days').toISOString(),
            lpFromDateStr = utils.getDefaultUTCDate(),
            lpStoryScreenTemplateLastUpdateDate = utils.getDefaultUTCDate(),

            selectedNewsId = ko.observable(),
            dateCategories = ko.observableArray([]),
            userFavourites=ko.observableArray([]),
            currentTime = ko.observable(),
            canAddNews = ko.observable(false),
            arrangeNewsFlag = ko.observable(false),
            arrangeTickerFlag = ko.observable(false),
            isStorySavedDirty = ko.observable(false),
            currentAlertNewsId = ko.observable(''),
            currentNotificationAlertId = ko.observable(''),
            producerScrollTop = ko.observable('0px'),
            _tempWpfArgs,
            userId = $.trim(helper.getCookie('user-id')),
            roleId = helper.getCookie('role-id'),
            executedScriptIds = [],
            updateSuggestedFlowFlag = ko.observable(false),
            tickerUserCurrentView = ko.observable(e.TickerStatus.Pending),
            videoEditorToken = '',
            currentViewId = ko.observable(e.UserType.TickerProducer),
            tickerCategoryLastUpdateDate = moment().subtract('days', 700).utc().toISOString(),
            tickerCountByStatus = ko.observable({ approvedTickerCount: 0, onAirTickerCount: 0, onAiredTickerCount: 0 }),
            isVideoCutterInstalled = ko.observable(false),
            isNotifying = false,
            tickerRefresh = ko.observable(false),
            showCutterImageLoader = ko.observable(false),
            programFilterIds = [],
            isCameramanFill = ko.observable(false),
            discardedFilters = [76],
            isUserSourceFilterChange = ko.observable(false),
            currentHash = '',
            currentSelectedBureau = ko.observable(0),
            bureauTeamCount = ko.observable(0),
            bureauAllTickerCount = ko.observable(0),
            currentProgramStoryCount = ko.observable(0),
            currentWorkingTicker = ko.observable(0),
            lastResourceRetrieveDate = moment().add(-1, 'days').toISOString(),


            newsFileFolderHistoryLastUpdateDate,
            newsFileDetailsLastUpdateDate,
            newsFileStatusHistoryLastUpdateDate,
            newsFoldersLastUpdateDate,
            newsFilesLastUpdateDate,
            newsFileLastUpdateDateObservable = ko.observable(0),
            clearAll = ko.observable(false),
            newsFileResourcesLastUpdateDate,
             listHeader,
            newsrightitem,


            // Computed Properties
            // ------------------------------

            programSelector = ko.computed({
                read: function () {
                    var tempObj = {};

                    if (currentProgram() && !currentProgram().isNullo) {
                        tempObj["program"] = currentProgram().name;

                        if (currentEpisode() && !currentEpisode().isNullo) {
                            tempObj["episode"] = currentEpisode().startTimeDisplay();
                            canAddNews(true);
                        } else {
                            tempObj["episode"] = 'Select Episode';
                            canAddNews(false);
                        }
                    } else {
                        tempObj["program"] = 'Select Program';
                        tempObj["episode"] = 'Select Episode';
                        canAddNews(false);
                    }

                    tempObj["channel"] = currentChannel();
                    tempObj["data"] = programSelectedDate();

                    return tempObj;
                },
                deferEvaluation: false
            }),

            programDisplayDate = ko.computed({
                read: function () {
                    var today = moment(programSelectedDate()).calendar();
                    if (today == "Today")
                        return today + " | " + moment(programSelectedDate()).format('MMMM DD, YYYY');

                    return moment(programSelectedDate()).format('MMMM DD, YYYY');
                },
                deferEvaluation: false
            }),

            toggleTickerPopup = function () {
                istickerPopupVisible(!istickerPopupVisible());
            },

            // Methods
            // ------------------------------

            extractUserInformation = function () {
                var tempUser = new User();

                tempUser.id = parseInt($.trim(helper.getCookie('user-id')));
                tempUser.name = $.trim(helper.getCookie('UserName').toUpperCase());
                tempUser.displayName = $.trim(helper.getCookie('UserName').toUpperCase());
                tempUser.userType = parseInt($.trim(helper.getCookie('role-id')));

                currentUser(tempUser);
            },

            init = function () {
                if (currentUser().userType === e.UserType.Producer || currentUser().userType === e.UserType.FReporter) {
                    setTimeout(function interval() {
                        currentTime(new Date());
                        setTimeout(interval, 10000)
                    }, 1000);
                } else {
                    setInterval(function () {
                        currentTime(new Date());
                    }, 1000);
                }

                currentProgram().isNullo = true;
                currentEpisode().isNullo = true;
            };

        init();

        return {
            currentUser: currentUser,
            currentChannel: currentChannel,
            currentProgram: currentProgram,
            currentEpisode: currentEpisode,
            programSelectedDate: programSelectedDate,
            programSelector: programSelector,
            lpPageCount: lpPageCount,
            lpFromDate: lpFromDate,
            lpToDate: lpToDate,
            lpStartIndex: lpStartIndex,
            searchKeywords: searchKeywords,
            newsFilterLastUpdateDateStr: newsFilterLastUpdateDateStr,
            newsLastUpdateDate: newsLastUpdateDate,
            selectedNewsId: selectedNewsId,
            dateCategories: dateCategories,
            lastChangeTime: lastChangeTime,
            lastUIRefreshTime: lastUIRefreshTime,
            pageIndex: pageIndex,
            lpNewsIndex: lpNewsIndex,
            arrangeNewsFlag: arrangeNewsFlag,
            canAddNews: canAddNews,
            selectedCategoryFilters: selectedCategoryFilters,
            selectedSourceFilters: selectedSourceFilters,
            isAllNewsFilterSelected: isAllNewsFilterSelected,
            selectedExtraFilters: selectedExtraFilters,
            selectedParentFilterIdCategory: selectedParentFilterIdCategory,
            selectedParentFilterIdSource: selectedParentFilterIdSource,
            selectedParentFilterIdExtra: selectedParentFilterIdExtra,
            programDisplayDate: programDisplayDate,
            _tempWpfArgs: _tempWpfArgs,
            selectedStoryFilter: selectedStoryFilter,
            sessionKey: sessionKey,
            lpFromDateStr: lpFromDateStr,
            lpStoryScreenTemplateLastUpdateDate: lpStoryScreenTemplateLastUpdateDate,
            currentAlertNewsId: currentAlertNewsId,
            currentTime: currentTime,
            extractUserInformation: extractUserInformation,
            currentStoryTemplateId: currentStoryTemplateId,
            storyTemplateCommentLastUpdateDate: storyTemplateCommentLastUpdateDate,
            producerScrollTop: producerScrollTop,
            lpNotificationsLastUpdateDate: lpNotificationsLastUpdateDate,
            lpAlertsLastUpdateDate: lpAlertsLastUpdateDate,
            currentNotificationAlertId: currentNotificationAlertId,
            userId: userId,
            roleId:roleId,
            NLEfromDate: NLEfromDate,
            NLEtoDate: NLEtoDate,
            NLEProgramFilter: NLEProgramFilter,
            NLEDateFilter: NLEDateFilter,
            NLETimeFilter: NLETimeFilter,
            executedScriptIds: executedScriptIds,
            updateSuggestedFlowFlag: updateSuggestedFlowFlag,
            videoEditorToken: videoEditorToken,
            lpHeadlineFromDate: lpHeadlineFromDate,
            lpHeadlineToDate: lpHeadlineToDate,
            searchHeadlineKeywords: searchHeadlineKeywords,
            headLineDateFilter: headLineDateFilter,
            headLineTimeFilter: headLineTimeFilter,
            lpStartIndexHeadline: lpStartIndexHeadline,
            lastChangeTimeHeadline: lastChangeTimeHeadline,
            lastUIRefreshTimeHeadline: lastUIRefreshTimeHeadline,
            lpPageCountHeadline: lpPageCountHeadline,
            refreshHeadlineFilter: refreshHeadlineFilter,
            headlineCalendarControlReference: headlineCalendarControlReference,
            cropWidthFactor: cropWidthFactor,
            cropHeightFactor: cropHeightFactor,
            searchTickerKeywords: searchTickerKeywords,
            lpTickerFromDate: lpTickerFromDate,
            lpTickerToDate: lpTickerToDate,
            lpPageCountTicker: lpPageCountTicker,
            lpStartIndexTicker: lpStartIndexTicker,
            lastChangeTimeTicker: lastChangeTimeTicker,
            arrangeTickerFlag: arrangeTickerFlag,
            teamCalendarControlReference: teamCalendarControlReference,
            tickerCalendarControlReference: tickerCalendarControlReference,
            tickerUserCurrentView: tickerUserCurrentView,
            tickerDateFilter: tickerDateFilter,
            tickerTimeFilter: tickerTimeFilter,
            lastUIRefreshTimeTicker: lastUIRefreshTimeTicker,
            currentTickerStatus: currentTickerStatus,
            tickerIsInEditMode: tickerIsInEditMode,
            tickerCategoryLastUpdateDate: tickerCategoryLastUpdateDate,
            
            currentViewId: currentViewId,
            tickerCountByStatus: tickerCountByStatus,
            isVideoCutterInstalled: isVideoCutterInstalled,
            isStorySavedDirty: isStorySavedDirty,
            isNotifying: isNotifying,
            isArchival: isArchival,
            tickerRefresh: tickerRefresh,
            selectPopUpResource: selectPopUpResource,
            programFilterIds: programFilterIds,
            showCutterImageLoader: showCutterImageLoader,
            isCameramanFill: isCameramanFill,
            userFavourites: userFavourites,
            selectedFavouriteResource: selectedFavouriteResource,
            unmarkSelectedResource: unmarkSelectedResource,
            discardedFilters: discardedFilters,
            isProducerUploadResource: isProducerUploadResource,
            uploadProducerResources: uploadProducerResources,
            currentHash: currentHash,
            isUserSourceFilterChange: isUserSourceFilterChange,
            currentSelectedBureau: currentSelectedBureau,
            bureauTeamCount: bureauTeamCount,
            istickerPopupVisible: istickerPopupVisible,
            toggleTickerPopup: toggleTickerPopup,
            lastRoute: lastRoute,
            bureauAllTickerCount: bureauAllTickerCount,
            isRefreshStoryStatus: isRefreshStoryStatus,
            currentProgramStoryCount: currentProgramStoryCount,
            refreshNewsBucket: refreshNewsBucket,
            tickerLastUpdateDate:tickerLastUpdateDate,
            tickerLineLastUpdateDate: tickerLineLastUpdateDate,
            checkTemplates: checkTemplates,
            isTickerImageExist:isTickerImageExist,
            tickerImageLastUpdateDate: tickerImageLastUpdateDate,
            tickerImagesList: tickerImagesList,
            currentWorkingTicker: currentWorkingTicker,
            currentResource: currentResource,
            isDefaultTab: isDefaultTab,
            lastResourceRetrieveDate: lastResourceRetrieveDate,
            isNewsCreated: isNewsCreated,
            newsFileFolderHistoryLastUpdateDate:newsFileFolderHistoryLastUpdateDate,
            newsFileDetailsLastUpdateDate:newsFileDetailsLastUpdateDate,
            newsFileStatusHistoryLastUpdateDate:newsFileStatusHistoryLastUpdateDate,
            newsFoldersLastUpdateDate:newsFoldersLastUpdateDate,
            newsFilesLastUpdateDate:newsFilesLastUpdateDate,
            newsFileResourcesLastUpdateDate: newsFileResourcesLastUpdateDate,
            lastCommand: lastCommand,
            currentCommand: currentCommand,
            currentScreen: currentScreen,
            lastUpdateTime: lastUpdateTime,
            pollingRequired: pollingRequired,
            views: views,
            newsFileLastUpdateDateObservable: newsFileLastUpdateDateObservable,
            refreshTickers: refreshTickers,
            clearAll: clearAll,
            foldersList: foldersList,
            refreshGenericList: refreshGenericList,
            refreshNewsFileList: refreshNewsFileList,
            filterLastUpdateDateStr: filterLastUpdateDateStr,
            selectedNewsFile: selectedNewsFile,
            pollingRequired: pollingRequired,
            locationId: locationId,
            isFilterPollingClick: isFilterPollingClick,
            programsList: programsList,
            cachedFilters: cachedFilters,
            notifyData: notifyData,
            lpStartOffSet: lpStartOffSet,
            isEventBit: isEventBit
        };
    });