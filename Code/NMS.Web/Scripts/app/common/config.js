﻿define('config',
    ['mock/mock', 'toastr'],
    function (mock, toastr) {
        var
            // Properties
            //------------
            ApiBaseUrl = '/api/',

            UserConsoleApiUrl = 'http://localhost:9000/api/',

            ServerDateFormat = 'YYYY-MM-DD',

            sessionKeyName = "SessionKey",

            currentURL = "",

            roleid = "role-id",

            userid = "user-id",

            newsTypeId = "NewsTypeId",

            UserName = "UserName",

            locationId = "LocationId",

            ShortDateFormat = 'Do MMM YY-HH:MM',

            DateFormat = 'MMMM D, YYYY',

            DateTimeFormat = 'MMMM D, YYYY HH:MM:SS',

            ResourceUploadUrl = '/api/ResourceUploader/',

            MMSUsermanagementUrl = 'http://10.3.12.120/',

            MediaServerUrl = 'http://10.3.12.119/api/Resource/',

            GoogleImageApiUrl = 'https://ajax.googleapis.com/ajax/services/search/images?v=1.0',
            GoogleVideosApiUrl = 'https://api.dailymotion.com/videos?sort=relevance&fields=id,thumbnail_url,duration,url,title&search=',

            isNewsEdit = false;
        verificationscreenflag = '',

        bucketId = 1,
        apiKey = "H@);KDJ7Xd",
        googlebucketId = 160,
        googleApiKey = 'R#o56!LVNb',

        storeExpirationMs = (1000 * 60 * 60 * 24),
        logger = toastr,

        hashes = {
            production: {
                home: '#/home',
                topicSelection: '#/topicselection',
                producerArchivalMedia: '#/producer-archivalmedia',
                topicSelectionNewsFilter: '#/topicselection/news-filter',
                topicSelectionMedia: '#/topicselection/media',
                topicSelectionGuests: '#/topicselection/guests',
                sequence: '#/sequence',
                production: '#/production',
                pendingStories: '#/pending-stories',
                report: '#/report',
                myheadline: '#/myheadline',
                headlineUpdates: '#/headlineUpdates',
                myTicker: '#/ticker',
                rundown: '#/rundown',
                tickerReporter: '#/home',
                bureauTicker: '#/bureauticker',
                filterPreference: '#/filterPreference',
                uploadMedia: '#/upload-media',
                XMLParser: '#/XMLParser-view',
                channelTicker: '#/channelTickers',
                alertTicker: '#/alerttickers',
                breakingTicker: '#/breakingtickers',
                categoryTicker: '#/categorytickers',
                newsFlow: '#/newsFlow-view',
                tagnews:'#/tagnews'
            },
            nmsBol: {
                home: '#/newsportal',
                topicSelection: '#/newsreviewed',
                controllerOutput: '#/controller-view-output',
                reportnews: '#/reportnews',
                taggerInput: '#/tagger',
                taggedPending:'#/pending',
                filterSocial:'#/filterSocial',
                tagreport: '#/tagreport',
                tagnews: '#/tagnews',
                copywriter: '#/copywriter',
                updatenews: '#/updatenews',
                reportnewsName: 'Report News',
                eventNews: '#/event'
            },
            fieldreporterverification: {
                home: '#/home',
                pending: '#/pending',
                reports: '#/reports',
                getnews: '#/getnews'
            },
            channelreporterverification: {
                home: '#/home',
                pending: '#/pending',
                reports: '#/reports',
                getnews: '#/getnews'
            },
            radioreporterverification: {
                home: '#/home',
                pending: '#/pending',
                reports: '#/reports',
                getnews: '#/getnews'
            },
            newspaperverification: {
                home: '#/home',
                pending: '#/pending',
                reports: '#/reports',
                getnews: '#/getnews'
            }
        },

        viewIds = {
            v3: {
                contentViewer: "#content-viewer-main",
            },
            nmsBol: {
                homeView: "#home-view-nmsBol",
                topicSelectionView: "#topic-selection-view-nmsBol",
                shellTopNavView: "#shell-top-nav-view",
                contentViewer: "#content-viewer-main",
                shellLeftView: "#shell-left-view",
                shellBottomView: "#shell-bottom-view",
                filterPreference: '#filterPreference-view',
                controllerOutput: '#controller-view-output',
                reportnews: '#shared-reportnews',
                reportnews: '#shared-reportnews',
                taggerInput: '#tagger-view-nmsBol',
                filterSocial: '#filtersocial-view-nmsBol',
                taggedPending: '#taggerpending-view-nmsBol',
                tagnews: '#tagnews-view-nmsBol',
                tagreport: '#tagreport-view-nmsBol',
                copywriter: '#copy-writer-view',
                updatenews: '#reporter-update-view'
            },
            production: {
                homeView: "#home-view",
                topicSelectionView: "#topic-selection-view",
                producerArchivalMedia: "#producer-archivalmedia",
                topicSelectionNewsFilterView: "#topicselection-news-content-view",
                topicSelectionMediaView: "#topicselection-media-view",
                topicSelectionGuestsView: "topicselection-guests-view",
                sequenceView: "#sequence-view",
                productionView: "#production-view",
                pendingStoriesView: "#pending-stories-view",
                reportView: "#report-view",
                shellTopNavView: "#shell-top-nav-view",
                shellRightView: "#shell-right-view",
                shellLeftView: "#shell-left-view",
                shellBottomView: "#shell-bottom-view",
                contentViewer: "#content-viewer-main",
                programPreview: '#production-programpreview',
                cropContentViewer: '#content-viewer-crop',
                headlineView: '#headline-view',
                myheadlineView: '#myheadline-view',
                rundownView: "#rundown-view",
                myTickerView: "#myticker-view",
                tickershellLeft: '#tickershell-left-view',
                tickerRundown: '#rundown-view',
                tickerReporter: '#reporter-home',
                bureauTicker: '#bureauticker-view',
                filterPreference: '#filterPreference-view',
                uploadMediaView: '#upload-media-view',
                XMLParserView: '#XMLParser-view',
                channelTickers: '#channel-ticker-view',
                alertTicker: '#alert-ticker-view',
                breakingTicker: '#breaking-ticker-view',
                categoryTicker: '#category-ticker-view',
                newsFlow: "#newsFlow-view",
                shellTopNavView1: '#shell-top-nav-view1'

            },
            fieldreporterverification: {
                homeView: "#home-view",
                pendingView: "#pending-view",
                reports: "#reports-view",
                getNewsView: "#newsdetails-view",
                shellTopNavView: "#shell-top-nav-view",
                shellLeftView: "#shell-left-view",
                shellTop: "#shell-top",
                myNewsView: '.myNewsPanelHeader'
            },
            channelreporterverification: {
                homeView: '#home-view',
                pendingView: '#pending-view',
                shellTop: "#shell-top",
                shellTopNavView: "#shell-top-nav-view",
                shellLeftView: "#shell-left-view",
                getNewsView: "#newsdetails-view"

            },
            radioreporterverification: {
                homeView: '#home-view',
                pendingView: '#pending-view',
                shellTop: "#shell-top",
                shellTopNavView: "#shell-top-nav-view",
                shellLeftView: "#shell-left-view",
                getNewsView: "#newsdetails-view"

            },
            newspaperverification: {
                homeView: '#home-view',
                pendingView: '#pending-view',
                shellTop: "#shell-top",
                shellTopNavView: "#shell-top-nav-view",
                shellLeftView: "#shell-left-view",
                getNewsView: "#newsdetails-view"
            }
        },

        views = {
            channelreporter: {
                home: { title: 'Home', url: '#/home', isInTopMenu: true, view: ".channelHomePanel" },
                live: { title: 'Live', url: '#/live', isInTopMenu: true, view: ".channelHomePanel2" },
                reportNews: { title: 'Report News', url: '#/report-news', isInTopMenu: false, view: ".reportNewsChannelReporter" },
                mynews: { title: 'My News', url: '#/mynews', isInTopMenu: true, view: ".myNewsPanelHeader" },
                reports: { title: 'Reports', url: '#/reports', isInTopMenu: true, view: "#reports-view" },
                updatenews: { title: 'Update News', url: '#/updatenews', isInTopMenu: false, view: ".updateNewsLeftRight" },
                shellTop: { title: '', url: '', isInTopMenu: false, view: "#shell-top" },
                shell: { title: '', url: '', isInTopMenu: false, view: "#shell-top-nav-view" },


            },
            newspaperreporter: {
                home: { title: 'Home', url: '#/home', isInTopMenu: true, view: ".channelHomePanel" },
                reportNews: { title: 'Report News', url: '#/report-news', isInTopMenu: false, view: ".reportNewsChannelReporter" },
                mynews: { title: 'My News', url: '#/mynews', isInTopMenu: true, view: ".myNewsPanelHeader" },
                reports: { title: 'Reports', url: '#/reports', isInTopMenu: true, view: "#reports-view" },
                updatenews: { title: 'Update News', url: '#/updatenews', isInTopMenu: false, view: ".updateNewsLeftRight" },
                shellTop: { title: '', url: '', isInTopMenu: false, view: "#shell-top" },
                shell: { title: '', url: '', isInTopMenu: false, view: "#shell-top-nav-view" }
            },
            radioreporter: {
                home: { title: 'Home', url: '#/home', isInTopMenu: true, view: ".channelHomePanel" },
                reportNews: { title: 'Report News', url: '#/report-news', isInTopMenu: false, view: ".reportNewsChannelReporter" },
                mynews: { title: 'My News', url: '#/mynews', isInTopMenu: true, view: ".myNewsPanelHeader" },
                reports: { title: 'Reports', url: '#/reports', isInTopMenu: true, view: "#reports-view" },
                updatenews: { title: 'Update News', url: '#/updatenews', isInTopMenu: false, view: ".updateNewsLeftRight" },
                shellTop: { title: '', url: '', isInTopMenu: false, view: "#shell-top" },
                shell: { title: '', url: '', isInTopMenu: false, view: "#shell-top-nav-view" }
            },
            fieldreporter: {
                home: { title: 'Home', url: '#/home', isInTopMenu: true, view: "#home-view" },
                reportNews: { title: 'Report News', url: '#/report-news', isInTopMenu: true, view: ".reportNews" },
                submitPackage: { title: 'Submit Package', url: '#/submitpackage', isInTopMenu: true, view: "#submitpackage-view" },
                mynews: { title: 'My News', url: '#/mynews', isInTopMenu: true, view: ".myNewsPanelHeader" },
                reports: { title: 'Reports', url: '#/reports', isInTopMenu: true, view: "#reports-view" },
                updatenews: { title: 'Update News', url: '#/updatenews', isInTopMenu: false, view: ".updateNewsLeftRight" },
                shellTop: { title: '', url: '', isInTopMenu: false, view: "#shell-top" },
                shell: { title: '', url: '', isInTopMenu: false, view: "#shell-top-nav-view" }
            },
            v3: {
                home: { title: 'Home', url: '#/home', isInTopMenu: true, view: "#home-view" },
                reportNews: { title: 'Report News', url: '#/report-news', isInTopMenu: true, view: ".reportNews" },
                assignment: { title: 'Assignment', url: '#/assignment', isInTopMenu: true, view: ".reportNews", bind: false },
                //submitPackage: { title: 'Submit Package', url: '#/submitpackage', isInTopMenu: true, view: "#submitpackage-view" },
                mynews: { title: 'My News', url: '#/mynews', isInTopMenu: true, view: ".myNewsPanelHeader" },
                //reports: { title: 'Reports', url: '#/reports', isInTopMenu: true, view: "#reports-view" },
                updatenews: { title: 'Update News', url: '#/updatenews', isInTopMenu: false, view: ".updateNewsLeftRight" },
                shellTop: { title: '', url: '', isInTopMenu: false, view: "#shell-top" },
                shell: { title: '', url: '', isInTopMenu: false, view: "#shell-top-nav-view" },
            },
            publicreporter: {
                home: { title: 'Home', url: '#/home', isInTopMenu: true, view: "#home-view" },
                submitPackage: { title: 'Submit Package', url: '#/submitpackage', isInTopMenu: true, view: "#submitpackage-view" },
                mynews: { title: 'My News', url: '#/mynews', isInTopMenu: true, view: ".myNewsPanelHeader" },
                reports: { title: 'Reports', url: '#/reports', isInTopMenu: true, view: "#reports-view" },
                updatenews: { title: 'Update News', url: '#/updatenews', isInTopMenu: false, view: ".updateNewsLeftRight" },
                shellTop: { title: '', url: '', isInTopMenu: false, view: "#shell-top" },
                shell: { title: '', url: '', isInTopMenu: false, view: "#shell-top-nav-view" }
            },
            courtreporter: {
                home: { title: 'Home', url: '#/home', isInTopMenu: true, view: "#home-view" },
                reportNews: { title: 'Report News', url: '#/report-news', isInTopMenu: true, view: "#reporter-reportnews-view" },
                mynews: { title: 'My News', url: '#/mycase', isInTopMenu: true, view: ".myNewsPanelHeader" },
                reports: { title: 'Reports', url: '#/reports', isInTopMenu: true, view: "#reports-view" },
                updatenews: { title: 'Update News', url: '#/updatenews', isInTopMenu: false, view: ".updateNewsLeftRight" },
                shellTop: { title: '', url: '', isInTopMenu: false, view: "#shell-top" },
                shell: { title: '', url: '', isInTopMenu: false, view: "#shell-top-nav-view" }
            },
            policereporter: {
                home: { title: 'Home', url: '#/home', isInTopMenu: true, view: "#home-view" },
                reportNews: { title: 'Report News', url: '#/report-news', isInTopMenu: true, view: "#reporter-reportnews-view" },
                mynews: { title: 'My News', url: '#/mycase', isInTopMenu: true, view: ".myNewsPanelHeader" },
                reports: { title: 'Reports', url: '#/reports', isInTopMenu: true, view: "#reports-view" },
                updatenews: { title: 'Update News', url: '#/updatenews', isInTopMenu: false, view: ".updateNewsLeftRight" },
                shellTop: { title: '', url: '', isInTopMenu: false, view: "#shell-top" },
                shell: { title: '', url: '', isInTopMenu: false, view: "#shell-top-nav-view" }
            },
            videoswitcher: {
                home: { title: 'Home', url: '#/home', isInTopMenu: false, view: ".home-view-videoswitcher" },
            },
            audiouser: {
                home: { title: 'Home', url: '#/home', isInTopMenu: false, view: ".home-view-audio" },
            },
            graphicuser: {
                home: { title: 'Home', url: '#/home', isInTopMenu: false, view: ".home-view-graphic" },
            },
            videoplayuser: {
                home: { title: 'Home', url: '#/home', isInTopMenu: false, view: ".videoplayuser-view" },
            },

            usermanagement: {
                login: { title: 'Login', url: '#/index', isInTopMenu: false, view: "#login-view" },
            },
            anchor: {
                home: { title: 'Home', url: '#/home', isInTopMenu: false, view: "#home-view-anchor" },
            },
            teleprompter: {
                home: { title: 'Home', url: '#/home', isInTopMenu: false, view: ".teleprompter-view" },
            },
            casper: {
                home: { title: 'Home', url: '#/home', isInTopMenu: false, view: ".main-section" },
            },
            videowall: {
                home: { title: 'Home', url: '#/home', isInTopMenu: false, view: ".main-section" },
            },
            nmsBol: {
                newsReviewed: { title: 'NewsReviewed', url: '#/newsreviewed', isInTopMenu: false, view: ".main-section" },
                NewsPortal: { title: 'NewsPortal', url: '#/newsportal', isInTopMenu: false, view: ".main-section" },
                controllerOutput: { title: 'ControllerOutput', url: '#/controller-view-output', isInTopMenu: false, view: ".main-section" },
                reportNews: { title: 'Report News', url: '#/reportnews', isInTopMenu: true, view: ".main-section" },
                updatenews: { title: 'Update News', url: '#/updatenews', isInTopMenu: false, view: "#reporter-update-view" },
                taggerInput: { title: 'TaggerInput', url: '#/tagger', isInTopMenu: false, view: ".main-section" },
                FilterSocial: { title: 'FilterSocial', url: '#/filterSocial', isInTopMenu: false, view: ".main-section" },
                tagreport: { title: 'Tagger Report', url: '#/tagreport', isInTopMenu: true, view: ".main-section" },
                tagnews: { title: 'Tag News', url: '#/tagnews', isInTopMenu: true, view: ".main-section" },
                taggedPending: { title: 'TaggerPending', url: '#/pending', isInTopMenu: false, view: ".main-section" },
                copywriter: { title: 'CopyWriter', url: '#/copywriter', isInTopMenu: false, view: ".main-section" },
                
            },

        },

            templateNames = {
                sourceFilterVerification: 'usercontrol.newssourcefiltersVerification',
                sourceFilter: 'usercontrol.newssourcefilters',
                newsCategoryControl: 'usercontrol.newscategorycontrol',
                programSelectionAndInfo: 'usercontrol.programselectionandinfo',
                programFlowDesigner: 'usercontrol.programflowdesigner',
                screenTemplateDesigner: 'usercontrol.screentemplatedesigner',
                newsStatsControl: 'usercontrol.newsstats',
                audioPlayerControl: 'usercontrol.audiorecorder',
                calendarControl: 'usercontrol.calendar',
                pendingStoriesInfoControl: 'usercontrol.pendingstoriesinfo',
                tickerLineTemplate: 'usercontrol.tickerlines',
                categoryNewsTicker: 'usercontrol.categorynewsticker',
                latestNewsTicker: 'usercontrol.latestnewsticker',
                breakingNewsTicker: 'usercontrol.breakingnewsticker',
                refenceNewsFileDetail : 'parent_newsfiledetail',
                newsfileVerificationControl : 'newsfile.verificationcontrol',
                relatedNews: 'relatednews',
                relatedContent: 'relatedcontent',
                guestSelection: 'guestselection',
                contentBucket: 'contentbucket',
                story: 'story',
                newsfileView: 'news.file.view',
                newsfileDetail: 'newsfiledetail',
                headline: 'news.headline',
                newsInfo: 'news.info',
                newsFileInfo: 'newsfile.info',
                tagFileInfo: 'tagfile.info',
                storyInfo: 'story.info',
                programStoriesPreview: 'program.stories.preview',
                programRundown: 'program.rundown',
                programPreview: 'program.preview',
                scriptEditor: 'script.editor',
                audioCommentControl: 'news.audiocommentcontrol',
                storyFilter: 'storyfilter',
                pendingStoriesRelatedContent: 'pendingstories.relatedcontent',
                headlineInfo: 'headlineinfo',
                newsGridView: 'newsGridView',
                generalFilterControl: 'usercontrol.generalfilter',
                generalFilterTickerControl: 'usercontrol.generalfilterticker',
                tickerStatusInfo: 'tickerstatus.info',
                tickerInfo: 'ticker.info',
                newsTickerLines: 'news.tickerlines',
                newsFileView: 'newsfile.view',
                contextMenu: 'newsfile.contextmenu',
                tagreporfile : 'tag.file.view'
            },

            toasts = {
                changesPending: 'Please save or cancel your changes before leaving the page.',
                errorSavingData: 'Data could not be saved. Please check the logs.',
                errorGettingData: 'Could not retrieve data.  Please check the logs.',
                invalidRoute: 'Cannot navigate. Invalid route',
                retreivedData: 'Data retrieved successfully',
                savedData: 'Data saved successfully',
                cantProceed: 'Please select stories to proceed',
                Proceedsuccess: 'Stories saved successfully!'
            },

            messages = {
                viewModelActivated: 'viewmodel-activation'
            },

            eventIds = {
                onLogIn: "logged-in"
            },

            modalDialogIds = {
            },

            storageType = {
                localStorage: 0,
                sessionStorage: 1
            },

            localStorageKeys = {
                credentials: "credentials"
            },

            stateKeys = {
                lastView: 'state.active-hash'
            },

            throttle = 500,
            pageTransitionThrottle = 500,
            maxItemsPerPage = 10,
            pollingInterval = 5,
            newsFilePollingInterval = 5,
            displayPollingInterval = 1,
            reporterPollingInterval = 5,
            publicReporterPollingInterval = 5,
            _useMocks = false, // Set this to toggle mocks
            _useWebAPI = true,
            noimageUrl = '/content/images/producer/noimage.jpg',
            imgurl = '../../Content/images/reporter/article-image.jpg',
            pleaseWait = '../../Content/images/producer/pleaseWait3.png',
            sampleDataFile = '/App_data/sampledata.txt',

            dataserviceInit = function () {
            },
            useMocks = function (val) {
                if (val) {
                    _useMocks = val;
                    init();
                }
                return _useMocks;
            },

            configureExternalTemplates = function () {
                infuser.defaults.templatePrefix = "_";
                infuser.defaults.templateSuffix = ".tmpl.html?v=5.0.0.275";
                infuser.defaults.templateUrl = "/Tmpl";
               //infuser.defaults.ajax.cache = false;
            },
             CutterpresentScreen = function (obj) {
                 if (obj == 'canvasName')
                 { return 'myCanvas'; }
                 if (obj == 'canvasButton')
                 { return '#lasso'; } //#lasso1 , #lasso2 , #lasso3
                 if (obj == 'pointDiffer')
                 { return 120; }
                 if (obj == 'Screen')
                 { return '#content-viewer'; }
             },


     
            init = function () {
                if (_useMocks) {
                    dataserviceInit = mock.dataserviceInit;
                }
                if (!_useWebAPI) {
                    ApiBaseUrl = 'http://localhost:9000/api/';
                }
                dataserviceInit();
                configureExternalTemplates();
            };

        init();

        return {
            logger: logger,
            ApiBaseUrl: ApiBaseUrl,
            ResourceUploadUrl: ResourceUploadUrl,
            MediaServerUrl: MediaServerUrl,
            hashes: hashes,
            viewIds: viewIds,
            eventIds: eventIds,
            storageType: storageType,
            localStorageKeys: localStorageKeys,
            modalDialogIds: modalDialogIds,
            useMocks: useMocks,
            stateKeys: stateKeys,
            dataserviceInit: dataserviceInit,
            throttle: throttle,
            maxItemsPerPage: maxItemsPerPage,
            pageTransitionThrottle: pageTransitionThrottle,
            pollingInterval: pollingInterval,
            displayPollingInterval: displayPollingInterval,
            window: window,
            toasts: toasts,
            templateNames: templateNames,
            noimageUrl: noimageUrl,
            reporterPollingInterval: reporterPollingInterval,
            publicReporterPollingInterval: publicReporterPollingInterval,
            sampleDataFile: sampleDataFile,
            views: views,
            ShortDateFormat: ShortDateFormat,
            DateFormat: DateFormat,
            DateTimeFormat: DateTimeFormat,
            ServerDateFormat: ServerDateFormat,
            messages: messages,
            sessionKeyName: sessionKeyName,
            roleid: roleid,
            verificationscreenflag: verificationscreenflag,
            userid: userid,
            CutterpresentScreen: CutterpresentScreen,
            pleaseWait: pleaseWait,
            bucketId: bucketId,
            apiKey: apiKey,
            GoogleImageApiUrl: GoogleImageApiUrl,
            GoogleVideosApiUrl: GoogleVideosApiUrl,
            googlebucketId:googlebucketId,
            googleApiKey: googleApiKey,
            MMSUsermanagementUrl: MMSUsermanagementUrl,
            currentURL:currentURL,
            newsTypeId:newsTypeId,
            locationId: locationId,
            UserName: UserName,
            isNewsEdit: isNewsEdit,
            newsFilePollingInterval: newsFilePollingInterval
        }
    });