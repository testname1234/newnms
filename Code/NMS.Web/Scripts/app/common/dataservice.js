﻿define('dataservice',
    [
        'config',
        'appdata'
    ],
    function (config, appdata) {
        var
            init = function () {

                //#region Production
                sessionkey = config.sessionKeyName,
                sessionkeyvalue = appdata.sessionKey,

                $.ajaxSetup({
                    headers: { sessionkey: sessionkeyvalue },
                    contentType: "application/json; charset=utf-8"
                });

                amplify.request.define('login', 'ajax', {
                    url: config.ApiBaseUrl + 'Login',
                    dataType: 'json',
                    type: 'POST'
                });
                amplify.request.define('load-initial-data', 'ajax', {
                    url: config.ApiBaseUrl + 'News/LoadInitialData',

                    dataType: 'json',
                    type: 'POST'
                });

                amplify.request.define('load-initial-data-NMS', 'ajax', {
                    url: config.ApiBaseUrl + 'News/LoadInitialDataNMS',

                    dataType: 'json',
                    type: 'POST'
                });
                

                amplify.request.define('load-initial-data-ticker', 'ajax', {
                    url: config.ApiBaseUrl + 'Ticker/LoadInitialData',

                    dataType: 'json',
                    type: 'POST'
                });
                amplify.request.define('load-initial-data-production-team', 'ajax', {
                    url: config.ApiBaseUrl + 'Program/NLELoadInitialData',
                    dataType: 'json',
                    type: 'POST'
                });
                amplify.request.define('producer-polling', 'ajax', {
                    url: config.ApiBaseUrl + 'News/ProducerPolling',
                    dataType: 'json',
                    type: 'POST'
                });
                amplify.request.define('ticker-polling', 'ajax', {
                    url: config.ApiBaseUrl + 'Ticker/TickerPolling',
                    dataType: 'json',
                    type: 'POST'
                });
                amplify.request.define('get-news-bunch', 'ajax', {
                    url: config.ApiBaseUrl + 'News/GetBunch',
                    dataType: 'json',
                    type: 'POST'
                });
                amplify.request.define('get-bunch-with-count', 'ajax', {
                    url: config.ApiBaseUrl + 'News/GetBunchWithCount',
                    dataType: 'json',
                    type: 'POST'
                });
                amplify.request.define('get-more-bunch', 'ajax', {
                    url: config.ApiBaseUrl + 'News/GetMoreNews',
                    dataType: 'json',
                    type: 'POST'
                });
                amplify.request.define('get-news-by-ids', 'ajax', {
                    url: config.ApiBaseUrl + 'News/GetNewsByIDs/',
                    dataType: 'json',
                    type: 'POST'
                });

                amplify.request.define('generate-screen-template', 'ajax', {
                    url: config.ApiBaseUrl + 'Program/GenerateScreenTemplateImage',
                    dataType: 'json',
                    type: 'POST'
                });

                amplify.request.define('get-celebrity-by-term', 'ajax', {
                    url: config.ApiBaseUrl + 'Celebrity/GetCelebrityByTerm/',
                    dataType: 'json',
                    type: 'GET'
                });

                amplify.request.define('save-program-data', 'ajax', {
                    url: config.ApiBaseUrl + 'Program/InsertUpdateSlots/',
                    dataType: 'json',
                    type: 'POST'
                });

                amplify.request.define('get-total-time', 'ajax', {
                    url: config.ApiBaseUrl + 'Channel/GetTotalTime/',
                    dataType: 'json',
                    type: 'POST'
                });

                amplify.request.define('delete-story', 'ajax', {
                    url: config.ApiBaseUrl + 'Program/DeleteSlot/?id={SlotId}&newsGuid={NewsGuid}&episodeId={EpisodeId}&NewsBucketId={NewsBucketId}',
                    dataType: 'json',
                    type: 'GET'
                });

                amplify.request.define('broadcasted-newsfiledetail', 'ajax', {
                    url: config.ApiBaseUrl + 'newsfile/GetBroadcatedNewsFileDetail?parentnewsid={data}',
                    dataType: 'json',
                    type: 'GET'
                });

                amplify.request.define('save-story-screenflow', 'ajax', {
                    url: config.ApiBaseUrl + 'Program/SaveSlotScreenTemplateFlow/',
                    dataType: 'json',
                    type: 'POST'
                });

                amplify.request.define('save-screen-template', 'ajax', {
                    url: config.ApiBaseUrl + 'Program/SaveSlotScreenElements/',
                    dataType: 'json',
                    type: 'POST'
                });

                amplify.request.define('save-mediawall-template', 'ajax', {
                    url: config.ApiBaseUrl + 'Program/InsertMediaWallTemplate/',
                    dataType: 'json',
                    type: 'POST'
                });


                amplify.request.define('send-to-broadcast', 'ajax', {
                    url: config.ApiBaseUrl + 'Program/GenerateRunDown',
                    dataType: 'json',
                    type: 'POST'
                });
                amplify.request.define('send-to-broadcastRundown', 'ajax', {
                    url: config.ApiBaseUrl + 'NewsFile/GenerateRunDown',
                    dataType: 'json',
                    type: 'POST'
                });

                amplify.request.define('save-slot-screen-elements-nle', 'ajax', {
                    url: config.ApiBaseUrl + 'Program/NLESaveSlotScreenElements',
                    dataType: 'json',
                    type: 'POST'
                });

                amplify.request.define('delete-story-screen-template-resource', 'ajax', {
                    url: config.ApiBaseUrl + 'Program/deleteStoryScreenTemplateResource/{id}',
                    dataType: 'json',
                    type: 'POST'
                });
               
                amplify.request.define('get-template-comments', 'ajax', {
                    url: config.ApiBaseUrl + 'Program/GetSlotScreenTemplateComments/{id}',
                    dataType: 'json',
                    type: 'POST'
                });

                amplify.request.define('mark-as-executed', 'ajax', {
                    url: config.ApiBaseUrl + 'Program/MarkAsExecuted/{id}',
                    dataType: 'json',
                    type: 'POST'
                });

                amplify.request.define('convert-to-unicode', 'ajax', {
                    url: config.ApiBaseUrl + 'Program/GetUnicodeText/',
                    dataType: 'json',
                    type: 'POST'
                });

                amplify.request.define('update-celebrity-stats', 'ajax', {
                    url: config.ApiBaseUrl + 'Program/UpdateCelebrityStatistics',
                    dataType: 'json',
                    type: 'POST'
                });

                //#endregion

                amplify.request.define('Package-Data', 'ajax', {
                    url: config.ApiBaseUrl + 'News/Package',
                    dataType: 'json',
                    type: 'POST'
                });
                amplify.request.define('upload-file', 'ajax', {
                    url: config.ApiBaseUrl + 'upload',
                    processData: false,
                    contentType: false,
                    type: 'POST'
                });

                amplify.request.define('Add-News', 'ajax', {
                    url: config.ApiBaseUrl + 'NewsFile/Add',
                    dataType: 'json',
                    type: 'POST'
                });

                amplify.request.define('Add-News-NewsFile', 'ajax', {
                    url: config.ApiBaseUrl + 'NewsFile/AddNewsFileReturnNews',
                    dataType: 'json',
                    type: 'POST'
                });

                amplify.request.define('UpdateNewsFileWithHistory', 'ajax', {
                    url: config.ApiBaseUrl + 'NewsFile/UpdateNewsFileWithHistory',
                    dataType: 'json',
                    type: 'POST'
                });


                amplify.request.define('OrganizationTagsUpdate', 'ajax', {
                    url: config.ApiBaseUrl + 'NewsFile/OrganizationTagsUpdate',
                    dataType: 'json',
                    type: 'POST'
                });

                amplify.request.define('Delete-Video', 'ajax', {
                    url: config.ApiBaseUrl + 'Resource/Delete/{fileName}',
                    dataType: 'json',
                    type: 'DELETE'
                });
                amplify.request.define('Load-Mynews', 'ajax', {
                    url: config.ApiBaseUrl + 'NewsFile/GetAllMyNews',
                    dataType: 'json',
                    type: 'GET'
                });

                amplify.request.define('Load-Mynews', 'ajax', {
                    url: config.ApiBaseUrl + 'NewsFile/GetAllMyNews',
                    dataType: 'json',
                    type: 'GET'
                });

                amplify.request.define('GetNewsFileForTaggReport', 'ajax', {
                    url: config.ApiBaseUrl + 'NewsFile/GetNewsFileForTaggReport?lstUpdate={data}',
                    dataType: 'json',
                    type: 'GET'
                });

                amplify.request.define('MyNewsPolling', 'ajax', {
                    url: config.ApiBaseUrl + 'NewsFile/MyNewsPolling',
                    dataType: 'json',
                    type: 'POST'
                });
                

                amplify.request.define('Get-News', 'ajax', {
                    url: config.ApiBaseUrl + '/NewsFile/GetNews/{id}',
                    dataType: 'json',
                    type: 'GET'
                });

                amplify.request.define('Update-News', 'ajax', {
                    url: config.ApiBaseUrl + '/News/Update',
                    dataType: 'json',
                    type: 'POST'
                });

                amplify.request.define('verification-load-initial-data', 'ajax', {
                    url: config.ApiBaseUrl + 'News/VerificationLoadInitialData',
                    dataType: 'json',
                    type: 'POST'
                });

                amplify.request.define('mark-verify-news', 'ajax', {
                    url: config.ApiBaseUrl + 'News/MarkVerifyNews',
                    dataType: 'json',
                    type: 'POST'
                });

                amplify.request.define('insert-news-comments', 'ajax', {
                    url: config.ApiBaseUrl + 'News/InsertComment',
                    dataType: 'json',
                    type: 'POST'
                });

                amplify.request.define('Load-Categories', 'ajax', {
                    url: config.ApiBaseUrl + 'category/GetCategoryByTerm',
                    type: 'GET'
                });

                amplify.request.define('Load-Locations', 'ajax', {
                    url: config.ApiBaseUrl + 'Location/GetLocationByTerm',
                    type: 'GET'
                });

                amplify.request.define('reporter-polling', 'ajax', {
                    url: config.ApiBaseUrl + 'News/ReporterPolling',
                    dataType: 'json',
                    type: 'POST'
                });

                amplify.request.define('reporter-load-initial-data', 'ajax', {
                    url: config.ApiBaseUrl + 'News/ReporterLoadInitialData?withoutFolder={data}',
                    dataType: 'json',
                    type: 'GET'
                }),
                amplify.request.define('channel-reporter-load-initial-data', 'ajax', {
                    url: config.ApiBaseUrl + 'Channel/LoadChannelInitialData',
                    dataType: 'json',
                    type: 'GET'
                });

                amplify.request.define('channel-reporter-get-videos', 'ajax', {
                    url: config.ApiBaseUrl + 'Channel/RefreshData?id={id}&isLive={isLive}',
                    dataType: 'json',
                    type: 'GET'
                });

                amplify.request.define('Get-News-By-Term', 'ajax', {
                    url: config.ApiBaseUrl + '/News/GetNewsByTerm/{id}',
                    dataType: 'json',
                    type: 'GET'
                });

                amplify.request.define('ChannelVideo-Mark-Processed', 'ajax', {
                    url: config.ApiBaseUrl + '/Resource/MarkIsProcessed',
                    dataType: 'json',
                    type: 'GET'
                });

                amplify.request.define('newspaper-reporter-load-initial-data', 'ajax', {
                    url: config.ApiBaseUrl + 'NewsPaper/LoadInitialData',
                    dataType: 'json',
                    type: 'GET'
                });

                amplify.request.define('newspaper-reporter-get-by-date', 'ajax', {
                    url: config.ApiBaseUrl + 'NewsPaper/RefreshData',
                    dataType: 'json',
                    type: 'GET'
                });

                amplify.request.define('get-program-episode', 'ajax', {
                    url: config.ApiBaseUrl + 'Program/GetProgramEpisode',
                    dataType: 'json',
                    type: 'POST'
                });
                amplify.request.define('get-episode', 'ajax', {
                    url: config.ApiBaseUrl + 'Program/GetProgramEpisodeByRundownId/{Id}',
                    dataType: 'json',
                    type: 'GET'
                });

                amplify.request.define('radio-reporter-load-initial-data', 'ajax', {
                    url: config.ApiBaseUrl + 'Radio/LoadRadioInitialData',
                    dataType: 'json',
                    type: 'GET'
                });

                amplify.request.define('radio-reporter-get-by-date', 'ajax', {
                    url: config.ApiBaseUrl + 'Radio/RefreshData',
                    dataType: 'json',
                    type: 'GET'
                });

                amplify.request.define('get-program-screenflow', 'ajax', {
                    url: config.ApiBaseUrl + 'Program/GetProgramScreenFlow/?programId={ProgramId}&producerId={UserId}',
                    dataType: 'json',
                    type: 'GET'
                });

                amplify.request.define('Get-News-With-Updates', 'ajax', {
                    url: config.ApiBaseUrl + 'News/GetNewsWithUpdates/',
                    dataType: 'json',
                    type: 'GET'
                });

                amplify.request.define('get-program', 'ajax', {
                    url: config.ApiBaseUrl + 'Program/getprogram/{id}',
                    dataType: 'json',
                    type: 'GET'
                });
                amplify.request.define('save-production-team-data', 'ajax', {
                    url: config.ApiBaseUrl + 'Program/NLESaveSlotScreenElements',
                    dataType: 'json',
                    type: 'POST'
                });
                amplify.request.define('get-Episode-Mics-Cameras', 'ajax', {
                    url: config.ApiBaseUrl + 'program/GetTemplateMicAndCamera/{id}',
                    dataType: 'json',
                    type: 'GET'
                });
                amplify.request.define('production-team-polling', 'ajax', {
                    url: config.ApiBaseUrl + 'Program/NLEPolling',
                    dataType: 'json',
                    type: 'POST'
                });
                amplify.request.define('production-team-sendcomments', 'ajax', {
                    url: config.ApiBaseUrl + 'Program/SendMessage',
                    dataType: 'json',
                    type: 'POST'
                });
                amplify.request.define('user-management-login', 'ajax', {
                    url: config.ApiBaseUrl + 'User/LoginMMS/',
                    dataType: 'json',
                    type: 'POST'
                });
                amplify.request.define('play-btn', 'ajax', {
                    url: 'http://localhost:9000/api/Teleprompter/Play',
                    dataType: 'json',
                    type: 'GET'
                });
                amplify.request.define('delete-slotscreenTemplate', 'ajax', {
                    url: config.ApiBaseUrl + 'Program/DeleteSlotScreenTemplate/{id}',
                    dataType: 'json',
                    type: 'POST'
                });

                amplify.request.define('cropp-image', 'ajax', {
                    url: config.ApiBaseUrl + 'Resource/CroppImage/',
                    dataType: 'json',
                    type: 'POST'
                });

                amplify.request.define('mediapost-resource', 'ajax', {
                    url: config.ApiBaseUrl + 'Resource/MediaPostResource/',
                    dataType: 'json',
                    type: 'POST'
                });
                amplify.request.define('getMedia-info', 'ajax', {
                    url: config.ApiBaseUrl + 'Resource/GetAllResourceInfoByGuid/',
                    dataType: 'json',
                    type: 'POST'
                });


                amplify.request.define('search-resource', 'ajax', {
                    url: config.ApiBaseUrl + 'Resource/SearchResource/?term={term}&pageSize={pageSize}&pageNumber={pageNumber}&resourceTypeId={resourceTypeId}&bucketId={bucketId}&alldata={alldata}',
                    dataType: 'json',
                    type: 'GET'
                });

                amplify.request.define('search-resource-by-date', 'ajax', {
                    url: config.ApiBaseUrl + 'Resource/GetResourceByDate/?ResourceDate={ResourceDate}',
                    dataType: 'json',
                    type: 'GET'
                });

                amplify.request.define('search-metas', 'ajax', {
                    url: config.ApiBaseUrl + 'Resource/SearchMetas?term={term}&Metas={metaTags}&pageSize={pageSize}&pageNumber={pageNumber}&resourceTypeId={resourceTypeId}&bucketId={bucketId}&alldata={alldata}',
                    dataType: 'json',
                    type: 'GET'
                });
                amplify.request.define('get-all-filters', 'ajax', {
                    url: config.ApiBaseUrl + 'News/GetAllFilters',
                    dataType: 'json',
                    type: 'GET'
                });

                amplify.request.define('get-userFavourite', 'ajax', {
                    url: config.MediaServerUrl + 'GetUserFavourite/?UserId={userId}',
                    dataType: 'json',
                    type: 'GET'
                });

                amplify.request.define('delete-userFavourite', 'ajax', {
                    url: config.MediaServerUrl + 'DeleteUserFavourite/?UserId={UserId}&guid={guid}',
                    dataType: 'json',
                    type: 'GET'
                });


                amplify.request.define('addUserFavourite', 'ajax', {
                    url: config.MediaServerUrl + 'AddUserFavourite/',
                    dataType: 'json',
                    type: 'POST'
                });

                amplify.request.define('Get-resource-Meta-By-Guid', 'ajax', {
                    url: config.MediaServerUrl + 'GetResourceMetaByGuid/?Guid={Guid}',
                    dataType: 'json',
                    type: 'GET'
                });

                amplify.request.define('mark-notification-read', 'ajax', {
                    url: config.ApiBaseUrl + 'Notification/MarkAsRead/?nid={nid}&uid={uid}',
                    dataType: 'json',
                    type: 'GET'
                });

                amplify.request.define('get-programFlow', 'ajax', {
                    url: config.ApiBaseUrl + 'Program/GetSuggestedFlow/?programId={programId}&key={key}',
                    dataType: 'json',
                    type: 'GET'
                });
                amplify.request.define('generate-episode-preview', 'ajax', {
                    url: config.ApiBaseUrl + 'Program/GetRunDownPreviewByEpisodeId/?EpisodeId={EpisodeId}',
                    dataType: 'json',
                    type: 'GET'
                });
                amplify.request.define('get-more-headlines', 'ajax', {
                    url: config.ApiBaseUrl + 'Program/GetMoreHeadlines',
                    dataType: 'json',
                    type: 'POST'
                });
                amplify.request.define('get-more-tickers', 'ajax', {
                    url: config.ApiBaseUrl + 'Ticker/GetMoreTickers',
                    dataType: 'json',
                    type: 'POST'
                });

                amplify.request.define('insert-ticker', 'ajax', {
                    url: config.ApiBaseUrl + 'Ticker/InsertTicker',
                    dataType: 'json',
                    type: 'POST'
                });

                amplify.request.define('update-ticker-tickerLine', 'ajax', {
                    url: config.ApiBaseUrl + 'Ticker/UpdateTickerAndTickerLines',
                    dataType: 'json',
                    type: 'POST'
                });

                amplify.request.define('insert-ticker-line', 'ajax', {
                    url: config.ApiBaseUrl + 'Ticker/InsertTickerLine',
                    dataType: 'json',
                    type: 'POST'
                });

                amplify.request.define('update-tickerStatus', 'ajax', {
                    url: config.ApiBaseUrl + 'Ticker/UpdateTickerStatus',
                    dataType: 'json',
                    type: 'POST'
                });

                amplify.request.define('update-ticker', 'ajax', {
                    url: config.ApiBaseUrl + 'Ticker/UpdateTicker',
                    dataType: 'json',
                    type: 'POST'
                });

                amplify.request.define('update-ticker-line', 'ajax', {
                    url: config.ApiBaseUrl + 'Ticker/UpdateTickerLine',
                    dataType: 'json',
                    type: 'POST'
                });

                amplify.request.define('get-taskByNleOrStory', 'ajax', {
                    url: config.ApiBaseUrl + 'Program/GetSlotScreenTemplate/?id={id}',
                    dataType: 'json',
                    type: 'GET'
                });

                amplify.request.define('submit-tickers', 'ajax', {
                    url: config.ApiBaseUrl + 'Ticker/SubmitTickers/',
                    dataType: 'json',
                    type: 'POST'
                });
                amplify.request.define('save-tickercategory-data', 'ajax', {
                    url: config.ApiBaseUrl + 'Ticker/UpdateTickerCategorySequence',
                    dataType: 'json',
                    type: 'POST'
                });

                amplify.request.define('error-logging-js', 'ajax', {
                    url: config.ApiBaseUrl + 'News/JSErrorLogging/?Msg={Msg}&Url={Url}&Linenumber={Linenumber}&User={User}',
                    dataType: 'json',
                    type: 'POST'
                });

                amplify.request.define('search-celebrity', 'ajax', {
                    url: config.ApiBaseUrl + 'Resource/SearchCelebrity/?term={term}&pageSize={pageSize}&pageNumber={pageNumber}',
                    dataType: 'json',
                    type: 'POST'
                });
                amplify.request.define('save-ticker-sequence', 'ajax', {
                    url: config.ApiBaseUrl + 'Ticker/UpdateTickerSequence',
                    dataType: 'json',
                    type: 'POST'
                });
                amplify.request.define('save-tickerline-sequence', 'ajax', {
                    url: config.ApiBaseUrl + 'Ticker/UpdateTickerLineSequence',
                    dataType: 'json',
                    type: 'POST'
                });
                amplify.request.define('getUsersByWorkRoleId', 'ajax', {
                    url: config.ApiBaseUrl + 'User/GetUsersByWorkRoleId/?workRolesId={workRoleId}',
                    dataType: 'json',
                    type: 'GET'
                });
                amplify.request.define('submit-metaData-byUser', 'ajax', {
                    url: config.ApiBaseUrl + 'News/SubmitMetaDataByUser',
                    dataType: 'json',
                    type: 'POST'
                });

                amplify.request.define('updateResource', 'ajax', {
                    url: config.ApiBaseUrl + 'Resource/UpdateResources',
                    dataType: 'json',
                    type: 'POST'
                });

                amplify.request.define('markTemplateStatus', 'ajax', {
                    url: config.ApiBaseUrl + 'Program/MarkSlotScreenTemplateStatus',
                    dataType: 'json',
                    type: 'POST'
                });

                amplify.request.define('news-add-bucket', 'ajax', {
                    url: config.ApiBaseUrl + 'News/AddToNewsBucket',
                    dataType: 'json',
                    type: 'POST'
                });

                amplify.request.define('update-News-BucketSequence', 'ajax', {
                    url: config.ApiBaseUrl + 'News/updateNewsBucketSequence',
                    dataType: 'json',
                    type: 'POST'
                });

                amplify.request.define('update-user-password', 'ajax', {
                    url: config.ApiBaseUrl + 'User/ChangePassword/?userId={userId}&password={password}',
                    dataType: 'json',
                    type: 'GET'
                });

                amplify.request.define('update-OnAirTicker-MCR', 'ajax', {
                    url: config.ApiBaseUrl + 'Ticker/UpdateOnAirTickerToMCR',
                    dataType: 'json',
                    type: 'POST'
                });
                amplify.request.define('create-OnAir-Segment', 'ajax', {
                    url: config.ApiBaseUrl + 'Ticker/CreateSegment',
                    dataType: 'json',
                    type: 'POST'
                });
                amplify.request.define('insert-OnAir-TickerLine', 'ajax', {
                    url: config.ApiBaseUrl + 'Ticker/InsertOnAirTickerLine',
                    dataType: 'json',
                    type: 'POST'
                });
                amplify.request.define('mark-status-OnAir-TickerLine', 'ajax', {
                    url: config.ApiBaseUrl + 'Ticker/MarkOnAirTickerStatus',
                    dataType: 'json',
                    type: 'POST'
                });
                amplify.request.define('Delete-Group-OnAir', 'ajax', {
                    url: config.ApiBaseUrl + 'Ticker/DeleteGroup',
                    dataType: 'json',
                    type: 'POST'
                });

                amplify.request.define('Get-MCR-Breaking', 'ajax', {
                    url: config.ApiBaseUrl + 'Ticker/GetMCRBreakingTickers',
                    dataType: 'json',
                    type: 'GET'
                });

                amplify.request.define('Get-MCR-Latest', 'ajax', {
                    url: config.ApiBaseUrl + 'Ticker/GetMCRLatestTickers',
                    dataType: 'json',
                    type: 'GET'
                });

                amplify.request.define('Get-MCR-Category', 'ajax', {
                    url: config.ApiBaseUrl + 'Ticker/GetMCRCategoryTickers',
                    dataType: 'json',
                    type: 'GET'
                });

                amplify.request.define('create-newsFile', 'ajax', {
                    url: config.ApiBaseUrl + 'NewsFile/InsertUpdateNewsFile',
                    dataType: 'json',
                    type: 'POST'
                });
                amplify.request.define('update-newsFile', 'ajax', {
                    url: config.ApiBaseUrl + 'NewsFile/InsertUpdateNewsFile',
                    dataType: 'json',
                    type: 'POST'
                });
                amplify.request.define('publish-newsFile', 'ajax', {
                    url: config.ApiBaseUrl + 'NewsFile/PublishNews',
                    dataType: 'json',
                    type: 'POST'
                });
                

                amplify.request.define('get-all-newsfile', 'ajax', {
                    url: config.ApiBaseUrl + 'NewsFile/GetAllNewsFiles',
                    dataType: 'json',
                    type: 'POST'
                });

                amplify.request.define('update-newsfile-action', 'ajax', {
                    url: config.ApiBaseUrl + 'NewsFile/UpdateNewsFileAction',
                    dataType: 'json',
                    type: 'POST'
                });
                amplify.request.define('update-newsfile-status', 'ajax', {
                    url: config.ApiBaseUrl + 'NewsFile/UpdateNewsFileStatus',
                    dataType: 'json',
                    type: 'POST'
                });

                amplify.request.define('program-to-print', 'ajax', {
                    url: config.ApiBaseUrl + 'NewsFile/GetNewsFilesDetailByFolderId/?FolderId={FolderId}&StoryCount={StoryCount}',
                    dataType: 'json',
                    type: 'GET'
                });

                amplify.request.define('create-newsfile-rundownfolder', 'ajax', {
                    url: config.ApiBaseUrl + 'NewsFile/CreateRundownFolder',
                    dataType: 'json',
                    type: 'POST'
                });
                amplify.request.define('delete-newsfileResource', 'ajax', {
                    url: config.ApiBaseUrl + 'NewsFile/DeleteFileResouceByGuid/?guid={guid}',
                    dataType: 'json',
                    type: 'GET'
                }); 
                amplify.request.define('get-fileDetail', 'ajax', {
                    url: config.ApiBaseUrl + 'NewsFile/GetNews/?id={newsFileId}',
                    dataType: 'json',
                    type: 'GET'
                });
                amplify.request.define('send-twitt', 'ajax', {
                    url: config.ApiBaseUrl + 'Ticker/PostToTwitter',
                    dataType: 'json',
                    type: 'POST'
                });
                
                amplify.request.define('change-template', 'ajax', {
                    url: config.ApiBaseUrl + 'Ticker/ChangeTemplate/?type={type}',
                    dataType: 'json',
                    type: 'POST'
                });
                amplify.request.define('update-news-sequence', 'ajax', {
                    url: config.ApiBaseUrl + 'NewsFile/UpdateNewsFileSequence/?sequenceId={sequenceIds}',
                    dataType: 'json',
                    type: 'POST'
                });
                amplify.request.define('mark-newsfilestatus', 'ajax', {
                    url: config.ApiBaseUrl + 'NewsFile/MarkNewsFileStatus',
                    dataType: 'json',
                    type: 'POST'
                });
                amplify.request.define('update-newsfile-programRelated', 'ajax', {
                    url: config.ApiBaseUrl + 'NewsFile/UpdateProgramNewsFile',
                    dataType: 'json',
                    type: 'POST'
                });
                amplify.request.define('news-file-polling', 'ajax', {
                    url: config.ApiBaseUrl + 'NewsFile/ProducerPolling',
                    dataType: 'json',
                    type: 'POST'
                });
                amplify.request.define('create-ticker-bynews', 'ajax', {
                    url: config.ApiBaseUrl + 'Ticker/CreateTickerByNewsFileId?NewsFileId={newsFileId}',
                    dataType: 'json',
                    type: 'GET'
                });
                amplify.request.define('mark-socialmedia-status', 'ajax', {
                    url: config.ApiBaseUrl + 'NewsFile/MarkStatusSocialMediaNews?NewsFileId={NewsFileId}&Status={Status}',
                    dataType: 'json',
                    type: 'POST'
                });
                amplify.request.define('mark-newfile-deleted', 'ajax', {
                    url: config.ApiBaseUrl + 'NewsFile/MarkNewsFileDeleted?newsFileId={newsFileId}',
                    dataType: 'json',
                    type: 'GET'
                });
                amplify.request.define('update-newfile-sequence', 'ajax', {
                    url: config.ApiBaseUrl + 'NewsFile/UpdateNewsFileSequenceNo',
                    dataType: 'json',
                    type: 'POST'
                });
                amplify.request.define('mark-titlevoiceover-status', 'ajax', {
                    url: config.ApiBaseUrl + 'NewsFile/MarkTitleVoiceOverStatus',
                    dataType: 'json',
                    type: 'POST'
                });
                
                amplify.request.define('loadResourceAgainstTag', 'ajax', {
                    url: config.ApiBaseUrl + 'Tag/LoadResourceAgainstTag?term={term}&type={type}',
                    dataType: 'json',
                    type: 'GET'
                });
                //
                amplify.request.define('copy-newfile-tofolder', 'ajax', {
                    url: config.ApiBaseUrl + 'NewsFile/CopyNewsFileWithFolderId?NewsFileId={NewsFileId}&FolderId={FolderId}&CreatedBy={CreatedBy}',
                    dataType: 'json',
                    type: 'POST'
                });
               
                amplify.subscribe("request.ajax.preprocess", function (defnSettings, settings, ajaxSettings) {
                    if (ajaxSettings.type == "POST") {
                        // This will still include the variable that matches the URL substitution:
                        var _settings = $.extend(true, {}, defnSettings.data, settings.data, ajaxSettings.data);
                        ajaxSettings.data = JSON.stringify(_settings);
                    }
                });


            },
            updateTickerSequence = function (data) {
                return $.Deferred(function (deferred) {
                    amplify.request({
                        resourceId: 'save-ticker-sequence',
                        data: data,
                        success: deferred.resolve,
                        error: deferred.reject
                    })
                }).promise();
            },
            updateTickerLineSequence = function (data) {
                 return $.Deferred(function (deferred) {
                     amplify.request({
                         resourceId: 'save-tickerline-sequence',
                         data: data,
                         success: deferred.resolve,
                         error: deferred.reject
                     })
                 }).promise();
             },
            jsErrorLogging = function (data) {
                return $.Deferred(function (deferred) {
                    amplify.request({
                        resourceId: 'error-logging-js',
                        data: data,
                        success: deferred.resolve,
                        error: deferred.reject
                    })
                }).promise();
            },
            login = function (data) {
                return $.Deferred(function (deferred) {
                    amplify.request({
                        resourceId: 'login',
                        data: data,
                        success: deferred.resolve,
                        error: deferred.reject
                    })
                }).promise();
            },

            //#region Production

            loadInitialData = function (data) {
                return $.Deferred(function (deferred) {
                    amplify.request({
                        resourceId: 'load-initial-data',
                        data: data,
                        success: deferred.resolve,
                        error: deferred.reject
                    })
                }).promise();
            },

            

            loadInitialDataNMS = function (data) {
                return $.Deferred(function (deferred) {
                    amplify.request({
                        resourceId: 'load-initial-data-NMS',
                        data: data,
                        success: deferred.resolve,
                        error: deferred.reject
                    })
                }).promise();
            },


            loadInitialDataProductionTeam = function (data) {
                return $.Deferred(function (deferred) {
                    amplify.request({
                        resourceId: 'load-initial-data-production-team',
                        data: data,
                        success: deferred.resolve,
                        error: deferred.reject
                    })
                }).promise();
            },
            loadInitialDataTicker = function (data) {
                return $.Deferred(function (deferred) {
                    amplify.request({
                        resourceId: 'load-initial-data-ticker',
                        data: data,
                        success: deferred.resolve,
                        error: deferred.reject
                    })
                }).promise();
            },

            producerPolling = function (data) {
                return $.Deferred(function (deferred) {
                    amplify.request({
                        resourceId: 'producer-polling',
                        data: data,
                        success: deferred.resolve,
                        error: deferred.reject
                    })
                }).promise();
            },
            tickerPolling = function (data) {
                return $.Deferred(function (deferred) {
                    amplify.request({
                        resourceId: 'ticker-polling',
                        data: data,
                        success: deferred.resolve,
                        error: deferred.reject
                    })
                }).promise();
            },
            productionTeamPolling = function (data) {
                return $.Deferred(function (deferred) {
                    amplify.request({
                        resourceId: 'production-team-polling',
                        data: data,
                        success: deferred.resolve,
                        error: deferred.reject
                    })
                }).promise();
            },

            getNewsBunch = function (data) {
                return $.Deferred(function (deferred) {
                    amplify.request({
                        resourceId: 'get-news-bunch',
                        data: data,
                        success: deferred.resolve,
                        error: deferred.reject
                    })
                }).promise();
            },
            //

            GetNewsFileForTaggReport = function (data) {
                return $.Deferred(function (deferred) {
                    amplify.request({
                        resourceId: 'GetNewsFileForTaggReport',
                        data: { data : data },
                        success: deferred.resolve,
                        error: deferred.reject
                    })
                }).promise();
            },

            getNewsBunchWithCount = function (data) {
                return $.Deferred(function (deferred) {
                    amplify.request({
                        resourceId: 'get-bunch-with-count',
                        data: data,
                        success: deferred.resolve,
                        error: deferred.reject
                    })
                }).promise();
            },

            getMoreNews = function (data) {
                return $.Deferred(function (deferred) {
                    amplify.request({
                        resourceId: 'get-more-bunch',
                        data: data,
                        success: deferred.resolve,
                        error: deferred.reject
                    })
                }).promise();
            },

            getNewsByIds = function (data) {
                return $.Deferred(function (deferred) {
                    amplify.request({
                        resourceId: 'get-news-by-ids',
                        data: data,
                        success: deferred.resolve,
                        error: deferred.reject
                    })
                }).promise();
            },

            getProgramEpisodes = function (data) {
                return $.Deferred(function (deferred) {
                    amplify.request({
                        resourceId: 'get-program-episode',
                        data: data,
                        success: deferred.resolve,
                        error: deferred.reject
                    })
                }).promise();
            },
             getProgramEpisodeByRoId = function (data) {
                 return $.Deferred(function (deferred) {
                     amplify.request({
                         resourceId: 'get-episode',
                         data: data,
                         success: deferred.resolve,
                         error: deferred.reject
                     })
                 }).promise();
             },

            getProgramScreenFlow = function (data) {
                return $.Deferred(function (deferred) {
                    amplify.request({
                        resourceId: 'get-program-screenflow',
                        data: data,
                        success: deferred.resolve,
                        error: deferred.reject
                    })
                }).promise();
            },

            generateScreenTemplate = function (data) {
                return $.Deferred(function (deferred) {
                    amplify.request({
                        resourceId: 'generate-screen-template',
                        data: data,
                        success: deferred.resolve,
                        error: deferred.reject
                    })
                }).promise();
            },

            getCelebrityByTerm = function (data) {
                return $.Deferred(function (deferred) {
                    amplify.request({
                        resourceId: 'get-celebrity-by-term',
                        data: data,
                        success: deferred.resolve,
                        error: deferred.reject
                    })
                }).promise();
            },
            saveProgramData = function (data) {
                return $.Deferred(function (deferred) {
                    amplify.request({
                        resourceId: 'save-program-data',
                        data: data,
                        success: deferred.resolve,
                        error: deferred.reject
                    })
                }).promise();
            },

            saveTickerCategoryData = function (data) {
                return $.Deferred(function (deferred) {
                    amplify.request({
                        resourceId: 'save-tickercategory-data',
                        data: data,
                        success: deferred.resolve,
                        error: deferred.reject
                    })
                }).promise();
            },

            submitTickers = function (data) {
                return $.Deferred(function (deferred) {
                    amplify.request({
                        resourceId: 'submit-tickers',
                        success: deferred.resolve,
                        data: data,
                        error: deferred.reject
                    })
                }).promise();
            },
            GetTotalTime = function (data) {
                return $.Deferred(function (deferred) {
                    amplify.request({
                        resourceId: 'get-total-time',
                        data: data,
                        success: deferred.resolve,
                        error: deferred.reject
                    })
                }).promise();
            },
            removeStory = function (data) {
                return $.Deferred(function (deferred) {
                    amplify.request({
                        resourceId: 'delete-story',
                        data: data,
                        success: deferred.resolve,
                        error: deferred.reject
                    })
                }).promise();
            },
            GetBroadcastedNewsFileDetail = function (pID) {
                return $.Deferred(function (deferred) {
                    amplify.request({
                        resourceId: 'broadcasted-newsfiledetail',
                        data: { data: pID },
                        success: deferred.resolve,
                        error: deferred.reject
                    })
                }).promise();
            },
             deleteslotscreenTemplate = function (data) {
                 return $.Deferred(function (deferred) {
                     amplify.request({
                         resourceId: 'delete-slotscreenTemplate',
                         data: data,
                         success: deferred.resolve,
                         error: deferred.reject
                     })
                 }).promise();
             },

            getSlotScreenTemplateComments = function (data) {
                return $.Deferred(function (deferred) {
                    amplify.request({
                        resourceId: 'get-template-comments',
                        data: data,
                        success: deferred.resolve,
                        error: deferred.reject
                    })
                }).promise();
            },

            saveStoryScreenFlow = function (data) {
                return $.Deferred(function (deferred) {
                    amplify.request({
                        resourceId: 'save-story-screenflow',
                        data: data,
                        success: deferred.resolve,
                        error: deferred.reject
                    })
                }).promise();
            },

            saveScreenTemplate = function (data) {
                return $.Deferred(function (deferred) {
                    amplify.request({
                        resourceId: 'save-screen-template',
                        data: data,
                        success: deferred.resolve,
                        error: deferred.reject
                    })
                }).promise();
            },

            saveMediaWallTemplateOnServer = function (data) {
                return $.Deferred(function (deferred) {
                    amplify.request({
                        resourceId: 'save-mediawall-template',
                        data: data,
                        success: deferred.resolve,
                        error: deferred.reject
                    })
                }).promise();
            },

            sendProgramToBroadCast = function (data) {

                return $.Deferred(function (deferred) {
                    amplify.request({
                        resourceId: 'send-to-broadcast',
                        data: data,
                        success: deferred.resolve,
                        error: deferred.reject
                    })
                }).promise();
            },

            sendProgramRundownToBroadCast = function (data) {
                return $.Deferred(function (deferred) {
                    amplify.request({
                        resourceId: 'send-to-broadcastRundown',
                        data: data,
                        success: deferred.resolve,
                        error: deferred.reject
                    })
                }).promise();
            },
            
            saveProductionTeamData = function (data) {
                return $.Deferred(function (deferred) {
                    amplify.request({
                        resourceId: 'save-production-team-data',
                        data: data,
                        success: deferred.resolve,
                        error: deferred.reject
                    })
                }).promise();
            },
            sendComments = function (data) {
                return $.Deferred(function (deferred) {
                    amplify.request({
                        resourceId: 'production-team-sendcomments',
                        data: data,
                        success: deferred.resolve,
                        error: deferred.reject
                    })
                }).promise();
            },

            saveScreenTemplateDataForNLE = function (data) {
                return $.Deferred(function (deferred) {
                    amplify.request({
                        resourceId: 'save-slot-screen-elements-nle',
                        data: data,
                        success: deferred.resolve,
                        error: deferred.reject
                    })
                }).promise();
            },

            deleteStoryScreenTemplateResource = function (data) {
                return $.Deferred(function (deferred) {
                    amplify.request({
                        resourceId: 'delete-story-screen-template-resource',
                        data: data,
                        success: deferred.resolve,
                        error: deferred.reject
                    })
                }).promise();
            },

            markScriptAsExecuted = function (data) {
                return $.Deferred(function (deferred) {
                    amplify.request({
                        resourceId: 'mark-as-executed',
                        data: data,
                        success: deferred.resolve,
                        error: deferred.reject
                    })
                }).promise();
            },

            updateCelebrityStatistics = function (data) {
                return $.Deferred(function (deferred) {
                    amplify.request({
                        resourceId: 'update-celebrity-stats',
                        data: data,
                        success: deferred.resolve,
                        error: deferred.reject
                    })
                }).promise();
            },
            updateTickerAndTickerLines = function (data) {
                return $.Deferred(function (deferred) {
                    amplify.request({
                        resourceId: 'update-ticker-tickerLine',
                        data: data,
                        success: deferred.resolve,
                        error: deferred.reject
                    })
                }).promise();
            },

            getUrduUnicodeText = function (data) {
                return $.Deferred(function (deferred) {
                    amplify.request({
                        resourceId: 'convert-to-unicode',
                        data: data,
                        success: deferred.resolve,
                        error: deferred.reject
                    })
                }).promise();
            },

        //#endregion

        //#region Others

        Package = function (data) {
            return $.Deferred(function (deferred) {
                amplify.request({
                    resourceId: 'Add-News',
                    data: data,
                    success: deferred.resolve,
                    error: deferred.reject
                })
            }).promise();
        },

        upload = function (data) {
            return $.Deferred(function (deferred) {
                $.ajax({
                    type: "POST",
                    url: config.ApiBaseUrl + 'upload',
                    data: data,
                    processData: false,
                    contentType: false,
                    success: function (data) {
                    }
                });
            }).promise();
        },

        onsubmit = function (data) {
            return $.Deferred(function (deferred) {
                amplify.request({
                    resourceId: 'Add-News',
                    data: data,
                    success: deferred.resolve,
                    error: deferred.reject
                })
            }).promise();
        },

        UpdateNewsFileWithHistory = function (data) {
            return $.Deferred(function (deferred) {
                amplify.request({
                    resourceId: 'UpdateNewsFileWithHistory',
                    data: data,
                    success: deferred.resolve,
                    error: deferred.reject
                })
            }).promise();
        },//

        onsubmitNewsFile = function (data) {
            return $.Deferred(function (deferred) {
                amplify.request({
                    resourceId: 'Add-News-NewsFile',
                    data: data,
                    success: deferred.resolve,
                    error: deferred.reject
                })
            }).promise();
        },//

        OrganizationTagsUpdate = function (data) {
            return $.Deferred(function (deferred) {
                amplify.request({
                    resourceId: 'OrganizationTagsUpdate',
                    data: data,
                    success: deferred.resolve,
                    error: deferred.reject
                })
            }).promise();
        },

        deletevideofile = function (data) {
            data = { fileName: data };
            return $.Deferred(function (deferred) {
                amplify.request({
                    resourceId: 'Delete-Video',
                    data: data,
                    success: deferred.resolve,
                    error: deferred.reject
                })
            }).promise();
        },

        LoadMyNewsList = function (data) {
            return $.Deferred(function (deferred) {
                amplify.request({
                    resourceId: 'Load-Mynews',
                    data: data,
                    success: deferred.resolve,
                    error: deferred.reject
                })
            }).promise();
        },

        MyNewsPolling = function (data) {
            return $.Deferred(function (deferred) {
                amplify.request({
                    resourceId: 'MyNewsPolling',
                    data: data,
                    success: deferred.resolve,
                    error: deferred.reject
                })
            }).promise();
        },

        loadCategories = function (data) {
            return $.Deferred(function (deferred) {
                amplify.request({
                    resourceId: 'Load-Categories',
                    data: data,
                    success: deferred.resolve,
                    error: deferred.reject
                })
            }).promise();
        },

        loadLocations = function (data) {
            return $.Deferred(function (deferred) {
                amplify.request({
                    resourceId: 'Load-Locations',
                    data: data,
                    success: deferred.resolve,
                    error: deferred.reject
                })
            }).promise();
        },

        GetNews = function (data) {
            return $.Deferred(function (deferred) {
                amplify.request({
                    resourceId: 'Get-News',
                    data: data,
                    success: deferred.resolve,
                    error: deferred.reject
                })
            }).promise();
        },

        updateNews = function (data) {
            return $.Deferred(function (deferred) {
                amplify.request({
                    resourceId: 'Update-News',
                    data: data,
                    success: deferred.resolve,
                    error: deferred.reject
                })
            }).promise();
        },


        markNewsVerified = function (data) {
            return $.Deferred(function (deferred) {
                amplify.request({
                    resourceId: 'mark-verify-news',
                    data: data,
                    success: deferred.resolve,
                    error: deferred.reject
                });
            }).promise();
        },

        insertNewsComments = function (data) {
            return $.Deferred(function (deferred) {
                amplify.request({
                    resourceId: 'insert-news-comments',
                    data: data,
                    success: deferred.resolve,
                    error: deferred.reject
                });
            }).promise();
        },

        verificationLoadInitialData = function (data) {
            return $.Deferred(function (deferred) {
                amplify.request({
                    resourceId: 'verification-load-initial-data',
                    data: data,
                    success: deferred.resolve,
                    error: deferred.reject
                });
            }).promise();
        },

        reporterLoadInitialData = function (boolVal) {
            return $.Deferred(function (deferred) {
                amplify.request({
                    data: { data: boolVal },
                    resourceId: 'reporter-load-initial-data',
                    success: deferred.resolve,
                    error: deferred.reject
                });
            }).promise();
        },

        reporterPolling = function (data) {
            return $.Deferred(function (deferred) {
                amplify.request({
                    resourceId: 'reporter-polling',
                    data: data,
                    success: deferred.resolve,
                    error: deferred.reject
                })
            }).promise();
        },

        channelReporterLoadInitialData = function (data) {
            return $.Deferred(function (deferred) {
                amplify.request({
                    resourceId: 'channel-reporter-load-initial-data',
                    data: data,
                    success: deferred.resolve,
                    error: deferred.reject
                });
            }).promise();
        },

        getChannelVideosByDate = function (data,isLive) {
            return $.Deferred(function (deferred) {
                amplify.request({
                    resourceId: 'channel-reporter-get-videos',
                    data: { id: data.id, isLive: isLive },
                    success: deferred.resolve,
                    error: deferred.reject
                })
            }).promise();
        },

        getNewsByTerm = function (data) {
            return $.Deferred(function (deferred) {
                amplify.request({
                    resourceId: 'Get-News-By-Term',
                    data: data,
                    success: deferred.resolve,
                    error: deferred.reject
                })
            }).promise();
        },

        getNewsWithUpdates = function (data) {
            return $.Deferred(function (deferred) {
                amplify.request({
                    resourceId: 'Get-News-With-Updates',
                    data: data,
                    success: deferred.resolve,
                    error: deferred.reject
                })
            }).promise();
        },

        markVideoProcessed = function (data) {
            return $.Deferred(function (deferred) {
                amplify.request({
                    resourceId: 'ChannelVideo-Mark-Processed',
                    data: data,
                    success: deferred.resolve,
                    error: deferred.reject
                })
            }).promise();
        },

        newspaperReporterLoadInitialData = function (data) {
            return $.Deferred(function (deferred) {
                amplify.request({
                    resourceId: 'newspaper-reporter-load-initial-data',
                    data: data,
                    success: deferred.resolve,
                    error: deferred.reject
                });
            }).promise();
        },

        getNewsPapersByDateRange = function (data) {
            return $.Deferred(function (deferred) {
                amplify.request({
                    resourceId: 'newspaper-reporter-get-by-date',
                    data: data,
                    success: deferred.resolve,
                    error: deferred.reject
                });
            }).promise();
        },

        radioReporterLoadInitialData = function (data) {
            return $.Deferred(function (deferred) {
                amplify.request({
                    resourceId: 'radio-reporter-load-initial-data',
                    data: data,
                    success: deferred.resolve,
                    error: deferred.reject
                });
            }).promise();
        },

        getRadioByDateRange = function (data) {
            return $.Deferred(function (deferred) {
                amplify.request({
                    resourceId: 'radio-reporter-get-by-date',
                    data: data,
                    success: deferred.resolve,
                    error: deferred.reject
                });
            }).promise();
        },

        getProgram = function (data) {
            data = { id: data };
            return $.Deferred(function (deferred) {
                amplify.request({
                    resourceId: 'get-program',
                    data: data,
                    success: deferred.resolve,
                    error: deferred.reject
                });
            }).promise();
        },

        newspaperReporterLoadInitialData = function (data) {
            return $.Deferred(function (deferred) {
                amplify.request({
                    resourceId: 'newspaper-reporter-load-initial-data',
                    data: data,
                    success: deferred.resolve,
                    error: deferred.reject
                });
            }).promise();
        };
        getEpisodeMicsCameras = function (data) {
            data = { id: data };
            return $.Deferred(function (deferred) {
                amplify.request({
                    resourceId: 'get-Episode-Mics-Cameras',
                    data: data,
                    success: deferred.resolve,
                    error: deferred.reject
                });
            }).promise();
        };
        usermanagementlogin = function (data) {
            return $.Deferred(function (deferred) {
                amplify.request({
                    resourceId: 'user-management-login',
                    data: data,
                    success: deferred.resolve,
                    error: deferred.reject
                });
            }).promise();
        },
            play = function () {
                console.log('In play');
                return $.Deferred(function (deferred) {
                    amplify.request({
                        resourceId: 'play-btn',
                        success: deferred.resolve,
                        error: deferred.reject
                    });
                }).promise();
            },

           croppImage = function (data) {
               return $.Deferred(function (deferred) {
                   amplify.request({
                       resourceId: 'cropp-image',
                       data: data,
                       success: deferred.resolve,
                       error: deferred.reject
                   });
               }).promise();
           },

           mediaPostResource = function (data) {
               return $.Deferred(function (deferred) {
                   amplify.request({
                       resourceId: 'mediapost-resource',
                       data: data,
                       success: deferred.resolve,
                       error: deferred.reject
                   });
               }).promise();
           },

          getMediaResourceDownloadStatus = function (data) {
              return $.Deferred(function (deferred) {
                  amplify.request({
                      resourceId: 'getMedia-info',
                      data: data,
                      success: deferred.resolve,
                      error: deferred.reject
                  });
              }).promise();
          },

        searchResourceMetaByGuid = function (data) {
            return $.Deferred(function (deferred) {
                amplify.request({
                    resourceId: 'Get-resource-Meta-By-Guid',
                    data: { Guid: data.Guid },
                    success: deferred.resolve,
                    error: deferred.reject
                });
            }).promise();
        },
        searchResource = function (data) {
            return $.Deferred(function (deferred) {
                amplify.request({
                    resourceId: 'search-resource',
                    data: { term: data.term, pageSize: data.pageSize, pageNumber: data.pageNumber, resourceTypeId: data.resourceTypeId, bucketId: data.bucketId, alldata: data.alldata },
                    success: deferred.resolve,
                    error: deferred.reject
                });
            }).promise();
        },
        getResourceByDate = function (data) {
             return $.Deferred(function (deferred) {
                 amplify.request({
                     resourceId: 'search-resource-by-date',
                     data: { ResourceDate: data.ResourceDate },
                     success: deferred.resolve,
                     error: deferred.reject
                 });
             }).promise();
        },
        searchResourceByMeta = function (data) {
            return $.Deferred(function (deferred) {
                amplify.request({
                    resourceId: 'search-metas',
                    data: data,
                    data: data,
                    success: deferred.resolve,
                    error: deferred.reject
                });
            }).promise();
        },


        getAllFilters = function (data) {
            return $.Deferred(function (deferred) {
                amplify.request({
                    resourceId: 'get-all-filters',
                    data: data,
                    success: deferred.resolve,
                    error: deferred.reject
                });
            }).promise();
        },

         getUserFavourite = function (data) {
             return $.Deferred(function (deferred) {
                 amplify.request({
                     resourceId: 'get-userFavourite',
                     data: { userId: data.userId },
                     success: deferred.resolve,
                     error: deferred.reject
                 });
             }).promise();
         },

         deleteUserFavourite = function (data) {
             return $.Deferred(function (deferred) {
                 amplify.request({
                     resourceId: 'delete-userFavourite',
                     data: { UserId: data.UserId, guid: data.ResourceGuid },
                     success: deferred.resolve,
                     error: deferred.reject
                 });
             }).promise();
         },

         addUserFavourite = function (data) {
             return $.Deferred(function (deferred) {
                 amplify.request({
                     resourceId: 'addUserFavourite',
                     data: data,
                     success: deferred.resolve,
                     error: deferred.reject
                 });
             }).promise();
         },
         markNotificaitonRead = function (data) {
             return $.Deferred(function (deferred) {
                 amplify.request({
                     resourceId: 'mark-notification-read',
                     data: { nid: data.nid, uid: data.uid },
                     success: deferred.resolve,
                     error: deferred.reject
                 });
             }).promise();
         },
         generateEpisodePreview = function myfunction(data) {
             return $.Deferred(function (deferred) {
                 amplify.request({
                     resourceId: 'generate-episode-preview',
                     data: data,
                     success: deferred.resolve,
                     error: deferred.reject
                 });
             }).promise();

         },
         getMoreHeadlines = function (data) {
             return $.Deferred(function (deferred) {
                 amplify.request({
                     resourceId: 'get-more-headlines',
                     data: data,
                     success: deferred.resolve,
                     error: deferred.reject
                 });
             }).promise();
         },
        getMoreTickers = function (data) {
            return $.Deferred(function (deferred) {
                amplify.request({
                    resourceId: 'get-more-tickers',
                    data: data,
                    success: deferred.resolve,
                    error: deferred.reject
                });
            }).promise();
        },
        insertTicker = function (data) {
            return $.Deferred(function (deferred) {
                amplify.request({
                    resourceId: 'insert-ticker',
                    data: data,
                    success: deferred.resolve,
                    error: deferred.reject
                });
            }).promise();
        },

        insertTickerLines = function (data) {
            return $.Deferred(function (deferred) {
                amplify.request({
                    resourceId: 'insert-ticker-line',
                    data: data,
                    success: deferred.resolve,
                    error: deferred.reject
                });
            }).promise();
        },

        updateTickerStatus = function (data) {
            return $.Deferred(function (deferred) {
                amplify.request({
                    resourceId: 'update-tickerStatus',
                    data: data,
                    success: deferred.resolve,
                    error: deferred.reject
                });
            }).promise();
        },
        searchCelebrity = function (data) {
            return $.Deferred(function (deferred) {
                amplify.request({
                    resourceId: 'search-celebrity',
                    data: { term: data.term, pageSize: data.pageSize, pageNumber: data.pageNumber },
                    success: deferred.resolve,
                    error: deferred.reject
                });
            }).promise();
        },
         updateTicker = function (data) {
             return $.Deferred(function (deferred) {
                 amplify.request({
                     resourceId: 'update-ticker',
                     data: data,
                     success: deferred.resolve,
                     error: deferred.reject
                 });
             }).promise();
         },

         updateTickerLines = function (data) {
             return $.Deferred(function (deferred) {
                 amplify.request({
                     resourceId: 'update-ticker-line',
                     data: data,
                     success: deferred.resolve,
                     error: deferred.reject
                 });
             }).promise();
         },
         newsFileProducerPolling = function (data) {
             return $.Deferred(function (deferred) {
                 amplify.request({
                     resourceId: 'news-file-polling',
                     data: data,
                     success: deferred.resolve,
                     error: deferred.reject
                 });
             }).promise();
         },

         getSuggestedFlow = function (data) {
             return $.Deferred(function (deferred) {
                 amplify.request({
                     resourceId: 'get-programFlow',
                     data: data,
                     success: deferred.resolve,
                     error: deferred.reject
                 });
             }).promise();
         };

        getTaskDoneNLEOrStorywriter = function (data) {
            return $.Deferred(function (deferred) {
                amplify.request({
                    resourceId: 'get-taskByNleOrStory',
                    data: { id: data.templateId },
                    success: deferred.resolve,
                    error: deferred.reject
                });
            }).promise();
        },

        submitMetaDataByUser = function (data) {
            return $.Deferred(function (deferred) {
                amplify.request({
                    resourceId: 'submit-metaData-byUser',
                    data: data,
                    success: deferred.resolve,
                    error: deferred.reject
                });
            }).promise();
        },

        getUsersByWorkRoleId = function (data) {
            return $.Deferred(function (deferred) {
                amplify.request({
                    resourceId: 'getUsersByWorkRoleId',
                    data: { workRoleId: data },
                    success: deferred.resolve,
                    error: deferred.reject
                });
            }).promise();
        },
        changePassword = function (data) {
            return $.Deferred(function (deferred) {
                amplify.request({
                    resourceId: 'update-user-password',
                    data: { userId: data.userId, password: data.password },
                    success: deferred.resolve,
                    error: deferred.reject
                });
            }).promise();
        },
        deletenewsFileResource = function (data) {
            return $.Deferred(function (deferred) {
                amplify.request({
                    resourceId: 'delete-newsfileResource',
                    data: { guid: data.guid},
                    success: deferred.resolve,
                    error: deferred.reject
                });
            }).promise();
        },
        getnewsFileDetail = function (data) {
            return $.Deferred(function (deferred) {
                amplify.request({
                    resourceId: 'get-fileDetail',
                    data: { newsFileId: data.newsFileId },
                    success: deferred.resolve,
                    error: deferred.reject
                });
            }).promise();
        },
        UpdateNewsSequence = function (data) {
            console.log(data);
            return $.Deferred(function (deferred) {
                amplify.request({
                    resourceId: 'update-news-sequence',
                    data: { sequenceIds: data.sequenceIds },
                    success: deferred.resolve,
                    error: deferred.reject
                });
            }).promise();
        },
        createTickerBynewsFileId = function (data) {
            console.log(data);
            return $.Deferred(function (deferred) {
                amplify.request({
                    resourceId: 'create-ticker-bynews',
                    data: { newsFileId: data.newsFileId },
                    success: deferred.resolve,
                    error: deferred.reject
                });
            }).promise();
        },
       markNewsFileDeleted = function (data) {
            console.log(data);
            return $.Deferred(function (deferred) {
                amplify.request({
                    resourceId: 'mark-newfile-deleted',
                    data: { newsFileId: data.newsFileId },
                    success: deferred.resolve,
                    error: deferred.reject
                });
            }).promise();
       },
       updateNewsFilesSequencing = function (data) {
            console.log(data);
            return $.Deferred(function (deferred) {
                amplify.request({
                    resourceId: 'update-newfile-sequence',
                    data: data,
                    success: deferred.resolve,
                    error: deferred.reject
                });
            }).promise();
       },
       markTitleVoiceOverStatus = function (data) {
            return $.Deferred(function (deferred) {
                amplify.request({
                    resourceId: 'mark-titlevoiceover-status',
                    data: data,
                    success: deferred.resolve,
                    error: deferred.reject
                });
            }).promise();
       },
        
        markNewsFileStatus = function (data) {
        
            return $.Deferred(function (deferred) {
                amplify.request({
                    resourceId: 'mark-newsfilestatus',
                    data:data ,
                    success: deferred.resolve,
                    error: deferred.reject
                });
            }).promise();
        },
        markSocialMediaStatus = function (data) {
        
            return $.Deferred(function (deferred) {
                amplify.request({
                    resourceId: 'mark-socialmedia-status',
                    data: { NewsFileId: data.NewsFileId , Status : data.Status },
                    success: deferred.resolve,
                    error: deferred.reject
                });
            }).promise();
        },
        
        updateProgramRelatedNews = function (data) {
       
            return $.Deferred(function (deferred) {
                amplify.request({
                    resourceId: 'update-newsfile-programRelated',
                    data:data ,
                    success: deferred.resolve,
                    error: deferred.reject
                });
            }).promise();
        },
        
        
         sendToTwitter = function (data) {
             return $.Deferred(function (deferred) {
                 amplify.request({
                     resourceId: 'send-twitt',
                     data: data,
                     success: deferred.resolve,
                     error: deferred.reject
                 });
             }).promise();
         },
          setTickerTemplate = function (data) {
              return $.Deferred(function (deferred) {
                  amplify.request({
                      resourceId: 'change-template',
                      data: {type:data.Type},
                      success: deferred.resolve,
                      error: deferred.reject
                  });
              }).promise();
          },
       
        
        updateResource = function (data) {
            return $.Deferred(function (deferred) {
                amplify.request({
                    resourceId: 'updateResource',
                    data: data,
                    success: deferred.resolve,
                    error: deferred.reject
                });
            }).promise();
        },


        addToNewsBucket = function (data) {
            return $.Deferred(function (deferred) {
                amplify.request({
                    resourceId: 'news-add-bucket',
                    data: data,
                    success: deferred.resolve,
                    error: deferred.reject
                });
            }).promise();
        },
        updateNewsBucketSequence = function (data) {
            return $.Deferred(function (deferred) {
                amplify.request({
                    resourceId: 'update-News-BucketSequence',
                    data: data,
                    success: deferred.resolve,
                    error: deferred.reject
                });
            }).promise();
        },
        updateOnAirTickerToMCR = function (data) {
            return $.Deferred(function (deferred) {
                amplify.request({
                    resourceId: 'update-OnAirTicker-MCR',
                    data: data,
                    success: deferred.resolve,
                    error: deferred.reject
                });
            }).promise();
        },
        createSegment = function (data) {
            return $.Deferred(function (deferred) {
                amplify.request({
                    resourceId: 'create-OnAir-Segment',
                    data: data,
                    success: deferred.resolve,
                    error: deferred.reject
                });
            }).promise();
        },
        insertOnAirTickerLine = function (data) {
            return $.Deferred(function (deferred) {
                amplify.request({
                    resourceId: 'insert-OnAir-TickerLine',
                    data: data,
                    success: deferred.resolve,
                    error: deferred.reject
                });
            }).promise();
        },
        markOnAirTickerStatus = function (data) {
            return $.Deferred(function (deferred) {
                amplify.request({
                    resourceId: 'mark-status-OnAir-TickerLine',
                    data: data,
                    success: deferred.resolve,
                    error: deferred.reject
                });
            }).promise();
        },
        deleteCompleteGroup = function (data) {
            return $.Deferred(function (deferred) {
                amplify.request({
                    resourceId: 'Delete-Group-OnAir',
                    data: data,
                    success: deferred.resolve,
                    error: deferred.reject
                });
            }).promise();
        },



        getMCRBreakingTickers = function (data) {
            return $.Deferred(function (deferred) {
                amplify.request({
                    resourceId: 'Get-MCR-Breaking',
                    data: data,
                    success: deferred.resolve,
                    error: deferred.reject
                });
            }).promise();
        },


        getMCRLatestTickers = function (data) {
            return $.Deferred(function (deferred) {
                amplify.request({
                    resourceId: 'Get-MCR-Latest',
                    data: data,
                    success: deferred.resolve,
                    error: deferred.reject
                });
            }).promise();
        },

        getMCRCategoryTickers = function (data) {
            return $.Deferred(function (deferred) {
                amplify.request({
                    resourceId: 'Get-MCR-Category',
                    data: data,
                    success: deferred.resolve,
                    error: deferred.reject
                });
            }).promise();
        },

        markTemplateStatus = function (data) {
            return $.Deferred(function (deferred) {
                amplify.request({
                    resourceId: 'markTemplateStatus',
                    data: data,
                    success: deferred.resolve,
                    error: deferred.reject
                });
            }).promise();
        },
         createNewsFile = function (data) {
             return $.Deferred(function (deferred) {
                 amplify.request({
                     resourceId: 'create-newsFile',
                     data: data,
                     success: deferred.resolve,
                     error: deferred.reject
                 });
             }).promise();
         },
         upadteNewsFile = function (data) {
             return $.Deferred(function (deferred) {
                 amplify.request({
                     resourceId: 'update-newsFile',
                     data: data,
                     success: deferred.resolve,
                     error: deferred.reject
                 });
             }).promise();
         },
        publishNewsFile = function (data) {
            return $.Deferred(function (deferred) {
                amplify.request({
                    resourceId: 'publish-newsFile',
                    data: data,
                    success: deferred.resolve,
                    error: deferred.reject
                });
            }).promise();
        },
        
         getAllNewsFile = function (data) {
              return $.Deferred(function (deferred) {
                  amplify.request({
                      resourceId: 'get-all-newsfile',
                      data: data,
                      success: deferred.resolve,
                      error: deferred.reject
                  });
              }).promise();
         },
       
        createRundownFolder = function (data) {
            return $.Deferred(function (deferred) {
                amplify.request({
                    resourceId: 'create-newsfile-rundownfolder',
                    data: data,
                    success: deferred.resolve,
                    error: deferred.reject
                });
            }).promise();
        },
         updateNewsFileAction = function (data) {
              return $.Deferred(function (deferred) {
                  amplify.request({
                      resourceId: 'update-newsfile-action',
                      data: data,
                      success: deferred.resolve,
                      error: deferred.reject
                  });
              }).promise();
          },
          updateNewsFileStatus = function (data) {
              return $.Deferred(function (deferred) {
                  amplify.request({
                      resourceId: 'update-newsfile-status',
                      data: data,
                      success: deferred.resolve,
                      error: deferred.reject
                  });
              }).promise();
          },
          copyNewsFileToFolder = function (data) {
              return $.Deferred(function (deferred) {
                  amplify.request({
                      resourceId: 'copy-newfile-tofolder',
                      data: { NewsFileId: data.NewsFileId, FolderId: data.FolderId, CreatedBy: data.CreatedBy},
                      success: deferred.resolve,
                      error: deferred.reject
                  });
              }).promise();
          },

        loadResourceAgainstTag = function (data) {
            return $.Deferred(function (deferred) {
                amplify.request({
                    resourceId: 'loadResourceAgainstTag',
                    data: { term: data.term, type: data.type},
                    success: deferred.resolve,
                    error: deferred.reject
                });
            }).promise();
        },

        programToPrint = function (data) {
            return $.Deferred(function (deferred) {
                amplify.request({
                    resourceId: 'program-to-print',
                    data: { FolderId: data.FolderId, StoryCount: data.MaxStoryCount },
                    success: deferred.resolve,
                    error: deferred.reject
                });
            }).promise();
        },
        

        //#endregion

        init();

        return {
            login: login,
            loadInitialData: loadInitialData,
            loadInitialDataProductionTeam: loadInitialDataProductionTeam,
            producerPolling: producerPolling,
            getNewsBunch: getNewsBunch,
            getNewsBunchWithCount: getNewsBunchWithCount,
            getMoreNews: getMoreNews,
            getNewsByIds: getNewsByIds,
            Package: Package,
            upload: upload,
            onsubmit: onsubmit,
            deletevideofile: deletevideofile,
            LoadMyNewsList: LoadMyNewsList,
            GetNews: GetNews,
            updateNews: updateNews,
            verificationLoadInitialData: verificationLoadInitialData,
            loadCategories: loadCategories,
            loadLocations: loadLocations,
            reporterPolling: reporterPolling,
            reporterLoadInitialData: reporterLoadInitialData,
            markNewsVerified: markNewsVerified,
            insertNewsComments: insertNewsComments,
            channelReporterLoadInitialData: channelReporterLoadInitialData,
            getChannelVideosByDate: getChannelVideosByDate,
            getNewsByTerm: getNewsByTerm,
            markVideoProcessed: markVideoProcessed,
            newspaperReporterLoadInitialData: newspaperReporterLoadInitialData,
            getNewsPapersByDateRange: getNewsPapersByDateRange,
            getProgramEpisodes: getProgramEpisodes,
            radioReporterLoadInitialData: radioReporterLoadInitialData,
            getRadioByDateRange: getRadioByDateRange,
            getProgramScreenFlow: getProgramScreenFlow,
            getNewsWithUpdates: getNewsWithUpdates,
            generateScreenTemplate: generateScreenTemplate,
            getCelebrityByTerm: getCelebrityByTerm,
            saveProgramData: saveProgramData,
            GetTotalTime: GetTotalTime,
            removeStory: removeStory,
            GetBroadcastedNewsFileDetail: GetBroadcastedNewsFileDetail,
            saveStoryScreenFlow: saveStoryScreenFlow,
            saveScreenTemplate: saveScreenTemplate,
            sendProgramToBroadCast: sendProgramToBroadCast,
            getProgram: getProgram,
            saveProductionTeamData: saveProductionTeamData,
            productionTeamPolling: productionTeamPolling,
            getEpisodeMicsCameras: getEpisodeMicsCameras,
            usermanagementlogin: usermanagementlogin,
            sendComments: sendComments,
            play: play,
            getProgramEpisodeByRoId: getProgramEpisodeByRoId,
            deleteslotscreenTemplate: deleteslotscreenTemplate,
            saveScreenTemplateDataForNLE: saveScreenTemplateDataForNLE,
            deleteStoryScreenTemplateResource: deleteStoryScreenTemplateResource,
            getSlotScreenTemplateComments: getSlotScreenTemplateComments,
            croppImage: croppImage,
            searchResource: searchResource,
            markNotificaitonRead: markNotificaitonRead,
            markScriptAsExecuted: markScriptAsExecuted,
            getSuggestedFlow: getSuggestedFlow,
            generateEpisodePreview: generateEpisodePreview,
            updateCelebrityStatistics: updateCelebrityStatistics,
            getMoreHeadlines: getMoreHeadlines,
            getMoreTickers: getMoreTickers,
            insertTicker: insertTicker,
            insertTickerLines: insertTickerLines,
            updateTickerStatus: updateTickerStatus,
            updateTicker: updateTicker,
            updateTickerLines: updateTickerLines,
            getTaskDoneNLEOrStorywriter: getTaskDoneNLEOrStorywriter,
            loadInitialDataTicker: loadInitialDataTicker,
            tickerPolling: tickerPolling,
            submitTickers: submitTickers,
            saveTickerCategoryData: saveTickerCategoryData,
            jsErrorLogging: jsErrorLogging,
            searchCelebrity: searchCelebrity,
            saveMediaWallTemplateOnServer: saveMediaWallTemplateOnServer,
            updateTickerSequence: updateTickerSequence,
            mediaPostResource: mediaPostResource,
            getMediaResourceDownloadStatus: getMediaResourceDownloadStatus,
            getUsersByWorkRoleId: getUsersByWorkRoleId,
            searchResourceMetaByGuid: searchResourceMetaByGuid,
            searchResourceMetaByGuid: searchResourceMetaByGuid,
            addUserFavourite: addUserFavourite,
            getUserFavourite: getUserFavourite,
            deleteUserFavourite: deleteUserFavourite,
            searchResourceByMeta: searchResourceByMeta,
            getUrduUnicodeText: getUrduUnicodeText,
            getAllFilters: getAllFilters,
            submitMetaDataByUser: submitMetaDataByUser,
            updateTickerAndTickerLines: updateTickerAndTickerLines,
            updateResource: updateResource,
            markTemplateStatus: markTemplateStatus,
            addToNewsBucket: addToNewsBucket,
            updateNewsBucketSequence: updateNewsBucketSequence,
            changePassword: changePassword,
            updateOnAirTickerToMCR: updateOnAirTickerToMCR,
            insertOnAirTickerLine: insertOnAirTickerLine,
            createSegment: createSegment,
            markOnAirTickerStatus: markOnAirTickerStatus,
            deleteCompleteGroup: deleteCompleteGroup,
            getMCRBreakingTickers: getMCRBreakingTickers,
            getMCRLatestTickers: getMCRLatestTickers,
            getMCRCategoryTickers: getMCRCategoryTickers,
            updateTickerLineSequence: updateTickerLineSequence,
            getResourceByDate: getResourceByDate,
            createNewsFile: createNewsFile,
            upadteNewsFile: upadteNewsFile,
            getAllNewsFile: getAllNewsFile,
            updateNewsFileAction: updateNewsFileAction,
            updateNewsFileStatus: updateNewsFileStatus,
            publishNewsFile: publishNewsFile,
            createRundownFolder: createRundownFolder,
            deletenewsFileResource: deletenewsFileResource,
            sendToTwitter: sendToTwitter,
            setTickerTemplate: setTickerTemplate,
            UpdateNewsSequence: UpdateNewsSequence,
            loadInitialDataNMS:loadInitialDataNMS,
            sendProgramRundownToBroadCast: sendProgramRundownToBroadCast,
            markNewsFileStatus: markNewsFileStatus,
            getnewsFileDetail: getnewsFileDetail,
            onsubmitNewsFile: onsubmitNewsFile,
            MyNewsPolling: MyNewsPolling,
            createTickerBynewsFileId: createTickerBynewsFileId,
            newsFileProducerPolling: newsFileProducerPolling,
            copyNewsFileToFolder: copyNewsFileToFolder,
            updateProgramRelatedNews: updateProgramRelatedNews,
            markNewsFileDeleted: markNewsFileDeleted,
            updateNewsFilesSequencing: updateNewsFilesSequencing,
            programToPrint: programToPrint,
            markSocialMediaStatus: markSocialMediaStatus,
            OrganizationTagsUpdate: OrganizationTagsUpdate,
            GetNewsFileForTaggReport: GetNewsFileForTaggReport,
            UpdateNewsFileWithHistory: UpdateNewsFileWithHistory,
            loadResourceAgainstTag: loadResourceAgainstTag,
            markTitleVoiceOverStatus: markTitleVoiceOverStatus
        }
    });