﻿define('enum',
    [],
    function () {
        var
            NewsType = {
                Story: 1,
                Package: 2,
                Assignment: 3
            },

            ContentType = {
                Image: 1,
                Video: 2,
                Audio: 3,
                Document: 4,
                Package: 8,
                None: 5,
            },

            NewsFilterType = {
                Website: 1,
                Radio: 2,
                SocialMedia: 3,
                NewsPaper: 4,
                PublicReporter: 6,
                FieldReporter: 7,
                Package: 8,
                Wire: 9,
                Channel: 10,
                Records: 11,
                PressRelease: 12,
                Category: 13,
                Verification: 15,
                Police: 16,
                Court: 17,
                Program: 18,
                NewsMedia: 19,
                TelevisionNews: 760,
                NewsPaperNews: 761,
                TickerNews: 762,
                EventType: 21
            },
            NewsFile = {
                AllNewsFile: 2,
                Bureau: 3,
                Rundown: 4,
                MarkStatus :10
            },
            NewsFileColorStatus = {
                Created: 1,
                CopyWriterApprovalPending : 2,
                ProductionApproved : 3,
                CopyWriterApproved : 4,
                CopyWriterAndProductionApproved :5,
                Deleted:7
            },
            NewsFileStatus={
                Red : 1,
                Blue : 2,
                Yellow : 3,
                Green : 4
            },
            NewsFileAction = {
                MoveTo: 2,
                CopyTo: 1,
                Edit: 3
            },
            ResourceStatus = {
                DownloadPending: 2,
                Completed:4,
            },
            Via = {
                PublicReporter: 1,
                FieldReporter: 2
            },

            GuestType = {
                Internal: 1,
                External: 2
            },
            MetaTypeId = {
             Filters : 7
            },
            ProgramElementType = {
                Break: 1,
                Segment: 2,
                ProgramStartup: 3,
                ProgramEnd: 4,
                Teaser: 5,
            },

            ProgramDesignerStep = {
                TemplateSelection: 1,
                ContentSelection: 2,
                Rundown: 3,
                Finished: 4,
            },

            ScreenElementType = {
                Anchor: 1,
                Video: 3,
                Graphic: 4,
                Guest: 8,
                Picture: 13,
                Logo: 19,
                Headline: 20,
                Ticker: 21,
                MediaWall: 22,
            },

            UserType = {
                Producer: 5,
                StoryWriter: 21,
                NLE: 20,
                TickerWriter: 49,
                TickerManager: 50,
                TickerProducer: 51,
                HeadlineProducer: 52,
                TickerReporter: 1047,
                FReporter: 3,
                Creporter: 7,
                NpReporter:10,
                CameraMan: 1048,
                NMSNewsFile: 1058,
                AllNewsProducer: 1067,
                controllerInput: 1072,
                Controlleroutput: 1073,
                taggedUser: 2072,
                tagreport: 2073,
                filterSocial: 2074,
                copyWriter: 2075,
                editorial:2076
            },


            NewsVerificationStatus = {
                Verified: 79,
                NotVerified: 80,
                Rejected: 81
            },

            CameraTypeId = {
                ENG: 1,
                DSNG: 2,
                VideoLibrary: 3,
                OtherChannel: 4
            },
            NewsFileFilterType = {
                FieldReporterFilter: 7,
                SocialMediaFilter: 24,
                ChannelReporterFilter: 10,
                EventType: 21
            },
 
            TickerStatus = {
                Pending: 5,
                OnHold: 2,
                Rejected: 3,
                Approved: 1,
                OnAired: 6,
                Deleted: 4,
                OnAir: 7,
                Freezed: 8

            },
            TickerType = {
                Breaking: { Value: 1, Text: 'Breaking' },
                Latest: { Value: 2, Text: 'Latest' },
                Category: { Value: 3, Text: 'Category' },
            },
            Severity = {
                Low: 1,
                Medium: 2,
                High: 3
            },

             Frequency = {
                 Low: 1,
                 Medium: 2,
                 High: 3
             },
            CommandSenderType = {
                PcrUser: 1,
                Teleprompter: 2,
                CasperClient: 3
            },

            NotificationType = {
            },
            NewsStatistics = {
                AddedToRundown: 1,
                OnAired: 2,
                ExecutedOnOtherChannels: 3
            },

            ConfigurationSetting = {
                MosId: 9,
                MosPort: 10,
                NcsId: 11,
                NcsPort: 12,
                IpAdress: 13,
                Tolerance: 14,
                AutoCheck: 15,
                AutoDelete: 16,
            },

            CommandType = {
                //prompter
                GetAllRunDowns_Prompter: 1,
                PlayPauseToggle_Prompter: 2,
                //Pause_Prompter: 3,
                Skip_Prompter: 4,
                UnSkip_Prompter: 5,
                StartListening_Prompter: 6,
                StopListening_Prompter: 7,
                PromptON_Prompter: 8,
                PromptOFF_Prompter: 9,
                FlipText_Prompter: 10,
                JumpToStory_Prompter: 11,
                Delete_Prompter: 12,
                CurrentSelectedRunOrder_Prompter: 13,
                UpdateSequence_Prompter: 23,
                ConnectHub_Prompter: 24,
                EditText_Prompter: 25,
                EditText_Save: 26,
                StartCleanUp_Prompter: 28,
                StopCleanUp_Prompter: 29,
                SetSpeed_Prompter: 30,
                PreviewON_Prompter: 35,
                PreviewOFF_Prompter: 36,
                ResetCurrentRunOrder_Prompter: 37,
                MappingModeOn_Prompter: 38,
                MappingModeOFF_Prompter: 39,
                MapActionToKey_Prompter: 40,
                NextStory_Prompter: 41,
                PreviousStory_Prompter: 42,
                DeleteProfile_Prompter: 43,
                LoadToPrompter_Prompter: 44,
                Tolerance_Prompter: 45,
                BrowseXML_Prompter: 46,
                NextStoryItem_Prompter: 47,
                PreviousStoryItem_Prompter: 48,
                RunOrderTop_Prompter: 49,
                RunOrderStoryTop_Prompter: 50,
                RunOrderStoryItemTop_Prompter: 51,
                PlayPauseToggleBackward_Prompter: 52,

                //casper
                Play_Caspar: 14,
                ClearGraphic_Caspar: 15,
                Connect_Caspar: 16,
                DisConnect_Caspar: 17,
                ListenRunOrder_Caspar: 18,
                LoadRunOrder_Caspar: 19,
                PlayItemWise_Caspar: 20,
                LoadInitialRunOrders_Caspar: 21,
                CurrentSelectedItem_Caspar: 27,
                CurrentFontSize_Prompter: 31,
                CurrentFontFamily_Prompter: 32,
                SetProfile_Prompter: 33,
                NewProfile_Prompter: 34,

                //pcrUser
                Notify_PcrUser: 22,

                // latest 52
            },

           CasparItemType = {
               Template: 1,
               Video: 2,
               Transformation: 3,
               Image: 4,
               Audio: 5,
               Crop: 6,
               DeckLinkInput: 7,
           },

            ScreenTemplateStatus = {
                InProcess: 1,
                ApprovalPending: 2,
                Approved: 3,
                Completed:4
            };


        return {
            NewsType: NewsType,
            NewsFilterType: NewsFilterType,
            ContentType: ContentType,
            Via: Via,
            GuestType: GuestType,
            ProgramElementType: ProgramElementType,
            ProgramDesignerStep: ProgramDesignerStep,
            NewsVerificationStatus: NewsVerificationStatus,
            UserType: UserType,
            ScreenElementType: ScreenElementType,
            CameraTypeId: CameraTypeId,
            CommandType: CommandType,
            CommandSenderType: CommandSenderType,
            ConfigurationSetting: ConfigurationSetting,
            CasparItemType: CasparItemType,
            NotificationType: NotificationType,
            NewsStatistics: NewsStatistics,
            TickerStatus: TickerStatus,
            Severity: Severity,
            TickerType: TickerType,
            Frequency: Frequency,
            ResourceStatus: ResourceStatus,
            MetaTypeId: MetaTypeId,
            ScreenTemplateStatus: ScreenTemplateStatus,
            NewsFile: NewsFile,
            NewsFileAction: NewsFileAction,
            NewsFileStatus: NewsFileStatus,
            NewsFileFilterType: NewsFileFilterType
        }
    });