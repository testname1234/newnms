﻿/// <reference path="../../lib/jquery-1.10.2.js" />
define('ko.bindingHandlers',
['jquery', 'ko', 'extensions', 'datacontext', 'underscore', 'moment', 'config', 'enum', 'helper'],
function ($, ko, ext, dc, _, moment, config, e, helper) {
    var unwrap = ko.utils.unwrapObservable;

    // escape
    //---------------------------
    ko.bindingHandlers.escape = {
        update: function (element, valueAccessor, allBindingsAccessor, viewModel) {
            var command = valueAccessor();
            $(element).keyup(function (event) {
                if (event.keyCode === 27) { // <ESC>
                    command.call(viewModel, viewModel, event);
                }
            });
        }
    };

    ko.bindingHandlers.datepicker = {
        init: function (element, valueAccessor, allBindingsAccessor) {
            //initialize datepicker with some optional options
            var options = allBindingsAccessor().datepickerOptions || {},
                $el = $(element);

            $el.datepicker(options);
            //handle the field changing
            ko.utils.registerEventHandler(element, "change", function () {
                var observable = valueAccessor();
                observable($el.datepicker("getDate"));
            });
            //handle disposal (if KO removes by the template binding)
            ko.utils.domNodeDisposal.addDisposeCallback(element, function () {
                $el.datepicker("destroy");
            });
        },
        update: function (element, valueAccessor) {
            var value = ko.utils.unwrapObservable(valueAccessor()),
                $el = $(element);

            //handle date data coming via json from Microsoft
            if (String(value).indexOf('/Date(') == 0) {
                value = new Date(parseInt(value.replace(/\/Date\((.*?)\)\//gi, "$1")));
            }

            var current = $el.datepicker("getDate");

            if (value - current !== 0) {
                $el.datepicker("setDate", value);
            }
        }
    };

    ko.bindingHandlers._tagIt = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            var source = allBindingsAccessor().source;
            var selectedObservableArrayInViewModel = valueAccessor();

            $(element).tagit({
                availableTags: source,
                allowSpaces: true,
                removeConfirmation: true,
                afterTagAdded: function (event, ui) {
                    selectedObservableArrayInViewModel.push($('.tagit-hidden-field', ui.tag).val());
                }
            });
        }
    };

    ko.bindingHandlers._multiSelect = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            var selectedObservableArrayInViewModel = valueAccessor();
            var source = selectedObservableArrayInViewModel.source;
            selectedObservableArrayInViewModel.onChange = function (option, checked) {
                var selectedItem = $(option).val();
                if (checked) {
                    source.push(selectedItem);
                }
                else {
                    source.remove(selectedItem);
                }
            };
            $(element).multiselect(selectedObservableArrayInViewModel);
        }
    };

    ko.bindingHandlers.editor = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            var selectedLanguage = $.trim(valueAccessor().language);
            var content = valueAccessor().content;
            var flag = false;
            var PlaceHolder = 'Enter Description...';

            var isContentSet = valueAccessor().isContentSet;
            if (selectedLanguage != '') {
                $(element).jqte({
                    change: function () {
                        if (isContentSet() && !flag) {
                            $(element).val(content());
                            flag = true;
                        }
                        var editorContent = $(element).val();
                        content(editorContent);
                    },

                    tabindex: '5'
                });
                $(element).jqteVal(content());
                $('.jqte_editor').attr('tabindex', '6');
                $('.jqte_editor').bind("paste", function (event) {
                    event.preventDefault();
                    //.replace(/<html[^]*<\/head>/, '').replace(/<body[^>]*>/,'').replace(/<\/body[^]*/,'')
                    var rawHtml = event.originalEvent.clipboardData.getData("text/html").replace(/style=['"][a-zA-Z0-9:;\.\s\(\)\-\,]*['"]/g, "");
                    if (rawHtml == '')
                        rawHtml = event.originalEvent.clipboardData.getData("text/plain");
                    //var text = event.originalEvent.clipboardData.getData("text/plain");
                    content(content() + $(rawHtml).text());
                    $(element).jqteVal(content());
                });
            }
        }
    };

    ko.bindingHandlers.videoUploader = {
        
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            var valueAcce = new valueAccessor();
            var url = valueAcce.url;
            var options = valueAcce.options;
            var src = valueAcce.src;
            var started = valueAcce.start;
            var finished = valueAcce.done;
            var resources = valueAcce.resources;
            var uploadProgress = valueAcce.progress;
            var tokenReceived = valueAcce.tokenReceived;
            var filesUploadedCount = 0;
            var oFilesUploadedCount = valueAcce.filesUploadedCount;
            var uploadedResources = valueAcce.uploadedResources;

            $(element).fileupload({
                url: url,
                acceptFileTypes: /(\.|\/)(gif|jpe?g|png|mp4|flv|mp3|wmv|avi|mpg|mpeg)$/i,
                add: function (e, data) {
                    var uploadFile = data.files[0];
                    if (!(/\.(gif|jpe?g|png|mp4|flv|mp3|wmv|avi|mpg|mpeg)$/i).test(uploadFile.name)) {
                        config.logger.error("Incorrect Format");
                    }
                    else {
                        if (bindingContext && bindingContext.$root.appdata && bindingContext.$root.appdata.currentProgram && bindingContext.$root.appdata.currentProgram().bucketId) {
                            var bucketId = bindingContext.$root.appdata.currentProgram().bucketId;
                            var apiKey = bindingContext.$root.appdata.currentProgram().apiKey;
                            apiKey = encodeURIComponent(apiKey);
                        }
                        else {
                            if (parseInt($.trim(helper.getCookie('BucketId').toUpperCase()))) {
                                var bucketId = parseInt($.trim(helper.getCookie('BucketId').toUpperCase()));
                            }

                            if ($.trim(helper.getCookie('ApiKey').toUpperCase())) {
                                var apiKey = $.trim(helper.getCookie('ApiKey').toUpperCase());
                            }
                        }

                        var source = "Reporter";

                        var userType = parseInt($.trim(helper.getCookie('role-id').toUpperCase()));
                        if (userType === 5 || userType === 52) {
                            source = "Producer";
                        }
                        if (userType === 3) {
                            source = "Field Reporter";
                        }
                        if (userType === 7) {
                            source = "Channel Reporter";
                        }
                        if (userType === 10) {
                            source = "NewsPaper Reporter";
                        }



                        var sourceTypeId = 7;
                        var sourceTypeName = $.trim(helper.getCookie('UserName'));
                        var sourceId = $.trim(helper.getCookie('user-id'));
                        $.getJSON('/api/resource/GetResourceIds?id=' + data.files[0].name + '&bucketId=' + bucketId + '&apiKey=' + apiKey + '&sourceTypeId=' + sourceTypeId + '&sourceTypeName=' + sourceTypeName + '&sourceId=' + sourceId + '&Source=' + source, function (result) {
                            if (result && result.IsSuccess) {
                                tokenReceived(true);
                                var resultdata = result.Data[0];
                                resources.push(resultdata);

                                options.headers = {};
                                options.headers['ResourceGuid'] = result.Data[0].Guid;

                                $(element).fileupload('option', options).bind('fileuploadstarted', function () {
                                    if (started) {
                                        started(true);
                                        finished(true);
                                    }
                                }).bind('fileuploadprogress', function (e, data) {
                                    var progress = parseInt(data.loaded / data.total * 100, 10);
                                    uploadProgress(progress + '%');
                                });
                                data.submit()
                                        .success(function (result, textStatus, jqXHR) {
                                        })
                                        .error(function (jqXHR, textStatus, errorThrown) {
                                        })
                                        .complete(function (result, textStatus, jqXHR) {
                                            filesUploadedCount++;
                                            oFilesUploadedCount(filesUploadedCount);
                                            uploadedResources.push(resultdata);

                                            if (bindingContext.$root.appdata && bindingContext.$root.appdata.currentUser && bindingContext.$root.appdata.currentUser().userType === bindingContext.$root.e.UserType.Producer && bindingContext.$root.appdata.currentUser().userType === bindingContext.$root.e.UserType.FReporter) {
                                                bindingContext.$root.appdata.uploadProducerResources.push(resultdata);
                                            }
                                            if (finished()) {
                                                if (result && result.responseJSON && result.responseJSON[0]) {
                                                    src(result.responseJSON[0].url);
                                                }
                                                if (bindingContext.$root.appdata && bindingContext.$root.appdata.currentUser && (bindingContext.$root.appdata.currentUser().userType === bindingContext.$root.e.UserType.NLE && bindingContext.$root.router && bindingContext.$root.config && bindingContext.$root.router.currentHash() != bindingContext.$root.config.hashes.production.uploadMedia)) {
                                                    setTimeout(function () {
                                                        bindingContext.$root.nleUploadResouces();
                                                        //$('.mediaDtls').children('p').children('.procesBtn').click();
                                                    }, 3000);

                                                }
                                                if (bindingContext.$root.appdata && bindingContext.$root.appdata.currentUser && (bindingContext.$root.appdata.currentUser().userType === bindingContext.$root.e.UserType.NLE && bindingContext.$root.router && bindingContext.$root.config && bindingContext.$root.router.currentHash() == bindingContext.$root.config.hashes.production.uploadMedia)) {
                                                    setTimeout(function () {
                                                        bindingContext.$root.nleUploadResouces();
                                                    }, 3000);

                                                }
                                                if (bindingContext.$root.appdata && bindingContext.$root.appdata.currentUser && bindingContext.$root.appdata.currentUser().userType === bindingContext.$root.e.UserType.Producer) {
                                                    setTimeout(function () {
                                                        bindingContext.$root.appdata.isProducerUploadResource(false);
                                                        bindingContext.$root.appdata.isProducerUploadResource(!bindingContext.$root.appdata.isProducerUploadResource());
                                                    }, 3000);
                                                }

                                            }

                                            finished(false);
                                        });
                            }
                        });
                    }
                }
            });
        }
    };

    ko.bindingHandlers.loadingBar = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            valueAccessor().showbar(false);
        },
    };

    ko.bindingHandlers.filterHover = {
        update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            var value = valueAccessor();
            var val = ko.unwrap(value);

            if (val > 0) {
                $(element).find("ul > li").hover(function () {
                    $(this).find('ul:first').stop(true, true).fadeIn();
                }, function () {
                    $(this).find('ul:first').stop(true, true).fadeOut();
                });
            }
        }
    };

    ko.bindingHandlers.categoryHover = {
        update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            var value = valueAccessor();
            var val = ko.unwrap(value);
            if (val == true) {
                $(element).mouseenter(function () {
                    $(this).addClass('active');
                    $(this).find('ul').stop(true, true).animate({ left: '50px' }, 200, 'linear');
                }).mouseleave(function () {
                    $(this).removeClass('active');
                    $(this).find('ul').stop(true, true).animate({ left: '-185px' }, 200, 'linear');
                });
            }
        }
    };

    ko.bindingHandlers.categoryClick = {
        update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            //$(element).on('click', function () {
            //    $(this).find('ul').parent('li').addClass("active");
            //    $(this).find('ul').stop(true, true).animate({ left: '50px' }, 200, 'linear');
            //    $(this).on('mouseleave', function () {
            //        $(this).find('ul').parent('li').removeClass("active");
            //        $(this).find('ul').stop(true, true).animate({ left: '-185px' }, 200, 'linear');
            //    });
            //});
        }
    };

    ko.bindingHandlers.dateTimePicker = {
        init: function (element, valueAccessor, allBindingsAccessor) {
            var selectedObservableArrayInViewModel = valueAccessor();
            var source = selectedObservableArrayInViewModel;
            //  for initial time
            var date = new Date(source());
            var hours = date.getHours();
            var minutes = date.getMinutes();
            var ampm = hours >= 12 ? 'pm' : 'am';
            hours = hours % 12;
            hours = hours ? hours : 12; // the hour '0' should be '12'
            minutes = minutes < 10 ? '0' + minutes : minutes;
            var cTime = hours + ':' + minutes + ' ' + ampm;
            $('select', element).each(function () {
                var $this = $(this), val = $this.children('option:first-child').text();
                if (!$this.parent().hasClass('xSelect')) {
                    $this.wrap('<div class="xSelect" style="position:relative;" />');
                    $this.parent().append('<p>' + val + '</p>');
                    $this.css({ 'position': 'absolute', 'top': '0px', 'left': '0px', 'width': '100%', 'height': '100%', 'z-index': '10', 'opacity': 0 });
                    $this.on('change', function () {
                        val = $(this).val();

                        $(this).parent().children('p').text(val);
                    });
                }
            });

            var myDate = new Date(source());
            var hours = myDate.getHours();
            var minutes = myDate.getMinutes();
            var ampm = hours >= 12 ? 'pm' : 'am';
            hours = hours % 12;
            hours = hours ? hours : 12; // the hour '0' should be '12'
            minutes = minutes < 10 ? '0' + minutes : minutes;
            var strTime = hours + ':' + minutes + ' ' + ampm;
            var monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
            var currentDate = myDate.getDate() + ' ' + (monthNames[myDate.getMonth()]) + ' ' + myDate.getFullYear();
            $("div.head > span:not(.time)", element).text(currentDate);
            $("div.head > span.time", element).text(', ' + strTime);

            $('.timepicker input.hour', element).val(hours);
            $('.timepicker input.min', element).val(minutes);
            if (ampm == 'am') {
                $('.timepicker select>option:eq(0)', element).attr('selected', true);
                $('.timepicker .xSelect p', element).text('am')
            } else {
                $('.timepicker select>option:eq(1)', element).attr('selected', true);
                $('.timepicker .xSelect p', element).text('pm')
            }

            $('.timepicker input[type=button]', element).click(function () {
                var hours = $('.timepicker input.hour', element).val();
                hours = hours < 10 ? '0' + hours : hours;
                var minutes = $('.timepicker input.min', element).val();
                minutes = minutes < 10 ? '0' + minutes : minutes;
                var amPm = $('.timepicker select.ampm', element).val();
                $("div.head > span.time", element).text(', ' + hours + ':' + minutes + ' ' + amPm);
                cTime = hours + ':' + minutes + ' ' + ampm;
                var date = $('.dates', element).text();
                source(date + ' ' + cTime);
            });

            $('div.head', element).click(function () {
                $(".optionsList .datepicker", element).datepicker({
                    showOn: "both",
                    inline: true,
                    showOtherMonths: true,
                    dayNamesMin: ['S', 'M', 'T', 'W', 'T', 'F', 'S'],
                    nextText: ' ',
                    prevText: ' ',
                    dateFormat: "dd MM yy",
                    onSelect: function (dateText, inst) {
                        var date = $(this).val();
                        source(date + ' ' + cTime);

                        $(".options.dateTime div.head > span:not(.time)").text(date);
                    }
                });
            });

            $('.timepicker').click(function (e) {
                e.stopPropagation();
            });
        },
        update: function (element, valueAccessor, allBindingsAccessor) {
            var selectedObservableArrayInViewModel = valueAccessor();
            var source = selectedObservableArrayInViewModel;
            //  for initial time
            var date = new Date(source());
            var hours = date.getHours();
            var minutes = date.getMinutes();
            var ampm = hours >= 12 ? 'pm' : 'am';
            hours = hours % 12;
            hours = hours ? hours : 12; // the hour '0' should be '12'
            minutes = minutes < 10 ? '0' + minutes : minutes;
            var cTime = hours + ':' + minutes + ' ' + ampm;
            $('select', element).each(function () {
                var $this = $(this), val = $this.children('option:first-child').text();
                if (!$this.parent().hasClass('xSelect')) {
                    $this.wrap('<div class="xSelect" style="position:relative;" />');
                    $this.parent().append('<p>' + val + '</p>');
                    $this.css({ 'position': 'absolute', 'top': '0px', 'left': '0px', 'width': '100%', 'height': '100%', 'z-index': '10', 'opacity': 0 });
                    $this.on('change', function () {
                        val = $(this).val();

                        $(this).parent().children('p').text(val);
                    });
                }
            });

            var myDate = new Date(source());
            var hours = myDate.getHours();
            var minutes = myDate.getMinutes();
            var ampm = hours >= 12 ? 'pm' : 'am';
            hours = hours % 12;
            hours = hours ? hours : 12; // the hour '0' should be '12'
            minutes = minutes < 10 ? '0' + minutes : minutes;
            var strTime = hours + ':' + minutes + ' ' + ampm;
            var monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
            var currentDate = myDate.getDate() + ' ' + (monthNames[myDate.getMonth()]) + ' ' + myDate.getFullYear();
            $("div.head > span:not(.time)", element).text(currentDate);
            $("div.head > span.time", element).text(', ' + strTime);

        }
    };

    ko.bindingHandlers.itemSelector = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            var selectedObservableArrayInViewModel = valueAccessor();
            var isSelected = selectedObservableArrayInViewModel.isSelected;
            var item = selectedObservableArrayInViewModel.item;
            var selectedItems = selectedObservableArrayInViewModel.selectedItems;

            if (isSelected()) {
                selectedItems.push(item);
            }

            else {
                selectedItems.remove(item);
            }
            $(element).click(function (e) {
                isSelected(!isSelected())
                if (isSelected()) {
                    selectedItems.push(item);
                }
                else {
                    selectedItems.remove(item);
                }
                e.stopPropagation();
            });
        }
    };

    ko.bindingHandlers.dropdown = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            $('div.head', element).click(function (e) {
                $(this).parent().addClass('active');
                $(".optionsList").mCustomScrollbar('destroy');
                $(".optionsList:not(.mCustomScrollbar)").mCustomScrollbar({
                    autoDraggerLength: true,
                    autoHideScrollbar: true,
                    scrollInertia: 100,
                    advanced: {
                        updateOnBrowserResize: true,
                        updateOnContentResize: true,
                        autoScrollOnFocus: false
                    }
                });
                $(this).closest('.options').siblings('.options').children('.optionsList').slideUp();
                $(this).closest('.options').children('.optionsList').slideToggle();
                $(this).toggleClass('active');
                e.stopPropagation();
            });
            $('body').click(function (e) {
                $('div.head', element).closest('.options').children('.optionsList').slideUp();
                //e.stopPropagation();
            });
        }
    };

    ko.bindingHandlers.dropDown = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            var selectedObservableArrayInViewModel = valueAccessor();
            var selector = selectedObservableArrayInViewModel.selector;
            $(element).click(function (e) {
                $(this).parent().children(selector).slideToggle();
                e.stopPropagation();
            });

            $('body').click(function () {
                $(element).parent().children(selector).slideUp();
            });
        }
    };

    ko.bindingHandlers.customHover = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            var selectedObservableArrayInViewModel = valueAccessor();
            var selector = selectedObservableArrayInViewModel.selector;

            $(element).mouseover(function (e) {
                //if (selector == '.tooltip') {
                //    $(element).children(selector).fadeOut();
                //    e.stopPropagation();
                //}
                //else {
                //    $(this).parent().children(selector).fadeOut();
                //    e.stopPropagation();
                //}
                console.log(element);
               
            });

            $(element).click(function (e) {
                if (selector == '.tooltip') {
                    $(element).children(selector).fadeIn();
                    e.stopPropagation();
                }
                else {
                    $(this).parent().children(selector).fadeIn();
                    e.stopPropagation();
                }
            });

            $('body').click(function () {
                $(selector).fadeOut();
            });

            $(selector).click(function (e) {
                e.stopPropagation();
            });
        }
    };

    ko.bindingHandlers.addTag = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            var selectedObservableArrayInViewModel = valueAccessor();
            var tags = selectedObservableArrayInViewModel.tags;
            $(element).keyup(function (event) {
                if (event.keyCode === 13) {
                    var value = $(this).val() + '  ';
                    //if (tags() && tags().length > 0 && tags()[tags().length - 1] == value) {
                    //    return true;
                    //}
                    $(this).val('');
                    if ($.trim(value) != '' && tags().indexOf($.trim(value)) < 0) {
                        tags.push(value);
                        $(".ui-autocomplete").hide();
                    }
                    //if ($.trim(value) != '' && tags.indexOf(value) < 0) {
                    //    tags.push(value);
                    //    if (bindingContext && bindingContext.$root.currentMetaSearchKeyword)
                    //    { bindingContext.$root.currentMetaSearchKeyword(value); }
                    //    $('.ui-autocomplete').attr('style', 'display:none');
                    //    $(element).removeClass('not_valid');
                    //}
                    else if ($.trim(value)=="") {
                        $(element).addClass('not_valid');
                    }
                }
            });
        }
    };//

    ko.bindingHandlers.addOrganization = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            var selectedObservableArrayInViewModel = valueAccessor();
            var tags = selectedObservableArrayInViewModel.tags;
            $(element).keyup(function (event) {
                if (event.keyCode === 13) {
                    var value = $(this).val() + '  ';
                    $(this).val('');
                    if ($.trim(value) != '' && tags().indexOf($.trim(value)) < 0) {
                        tags.push(value);
                        $(".ui-autocomplete").hide();
                    }
                    else if ($.trim(value) == "") {
                        $(element).addClass('not_valid');
                    }
                } else {
                    return false;
                }
            });
        }
    };

    ko.bindingHandlers.addEvent = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            var selectedObservableArrayInViewModel = valueAccessor();
            var tags = selectedObservableArrayInViewModel.tags;
            $(element).keyup(function (event) {
                if (event.keyCode === 13) {
                    var value = $(this).val() + '  ';
                    $(this).val('');
                    if ($.trim(value) != '') {
                        tags({ id: 0, value: value });
                        $(".ui-autocomplete").hide();
                    }
                    else if ($.trim(value) == "") {
                        $(element).addClass('not_valid');
                    }
                } else {
                    return false;
                }
            });
        }
    };

    ko.bindingHandlers.removeTag = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            var selectedObservableArrayInViewModel = valueAccessor();
            var tags = selectedObservableArrayInViewModel.tags;
            var tag = selectedObservableArrayInViewModel.tag;

            $(element).click(function (event) {
                tags.remove(tag);
                if (bindingContext && bindingContext.$root.currentMetaSearchKeyword) {
                    if (tags.length > 0)
                        bindingContext.$root.currentMetaSearchKeyword(tags[tags.length - 1]);
                    else
                        bindingContext.$root.currentMetaSearchKeyword(null);
                }
            });
        }
    };

    ko.bindingHandlers.removeAllTag = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            var selectedObservableArrayInViewModel = valueAccessor();
            var tags = selectedObservableArrayInViewModel.tags;

            $(element).click(function (event) {
                tags([]);
                if (bindingContext.$root.currentMetaSearchKeyword)
                    bindingContext.$root.currentMetaSearchKeyword(null);
            });
        }
    };

    ko.bindingHandlers.newsDrag = {
        init: function (element, valueAccessor) {
            var options = valueAccessor();

            $(element).draggable({
                start: function (e) {
                    $('.latestCol').stop(true, true).animate({ width: '190px' }, 200, 'linear');
                },
                stop: function () {
                    $('.latestCol').stop(true, true).animate({ width: '60px' }, 200, 'linear');
                },
                drag: function (e, ui) {
                    if (ui.position.left > 700) {
                        if (options.callback && options.news.isUsed() == false) {
                            var story;
                            if (options.story)
                                story = options.story;

                            options.callback(options.news, story);
                            // $(e.target).hide('medium', function () { $(this).remove(); });
                        }
                    }
                },
                revert: "invalid",
                scroll: true,
                //cursorAt: { top: 121, right: 533 }
            });

        },
        update: function (element, valueAccessor) {
            var options = valueAccessor();

            if (options.canAddNews())
                $(element).draggable("enable");
            else
                $(element).draggable("disable");

            if (options.isReadOnly) {
                $(element).draggable("disable");
            }

        }
    };

    ko.bindingHandlers.screenElementDrag = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            var options = valueAccessor();

            $(element).draggable({
                zIndex: 999,
                start: function (e) {
                    options.isDragging(true);
                    var elementId = $(e.currentTarget).data('elementid');
                    if (elementId)
                        options.selectedElementId(elementId);
                },
                stop: function (e, ui) {
                    options.isDragging(false);
                },
                helper: function (e) {
                    var dragIcon = $(e.currentTarget).data('dragicon')
                    return '<img class="screen-element-dragicon" src="' + dragIcon + '">';
                }
            });
        }
    };

    ko.bindingHandlers.newsFileDrag = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            var options = valueAccessor();
            $(element).draggable(
                { revert: true });
            $(element).draggable({
                start: function (e) {
                    var elementId = options.selectedId;
                    if (elementId)
                        options.selectedElementId(elementId);
                },
                stop: function (e, ui) {
                    return true;
                },
            });
        }
    };

    ko.bindingHandlers.newsFileDrop = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            var options = valueAccessor();

            $(element).droppable({
                drop: function (e, ui) {
                    if (options.callback)
                        options.callback(options.dropNews());

                    $("#news" + options.dropNews()).draggable("option", "stop", true);
                    $("#news" + options.dropNews()).draggable("option", "revert", true);
                    $("#news" + options.dropNews()).draggable("destroy");

                },
                addClasses: options.addClasses,
                hoverClass: 'ui-state-highlight screentemplate-hover',
                tolerance: "pointer"
            });
        }
    };

    ko.bindingHandlers.screenElementDrop = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            var options = valueAccessor();

            $(element).droppable({
                drop: function (e, ui) {
                    var elementId = $(ui.draggable[0]).data('elementid');
                    var templateId = $(e.target).data('templateid');
                    var templateIndex = $(e.target).data('index');

                    options.dropTemplate(elementId, templateId, templateIndex);
                },
                addClasses: options.addClasses,
                hoverClass: 'ui-state-highlight screentemplate-hover',
                tolerance: "pointer"
            });
        }
    };

    ko.bindingHandlers.contentDrag = {
        init: function (element, valueAccessor) {
            var options = valueAccessor();

            if (options && options.enabled) {
                $(element).draggable({
                    start: function (e) {
                    },
                    stop: function () {
                    },
                    drag: function (e, ui) {
                        if (ui.offset.left > 700) {
                            if (options.callback)
                                options.callback(options.content);
                        }
                    },
                    revert: "invalid",
                    scroll: true
                });
            }
        },
        update: function (element, valueAccessor) {
        }
    };

    ko.bindingHandlers.elementPosition = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            var options = valueAccessor();

            $(element).click(function () {
                var context = ko.contextFor(this);
                if (options && context) {
                    options.top($(this).parent().position().top);
                    options.left($(this).parent().position().left);
                    options.callback(context.$data, context.$index());
                }
            });
        }
    };

    ko.bindingHandlers.newsBucketHover = {
        init: function (element, valueAccessor) {
            $(element).hover(
            function () {
                $(this).stop(true, true).animate({ width: '190px' }, 200, 'linear');
            },
            function () {
                $(this).stop(true, true).animate({ width: '75px' }, 200, 'linear');
            });
        }
    };

    ko.bindingHandlers.autoComplete = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            var selectedObservableArrayInViewModel = valueAccessor();
            var source = selectedObservableArrayInViewModel.source;
            var selected = selectedObservableArrayInViewModel.selected;
            var tags = selectedObservableArrayInViewModel.tags;

            var addTag = function (item) {
                if (tags) {
                    if ($.trim(item.value) != '' && tags.indexOf(item) < 0) {
                        var toInsert = $.trim(item.value);
                        tags.push(toInsert);
                        $(element).val('');

                        $(element).removeClass('not_valid');
                    }
                    else {
                        $(element).addClass('not_valid');
                    }
                }
            };

            var select = function (item) {
              if (selected && item) {
                    selected(item);
                }
            };



            var init = function () {
                $(element).autocomplete({
                    minLength: 1,
                    autoFocus: false,
                    select: function (event, ui) {
                        select(ui.item);
                        addTag(ui.item);
                        event.target.value = '';
                        $(element).autocomplete();
                        setTimeout(function () { event.target.value = ''; }, 200)
                        $(element).autocomplete("destroy");
                        init();
                    },
                    change: function (event, ui) {
                        select(ui.item);
                        //addTag(ui.item);
                    },
                    source: function (request, response) {
                        var term = $.trim(request.term);
                        if (term != '' && term.length > 0) {
                            jQuery.get(source + term, {
                                query: request.term
                            }, function (data) {
                                var result = [];
                                var suggestion = data.Data;
                                
                                if (suggestion && suggestion.length > 0) {
                                    if (suggestion[0].hasOwnProperty('Location')) {
                                        for (var i = 0; i < data.Data.length; i++) {
                                            result.push({ value: data.Data[i].Location, id: data.Data[i].LocationId, name: data.Data[i].Location });
                                        }
                                    }
                                    else if (suggestion[0].hasOwnProperty('Category')) {
                                        for (var i = 0; i < data.Data.length; i++) {
                                            result.push({ value: data.Data[i].Category, id: data.Data[i].CategoryId, name: data.Data[i].Category });
                                        }
                                    }
                                    else if (suggestion[0].hasOwnProperty('Tag')) {
                                        for (var i = 0; i < data.Data.length; i++) {
                                            result.push({ value: data.Data[i].Tag, id: data.Data[i].TagId, name: data.Data[i].Tag });
                                        }
                                    }
                                    else if (suggestion[0].hasOwnProperty('action')) {
                                        for (var i = 0; i < data.Data.length; i++) {
                                            result.push({ value: data.Data[i].action, id: data.Data[i].type, name: data.Data[i].action });
                                        }
                                    } else if (suggestion[0].hasOwnProperty('OrganizationId')) {
                                        for (var i = 0; i < data.Data.length; i++) {
                                            result.push({ value: data.Data[i].Name, id: data.Data[i].OrganizationId, name: data.Data[i].Name });
                                        }
                                    }
                                    else if (suggestion[0].hasOwnProperty('EventTypeId')) {
                                        for (var i = 0; i < data.Data.length; i++) {
                                            result.push({ value: data.Data[i].Name, id: data.Data[i].EventTypeId, name: data.Data[i].Name });
                                        }
                                    }
                                    response(result);
                                } else {
                                    response(result);
                                }
                            })
                        }
                    }
                }).data('ui-autocomplete')._renderItem = function (ul, item) {
                    return $("<li></li>")
                        .append($("<a>").text(item.value))
                        .appendTo(ul);
                };
            }
            init();
        }
    };


    ko.bindingHandlers.autoCompleteOrg = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            var selectedObservableArrayInViewModel = valueAccessor();
            var source = selectedObservableArrayInViewModel.source;
            var selected = selectedObservableArrayInViewModel.selected;
            var tags = selectedObservableArrayInViewModel.tags;

            //var addTag = function (item) {
            //    if (tags) {
            //        if ($.trim(item.value) != '' && tags.indexOf(item) < 0) {
            //            tags.push(item);
            //            $(element).val('');
            //            $(element).removeClass('not_valid');
            //        }
            //        else {
            //            $(element).addClass('not_valid');
            //        }
            //    }
            //};

            var select = function (item) {
                if (selected && item) {
                    selected.push(item);
                }
            };
            var init = function () {
                $(element).autocomplete({
                    minLength: 1,
                    autoFocus: false,
                    select: function (event, ui) {
                        //this._trigger("selected", event, { item: this.active });
                        select(ui.item);
                        setTimeout(function () { event.target.value = ''; }, 200)

                        $(element).autocomplete("destroy");
                        init();
                    },
                    change: function (event, ui) {
                      
                        //  select(ui.item);
                        //addTag(ui.item);
                    },
                    source: function (request, response) {
                        var term = $.trim(request.term);
                        if (term != '' && term.length > 0) {
                            jQuery.get(source + term, {
                                query: request.term
                            }, function (data) {
                                var result = [];
                                var suggestion = data.Data;
                                if (suggestion && suggestion.length > 0) {
                                    if (suggestion[0].hasOwnProperty('OrganizationId')) {
                                        for (var i = 0; i < data.Data.length; i++) {
                                            result.push({ value: data.Data[i].Name, id: data.Data[i].OrganizationId, name: data.Data[i].Name });
                                        }
                                    }
                                    response(result);
                                } else {
                                    response(result);
                                }
                            })
                        }
                    }
                }).data('ui-autocomplete')._renderItem = function (ul, item) {
                    return $("<li></li>")
                        .append($("<a>").text(item.value))
                        .appendTo(ul);
                };

               
            }
            init();
        }
    };

    ko.bindingHandlers.autoCompleteMetaTags = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            var selectedObservableArrayInViewModel = valueAccessor();
            var source = selectedObservableArrayInViewModel.source;
            var selected = selectedObservableArrayInViewModel.selected;
            var tags = selectedObservableArrayInViewModel.tags;

            var addTag = function (item) {
                if (tags) {
                    if ($.trim(item.value) != '' && tags.indexOf(item) < 0) {
                        tags.push(item);
                        $(element).val('');

                        $(element).removeClass('not_valid');
                    }
                    else {
                        $(element).addClass('not_valid');
                    }
                }
            };

            var select = function (item) {
                if (selected) {
                    selected(item);
                }
            };

            $(element).autocomplete({
                minLength: 1,
                autoFocus: false,
                select: function (event, ui) {
                    select(ui.item);
                    // addTag(ui.item);
                },
                change: function (event, ui) {
                    select(ui.item);
                    //addTag(ui.item);
                },
                source: function (request, response) {
                    var term = $.trim(request.term);
                    if (term != '' && term.length > 3) {
                        source = "http://10.3.12.119/api/Resource/SearchTag?term=" + term + "&pageSize=10&&pageNumber=1";
                        $.getJSON(source, function (data) {
                            var result = [];
                            var suggestion = data.Data;
                            if (suggestion.length > 0) {
                                for (var i = 0; i < data.Data.length; i++) {
                                    result.push({ value: data.Data[i], id: "tag" + i, name: data.Data[i] });
                                }
                                response(result);
                            }
                        });
                    }
                }
            }).data('ui-autocomplete')._renderItem = function (ul, item) {
                return $("<li></li>")
                    .append($("<a>").text(item.value))
                    .appendTo(ul);
            };
        }
    };


    ko.bindingHandlers.producerAutoComplete = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            var selectedObservableArrayInViewModel = valueAccessor();
            var selected = selectedObservableArrayInViewModel.selected;
            var Availablesource = ["Smiley", "Angry", "Normal"];
            $(element).autocomplete({
                source: Availablesource,
                select: function (event, ui) {
                    if (ui && ui.item && ui.value)
                        selected(ui.item.value);
                },
                change: function (event, ui) {
                    if (ui.item.value)
                        selected(ui.item.value);
                },
            });
        }
    };

    ko.bindingHandlers.sortableList = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            var options = valueAccessor();

            var sortableOption;
            if (options && options.stories) {
                sortableOption = {
                    start: function (event, ui) {
                        var dustBin = $('.dustbin');
                        dustBin.stop(true, true).fadeIn();
                        dustBin.css({
                            'top': ui.originalPosition.top + 20
                        });
                    },
                    stop: function (event, ui) {
                        $('.dustbin').hide();
                    },
                    update: function (event, ui) {

                        var arrayPositions = $(this).sortable('toArray');
                        options.callback(arrayPositions);
                        $(element).sortable('refresh');
                    },

                    remove: function (event, ui) {
                        var index = $(ui.item).attr('id');
                        options.removeStory(index);
                        $('#trash').children().remove();
                    },
                    revert: false,
                    connectWith: "#trash"
                }
            }
            else if (options && options.categories) {
                sortableOption = {
                    update: function (event, ui) {
                        var arrayPositions = $(this).sortable('toArray');
                        options.callback(arrayPositions);
                        $(element).sortable("refresh");
                    },
                    revert: false
                }
            }
            else if (options && options.tickers) {
                sortableOption = {
                    update: function (event, ui) {
                        var arrayPositions = $(this).sortable('toArray');
                        options.callback(arrayPositions);
                    },
                    revert: false
                }
            }
            $(element).sortable(sortableOption);

        },
        update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            $(element).sortable("refresh");
            var options = valueAccessor();
            if (bindingContext.$rawData.type == 2) {
                $(element).sortable("disable");
            }
            else if (options.disabled && !options.disabled()) {
                $(element).sortable("disable");
            }
            else {
                $(element).sortable("enable");
            }
        },
    };

    ko.bindingHandlers.sortableScreenTemplateList = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            var options = valueAccessor();
            $(element).sortable({
                start: function (event, ui) {
                },
                stop: function (e, ui) {
                },
                update: function (event, ui) {
                    var arrayPositions = $(this).sortable('toArray', { attribute: 'uid' });
                    arrayPositions.pop();
                    $(element).sortable('refresh');
                    options.callback(arrayPositions);
                },
                revert: false,
            });
        },
        update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        },
    };
    ko.bindingHandlers.sortableProgramNewsList = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            var options = valueAccessor();
            //$(element).sortable('refresh');
            $(element).sortable({
                start: function (event, ui) {
                  //  $(element).sortable('refresh');
                },
                stop: function (e, ui) {
                    //$(element).sortable('refresh');
                },
                update: function (event, ui) {
                    var arrayPositions = $(this).sortable('toArray', { attribute: 'uid' });
                    //arrayPositions.pop();
                    $(element).sortable('refresh');
                    options.callback(arrayPositions);
                },
                revert: false,
            });
        },
        update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        },
    };
    

    ko.bindingHandlers.jqCycle = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        },
        update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            var options = valueAccessor();

            if (options.canApplyCycle()) {
                $(element).cycle({
                    fx: "carousel",
                    carouselVisible: 2,
                    pauseOnHover: true,
                    speed: 500,
                    timeout: 0,
                    slides: "li",
                    prev: $(element).siblings('.control-prev')[0],
                    next: $(element).siblings('.control-next')[0],
                    log: false,
                    allowWrap: false,
                    swipe: true
                });
            }
        }
    };

    ko.bindingHandlers.jqueryCycle = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        },
        update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            var options = valueAccessor();

            if (options.canApplyCycle) {
                $(element).cycle({
                    fx: "carousel",
                    carouselVisible: 2,
                    pauseOnHover: true,
                    speed: 500,
                    timeout: 0,
                    slides: "li",
                    prev: $(element).siblings('.control-prev')[0],
                    next: $(element).siblings('.control-next')[0],
                    log: false,
                    allowWrap: false,
                    swipe: true
                });
            }
        }
    };

    ko.bindingHandlers.languageChange = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            var selectedObservableArrayInViewModel = valueAccessor();
            var selector = selectedObservableArrayInViewModel.selector();
            var selectedLanguage = selectedObservableArrayInViewModel.selectedLanguage;
            var _element = selectedObservableArrayInViewModel.element;
            _element(element);
            var tempObject = {};
            var urduEditors = [];

            //setTimeout(function () {
            //    debugger
            //    if ($(element)[0].id == "urduRN") {
            //        $(element).click();
            //    }
            //}, 500);
            $(element).click(function () {
                
                var language = $(this).data('lang');

                if (language == "ur") {
                    selectedLanguage('UR');

                    for (var i = 0; i < selector.length; i++) {
                        if (urduEditors.indexOf(selector[i]) < 0) {
                            var elements = $(selector[i]).not($('.uploadVideo .editingSection .aside .notAdd'));

                            if (!$(elements).hasClass('secondTime')) {
                                $(elements).UrduEditor();
                                elements.attr("style", "font-family: 'Jameel Noori Nastaleeq', 'Nafees nastaleeq', 'Nafees Web Naskh', Helvetica, sans-serif; background-color: rgb(245, 245, 245);font-size:17px");
                            }
                            else {
                                elements.attr("UrduEditorId", "UrduEditor_1");
                                elements.attr("dir", "rtl");
                                elements.attr("style", "font-family: 'Jameel Noori Nastaleeq', 'Nafees nastaleeq', 'Nafees Web Naskh', Helvetica, sans-serif; background-color: rgb(245, 245, 245);font-size:17px");
                                elements.attr("lang", "ur");
                            }

                            if (elements.length > 0) {
                                urduEditors.push(selector[i]);
                                if (selector[i] == '.reportNewsChannelReporter .editingSection .editor .jqte_editor')
                                    $(elements).addClass('secondTime');
                            }
                        } else {
                            $(selector[i]).not($('.uploadVideo .editingSection .aside .notAdd')).attr("UrduEditorId", "UrduEditor_1");
                            $(selector[i]).not($('.uploadVideo .editingSection .aside .notAdd')).attr("dir", "rtl");
                            $(selector[i]).not($('.uploadVideo .editingSection .aside .notAdd')).attr("style", "font-family: 'Jameel Noori Nastaleeq', 'Nafees nastaleeq', 'Nafees Web Naskh', Helvetica, sans-serif; background-color: rgb(245, 245, 245);font-size:17px");
                            $(selector[i]).not($('.uploadVideo .editingSection .aside .notAdd')).attr("lang", "ur");
                        }

                        $(selector[i]).css({ 'background-color': '#f5f5f5' });
                        if (selector[i] == '.updateReportnews .editingSection .editor input[type=text]' || '.uploadVideo .editingSection .aside input[type=text]' || '.reportNews .editingSection .editor input[type=text]' || '.leftPanel .editor input[type=text]' || '.reportNewsChannelReporter .editingSection .editor input[type=text]') {
                            $(selector[i]).not($('.uploadVideo .editingSection .aside .notAdd')).not($('.uploadVideo .editingSection .aside .notAdd')).attr("placeholder", "عنوان");
                            if (selector[i] == ".titleStyle") {
                                $(selector[i]).attr("placeholder", "سلگ");
                            }
                            if (selector[i] == '.uploadVideo .editingSection .aside input[type=text],textarea') {
                                $('.uploadVideo .editingSection .aside textarea').attr('placeholder', 'تفصیل ')
                            }
                            if (selector[i] == '.reportNewsEditor') {
                                $('.reportNewsEditor').attr('placeholder', 'تفصیل ')
                            }
                            if (selector[i] == '.urduFontFamily') {
                                $('.reportNewsEditor').attr('placeholder', 'تفصیل ')
                            }
                            if (selector[i] == '.textboxCss .textboxHighLight') {
                                $('.textboxCss.textboxHighLight').attr('placeholder', 'جھلکیاں ')
                            }
                            if (selector[i] == '.textboxHighLight') {
                                $('.textboxHighLight').attr('placeholder', 'جھلکیاں ')
                            }


                            if (selector[i] == '.reportNews .editingSection .editor .jqte_editor' || '.leftPanel .editor .jqte_editor') {

                                if (selector[i] == '.leftPanel .editor .jqte_editor') {
                                    var index = $('.leftPanel .editor .jqte_editor');
                                    if (index.length > 0)
                                        index = index[0].innerHTML;

                                    if (index == '<p class="placeholderclass"> Enter Description</p>') {
                                        $('.leftPanel .editor .jqte_editor').html('');
                                    }
                                }
                                else {
                                    var index = $('.reportNews .editingSection .editor .jqte_editor');
                                    if (index.length > 0)
                                        index = index[0].innerHTML;

                                    if (index == '<p class="placeholderclass"> Enter Description</p>') {
                                        $('.reportNews .editingSection .editor .jqte_editor').html('');
                                    }
                                }
                            }
                        }
                    }
                }

                if (language == "en") {
                    selectedLanguage('EN');
                    for (var i = 0; i < selector.length; i++) {
                        $(selector[i]).not($('.uploadVideo .editingSection .aside .notAdd')).removeAttr('urdueditorid dir lang style');
                        if (selector[i] == '.updateReportnews .editingSection .editor input[type=text]' || '.uploadVideo .editingSection .aside input[type=text]' || '.reportNews .editingSection .editor input[type=text]' || '.leftPanel .editor input[type=text]' || '.reportNewsChannelReporter .editingSection .editor input[type=text]') {
                            $(selector[i]).not($('.uploadVideo .editingSection .aside .notAdd')).attr("placeholder", "Enter Title");

                            if (selector[i] == '.uploadVideo .editingSection .aside input[type=text],textarea') {
                                $('.uploadVideo .editingSection .aside textarea').attr('placeholder', 'Enter Comments ')
                            }
                            if (selector[i] == '.titleStyle') {
                                $('.titleStyle').attr('placeholder', 'Enter Slug ')
                            }
                            if (selector[i] == '.reportNewsEditor') {
                                $('.reportNewsEditor').attr('placeholder', 'Enter Comments ')
                            }
                            if (selector[i] == '.urduFontFamily') {
                                $('.urduFontFamily').attr('placeholder', 'Enter Comments ')
                            }
                            if (selector[i] == '.textboxCss .textboxHighLight') {
                                $('.textboxCss.textboxHighLight').attr('placeholder', 'Highlights ')
                            }
                            if (selector[i] == '.textboxHighLight') {
                                $('.textboxHighLight').attr('placeholder', 'Highlights ')
                            }
                        }
                    }
                }
            });
        },
    };

    ko.bindingHandlers.languageChangeProducer = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            var selectedObservableArrayInViewModel = valueAccessor();
            var tempObject = {};
            var urduEditors = [];
            var languageCode = selectedObservableArrayInViewModel.languageCode;
            $(element).click(function () {
                var selectedObservableArrayInViewModel = valueAccessor();
                var languageCode = selectedObservableArrayInViewModel.languageCode;
                var language = $(this).data('lang');
                if (language == "ur") {
                    languageCode('ur');
                    $(element).parent().parent().find('td').eq(1).children('input').UrduEditor();
                }
                if (language == "en") {
                    languageCode('en');
                    $(element).parent().parent().find('td').eq(1).children('input').removeAttr('urdueditorid dir lang style');
                }
            });
        },
    };

    ko.bindingHandlers.channelDropdown = {
        update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            var value = valueAccessor();
            var val = ko.unwrap(value);

            if (val == true) {
                $(element).each(function () {
                    var
                        $this = $(this),
                        val = $this.children('option:first-child').text();

                    if (!$this.parent().hasClass('xSelect')) {
                        $this.wrap('<div class="xSelect" style="position:relative;" />');
                        $this.parent().append('<p>' + val + '</p>');
                        $this.css({ 'position': 'absolute', 'top': '0px', 'left': '0px', 'width': '100%', 'height': '100%', 'z-index': '10', 'opacity': 0 });
                        $this.on('change', function () {
                            val = $('#' + this.id + ' :selected').text();
                            $(this).parent().children('p').text(val);
                        });
                    }
                });
            }
        }
    };

    ko.bindingHandlers.programDropdown = {
        update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            var value = valueAccessor();
            var val = ko.unwrap(value);

            if (val == true) {
                var thist = 0;
                $(element).hover(function () {
                    if (thist == 1) {
                        $(this).stop(true, true).animate({
                            width: '297px'
                        }, 400, 'linear');
                    }
                    thist = 1;
                }, function () {
                    $(this).children('li').find('ul').stop(0, 0).slideUp('fast');
                    $(this).children('li').find('span#datepickerhome').stop(0, 0).slideUp('fast');
                    $(this).stop(true, true).animate({ width: '75px' }, 400, 'linear');
                });
            };
        }
    }

    ko.bindingHandlers.dropdownSlide = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            $('body').click(function () {
                $(element).children('ul').hide();
            });
        },
        update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            var value = valueAccessor();
            var val = ko.unwrap(value);

            if (val == true) {
                $(element).click(function (event) {
                    event.stopPropagation();
                    $(this).siblings().children('ul').hide();
                    $(this).children('ul').show();
                });
            };
        }
    };

    ko.bindingHandlers.tabActive = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        },
        update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {

            var value = valueAccessor();
            var val = ko.unwrap(value);

            if (val == true) {
                $($(element).children()[0]).addClass('active');
                $(element).children().click(function () {
                    $(element).children().removeClass("active");
                    $(this).addClass("active");
                    //  $('.flowChart').show();
                    // $(element).show();
                });
            };
        }
    };

    ko.bindingHandlers.tabClose = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            //$(element).hide();
        },
        update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            var value = valueAccessor();
            var val = ko.unwrap(value);

            if (val == true) {
                $(".close-Icon").click(function () {
                    $(this).parent('.flowChart').hide();
                    $(".tabs li").removeClass("active");
                });
            };
        }
    };
    ko.bindingHandlers.closeSuggestedFlow = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            $(element).click(function () {
                $(".close-Icon").click();
            });
        },
    };

    ko.bindingHandlers.togglePopup = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        },
        update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            var value = valueAccessor();
            var val = ko.unwrap(value);

            if (val == true) {
                $(element).show();
            } else {
                $(element).hide();
            }
        }
    }

    ko.bindingHandlers.displayTab = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        },
        update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            var value = valueAccessor();
            var val = ko.unwrap(value);
            if (val) {
                $(element).children().css("display", "none");
                $("#" + val).fadeToggle();
            };
        }
    };

    ko.bindingHandlers.activate = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            var value = valueAccessor();
            var cssClass = ko.unwrap(value);

            if (cssClass) {
                $(element).click(function () {
                    if ($(this).hasClass(cssClass))
                        $(this).removeClass(cssClass);
                    else
                        $(this).addClass(cssClass);
                });
            };
        }
    };

    ko.bindingHandlers.customScrollBar = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            var options = valueAccessor();
            var onTotalScrollOffset = 0;

            if (options && options.onTotalScrollOffset) {
                onTotalScrollOffset = options.onTotalScrollOffset;
            }

            if (options && options.fixResolutionMyNews) {
                $(element).css('max-height', ($(window).height() - 180) + 'px');
            }

            var callBackOptions = {
                onTotalScroll: function (element) {
                    if (options && options.callback)
                        options.callback();


                },
                onTotalScrollOffset: onTotalScrollOffset,
                alwaysTriggerOffsets: false
            }
            if (Boolean($(element).attr("data-mainsection"))) {
                callBackOptions = {
                    onTotalScroll: function (element) {
                        if (options && options.callback)
                            options.callback();

                    },
                    onTotalScrollOffset: onTotalScrollOffset,
                    onScroll: function () { },
                    whileScrolling: function () {
                        bindingContext.$root.utils.WhileScrolling1();
                    },
                    alwaysTriggerOffsets: false
                }
            }
            $(element).mCustomScrollbar({
                autoDraggerLength: true,
                autoHideScrollbar: true,
                keyboard: { enable: true },
                keyboard: { scrollType: "stepless" },
                scrollInertia: 100,
                advanced: {
                    updateOnBrowserResize: true,
                    autoScrollOnFocus: true,
                    updateOnContentResize: true
                },
                callbacks: callBackOptions
            });
        }
    };

    ko.bindingHandlers.adjustHeight = {
        init: function (element, valueAccessor) {
            var height = $(window).height() - $('header.header').outerHeight() - $('.singlebar-menu').outerHeight() - $('.footer').outerHeight() - 40;
            $(element).height(height);

            $(window).resize(function () {
                var height = $(window).height() - $('header.header').outerHeight() - $('.singlebar-menu').outerHeight() - $('.footer').outerHeight() - 40;
                $(element).height(height);
            });
        }
    };

    ko.bindingHandlers.selectedNews = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        },
        update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            var value = valueAccessor();
            var val = ko.unwrap(value);
            if (val == true)
                $(element).addClass('active');
            else
                $(element).removeClass('active');
        }
    };

    ko.bindingHandlers.comments = {
        update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            // Keyboard Language Selection
            $(element).delegate('i', 'click', function (e) {
                $(this).closest(element).children('.dropdown').slideToggle();
                e.stopPropagation();
            });
            $(element).delegate('.dropdown', 'click', function (e) {
                e.stopPropagation();
            });
        },
    };

    ko.bindingHandlers.clearContent = {
        update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            var clear = valueAccessor().clear;
            if (clear()) {
                $(".jqte_editor").empty(' ');
                clear(false);
            }
        },
    };

    ko.bindingHandlers.hoverStyle = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            var value = valueAccessor();
            var val = ko.unwrap(value);

            if (val) {
                $(element).hover(function () {
                    $(this).toggleClass(val);
                });
            };
        }
    };

    ko.bindingHandlers.buttonHover = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            var value = valueAccessor();
            var val = ko.unwrap(value);

            if (val) {
                $(element).hover(function () {
                    $(this).stop(true, true).animate({ width: '275px' }, 400, 'linear');
                    $(this).find('img').css('z-index', '-1');
                }, function () {
                    $(this).stop(true, true).animate({ width: '30px' }, 400, 'linear');
                    $(this).find('img').css('z-index', '99');
                });
            };
        }
    };

    ko.bindingHandlers.lastCharacters = {        
        init: function (element, valueAccessor, allBindingsAccessor, viewModel) {
            var options = valueAccessor();
            if (options) {
                var lastChars = ko.computed(function () {
                    var text = options.text;
                    var length = options.length || 5;
                    var result;
                    if (text && text.length > 0)
                        result = text.substring(text.length - length, text.length);
                    else
                        result = '';
                    return result;
                });
                ko.applyBindingsToNode(element, {
                    text: lastChars
                }, viewModel);

                return {
                    controlsDescendantBindings: true
                };
            }
        }
    };

    ko.bindingHandlers.trimText = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel) {
            var options = valueAccessor();

            if (options) {
                var trimmedText = ko.computed(function () {
                    var untrimmedText = options.text;
                    var maxLength = options.length || 25;
                    var minLength = 5;
                    if (maxLength < minLength) maxLength = minLength;
                    var text = untrimmedText.length > maxLength ? untrimmedText.substring(0, maxLength - 1) + '...' : untrimmedText;
                    return text;
                });

                if (options.isHtml) {
                    ko.applyBindingsToNode(element, {
                        html: trimmedText
                    }, viewModel);
                }
                else {
                    ko.applyBindingsToNode(element, {
                        text: trimmedText
                    }, viewModel);
                }
                return {
                    controlsDescendantBindings: true
                };
            }
        }
    };

    ko.bindingHandlers.newscropper = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            var selectedObservableArrayInViewModel = valueAccessor();
            var coordinates = selectedObservableArrayInViewModel.coordinates;
            var markVideoProcessed = selectedObservableArrayInViewModel.markVideoProcessed;
            var jcrop_api, boundx, boundy;
            var flag = true;

            function updatePreview(c) {
                if (parseInt(c.w) > 0) {
                    var rx = 220 / c.w, ry = 220 / c.h;
                }
            }
            function bindCoordinates(c) {
                if (flag) {
                    markVideoProcessed.call();
                    flag = false;
                }

                var croppedpoints = { Bottom: c.y2, Top: c.y, Right: c.x2, Left: c.x, width: c.w, height: c.h, ResourceTypeId: '', FileName: '' };
                coordinates(croppedpoints);
            }
            $(element).Jcrop({
                onChange: updatePreview,
                onSelect: bindCoordinates,
                bgFade: true,
                bgOpacity: .50,
            },
           function () {
               jcrop_api = this;
           });
        }
    };

    ko.bindingHandlers.newspapercropping = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            var coordinates = valueAccessor().coordinates;
        },
        update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            var Coordinates = valueAccessor().coordinates;
            var imageUrl = valueAccessor().imageUrl;

            function ShowCoordinates(c) {
                var croppedpoints = { Bottom: c.y2, Top: c.y, Right: c.x2, Left: c.x };
                coordinates(croppedpoints);
            }

            if (Coordinates().length > 0) {
                $('.jcrop-holder img').attr('src', imageUrl());
                $(element).Jcrop({
                    bgColor: 'black',
                    bgOpacity: .5,
                    onSelect: ShowCoordinates,
                    setSelect: [Coordinates()[0].left, Coordinates()[0].top, Coordinates()[0].right, Coordinates()[0].bottom]
                }, { trueSize: [660] });
            } else { }
        }
    };

    ko.bindingHandlers.rightSectionNews = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            var rightsection = valueAccessor();
            var rightsectionelement = element;
            var totalheight = $('.rightSection').height();
            var correctedheight = totalheight - 335;
            if (rightsection == true) {
                $(rightsectionelement).height(correctedheight);
            }

            else { }
        }
    };

    ko.bindingHandlers.imagecropped = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            var selectedObservableArrayInViewModel = valueAccessor();
            var coordinates = selectedObservableArrayInViewModel.coordinates;
            var canvasImage = selectedObservableArrayInViewModel.canvasImage;
            var img = canvasImage();
            var context = element.getContext("2d");
            $img = $(img),
            imgW = 906,
            imgH = 679;

            var ratioY = imgH / 679,
                ratioX = imgW / 906;

            var getX = coordinates().Left * ratioX,
                getY = coordinates().Bottom * ratioY,
                getWidth = coordinates().width * ratioX,
                getHeight = coordinates().height * (ratioY);

            context.drawImage(img, getX, getY, getWidth, getHeight, 0, 0, 50, 42);
        }
    };

    ko.bindingHandlers.calendarControl = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            var options = valueAccessor();

            var to = new Date();
            var from = new Date(to.getTime() - 1000 * 60 * 60 * 24 * 14);

            options.toBufferVal.subscribe(function (value) {
                $(element).DatePickerSetDate([options.fromBufferVal(), value]);

            });

            options.fromBufferVal.subscribe(function (value) {
                $(element).DatePickerSetDate([value, options.toBufferVal()]);
            });

            $(element).DatePicker({
                inline: true,
                date: [options.fromBufferVal(), options.toBufferVal()],
                calendars: 3,
                mode: 'range',
                current: new Date(to.getFullYear(), to.getMonth() - 1, 1),
                onChange: function (dates, el) {
                    options.fromBufferVal(dates[0]);
                    options.toBufferVal(dates[1]);
                    options.isChange(!options.isChange());
                    options.currentOptionVal(null);
                    $('.calenderRight').find('.active').removeClass('active');
                }
            });
        },
        update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            var options = valueAccessor();

            if (options) {
                $(element).DatePickerSetDate([bindingContext.$root.appdata.lpFromDate, bindingContext.$root.appdata.lpToDate]);

            }
        }
    };

    ko.bindingHandlers.customCalendar = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            var options = valueAccessor();
            var to = options.to;
            var from = options.from;
            var isSetCustom = options.isSetCustom;
            var clear = options.clear;
            var mode = options.mode;
            var dateObj = {};

            if (!mode) {
                mode = 'single';
                dateObj = from();
            }
            else {
                dateObj = [from(), to()];
            }

            to.subscribe(function (value) {
                if (mode == 'range') {
                    $(element).DatePickerSetDate([from(), to()]);
                }
            });

            from.subscribe(function (value) {
                if (mode == 'single') {
                    $(element).DatePickerSetDate(value);
                }
                else {
                    $(element).DatePickerSetDate([from(), to()]);
                }
            });

            $(element).DatePicker({
                inline: true,
                date: dateObj,
                calendars: 3,
                mode: mode,
                current: new Date(to().getFullYear(), to().getMonth() - 1, 1),
                onChange: function (dates, el) {
                    if (dates instanceof Array) {
                        to(dates[1]);
                        from(dates[0]);
                        clear();
                        isSetCustom(true);
                    }
                    else {
                        from(dates);
                        clear();
                    }
                }
            });
        }
    };

    ko.bindingHandlers.smallCalendar = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            var options = valueAccessor();
            var currentDate = options.currentDate;

            $(element).datepicker({
                onSelect: function (dateValue) {
                    options.currentDate(moment(dateValue).toISOString());
                    //  $(this).datepicker().hide();
                }
            });
            if (options.bindToggle) {
                $(element).click(function () {
                    $(this).toggle();
                });
            }
        }
    };

    ko.bindingHandlers.activeElement = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            var options = valueAccessor();

            $(element).click(function () {
                if ($(this).hasClass(options)) {
                    $('.' + options).removeClass();
                }
                else {
                    $('.' + options).removeClass();
                    $(this).addClass(options);
                }
            });
        }
    };

    ko.bindingHandlers.numericBox = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            var options = valueAccessor();
            var min = valueAccessor().min;
            var max = valueAccessor().max;

            $(element).keyup(function (e) {
                var val = $(element).val();
                if (!isNaN(val) && (val >= min && val <= max)) {
                    return true;
                }
                else {
                    $(element).val(val.substring(0, val.length - 1));
                }
            });
        }
    };

    ko.bindingHandlers.videoEditor = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            var markVideoProcessed = valueAccessor().markVideoProcessed;
            var videoControlElement = valueAccessor().videoControlElement;
            var video = $(element)[0];
            videoControlElement(video);
            video.onended = function () {
                markVideoProcessed.call();
            };
        }
    };

    ko.bindingHandlers.videoEditorCasparCG = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            var markVideoProcessed = valueAccessor().markVideoProcessed;
            var videoControlElement = valueAccessor().videoControlElement;
            var video = $(element)[0];
            videoControlElement(video);
            video.onended = function () {
                markVideoProcessed.call();
            };
        }
    };

    ko.bindingHandlers.videoEditorVolumeBar = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            var volumeBar = $(element)[0];
            var videoControlElement = valueAccessor().videoControlElement();
            volumeBar.addEventListener("change", function () {
                videoControlElement.volume = volumeBar.value;
            });
        }
    };

    ko.bindingHandlers.videoEditorBar = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            var progressBar = $('.progressbar', element)[0];
            var videoControlElement = valueAccessor().videoControlElement();
            var totalSelections = valueAccessor().totalSelections;
            var startBuffering = valueAccessor().startBuffering;
            var videoCurrentTime = valueAccessor().videoCurrentTime;
            var videoDuration = valueAccessor().videoDuration;
            var divInnerOffset = '';
            var start = ko.observable(0);
            var end = ko.observable(0);
            var TotalSelections = [];
            var factor = valueAccessor().factor;
            var fact;
            var durationPercent;

            videoControlElement.oncanplay = function () {
                factor(videoControlElement.duration / $(element).width());
                fact = factor();
                videoDuration(videoControlElement.duration);
                durationPercent = (10 / 100) * videoDuration();
                startBuffering(true);
            }

            $(element).click(function (e) {
                divInnerOffset = e.pageX - $(element).offset().left;
                videoControlElement.currentTime = divInnerOffset * fact;
                var ctrlPressed = 0;
                if (parseInt(navigator.appVersion) > 3) {
                    var evt = e ? e : window.event;
                    ctrlPressed = evt.ctrlKey;
                    if (ctrlPressed) {
                        if (totalSelections().length == 0) {
                            TotalSelections = [];
                        }
                        videoPoints();
                    }
                    else { }
                }
            });

            videoControlElement.addEventListener("timeupdate", function () {
                videoCurrentTime(videoControlElement.currentTime);
                var value = (100 / videoControlElement.duration) * videoControlElement.currentTime;
                progressBar.style.width = value + 3.5 + '%';
            });

            progressBar.addEventListener("mousedown", function () {
                if (videoControlElement.paused == true) {
                    videoControlElement.pause();
                } else { videoControlElement.play(); }
            });

            // functions to be called-------------------------------------------------------
            function clearOffsetValue() {
                divInnerOffset = ' ';
            }
            function clearValues() {
                start('');
                end('');
            }
            function videoPoints() {
                var noDuplicaiton = false;
                start(videoControlElement.currentTime);
                end(start() + durationPercent);

                if (totalSelections().length > 0 && TotalSelections.length == 0)
                { TotalSelections = totalSelections(); }

                if (TotalSelections.length > 0) {
                    var s = start();
                    var t = end();
                    for (var i = 0; i < TotalSelections.length; i++) {
                        if ((s + end()) < TotalSelections[i].From || s > (TotalSelections[i].From + (TotalSelections[i].To - TotalSelections[i].From))) {
                            noDuplicaiton = true;
                        } else {
                            return false;
                        }
                    }
                }
                if (noDuplicaiton || TotalSelections.length == 0) {
                    if ((start() + (end() - start())) > (videoControlElement.duration - 3)) {
                        end(start() + ((0.5 / 100) * videoControlElement.duration));
                    }
                    TotalSelections.push({ From: start(), To: end(), Left: start() / fact, width: (end() / fact) - (start() / fact) });
                    totalSelections(TotalSelections);
                    noDuplicaiton = false;
                }
                clearValues();
            }
        }
    };

    ko.bindingHandlers.videoEditorBarCasparCG = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            var progressBar = $('.progressbar', element)[0];
            var videoControlElement = valueAccessor().videoControlElement();
            var totalSelections = valueAccessor().totalSelections;
            var startBuffering = valueAccessor().startBuffering;
            var videoCurrentTime = valueAccessor().videoCurrentTime;
            var videoDuration = valueAccessor().videoDuration;
            var videoselectionfrom = valueAccessor().videoselectionfrom;
            var videoselectionto = valueAccessor().videoselectionto;
            var divInnerOffset = '';
            var start = ko.observable(0);
            var end = ko.observable(0);
            var TotalSelections = [];
            var factor = valueAccessor().factor;
            var fact;
            var durationPercent;
            var cl = valueAccessor().cl;

            videoControlElement.oncanplay = function () {
                factor(videoControlElement.duration / $(element).width());
                fact = factor();
                videoDuration(videoControlElement.duration);
                durationPercent = (10 / 100) * videoDuration();
                startBuffering(true);
            }

            $(element).click(function (e) {
                videoControlElement.oncanplay();
                divInnerOffset = e.pageX - $(element).offset().left;
                videoControlElement.currentTime = divInnerOffset * fact;
                var ctrlPressed = 0;
                if (parseInt(navigator.appVersion) > 3) {
                    var evt = e ? e : window.event;
                    ctrlPressed = evt.ctrlKey;
                    if (ctrlPressed && cl() == 0) {
                        if (totalSelections().length == 0) {
                            TotalSelections = [];
                        }
                        videoPoints();
                        cl(1);
                    }
                    else { }
                }
            });

            videoControlElement.addEventListener("timeupdate", function () {
                videoCurrentTime(videoControlElement.currentTime);
                var value = (100 / videoControlElement.duration) * videoControlElement.currentTime;
                progressBar.style.width = value + 0 + '%';
                chktime();
            });

            progressBar.addEventListener("mousedown", function () {
                if (videoControlElement.paused == true) {
                    videoControlElement.pause();
                } else { videoControlElement.play(); }
            });

            // functions to be called-------------------------------------------------------
            function clearOffsetValue() {
                divInnerOffset = ' ';
            }
            function clearValues() {
                start('');
                end('');
            }
            function videoPoints() {
                var noDuplicaiton = false;
                start(videoControlElement.currentTime);
                end(start() + durationPercent);

                if (totalSelections().length > 0 && TotalSelections.length == 0)
                { TotalSelections = totalSelections(); }

                if (TotalSelections.length > 0) {
                    var s = start();
                    var t = end();
                    for (var i = 0; i < TotalSelections.length; i++) {
                        if ((s + end()) < TotalSelections[i].From || s > (TotalSelections[i].From + (TotalSelections[i].To - TotalSelections[i].From))) {
                            noDuplicaiton = true;
                        } else {
                            return false;
                        }
                    }
                }
                if (noDuplicaiton || TotalSelections.length == 0) {
                    if ((start() + (end() - start())) > (videoControlElement.duration - 3)) {
                        end(start() + ((0.5 / 100) * videoControlElement.duration));
                    }
                    TotalSelections.push({ From: start(), To: end(), Left: start() / fact, width: (end() / fact) - (start() / fact) });
                    totalSelections(TotalSelections);

                    noDuplicaiton = false;
                }
                videoselectionfrom($(TotalSelections).attr("From"));
                videoselectionto($(TotalSelections).attr("To"));
                //app.clipVideo(videoselectionfrom(), videoselectionto());
                clearValues();
            }
        }
    };

    ko.bindingHandlers.updateRangeValue = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            var options = valueAccessor();
            $(element).click(function () {
            });
        },
        update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            var options = valueAccessor();
            var value = (options.timeUpdateDuration / options.totalPreviewTime) * 100;
            element.value = value > 0 ? value + 6 : 0;
        }
    }

    ko.bindingHandlers.textAnimate = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            var options = valueAccessor();
            var animationDuration = options.animationDuration;
            //element.style.webkitAnimationDuration = animationDuration + 's';
            //element.style.animationDuration = animationDuration + 's';
        },
        update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            var options = valueAccessor();
            var animationDuration = options.animationDuration;

            //var keyframes;

            //var allCssFile = document.styleSheets;

            //if (allCssFile.length > 0) {
            //    for (var i = 0; i < allCssFile.length; ++i) {
            //        // loop through all the rules
            //        if (allCssFile[i].cssRules) {
            //            for (var j = 0; j < allCssFile[i].cssRules.length; ++j) {
            //                // find the -webkit-keyframe rule whose name matches our passed over parameter and return that rule
            //                if (allCssFile[i].cssRules[j].type == window.CSSRule.WEBKIT_KEYFRAMES_RULE && allCssFile[i].cssRules[j].name == '@-webkit-keyframes')
            //                    return allCssFile[i].cssRules[j];
            //            }
            //        }
            //    }
            //}

            //element.style.webkitAnimationDuration = animationDuration + 's';
            //element.style.animationDuration = animationDuration + 's';
        }
    }

    ko.bindingHandlers.buffering = {
        update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            var videoControlElement = valueAccessor().videoControlElement();
            var startBuffering = valueAccessor().startBuffering;
            if (startBuffering()) {
                videoControlElement.addEventListener("progress", function () {
                    $(element)[0].style.width = ((videoControlElement.buffered.end(i) / videoControlElement.duration) - (videoControlElement.buffered.start(i) / videoControlElement.duration)) * 100 + '%';
                });
            }
        }
    };

    ko.bindingHandlers.resizable = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            var videoControlElement = valueAccessor().videoControlElement();
            var totalSelections = valueAccessor().totalSelections;
            var item = valueAccessor().item;
            var factor = valueAccessor().factor();
            var handle = ko.observable('');
            var parent = $(element).parent('.bar');
            var eleWidth = '';//$(element).width();
            var eleLeft = '';
            $(element).resizable({
                containment: "parent",
                handles: "e,w",
                start: function (event, ui) {
                    eleWidth = $(this).width();
                    eleLeft = $(this).offset().left;
                    var handleTarget = $(event.originalEvent.target);
                    if (handleTarget.hasClass('ui-resizable-e')) {
                        handle('To');
                    } else if (handleTarget.hasClass('ui-resizable-w')) {
                        handle('From');
                    } else { }
                },
                stop: function (event, ui) {
                    var divOffset = event.pageX - $(element).parent('.bar').offset().left;
                    var handlecurrentTime = videoControlElement.currentTime = divOffset * factor;
                    if (handle() == 'To') {
                        var index = totalSelections().indexOf(item);
                        if (totalSelections().length > 1) {
                            var prevVal = item.TO;
                            item.To = handlecurrentTime;
                            if (totalSelections()[index + 1]) {
                                if (handlecurrentTime >= totalSelections()[index + 1].From) {
                                    item.To = prevVal;
                                    $(this).offset({ left: eleLeft });
                                    $(this).width(eleWidth);
                                }
                            }
                        } else {
                            item.To = handlecurrentTime;
                        }
                    }
                    else if (handle() == 'From') {
                        var index = totalSelections().indexOf(item);
                        if (totalSelections().length > 1) {
                            var prevVal = item.From;
                            item.From = handlecurrentTime;
                            if (totalSelections()[index - 1]) {
                                if (handlecurrentTime <= totalSelections()[index - 1].To) {
                                    item.From = prevVal;
                                    $(this).offset({ left: eleLeft });
                                    $(this).width(eleWidth);
                                }
                            }
                        } else {
                            item.From = handlecurrentTime;
                        }
                    } else { }
                }
            });
        }
    };

    ko.bindingHandlers.resizableCasparCG = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            var videoControlElement = valueAccessor().videoControlElement();
            var totalSelections = valueAccessor().totalSelections;
            var item = valueAccessor().item;
            var factor = valueAccessor().factor();
            var handle = ko.observable('');
            var parent = $(element).parent('.bar');
            var eleWidth = '';//$(element).width();
            var eleLeft = '';
            var videoselectionfrom = valueAccessor().videoselectionfrom;
            var videoselectionto = valueAccessor().videoselectionto;
            $(element).resizable({
                containment: "parent",
                handles: "e,w",
                start: function (event, ui) {
                    eleWidth = $(this).width();
                    eleLeft = $(this).offset().left;
                    var handleTarget = $(event.originalEvent.target);
                    if (handleTarget.hasClass('ui-resizable-e')) {
                        handle('To');
                    } else if (handleTarget.hasClass('ui-resizable-w')) {
                        handle('From');
                    } else { }
                },
                stop: function (event, ui) {
                    var divOffset = event.pageX - $(element).parent('.bar').offset().left;
                    var handlecurrentTime = videoControlElement.currentTime = divOffset * factor;
                    if (handle() == 'To') {
                        var index = totalSelections().indexOf(item);
                        if (totalSelections().length > 1) {
                            var prevVal = item.TO;
                            item.To = handlecurrentTime;
                            if (totalSelections()[index + 1]) {
                                if (handlecurrentTime >= totalSelections()[index + 1].From) {
                                    item.To = prevVal;
                                    $(this).offset({ left: eleLeft });
                                    $(this).width(eleWidth);
                                }
                            }
                        } else {
                            item.To = handlecurrentTime;
                        }
                        videoselectionto(item.To);
                        getseek();
                    }
                    else if (handle() == 'From') {
                        var index = totalSelections().indexOf(item);
                        if (totalSelections().length > 1) {
                            var prevVal = item.From;
                            item.From = handlecurrentTime;
                            if (totalSelections()[index - 1]) {
                                if (handlecurrentTime <= totalSelections()[index - 1].To) {
                                    item.From = prevVal;
                                    $(this).offset({ left: eleLeft });
                                    $(this).width(eleWidth);
                                }
                            }
                        } else {
                            item.From = handlecurrentTime;
                        }
                        videoselectionfrom(item.From);
                        getseek();
                    } else { }
                }
            });
        }
    };

    ko.bindingHandlers.linearHover = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            var value = valueAccessor();
            var width1 = value.width1;
            var width2 = value.width2;

            $(element).hover(function () {
                $(this).stop(true, true).animate({ width: width1 }, 400, 'linear');
            }, function () {
                $(this).stop(true, true).animate({ width: width2 }, 400, 'linear');
            });
        }
    };

    ko.bindingHandlers.toggleState = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            $(element).click(function () {
                if (!valueAccessor()) {
                    if ($(this).parent().children().hasClass('active')) {
                        $(this).parent().children().removeClass('active');
                    }
                }
                $(this).toggleClass('active');
            });
        }
    };

    ko.bindingHandlers.showContentGallery = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            var resetvar2 = 0;
            $(element).click(function () {
                if (resetvar2 == 0) {
                    $(this).parents('.galleryView').addClass('active').stop(true, true).animate({ left: '0px' }, 200, 'linear');
                    resetvar2 = 1;
                } else {
                    $(this).parents('.galleryView').removeClass('active').stop(true, true).animate({ left: '-142px' }, 200, 'linear');
                    resetvar2 = 0;
                }
            });
        }
    };

    ko.bindingHandlers.myNewsLeftNavHover = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            $('li', element).on('mouseover', function () {
                $(this).find('ul').parent('li').addClass("active");
                $(this).find('ul').stop(true, true).animate({
                    left: '50px'
                }, 200, 'linear');
                $(this).on('mouseleave', function () {
                    $(this).find('ul').parent('li').removeClass("active");
                    $(this).find('ul').stop(true, true).animate({
                        left: '-340px'
                    }, 200, 'linear');
                });
            });
        }
    };

    ko.bindingHandlers.navigator = {
        init: function (element, valueAccessor, allBindingAccessor, viewModel, bindingContext) {
            $(window).keyup(function (evt) {
                if (event.keyCode == 37) {
                    // viewModel.previous();
                }
                else if (event.keyCode == 39) {
                    //viewModel.next();
                }
                else if (event.keyCode == 27) {
                    //viewModel.contentViewerControl.isPopUpVisible(false);
                }
            });
        },
        update: function (element, valueAccessor, allBindingsAccessor, viewModel) {
            // $(element).show("slide", { direction: "right" }, 1000);
        }
    };

    ko.bindingHandlers.thumScrollPrev = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            var currentPos = 0;
            $(element).click(function () {
                var totalWidth = $(element).siblings('.cycle-slideshow')[0].scrollWidth;
                var offset = $(element).siblings('.cycle-slideshow').offset();
                var factorPrev = (offset.left / totalWidth) * 100;
                var factorNext = (totalWidth + offset.left);

                if (factorPrev < -2.6) {
                    var pos = $(element).parent('.customthumb').scrollLeft();
                    $(element).parent('.customthumb').animate({ scrollLeft: pos - 300 }, 600);
                }
                else {
                    $(element).hide();
                }
                if (factorNext <= 1300) {
                    $(element).siblings('a').show();
                }
            });
        }
    };

    ko.bindingHandlers.thumScrollNex = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            var currentPos = 0;
            $(element).click(function () {
                var totalWidth = $(element).siblings('.cycle-slideshow')[0].scrollWidth;
                var offset = $(element).siblings('.cycle-slideshow').offset();
                var factorPrev = (offset.left / totalWidth) * 100;
                var factorNext = (totalWidth + offset.left);

                if (factorNext > 1300) {
                    var pos = $(element).parent('.customthumb').scrollLeft();
                    $(element).parent('.customthumb').animate({ scrollLeft: pos + 300 }, 600);
                }
                else {
                    $(element).hide();
                }
                if (factorPrev >= 2.6) {
                    $(element).siblings('a').show();
                }
            });
        }
    };

    ko.bindingHandlers.scrollPrev = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            var currentPos = 0;
            $(element).click(function () {
                var totalWidth = $(element).siblings('.cycle-slideshow')[0].scrollWidth;
                var offset = $(element).siblings('.cycle-slideshow').offset();
                var factorPrev = (offset.left / totalWidth) * 100;
                var factorNext = (totalWidth + offset.left);

                if (factorPrev < 3) {
                    var pos = $(element).parent('.customthumb').scrollLeft();
                    $(element).parent('.customthumb').animate({ scrollLeft: pos - 220 }, 600);
                }
                else {
                    $(element).hide();
                }
                if (factorNext <= 970) {
                    $(element).siblings('a').show();
                }
            });
        }
    };

    ko.bindingHandlers.scrollNex = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            var currentPos = 0;
            $(element).click(function () {
                var totalWidth = $(element).siblings('.cycle-slideshow')[0].scrollWidth;
                var offset = $(element).siblings('.cycle-slideshow').offset();
                var factorPrev = (offset.left / totalWidth) * 100;
                var factorNext = (totalWidth + offset.left);

                if (factorNext > 970) {
                    var pos = $(element).parent('.customthumb').scrollLeft();
                    $(element).parent('.customthumb').animate({ scrollLeft: pos + 220 }, 600);
                }
                else {
                    $(element).hide();
                }
                if (factorPrev <= 9.6) {
                    $(element).siblings('a').show();
                }
            });
        }
    };

    ko.bindingHandlers.cycleArrows = {
        init: function (element, valueAccessor) {
            $(element).bind("DOMSubtreeModified", function (e, i) {
                var innerwidth = 0;
                var totalwidth = $(element)[0].scrollWidth;
                $('.cycle-slide', element).each(function () {
                    innerwidth += $(this).width();
                });

                if (innerwidth < totalwidth) {
                    $(element).siblings('.arrows').hide();
                }
                else {
                    $(element).siblings('.arrows').show();
                }
            });
        }
    };

    ko.bindingHandlers.textEditor = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            var options = valueAccessor();

            $(element).jqte({
                format: false,
                sub: false,
                sup: false,
                rule: false,
                outdent: false,
                indent: false,
                source: false,
                strike: false,
                linktypes: false,
                color: false,
                link: false,
                remove: false,
                unlink: false,
                change: function (e) {
                    if (options) {
                        if (options.isChangeAllowed()) {
                            options.valueField($(element).val());
                        }
                        if (!options.isChangeAllowed()) {
                            options.isChangeAllowed(true);
                        }
                    }
                }
            });
            $('.jqte_editor').bind("paste", function (event) {
                event.preventDefault();
                var text = event.originalEvent.clipboardData.getData("text/html").replace(/style=['"][a-zA-Z0-9:;\.\s\(\)\-\,]*['"]/g, "").replace('<html>', '').replace('</html>', '').replace('<body>', '').replace('</body>', '');
                if (text == '')
                    text = event.originalEvent.clipboardData.getData("text/plain");
                options.valueField($('.jqte_editor').html() + text);
                $(element).jqteVal($('.jqte_editor').html() + text);
            });
        },
        update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            var options = valueAccessor();
        }
    };

    ko.bindingHandlers.stop = {
        update: function (element, valueAccessor) {
            var player = $(element)[0];
            var stopPlayer = valueAccessor().stopPlayer;
            if (player && player.play && stopPlayer()) {
                player.pause();
                stopPlayer(false);
            } else { }
        }
    };

    ko.bindingHandlers.cssOverlay = {
        init: function (element, valueAccessor) {
            var options = valueAccessor();
        },
        update: function (element, valueAccessor) {
        }
    };

    ko.bindingHandlers.keyBoardControl = {
        init: function (element, valueAccessor) {
            var flag = true;
            var scrolllength = 30;
            $(element).keyup(
            function (e) {
                var elefChild = $(element).siblings('.suggestions').children('.mCustomScrollBox').children('.mCSB_container').children('ul').children('li:first-child');
                var elelChild = $(element).siblings('.suggestions').children('.mCustomScrollBox').children('.mCSB_container').children('ul').children('li:last-child');
                var curr = $(element).siblings('.suggestions').children('.mCustomScrollBox').children('.mCSB_container').find('.currentItemKeyBoardControl');

                if (e.keyCode == 40) {
                    if (flag) {
                        elefChild.attr('class', 'display_box currentItemKeyBoardControl');
                        flag = false;
                    }

                    if (curr.length) {
                        scrolllength -= 41;
                        $(element).siblings('.suggestions').children('.mCustomScrollBox').children('.mCSB_container').attr('style', 'position:relative;Top:' + scrolllength + 'px');
                        $(curr).attr('class', 'display_box');
                        $(curr).next().attr('class', 'display_box currentItemKeyBoardControl');
                    } else {
                        elefChild.attr('class', 'display_box currentItemKeyBoardControl');
                    }
                }
                if (e.keyCode == 38) {
                    if (flag) {
                        elelChild.attr('class', 'display_box currentItemKeyBoardControl');
                        flag = false;
                    }
                    if (curr.length) {
                        scrolllength += 41;

                        if (scrolllength != 30 || scrolllength != 0) {
                            $(element).siblings('.suggestions').children('.mCustomScrollBox').children('.mCSB_container').attr('style', 'Top:' + scrolllength + 'px;position:relative');
                        }
                        $(curr).attr('class', 'display_box');
                        $(curr).prev().attr('class', 'display_box currentItemKeyBoardControl');
                    } else {
                        elelChild.attr('class', 'display_box currentItemKeyBoardControl');
                    }
                }
                if (e.keyCode == 13) {
                    curr.click();

                }
            });

            $(element).siblings('.suggestions').mouseenter(function () {
                var curr = $(element).siblings('.suggestions').children('.mCustomScrollBox').children('.mCSB_container').children('ul').children('li').removeClass('currentItemKeyBoardControl')
                flag = true
            });

            $('body', 'html').click(function (e) {
                $(element).siblings('.suggestions').removeClass('showForcely');
                e.stopPropagation();
            });

            $(element).click(function (e) {
                e.stopPropagation();
            });

            $(element).siblings('.suggestions').click(function (e) {
                e.stopPropagation();
            });
        },
    };


    ko.bindingHandlers.keyBoardControlProducer = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            var flag = true;
            var scrolllength = 30;
            var scrolled = 0;
            $(element).keyup(

            function (e) {

                if (e.keyCode == 8) {

                    if (bindingContext.$root.appdata.currentUser && bindingContext.$root.appdata.currentUser()) {
                        if ((bindingContext.$root.appdata.currentUser().userType == bindingContext.$root.e.UserType.Producer || bindingContext.$root.appdata.currentUser().userType == bindingContext.$root.e.UserType.FReporter || bindingContext.$root.appdata.currentUser().userType === bindingContext.$root.e.UserType.HeadlineProducer || bindingContext.$root.appdata.currentUser().userType == bindingContext.$root.e.UserType.FReporter)) {
                            scrolled = 0;
                        }
                    }
                }
                var elefChild = $(element).siblings('.suggestions').children('ul').children('li:first-child');
                var elelChild = $(element).siblings('.suggestions').children('ul').children('li:last-child');
                var curr = $(element).siblings('.suggestions').find('.currentItemKeyBoardControl');

                if (e.keyCode == 40) {
                    if (flag) {
                        elefChild.attr('class', 'display_box currentItemKeyBoardControl');
                        flag = false;
                    }

                    if (curr.length) {
                        scrolllength -= 35;
                        scrolled = scrolled + 20;

                        $("#archivalId").animate({
                            scrollTop: scrolled
                        });

                        $(curr).attr('class', 'display_box');
                        $(curr).next().attr('class', 'display_box currentItemKeyBoardControl');
                    } else {
                        elefChild.attr('class', 'display_box currentItemKeyBoardControl');
                    }
                }
                if (e.keyCode == 38) {
                    if (flag) {
                        elelChild.attr('class', 'display_box currentItemKeyBoardControl');
                        flag = false;
                    }
                    if (curr.length) {
                        scrolllength += 35;

                        scrolled = scrolled - 20;

                        $("#archivalId").animate({
                            scrollTop: scrolled
                        });
                        if (scrolllength != 30 || scrolllength != 0) {
                        }
                        $(curr).attr('class', 'display_box');
                        $(curr).prev().attr('class', 'display_box currentItemKeyBoardControl');
                    } else {
                        elelChild.attr('class', 'display_box currentItemKeyBoardControl');
                    }
                }
                if (e.keyCode == 13) {
                    curr.click();

                }
            });

            $(element).siblings('.suggestions').mouseenter(function () {
                var curr = $(element).siblings('.suggestions').children('ul').children('li').removeClass('currentItemKeyBoardControl')
                flag = true
            });

            $('body', 'html').click(function (e) {
                $(element).siblings('.suggestions').removeClass('showForcely');
                //e.stopPropagation();
            });

            $(element).click(function (e) {
                //e.stopPropagation();
            });

            $(element).siblings('.suggestions').click(function (e) {
                //e.stopPropagation();
            });
        },
    };


    ko.bindingHandlers.shellLeftArrowTrigger = {
        init: function (element, valueAccessor) {
            $(element).click(
          function () {
              if ($(this).hasClass('reportNewsChannelReporter')) {
                  $(element).click(
                  function () {
                      $(".reportNewsChannelReporter.left").stop(true, true).animate({
                          left: '0px'
                      }, 200, 'linear');
                      $(".reportNewsChannelReporter.left").show();
                  });
              } else if ($(this).hasClass('updateNewsLeftRight')) {
                  $(".updateNewsLeftRight.left").stop(true, true).animate({
                      left: '0px'
                  }, 200, 'linear');
                  $(".updateNewsLeftRight.left").show();
              }
          });
        }
    };

    ko.bindingHandlers.shellRightArrowTrigger = {
        init: function (element, valueAccessor) {
            $(element).click(
            function () {
                if ($(this).hasClass('reportNewsChannelReporter')) {
                    $(".reportNewsChannelReporter.right").stop(true, true).animate({
                        right: '0px'
                    }, 200, 'linear');
                    $(".reportNewsChannelReporter.right").show();
                } else if ($(this).hasClass('updateNewsLeftRight')) {
                    $(".updateNewsLeftRight.right").stop(true, true).animate({
                        right: '0px'
                    }, 200, 'linear');
                    $(".updateNewsLeftRight.right").show();
                }
            });
        }
    };

    ko.bindingHandlers.closePanel = {
        init: function (element, valueAccessor) {
            $(element).click(function () {
                $(element).parent('h2').parent().hide();
                $(element).parent('h2').parent().stop(true, true).animate({
                    right: '-253px'
                }, 200, 'linear');
            });
        }
    };

    ko.bindingHandlers.preview = {
        update: function (element, valueAccessor) {
            if (valueAccessor().totalTemplates()) {
                var autoforward = valueAccessor().autoforward;
                var templates = valueAccessor().totalTemplates();
                var allDurations = [];
                for (var i = 0; i < templates.length; i++) {
                    allDurations[i] = templates[i].contentDuration();
                }
                startPreview();
            }
            function startPreview() {
                var i = 0;
                setTimeout(function test() {
                    $(element).parents('.mainsection')
                                            .siblings('header')
                                            .children('.playlist')
                                            .children('.container')
                                            .children('.playlistIco')
                                            .children('ul')
                                            .children('li').eq(i)
                                            .children('a')
                                            .click();
                    if (i <= allDurations.length - 1) {
                        setTimeout(test, 2000); //*allDurations[i+1]);
                        i++;
                    } else {
                        clearInterval(test);
                        autoforward();
                    }
                }, 2000);//*allDurations[0]);
            }
        }
    };

    ko.bindingHandlers.OpenFileUpload = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, context) {
            var handler = ko.utils.unwrapObservable(valueAccessor()),
                newValueAccessor = function () {
                    return function (data, event) {
                        //$('body').find('.editorUploader').click();
                        $('.mediaFiles').children('form').children('.editorUploader ').click();
                    };
                };

            ko.bindingHandlers.click.init(element, newValueAccessor, allBindingsAccessor, viewModel, context);
        }
    };

    ko.bindingHandlers.checkBox = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, context) {
        },
        update: function (element, valueAccessor, allBindingsAccessor, viewModel, context) {
            var options = valueAccessor();
            if (options.apply) {
                var checkBox = $(element);
                if (!$(element).parent().hasClass('xCheckbox')) {
                    $(element).fadeTo(0, 0).wrap('<div class="xCheckbox" style="position:relative;" />');
                    $(element).css({ 'position': 'absolute', 'width': '100%', 'height': '100%', 'left': '0', 'top': '0', 'z-index': '10' });
                    if ($(checkBox).is(':checked')) { $(checkBox).parent().addClass('checked'); }
                    $(checkBox).on('change', function () {
                        if ($(this).is(':checked')) {
                            $(this).parent().addClass('checked');
                        } else {
                            $(this).parent().removeClass('checked');
                        }
                    });
                }
            }
        }
    };
    ko.bindingHandlers.checkBoxProduction = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, context) {
            var options = valueAccessor();
            $(element).click(
            function () {
                if (!$(element).parent().hasClass('checked')) {
                    $(element).parent().addClass('checked');
                    options.callback(true);
                }
                else {
                    $(element).parent().removeClass('checked');
                    options.callback(false);
                }
            });

        },

    };
    ko.bindingHandlers.timelineBar = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, context) {
        },
        update: function (element, valueAccessor, allBindingsAccessor, viewModel) {
            var showtimelinebar = ko.observable();
            showtimelinebar(valueAccessor().showdata());
            if (showtimelinebar != '') {
                var totalwidth = $(element).width();
                var totalchilds = ($(element).find('li').length) - 1;
                var diff = totalwidth / totalchilds;

                var marginleft = 0;
                $(element).find('li').each(function (index, element) {
                    var $this = $(this);
                    $this.width($this.data('width'));

                    if ($this.find('span').length == 2) {
                        if (index == 1) {
                            marginleft = marginleft + diff + 100;
                            $this.css('left', marginleft + 'px');
                            $this.find('span').first().css('left', '-12px');
                            $this.find('span').eq(1).css('left', '2px');
                        }
                        else {
                            marginleft = marginleft + diff;
                            $this.css('left', marginleft + 'px');
                            $this.find('span').first().css('left', '-12px');
                            $this.find('span').eq(1).css('left', '2px');
                        }
                    }

                    if ($this.find('span').length == 1) {
                        if (index == 1) {
                            marginleft = marginleft + diff + 100;
                            $this.css('left', marginleft + 'px');
                            $this.find('span').first().css('left', '-12px');
                            $this.find('span').eq(1).css('left', '2px');
                        }
                        else {
                            marginleft = marginleft + diff;
                            $this.css('left', marginleft + 'px');
                            $this.find('span').first().css('left', '-12px');
                            $this.find('span').eq(1).css('left', '2px');
                        }
                    }

                    if ($this.find('span').length == 4) {
                        if (index == 1) {
                            marginleft = marginleft + diff + 100;
                            $this.css('left', marginleft + 'px');
                            $this.find('span').first().css('left', '-16px');
                            $this.find('span').eq(1).css('left', '4px');
                            $this.find('span').eq(2).css('left', '22px');
                            $this.find('span').eq(3).css('left', '40px');
                        }
                        else {
                            marginleft = marginleft + diff;
                            $this.css('left', marginleft + 'px');
                            $this.find('span').first().css('left', '-16px');
                            $this.find('span').eq(1).css('left', '4px');
                            $this.find('span').eq(2).css('left', '22px');
                            $this.find('span').eq(3).css('left', '40px');
                        }
                    }
                });
            }
        }
    };

    ko.bindingHandlers.tickeranimation = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, context) {
            var count = 0;
            var countforwardBackward = 0;
            var speedslow = 20;
            var speedfast = 4;

            $(element).click(
       function () {
           if ($(element)[0].attributes[0].nodeValue == 'playpause') {
               if ((count % 2) == 0) {
                   document.getElementById('myMarquee').stop();
                   $('#myMarquee').stop();
                   count = count + 1;
               }
               else {
                   document.getElementById('myMarquee').start();
                   count = count + 1;
               }
           }

           if ($(element)[0].attributes[0].nodeValue == 'forwardbackword') {
               if ((countforwardBackward % 2) == 0) {
                   document.all.myMarquee.direction = "down";
                   countforwardBackward = countforwardBackward + 1;
               }
               else {
                   document.all.myMarquee.direction = "up";
                   countforwardBackward = countforwardBackward + 1;
               }
           }

           if ($(element)[0].attributes[0].nodeValue == 'slow') {
               if (speedslow < 0) {
               }
               else {
                   speedslow = document.getElementById('myMarquee').getAttribute('scrollamount') - 4;
                   document.getElementById('myMarquee').setAttribute('scrollamount', speedslow, 0);
               }
           }

           if ($(element)[0].attributes[0].nodeValue == 'fast') {
               if (speedfast > 30) {
               }
               else {
                   speedfast = document.getElementById('myMarquee').getAttribute('scrollamount') + 4;
                   document.getElementById('myMarquee').setAttribute('scrollamount', speedfast, 0);
               }
           }
       });
        }
    };

    ko.bindingHandlers.showPanel = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, context) {
            $(element).click(function () {
                $(this).siblings().find('.nav-step1').toggleClass('active', 500, 'linear');
            });
        }
    };
    ko.bindingHandlers.expandCollapse = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, context) {
            $(element).click(function () {
                growingDiv = $(this).next('div');
                if (growingDiv.height() > 0) {
                    growingDiv.css("height", 0);
                } else {
                    var wrapper = $(this).next('div').children('div').height();
                    growingDiv.css("height", wrapper);
                }
            });
        }
    };

    ko.bindingHandlers.rigthMenu = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, context) {
            var wd = $('.settingsmenu').outerWidth();
            $(element).click(function (el) {
                if ($(this).hasClass('active')) {
                    if ($(this).attr('title') == 'alerts') {
                        $(".stngstmenu").css('display', 'none');
                        $(".alertmenu").css('display', 'block');
                    } else {
                        $(".alertmenu").css('display', 'none');
                        $(".stngstmenu").css('display', 'block');
                    }
                } else {
                    if ($(this).attr('title') == 'settings' || $(this).attr('title') == 'alerts') {
                        var wd = $('.settingsmenu').outerWidth();
                        tgt = $(this).attr('href');
                        if ($(this).attr('title') == 'settings') {
                            $(".stngstmenu").css('display', 'block');
                        } else {
                            $(".alertmenu").css('display', 'block');
                        }
                        $('.btn-alerts,.btn-settings').addClass('active');
                        $('.settingsmenu').stop(true, true).animate({ right: 0 }, 1000, eas);
                        $('.container').stop(true, true).css({ 'left': 0 }).animate({ left: -wd + 17 }, 1000, eas);
                        $('.headerMenu').stop(true, true).css({ 'margin-right': 290 }).animate({ left: -wd + 17 }, 1000, eas);
                        $('.headerMenu, .container').stop(true, true).css({ 'left': 0 }).animate({ left: -wd }, 1000, eas, function () { $('.settingsmenu ' + tgt).fadeIn(); });
                    }
                }
            });
        }
    };
    ko.bindingHandlers.contextMenu = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, context) {
            $.contextMenu({
                selector: '.hasmenu',
                callback: function (key, options) {
                    var sId = 0;
                    var childstoryItemId = 0;
                    if ($(this).hasClass("IsChild")) {
                        sId = $(this).parent("div").parent("div").parent("div").siblings("article").attr("sId");
                        childstoryItemId = $(this).attr("SIId");
                    } else {
                        sId = $(this).attr("sId");
                    }
                    var roId = $(this).attr("roId");
                    context.$root.cMenuAction(key, sId, roId, childstoryItemId);
                },
                items: {
                    "load": { name: "load", icon: "edit" },
                    "skip": { name: "skip", icon: "edit" },
                    "unSkip": { name: "unSkip", icon: "edit" },
                    //"delete": { name: "delete", icon: "delete" },
                    "sep1": "---------",
                    "quit": { name: "Quit", icon: "quit" }
                }
            });

            $(element).on('click', function (data) {
                //alert(data);
            })
        }
    };
    ko.bindingHandlers.closeRightPanel = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, context) {
            var eas = 'easeOutExpo',
            loader = '<img src="/Content/images/coordinator/loader.gif" id="temploader" alt="" width="25">',
            anmdur = 1000;
            $(element).click(function () {
                var wd = $('.settingsmenu').outerWidth();
                $('.btn-alerts,.btn-settings').removeClass('active');
                $(".stngstmenu , .alertmenu").css("display", "none")
                $('.settingsmenu').animate({ right: -wd }, 1000, eas, function () { $(this).removeAttr('style'); });
                $('.container').stop(true, true).css({ 'left': -wd }).animate({ left: 0 }, 1000, eas);
                $('.headerMenu').stop(true, true).css({ 'margin-right': 0 }).animate({ left: -wd + 17 }, 1000, eas);
                $('.headerMenu, .container').stop(true, true).css({ 'left': -wd }).animate({ left: 0 }, 1000, eas, function () { $(this).not('.container').removeAttr('style'); });
            });
        }
    };

    ko.bindingHandlers.UI_Toggler = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, context) {
            $(element).click(function () {
                if ($(element).children().first().hasClass("teleprompter_setting")) {
                    $('.divSettings').css("display", "block");
                    $('.divRO').css("display", "none");
                }
                if ($(element).children().first().hasClass("teleprompter_runorder")) {
                    $('.divRO').css("display", "block");
                    $('.divSettings').css("display", "none");
                }
            });
        }
    };

    ko.bindingHandlers.alertDropDown = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, context) {
            $(element).click(function () {
                if ($('.pulldown_click_alert', element).css("display") == "none")
                    $('.pulldown_click_alert', element).css("display", "block");
                else
                    $('.pulldown_click_alert', element).css("display", "none");
            });
        }
    };

    ko.bindingHandlers.searchBoxSuggestions = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, context) {
            $(element).focus(function () {
                if ($('.pulldown_click').css("display") == "none")
                    $('.pulldown_click').css("display", "block");
                else
                    $('.pulldown_click').css("display", "none");
            });
        }
    };

    ko.bindingHandlers.highLightSelected = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, context) {
            $(element).click(function () {
                $(this).addClass('item-active').siblings().removeClass('item-active');
            });
        }
    };

    //Casper
    ko.bindingHandlers.popDbl = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, context) {

            var currentItem = valueAccessor().currentItem;
            var templateWindows = valueAccessor().templateWindows;
            var templateData = valueAccessor().templateData;
            var flashTemplateKeys = valueAccessor().flashTemplateKeys;
            var popUpDisplayDecision = valueAccessor().popUpDisplayDecision;
            var cdata = valueAccessor().data;
            var vidsrc = valueAccessor().vidsrc;
            var videoeditorcaspar = valueAccessor().videoeditorcaspar;
            var emptyPopupFlag = valueAccessor().emptyPopupFlag;
            var casperMedia = valueAccessor().casperMedia;
            var visibleFourWindowPopupFlag = valueAccessor().visibleFourWindowPopupFlag;

            $(element).click(function () {
                context.$root.clearCurrent();
                currentContext('ItemSelection');
                cdata.isSelected(true);
                currentItem(cdata);
                if (visibleFourWindowPopupFlag()) {

                    emptyPopupFlag(false);
                    casperMedia([]);
                    templateWindows([]);
                    flashTemplateKeys([]);
                    vidsrc(cdata.MediaPath);
                    videoeditorcaspar.src(cdata.MediaPath);

                    if (cdata && (cdata.CasparItemId == e.CasparItemType.Video || cdata.CasparItemId == e.CasparItemType.Image)) {
                        context.$root.setTransitionTab(cdata);
                        popUpDisplayDecision("Video");
                        emptyPopupFlag(false);
                    }

                    if (cdata && cdata.CasparItemId !== e.CasparItemType.Template) {
                        popUpDisplayDecision("transition");
                        emptyPopupFlag(false);
                    }
                    if (cdata && cdata.Template && cdata.Template.FlashTemplate && cdata.Template.FlashTemplate.FlashTemplateWindows) {
                        var result = context.$root.ResoulutionFix(cdata.Template.FlashTemplate.FlashTemplateWindows);
                        templateWindows(result);
                        popUpDisplayDecision("window");
                        emptyPopupFlag(false);

                    }
                    if (cdata && cdata.Template && cdata.Template.FlashTemplate && cdata.Template.FlashTemplate.FlashTemplateKeys) {
                        templateData(cdata.TemplateDataList);
                        for (var i = 0; i < cdata.Template.FlashTemplate.FlashTemplateKeys.length; i++) {
                            cdata.Template.FlashTemplate.FlashTemplateKeys[i]["KeyValue"] = "";
                            for (var j = 0; j < cdata.TemplateDataList.length; j++) {
                                if (cdata.Template.FlashTemplate.FlashTemplateKeys[i].FlashKey == templateData()[j].TemplateDataKey) {
                                    cdata.Template.FlashTemplate.FlashTemplateKeys[i].KeyValue = templateData()[j].TemplateDataValue;
                                }
                            }
                        }
                        flashTemplateKeys(cdata.Template.FlashTemplate.FlashTemplateKeys);
                        popUpDisplayDecision("key");
                        emptyPopupFlag(false);
                    }
                    context.$root.sanSearchText('');
                    context.$root.selectedLanguage("");
                    context.$root.selectedLanguage("URDU");
                    var name = $(element).data('target');
                    $(name).modal('toggle');
                    visibleFourWindowPopupFlag(false);
                    if (popUpDisplayDecision() == "key") {
                        setTimeout(function () {
                            $(".templateDataFocus > textarea").first().focus();  //focus on first element
                        }, 1000);
                    }
                }
            });
        }
    };

    ko.bindingHandlers.ChangelanguagePopUp = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, context) {
            var delay = 400
            var selectedLanguage = valueAccessor().selectedLanguage;

            $(element).click(function (e) {
                var name = $(this).get(0).innerText;
                var des = valueAccessor().des;
                var src = valueAccessor().src;
                $(src).html(name + '<span class="caret"></span>');
                if (name == 'URDU') {//if english
                    $(des).each(function (index) {
                        if (
                            $(this).attr("lang")) {
                            $(this).attr("UrduEditorId", "UrduEditor_1");
                            $(this).attr("dir", "rtl");
                            $(this).attr("style", "font-family: 'Jameel Noori Nastaleeq',background-color: rgb(245, 245, 245);");
                            $(this).attr("font-size", "18px");
                            $(this).attr("lang", "ur");
                        } else {
                            $(this).UrduEditor();
                        }
                    });
                    $(des).attr("placeholder", "ویلیو ...");
                    $(des).css("background-color", "#171515");
                    selectedLanguage("URDU");
                }
                else {
                    $('.btnvkimg').remove();
                    $('.btnengimg').remove();
                    $('.btnurduimg').remove();
                    $(this).attr("style", "font-family: 'sans-serif',background-color: rgb(245, 245, 245);");
                    $(des).removeAttr('urdueditorid');
                    $(des).removeAttr('style');
                    $(des).removeAttr('dir');
                    $(des).attr("placeholder", "value...");
                    selectedLanguage("ENGLISH");
                }
            });
        }
    };

    ko.bindingHandlers.KeyBoardMovement = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, context) {
            $('body').keydown(function (e) {
                if (e.keyCode == 38) {
                    $('.panel-body article').each(function (e, ele) {
                        if ($(ele).hasClass('selected')) {
                            if ($(this).prev().is('article')) {
                                $(this).removeClass('selected');
                                $(this).prev('article').click();
                                var idAttr = "#" + $(this).prev('article').attr("id") + "";
                                $("#wrapperRundown").mCustomScrollbar("scrollTo", idAttr);
                                return false;
                            }
                            return false;
                        }
                    });

                    $('.panel-body .SortAbleDiv').each(function (e, ele) {
                        if ($(ele).children('article').hasClass('selected')) {
                            if ($(this).prev().is('.SortAbleDiv')) {
                                $(this).children('article').removeClass('selected');
                                $(this).prev('.SortAbleDiv').children('article').click();
                                var idAttr = "#" + $(this).prev('.SortAbleDiv').attr("id") + "";
                                $("#wrapperRundown").mCustomScrollbar("scrollTo", idAttr);
                                return false;
                            }
                            return false;
                        }
                    });
                    $('.panel3 ul li').each(function (e, ele) {
                        if ($(ele).hasClass('selected')) {
                            if ($(this).prev().is('li')) {
                                $(this).removeClass('selected');
                                $(this).prev('li').click();
                                // $(".panel-body ").children('.mCustomScrollBox').children('.mCSB_container').attr('style', 'position:relative;   Top:-' + $(this).offset().top + 'px')
                                return false;
                            }
                            return false;
                        }
                    });
                    $('.panel1 ul li').each(function (e, ele) {
                        if ($(ele).hasClass('selected')) {
                            if ($(this).prev().is('li')) {
                                $(this).removeClass('selected');
                                $(this).prev('li').click();
                                // $(".panel-body ").children('.mCustomScrollBox').children('.mCSB_container').attr('style', 'position:relative;   Top:-' + $(this).offset().top + 'px')
                                return false;
                            }
                            return false;
                        }
                    });
                    $('.panel2 ul li').each(function (e, ele) {
                        if ($(ele).hasClass('selected')) {
                            if ($(this).prev().is('li')) {
                                $(this).removeClass('selected');
                                $(this).prev('li').click();
                                // $(".panel-body ").children('.mCustomScrollBox').children('.mCSB_container').attr('style', 'position:relative;   Top:-' + $(this).offset().top + 'px')
                                return false;
                            }
                            return false;
                        }
                    });
                }

                if (e.keyCode == 40) {
                    $('.panel-body article').each(function (e, ele) {
                        if ($(ele).hasClass('selected')) {
                            if ($(this).next().is('article')) {
                                $(this).removeClass('selected');
                                $(this).next('article').click();
                                var idAttr = "#" + $(this).prev('article').prev('article').prev('article').prev('article').prev('article').attr("id") + "";
                                $("#wrapperRundown").mCustomScrollbar("scrollTo", idAttr);
                                return false
                            }
                            return false;
                        }
                    });
                    $('.panel-body .SortAbleDiv').each(function (e, ele) {
                        if ($(ele).children('article').hasClass('selected')) {
                            if ($(this).next().is('.SortAbleDiv')) {
                                $(this).children('article').removeClass('selected');
                                $(this).next('.SortAbleDiv').children('article').click();
                                var idAttr = "#" + $(this).prev('.SortAbleDiv').prev('.SortAbleDiv').prev('.SortAbleDiv').prev('.SortAbleDiv').prev('.SortAbleDiv').attr("id") + "";

                                $("#wrapperRundown").mCustomScrollbar("scrollTo", idAttr);
                                return false
                            }
                            return false;
                        }
                    });
                    $('.panel3 ul li').each(function (e, ele) {
                        if ($(ele).hasClass('selected')) {
                            if ($(this).next().is('li')) {
                                $(this).removeClass('selected');
                                $(this).next('li').click();
                                return false
                            }
                            return false;
                        }
                    });
                    $('.panel1 ul li').each(function (e, ele) {
                        if ($(ele).hasClass('selected')) {
                            if ($(this).next().is('li')) {
                                $(this).removeClass('selected');
                                $(this).next('li').click();
                                return false
                            }
                            return false;
                        }
                    });
                    $('.panel2 ul li').each(function (e, ele) {
                        if ($(ele).hasClass('selected')) {
                            if ($(this).next().is('li')) {
                                $(this).removeClass('selected');
                                $(this).next('li').click();
                                return false
                            }
                            return false;
                        }
                    });
                }
            });

            shortcut.add("enter", function () {
                var ele = $('.panel-body').find('article.selected');
                if ($(ele).hasClass('selected')) {
                    context.$root.visibleFourWindowPopupFlag(true);
                    $(ele).click();
                }
            });
        }
    };

    ko.bindingHandlers.BolCGtabsNav = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, context) {
            $(".panels.border-color.panel3").show();
            $(".text-color.template-tab.tab1").click(function () {
                $(".panels.border-color.panel1").show();
                $(".panels.border-color.panel2").hide();
                $(".panels.border-color.panel3").hide();
                $(".text-color.item-color.border-color.li_tab1").addClass("active");
                $(".text-color.item-color.border-color.li_tab2").removeClass("active")
                $(".text-color.item-color.border-color.li_tab3").removeClass("active")
            });

            $(".text-color.media-tab.tab2").click(function () {
                $(".panels.border-color.panel2").show();
                $(".panels.border-color.panel1").hide();
                $(".panels.border-color.panel3").hide();
                $(".text-color.item-color.border-color.li_tab2").addClass("active");
                $(".text-color.item-color.border-color.li_tab1").removeClass("active")
                $(".text-color.item-color.border-color.li_tab3").removeClass("active")
            });

            $(".text-color.tab3").click(function () {
                $(".panels.border-color.panel2").hide();
                $(".panels.border-color.panel1").hide();
                $(".panels.border-color.panel3").show();
                $(".text-color.item-color.border-color.li_tab3").addClass("active");
                $(".text-color.item-color.border-color.li_tab1").removeClass("active")
                $(".text-color.item-color.border-color.li_tab2").removeClass("active")
            });

            $(".text-color.tab3.player").click(function () {
                $(".modal-body.popUpBG.panels.border-color.playerPanel").show();
                $(".modal-body.popUpBG.panels.border-color.transitionPanel").hide();
                $(".text-color.item-color.border-color.li_tab3335566.playertab").addClass("active");
                $(".text-color.item-color.border-color.li_tab3335566.transition").removeClass("active");
            });
        }
    };

    ko.bindingHandlers.AlertDropdown = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, context) {
            $(element).click(function (e) {
                if ($(this).parent("a").next("ul").hasClass("hide-alerts")) {
                    $(this).parent("a").next("ul").removeClass("hide-alerts").addClass("show-alerts");
                }
                else {
                    $(this).parent("a").next("ul").removeClass("show-alerts").addClass("hide-alerts");
                }
            });
        }
    };

    ko.bindingHandlers.rangeSlider = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, context) {
            //$(element).change(function (e) {
            //    var item = valueAccessor().item
            //    if ($(element).hasClass('X')) {
            //        item.X = $(this).val();
            //    } else if ($(element).hasClass('Y')) {
            //        item.Y = $(this).val();
            //    } else if ($(element).hasClass('Width')) {
            //        item.ActualWidth = $(this).val();
            //    } else if ($(element).hasClass('Height')) {
            //        item.ActualHeight = $(this).val();
            //    }
            //    //context.$root.checkValue();
            //});
        }
    };

    ko.bindingHandlers.popUpTabsNav = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, context) {
            $(element).click(function () {
                $(this).parent('ul').children('li').each(function () {
                    $(this).removeClass('active');
                });
                if ($(this).hasClass('fillKey')) {
                    $(this).addClass('active');
                    $('.panelWindowPopup').hide();
                    $('.transitionPanel').hide();
                    $('.panelDataPopup').show();
                    $('.playerPanel').hide();
                } else if ($(this).hasClass('FourWindow')) {
                    $(this).addClass('active');
                    $('.panelDataPopup').hide();
                    $('.transitionPanel').hide();
                    $('.panelWindowPopup').show();
                    $('.playerPanel').hide();
                } else if ($(this).hasClass('transition')) {
                    $(this).addClass('active');
                    $('.panelDataPopup').hide();
                    $('.panelWindowPopup').hide();
                    $('.playerPanel').hide();
                    $('.transitionPanel').show();
                } else if ($(this).hasClass('playertab')) {
                    $(this).addClass('active');
                    $('.panelDataPopup').hide();
                    $('.panelWindowPopup').hide();
                    $('.transitionPanel').hide();
                    $('.playerPanel').show();
                }

            });
        },
        update: function (element, valueAccessor, allBindingsAccessor) {
            var Decision = valueAccessor().Decision();
            if (Decision == "window") {
                $('.li_tab444').click();
            }
            else if (Decision == "key") {
                $('.li_tab333').click();
            }
            else if (Decision == "transition") {
                $('.li_tab3335566').click();
            }
            else if (Decision == "Video") {
                $('.li_tab12345').click();
            }

        }
    };
    ko.bindingHandlers.settingsTab = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, context) {
            $(element).click(function () {
                $(this).parent('ul').children('li').each(function () {
                    $(this).removeClass('active');
                });
                if ($(this).hasClass('lSettings')) {
                    $(this).addClass('active');
                    $('.connectSetting').hide();
                    $('.ListenSetting').show();
                } else if ($(this).hasClass('cSettings')) {
                    $(this).addClass('active');
                    $('.ListenSetting').hide();
                    $('.connectSetting').show();
                }
            });
        },
    };

    ko.bindingHandlers.settings = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, context) {
            $(element).click(function (e) {
                e.preventDefault();
                var name = $(this).data('target');
                $(name).modal('toggle');
            });
        }
    };
    ko.bindingHandlers.PreviewPopup = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, context) {
            var previewItem = valueAccessor().previewItem;
            var cData = valueAccessor().data;
            $(element).dblclick(function (e) {
                previewItem(cData);
                e.preventDefault();
                var name = $(this).data('popup');
                $(name).modal('toggle');
            });
        }
    };

    ko.bindingHandlers.StopPropagation = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, context) {
            $(element).click(function (e) {
                e.stopPropagation();
            });
        }
    };
    ko.bindingHandlers.DraggableItem = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, context) {
            $(element).draggable({
                helper: "clone",
                appendTo: '.dumy',
                revert: function (is_valid_drop) {
                    if (!is_valid_drop) {
                        $(".templateDrag").hide();
                        return 'invalid';
                    }
                    $(".templateDrag").hide();

                },

                start: function (event, ui) {

                    if ($(this).hasClass("windowDrop")) {
                        $(ui.helper).addClass("draggedItem");
                        $(ui.helper).width("50px");
                        $(ui.helper).height("50px");
                        $(ui.helper).css("list-style-type", "none");
                        $(ui.helper).css("z-index", "100000");
                        $(ui.helper).children("div").children('img').width("100%");
                        $(ui.helper).children("div").children('img').height("100%");
                        $(ui.helper).children("div").children('span').css("display", "none");
                    }

                    if ($(this).attr("data-CIT") == e.CasparItemType.Template) {
                        $(".templateDrag").show();
                        $(ui.helper).addClass("draggedItem");
                        $(ui.helper).width("50px");
                        $(ui.helper).height("50px");
                        $(ui.helper).css("list-style-type", "none");
                        $(ui.helper).children("div").children('img').width("100%");
                        $(ui.helper).children("div").children('img').height("100%");
                        $(ui.helper).children("div").children('span').css("display", "none");

                    } else {
                        $(".templateDrag").hide();
                        $(ui.helper).addClass("draggedItem");
                    }


                }
            });
        }
    };

    ko.bindingHandlers.DroppableArea = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, context) {
            $(element).droppable({
                greedy: true,
                drop: function (event, ui) {
                    $("dumy").html('');
                    if ($(ui.draggable).hasClass("windowDrop")) {
                        return false;
                    }
                    if ($(ui.draggable).hasClass("sort")) {
                        return false;
                    } else {
                        var targetId = 0;
                        var targetTypeId = 0;
                        var fTemplateId = $(ui.draggable).attr("data-FTID");
                        var cItemId = $(ui.draggable).attr("data-CIT");
                        var fTemplateName = $(ui.draggable).attr("data-FTN");
                        var MName = $(ui.draggable).attr("data-MN");
                        var resourceUrl = $(ui.draggable).attr("data-Ulr");
                        var targetName = $(ui.draggable).attr("data-Target");
                        var FileExt = $(ui.draggable).attr("data-extension");
                        var isLocalMedia = $(ui.draggable).attr("data-isLocalMedia");
                        var duration = $(ui.draggable).attr("data-duration");
                        var runOrderId = context.$root.currentRundown().CasparRundownId;
                        var storyId = context.$root.currentStoryToDisplay().StoryId;

                        var obj = { StoryId: storyId, Target: targetName, CasparItemId: cItemId, Name: MName, SequenceOrder: valueAccessor().data, Duration: parseInt(duration) * 1000, Template: { FlashTemplateId: fTemplateId, FlashTemplate: { Name: fTemplateName } } };

                        if ($(event.target).hasClass("templateDrag")) {
                            targetId = $(event.target).parent("div").parent("article").attr("id");
                            targetTypeId = $(event.target).attr("targetTypeId");
                        }
                        context.$root.droppedItem(obj, targetId, targetTypeId, resourceUrl, isLocalMedia, FileExt, runOrderId);
                    }
                },
            }).sortable({
                items: ".SortAbleDiv",
                //handle: 'span.sortingHandle ',
                update: function (event, ui) {
                    var arrayPositions = $(this).sortable('toArray');
                    var roId = $(this).sortable('toArray');
                    context.$root.updateRundownItemSequence(arrayPositions);
                },
            });
        }
    };
    ko.bindingHandlers.CasparContextMenu = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, context) {
            $.contextMenu({
                selector: '.hasmenu',
                callback: function (key, options) {
                    var CRID = $(this).attr("RCIID");
                    //var cindex = $(this).attr("index");
                    var cindex = $("article").index($(this));

                    if (CRID && CRID > 0) {
                        if (key == "Properties") {
                            context.$root.visibleFourWindowPopupFlag(true);
                            $(this).click();
                        } else {
                            $(this).click();
                        }
                        context.$root.cMenuAction(key, CRID, cindex);
                    }
                },
                items: {

                    //"Play": { name: "Play", icon: "Play" },
                    "Skip": { name: "Skip", icon: "Skip" },
                    "UnSkip": { name: "UnSkip", icon: "UnSkip" },
                    "sep1": "---------",
                    "Properties": { name: "Properties", icon: "Properties" }
                }
            });
        }
    };

    ko.bindingHandlers.CasparVideoWallContextMenu = {
        update: function (element, valueAccessor, allBindingsAccessor, viewModel, context) {
            var wallMenuObj = {};
            var walls = context.$root.VideoWalls();
            wallMenuObj["0"] = {
                name: "Select Video Wall",
                //icon: 'videowall',
                "disabled": true
            };
            wallMenuObj["sep1"] = "---------";
            for (var i = 0; i < walls.length; i++) {
                var n = walls[i].Name
                wallMenuObj[walls[i].VideoWallId] = {
                    name: n,
                    id: walls[i].VideoWallId
                };
            }
            //$.contextMenus.removeAll()
            if (walls.length > 0) {

                //alert(JSON.stringify(wallMenuObj))
                $.contextMenu({
                    selector: '.hasmenuwall',
                    callback: function (key, options) {
                        var CRID = $(this).attr("RCIID");

                        var targetId = 0;
                        var targetTypeId = 0;
                        var fTemplateId = $(this).attr("data-FTID");
                        var cItemId = $(this).attr("data-CIT");
                        var fTemplateName = $(this).attr("data-FTN");
                        var MName = $(this).attr("data-MN");
                        var resourceUrl = $(this).attr("data-Ulr");
                        var targetName = $(this).attr("data-Target");
                        var FileExt = $(this).attr("data-extension");
                        var isLocalMedia = $(this).attr("data-isLocalMedia");
                        var runOrderId = context.$root.currentRundown().CasparRundownId;
                        var storyId = context.$root.currentStoryToDisplay().StoryId;
                        var videoid = key

                        var obj = { VideoWallId: videoid, StoryId: storyId, Target: targetName, CasparItemId: cItemId, Name: MName, Template: { FlashTemplateId: fTemplateId, FlashTemplate: { Name: fTemplateName } } };
                        if ($(event.target).hasClass("templateDrag")) {
                            targetId = $(event.target).parent("div").parent("article").attr("id");
                            targetTypeId = $(event.target).attr("targetTypeId");
                        }
                        context.$root.droppedItem(obj, targetId, targetTypeId, resourceUrl, isLocalMedia, FileExt, runOrderId);
                    },
                    items: wallMenuObj
                });
            }
        }
    };

    ko.bindingHandlers.RundownContextMenu = {
        update: function (element, valueAccessor, allBindingsAccessor, viewModel, context) {
            var rundownMenuObj = {};
            rundownMenuObj["0"] = {
                name: "Add Story",
                //icon: 'videowall',
                "disabled": false
            };
            //$.contextMenus.removeAll()

            //alert(JSON.stringify(wallMenuObj))
            $.contextMenu({
                selector: '.hasmenurundown',
                callback: function (key, options) {
                    $(this).click();
                    $('.AddNewStory').modal('show');
                },
                items: rundownMenuObj
            });
        }
    };

    ko.bindingHandlers.ShowAudioPlayer = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, context) {
            $(element).on('click', function (data) {
                $(element).parent('ul').parent('div').parent('div').parent('article').parent('section').children('.cancellationBox').css({
                    'display': 'initial'
                });
            })
        }
    };
    ko.bindingHandlers.closeAudioPlayer = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, context) {
            $(element).on('click', function (data) {
                $(element).parent('h3').parent('.cancellationBox').css({
                    'display': 'none'
                });
            })
        }
    };

    ko.bindingHandlers.ResetScroll = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, context) {
            $(element).on('click', function (data) {
                var totalPara = $(element).parent('p').parent('div').children('.AditorCommentArea').children('div').children('div').children('div').find('p').length + 1;
                var count = $(element).parent('p').parent('div').children('.AditorCommentArea').children('div').children('div').children('div')[0].scrollHeight;
                //$(element).parent('p').parent('div').children('.AditorCommentArea').children('div').children('div').children('div').mCustomScrollbar().resize();
                //$(element).parent('p').parent('div').children('.AditorCommentArea').children('div').children('div').children('div').css({
                //    'top': -count+200+'px'
                //});

            })
        }
    };

    ko.bindingHandlers.expandCollapseCRundown = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, context) {
            $(element).click(function () {
                growingDiv = $(this).parent('span').parent('div').parent('article').next('div');
                if (growingDiv.height() > 0) {
                    growingDiv.css("height", 0);
                    $(this).html('+');
                } else {
                    var wrapper = $(this).parent('span').parent('div').parent('article').next('div').children('div').height();
                    growingDiv.css("height", wrapper);
                    $(this).html('-');
                }
            });
        }
    };

    ko.bindingHandlers.sanAreaTabs = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, context) {
            var sFlag = valueAccessor().searchFlag;
            var sanSearchTypeId = valueAccessor().sanSearchTypeId;

            $(element).click(function () {
                $('.search-icons label').removeClass('selctedSAN');
                $(this).parent().addClass('selctedSAN');
                if ($(this).attr("cmd") == "audio") {
                    sFlag(false);
                    sanSearchTypeId(3);
                    context.$root.sanResult([]);
                    $(".templateSearchArea").hide();
                    $(".localMediaSearchArea").hide();
                    $(".sanSearchArea").show();
                    if (context.$root.sanSearchText()) {
                        context.$root.getSanResult(10, 1, context.$root.sanSearchText());
                    } else {
                        context.$root.getSanResult(10, 1, context.$root.mediaSearchText());
                    }

                } else if ($(this).attr("cmd") == "video") {
                    sFlag(false);
                    sanSearchTypeId(2);
                    context.$root.sanResult([]);
                    $(".templateSearchArea").hide();
                    $(".localMediaSearchArea").hide();
                    $(".sanSearchArea").show();
                    if (context.$root.sanSearchText()) {
                        context.$root.getSanResult(10, 1, context.$root.sanSearchText());
                    } else {
                        context.$root.getSanResult(10, 1, context.$root.mediaSearchText());
                    }

                } else if ($(this).attr("cmd") == "image") {
                    sFlag(false);
                    sanSearchTypeId(1);
                    context.$root.sanResult([]);
                    $(".templateSearchArea").hide();
                    $(".localMediaSearchArea").hide();
                    $(".sanSearchArea").show();
                    if (context.$root.sanSearchText()) {
                        context.$root.getSanResult(10, 1, context.$root.sanSearchText());
                    } else {
                        context.$root.getSanResult(10, 1, context.$root.mediaSearchText());
                    }

                } else if ($(this).attr("cmd") == "template") {
                    sFlag(true);
                    $(".sanSearchArea").hide();
                    $(".localMediaSearchArea").hide();
                    $(".templateSearchArea").show();
                    if (context.$root.sanSearchText()) {
                        context.$root.getSanResult(10, 1, context.$root.sanSearchText());
                    } else {
                        context.$root.getSanResult(10, 1, context.$root.mediaSearchText());
                    }

                } else if ($(this).attr("cmd") == "all") {
                    sFlag(false);
                    sanSearchTypeId(null);
                    context.$root.sanResult([]);
                    $(".templateSearchArea").hide();
                    $(".localMediaSearchArea").hide();
                    $(".sanSearchArea").show();
                    if (context.$root.sanSearchText()) {
                        context.$root.getSanResult(10, 1, context.$root.sanSearchText());
                    } else {
                        context.$root.getSanResult(10, 1, context.$root.mediaSearchText());
                    }

                } else if ($(this).attr("cmd") == "localMediaArea") {
                    sFlag(true);
                    context.$root.mediaSearchText("");
                    context.$root.sanSearchText("");
                    $(".templateSearchArea").hide();
                    $(".sanSearchArea").hide();
                    $(".localMediaSearchArea").show();
                    if (context.$root.sanSearchText()) {
                        context.$root.getSanResult(10, 1, context.$root.sanSearchText());
                    } else {
                        context.$root.getSanResult(10, 1, context.$root.mediaSearchText());
                    }

                } else { }


            });
        }
    };

    ko.bindingHandlers.reportBugHandler = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, context) {
            var options = valueAccessor();
            $(element).click(function () {
                if (options.reportBugName() && options.reportBugEmail() && options.reportBugContact() && options.reportBugDetail()) {
                    var obj = { Name: options.reportBugName(), Email: options.reportBugEmail(), Contact: options.reportBugContact(), Detail: options.reportBugDetail() };
                    var name = $(element).data('target');
                    $(name).modal('toggle');
                    setTimeout(function () {
                        context.$root.reportBug(obj);
                    }, 1000);
                } else {

                    config.logger.error("Fill all fields");
                }
            });
        }
    };
    ko.bindingHandlers.windowPopUpDroppableArea = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, context) {
            $(element).droppable({
                greedy: true,
                drop: function (event, ui) {
                    $("dumy").html('');
                    var targetName = $(ui.draggable).attr("data-Target");
                    var resourceUrl = $(ui.draggable).attr("data-Ulr");
                    var draggedItemCasparItemId = $(ui.draggable).attr("data-CIT");
                    var fileExtension = $(ui.draggable).attr("data-extension");
                    var isLocalMedia = $(ui.draggable).attr("data-isLocalMedia");
                    var targetWindowId = $(event.target).attr("FTWId");
                    var runOrderId = context.$root.currentRundown().CasparRundownId;
                    var storyId = context.$root.currentStoryToDisplay().StoryId;

                    if (targetWindowId) {
                        context.$root.updateTemplateWindowsOnDrag(targetName, targetWindowId, resourceUrl, draggedItemCasparItemId, fileExtension, isLocalMedia, runOrderId, storyId);
                    } else {
                        $(ui.draggable).draggable('option', 'revert', true);
                    }
                },
            });

        }
    };
    ko.bindingHandlers.windowPopUpDroppableAreaWall = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, context) {
            $(element).droppable({
                greedy: true,
                drop: function (event, ui) {
                    $("dumy").html('');
                    var targetName = $(ui.draggable).attr("data-Target");
                    var resourceUrl = $(ui.draggable).attr("data-Ulr");
                    var draggedItemCasparItemId = $(ui.draggable).attr("data-CIT");
                    var fileExtension = $(ui.draggable).attr("data-extension");
                    var isLocalMedia = $(ui.draggable).attr("data-isLocalMedia");
                    var targetWindowId = $(event.target).attr("FTWId");
                    var runOrderId = context.$root.currentRundown().CasparRundownId;
                    var storyId = context.$root.currentStoryToDisplay().StoryId;
                    var WallId = context.$root.currentItem().VideoWallId;


                    if (targetWindowId) {
                        context.$root.updateTemplateWindowsOnDrag(targetName, targetWindowId, resourceUrl, draggedItemCasparItemId, fileExtension, isLocalMedia, runOrderId, storyId, WallId);
                    } else {
                        $(ui.draggable).draggable('option', 'revert', true);
                    }
                },
            });
        }
    };
    ko.bindingHandlers.scrollToTop = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, context) {
            $(element).on('click', function () {

                $('#home-view').mCustomScrollbar("scrollTo", 0, { scrollInertia: 3000 });
                setTimeout(function () {
                    $('#home-view .mCSB_container').css("top", 0);
                }, 3000)
            });
        }
    }

    ko.bindingHandlers.checkedForAll = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, context) {
            var options = valueAccessor();
            $(element).click(
            function () {
                if (!$(element).hasClass('checked')) {
                    $(element).addClass('checked');
                    options.callback(true);
                }



                else {
                    $(element).removeClass('checked');
                    options.callback(false);
                }
            });
        }
    };

    ko.bindingHandlers.ShowCurrentTab = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, context) {
            var options = valueAccessor();
            $(element).click(
            function () {
                $('#tab1').removeClass('tabHide');
                $('#tab1').show();
                //$('#tab2').removeClass('tabHide');
            });

        }
    };

    ko.bindingHandlers.LoadingBarBolCg = {
        update: function (element, valueAccessor, allBindingsAccessor, viewModel) {
            var loadingScreenFlag = valueAccessor().loadingScreenFlag;
            if (loadingScreenFlag()) {
                $(".loadingDiv").show();
            }
            else {
                $(".loadingDiv").hide();
            }
        }
    };
    ko.bindingHandlers.toggleClassActive = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, context) {
            $(element).on('click', function (data) {
                if (!$(element).hasClass('active')) {
                    $(element).addClass('active');
                }
            })
        }
    };
    ko.bindingHandlers.BrowseControl = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, context) {
            $(element).on('click', function () {
                context.$root.browseFile();
            });
        }
    };
    ko.bindingHandlers.BrowseControlWall = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, context) {

            $(element).on('click', function () {
                context.$root.browseFileWall();
            });
        }
    };


    ko.bindingHandlers.clickChild = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, context) {
            $(element).hover(
         function () {
             $(element).addClass('overlay')
         },
         function () {
             $(element).removeClass('overlay')
         });
            $(element).on('click', function (data) {
                if ($(element).hasClass('overlay')) {
                    $(element).removeClass('overlay');
                    $(element).css('opacity', 1.0);
                    $(element).children('area').click();
                    if ($('.galleryView').hasClass('active'))
                    { }
                    else
                    {
                        $('.galleryView').children('span').click();
                    }
                }
            })
        }
    };

    ko.bindingHandlers.ThemeLoader = {
        update: function (element, valueAccessor, allBindingsAccessor, viewModel, context) {
            var className = valueAccessor().currentTheme;
            if (className()) {
                $("body").addClass(className());
            }
        }
    };

    ko.bindingHandlers.AddNewRunDown = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, context) {
            $(element).on('click', function () {
                var name = $(element).data('target');
                $(name).modal('toggle');

            });
        }
    };

    ko.bindingHandlers.prompterSort = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, context) {
            $(element).sortable({
                items: ".SortAbleDiv",
                // handle: 'span.sortingHandle ',
                update: function (event, ui) {
                    var arrayPositions = $(this).sortable('toArray');
                    context.$root.arrangeStories(arrayPositions);
                },
            });
        }
    };
    ko.bindingHandlers.controllerDropdown = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, context) {

            $(".preferenceSelect").change(function () {
                // Get the selected value
                var selected = $("option:selected", $(this)).val();
                // Get the ID of this element
                var thisID = $(this).attr("id");
                // Reset so all values are showing:
                $(".preferenceSelect option").each(function () {
                    $(this).show();
                });
                $(".preferenceSelect").each(function () {
                    if ($(this).attr("id") != thisID) {
                        $("option[value='" + selected + "']", $(this)).attr("disabled", true);
                    }
                });

            });
        }
    };

    ko.bindingHandlers.deleteUploadResource = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, context) {
            $(element).on('click', function (data) {
                $(element).parent('li').remove();
            })
        }
    };


    ko.bindingHandlers.producerScrollPos = {
        update: function (element, valueAccessor, allBindingsAccessor, viewModel) {
            var options = valueAccessor();
            var scrollPos = 0;
            if (options.canScroll()) {
                if (options.storeScroll()) {
                    appdata.producerScrollTop($('#home-view').children('div').children('.mCSB_container').css('top'));
                }
                else {
                    scrollPos = appdata.producerScrollTop();
                    scrollPos = scrollPos.replace('px', '');
                    $('#home-view').children('div').children('.mCSB_container').css('top', parseInt(scrollPos));
                }
            }
        }
    };
    ko.bindingHandlers.tickerDropDown = {

        init: function (element, valueAccessor, allBindingsAccessor, viewModel, context) {

            var hasDropDown = parseInt($(element).text());
            $(element).addClass('more');
            $(element).on('click', function () {
                viewModel.dropDownVisible(!viewModel.dropDownVisible());
            });

        },
        update: function (element, valueAccessor, allBindingsAccessor, viewModel) {
            var options = valueAccessor();
            if (options && options.dropDownVisible()) {
                $(element).parents('.new-wrap').find('.more-news').stop(0, 0).slideDown(400);
            }
            else if (options && !options.dropDownVisible()) {
                $(element).parents('.new-wrap').find('.more-news').stop(0, 0).slideUp(400);
            }
        }
    };

    ko.bindingHandlers.tickerEditorControls = {

        init: function (element, valueAccessor, allBindingsAccessor, viewModel, context) {

            var options = valueAccessor();

            if (options && options.tickerMode) {

                var tickerMode = options.tickerMode;

                var selectedData = valueAccessor().selectedData;

                var typeOfData = selectedData instanceof context.$root.model.Ticker ? 'Ticker' : selectedData instanceof context.$root.model.TickerLine ? 'TickerLine' : '';

                var languageCode = '';

                languageCode = typeOfData === 'Ticker' ? selectedData.tickerLines()[0].languageCode() : typeOfData === 'TickerLine' ? selectedData.languageCode() : '';

                $(element).on('click', function (e) {


                    if (tickerMode === 'add' || tickerMode === 'edit' || tickerMode == 'edittickerline') {
                        $(this).parents('ul').find('li span,li strong,li small').fadeOut(0).parents('ul').find('.prioritysec').fadeIn(300);
                        $(this).parents('ul').find('li span i.icon-newcheck').parent().fadeIn(300);
                        var ptxt = $(this).parents('.newslst').find('.timenews p').clone(true),
                            rmvspn = $(ptxt).find('span').remove(), txt = $(ptxt).text();
                        $(this).parents('.newslst').find('.timenews strong, .timenews p').fadeOut(0);
                        $(this).parents('.editControls').children('.setToggle').removeClass('closeBox');
                        $(this).parent().parent().children('.editControls').children('.setToggle').removeClass('closeBox');
                        $(this).parent().parent().find('.addControl.Freezed').hide();
                        $(this).parent().parent().find('.tickerInfo').hide();
                        $(this).parent().parent().find('.tickerDropDown').hide();
                        $(this).parent().parent().parent().find('.timenews').find('span.tickerInfo').hide();

                        var inputBox = $(this).parents('.newslst').find('.timenews').children('.tickertext');
                        $(inputBox).addClass('editFalse');

                        if (tickerMode === 'edit') {
                            context.$root.appdata.tickerIsInEditMode(true);
                            $(this).parents('.newslst').find('.timenews').children('span').removeClass('tickerInfo');
                            $(this).parents('.newslst').find(".severityBar").show();
                            $(this).parents('.newslst').find(".frequencyBar").show();
                            $(this).parents('.newslst').find('.prioritysec').show();
                            $(this).parents('.newslst').find('.othertabs').hide();
                            $(this).parents('.newslst').find('.updatelineli').show();
                            if (typeOfData == 'Ticker')
                                context.$root.textBuffer(selectedData.tickerLines()[0].title());
                            else if (typeOfData == 'TickerLine')
                                context.$root.textBuffer(selectedData.title());

                        }
                        if (tickerMode == 'edittickerline') {
                            $(this).parents('.newslst').find('.editControls').find('.checkticker').css("display", "inline-block");

                        }
                        else if (tickerMode === 'add') {
                            $(this).parents('.newslst').find(".frequencyBar").hide();
                            $(this).parents('.newslst').find(".severityBar").hide();
                            $(this).parents('.newslst').find('.othertabs').hide();
                            $(this).parents('.newslst').find('.prioritysec').hide();
                            $(this).parents('.newslst').find('.updatelineli').show();
                            context.$root.appdata.tickerIsInEditMode(true);
                        }
                    }
                    else if (tickerMode === 'check') {

                        if (options.callBack && options.type) {
                            options.callBack.updateTickerStatus(selectedData, context.$root.e.TickerStatus.Approved, options.type);
                        }
                        context.$root.textBuffer('');
                        context.$root.appdata.tickerIsInEditMode(false);
                    }
                    else
                        context.$root.appdata.tickerIsInEditMode(false);


                    $(inputBox).removeClass('englishCSS')
                    $(inputBox).UrduEditor();
                    $(inputBox).focus();

                    $(inputBox).keyup(function (e) {
                        typeOfData === 'Ticker' ? context.$root.textBuffer($(this).val()) : typeOfData === 'TickerLine' ? context.$root.textBuffer($(this).val()) : '';
                        $(this).css("border-color", "#3079ed");
                    });
                });

            }
        },
        update: function (element, valueAccessor, allBindingsAccessor, viewModel) {

        }
    };

    ko.bindingHandlers.tickerPrioritySection = {

        init: function (element, valueAccessor, allBindingsAccessor, viewModel, context) {

            $(element).children('ul:not(.frequency)').each(function () {
                $(this).find('li').first().addClass('active');
            });
            $(element).children('ul:not(.frequency)').children('li').on('click', function () {
                $(this).addClass('active').siblings().removeClass('active');
            });

        },
        update: function (element, valueAccessor, allBindingsAccessor, viewModel) {

        }
    };

    ko.bindingHandlers.tickerRundownCount = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, context) {
            var selectedValue = valueAccessor().selectedValue;

            $(element).each(function () {
                var $this = $(this), val = $this.children('option:selected').text();
                if (!$this.parent().hasClass('xSelect')) {
                    $this.wrap('<div class="xSelect" style="position:relative;" />');
                    $this.parent().append('<p>' + val + '</p>');
                    $this.css({ 'position': 'absolute', 'top': '0px', 'left': '0px', 'width': '100%', 'height': '100%', 'z-index': '10', 'opacity': 0 });
                    $this.on('change', function () {
                        val = $(this).val();
                        if (selectedValue)
                        { $(this).parent().children('p').text(selectedValue + " Times"); }

                        else
                        {
                            $(this).parent().children('p').text(val);
                        }
                    });
                }
            });

        },
        update: function (element, valueAccessor, allBindingsAccessor, viewModel) {

        }
    };

    ko.bindingHandlers.tickerEditorSect = {

        init: function (element, valueAccessor, allBindingsAccessor, viewModel, context) {
            var options = valueAccessor();
            $(element).on('click', function () {
                var qnt = $(".eachrow").length;
                var nq = qnt - 1;
                var aq = qnt + 1;
                var tgt = $(this).find('i');
                if (tgt.is('.icon-tickrsub')) {
                    options.callback($(this).parent().children()[1].value, true);
                    $(this).parent().remove();
                } else {
                    if ($(this).parent().find('input').get(0).value.trim()) {
                        $(this).parent().find('input').get(0).disabled = true;
                        var getcopy = $(this).parent().clone(true).addClass('sub');
                        //var severityval = $(this).parent().find('ul').find('.active').attr('severityval');
                        var severityval = $(this).parent().find('ul').find('.active')[0].attributes['severityval'].value;
                        var frequencyval = $(this).parent().find('ul').find('.active')[1].attributes['frequencyval'].value;
                        options.callback($(this).parent().children()[1].value, false, severityval, frequencyval);
                        $(getcopy).insertBefore($('.popups #content-viewer .innercont .news-tickr .edtrsec .mCustomScrollBox .mCSB_container').children('div:last-child'));
                        $(getcopy).find('i').removeClass('icon-tickradd').addClass('icon-tickrsub');
                        $(getcopy).find('ul').addClass('disablePriority');
                        //$(getcopy).find('.line-severity li').each(function () {
                        //    $(this).on("click", function () {
                        //        debugger
                        //   })
                        //});
                        //$(getcopy).find('.line-frequency li').each(function () {
                        //    $(this).on("click", function () {
                        //        debugger
                        //    })
                        //});
                        setTimeout(function () { $('.edtrsec').mCustomScrollbar('scrollTo', 'bottom'); }, 100);
                    }
                    $(this).parent().find('input').get(0).value = '';
                    $(this).parent().find('input').get(0).disabled = false;
                }
            });

        },
        update: function (element, valueAccessor, allBindingsAccessor, viewModel) {

        }
    };
    ko.bindingHandlers.languageChangeTickerWriter = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            var selectedObservableArrayInViewModel = valueAccessor();
            var tempObject = {};
            var urduEditors = [];
            var languageCode = selectedObservableArrayInViewModel.languageCode;
            $(element).click(function () {
                var selectedObservableArrayInViewModel = valueAccessor();
                var languageCode = selectedObservableArrayInViewModel.languageCode;
                var language = $(this).data('lang');
                if (language == "ur") {
                    languageCode('ur');
                    $('.eachrow').each(function () {
                        if ($(this).children('input')) {
                            if ($(this).children('input').hasClass('urdumode')) {
                                $(this).children('input').attr("UrduEditorId", "UrduEditor_1");
                                $(this).children('input').attr("dir", "rtl");
                                $(this).children('input').attr("lang", "ur");
                            }
                            else {
                                $(this).children('input').addClass('urdumode')
                                //  $(this).children('input').UrduEditor();
                            }

                        }

                        $(this).children('input').addClass('tickerUrduContent');
                    });
                }
                if (language == "en") {
                    languageCode('en');
                    $('.eachrow').each(function () {
                        $(this).children('input').removeAttr('urdueditorid dir lang style');
                        $(this).children('input').removeClass('tickerUrduContent');
                    });
                }
            });
        },
    };
    ko.bindingHandlers.PrompterDefaultLanguage = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, context) {
            var selectedLanguage = valueAccessor().selectedLanguage;
            $(element).click(function () {
                if (selectedLanguage() == "URDU") {
                    $(".SelectUrdu").click();
                } else if (selectedLanguage() == "ENGLISH") {
                    $("SelectEnglish").click();
                } else { $(".SelectUrdu").click(); }
            });
        },
    };

    ko.bindingHandlers.EditRunDownItemName = {
        update: function (element, valueAccessor, allBindingsAccessor, viewModel) {
            //---------------
            var data = valueAccessor().data;
            var updateRundownItemProperties = valueAccessor().updateRundownItemProperties;
            var propertyToUpdate = valueAccessor().property;

            $(element).dblclick(function (e) {
                oriVal = $(this).text();
                $(this).text("");
                if (propertyToUpdate == "VideoLayer") {
                    $('<input style="width:35px" class="newValue" type="text" value="' + oriVal + '" /> ').appendTo(this).focus();
                } else {
                    $('<input class="newValue" type="text" value="' + oriVal + '" /> ').appendTo(this).focus();
                }
                $(".newValue").on('focusout', function () {
                    var $this = $(this);
                    var newVal = $this.val();
                    $(element).text(newVal || oriVal);
                    if (propertyToUpdate == "VideoLayer") {
                        data.VideoLayer = newVal;
                    } else {
                        data.Name = newVal;
                    }

                    if (newVal && newVal !== oriVal) {
                        updateRundownItemProperties(newVal, data.CasparRundownItemId, propertyToUpdate);
                    }
                    $this.remove();
                });
            });
        }
    };

    ko.bindingHandlers.CGDefaultLanguage = {
        update: function (element, valueAccessor, allBindingsAccessor, viewModel, context) {
            var selectedLanguage = valueAccessor().selectedLanguage;
            if (selectedLanguage() == "URDU") {
                selectedLanguage("");
                $(".SelectUrdu").click();
            } else if (selectedLanguage() == "ENGLISH") {
                selectedLanguage("");
                $("SelectEnglish").click();
            } else { $(".SelectUrdu").click(); }
        },
    };

    ko.bindingHandlers.checkforunsubmitTicker = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, context) {
            var selectedValue = valueAccessor().selectedValue;
            var options = valueAccessor();

            $(element).click(function () {
                if (options.callback)
                    options.callback();
                if (options.isTickerClear && options.isTickerClear()) {
                    $('.eachrow').each(function () {
                        if ($(this).hasClass('sub')) {
                            $(this).remove();
                        }
                        else {
                            $(this).children('input').val('');
                        }
                    });
                    if (options.closePopup)
                        options.closePopup();
                }
            });
        }
    };

    ko.bindingHandlers.updateTickersCount = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, context) {
            var selectedValue = valueAccessor().currentCount;
            var lastTicker = valueAccessor().lastTicker;
            $(element).keyup(function (e) {

                setTimeout(function () {
                    selectedValue(0);
                    $(element).parent().parent().children().each(function () {
                        var $this = $(this);
                        if ($this.children('input').val().trim()) {
                            selectedValue(selectedValue() + 1);
                            if (lastTicker) {
                                lastTicker($this.children('input').val().trim());
                            }
                        }
                    });
                }, 500);
            });
        },
    };



    ko.bindingHandlers.videoMarkers = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, context) {

            element.addEventListener('loadedmetadata', function () {
                element.currentTime = valueAccessor().From();
                $("#startPoint").remove();
                var width = $("#pointerArea").width();
                var time = element.duration;
                var percent = width / time;
                var left = parseInt(percent * valueAccessor().From());
                $("#pointerArea").append("<div class='marker' id='startPoint' style='width:auto;position:absolute;margin-left:" + left + "px;cursor:pointer;margin-top:-25px;'><div class='arrow-up'></div></div>");

                $("#endPoint").remove();
                var width = $("#pointerArea").width();
                var time = element.duration;
                var percent = width / time;
                var left = parseInt(percent * valueAccessor().TOs());
                $("#pointerArea").append("<div class='marker' id='endPoint' style='width:auto;position:absolute;margin-left:" + left + "px;cursor:pointer;margin-top:-25px;'><div class='arrow-up'></div></div>");
            });
        }
    };

    ko.bindingHandlers.videoPlayerKeyPress = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, context) {

            $(document).keydown(function (e) {

                if (e.target.localName == 'body') {
                    videoUrl(videoEditor().src())
                    var videoPlayer = $('#videoplayer').get(0);
                    if (videoUrl()) {
                        if (e.keyCode == 39) {   //   right arrow
                            if (option1() == "true") {
                                videoPlayer.play();
                                isPlaying(true);
                                if (From() == 0) {
                                    From(videoPlayer.currentTime);
                                }
                                videoPlayer.pause();
                            }
                            else {
                                videoPlayer.currentTime = videoPlayer.currentTime + seekchangeVal;
                                if (From() == 0) {
                                    From(videoPlayer.currentTime);
                                }
                                if (TOs() == 0) {
                                    TOs(videoPlayer.currentTime);
                                }
                            }
                        }
                        else if (e.keyCode == 37) {   //  left arrow
                            videoPlayer.currentTime = videoPlayer.currentTime - seekchangeVal;
                        }
                        else if (e.keyCode == 32) {   //  space
                            if (isPlaying() == false) {
                                videoPlayer.play();
                                isPlaying(true);
                            } else if (isPlaying() == true) {
                                videoPlayer.pause();
                                isPlaying(false);
                            }
                        }
                        else if (e.keyCode == 13) {   //  enter
                            videoPlayer.pause();
                            isPlaying(false);
                            if (From() == 0) {
                                From(videoPlayer.currentTime);
                            }
                            else {
                                TOs(videoPlayer.currentTime);
                            }
                        }
                        makeTime(videoPlayer.currentTime);

                        var width = $("#pointerArea").width();
                        var time = videoPlayer.duration;
                        var percent = width / time;
                        var left = parseInt(percent * From());
                        $("#startPoint").remove();
                        $("#pointerArea").append("<div class='marker' id='startPoint' style='width:auto;position:absolute;margin-left:" + left + "px;cursor:pointer;margin-top:-25px;'><div class='arrow-up'></div></div>");

                        var right = parseInt(percent * TOs());
                        $("#endPoint").remove();
                        $("#pointerArea").append("<div class='marker' id='endPoint' style='width:auto;position:absolute;margin-left:" + right + "px;cursor:pointer;margin-top:-25px;'><div class='arrow-up'></div></div>");

                        listFromTos.push({ From: From(), To: TOs(), Left: '', width: '' });
                    }
                }
            });
        }
    };

    ko.bindingHandlers.chatBoxProducer = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, context) {
            var options = valueAccessor();
            $(document).keydown(function (e) {
                if (e.keyCode == 13) {
                    if (e.target.className == "chatTextArea")
                        options.callback();

                }
            });
        }
    };
    ko.bindingHandlers.toggleActiveClass = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, context) {
            var options = valueAccessor();
            $(element).click(function () {
                if ($(this).hasClass('active')) {
                    $(this).removeClass('active');
                }
                else {
                    $('.topTags').each(function () {
                        $(this).removeClass('active');
                    });
                    $(this).addClass('active');
                }
            });
        }
    };

    ko.bindingHandlers.toggleLeftPanel = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, context) {
            var options = valueAccessor();
            $('body').live('click', function (e) {

                if (e.target.className != "leftPanelRundown") {
                    context.$root.productionlp.isStoryBucketVisible(false)
                }

            });
            $(element).click(function () {

            });
        }
    };
    ko.bindingHandlers.toggleGoogleLink = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, context) {
            var options = valueAccessor();
            $(element).click(function () {
                if ($(this).hasClass('toggleGoogleContent')) {
                    $(this).removeClass('toggleGoogleContent');
                }
                else {
                    $(this).addClass('toggleGoogleContent');
                }
            });
        }
    };

    ko.bindingHandlers.toggleArchivalButton = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, context) {
            var options = valueAccessor();
            $(element).click(function () {
                if ($(this).hasClass('toggleArchivalButton')) {
                    $("#archivalSearchBtn").attr('title', 'Turn Archival Search ON');
                    $(this).removeClass('toggleArchivalButton');
                }
                else {
                    $(this).addClass('toggleArchivalButton');
                    $("#archivalSearchBtn").attr('title', 'Turn Archival Search OFF');
                }
            });
        }
    };
    ko.bindingHandlers.addCurrentResource = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, context) {
            var options = valueAccessor();
            $(element).click(function () {

            });
        }
    };

    ko.bindingHandlers.toggleMyTeamSearch = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            var options = valueAccessor();
            $(element).click(function () {
                $(element).parent().children().each(function () {
                    $(this).removeClass('TeamSearchEnable');
                });
                $(element).addClass('TeamSearchEnable');

                //if ($(this).hasClass('TeamSearchEnable')) {
                //    $(this).removeClass('TeamSearchEnable');
                //    $(this).attr('title', 'Team Only Serach OFF');
                //}
                //else {
                //    $(this).addClass('TeamSearchEnable');
                //    $(this).attr('title', 'Team Only Serach ON');
                //}
            });
        }
    };

    ko.bindingHandlers.ReportertickerEditorSect = {

        init: function (element, valueAccessor, allBindingsAccessor, viewModel, context) {
            var options = valueAccessor();
            $(element).on('click', function () {
                var qnt = $(".eachrow").length;
                var nq = qnt - 1;
                var aq = qnt + 1;
                var tgt = $(this).find('i');
                if (tgt.is('.icon-tickrsub')) {
                    options.callback($(this).parent(), true);
                    $(this).parent().remove();
                } else {
                    if ($(this).parent().children()[1].value.trim()) {
                        $(this).parent().children()[1].disabled = true;
                        var getcopy = $(this).parent().clone(true).addClass('sub');
                        options.callback($(this).parent(), false);
                        $(getcopy).insertBefore($('.innercont .news-tickr .edtrsec .mCustomScrollBox .mCSB_container').children('div:last-child'));
                        $(getcopy).find('i').removeClass('icon-tickradd').addClass('icon-tickrsub');
                        setTimeout(function () { $('.edtrsec').mCustomScrollbar('scrollTo', 'bottom'); }, 100);
                    }
                    $(this).parent().children()[1].disabled = false;
                    $(this).parent().children()[1].value = '';
                }
            });

        },
        update: function (element, valueAccessor, allBindingsAccessor, viewModel) {

        }
    };

    ko.bindingHandlers.tickerReporterWriter = {

        init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            var options = valueAccessor();
            $(element).each(function () {
                if ($(this).children('input')) {
                    if ($(this).children('input').hasClass('urdumode')) {
                        $(this).children('input').attr("UrduEditorId", "UrduEditor_1");
                        $(this).children('input').attr("dir", "rtl");
                        $(this).children('input').attr("lang", "ur");
                    }
                    else {
                        $(this).children('input').addClass('urdumode')
                        //$(this).children('input').UrduEditor();
                    }

                }

                $(this).children('input').addClass('tickerUrduContent');
            });
        }
    };

    ko.bindingHandlers.activeNewsTab = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            var options = valueAccessor();
            $(element).click(function () {
                $(element).parent().children('li').each(function () {
                    $(this).removeClass('activeNewsTab');
                });
                $(element).addClass('activeNewsTab');
            });
        }
    }
    ko.bindingHandlers.test = {

        init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {

        },
        update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            if (viewModel.isGreen() && !viewModel.isRed())
                $(element).addClass('greenBorder');
            else if (viewModel.isRed())
                $(element).addClass('redBorder');
            else if (!viewModel.isGreen())
                $(element).addClass('blueBorder');
        }
    };
    ko.bindingHandlers.tickerClear = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {

        },
        update: function (element, valueAccessor, allBindingsAccessor, viewModel) {
            var options = valueAccessor();
            if (options.isTickerClear && options.isTickerClear()) {
                $('.eachrow').each(function () {
                    if ($(this).hasClass('sub')) {
                        $(this).remove();
                    }
                    else {
                        $(this).children('input').val('');
                    }
                });
            }
        }
    };


    ko.bindingHandlers.testingEditor = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            var options = valueAccessor();
            var EditorId;
            $(element).click(function () {
                if ($(element).hasClass('en')) {
                    $('#txtEditor1').removeAttr('urdueditorid dir lang style');
                }
                else if ($(element).hasClass('un')) {
                    if ($(element).hasClass('secondTime')) {
                        $("#txtEditor1").attr("UrduEditorId", EditorId);
                        $("#txtEditor1").attr("dir", "rtl");
                        $("#txtEditor1").attr("style", "font-family: 'Jameel Noori Nastaleeq', 'Nafees nastaleeq', 'Nafees Web Naskh', Helvetica, sans-serif; background-color: rgb(245, 245, 245);");
                        $("#txtEditor1").attr("lang", "ur");
                    }
                    else {
                        $("#txtEditor1").UrduEditor();
                        EditorId = $("#txtEditor1").attr("urdueditorid");
                        $(element).addClass('secondTime');
                    }
                }

            });
        }
    };
    ko.bindingHandlers.setHtml = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            var options = valueAccessor();
            $(element).html(' ' + $(element).html());
        }
    };

    ko.bindingHandlers.slideGroupStories = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            $(element).click(function (e) {
                $(this).parent('div').children('.shwmrartcls').slideToggle();
                e.stopPropagation();
            });

            $('body').click(function () {
                $(this).parent('div').children('.shwmrartcls').slideUp();
            });
        }
    };
    ko.bindingHandlers.setUrduEditorForKeys = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            $(element).UrduEditor();
        }
    };
    ko.bindingHandlers.seletctedNewsFile = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            $(element).click(function (e) {
                $(this).parent().parent().parent().parent().each(function () {
                    $(this).parent().removeClass('currentSelectedFile');
                });
                $(this).parent().addClass('currentSelectedFile');
            });
        }
    };
    ko.bindingHandlers.editableNewsCopy = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, context) {
            $(element).click(function (e) {
                var selectedObservableArrayInViewModel = valueAccessor();
                var fileObject = selectedObservableArrayInViewModel.selectedFile;
                context.$root.currentNewsFile(fileObject);
                $('.newsfilecontextmenu1').hide();
                $('.newsfilecontextmenu1').css({
                    top: event.pageY + 'px',
                    left: event.pageX + 'px'
                }).show();
                event.preventDefault();
                return false;
            });
            $('body').click(function () {
                $('.newsfilecontextmenu1').hide();
            });
        }
    };
    ko.bindingHandlers.editableNewsStatus = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, context) {
            $(element).click(function (e) {
                var selectedObservableArrayInViewModel = valueAccessor();
                var fileObject = selectedObservableArrayInViewModel.selectedFile;
                context.$root.currentNewsFile(fileObject);
                $('.newsfilecontextmenu2').hide();
                $('.newsfilecontextmenu2').css({
                    top: event.pageY + 'px',
                    left: event.pageX + 'px'
                }).show();
                event.preventDefault();
                return false;
            });
            $('body').click(function () {
                $('.newsfilecontextmenu2').hide();
            });
        }
    };
    

    ko.bindingHandlers.onTextPaste = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {

        },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel) {
        $(element).keypress(function (e) {
            if (this.value.length > 700) {
                e.preventDefault();
                config.logger.clear();
                config.logger.error("Maximum Length Exceeded");
                $(this).val($(this).val().substring(0, 700));
            }
        });
    }
    };
    
    ko.bindingHandlers.newsFilecontextmenu = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, context) {
            $(element).on('contextmenu', function (event) {
                event.stopPropagation();
                var fileObject = ko.dataFor(event.target);
                $.grep(context.$root.appdata.foldersList(), function (e) {
                    if (e.FolderId == parseInt(fileObject.folderId))
                        e.isProgramVisible(false)
                    else
                        e.isProgramVisible(true);

                    return e;
                });

                //if (result.length == 0)
                //    {
                context.$root.currentNewsFile(fileObject);
                $('.newsfilecontextmenu').hide();
                //$('.newsfilecontextmenu').css({
                //    top: (event.pageY - 80)+ 'px',
                //    left: event.pageX + 'px'
                //}).show();
                
                //var mod = Math.abs(event.screenX - event.clientX);
                //var mod = window.outerWidth / window.innerWidth;
                //$('.newsfilecontextmenu').css({
                //    top: (event.pageY - 80) + 'px',
                //    left: (event.clientX  * mod) + 'px'
                //}).show();

                
                $('.newsfilecontextmenu').show();
                $('.newsfilecontextmenu').offset({ left: event.pageX, top: event.pageY });
               
                event.preventDefault();
                return false;
                //}
            });
            $('body').click(function () {
                $('.newsfilecontextmenu').hide();
            });
            $(".newsfilecontextmenu").mouseleave(function (ele) {
                $('.newsfilecontextmenu').hide();
            });
            
        }
    };

});