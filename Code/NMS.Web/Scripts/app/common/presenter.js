define('presenter',
    ['jquery'],
    function ($) {
        var
            transitionOptions = {
                ease: 'swing',
                fadeOut: 300,
                floatIn: 500,
                offsetLeft: '20px',
                offsetRight: '-20px',
                direction: { left: "left", right: "right" }

            },

            entranceThemeTransition = function ($view, route) {

                if (route === "#/home") {

                    $($view).fadeIn(transitionOptions.floatIn).addClass('view-active');
                }
                else {
                    $($view).show('slide', { direction: transitionOptions.direction.right, easing: transitionOptions.ease }, transitionOptions.floatIn).addClass('view-active');
                }
            },

            highlightActiveView = function (route, group) {
                // Reset top level nav links
                // Find all NAV links by CSS classname 
                var $group = $(group);
                if ($group) {
                    $(group + '.route-active').removeClass('route-active');
                    if (route) {
                        // Highlight the selected nav that matches the route
                        $group.has('a[href="' + route + '"]').addClass('route-active');
                    }
                }

            },

            resetViews = function (route) {
                if (route === "#/home") {
                    $('.view').fadeOut(transitionOptions.fadeOut).removeClass('view-active');
                }
                else {
                    $('.view').hide('slide', { direction: transitionOptions.direction.left, easing: transitionOptions.ease }, transitionOptions.fadeOut).removeClass('view-active');
                }

                if (route === "#/topicselection")
                    $('.view').hide();


            },

            toggleActivity = function (show) {
                //$('#busyindicator').activity(show);
                if (show)
                    $('.imgNewLoader').show();
                else $('.imgNewLoader').hide();
            },

            transitionTo = function ($view, route, group) {

                toggleActivity(true);

                resetViews(route);
                entranceThemeTransition($view, route);
                highlightActiveView(route, group);

                toggleActivity(false);
            },
            showAlert = function (show) {
                if (show)
                $('#newsAlert').addClass('active');
                else
                $('#newsAlert').removeClass('active');
            },
            showNotificaiton = function (show) {
                if (show)
                    $('#newsAlert1').addClass('active');
                else
                    $('#newsAlert1').removeClass('active');
            };


        return {
            toggleActivity: toggleActivity,
            transitionOptions: transitionOptions,
            transitionTo: transitionTo,
            showAlert: showAlert,
            showNotificaiton: showNotificaiton
        };
    });
