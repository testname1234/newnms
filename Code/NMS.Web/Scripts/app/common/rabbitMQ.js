﻿define('rabbitMQ',
    [],
    function () {
        var client, isConnected;

        rabbitobj = {
            ConstantKey: 'CONSTANTVALUE',
            MangerRoleId: 2077,
            EditorialRoleId: 2078,
            DefaultFilterId: 78,
            MenuFilterId: 13,
            RabbitMQServerAddress: 'http://10.3.12.118:15674',
            RabbitMQUserName: 'program',
            RabbitMQPassword: 'Axact123'
        };

        function connect() {
            var ws = new SockJS(rabbitobj.RabbitMQServerAddress + '/stomp');
            client = Stomp.over(ws);
            client.heartbeat.outgoing = 10000;
            client.heartbeat.incoming = 0;
            client.connect(rabbitobj.RabbitMQUserName, rabbitobj.RabbitMQPassword, onConnect, onError, '/');
        }

        function onConnect() {
            isConnected = true;
            console.log('RabitMQ Connected');
        }

        function onError(err) {
            isConnected = false;
            console.log('RabitMQ Error', err);
            setTimeout(function () { connect(); }, 5000);
        }
        function init() {
            connect();
        }

        function unsubscribe(path) {
            if (isConnected) {
                client.unsubscribe("/exchange/" + path);
            } else {
                setTimeout(function () {
                    unsubscribe(path);
                }, 2000);
            }
        }
        function subscribe(path, callback) {
            if (isConnected) {
                var subId = client.subscribe("/exchange/" + path, function (d) {
                    if (callback) {
                        callback(JSON.parse(d.body));
                    }
                });
            } else {
                setTimeout(function () {
                    subscribe(path, callback);
                }, 2000);
            }
        }
        init();

        return {
            subscribe: subscribe,
            unsubscribe: unsubscribe
        };
    });