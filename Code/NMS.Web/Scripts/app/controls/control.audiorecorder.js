﻿define('control.audiorecorder',
    ['moment', 'config', 'control.uploader', 'model.shortnews', 'manager'],
    function (moment, config, uploader, shortnews, manager) {

        var audioRecorder = function () {
            var self = this;
            self.recorder = ko.observable(),
            self.startTime = ko.observable(0),
            self.selectedNews = new shortnews(),
            self.audiouploader = ko.observable(new uploader()),
            self.stop = ko.observable(false),
            self.filename = ko.observable(''),
            self.audioobject = ko.observableArray([]),
            self.input,
            self.NewsId,
            self.UserId,
            self.CommentTypeId=2,
            self.Comments = ko.observable(''),

            self.counter = function () {
                if (self.stop()) {
                    return false;
                } else {
                    self.startTime(self.startTime() + 1);
                    setTimeout(function () {
                        if (self.startTime() >= 1200)
                        { self.initialize(); config.logger.warning('Maximum time availed');} else
                        {
                            self.counter();
                        }
                    }, 1000);
                }
            },

            self.recordTime = ko.computed({
                read: function () {
                    time=moment.utc(self.startTime()*1000).format("HH:mm:ss");
                    return time+" / 00:20:00";
                },
                
            }),

            self.sendtoserver = function (obj) {
                var dataa = obj()[0].object;
                dataa["name"] = "audiocomment.wav";
                var e = null;
                $('.commentupload').fileupload('add', {
                        files: dataa || [{ name: 'audiocomment.wav' }],
                        fileInput: $('.commentupload')
                    });
            },

            self.resetcontroller = function () {
                self.startTime(0);
                self.recorder.record = null;
                self.audioobject([]);
            },

            self.removeaudio = function (obj) {
                $.when(manager.news.deletevideofile(self.audioobject()[0].Id))
                       .done(function (responseData) {
                           self.resetcontroller();
                       })
                       .fail(function () {
                           self.resetcontroller();
                       })
                },
            
              self.createaudiofile = function () {
                  self.stop(true);
                  $('.cancellationBox .recorderstop').addClass("recorder");
                  $('.cancellationBox .recorder').removeClass("recorderstop");
                  self.recorder && self.recorder.exportWAV(function (blob) {
                       var url = URL.createObjectURL(blob);
                       var li = document.createElement('li');
                       var hf = document.createElement('a');

                       self.filename(hf.download);
                       self.audioobject.push({ Url: url, object: blob, name: 'audiocomment.wav', ResourceGuid:'' , Id: ''});
                       self.sendtoserver(self.audioobject);
                   });
              },

            self.initialize = function () {
                if (self.audioobject().length > 0) { config.logger.warning('Audio comment already present'); }
                else {
                        if (self.recorder.record) {
                            self.recorder && self.recorder.stop();
                            self.createaudiofile();
                            self.input.mediaStream.stop()
                            config.logger.success('Recorded');
                        }
                        else {
                            try {
                                // webkit shim
                                window.AudioContext = window.AudioContext || window.webkitAudioContext;
                                navigator.getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia || navigator.msGetUserMedia;
                                window.URL = window.URL || window.webkitURL || window.mozURL || window.msURL;

                                audio_context = new AudioContext;
                                console.log('navigator.getUserMedia ' + (navigator.getUserMedia ? 'available.' : 'not present!'));
                            } catch (e) {
                                alert('No web audio support in this browser!');
                            }
                            navigator.getUserMedia({ audio: true },
                                  function (stream) {
                                      self.input = audio_context.createMediaStreamSource(stream);
                                      self.input.connect(audio_context.destination);
                                      console.log('Input connected to audio context destination.');
                                      self.recorder = new Recorder(self.input);
                                      console.log('Recorder initialised.');
                                      self.recording(stream);
                                  }
                                , function (e) {
                                    alert('No live audio input: ' + e);
                                });
                        }
                    }
            },

              self.submitaudioComments = function (data) {
                  PostInput = {
                      NewsId: self.NewsId,
                      UserId: self.UserId,
                      CommentTypeId: self.CommentTypeId,
                      Comment: self.Comments,
                  };
                  if (self.audioobject().length > 0) {
                      PostInput["ResourceGuid"] = self.audioobject()[0].ResourceGuid;
                  }
                
                  if (PostInput.NewsId) {
                      if (PostInput.Comment().length > 0 || PostInput.ResourceGuid) {
                          $.when(manager.news.insertNewsComments(PostInput))
                              .done(function () {
                                  config.logger.success("Comments has been submitted.");
                                  self.Comments('');
                              })
                              .fail(function (ex) {
                                  config.logger.error("Failed to submit the comments, please try again later.");
                              });
                      } 
                      else{
                          config.logger.warning("Commentbox and Audio is empty");
                      }
                  }
                  else { config.logger.warning("No news found"); }
              },

              self.GuidSet = ko.computed({
                  read: function () {
                      
                      if (self.audiouploader().uploadedResources()[0]) {
                          if (self.audioobject().length > 0)
                          {
                              self.audioobject()[0].ResourceGuid = self.audiouploader().uploadedResources()[0].Guid;
                              self.audioobject()[0].Id = self.audiouploader().mappedResources()[0].id;
                              return self.audioobject()[0].ResourceGuid;
                          }
                          }
                  },

              }),

            self.recording = function (stream) {
                self.stop(false);
                $('.cancellationBox .recorder').addClass("recorderstop");
                $('.cancellationBox .recorderstop').removeClass("recorder");
                self.counter();
                self.recorder && self.recorder.record();
            };
            };

        return audioRecorder;
    });
