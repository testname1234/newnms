﻿define('control.calendar',
    ['moment', 'config', 'router'],
    function (moment, config, router) {

        var
            Calendar = function (mode) {
                var self = this;
               
                self.mode = mode;
                self.to = ko.observable(new Date()).extend({ throttle: 500 });

                self.toDate = ko.computed({
                    read: function () {
                        return moment(self.to()).format(config.DateFormat);
                    },
                    deferEvaluation: true
                }),

                self.from = ko.observable(new Date()).extend({ throttle: 500 });

                self.fromDate = ko.computed({
                    read: function () {
                        return moment(self.from()).format(config.DateFormat);
                    },
                    deferEvaluation: true
                }),

                self.range = ko.computed({
                    read: function () {

                        //var a = config;
                        //var b = router;
                        if (config.roleid == 7 && router.currentHash() == '#/home') {
                            if (self.fromDate() == self.toDate()) {
                                return self.fromDate();
                            }
                            else {
                                return self.fromDate();
                            }
                        }
                        else {
                            if (self.fromDate() == self.toDate()) {
                                return self.fromDate();
                            }
                            else {
                                return self.fromDate() + ' - ' + self.toDate();
                            }
                        }

                        



                    },
                    deferEvaluation: true
                });
                self.setCustom = function (item,element) {
                    item.clear();
                    if (self.mode == 'range') {
                        item.isSetCustom(true);
                    }
                },

                self.isSetToday = ko.observable(false);
                self.isSetYesterday = ko.observable(false);
                self.isSetLastWeek = ko.observable(false);
                self.isSetLastMonth = ko.observable(false);
                self.isSetCustom = ko.observable(false);

                self.setRange = ko.computed({
                    read: function () {
                        if (self.isSetToday()) {
                            self.from(new Date());
                            self.to(new Date());
                        }
                        else if (self.isSetYesterday()) {
                            self.from(new Date(moment(new Date()).add('days', -1)));
                            self.to(new Date(moment(new Date()).add('days', -1)));
                        }
                        else if (self.isSetLastWeek()) {
                            self.from(new Date(moment(new Date()).add('days', -7)));
                            self.to(new Date());
                        }
                        else if (self.isSetLastMonth()) {
                            self.from(new Date(moment(new Date()).add('months', -1)));
                            self.to(new Date());
                        }
                    },
                    deferEvaluation: false
                });

                self.applyDates = function () {

                }

                self.clear = function () {
                    self.isSetToday(false);
                    self.isSetYesterday(false);
                    self.isSetLastWeek(false);
                    self.isSetLastMonth(false);
                    self.isSetCustom(false);
                };

                self.isNullo = false;

                return self;
            }

        Calendar.prototype.setToday = function (item, element) {
            var date = new Date();
            item.to(date);
            item.from(date);
            item.clear();
            item.isSetToday(true);
        };

        Calendar.prototype.setYesterday = function (item, element) {
            var date = new Date();
            var to = date.setDate(date.getDate() - 1);
            var from = date;
            item.to(new Date(to));
            item.from(new Date(from));
            item.clear();
            item.isSetYesterday(true);

        };

        Calendar.prototype.setLastWeek = function (item, element) {
            var date1 = new Date();
            var date2 = new Date();
            var to = date1.setDate(date1.getDate() - 1);
            var from = date2.setDate(date2.getDate() - 7);
            item.to(new Date(to));
            item.from(new Date(from));
            item.clear();
            item.isSetLastWeek(true);
        };

        Calendar.prototype.setLastMonth = function (item, element) {
            var date1 = new Date();
            var date2 = new Date();
            var to = date1.setDate(date1.getDate() - 1);
            var from = date2.setMonth(date2.getMonth() - 1);
            item.to(new Date(to));
            item.from(new Date(from));
            item.clear();
            item.isSetLastMonth(true);
        };

        Calendar.Nullo = new Calendar();

        Calendar.Nullo.isNullo = true;

        return Calendar;
    });