﻿define('ControllerScript',
    ['config', 'manager', 'ko', 'appdata'],
    function (config, manager, ko, appdata) {

        var condition = 0,
         starter = 0,
         imagelocation,
         points = [],
         pointDistacne = config.CutterpresentScreen('pointDiffer'),
         canvasId = config.CutterpresentScreen('canvasName'),
         canvas = document.getElementById("myCanvas"),
         cornercoords = [],
         lassoButton = config.CutterpresentScreen('canvasButton'),
         secondcornercoords = [],
         imageObj = new Image();


        cuttingSCreen = config.CutterpresentScreen('Screen'),
        ChannelId = '',
        backupPoints = ko.observableArray();
        sendpoints = ko.observableArray();

        Getimage = function (imgsrc) {
            appdata.cropWidthFactor = 0;
            appdata.cropHeightFactor = 0;
            if (imgsrc == "../../Content/images/producer/pleaseWait3.png") {
                $("#myCanvas").css({
                    'margin-top': '0px'
                });
            }
            if (imgsrc != "../../Content/images/producer/pleaseWait3.png") {
                $("#myCanvas").css({
                    'margin-top': '0px'
                });
            }
            canvasId = config.CutterpresentScreen('canvasName'),
            canvas = document.getElementById("myCanvas"),

            ctx = canvas.getContext('2d');

            imagelocation = imgsrc;
            imageObj = new Image();
            imageObj.src = imagelocation;
            //imageObj.height = '380';
            //imageObj.width = '730';
            ctx.globalAlpha = 1;

            imageObj.onload = function () {
                ctx.clearRect(0, 0, canvas.width, canvas.height);
                var w = imageObj.width;
                var h = imageObj.height;
                var heightFactor = 0;
                var widthFactor = 0;
                if (w > canvas.width) {
                    widthFactor = w / canvas.width;
                }
                if (h > canvas.height) {
                    heightFactor = h / 420;
                }
                if (widthFactor != 0 && heightFactor != 0) {
                    w = canvas.width;
                    h = canvas.height;
                    imageObj.height = h;
                    imageObj.width = w;

                    appdata.cropWidthFactor = widthFactor;
                    appdata.cropHeightFactor = heightFactor;
                }
                ctx.drawImage(imageObj, 0, 0, w, h);
            };
        },

            cropping = function () {
                if (condition >= 1) {
                    $('#lasso' + condition).removeClass('active');
                    condition = 0;
                    creatingPoints();
                }
                ctx.clearRect(0, 0, canvas.width, canvas.height);
                ctx.beginPath();
                var w = imageObj.width;
                var h = imageObj.height;
                ctx.width = w;
                ctx.height = h;
                ctx.globalCompositeOperation = 'destination-over';
                var offset = $('#myCanvas').offset();

                for (var i = 0; i < backupPoints().length; i++) {

                    for (var j = 0; j < backupPoints()[i].length; j += 2) {
                        var x = parseInt(jQuery.trim(backupPoints()[i][j]));
                        var y = parseInt(jQuery.trim(backupPoints()[i][j + 1]));

                        if (j == 0) {
                            ctx.moveTo(x - offset.left, y - offset.top);

                        } else {
                            ctx.lineTo(x - offset.left, y - offset.top);
                        }
                    }
                }
                if (this.isOldIE) {
                    ctx.fillStyle = '';
                    ctx.fill();
                    var fill = $('fill', myCanvas).get(0);
                    fill.color = '';
                    fill.src = element.src;
                    fill.type = 'tile';
                    fill.alignShape = false;
                }
                else {
                    try {
                        var pattern = ctx.createPattern(imageObj, "no-repeat");
                        ctx.fillStyle = pattern;
                        ctx.fill();
                    }
                    catch (err) {
                        config.logger.error('Image is Broken!');
                    }

                }
                //draw the polygon
                setTimeout(function () {
                    ctx.globalCompositeOperation = 'source-over';
                    ctx.globalAlpha = 0.3;
                    var w = imageObj.width;
                    var h = imageObj.height;
                    var heightFactor = 0;
                    var widthFactor = 0;
                    if (w > canvas.width) {
                        widthFactor = w / canvas.width;
                    }
                    if (h > canvas.height) {
                        heightFactor = h / 420;
                    }
                    if (widthFactor != 0 && heightFactor != 0) {
                        w = canvas.width;
                        h = canvas.height;
                        imageObj.height = h;
                        imageObj.width = w;

                        appdata.cropWidthFactor = widthFactor;
                        appdata.cropHeightFactor = heightFactor;
                    }
                    ctx.drawImage(imageObj, 0, 0, w, h);
                }, 200);
            },

            resetCanvas = function () {
                //ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height);
                condition = 0;
                var n = sendpoints().length;
                starter = 0;
                points = [];
                sendpoints([]);
                backupPoints([]);

                $('.spot').remove();
                $("#lasso1").removeClass('active');
                $("#lasso2").removeClass('active');
                $("#lasso3").removeClass('active');
                //ctx.setTransform(1, 0, 0, 1, 0, 0);
                for (var a = 0; a <= n - 1; a++) {
                    var box = '.square' + a + 'select';
                    $(box).remove();
                }


            };
        lasso1 = function () {
            if (condition != 4) {
                if (condition == 1) {
                    $('#lasso1').removeClass('active');
                    condition = 0;
                    if (typeof (points[0]) != 'undefined')
                        creatingPoints();
                }
                else {
                    $('#lasso' + condition).removeClass('active');
                    starter = 0;
                    condition = 1;
                    $('#lasso1').addClass('active');
                }
            }
        },
        lasso2 = function () {
            if (condition != 4) {
                if (condition == 2) {
                    $('#lasso2').removeClass('active');
                    condition = 0;
                    if (typeof (points[0]) != 'undefined')
                        creatingPoints();
                }
                else {
                    $('#lasso' + condition).removeClass('active');
                    starter = 0;
                    condition = 2;
                    $('#lasso' + '2').addClass('active');
                }
            }
        },

        lasso3 = function () {
            if (condition == 3) {
                $('#lasso' + '3').removeClass('active');
                condition = 0;
                if (typeof (points[0]) != 'undefined')
                    rectangleselector(backupPoints().length);
            }
            else {
                $('#lasso' + condition).removeClass('active');
                starter = 0;
                condition = 3;
                $('#lasso' + '3').addClass('active');
            }
        },
  setTimeout(function () {
      $(cuttingSCreen).ready(function () {

          this.isOldIE = (window.G_vmlCanvasManager);

          function init() {
              canvas.addEventListener('mousedown', mouseDown, false);
              canvas.addEventListener('mousemove', mouseMove, false);
          }

          $(document).mouseup(function () {
              if (starter == 1) {
                  starter = 0;
                  if (points.length == 2 && condition == 3) {
                      if ($('.squareselect').height() == 10 && $('.squareselect').width() == 10) {
                          $('.squareselect').remove();
                          points = [];
                      }
                  }
                  if (condition == 3) {
                      $("#lasso3").click();
                  }
              }
          });



          rectangleselector = function (n) {
              //condition = 4;
              var height = $('.square' + n + 'select').height();
              var width = $('.square' + n + 'select').width();
              var px = points[0];
              var py = points[1];
              points.push(px + width, py, px + width, py + height, px, py + height);
              //cropping();
              creatingPoints();
          };

          setTimeout(function () {
              $('#myCanvas').mousemove(function (e) {
                  if (condition == 1 || condition == 2 || condition == 3) {
                      ctx.beginPath();
                      $('#posx').html(e.offsetX);
                      $('#posy').html(e.offsetY);

                      if (starter == 1 && condition == 3) {
                          var n = backupPoints().length;
                          var top = $('.square' + n + 'select').position().top - canvas.offsetTop;
                          var left = $('.square' + n + 'select').position().left - canvas.offsetLeft;
                          $('.square' + n + 'select').width(e.offsetX - left - 10);
                          $('.square' + n + 'select').height(e.offsetY - top - 10);
                      }

                      if (starter == 1 && condition == 1) {
                          making(e);
                      }
                  }
              });
          }, 500);
          setTimeout(function () {
              $('#myCanvas').mousedown(function (e) {
                  if (condition == 1 || condition == 2) {
                      starter = 1;
                      making(e);
                  }
                  else if (condition == 3) {
                      starter = 1;
                      var offset = $('#' + canvasId).offset();
                      var n = backupPoints().length;
                      //var squareX = offset.left - e.offsetX;
                      //var squareY = offset.top - e.offsetY;
                      var squareX = e.offsetX + canvas.offsetLeft;
                      var squareY = e.offsetY + canvas.offsetTop;
                      $('#' + canvasId).parent().append('<div class="square' + n + 'select" style="position: absolute;border-style: dashed;border-color: #ff0000; font-size: 100px; left: ' + squareX + 'px; top: ' + squareY + 'px; width: 10px; height: 10px;"></div>');
                      points.push(e.pageX, e.pageY);
                  }
                  else {
                      config.logger.error('Please Select Tool First');
                  }
              });
          }, 500);
          making = function (e) {
              if (e.which == 1) {
                  if (condition == 2) {
                      /// second lasso functionality pointers/////
                      var pointer = $('<span class="spot">').css({
                          'position': 'absolute',
                          'background-color': '#ff0000',
                          'width': '5px',
                          'height': '5px',
                          'top': e.pageY - pointDistacne + 60,
                          'left': e.pageX - pointDistacne + 10
                      });
                      //////////////////////////////////////////////
                  } else {
                      var pointer = $('<span class="spot">').css({
                          'position': 'absolute',
                          'background-color': '#ff0000',
                          'width': '2px',
                          'height': '2px',
                          'top': e.pageY - pointDistacne + 60,
                          'left': e.pageX - pointDistacne + 10
                      });
                  }
                  $('#' + canvasId).css('cursor', 'crosshair');
                  //store the points on mousedown
                  points.push(e.pageX, e.pageY);
                  ctx.globalCompositeOperation = 'destination-out';
                  if (points.length >= 3 && condition == 2) {
                      var offset = $('#' + canvasId).offset();
                      var oldposx = points[points.length - 4];
                      var oldposy = points[points.length - 3];
                      var posx = points[points.length - 2];
                      var posy = points[points.length - 1];

                      ctx.beginPath();
                      ctx.moveTo(oldposx - offset.left, oldposy - offset.top);
                      if (oldposx != '') {
                          ctx.lineTo(posx - offset.left, posy - offset.top);
                          ctx.lineWidth = 5;
                          ctx.strokeStyle = 'red';
                          ctx.stroke();
                      }
                  }
              }
              //$(document.body).append(pointer);
              $(cuttingSCreen).append(pointer);

              //'#cropimage-view'
          };

          creatingPoints = function () {
              var offset = $('#' + canvasId).offset();
              var points123 = [];
              for (var i = 0; i < points.length; i += 2) {
                  var x = parseInt(jQuery.trim(points[i]));
                  var y = parseInt(jQuery.trim(points[i + 1]));
                  if (appdata.cropWidthFactor == 0) {
                      appdata.cropWidthFactor = 1;
                  }
                  if (appdata.cropHeightFactor == 0) {
                      appdata.cropHeightFactor = 1;
                  }
                  points123.push({ x: parseInt((x - offset.left) * appdata.cropWidthFactor), y: parseInt((y - offset.top) * appdata.cropHeightFactor) });
              }
              sendpoints().push(points123);

              backupPoints.push(points);
              points = [];
              cropping();
          };



      });
  }, 500);
        return {
            imagelocation: imagelocation,
            Getimage: Getimage,
            cropping: cropping,
            canvasId: canvasId,
            sendpoints: sendpoints,
            resetCanvas: resetCanvas,
            ChannelId: ChannelId,
            lasso3: lasso3,
            lasso1: lasso1,
            lasso2: lasso2
        };

    });