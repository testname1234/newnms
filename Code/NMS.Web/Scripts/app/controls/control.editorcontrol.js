﻿define('control.editorcontrol',
    [],
    function () {

        var

         Editor = function (lang,content) {

             var self = this;
             self.selectedLanguage = lang;
             self.content = ko.observable(content);
             self.isContentSet = ko.observable(false);
             
         };

        return Editor;
    });