﻿define('control.language',
    [],
    function () {

        var Language = function (languageCode,selector) {
            var self = this;

            self.langCode = ko.observable(languageCode),
            self.urduElement=ko.observable(),
            self.englishElement = ko.observable(),

            self.langName = ko.computed({
                read: function () {
                    var name = 'Unknown';
                    if(langCode.toLowerCase()=='en'){
                        name = 'English';
                    }
                    else if (langCode.toLowerCase() == 'ur') {
                        name = 'Urdu';
                    }

                },
                deferEvaluation: true
            }),

            self.selector = ko.observableArray(selector);
        };

        return Language;
    });
