﻿define('control.listheader',
    ['control.calendar'],
    function (Calendar) {

        var ListHeader = function (calendarMode) {
            var self = this;

            self.heading=ko.observable(''),
            self.onHeadingClicked = function () { },
            self.count = ko.observable(0),
            self.searchText = ko.observable('').extend({ throttle: 500 }),
            self.calendar = new Calendar(calendarMode);
            self.isVisible = ko.observable(true),
            self.isNullo = false;

            self.searchText.subscribe(function (newValue) {
                setTimeout(function () {
                    self.searchText(self.searchText());
                }, 200);
            });
        };

        ListHeader.Nullo = new ListHeader();

        ListHeader.Nullo.isNullo = true;

        return ListHeader;
});
