﻿define('control.mcrusers',
    [
      'moment',
      'datacontext',
      'model',
      'enum',
      'config'
    ],
    function (moment, dc, model, e,config) {

        var Mcrusers = function () {
            var self = this;
            //properties
            self.episode = ko.observable(),
            self.programName = ko.observable(),
            self.stories = ko.observableArray([]),
            self.currentStoryIndex = ko.observable(0),
            self.currentScreenTemplate = ko.observable(),
            self.nextScreenTemplate = ko.observable(),
            self.totalTemplates = ko.observable(),
            self.progressBar = ko.observable(0),
            self.totalDuration = ko.observable(0),
            self.servedIndexes = [],
            self.displayDuration = ko.observable(0),
            self.tempList = [],
            self.percent = 0,
            self.prevDurations = 0,
            self.overallscript = ko.observableArray([]),
            self.starttime = ko.observable();
            self.endtime = ko.observable();
            self.alltemplates = ko.observableArray([]),
            self.screenTemplates = ko.observableArray([]),
            self.MosId=ko.observable(),
            self.port = ko.observable(),
            

            //new
            self.currentStory = ko.observable(new model.Story);

            //computed properties
            self.extractStories = ko.computed({
                read: function () {
                    var cEpisode = self.episode();
                    if (cEpisode) {
                        if (cEpisode.programElements && cEpisode.programElements().length > 0) {
                            var stories = [];
                            for (var i = 0 ; i < cEpisode.programElements().length; i++) {

                                if (cEpisode.programElements()[i].stories().length > 0) {

                                    for (var j = 0 ; j < cEpisode.programElements()[i].stories().length; j++) {
                                        if (cEpisode.programElements()[i].stories()[j].screenFlow().screenTemplates().length > 0) {
                                            stories.push(cEpisode.programElements()[i].stories()[j]);
                                        }
                                    }
                                }
                            }
                            stories.sort(function (a, b) { return a.sequenceId - b.sequenceId });
                            for (var i = 0; i < stories.length; i++) {
                                stories[i]['serialNo'] = i + 1;
                            }
                            self.stories(stories);
                            self.overAllduration();
                        }
                    }
                    return [];
                },
                deferevaluation: false
            }),

            self.setCurrentStory = function (sId) {
                var data = dc.stories.getLocalById(sId);
                if (data) {
                    self.currentStory(data);
                    self.totalTemplates(data.screenFlow().screenTemplates());
                    for (var i = 0; i < self.totalTemplates().length; i++) {
                        self.totalTemplates()[i]['tempNo'] = i + 1;
                    }
                    if (self.currentStory) {
                        for (var i = 0; i < self.currentStory().screenFlow().screenTemplates().length; i++) {
                            self.setCurrentScreenTemplate(self.currentStory().screenFlow().screenTemplates()[i]);
                        }
                    }
                    self.setDisplayTemplateInitially();

                } else { alert("No story found 4040"); }
            },

            self.setDisplayTemplateInitially = function () {
                self.currentScreenTemplate(self.totalTemplates()[0]);
                self.nextScreenTemplate(self.totalTemplates()[0]);//self.totalTemplates()[1]);
            },

            self.setCurrentScreenTemplate = function (data) {
                var prevDurations = 0;
                for (var i = 0; i < self.screenTemplates().length; i++) {
                    prevDurations += self.screenTemplates()[i].contentDuration();
                    if (self.screenTemplates()[i].id == data.id
                        && self.screenTemplates()[i].storyScreenTemplateId == data.storyScreenTemplateId) {
                        break;
                    }
                }
                self.percent = (prevDurations * 100) / self.totalDuration();
                self.displayDuration(moment.utc(prevDurations * 1000).format("HH:mm:ss"));
                self.progressBar(self.percent);

                //var tempList = self.totalTemplates();
                //if (data) {
                // var index = tempList.indexOf(data);
                //self.currentScreenTemplate(data);
                //if (tempList[index + 1]) {
                //    self.nextScreenTemplate(tempList[index + 1]);
                //}
                //}
            },
            self.autoforward = function () {
                self.tempList = [];
                self.done();
            };
            self.done = function () {
                if (self.stories()[self.currentStoryIndex() + 1]) {
                    self.currentStoryIndex(self.currentStoryIndex() + 1);
                } else {
                    return false;
                }
            };
            self.overAllduration = function () {
                var total = 0;
                var arr = [];
                var scripts = [];
                var alltemplatess = [];
                for (var i = 0; i < self.stories().length; i++) {
                    for (var j = 0; j < self.stories()[i].screenFlow().screenTemplates().length; j++) {
                        total += self.stories()[i].screenFlow().screenTemplates()[j].contentDuration();
                        scripts.push(self.stories()[i].screenFlow().screenTemplates()[j].script());
                        arr.push(self.stories()[i].screenFlow().screenTemplates()[j]);

                    }

                }
                self.totalDuration(total);
                self.screenTemplates(arr);
                self.overallscript(scripts);


                for (var i = 0; i < arr.length; i++) {
                    var isOn = 0;
                    var isOff = 0;
                    arr[i]["isOffmics"] = 0;
                    arr[i]["isOnmics"] = 0;
                    for (var j = 0; j < arr[i].mics().length; j++) {
                        if (arr[i].mics()[j].isOn == true) {
                            isOn = isOn + 1;
                            arr[i].isOnmics = isOn;
                        }
                        else {
                            isOff = isOff + 1;
                            arr[i].isOffmics = isOff;
                        }
                    }
                }

                self.alltemplates(arr);
                self.starttime(moment.utc(self.episode().startTime()).format("HH:mm:ss"));
                self.endtime(moment.utc(self.episode().endTime()).format("HH:mm:ss"));


            }

            self.checkNextTemplate = function (data) {
                if (data) {
                    if (self.nextScreenTemplate()) {
                        self.currentScreenTemplate(self.nextScreenTemplate());
                    }
                    self.nextScreenTemplate(data);
                }
            }
            self.activate = function (commandType) {

            };


            //cummunicating  to wpf
            self.saveSettings = function (a,b) {
                
                  if (self.MosId && self.port && a && b) {
                      self.MosId(a);
                      self.port(b);
                      self.sendCommand(e.CommandType.Notify_PcrUser);
                    }
                
            },

            self.sendCommand = function (commandType) {
                var cType = commandType;
                var obj = { MosId: self.MosId(), MosPort: self.port() };
                app.SendCommand(e.CommandSenderType.PcrUser, cType, JSON.stringify(obj));
            };

        };

        return Mcrusers;
    });