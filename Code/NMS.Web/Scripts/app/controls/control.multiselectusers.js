﻿define('control.multiselectusers',
    [],
    function () {

        var MultiSelect = function () {

            var self = this;
            self.option = { name: '', id: '', designation: '', img: '', isSelected: ko.observable(false) },
            self.items = ko.observableArray(),
            self.filteredItems = ko.observableArray([]),
            self.selectedItems = ko.observableArray([]),
            self.currentItem = ko.observable();
            self.classes = 'options',
            self.searchText = ko.observable(),

            self.selectUser = function (item) {
                item.isSelected(!item.isSelected());
                if (item.isSelected()) {
                    self.selectedItems.push(item);
                }
                
                else {
                    var index = self.selectedItems.indexOf(item);
                    self.selectedItems.remove(item);
                    self.selectedItems.slice(index, 1);
                    self.selectedItems.valueHasMutated();
                }
            },

            self.searchUser = function () {
                
                if (event.keyCode == 40) {   //   down
                    if (self.currentItem())
                    {
                        var index = self.filteredItems().indexOf(self.currentItem());
                        self.currentItem(self.filteredItems()[index + 1]);
                    }
                    else
                        self.currentItem(self.filteredItems()[0]);
                }
                else if (event.keyCode == 38) {   //  up 
                    if (self.currentItem())
                    {
                        var index = self.filteredItems().indexOf(self.currentItem());
                        self.currentItem(self.filteredItems()[index - 1]);
                    }
                    else
                        self.currentItem(self.filteredItems()[self.filteredItems().length - 1]);
                }
                else if (event.keyCode == 13) {   //  enter
                    if (self.currentItem())
                        self.selectUser(self.currentItem());
                }
                else if (event.keyCode == 27) {   //  esc
                    self.currentItem(null);
                }

                if (!self.searchText()) {
                    self.filteredItems(self.items());
                } else {
                    var arr = [];
                    for (i = 0; i < self.items().length; i++) {
                        var item = self.items()[i];
                        if (item.name && item.name.toLowerCase().indexOf(self.searchText().toLowerCase()) > -1) {
                            arr.push(item);
                        }
                    }
                    self.filteredItems(arr);
                }
            },

            self.removeUser = function (item,event) {
                item.isSelected(false);
                self.selectedItems.remove(item);
            }


        };

        return MultiSelect;
    });