﻿define('control.recorder',
    ['moment', 'config', 'control.uploader'],
    function (moment, config, uploader) {

        var audioRecorder = function () {
            var self = this;
            self.recorder = ko.observable(),
            self.startTime = ko.observable(0);
            self.uploader = ko.observable(new uploader()),
            self.stop = ko.observable(false),
            self.filename = ko.observable(''),
            self.localUrl = ko.observable(''),

            self.counter = function () {
                if (self.stop()) {
                    return false;
                } else {
                    self.startTime(self.startTime() + 1);
                    setTimeout(function () {
                        self.counter();
                    }, 1000);
                }
            },

            self.recordTime = ko.computed({
                read: function () {
                    time=moment.utc(self.startTime()*1000).format("HH:mm:ss");
                    return time+" / 00:20:00";
                },
                
            }),

            self.sendWaveToPost = function (blob) {
                self.uploader().src(blob);
                var file2 = new FileReader();
                //file2.onloadend = function (e) {
                //    $.ajax({
                //        url: self.postUrl,
                //        type: "POST",
                //        data: file2.result,
                //        processData: false,
                //        contentType: "text/plain"
                //    });
                //};
                //file2.readAsDataURL(blob);
            },

              self.createaudiofile = function (element, flag) {
                  self.stop(true);
                  self.recorder && self.recorder.exportWAV(function (blob) {
                       var url = URL.createObjectURL(blob);
                       var li = document.createElement('li');
                       var hf = document.createElement('a');
                 
                       hf.href = url;
                       hf.download = new Date().toISOString() + '.wav';
                       hf.innerHTML = hf.download;
                       li.appendChild(hf);
                       element.appendChild(li);

                       if (flag==true)
                       {
                           var au = document.createElement('audio');
                           au.controls = true;
                           au.src = url;
                           li.appendChild(au);
                       }
                       self.filename(hf.download);
                       self.localUrl(url);
                       self.sendWaveToPost(url);
                   });
              },


            self.initialize = function () {
                    try {
                        // webkit shim
                        window.AudioContext = window.AudioContext || window.webkitAudioContext;
                        navigator.getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia;
                        window.URL = window.URL || window.webkitURL;

                        audio_context = new AudioContext;
                        console.log('navigator.getUserMedia ' + (navigator.getUserMedia ? 'available.' : 'not present!'));
                    } catch (e) {
                        alert('No web audio support in this browser!');
                    }
                    navigator.getUserMedia({ audio: true },
                          function (stream) {
                              var input = audio_context.createMediaStreamSource(stream);
                              input.connect(audio_context.destination);
                              console.log('Input connected to audio context destination.');
                              self.recorder = new Recorder(input);
                              console.log('Recorder initialised.');
                              self.recording(stream);
                          }
                        , function (e) {
                            alert('No live audio input: ' + e);
                        });
                
            },

            self.recording = function (stream) {
                self.stop(false);
                self.counter();
                self.recorder && self.recorder.record();
            };

            };

        return audioRecorder;
    });
