﻿define('control.runorder',
    ['ko', 'underscore'],
    function (ko, _) {
        var RunOrder = function () {
            var self = this;
             self.Slug = ko.observable(),
             self.Stories = ko.observableArray([])
        };

        return RunOrder;
    });