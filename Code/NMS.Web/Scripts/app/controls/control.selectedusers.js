﻿define('control.selectedusers',
    [],
    function () {

        var SelectedUser = function () {

            var self = this;
            self.selectedItems = ko.observableArray(),
            self.classes = 'options1'
            
            self.removeUser = function (item) {
                item.isSelected(false);
            }

        };

        return SelectedUser;
    });