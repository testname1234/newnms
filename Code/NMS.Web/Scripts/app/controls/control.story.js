﻿define('control.story',
    ['ko', 'underscore'],
    function (ko, _) {

        var Story = function () {
            var self = this;

            self.Slug=ko.observable(),
            self.StoryItems = ko.observableArray([])

        };

        return Story;
    });