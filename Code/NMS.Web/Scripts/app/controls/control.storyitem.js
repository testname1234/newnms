﻿define('control.storyitem',
    ['ko', 'underscore'],
    function (ko, _) {

        var StoryItem = function () {
            var self = this;
            self.Slug=ko.observable(),
            self.Instructions=ko.observable(),
            self.Detail=ko.observable()
        };

        return StoryItem;
    });