﻿define('control.tag',
    [],
    function () {

        var Tag = function (src) {
            var self = this;

            self.tags = ko.observableArray([]),
            self.source = src,
            self.term = ko.observable('').extend({throttle:200}),
            self.removeTag = function (item, event) {
                var index = self.tags().indexOf(item);
                if (index > -1) {

                    self.tags.splice(index, 1);
                }
            };
        };

        return Tag;
    });
