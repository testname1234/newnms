﻿ define('control.templatepaging',
    [],
    function () {

        var TempPaging = function (flag) {
    	    var self = this;
            self.includeAllnextAllPrevious = flag,
    	    self.list = ko.observableArray([]),
    	    self.currentItem = ko.observable(''),
    	    self.currentIndex = ko.observable(-1),
		    self.nextItem = function () {
		        self.changeRecord(1);
		    },

			self.previousItem = function () {
			    self.changeRecord(0);
			},

		    self.changeRecord = function (data) {
		        var tempList = self.list();
		        if (data == 1) {
		            self.currentIndex(self.currentIndex() + 1);
		            var item = tempList[self.currentIndex()];
		            self.currentItem(item);
		            self.loadFromServer(item.id);
		        }
		        if (data == 0) {
		            self.currentIndex(self.currentIndex() - 1);
		            var item = tempList[self.currentIndex()];
		            self.currentItem(item);
		            self.loadFromServer(item.id);
		        }
		    };
				
    	    if (self.includeAllnextAllPrevious) {
    	              self.allPreviousItems = ko.computed({
    	                    read: function () {
    	                        var index = self.currentIndex();
    	                        var tempList = self.list();
    	                        if (tempList && tempList.length > 0) {
    	                            return tempList.slice(0, index);
    	                        }
    	                        return [];

    	                    },
    	                    deferEvaluation: true
    	                }),

                       self.allNextItems = ko.computed({
                           read: function () {
                                var index = self.currentIndex();
                                var tempList = self.list();
                                if (tempList && tempList.length > 0) {
                                    return tempList.slice(index+1);
                                }
                                return [];
                            },
                            deferEvaluation: true
                       });

    	              self.setThisCurrent = function (data) {
    	                  var index = self.list().indexOf(data);
    	                  self.currentIndex(index);
                          self.currentItem(data)
    	              }
    	        }
		    self.loadFromServer = function () {

		    };

    	};

    	    return TempPaging;
    });
