﻿define('control.tool',
    [],
    function () {

        var Tool = function (Id,Name) {
            var self = this;

            self.id=Id,
            self.name=Name,
            self.isSelected=ko.observable(false);
        };

        return Tool;
    });
