﻿define('control.uploader',
    ['config', 'datacontext', 'model.news', 'model.mapper'],
    function (config, dc, News, mapper) {
        var
         Uploader = function () {
             var self = this;
             self.postUrl = config.ResourceUploadUrl,
             self.options = {
                 url: self.postUrl,
                 autoUpload: true
             };
             self.src = ko.observable(''),
             self.tokenReceived = ko.observable(false),
             self.started = ko.observable(false),
             self.finished = ko.observable(false),
             self.uploadProgress = ko.observable('0%'),
             self.resources = ko.observableArray([]),

             self.uploadedResources = ko.observableArray([]),

             self.deletevideofile = function () {

             };
             self.filesUploadedCount = ko.observable(0);
             self.mappedResources = ko.computed({
                 read: function () {
                     self.uploadedResources(_.uniq(self.uploadedResources()));
                     self.resources(_.uniq(self.resources()));
                     var list = self.uploadedResources();
                     var arr = [];
                     for (var i = 0; i < list.length; i++) {
                             arr.push(mapper.resource.fromDto(list[i]));
                     }


                     return arr;
                 },
                 deferEvaluation: true
             });
         };
       
        return Uploader;
    });