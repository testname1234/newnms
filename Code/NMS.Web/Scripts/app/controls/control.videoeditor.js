﻿define('control.videoeditor',
   ['moment'],
   function (moment) {

       var VideoEditor = function () {
           var self = this;

           self.src = ko.observable(''),
           self.isProcessed = ko.observable(false),
           self.backgroundUrl = ko.observable('/Content/images/radioReporter/audio.png'),
           self.factor = ko.observable(''),
           self.videoCurrentTime = ko.observable(''),
           self.videoDuration = ko.observable(''),
           self.videoControlElement = ko.observable(''),
           self.totalSelections = ko.observableArray([]),
           self.startBuffering = ko.observable(false),
           self.bufferValue = ko.observable(''),
           self.editedVideoLength = ko.computed({
               read: function () {
                   var selections = self.totalSelections();
                   var selectionLength = selections.length;
                   var arrFrom = [];
                   var arrTo = [];
                   var totalTimeSpan = 0;
                   //extract array::
                   for (var i = 0; i < selectionLength; i++) {
                       arrFrom[i] = selections[i].From;
                       arrTo[i] = selections[i].To;
                   }
                   //subtract array::
                   for (var i = 0; i < arrFrom.length; i++) {
                       totalTimeSpan += arrTo[i] - arrFrom[i];
                   }
                   var result = moment.utc(totalTimeSpan * 1000).format("HH:mm:ss");
                   return result;
               },
               deferEvaluation: true
           }),

           self.duration = ko.computed({
               read: function () {
                   return moment.utc(self.videoDuration() * 1000).format("HH:mm:ss");
               },
               deferEvaluation: true
           }),
           self.currentTime = ko.computed({
               read: function () {
                   return moment.utc(self.videoCurrentTime() * 1000).format("HH:mm:ss");
               },
               deferEvaluation: true
           }),


           self.addEvents = ko.computed({
               read: function () {
                   var video = self.videoControlElement();
                   video.onplay = function (a, b) {
                       self.backgroundUrl('/Content/images/radioReporter/audio.gif');
                   };
                   video.onpause = function () {
                       self.backgroundUrl('/Content/images/radioReporter/audio.png');
                   };
               },
               deferEvaluation: false
           }),

           self.play = function () {
               var video = self.videoControlElement();
               if (video.paused == true) {
                   video.play();
               } else {
                   video.pause();
               }
           },
           self.mute = function () {
               
               var video = self.videoControlElement();
               if (video.muted == false) {
                   video.muted = true;
               } else {
                   video.muted = false;
               }

           },
           self.forward = function () {
               
               var video = self.videoControlElement();
               var fps = (2 * video.duration / 100);
               video.currentTime += fps;

           },
           self.backward = function () {
               
               var video = self.videoControlElement();
               var fps = (2 * video.duration / 100);
               video.currentTime -= fps;
           },

           self.stop = function () {
               var video = self.videoControlElement();
               if (video.paused !== true) {
                    video.pause();
               }
               video.currentTime = 0;
           };

       };

       return VideoEditor;
   });


