﻿define('manager.categories',
    [
        'dataservice',
        'datacontext'
    ],
    function (dataservice, dc) {
        var
            loadCategories = function (requestData) {
                return $.Deferred(function (d) {
                    $.when(dataservice.loadCategories(requestData))
                     .done(function (responseData) {
                         if (responseData.Data) {
                             dc.newsCategories.fillData(responseData.Data);
                         }
                         d.resolve(responseData);
                     })
                     .fail(function (responseData) {
                         d.reject(responseData);
                     });
                }).promise();
            };
        return {
            loadCategories: loadCategories
        }
    });