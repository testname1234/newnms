﻿define('manager.channel',
    [
        'jquery',
        'dataservice',
        'datacontext',
        'underscore'
    ],
    function ($, dataservice, dc, _) {
        var
            channelReporterloadInitialData = function () {
                var data = {};
                data.id = moment(new Date()).format('YYYY-MM-DD');
                return $.Deferred(function (d) {
                    $.when(dataservice.channelReporterLoadInitialData(data))
                        .done(function (responseData) {
                            if (responseData.IsSuccess) {

                                var channels = responseData.Data.Channels;
                                var channelVideos = responseData.Data.ChannelVideos;
                                var programs = responseData.Data.Programs;
                                var hourlySlots = [
                                    { id: '00:00', name: '01:00' },
                                    { id: '01:00', name: '02:00' },
                                    { id: '02:00', name: '03:00' },
                                    { id: '03:00', name: '04:00' },
                                    { id: '04:00', name: '05:00' },
                                    { id: '05:00', name: '06:00' },
                                    { id: '06:00', name: '07:00' },
                                    { id: '07:00', name: '08:00' },
                                    { id: '08:00', name: '09:00' },
                                    { id: '09:00', name: '10:00' },
                                    { id: '10:00', name: '11:00' },
                                    { id: '11:00', name: '12:00' },
                                    { id: '12:00', name: '13:00' },
                                    { id: '13:00', name: '14:00' },
                                    { id: '14:00', name: '15:00' },
                                    { id: '15:00', name: '16:00' },
                                    { id: '16:00', name: '17:00' },
                                    { id: '17:00', name: '18:00' },
                                    { id: '18:00', name: '19:00' },
                                    { id: '19:00', name: '20:00' },
                                    { id: '20:00', name: '21:00' },
                                    { id: '21:00', name: '22:00' },
                                    { id: '22:00', name: '23:00' },
                                    { id: '23:00', name: '00:00' }
                                ];

                                if (channels && channels.length > 0) {
                                    dc.channels.fillData(channels);
                                }

                                if (programs && programs.length > 0) {
                                    dc.programs.fillData(programs, { groupByChannelId: true });
                                }

                                if (channelVideos && channelVideos.length > 0) {
                                    dc.channelVideos.fillData(channelVideos, { groupByProgramIdObservable: true, groupByChannelId: true, groupByHourlySlotId: true, groupBySelectedDateId: true });
                                }



                                dc.hourlySlots.fillData(hourlySlots);

                                d.resolve(responseData);
                            }
                            else {
                                d.reject();
                            }
                        })
                        .fail(function (responseData) {
                            d.reject();
                        });
                }).promise();
            },
            getChannelVideosByDate = function (data, isLive) {
                if (data) {
                    data.id = moment(new Date(data.id)).format('YYYY-MM-DD');
                }
                if (!isLive) {
                    isLive = null;
                }

                dc.channelVideos.clearAllNews(true);
                dc.hourlySlots.clearAllNews(true);

                return $.Deferred(function (d) {
                    $.when(dataservice.getChannelVideosByDate(data, isLive))
                        .done(function (responseData) {
                            if (responseData.IsSuccess) {


                                var hourlySlots = [
                                   { id: '00:00', name: '01:00' },
                                   { id: '01:00', name: '02:00' },
                                   { id: '02:00', name: '03:00' },
                                   { id: '03:00', name: '04:00' },
                                   { id: '04:00', name: '05:00' },
                                   { id: '05:00', name: '06:00' },
                                   { id: '06:00', name: '07:00' },
                                   { id: '07:00', name: '08:00' },
                                   { id: '08:00', name: '09:00' },
                                   { id: '09:00', name: '10:00' },
                                   { id: '10:00', name: '11:00' },
                                   { id: '11:00', name: '12:00' },
                                   { id: '12:00', name: '13:00' },
                                   { id: '13:00', name: '14:00' },
                                   { id: '14:00', name: '15:00' },
                                   { id: '15:00', name: '16:00' },
                                   { id: '16:00', name: '17:00' },
                                   { id: '17:00', name: '18:00' },
                                   { id: '18:00', name: '19:00' },
                                   { id: '19:00', name: '20:00' },
                                   { id: '20:00', name: '21:00' },
                                   { id: '21:00', name: '22:00' },
                                   { id: '22:00', name: '23:00' },
                                   { id: '23:00', name: '00:00' }
                                ];

                                var channelVideos = responseData.Data.ChannelVideos;

                                if (channelVideos && channelVideos.length > 0) {
                                    dc.channelVideos.fillData(channelVideos, { groupByProgramIdObservable: true, groupByChannelId: true, groupByHourlySlotId: true, groupBySelectedDateId: true });
                                }
                                dc.hourlySlots.fillData(hourlySlots);

                                d.resolve(responseData);
                            }
                            else {
                                d.reject();
                            }
                        })
                        .fail(function (responseData) {
                            d.reject();
                        });
                }).promise();
            },
            GetTotalTime = function (data) {
                return $.Deferred(function (d) {
                    $.when(dataservice.GetTotalTime(data))
                        .done(function (responseData) {
                            d.resolve(responseData);
                        })
                        .fail(function () {
                            d.reject();
                        });
                }).promise();
            };

        return {
            channelReporterloadInitialData: channelReporterloadInitialData,
            getChannelVideosByDate: getChannelVideosByDate,
            GetTotalTime: GetTotalTime
        }
    });