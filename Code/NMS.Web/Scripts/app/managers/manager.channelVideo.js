﻿define('manager.channelvideo',
    [
        'jquery',
        'dataservice',
        'datacontext',
        'underscore'
    ],
    function ($, dataservice, dc, _) {
        var
            markVideoProcessed = function (data) {
                return $.Deferred(function (d) {
                    $.when(dataservice.markVideoProcessed(data))
                        .done(function () {
                            d.resolve();
                        })
                        .fail(function () {
                            d.reject();
                        });
                }).promise();
            };

        return {
            markVideoProcessed: markVideoProcessed
        }
    });