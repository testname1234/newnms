﻿define('manager',
    [
        'manager.news',
        'manager.categories',
        'manager.locations',
        'manager.tags',
        'manager.channel',
        'manager.channelvideo',
        'manager.newspaper',
        'manager.radio',
        'manager.production',
        'manager.mcrusers',
        'manager.usermanagement',
        'manager.teleprompter',
        'manager.bolcg',
        'manager.ticker',
        'manager.newsfile'
    ],
    function (news, categories, locations, tags, channels, channelVideos, newspaper, radio, production, mcrusers, usermanagement, teleprompter, bolcg, ticker,newsfile) {

        return {
            news: news,
            categories: categories,
            locations: locations,
            tags: tags,
            channels: channels,
            channelVideos: channelVideos,
            newspaper: newspaper,
            radio: radio,
            production: production,
            mcrusers: mcrusers,
            usermanagement: usermanagement,
            teleprompter: teleprompter,
            bolcg: bolcg,
            ticker: ticker,
            newsfile: newsfile
    }
});