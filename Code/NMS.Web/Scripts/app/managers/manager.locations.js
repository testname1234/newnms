﻿define('manager.locations',
    [
        'dataservice',
        'datacontext'
    ],
    function (dataservice, dc) {
        var
            loadLocations = function (requestData) {
                return $.Deferred(function (d) {
                    $.when(dataservice.loadLocations(requestData))
                     .done(function (responseData) {
                         if (responseData.Data) {
                             dc.newsLocation.fillData(responseData.Data);
                         }
                         d.resolve(responseData);
                     })
                     .fail(function (responseData) {
                         d.reject(responseData);
                     });
                }).promise();
            };
        return {
            loadLocations: loadLocations
        }
    });