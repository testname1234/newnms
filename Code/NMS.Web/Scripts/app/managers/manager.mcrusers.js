﻿define('manager.mcrusers',
    [
        'dataservice',
        'manager.production',
        'datacontext'
    ],
    function (dataservice, production, dc) {
        var
            getProgramEpisodes = function (requestData) {
                return $.Deferred(function (d) {
                    $.when(dataservice.getProgramEpisodes(requestData))
                     .done(function (responseData) {
                         if (responseData.IsSuccess && responseData.Data) {
                             production.saveProgramEpisodes(responseData.Data);
                             d.resolve(responseData);
                         }
                         else {
                             d.reject(responseData);
                         }
                     })
                     .fail(function (responseData) {
                         d.reject(responseData);
                     });
                }).promise();
            },

            //singleEpisode
             getProgramEpisodeByRoId = function (requestData) {
                 return $.Deferred(function (d) {
                     $.when(dataservice.getProgramEpisodeByRoId(requestData))
                      .done(function (responseData) {
                          if (responseData.IsSuccess && responseData.Data) {
                              production.saveProgramEpisodes(responseData.Data);
                              d.resolve(responseData);
                          }
                          else {
                              d.reject(responseData);
                          }
                      })
                      .fail(function (responseData) {
                          d.reject(responseData);
                      });
                 }).promise();
             },
             //---- end
            getProgram = function (requestData) {
                return $.Deferred(function (d) {
                    $.when(dataservice.getProgram(requestData))
                     .done(function (responseData) { 
                         if (responseData.IsSuccess && responseData.Data) {
                             d.resolve(responseData);
                         }
                         else {
                             d.reject(responseData);
                         }

                     })
                     .fail(function (responseData) {
                         d.reject(responseData);
                     });
                }).promise();
            },
            getEpisodeMicsCameras = function (requestData) {
                return $.Deferred(function (d) {
                    $.when(dataservice.getEpisodeMicsCameras(requestData))
                     .done(function (responseData) {
                         if (responseData.IsSuccess && responseData.Data) {

                             var templates = responseData.Data;
                             for (var i = 0; i < templates.length; i++) {
                                 var screenTempCamera = [];
                                 var screenTempMics = [];
                                 var screentempResources = [];
                                 if (templates[i].SlotScreenTemplateCameras && templates[i].SlotScreenTemplateCameras != 'undefined') {
                                     for (var j = 0; j < templates[i].SlotScreenTemplateCameras.length; j++) {
                                         templates[i].SlotScreenTemplateCameras[j].slotScreenTemplateId = templates[i].SlotScreenTemplateId;
                                         screenTempCamera.push(templates[i].SlotScreenTemplateCameras[j]);                                     
                                     }
                                     dc.screenTemplateCameras.fillData(screenTempCamera, { groupCamerasByScreenTemplateId: true });
                                 }

                                 if (templates[i].SlotScreenTemplateMics && templates[i].SlotScreenTemplateMics != 'undefined') {
                                 for (var j = 0; j < templates[i].SlotScreenTemplateMics.length; j++) {
                                     templates[i].SlotScreenTemplateMics[j].slotScreenTemplateId = templates[i].SlotScreenTemplateId;
                                     screenTempMics.push(templates[i].SlotScreenTemplateMics[j]);       
                                 }
                                 dc.screenTemplateMics.fillData(screenTempMics, { groupMicsByScreenTemplateId: true });
                               }
                                 if (templates[i].SlotScreenTemplateResources && templates[i].SlotScreenTemplateResources.length > 0) {
                                     for (var j = 0; j < templates[i].SlotScreenTemplateResources.length; j++) {
                                         screentempResources.push(templates[i].SlotScreenTemplateResources[j]);
                                     }
                                     dc.resources.fillData(screentempResources, { groupResourcesByScreenTemplateId: true });

                                 }
                             }
                          
                             d.resolve(responseData);
                         }
                         else {
                             d.reject(responseData);
                         }

                     })
                     .fail(function (responseData) {
                         d.reject(responseData);
                     });
                }).promise();
            };
        return {
            getProgramEpisodes: getProgramEpisodes,
            getProgram: getProgram,
            getEpisodeMicsCameras: getEpisodeMicsCameras,
            getProgramEpisodeByRoId: getProgramEpisodeByRoId
        }
    });