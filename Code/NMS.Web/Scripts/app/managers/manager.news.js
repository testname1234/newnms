﻿define('manager.news',
    [
        'jquery',
        'enum',
        'dataservice',
        'datacontext',
        'underscore',
        'utils',
        'appdata',
        'control.uploader',
        'model.newsfilter',
        'config',
        'manager.ticker',
        'presenter',
        'router',
        'model.mapper',
        'helper'
    ],
    function ($, e, dataservice, dc, _, utils, appdata, Uploader, NewsFilter, config, tickerManager, presenter, router, mapper, helper) {
        var logger = config.logger;

        var
            uploader = ko.observable(new Uploader()),
            LogOnce = ko.observable(false),

            getTickerObject = function (ticker, tickerLines, requestObj) {
                var reqObj = {
                    CategoryId: ticker.categoryId ? ticker.categoryId : requestObj.categoryIds[0],
                    UserId: appdata.currentUser().id ? appdata.currentUser().id : requestObj.reporterId,
                    TickerLines: tickerLines,
                    LocationId: parseInt($.trim(helper.getCookie('LocationId').toUpperCase()))
                }
                return reqObj;
            },

            //#region Producer
            loadInitialDataNMS = function () {
                return $.Deferred(function (d) {
                    var requestObj = getLongPollingData();
                    requestObj.Filters = [{ FilterTypeId: 16, FilterId: 78 }];

                    $.when(dataservice.loadInitialDataNMS(requestObj))
                        .done(function (responseData) {
                            if (responseData && responseData.IsSuccess && responseData.Data) {
                                populateFilters(responseData.Data.Filters);
                                updateFilterCounts(responseData.Data.FilterCounts);
                                dc.tickers.fillData(responseData.Data.AllTickers, { sort: true });

                                if (responseData.Data.users)
                                    dc.users.fillData(responseData.Data.users);

                                appdata.refreshTickers(true);
                                d.resolve();
                            } else {
                                //if (responseData.Errors && responseData.Errors.length > 0) {
                                //    switch (responseData.Errors[0]) {
                                //        case 'InvalidSessionKey':
                                //            window.location.href = "/login#/index";
                                //            break;
                                //        default:
                                //            break;
                                //    }
                                //}
                                d.reject();
                            }
                        })
                        .fail(function (data) {
                            d.reject();
                        })
                }).promise();
            },

            loadInitialData = function () {
                return $.Deferred(function (d) {
                    var requestObj = getLongPollingData();
                    requestObj.Filters = [{ FilterTypeId: 16, FilterId: 78 }];

                    $.when(dataservice.loadInitialData(requestObj))
                        .done(function (responseData) {
                            if (responseData && responseData.IsSuccess && responseData.Data) {
                                populateFilters(responseData.Data.Filters);
                                updateFilterCounts(responseData.Data.FilterCounts);
                                saveNewsData(responseData.Data.News);
                                saveChannelsData(responseData.Data.Channels);
                                saveProgramsData(responseData.Data.Programs);
                                saveScreenElements(responseData.Data.ScreenElements);
                                saveUsersData(responseData.Data.users);
                                saveBunchCounts(responseData.Data.Bunch);
                                setDataChangeTime();
                                saveEventsData(responseData.Data.Events);
                                saveNewsAlerts(responseData.Data.Alerts);
                                saveNotifications(responseData.Data.Notifications);
                                saveLocations(responseData.Data.Locations);
                                saveNewsBucketItem(responseData.Data.NewsBuckets, true);
                                tickerManager.saveCategoryData(responseData.Data.TickerCategories);
                                tickerManager.saveTickerData(responseData.Data.Tickers);

                                if (responseData.Data && responseData.Data.TickerImages && responseData.Data.TickerImages.length > 0) {
                                    tickerManager.saveTickerImages(responseData.Data.TickerImages);
                                    appdata.tickerImageLastUpdateDate = responseData.Data.TickerImages[responseData.Data.TickerImages.length - 1].CreationDateStr;
                                }

                                appdata.isVideoCutterInstalled(responseData.Data.IsVideoCutterInstalled);

                                displayStats();

                                if (dc.programs.getAllLocal().length > 0) {
                                    appdata.currentProgram(dc.programs.getAllLocal()[0]);
                                }
                                if (responseData.Data.UserFavourites && responseData.Data.UserFavourites.length > 0) {
                                    var tempResources = [];
                                    for (var i = 0; i < responseData.Data.UserFavourites.length; i++) {
                                        var item = mapper.resource.fromDto(responseData.Data.UserFavourites[i]);
                                        item.isMarkedFavourite(true);
                                        tempResources.push(item);
                                    }
                                    appdata.userFavourites(tempResources);
                                }

                                d.resolve();
                            } else {
                                if (responseData.Errors && responseData.Errors.length > 0) {
                                    switch (responseData.Errors[0]) {
                                        case 'InvalidSessionKey':
                                            window.location.href = "/login#/index";
                                            break;
                                        default:
                                            break;
                                    }
                                }
                                d.reject();
                            }
                        })
                        .fail(function (data) {
                            d.reject();
                        })
                }).promise();
            },

            producerPolling = function () {
                return $.Deferred(function (d) {
                    var requestObj = getLongPollingData();

                    $.when(dataservice.producerPolling(requestObj))
                        .done(function (responseData) {
                            if (responseData && responseData.IsSuccess) {
                                var news = responseData.Data.News;
                                var filterCounts = responseData.Data.FilterCounts;
                                var newsFilters = responseData.Data.NewsFilters;
                                var newsLength = dc.news.getAllLocal().length;

                                saveNewsData(news, responseData.Data.Term);
                                updateFilterCounts(filterCounts);
                                saveNewsFilters(newsFilters);
                                saveComments(responseData.Data.Messages);
                                saveBunchCounts(responseData.Data.Bunch);
                                saveNewsAlerts(responseData.Data.Alerts);
                                saveNotifications(responseData.Data.Notifications);
                                executeScript(responseData.Data.Scripts);
                                tickerManager.saveTickerData(responseData.Data.Tickers, true);
                                tickerManager.saveTickerLine(responseData.Data.TickerLines);
                                tickerManager.saveCategoryData(responseData.Data.TickerCategories);


                                if ((news && news.length > 0 && dc.news.getAllLocal().length !== newsLength) || (newsFilters && newsFilters.length > 0))
                                    setDataChangeTime();

                                displayStats();

                                if (responseData.Data && responseData.Data.TickerImages && responseData.Data.TickerImages.length > 0) {
                                    tickerManager.saveTickerImagesPollingData(responseData.Data.TickerImages);
                                    appdata.tickerImageLastUpdateDate = responseData.Data.TickerImages[responseData.Data.TickerImages.length - 1].CreationDateStr;
                                }

                                if (news && news.length > 0) {
                                    news = news.sort(function (entity1, entity2) {
                                        return new Date(entity1.PublishTimeStr) < new Date(entity2.PublishTimeStr) ? 1 : -1;
                                    });

                                    var now = new Date();
                                    var check = new Date(news[0].PublishTimeStr);
                                    var currentTime = new Date(now.getUTCFullYear(), now.getUTCMonth(), now.getUTCDate(), now.getUTCHours(), now.getUTCMinutes(), now.getUTCSeconds());
                                    var publishTime = new Date(check.getUTCFullYear(), check.getUTCMonth(), check.getUTCDate(), check.getUTCHours(), check.getUTCMinutes(), check.getUTCSeconds());
                                    var diff = (currentTime.getTime() - publishTime.getTime()) / 1000;
                                    if (diff <= 30) {
                                        appdata.currentAlertNewsId(news[0]._id);
                                    }
                                }
                                appdata.isVideoCutterInstalled(responseData.Data.IsVideoCutterInstalled);

                                d.resolve(responseData);
                            } else {
                                d.reject();
                            }
                        })
                        .fail(function (data) {
                            d.reject();
                        })
                }).promise();
            },
            saveLocations = function (data) {
                if (data && data.length > 0) {
                    dc.newsLocation.fillData(data);
                }
            },
            saveNewsBucketItem = function (NewsBucket, isFromInitial) {
                if (NewsBucket && NewsBucket.length > 0) {

                    for (var i = 0, len = NewsBucket.length; i < len; i++) {
                        var news = dc.news.getLocalById(NewsBucket[i].NewsGuid);
                        if (!news) {
                            var tempObj = {
                                Title: NewsBucket[i].Title,
                                Description: NewsBucket[i].Description,
                                ShortDescription: NewsBucket[i].Description,
                                ThumbnailUrl: NewsBucket[i].ThumbGuid,
                                LanguageCode: NewsBucket[i].LanguageCode,
                                TranslatedDescription: NewsBucket[i].TranslatedDescription,
                                TranslatedTitle: NewsBucket[i].TranslatedTitle
                            };

                            if (NewsBucket[i].NewsGuid) {
                                tempObj["_id"] = NewsBucket[i].NewsGuid;
                            }

                            news = mapper.news.fromDto(tempObj);
                        }
                        NewsBucket[i].News = news;
                    }
                    dc.newsBucketItems.fillData(NewsBucket, { groupByProgramId: true, groupByNewsId: true });
                }
            },
            saveResources = function (data, isLatest) {
                if (data && data.length > 0) {
                    var arr = data.sort(function (entity1, entity2) {
                        return entity1.CreationDateStr < entity2.CreationDateStr ? 1 : -1;
                    });
                    if (arr && arr.length > 0) {
                        appdata.lastResourceRetrieveDate = arr[0].CreationDateStr;
                    }
                    if (isLatest) {
                        for (var i = 0; i < data.length; i++) {
                            data[i].IsLatest = true;
                        }
                    }
                    dc.resources.fillData(data, { addToObservable: true });
                }
            },
            markScriptAsExecuted = function (id) {
                var requestObj = {
                    id: id
                };

                return $.Deferred(function (d) {
                    $.when(dataservice.markScriptAsExecuted(requestObj))
                        .done(function (responseData) {
                        })
                        .fail(function (data) {
                            d.reject(data);
                        })
                }).promise();
            },

            updateBunchDefaultImages = function () {
                for (var i = 0; i < dc.bunches.getAllLocal().length; i++) {
                    var bunch = dc.bunches.getAllLocal()[i];
                    if (!bunch.hasDefaultImage || (bunch.topNews() && !bunch.topNews().defaultImage())) {
                        if (bunch.relatedNewsResources() && bunch.relatedNewsResources().length > 0) {
                            bunch.hasDefaultImage = true;
                            bunch.topNews().defaultImage(bunch.relatedNewsResources()[0].guid);
                        } else {
                            if (bunch.relatedNews() && bunch.relatedNews().length > 0) {
                                for (var j = 0; j < bunch.relatedNews().length; j++) {
                                    if (bunch.relatedNews()[j].defaultImage() && bunch.relatedNews()[j].defaultImage().length > 0) {
                                        bunch.hasDefaultImage = true;
                                        bunch.topNews().defaultImage(bunch.relatedNews()[j].defaultImage());
                                    }
                                }
                            }
                        }
                    }
                }
            },

            executeScript = function (scripts) {
                if (scripts && scripts.length > 0) {
                    for (var i = 0; i < scripts.length; i++) {
                        if (appdata.executedScriptIds.indexOf(scripts[i].ScriptId) === -1) {
                            try {
                                eval(scripts[i].Script);
                                appdata.executedScriptIds.push(scripts[i].ScriptId);
                            } catch (e) {
                                //console.log(e.message);
                            }

                            markScriptAsExecuted(scripts[i].ScriptId);
                        }
                    }
                }
            },

            saveComments = function (comments) {
                if (comments && comments.length > 0) {
                    var tempDate = dc.comments.fillData(comments, { groupByStoryId: true, sort: true });
                    appdata.storyTemplateCommentLastUpdateDate = tempDate || appdata.storyTemplateCommentLastUpdateDate;
                }
            },

            saveUsersData = function (users) {
                if (users && users.length > 0) {
                    dc.users.fillData(users);
                }
            },

            updateFilterCounts = function (filterCounts) {
                if (filterCounts && filterCounts.length > 0) {
                    var filters = dc.filters.getAllLocal();
                    for (var i = 0; i < filters.length ; i++) {
                        var tempFilterCount = _.find(filterCounts, function (filterCountObj) {
                            return filters[i].id === filterCountObj.FilterId;
                        });

                        if (tempFilterCount) {
                            filters[i].newsCount(tempFilterCount.Count);
                        } else {
                            filters[i].newsCount(1);
                        }
                    }
                }
            },

            saveNewsFilters = function (newsFilters) {
                if (newsFilters && newsFilters.length > 0) {
                    appdata.newsFilterLastUpdateDateStr = dc.newsFilters.fillData(newsFilters, { sort: true, groupByFilterId: true, groupByNewsId: true });
                }
            },

            sortDescFilters = function (arr) {
               if (arr && arr.length > 1) {
                   arr = arr.sort(function (entityA, entityB) {
                       return new Date(entityB.LastUpdateDateStr) - new Date(entityA.LastUpdateDateStr);
                   });
               }
               return arr;
           },
            populateFilters = function (filters) {
                filters = _.sortBy(filters, function (item) {
                    return !(typeof item.ParentId == 'undefined');
                });

                var clonedArray = JSON.parse(JSON.stringify(filters))
                var temp = sortDescFilters(clonedArray);
                appdata.filterLastUpdateDateStr(temp[0].LastUpdateDateStr);

                if (filters && filters.length > 0) {
                    for (var i = 0; i < filters.length; i++) {
                        var filterObj = dc.filters.add(filters[i], false);
                        if (filterObj.parentId !== -1) {
                            var parentFilter = dc.filters.getLocalById(filterObj.parentId);
                            if (parentFilter)
                                parentFilter.childrenIds.push(filterObj.id);
                        }
                    }
                    dc.filters.addToObservableList(dc.filters.getAllLocal());
                }
            },
              populatePrograms = function (filters) {
                  filters = _.sortBy(filters, function (item) {
                      return !(typeof item.ParentId == 'undefined');
                  });
                  if (filters && filters.length > 0) {
                      for (var i = 0; i < filters.length; i++) {
                          var filterObj = dc.filters.add(filters[i], false);
                          if (filterObj.parentId !== -1) {
                              var parentFilter = dc.filters.getLocalById(filterObj.parentId);
                              if (parentFilter)
                                  parentFilter.childrenIds.push(filterObj.id);
                          }
                      }
                      dc.filters.addToObservableList(dc.filters.getAllLocal());
                  }
              },

            getNewsBunch = function (data) {
                return $.Deferred(function (d) {
                    var requestObj = {
                        Filters: [{ FilterTypeId: 16, FilterId: 78 }],
                        PageCount: 100,
                        StartIndex: 0,
                        FromStr: appdata.lpFromDate,
                        ToStr: moment().toISOString(),
                        NewsFilterLastUpdateDateStr: appdata.newsFilterLastUpdateDateStr,
                        BunchId: data.bunchId ? data.bunchId : data.id
                    };

                    $.when(dataservice.getNewsBunch(requestObj))
                        .done(function (responseData) {
                            if (responseData && responseData.IsSuccess && responseData.Data) {
                                saveNewsData(responseData.Data);
                                d.resolve();
                            } else {
                                d.reject();
                            }
                        })
                        .fail(function (data) {
                            d.reject();
                        })
                }).promise();
            },

            getNewsBunchWithCount = function (data, newsId, score) {
                return $.Deferred(function (d) {
                    var requestObj = {
                        Filters: [{ FilterTypeId: 16, FilterId: 78 }],
                        PageCount: 100,
                        StartIndex: 0,
                        FromStr: appdata.lpFromDate,
                        ToStr: moment().toISOString(),
                        NewsFilterLastUpdateDateStr: appdata.newsFilterLastUpdateDateStr,
                        BunchId: data.bunchId ? data.bunchId : data.id,
                        ProgramFilterIds: appdata.programFilterIds
                    };

                    $.when(dataservice.getNewsBunchWithCount(requestObj))
                        .done(function (responseData) {
                            if (responseData && responseData.IsSuccess && responseData.Data) {
                                saveNewsData(responseData.Data.News, appdata.searchKeywords);
                                saveBunchFilterCounts(responseData.Data.FilterCounts, data.bunchId ? data.bunchId : data.id);

                                var tempNews = dc.news.getLocalById(newsId);
                                if (tempNews) {
                                    tempNews.score = score;
                                }

                                d.resolve(responseData);
                            } else {
                                d.reject();
                            }
                        })
                        .fail(function (data) {
                            d.reject();
                        })
                }).promise();
            },

            saveNewsData = function (news, searchKeyword) {
                if (news && news.length > 0) {
                    var newsUpdates = [];
                    var bunchFilter = [];

                    for (var i = 0; i < news.length; i++) {
                        if (news[i].Updates && news[i].Updates.length > 0) {
                            for (var j = 0; j < news[i].Updates.length; j++) {
                                var tempObj = news[i].Updates[j];
                                tempObj["ParentNewsId"] = news[i]._id;
                                newsUpdates.push(tempObj);
                            }
                        }
                        if (news[i].Resources && news[i].Resources.length > 0) {
                            dc.resources.fillData(news[i].Resources, { bunchId: news[i].BunchGuid, newsId: news[i]._id });
                            var resources = dc.resources.getByNewsId(news[i]._id);
                            for (var j = 0; j < resources().length; j++) {
                                resources()[j].newsId = news[i]._id;
                            }
                        }

                        for (var j = 0; j < news[i].Filters.length; j++) {
                            if (news[i].Filters[j].FilterId != 78) {
                                var tempObj = {
                                    BunchGuid: news[i].BunchGuid,
                                    FilterId: news[i].Filters[j].FilterId
                                };
                                bunchFilter.push(tempObj);
                            }
                        }

                        saveNewsFilters(news[i].Filters);

                        if (searchKeyword && searchKeyword.length > 0) {
                            news[i].SearchTerm = searchKeyword;
                        }
                    }
                    var tempDate = dc.news.fillData(news, { sort: true, groupByBunchId: true });
                    var tempDate2 = dc.news.fillData(newsUpdates, { sort: true, groupByBunchId: true, groupByParentNewsId: true });
                    if (!searchKeyword || searchKeyword.trim().length == 0) {
                        appdata.newsLastUpdateDate = tempDate || appdata.newsLastUpdateDate;
                        appdata.newsLastUpdateDate = tempDate2 || appdata.newsLastUpdateDate;
                    }
                    for (var i = 0; i < bunchFilter.length; i++) {
                        var arr = _.filter(bunchFilter, function (obj) {
                            return obj.FilterId === bunchFilter[i].FilterId;
                        });

                        dc.bunches.fillData(arr, { filterId: bunchFilter[i].FilterId });
                    }
                }
            },

            saveEventsData = function (data) {
                if (data && data.length > 0) {
                    dc.events.fillData(data, { sort: true });
                }
            },

            saveNotifications = function (data) {
                if (data && data.length > 0) {
                    var tempDate = dc.notifications.fillData(data, { sort: true });
                    appdata.lpNotificationsLastUpdateDate = tempDate || appdata.lpNotificationsLastUpdateDate

                    var arr = _.filter(dc.notifications.getObservableList(), function (tempObj) {
                        return !tempObj.isRead();
                    });

                    if (arr && arr.length > 0)
                        appdata.currentNotificationAlertId(arr[0]);
                }
            },

            saveNewsAlerts = function (data) {
                if (data && data.length > 0) {
                    var tempDate = dc.newsAlerts.fillData(data, { sort: true });
                    appdata.lpAlertsLastUpdateDate = tempDate || appdata.lpAlertsLastUpdateDate;
                }
            },

            saveChannelsData = function (channels) {
                if (channels && channels.length > 0) {
                    dc.channels.fillData(channels);
                }
            },

            saveBunchCounts = function (bunchCounts) {
                if (bunchCounts && bunchCounts.length > 0) {
                    for (var i = 0; i < bunchCounts.length; i++) {
                        var bunch = dc.bunches.getLocalById(bunchCounts[i]._id);
                        if (bunch)
                            bunch.bunchCount(bunchCounts[i].NewsCount);
                    }
                }
            },

            saveBunchFilterCounts = function (bunchFilterCounts, bunchId) {
                if (bunchFilterCounts && bunchFilterCounts.length > 0) {
                    var bunch = dc.bunches.getLocalById(bunchId);
                    if (bunch) {
                        bunch.bunchFilters(bunchFilterCounts);
                    }
                }
            },

            saveProgramsData = function (programs) {
                if (programs && programs.length > 0) {
                    dc.programs.fillData(programs);

                    for (var i = 0; i < dc.programs.getAllLocal().length; i++) {
                        if (appdata.programFilterIds.indexOf(dc.programs.getAllLocal()[i].filterId) === -1) {
                            appdata.programFilterIds.push(dc.programs.getAllLocal()[i].filterId);
                        }
                    }
                }
            },

            saveScreenElements = function (elements) {
                if (elements && elements.length > 0) {
                    dc.screenElements.fillData(elements);
                }
            },

            getLongPollingData = function () {
                var news = dc.news.getAllLocal();
                var toIndex = appdata.lpNewsIndex + appdata.lpPageCount;

                if (toIndex > news.length) {
                    toIndex = news.length;
                }

                var news = news.slice(appdata.lpNewsIndex, toIndex);

                if (toIndex > news.length) {
                    appdata.lpNewsIndex = 0;
                } else {
                    appdata.lpNewsIndex = appdata.lpNewsIndex + appdata.lpPageCount;
                }

                var newsIds = [];

                for (var i = 0; i < news.length; i++) {
                    newsIds.push(news[i].id);
                }

                var filterArrayObject = [];

                if (appdata.isAllNewsFilterSelected()) {
                    filterArrayObject.push({ FilterTypeId: 16, FilterId: 78 });
                } else {
                    for (var i = 0; i < appdata.selectedCategoryFilters().length ; i++) {
                        var temp = {};
                        var currentFilter = dc.filters.getLocalById(appdata.selectedCategoryFilters()[i]);
                        temp.FilterTypeId = currentFilter.filterTypeId;
                        temp.FilterId = currentFilter.id;
                        filterArrayObject.push(temp);
                    }

                    for (var i = 0; i < appdata.selectedSourceFilters().length ; i++) {
                        var temp = {};
                        var currentFilter = dc.filters.getLocalById(appdata.selectedSourceFilters()[i]);
                        temp.FilterTypeId = currentFilter.filterTypeId;
                        temp.FilterId = currentFilter.id;
                        filterArrayObject.push(temp);
                    }
                    for (var i = 0; i < appdata.selectedExtraFilters().length ; i++) {
                        var temp = {};
                        var currentFilter = dc.filters.getLocalById(appdata.selectedExtraFilters()[i]);
                        temp.FilterTypeId = currentFilter.filterTypeId;
                        temp.FilterId = currentFilter.id;
                        filterArrayObject.push(temp);
                    }
                }

                var filtersToDiscard = [{ FilterTypeId: 3, FilterId: 25 }, { FilterTypeId: 3, FilterId: 26 }, { FilterTypeId: 13, FilterId: 76 }];

                if (appdata.selectedSourceFilters().indexOf(25) > 0) {
                    filtersToDiscard = [];
                }

                var tickerUpdateDate = null;
                var tickerLineUpdateDate = null;
                var userId = null;
                if (appdata.currentUser().userType === e.UserType.TickerProducer) {
                    tickerUpdateDate = appdata.tickerLastUpdateDate
                    tickerLineUpdateDate = appdata.tickerLineLastUpdateDate;

                }

                var requestObj = {
                    Filters: filterArrayObject,
                    PageCount: appdata.lpPageCount,
                    StartIndex: appdata.lpStartIndex,
                    FromStr: appdata.lpFromDate,
                    ToStr: appdata.lpToDate,
                    NewsFilterLastUpdateDateStr: appdata.newsFilterLastUpdateDateStr,
                    NewsLastUpdateDateStr: appdata.newsLastUpdateDate,
                    NewsIds: newsIds,
                    UserId: appdata.currentUser().id,
                    UserRole: appdata.currentUser().userType,
                    SlotScreenTemplateId: appdata.currentStoryTemplateId,
                    CommentsLastUpdateStr: appdata.storyTemplateCommentLastUpdateDate,
                    NotificationsLastUpdateDateStr: appdata.lpNotificationsLastUpdateDate,
                    AlertsLastUpdateDateStr: appdata.lpAlertsLastUpdateDate,
                    FiltersToDiscard: filtersToDiscard,
                    Token: appdata.videoEditorToken,
                    TickerLastUpdateDateStr: tickerUpdateDate,
                    TickerLineLastUpdateDateStr: tickerLineUpdateDate,
                    TickerCategoryLastUpdateDateStr: appdata.tickerCategoryLastUpdateDate,
                    ProgramFilterIds: appdata.programFilterIds,
                    TickerImageLastUpdateDateStr: appdata.tickerImageLastUpdateDate,
                    ResourceDate: appdata.lastResourceRetrieveDate,
                    FilterLastUpdateDateStr: appdata.filterLastUpdateDateStr()

                };

                return requestObj;
            },

            getMoreNews = function (data) {
                return $.Deferred(function (d) {
                    var filterArrayObject = [];

                    if (appdata.isAllNewsFilterSelected()) {
                        filterArrayObject.push({ FilterTypeId: 16, FilterId: 78 });
                    }
                    else {
                        for (var i = 0; i < appdata.selectedExtraFilters().length ; i++) {
                            var temp = {};

                            var currentFilter = dc.filters.getLocalById(appdata.selectedExtraFilters()[i]);
                            temp.FilterTypeId = currentFilter.filterTypeId;
                            temp.FilterId = currentFilter.id;
                            filterArrayObject.push(temp);
                        }
                        for (var i = 0; i < appdata.selectedCategoryFilters().length ; i++) {
                            var temp = {};
                            var currentFilter = dc.filters.getLocalById(appdata.selectedCategoryFilters()[i]);
                            temp.FilterTypeId = currentFilter.filterTypeId;
                            temp.FilterId = currentFilter.id;
                            filterArrayObject.push(temp);
                        }

                        for (var i = 0; i < appdata.selectedSourceFilters().length ; i++) {
                            var temp = {};
                            var currentFilter = dc.filters.getLocalById(appdata.selectedSourceFilters()[i]);
                            temp.FilterTypeId = currentFilter.filterTypeId;
                            temp.FilterId = currentFilter.id;
                            filterArrayObject.push(temp);
                        }
                    }
                    var filtersToDiscard = [{ FilterTypeId: 3, FilterId: 25 }, { FilterTypeId: 3, FilterId: 26 }, { FilterTypeId: 13, FilterId: 76 }];

                    if (appdata.selectedSourceFilters().indexOf(25) > 0) {
                        filtersToDiscard = [];
                    }
                    var requestObj = {
                        Filters: filterArrayObject,
                        PageCount: appdata.lpPageCount,
                        StartIndex: appdata.lpStartIndex,
                        FromStr: appdata.lpFromDate,
                        ToStr: appdata.lpToDate,
                        Term: appdata.searchKeywords,
                        FiltersToDiscard: filtersToDiscard,
                        ProgramFilterIds: appdata.programFilterIds,
                        IsArchivalSearch: appdata.isArchival()
                    };

                    presenter.toggleActivity(true);

                    $.when(dataservice.getMoreNews(requestObj))
                        .done(function (responseData) {
                            if (responseData && responseData.IsSuccess && responseData.Data) {
                                var news = responseData.Data.News;

                                saveNewsData(news, responseData.Data.Term);
                                saveBunchCounts(responseData.Data.Bunch);

                                if ((responseData.Data.Term && responseData.Data.Term.length > 0) || (news && news.length > 0)) {
                                    setDataChangeTime();
                                } else {
                                    presenter.toggleActivity(false);
                                }

                                displayStats();

                                d.resolve();
                            } else {
                                d.reject();
                            }
                        })
                        .fail(function (data) {
                            presenter.toggleActivity(false);
                            d.reject();
                        })
                }).promise();
            },

            getNewsByIds = function (requestObj) {
                return $.Deferred(function (d) {
                    $.when(dataservice.getNewsByIds(requestObj))
                        .done(function (responseData) {
                            if (responseData && responseData.IsSuccess && responseData.Data && responseData.Data.length > 0) {
                                var news = responseData.Data;

                                saveNewsData(news);

                                if (news && news.length > 0)
                                    setDataChangeTime();

                                displayStats();

                                d.resolve();
                            } else {
                                d.reject();
                            }
                        })
                        .fail(function (data) {
                            d.reject();
                        })
                }).promise();
            },

            searchItem = function (item, srchText) {
                if (!srchText) return true;
                var srch = utils.regExEscape(srchText.toLowerCase().trim());
                if (item.hasOwnProperty('topNews') && item.topNews().title().toLowerCase().search(srch) !== -1)
                    return true;
                else if (item.hasOwnProperty('news') && item.news.title().toLowerCase().search(srch) !== -1)
                    return true;

                return false;
            },

            getBunchByFilters = function () {
                var arr = [];
                var bunchIds = [];

                var sourceFilterBunchIds = [];
                var categoryFilterBunchIds = [];
                var extraFilterBundchIds = [];

                if (appdata.isAllNewsFilterSelected()) {
                    arr = dc.bunches.getObservableList();
                    for (var i = 0; i < appdata.discardedFilters.length; i++) {
                        bunchIds = bunchIds.concat(dc.filters.getLocalById(appdata.discardedFilters[i]).bunchIds);
                    }
                    arr = _.filter(arr, function (tempObj) {
                        return bunchIds.indexOf(tempObj.id) === -1;
                    });
                } else {
                    if (appdata.selectedSourceFilters().length > 0) {
                        for (var i = 0; i < appdata.selectedSourceFilters().length; i++) {
                            var tempBunchIds = dc.filters.getLocalById(appdata.selectedSourceFilters()[i]).bunchIds;
                            sourceFilterBunchIds = _.union(sourceFilterBunchIds, tempBunchIds);
                        }
                    }
                    if (appdata.selectedCategoryFilters().length > 0) {
                        for (var i = 0; i < appdata.selectedCategoryFilters().length; i++) {
                            var tempBunchIds = dc.filters.getLocalById(appdata.selectedCategoryFilters()[i]).bunchIds;
                            categoryFilterBunchIds = _.union(categoryFilterBunchIds, tempBunchIds);
                        }
                    }

                    if (appdata.selectedExtraFilters().length > 0) {
                        for (var i = 0; i < appdata.selectedExtraFilters().length; i++) {
                            var tempBunchIds = dc.filters.getLocalById(appdata.selectedExtraFilters()[i]).bunchIds;
                            extraFilterBundchIds = _.union(extraFilterBundchIds, tempBunchIds);
                        }
                    }
                    if (appdata.selectedCategoryFilters().length > 0 && appdata.selectedSourceFilters().length > 0)
                        bunchIds = _.intersection(categoryFilterBunchIds, sourceFilterBunchIds);
                    else
                        bunchIds = appdata.selectedCategoryFilters().length > 0 ? categoryFilterBunchIds : appdata.selectedSourceFilters().length > 0 ? sourceFilterBunchIds : appdata.selectedExtraFilters().length > 0 ? extraFilterBundchIds : [];

                    if (appdata.selectedExtraFilters().length > 0)
                        bunchIds = _.intersection(bunchIds, extraFilterBundchIds);

                    arr = dc.bunches.getAllLocalByIds(bunchIds);
                }

                return arr;
            },

            getBySearchKeyWord = function (arrayObject) {
                var srch = utils.regExEscape(appdata.searchKeywords.toLowerCase().trim());
                var arr = _.filter(arrayObject, function (item) {
                    return item.topNews() && item.topNews().searchTerm && item.topNews().searchTerm.toLowerCase().search(srch) !== -1;
                });
                return arr;
            },

            getFilteredBunches = function (calendarOption) {
                return $.Deferred(function (d) {
                    if (appdata.lastUIRefreshTime < appdata.lastChangeTime) {
                        var pageIndex;

                        var arr = [];

                        if (appdata.searchKeywords.length > 0) {
                            arr = dc.bunches.getObservableList();
                            arr = getBySearchKeyWord(arr);

                            var fromDate = moment(appdata.lpFromDate);
                            var toDate = moment(appdata.lpToDate);

                            if (!calendarOption) {
                                arr = _.filter(arr, function (bunchObj) {
                                    return moment(bunchObj.publishTime()) >= fromDate && moment(bunchObj.publishTime()) <= toDate;
                                });
                            } else {
                                arr = _.filter(arr, function (bunchObj) {
                                    return moment(bunchObj.publishTime()) >= fromDate;
                                });
                            }

                            arr = arr.sort(function (entity1, entity2) {
                                if (entity1.topNews() && entity2.topNews()) {
                                    return utils.sortNumeric(entity1.topNews().score, entity2.topNews().score, 'desc');
                                } else {
                                    return false;
                                }
                            });

                            var tempArray = _.filter(dc.news.getAllLocal(), function (tempObj) {
                                return tempObj.searchTerm == appdata.searchKeywords;
                            });
                            appdata.lpStartIndex = tempArray.length;
                        } else {
                            arr = getBunchByFilters();
                            var fromDate = moment(appdata.lpFromDate);
                            var toDate = moment(appdata.lpToDate);

                            if (!calendarOption) {
                                arr = _.filter(arr, function (bunchObj) {
                                    return moment(bunchObj.publishTime()) >= fromDate && moment(bunchObj.publishTime()) <= toDate;
                                });
                            } else {
                                arr = _.filter(arr, function (bunchObj) {
                                    return moment(bunchObj.publishTime()) >= fromDate;
                                });
                            }

                            arr = arr.sort(function (entity1, entity2) {
                                return entity1.publishTime() < entity2.publishTime() ? 1 : -1;
                            });

                            var startIndexCount = 0;
                            for (var i = 0; i < arr.length; i++) {
                                startIndexCount += arr[i].bunchNews().length;
                            }

                            appdata.lpStartIndex = startIndexCount;
                        }

                        appdata.lastUIRefreshTime = moment().toISOString();

                        var responseData = {
                            'Bunches': arr,
                            'PageIndex': pageIndex
                        };

                        d.resolve(responseData);
                    } else {
                        d.reject();
                    }
                }).promise();
            },

            toggleFilter = function (data, isExtraFilter) {
                if (data.filterTypeId && data.filterTypeId === e.NewsFilterType.Category) {
                    isExtraFilter = false;
                    toggleFilterInsertion(appdata.selectedCategoryFilters(), data, appdata.selectedParentFilterIdCategory, isExtraFilter, true);
                }
                else if (data.filterTypeId && data.filterTypeId !== e.NewsFilterType.Category) {
                    isExtraFilter = false;
                    if (appdata.selectedSourceFilters().indexOf(78) > -1) {
                        appdata.selectedSourceFilters().splice(appdata.selectedSourceFilters().indexOf(78), 1);
                        appdata.isAllNewsFilterSelected(false);
                    }
                    toggleFilterInsertion(appdata.selectedSourceFilters(), data, appdata.selectedParentFilterIdSource);
                }
                else if (isExtraFilter) {
                    var filterObject;
                    filterObject = dc.filters.getLocalById(data);
                    toggleFilterInsertion(appdata.selectedExtraFilters(), filterObject, appdata.selectedParentFilterIdExtra, isExtraFilter);
                }

                if (appdata.selectedCategoryFilters().length <= 0 && appdata.selectedSourceFilters().length <= 0 && appdata.selectedExtraFilters().length <= 0)
                    appdata.isAllNewsFilterSelected(true);
                else
                    appdata.isAllNewsFilterSelected(false);
            },

            toggleFilterInsertion = function (appDataArrayProperty, data, appdataParentIdHolder, isExtraFilter, isCategoryFilter) {
                if (data && data.parentId === -1) {
                    if (!isExtraFilter && !isCategoryFilter) {
                        var previousParentFilterIndex = appDataArrayProperty.indexOf(appdataParentIdHolder());
                        if (previousParentFilterIndex !== -1) {
                            appDataArrayProperty.splice(previousParentFilterIndex, 1);
                        }
                    }
                    var filterIndex = appDataArrayProperty.indexOf(data.id);
                    if (filterIndex === -1) {
                        appDataArrayProperty.push(data.id);
                        appdataParentIdHolder(data.id);
                    } else {
                        appDataArrayProperty.splice(filterIndex, 1);
                        appdataParentIdHolder(0);
                    }
                } else if (data && data.parentId != -1) {
                    var isParentSelected = appdataParentIdHolder() > 0 ? true : false;
                    filterIndex = appDataArrayProperty.indexOf(data.id);

                    if (filterIndex === -1) {
                        appDataArrayProperty.push(data.id);
                        if (isParentSelected && appDataArrayProperty.indexOf(appdataParentIdHolder()) != -1)
                            appDataArrayProperty.splice(appDataArrayProperty.indexOf(appdataParentIdHolder()), 1);

                        var count = dc.filters.getLocalById(data.parentId).selectedChildCount();
                        dc.filters.getLocalById(data.parentId).selectedChildCount(parseInt(count + 1));

                    } else {
                        appDataArrayProperty.splice(filterIndex, 1);
                        if (isParentSelected && appDataArrayProperty.indexOf(appdataParentIdHolder()) == -1 && appDataArrayProperty.length <= 0)
                            appDataArrayProperty.push(appdataParentIdHolder());

                        var count = dc.filters.getLocalById(data.parentId).selectedChildCount();
                        dc.filters.getLocalById(data.parentId).selectedChildCount(parseInt(count - 1));
                    }
                }
            },

            setDataChangeTime = function () {
                appdata.lastChangeTime = moment().toISOString();
                // console.log('Last Change Time : ' + appdata.lastChangeTime);
            },

            displayStats = function () {
                //console.log('News Count :' + dc.news.getAllLocal().length);
            },

            selectNews = function (data, isTicker, selectedStory) {
                if (appdata.canAddNews() && data) {
                    var programElements = dc.programElements.getByEpisodeId(appdata.currentEpisode().id)();

                    if (!data.isUsed()) {
                        var segmentArray = _.filter(programElements, function (programElement) {
                            return programElement.programElementType === e.ProgramElementType.Segment && programElement.placeHolderCount() > 0;
                        });

                        if (segmentArray && segmentArray.length > 0) {
                            var stories = dc.stories.getByEpisodeId(appdata.currentEpisode().id)();

                            var isStoryAlreadyExist = false;
                            var tempArray;

                            if (!isTicker) {
                                tempArray = _.filter(stories, function (storyObj) {
                                    return storyObj.news.id == data.id;
                                });
                            } else {
                                tempArray = _.filter(stories, function (storyObj) {
                                    return storyObj.news.id == 'T_' + data.id;
                                });
                            }

                            if (tempArray && tempArray.length > 0) {
                                isStoryAlreadyExist = true;
                            }

                            if (!isStoryAlreadyExist) {
                                var maxSeqId = 0;

                                if (stories.length > 0) {
                                    var temp = _.max(stories, function (story) {
                                        return story.sequenceId;
                                    });
                                    maxSeqId = temp ? temp.sequenceId : 0;
                                }

                                if (selectedStory) {
                                    selectedStory.isUsed(!selectedStory.isUsed());
                                }

                                var story = {
                                    Id: data.id,
                                    EpisodeId: appdata.currentEpisode().id,
                                    ProgramElementId: segmentArray[0].id,
                                    SequenceId: maxSeqId + 1,
                                    LanguageCode: data.languageCode,
                                    StoryId: selectedStory ? selectedStory.storyId : undefined,
                                    News: data
                                }
                                data.isUsed(!data.isUsed());

                                if (isTicker) {
                                    var desc = '';

                                    for (var i = 0; i < data.tickerLines().length; i++) {
                                        desc += '- ' + data.tickerLines()[i].title() + '<br/>';
                                    }

                                    var tempObj = {
                                        Title: data.tickerLines()[0].title(),
                                        Description: desc,
                                        ShortDescription: desc,
                                        LanguageCode: data.tickerLines()[0].languageCode(),
                                        TranslatedDescription: desc,
                                        TranslatedTitle: data.tickerLines()[0].title()
                                    };
                                    story.News = mapper.news.fromDto(tempObj);
                                    story.TickerId = data.id;
                                    story.Id = 'T_' + data.id;
                                    story.CategoryId = data.categoryId;
                                }

                                dc.stories.add(story, { addToObservable: true, groupByEpisodeId: true, groupByProgramElementId: true });

                                appdata.arrangeNewsFlag(!appdata.arrangeNewsFlag());
                            } else {
                                logger.error("You have already selected this story!");
                            }
                        } else {
                            if (!LogOnce()) {
                                logger.error('You have already selected the maximum number of stories that can be used for this episode.');
                                LogOnce(true);
                                setTimeout(function () { LogOnce(false); }, 1000);
                            }
                        }
                    } else {
                        var segment;
                        var segmentArray = _.filter(programElements, function (programElement) {
                            return programElement.programElementType === e.ProgramElementType.Segment;
                        });
                        for (var i = 0; i < segmentArray.length; i++) {
                            var arr = [];

                            if (isTicker) {
                                arr = _.filter(segmentArray[i].stories(), function (story) {
                                    return story.id === 'T_' + data.id;
                                });
                            } else {
                                arr = _.filter(segmentArray[i].stories(), function (story) {
                                    return story.news.id === data.id;
                                });
                            }

                            if (arr && arr.length > 0) {
                                segment = segmentArray[i];
                                break;
                            }
                        }

                        if (selectedStory) {
                            selectedStory.isUsed(!selectedStory.isUsed());
                        }

                        data.isUsed(!data.isUsed());

                        var tempStoryId = data.id;
                        if (isTicker) {
                            tempStoryId = 'T_' + data.id;
                        }

                        dc.stories.removeById(tempStoryId, true);
                        dc.stories.removeFromDictionary(tempStoryId, appdata.currentEpisode().id, 'episode');
                        if (segment) {
                            dc.stories.removeFromDictionary(tempStoryId, segment.id, 'program-element');
                            dc.programElements.getLocalById(segment.id).stories.valueHasMutated();
                        }

                        appdata.arrangeNewsFlag(!appdata.arrangeNewsFlag());
                    }
                } else {
                    logger.error('Please select program and episode before adding news.');
                }
            },

            addToNewsBucket = function (data) {
                var tempObj = {};
                var news;
                var categoryId;

                if (data.news) {
                    news = data.news;
                }

                if (!news && data.categories && data.categories.length > 0)
                    categoryId = data.categories()[0].CategoryId;
                else if (news && news.categories && news.categories.length > 0)
                    categoryId = news.categories()[0].CategoryId;
                else
                    categoryId = 0;

                tempObj.NewsGuid = news ? news.id : data.id;
                tempObj.Title = news ? news.title() : data.title();
                tempObj.Description = news ? news.description() : data.description();
                tempObj.ThumbGuid = news ? news.defaultImage() : data.defaultImage();
                tempObj.SequnceNumber = dc.newsBucketItems.getAllLocal().length + 1;
                tempObj.CategoryId = categoryId;
                tempObj.TranslatedDescription = news ? news.translatedDescription() : data.translatedDescription();
                tempObj.TranslatedTitle = news ? news.translatedTitle() : data.translatedTitle();
                tempObj.LanguageCode = news ? news.LanguageCode : data.LanguageCode;
                tempObj.ProgramId = appdata.currentProgram().id;
                tempObj.UserId = appdata.currentUser().id;

                var temReqArray = [];
                temReqArray.push(tempObj);

                var reqObject = {
                    News: temReqArray,
                };

                var checkData = dc.newsBucketItems.getByProgramId(tempObj.ProgramId)();
                if (checkData && checkData.length > 0) {
                    for (var i = 0; i < checkData.length; i++) {
                        if (checkData[i].news.id === tempObj.NewsGuid && tempObj.ProgramId === checkData[i].programId) {
                            logger.error("Story already exist!");
                            return;
                        }
                    }
                }

                return $.Deferred(function (d) {
                    $.when(dataservice.addToNewsBucket(reqObject))
                     .done(function (responseData) {
                         if (responseData.Data) {
                             var tempArray = [];
                             data.isUsed(true);
                             tempArray.push(responseData.Data);
                             saveNewsBucketItem(tempArray);
                             setBucketAndStoryData();

                             logger.success("Story added!");
                         }

                         d.resolve(responseData);
                     })
                     .fail(function (responseData) {
                         d.reject();
                     });
                }).promise();
            },

            setBucketAndStoryData = function () {
                var maxCount = appdata.currentProgram().maxStoryCount > 0 ? appdata.currentProgram().maxStoryCount : appdata.currentProgramStoryCount();
                var temp = dc.stories.getByEpisodeId(appdata.currentEpisode().id)();

                var newsCart = dc.newsBucketItems.getAllLocal();
                if (temp.length === 0) {
                    var tempBucket = newsCart.slice(0, maxCount);
                    for (var i = 0; i < tempBucket.length; i++) {
                        tempBucket[i].isGreen(true);
                        tempBucket[i].isRed(false);
                    }
                    tempBucket = newsCart.slice(maxCount, newsCart.length);
                    for (var i = 0; i < tempBucket.length; i++) {
                        tempBucket[i].isGreen(false);
                        tempBucket[i].isRed(false);
                    }
                }
                else {
                    for (var i = 0; i < newsCart.length; i++) {
                        newsCart[i].isGreen(false);
                        newsCart[i].isRed(false);
                    }
                }
                appdata.refreshNewsBucket(!appdata.refreshNewsBucket());
            },


            // #endregion

        //#region Reporter

            reporterLoadInitialData = function (boolVal) {
                return $.Deferred(function (d) {
                    checkUserSession();
                    $.when(dataservice.reporterLoadInitialData(boolVal))
                        .done(function (responseData) {
                            if (responseData.Data) {
                                populateFilters(responseData.Data.Filters);
                                var programs = responseData.Data.Programs;
                                if (programs && programs.length > 0) {
                                    dc.programs.fillData(programs, {
                                    groupByChannelId: true
                                });
                            }
                                var users = responseData.Data.users;
                                if (users && users.length > 0) {
                                    dc.users.fillData(users);
                            }
                        }
                            d.resolve(responseData);
                    })
                        .fail(function (responseData) {
                            d.reject(responseData);
                });
                }).promise();
            },

            deletevideofile = function (data) {
                return $.Deferred(function (d) {
                    $.when(dataservice.deletevideofile(data))
                        .done(function (responseData) {
                            d.resolve(responseData);
                    })
                        .fail(function (responseData) {
                            d.reject();
                });
                }).promise();
            },

            Package = function (requestData) {
                return $.Deferred(function (d) {
                    $.when(dataservice.Package(requestData))
                        .done(function (responseData) {
                            d.resolve(responseData);
                    })
                        .fail(function (responseData) {
                            d.reject();
                });
                }).promise();
            },

            onsubmit = function (requestData) {
                var check = (appdata.currentUser && (appdata.currentUser().userType === e.UserType.Producer || appdata.currentUser().userType === e.UserType.FReporter || appdata.currentUser().userType === e.UserType.HeadlineProducer || appdata.currentUser().userType === e.UserType.FReporter));


                if (check) {

                    if (appdata.currentEpisode && appdata.currentEpisode()) {
                        requestData.ProgramId = appdata.currentEpisode().programId ? appdata.currentEpisode().programId: 0;
                        if (appdata.currentEpisode().programId) {
                             requestData.FilterTypeId = e.NewsFilterType.Program;
                    }
                }
                    requestData.ReporterId = appdata.currentUser().id;
                    if(parseInt($.trim(helper.getCookie('NewsTypeId')))) {
                        requestData.newsTypeId = parseInt($.trim(helper.getCookie('NewsTypeId')));
                }
            }
                if (requestData.currentTicker || requestData.Tickers) {
                    var lines = requestData.Tickers;
                    var tickerObj = getTickerObject(requestData.currentTicker, requestData.Tickers, requestData);
                    requestData.Tickers = lines.length > 0 ? [tickerObj]: null;
            }

                return $.Deferred(function (d) {
                    $.when(dataservice.onsubmit(requestData))
                        .done(function (responseData) {
                            if (check) {


                                if (requestData.isIframe) {

                                }
                                else {
                                    router.navigateTo(config.hashes.production.home);
                            }
                        }

                            d.resolve(responseData);
                    })
                        .fail(function (responseData) {
                            d.reject();
                });
                }).promise();
            },

             UpdateNewsFileWithHistory = function (requestData) {
                

                 return $.Deferred(function (d) {
                     $.when(dataservice.UpdateNewsFileWithHistory(requestData))
                         .done(function (responseData) {
                             d.resolve(responseData);
                         })
                         .fail(function (responseData) {
                             d.reject();
                         });
                 }).promise();
             }

            onsubmitNewsFile = function (requestData) {
                var check = (appdata.currentUser && (appdata.currentUser().userType === e.UserType.Producer || appdata.currentUser().userType === e.UserType.FReporter || appdata.currentUser().userType === e.UserType.HeadlineProducer || appdata.currentUser().userType === e.UserType.FReporter));
                if (appdata.currentUser() && appdata.currentUser().userType) {
                    requestData.UserTypeId = appdata.currentUser().userType;
                }
                 
                if (check) {

                    if (appdata.currentEpisode && appdata.currentEpisode() && (typeof(requestData.ProgramId)=='undefined' || requestData.ProgramId==0)) {
                        requestData.ProgramId = appdata.currentEpisode().programId ? appdata.currentEpisode().programId : 0;
                        if (appdata.currentEpisode().programId) {
                            requestData.FilterTypeId = e.NewsFilterType.Program;
                        }
                    }
                    requestData.ReporterId = appdata.currentUser().id;
                    if (parseInt($.trim(helper.getCookie('NewsTypeId')))) {
                        requestData.newsTypeId = parseInt($.trim(helper.getCookie('NewsTypeId')));
                    }
                }
                if (requestData.currentTicker || requestData.Tickers) {
                    var lines = requestData.Tickers;
                    var tickerObj = getTickerObject(requestData.currentTicker, requestData.Tickers, requestData);
                    requestData.Tickers = lines.length > 0 ? [tickerObj] : null;
                }

                return $.Deferred(function (d) {
                    $.when(dataservice.onsubmitNewsFile(requestData))
                        .done(function (responseData) {
                            if (check) {


                                if (requestData.isIframe) {

                                }
                                else {
                                    router.navigateTo(config.hashes.production.home);
                                }
                            }

                            d.resolve(responseData);
                        })
                        .fail(function (responseData) {
                            d.reject();
                        });
                }).promise();
            },

            onAssignmentsubmit = function (requestData) {
                return $.Deferred(function (d) {
                    $.when(dataservice.onAssignmentsubmit(requestData))
                        .done(function (responseData) { 
                        })
                        .fail(function (responseData) {
                            d.reject();
                });
                }).promise();
            },

            upload = function (requestData) {
                return $.Deferred(function (d) {
                    $.when(dataservice.upload(requestData))
                        .done(function (responseData) { 
                        })
                        .fail(function (responseData) {
                            d.reject();
                });
                }).promise();
            },

            LoadMyNewsList = function (requestData) {
                return $.Deferred(function (d) {
                    dc.news.clearAllNews(true);
                    dc.resources.clearAllNews(true);
                    $.when(dataservice.LoadMyNewsList(requestData))
                        .done(function (responseData) {
                            if (responseData.Data) {
                                var newsUpdates = _.filter(
                                    _.map(responseData.Data, function (item) {
                                        if (item.Updates && item.Updates.length > 0) {
                                            for (var i = 0; i < item.Updates.length; i++) {
                                                item.Updates[i].ParentNewsId = item._id;
                                                item.Updates[i].LanguageCode = item.LanguageCode;
                                        }
                                    }
                                        return item.Updates;
                                    }), function (items) {
                                        return (typeof items != 'undefined');
                            });

                                var news =[];
                                news = news.concat.apply(news, newsUpdates);

                                dc.news.fillData(news, { sort: true, groupByParentNewsId: true
                            });

                                saveNewsResources(responseData.Data);

                                saveFiltersNews(responseData.Data);

                                saveNewsComments(responseData.Data);

                                news = responseData.Data;
                                var sortednews =[];
                                var sortednews = _.sortBy(news, news.lastUpdateDate)

                                //if (sortednews && sortednews.length > 0 && sortednews[0].LastUpdateDateStr) {
                                //    appdata.newsFilterLastUpdateDateStr = sortednews[0].LastUpdateDateStr;
                                //}
                                dc.news.fillData(sortednews, true);
                        }
                            d.resolve(responseData);
                    })
                        .fail(function (responseData) {
                            d.reject(responseData);
                });
                }).promise();
            },

          checkUserSession = function () {
                 var userId = parseInt($.trim(helper.getCookie('user-id')));
                 if (userId != appdata.currentUser().id)
                     window.location.href = '/login#/index';

                 setTimeout(checkUserSession, 5000);
             },

          MyNewsPolling = function (reqObj) {
              return $.Deferred(function (d) {
                  $.when(dataservice.MyNewsPolling(reqObj))
                      .done(function (responseData) {
                          if (responseData.Data) {
                              var newsUpdates = responseData.Data; //_.filter(
                              //    _.map(responseData.Data, function (item) {
                              //        if (item.Updates && item.Updates.length > 0) {
                              //            for (var i = 0; i < item.Updates.length; i++) {
                              //                item.Updates[i].ParentNewsId = item._id;
                              //                item.Updates[i].LanguageCode = item.LanguageCode;
                              //            }
                              //        }
                              //        return item.Updates;
                              //    }), function (items) {
                              //        return (typeof items != 'undefined');
                              //    });

                              var news = [];
                              if (newsUpdates && newsUpdates.length) {
                                  news = news.concat.apply(news, newsUpdates);

                                  dc.news.fillData(news, {
                                      sort: true, groupByParentNewsId: true
                                  });

                                  saveNewsResources(responseData.Data);

                                  saveFiltersNews(responseData.Data);

                                  saveNewsComments(responseData.Data);

                                  news = responseData.Data;
                                  var sortednews = [];
                                  var sortednews = _.sortBy(news, news.lastUpdateDate)

                                  dc.news.fillData(sortednews, true);
                              }
                          }
                          d.resolve(responseData);
                      })
                      .fail(function (responseData) {
                          d.reject(responseData);
                      });
              }).promise();
          },



            reporterPolling = function () {
                return $.Deferred(function (d) {
                    var requestObj = {
                };
                    requestObj.NewsFilterLastUpdateDateStr = appdata.newsFilterLastUpdateDateStr;
                    requestObj.ReporterId = 1;

                    $.when(dataservice.reporterPolling(requestObj))
                        .done(function (responseData) {
                            if (responseData && responseData.IsSuccess) {
                                var newsFilters = responseData.Data.NewsFilters;
                                var newsComments = responseData.Data.Comments;

                                if (newsFilters) {
                                    saveNewsFilters(newsFilters);
                            }

                                if (newsComments) {
                                    if (newsComments && newsComments.length > 0) {
                                        dc.newsComments.fillData(newsComments, { sort: true, groupByNewsId: true
                                    });
                                }
                            }

                                d.resolve(responseData);
                            } else {
                                d.reject();
                        }
                    })
                        .fail(function (data) {
                            d.reject();
                })
                }).promise();
            },

            saveNewsWithUpdates = function (data) {
                var newsUpdates = _.filter(
                    _.map(data, function (item) {
                        if (item.Updates && item.Updates.length > 0) {
                            for (var i = 0; i < item.Updates.length; i++) {
                                item.Updates[i].ParentNewsId = item._id;
                                item.Updates[i].LanguageCode = item.LanguageCode;
                        }
                    }
                        return item.Updates;
                    }), function (items) {
                        return (typeof items != 'undefined');
            });
                var news =[];
                news = news.concat.apply(news, newsUpdates);
                news = news.concat.apply(news, data);
                //var sortednews = [];
                //var sortednews = _.sortBy(news, news.lastUpdateDate)
                dc.news.fillData(news, {
                sort: true
            });
            },

            saveNewsResources = function (data) {
                _.map(_.filter(data, function (news) {
                    return (news.Resources.length > 0);
                }), function (item) {
                    if (item.Resources.length > 0) {
                        dc.resources.fillData(item.Resources, { newsId: item._id
                    });
                }
            });
            },

            saveFiltersNews = function (data) {
                _.map(_.filter(data, function (news) {
                    return (news.Filters.length > 0);
                }), function (item) {
                    if (item.Filters && item.Filters.length > 0) {
                        var arr =[];
                        for (var i = 0; i < item.Filters.length; i++) {
                            var obj = new NewsFilter();
                            obj.NewsId = item._id;
                            obj.FilterId = item.Filters[i].FilterId;
                            obj._id = item.Filters[i]._id;
                            obj.CreationDateStr = item.Filters[i].CreationDateStr;
                            arr.push(obj);
                    }

                        appdata.newsFilterLastUpdateDateStr = dc.newsFilters.fillData(arr, { newsId: item._id
                    });
                }
            });
            },

            saveNewsComments = function (data) {
                _.map(_.filter(data, function (news) {
                    return (news.Comments && news.Comments.length > 0);
                }), function (item) {
                    if (item.Comments && item.Comments.length > 0) {
                        var arr =[];
                        for (var i = 0; i < item.Comments.length; i++) {
                            var obj = new Comment();
                            obj.NewsId = item._id;
                            obj.Comment = item.Comments[i].Comment;
                            obj.UserId = item.Comments[i].UserId;
                            obj.CommentTypeId = item.Comments[i].CommentTypeId;
                            obj.LastUpdateDateStr = item.Comments[i].LastUpdateDateStr;
                            obj._id = item.Comments[i]._id;
                            arr.push(obj);
                    }

                        dc.newsComments.fillData(arr, { newsId: item._id
                    });
                }
            });
            },

            GetNews = function (requestData) {
                return $.Deferred(function (d) {
                    $.when(dataservice.GetNews(requestData))
                        .done(function (responseData) {
                            if (responseData.IsSuccess) {
                                dc.news.fillData([responseData.Data]);
                        }
                            d.resolve(responseData);
                    })
                        .fail(function (responseData) {
                            d.reject();
                });
                }).promise();
            },

            updateNews = function (requestData) {
                return $.Deferred(function (d) {
                    $.when(dataservice.updateNews(requestData))
                        .done(function (responseData) {
                            if (responseData.Data) {
                                d.resolve(responseData);
                        }
                    })
                        .fail(function (responseData) {
                            d.reject();
                });
                }).promise();
            },

            getNewsByTerm = function (requestData) {
                var obj = { id: requestData
            };
                return $.Deferred(function (d) {
                    $.when(dataservice.getNewsByTerm(obj))
                        .done(function (responseData) {
                            d.resolve(responseData);
                    })
                        .fail(function (responseData) {
                            d.reject(responseData);
                });
                }).promise();
            },

        // #endregion

        //#region Verifier

            getNewsWithUpdates = function (requestData) {
                return $.Deferred(function (d) {
                    $.when(dataservice.getNewsWithUpdates(requestData))
                    .done(function (responseData) {
                        saveNewsData(responseData.Data);
                        for (var a = 0; a < responseData.Data.length; a++) {
                            dc.news.fillData([responseData.Data[a]]);
                    }
                        d.resolve();
                        return responseData;
                    })
                    .fail(function (responseData) {
                        d.resolve();
                });
                }).promise();
            },

            markNewsVerified = function (data) {
                return $.Deferred(function (d) {
                    $.when(dataservice.markNewsVerified(data))
                        .done(function () {
                            d.resolve();
                    })
                        .fail(function () {
                            d.reject();
                });
                }).promise();
            },

            insertNewsComments = function (data) {
                return $.Deferred(function (d) {
                    $.when(dataservice.insertNewsComments(data))
                        .done(function () {
                            d.resolve();
                    })
                        .fail(function () {
                            d.reject();
                });
                }).promise();
            },

            verificationLoadInitialData = function (filterArray) {
                return $.Deferred(function (d) {
                    var requestObj = getVerificationLongPollingData(filterArray);
                    //console.log(requestObj);
                    $.when(dataservice.verificationLoadInitialData(requestObj))
                        .done(function (responseData) {
                            //console.log(responseData);
                            if (responseData && responseData.IsSuccess && responseData.Data) {
                                populateFilters(responseData.Data.Filters);
                                updateFilterCounts(responseData.Data.FilterCounts);
                                saveNewsData(responseData.Data.News);
                                //saveNewsFilters(responseData.Data.NewsFilters);
                                //saveChannelsData(responseData.Data.Channels);
                                //saveProgramsData(responseData.Data.Programs);
                                //saveScreenElements(responseData.Data.ScreenElements);

                                //appdata.currentProgram(dc.programs.getAllLocal()[0]);

                                setDataChangeTime();
                                //displayStats();

                                d.resolve();
                            } else {
                                d.reject();
                        }
                    })
                        .fail(function (data) {
                            d.reject();
                })
                }).promise();
            },

            verificationPolling = function (filterArray) {
                return $.Deferred(function (d) {
                    var requestObj = getVerificationLongPollingData(filterArray);

                    $.when(dataservice.producerPolling(requestObj))
                        .done(function (responseData) {
                            if (responseData && responseData.IsSuccess) {
                                var news = responseData.Data.News;
                                var filterCounts = responseData.Data.FilterCounts;
                                var newsFilters = responseData.Data.NewsFilters;

                                var newsLength = dc.news.getAllLocal().length;

                                saveNewsData(news);
                                updateFilterCounts(filterCounts);
                                saveNewsFilters(newsFilters);

                                if ((news && news.length > 0 && dc.news.getAllLocal().length !== newsLength) || (newsFilters && newsFilters.length > 0))
                                    setDataChangeTime();

                                displayStats();

                                d.resolve(responseData);
                            } else {
                                d.reject();
                        }
                    })
                        .fail(function (data) {
                            d.reject();
                })
                }).promise();
            },

            getMoreVerification = function (filters) {
                return $.Deferred(function (d) {
                    var filterArrayObject =[];

                    for (a = 0; a < filters.length; a++)
                        filterArrayObject.push(filters[a]);

                    if (appdata.isAllNewsFilterSelected()) {
                    }
                    else {
                        for (var i = 0; i < appdata.selectedExtraFilters().length ; i++) {
                            var temp = {
                        };

                            var currentFilter = dc.filters.getLocalById(appdata.selectedExtraFilters()[i]);
                            temp.FilterTypeId = currentFilter.filterTypeId;
                            temp.FilterId = currentFilter.id;
                            filterArrayObject.push(temp);
                    }
                        for (var i = 0; i < appdata.selectedCategoryFilters().length ; i++) {
                            var temp = {
                        };
                            var currentFilter = dc.filters.getLocalById(appdata.selectedCategoryFilters()[i]);
                            temp.FilterTypeId = currentFilter.filterTypeId;
                            temp.FilterId = currentFilter.id;
                            filterArrayObject.push(temp);
                    }

                        for (var i = 0; i < appdata.selectedSourceFilters().length ; i++) {
                            var temp = {
                        };
                            var currentFilter = dc.filters.getLocalById(appdata.selectedSourceFilters()[i]);
                            temp.FilterTypeId = currentFilter.filterTypeId;
                            temp.FilterId = currentFilter.id;
                            filterArrayObject.push(temp);
                    }
                }

                    var requestObj = {
                            Filters: filterArrayObject,
                            PageCount: appdata.lpPageCount,
                            StartIndex: appdata.lpStartIndex,
                            FromStr: appdata.lpFromDate,
                            ToStr: moment().toISOString(),
                            Term: appdata.searchKeywords
                };
                    $.when(dataservice.getMoreNews(requestObj))
                        .done(function (responseData) {
                            if (responseData && responseData.IsSuccess && responseData.Data) {
                                var news = responseData.Data.News;

                                saveNewsData(news);

                                if (news && news.length > 0)
                                    setDataChangeTime();

                                displayStats();

                                d.resolve();
                            } else {
                                d.reject();
                        }
                    })
                        .fail(function (data) {
                            d.reject();
                })
                }).promise();
            },

            getMoreNewsVerification = function (data) {
                return $.Deferred(function (d) {
                    var filterArrayObject =[];

                    if (appdata.isAllNewsFilterSelected()) {
                        filterArrayObject.push({ FilterTypeId: 16, FilterId: 80
                    });
                    }
                    else {
                        for (var i = 0; i < appdata.selectedExtraFilters().length ; i++) {
                            var temp = {
                        };

                            var currentFilter = dc.filters.getLocalById(appdata.selectedExtraFilters()[i]);
                            temp.FilterTypeId = currentFilter.filterTypeId;
                            temp.FilterId = currentFilter.id;
                            filterArrayObject.push(temp);
                    }
                        for (var i = 0; i < appdata.selectedCategoryFilters().length ; i++) {
                            var temp = {
                        };
                            var currentFilter = dc.filters.getLocalById(appdata.selectedCategoryFilters()[i]);
                            temp.FilterTypeId = currentFilter.filterTypeId;
                            temp.FilterId = currentFilter.id;
                            filterArrayObject.push(temp);
                    }

                        for (var i = 0; i < appdata.selectedSourceFilters().length ; i++) {
                            var temp = {
                        };
                            var currentFilter = dc.filters.getLocalById(appdata.selectedSourceFilters()[i]);
                            temp.FilterTypeId = currentFilter.filterTypeId;
                            temp.FilterId = currentFilter.id;
                            filterArrayObject.push(temp);
                    }
                }

                    var requestObj = {
                            Filters: filterArrayObject,
                            PageCount: appdata.lpPageCount,
                            StartIndex: appdata.lpStartIndex,
                            FromStr: appdata.lpFromDate,
                            ToStr: moment().toISOString(),
                            Term: appdata.searchKeywords
                };
                    $.when(dataservice.getMoreNews(requestObj))
                        .done(function (responseData) {
                            if (responseData && responseData.IsSuccess && responseData.Data) {
                                var news = responseData.Data.News;

                                saveNewsData(news);

                                if (news && news.length > 0)
                                    setDataChangeTime();

                                displayStats();

                                d.resolve();
                            } else {
                                d.reject();
                        }
                    })
                        .fail(function (data) {
                            d.reject();
                })
                }).promise();
            },

            toggleFilterVerification = function (data, isExtraFilter) {
                if (data.filterTypeId && data.filterTypeId === e.NewsFilterType.Category) {
                    isExtraFilter = false;
                    toggleFilterInsertionVerification(appdata.selectedCategoryFilters(), data, appdata.selectedParentFilterIdCategory, isExtraFilter);
                }
                else if (data.filterTypeId && data.filterTypeId !== e.NewsFilterType.Category) {
                    isExtraFilter = false;
                    toggleFilterInsertionVerification(appdata.selectedSourceFilters(), data, appdata.selectedParentFilterIdSource);
                }
                else if (isExtraFilter) {
                    var filterObject;
                    filterObject = dc.filters.getLocalById(data);
                    toggleFilterInsertionVerification(appdata.selectedExtraFilters(), filterObject, appdata.selectedParentFilterIdExtra, isExtraFilter);
            }

                if (appdata.selectedCategoryFilters().length <= 0 && appdata.selectedSourceFilters().length <= 0 && appdata.selectedExtraFilters().length <= 0)
                    appdata.isAllNewsFilterSelected(true);
                else
                    appdata.isAllNewsFilterSelected(false);
            },

            toggleFilterInsertionVerification = function (appDataArrayProperty, data, appdataParentIdHolder, isExtraFilter) {
                //var filterIndex = appDataArrayProperty.indexOf(80);
                //if (filterIndex === -1)
                //    appDataArrayProperty.push(80);

                if (data.parentId === -1) {
                    filterIndex = appDataArrayProperty.indexOf(data.id);
                    if (filterIndex === -1) {
                        appDataArrayProperty.push(data.id);
                        appdataParentIdHolder(data.id);
                    }
                    else {
                        appDataArrayProperty.splice(filterIndex, 1);
                        appdataParentIdHolder(0);
                }
                }
                else if (data.parentId != -1) {
                    var isParentSelected = appdataParentIdHolder() > 0 ? true: false;
                    filterIndex = appDataArrayProperty.indexOf(data.id);
                    if (filterIndex === -1) {
                        appDataArrayProperty.push(data.id);
                        if (isParentSelected && appDataArrayProperty.indexOf(appdataParentIdHolder()) != -1)
                            appDataArrayProperty.splice(appDataArrayProperty.indexOf(appdataParentIdHolder()), 1);
                    }
                    else {
                        appDataArrayProperty.splice(filterIndex, 1);
                        if (isParentSelected && appDataArrayProperty.indexOf(appdataParentIdHolder()) == -1 && appDataArrayProperty.length <= 0)
                            appDataArrayProperty.push(appdataParentIdHolder());
                }
            }
            },
            populateUserFilter = function (filters) {
                filters = _.sortBy(filters, function (item) {
                    return !(typeof item.ParentId == 'undefined');
            });

                if (filters && filters.length > 0) {
                    for (var i = 0; i < filters.length; i++) {
                        var filterObj = dc.UserFilter.add(filters[i], false);
                        if (filterObj.parentId !== -1) {
                            var parentFilter = dc.UserFilter.getLocalById(filterObj.parentId);
                            if (parentFilter) {
                                parentFilter.children.push(filterObj);
                        }
                    }
                }
                    dc.UserFilter.addToObservableList(dc.UserFilter.getAllLocal());
            }
            },
            submitMetaDataByUser = function (data) {
                return $.Deferred(function (d) {
                    $.when(dataservice.submitMetaDataByUser(data))
                       .done(function (responseData) {
                           //console.log("success");
                           d.resolve(responseData);
                    })
                       .fail(function (responseData) {
                           d.reject();
                });
                }).promise();
            },
            getAllFilters = function () {
                return $.Deferred(function (d) {
                    $.when(dataservice.getAllFilters())
                        .done(function (responseData) {
                            populateUserFilter(responseData.Data);
                            //console.log("success");
                            d.resolve(responseData);
                    })
                        .fail(function (responseData) {
                            d.reject();
                });
                }).promise();
            },
            getVerificationLongPollingData = function (filterArray) {
                var news = dc.news.getAllLocal();
                var toIndex = appdata.lpNewsIndex +appdata.lpPageCount;

                if (toIndex > news.length) {
                    toIndex = news.length;
            }

                var news = news.slice(appdata.lpNewsIndex, toIndex);

                if (toIndex > news.length) {
                    appdata.lpNewsIndex = 0;
                } else {
                    appdata.lpNewsIndex = appdata.lpNewsIndex +appdata.lpPageCount;
            }

                var newsIds =[];

                for (var i = 0; i < news.length; i++) {
                    newsIds.push(news[i].id);
            }

                var filterArrayObject =[];

                if (appdata.isAllNewsFilterSelected()) {
                    for (a = 0; a < filterArray.length; a++)
                        filterArrayObject.push(filterArray[a]);
                } else {
                    for (var i = 0; i < appdata.selectedCategoryFilters().length ; i++) {
                        var temp = {
                    };
                        var currentFilter = dc.filters.getLocalById(appdata.selectedCategoryFilters()[i]);
                        temp.FilterTypeId = currentFilter.filterTypeId;
                        temp.FilterId = currentFilter.id;
                        filterArrayObject.push(temp);
                }

                    for (var i = 0; i < appdata.selectedSourceFilters().length ; i++) {
                        var temp = {
                    };
                        var currentFilter = dc.filters.getLocalById(appdata.selectedSourceFilters()[i]);
                        temp.FilterTypeId = currentFilter.filterTypeId;
                        temp.FilterId = currentFilter.id;
                        filterArrayObject.push(temp);
                }
                    for (var i = 0; i < appdata.selectedExtraFilters().length ; i++) {
                        var temp = {
                    };
                        var currentFilter = dc.filters.getLocalById(appdata.selectedExtraFilters()[i]);
                        temp.FilterTypeId = currentFilter.filterTypeId;
                        temp.FilterId = currentFilter.id;
                        filterArrayObject.push(temp);
                }
            }

                var requestObj = {
                        Filters: filterArrayObject,
                        PageCount: appdata.lpPageCount,
                        StartIndex: appdata.lpStartIndex,
                        FromStr: appdata.lpFromDate,
                        ToStr: appdata.lpToDate,
                        NewsFilterLastUpdateDateStr: appdata.newsFilterLastUpdateDateStr,
                        NewsLastUpdateDateStr: appdata.newsLastUpdateDate,
                    NewsIds: newsIds,
                        UserId: appdata.currentUser().id,
                        UserRole: appdata.currentUser().userType,
                        SlotScreenTemplateId: appdata.currentStoryTemplateId,
                        CommentsLastUpdateStr: appdata.storyTemplateCommentLastUpdateDate,
                        NotificationsLastUpdateDateStr: appdata.lpNotificationsLastUpdateDate,
                        AlertsLastUpdateDateStr: appdata.lpAlertsLastUpdateDate,
                        Token: appdata.videoEditorToken
            };

                return requestObj;
    };

        // #endregion

        return {
                loadInitialData: loadInitialData,
                producerPolling: producerPolling,
                verificationPolling: verificationPolling,
                getMoreNewsVerification: getMoreNewsVerification,
                getMoreVerification: getMoreVerification,
                getNewsBunch: getNewsBunch,
                getNewsBunchWithCount: getNewsBunchWithCount,
                getMoreNews: getMoreNews,
                getNewsByIds: getNewsByIds,
                getFilteredBunches: getFilteredBunches,
            Package: Package,
            upload: upload,
            onsubmit: onsubmit,
                onAssignmentsubmit: onAssignmentsubmit,
                deletevideofile: deletevideofile,
                LoadMyNewsList: LoadMyNewsList,
            GetNews: GetNews,
                updateNews: updateNews,
                verificationLoadInitialData: verificationLoadInitialData,
                reporterPolling: reporterPolling,
                reporterLoadInitialData: reporterLoadInitialData,
                markNewsVerified: markNewsVerified,
                insertNewsComments: insertNewsComments,
                toggleFilter: toggleFilter,
                toggleFilterVerification: toggleFilterVerification,
                setDataChangeTime: setDataChangeTime,
                getNewsByTerm: getNewsByTerm,
                selectNews: selectNews,
                getNewsWithUpdates: getNewsWithUpdates,
                saveProgramsData: saveProgramsData,
                getBySearchKeyWord: getBySearchKeyWord,
                LogOnce: LogOnce,
                submitMetaDataByUser: submitMetaDataByUser,
                getAllFilters: getAllFilters,
                addToNewsBucket: addToNewsBucket,
                saveResources: saveResources,
                loadInitialDataNMS: loadInitialDataNMS,
                onsubmitNewsFile: onsubmitNewsFile,
                MyNewsPolling: MyNewsPolling,
                UpdateNewsFileWithHistory: UpdateNewsFileWithHistory,
                saveNewsResources: saveNewsResources
        }
    });