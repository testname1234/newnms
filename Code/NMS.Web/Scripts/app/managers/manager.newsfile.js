﻿define('manager.newsfile',
    [
        'jquery',
        'enum',
        'dataservice',
        'datacontext',
        'underscore',
        'utils',
        'appdata',
        'config',
        'model',
        'model.mapper',
        'presenter',
        'router',
        'helper'
    ],
    function ($, e, dataservice, dc, _, utils, appdata, config, model, mapper, presenter, router, helper) {

        var
            // #region Local Data Processing



            // #endregion

            // #region Remote Calls
              getNewsStatisticParams = function (folderId) {
                  if (folderId) {
                      var programId = dc.newsFolders.getLocalById(folderId).programId;
                      var programName = dc.newsFolders.getLocalById(folderId).name;
                      var EpisodeId = dc.newsFolders.getLocalById(folderId).episodeId;
                      var EpisodeStartTimeStr = dc.newsFolders.getLocalById(folderId).startTime;
                      var EpisodeEndTimeStr = dc.newsFolders.getLocalById(folderId).endTime;

                      return {
                          ProgramId: programId,
                          ProgramName: programName,
                          EpisodeId: EpisodeId,
                          EpisodeStartTimeStr: EpisodeStartTimeStr,
                          UserId: appdata.currentUser().id,
                          EpisodeEndTimeStr: EpisodeEndTimeStr
                      };
                  }
              },

           loadInitialData = function () {
               var folderIds = getFolderIds();

               var reqObj = {
                   WorkRoleId: appdata.currentUser().userType,
                   UserId: appdata.currentUser().id,
                   FolderIds: folderIds,
                   PageSize: appdata.lpPageCount,
                   PageOffset: appdata.lpStartOffSet
               }
               return $.Deferred(function (d) {
                   $.when(dataservice.getAllNewsFile(reqObj))
                       .done(function (responseData) {
                           if (responseData && responseData.IsSuccess && responseData.Data) {

                               saveProgramData(responseData.Data.Programs);
                               generateUIForFolders();
                               //saveNewsFiles(responseData.Data.NewsFile, true);

                               appdata.refreshNewsFileList(!appdata.refreshNewsFileList());
                               appdata.refreshGenericList(!appdata.refreshGenericList());

                               d.resolve();
                           } else {
                               d.reject();
                           }
                       })
                       .fail(function (data) {
                           d.reject();
                       })
               }).promise();
           },

           OrganizationTagsUpdate = function (requestData) {
               return $.Deferred(function (d) {
                   $.when(dataservice.OrganizationTagsUpdate(requestData))
                       .done(function (responseData) {
                           d.resolve(responseData);
                       })
                       .fail(function (responseData) {
                           d.reject();
                       });
               }).promise();
           },
           //

           GetNewsFileForTaggReport = function (dt) {
               var date = null;
               if (dt)
                   date = dt;
               return $.Deferred(function (d) {
                   $.when(dataservice.GetNewsFileForTaggReport(date))
                       .done(function (responseData) {
                           dc.newsFiles.fillData(responseData.Data);
                           d.resolve(responseData);
                       })
                       .fail(function (responseData) {
                           d.reject();
                       });
               }).promise();
           },


           newsFilePolling = function () {
               var reqObj = {
                   WorkRoleId: appdata.currentUser().userType,
                   NewsFileLastUpdateStr: appdata.newsFilesLastUpdateDate,
                   FileDetailLastUpdateStr: appdata.newsFileDetailsLastUpdateDate,
                   FileStatusHistoryLastUpdateStr: appdata.newsFileStatusHistoryLastUpdateDate,
                   FileFolderHistoryLastUpdateStr: appdata.newsFileFolderHistoryLastUpdateDate,
                   FileResourceLastUpdateStr: appdata.newsFileResourcesLastUpdateDate
               }
               return $.Deferred(function (d) {
                   $.when(dataservice.getAllNewsFile(reqObj))
                       .done(function (responseData) {
                           if (responseData && responseData.IsSuccess && responseData.Data) {
                               saveNewsFileFolderHistory(responseData.Data.FileFolderHistory, true);
                               saveNewsFileDetails(responseData.Data.FileDetail, true);
                               saveNewsFileStatusHistory(responseData.Data.FileStatusHistory, true);
                               saveNewsFolders(responseData.Data.Folder, true);
                               saveNewsFiles(responseData.Data.NewsFile, true);
                               saveNewsFileResources(responseData.Data.FileResource, true);
                               saveResources(responseData.Data.FileResource, true);
                               removeObsoleteFolderData();
                               d.resolve();
                           } else {
                               d.reject();
                           }
                       })
                       .fail(function (data) {
                           d.reject();
                       })
               }).promise();
           },

             newsFileFilterSelected = function () {
                 return $.Deferred(function (d) {
                     appdata.isFilterPollingClick(true);
                     var requestObj = getSelectedFiltersRequestData();
                     if (requestObj.Filters && requestObj.Filters.length == 1 && requestObj.Filters[0].FilterId === 78) {
                     }
                     else {
                         appdata.cachedFilters(requestObj.Filters);
                         document.cookie = "CachedFilters" + "=" + JSON.stringify(appdata.cachedFilters());
                     }


                     $.when(dataservice.newsFileProducerPolling(requestObj))
                         .done(function (responseData) {
                             appdata.isFilterPollingClick(false);

                             if (responseData && responseData.IsSuccess) {
                                 var filterCounts = responseData.Data.FilterCounts;
                                 var newsFilters = responseData.Data.NewsFilters;
                                 saveNewsFiles(responseData.Data.NewsFile, true);
                                 appdata.refreshGenericList(!appdata.refreshGenericList());
                                 appdata.refreshNewsFileList(!appdata.refreshNewsFileList());
                                 if (responseData.Data.Filters)
                                     populateFilters(responseData.Data.Filters);

                                 d.resolve(responseData);
                             } else {
                                 d.reject();
                             }
                         })
                         .fail(function (data) {
                             d.reject();
                         })
                 }).promise();
             },
             newsFileProducerPolling = function (isScrollOffset) {
                 return $.Deferred(function (d) {
                     if (!isScrollOffset)
                         appdata.lpStartOffSet = 0;

                     presenter.toggleActivity(true);
                     var requestObj = getLongPollingData();
                     updateProgramSlotTime();
                     checkUserSession();
                     //appdata.isFilterPollingClick(true);
                     if (parseInt(moment().diff(appdata.lpToDate, 'days')) == 0) {
                         $.when(dataservice.newsFileProducerPolling(requestObj))
                             .done(function (responseData) {
                                 presenter.toggleActivity(false);
                                 updateProgramIntervals();
                                 //appdata.isFilterPollingClick(false);
                                 if (responseData && responseData.IsSuccess) {

                                     var filterCounts = responseData.Data.FilterCounts;
                                     var newsFilters = responseData.Data.NewsFilters;
                                     saveNewsFiles(responseData.Data.NewsFile, true);

                                     _.filter(dc.newsFiles.getAllLocal(), function (obj) {
                                         return obj.updateDisplayDate(!obj.updateDisplayDate());
                                        });

                                     if (responseData.Data.NewsFile && responseData.Data.NewsFile.length > 0) {
                                        appdata.refreshNewsFileList(!appdata.refreshNewsFileList());
                                     }
                                      


                                     if (appdata.selectedNewsFile()) {
                                         appdata.selectedNewsFile().updateDisplayDate(!appdata.selectedNewsFile().updateDisplayDate());
                                     }

                                     if (responseData.Data.Filters)
                                         populateFilters(responseData.Data.Filters);

                                     d.resolve(responseData);
                                 } else {
                                     d.reject();
                                 }
                             })
                             .fail(function (data) {
                                 d.reject();
                             })
                     }

                 }).promise();
             },
             sortDescFilters = function (arr) {
                 if (arr && arr.length > 1) {
                     arr = arr.sort(function (entityA, entityB) {
                         return new Date(entityB.LastUpdateDateStr) - new Date(entityA.LastUpdateDateStr);
                     });
                 }
                 return arr;
             },
             checkUserSession = function () {
                 var userId = parseInt($.trim(helper.getCookie('user-id')));
                 if (userId != appdata.currentUser().id)
                     sessionClosed();

             },

             sessionClosed = function () {
                window.location.href = '/login#/index';
            },
            populateFilters = function (filters) {
                filters = _.sortBy(filters, function (item) {
                    return !(typeof item.ParentId == 'undefined');
                });

                var clonedArray = JSON.parse(JSON.stringify(filters));
                var temp = sortDescFilters(clonedArray);

                if (temp && temp.length > 0)
                    appdata.filterLastUpdateDateStr(temp[0].LastUpdateDateStr);

                if (filters && filters.length > 0) {
                    for (var i = 0; i < filters.length; i++) {
                        var filterObj = dc.filters.add(filters[i], false);
                        if (filterObj.parentId !== -1) {
                            var parentFilter = dc.filters.getLocalById(filterObj.parentId);
                            if (parentFilter)
                                parentFilter.childrenIds.push(filterObj.id);
                        }
                    }
                    dc.filters.addToObservableList(dc.filters.getAllLocal());
                }
            },
           getFolderIds = function () {
               var folderIds = [];
               if (appdata.foldersList()) {
                   for (var i = 0; i < appdata.foldersList().length ; i++) {
                       if (parseInt(appdata.foldersList()[i].FolderId) != 0)
                           folderIds.push(parseInt(appdata.foldersList()[i].FolderId));
                   }
               }
               return folderIds;
           },

          getSelectedFiltersRequestData = function () {

              var folderIds = getFolderIds();

              var filterArrayObject = [];

              if (appdata.isAllNewsFilterSelected()) {
                  if (appdata.currentUser().userType === e.UserType.filterSocial) {
                      filterArrayObject.push({ FilterTypeId: 3, FilterId: 24 });
                  }
                  else {
                      filterArrayObject.push({ FilterTypeId: 16, FilterId: 78 });
                  }

              } else {
                  for (var i = 0; i < appdata.selectedCategoryFilters().length ; i++) {
                      var temp = {
                      };
                      var currentFilter = dc.filters.getLocalById(appdata.selectedCategoryFilters()[i]);
                      temp.FilterTypeId = currentFilter.filterTypeId;
                      temp.FilterId = currentFilter.id;
                      filterArrayObject.push(temp);
                  }

                  for (var i = 0; i < appdata.selectedSourceFilters().length ; i++) {
                      var temp = {
                      };
                      var currentFilter = dc.filters.getLocalById(appdata.selectedSourceFilters()[i]);
                      temp.FilterTypeId = currentFilter.filterTypeId;
                      temp.FilterId = currentFilter.id;
                      filterArrayObject.push(temp);
                  }
                  for (var i = 0; i < appdata.selectedExtraFilters().length ; i++) {
                      var temp = {
                      };
                      var currentFilter = dc.filters.getLocalById(appdata.selectedExtraFilters()[i]);
                      temp.FilterTypeId = currentFilter.filterTypeId;
                      temp.FilterId = currentFilter.id;
                      filterArrayObject.push(temp);
                  }

                  if (appdata.selectedSourceFilters().length == 0 && !appdata.isAllNewsFilterSelected() && appdata.currentUser().userType != e.UserType.filterSocial) {
                      filterArrayObject.push({ FilterTypeId: 16, FilterId: 78 });
                  }
                  if (appdata.currentUser().userType === e.UserType.filterSocial) {
                      filterArrayObject.push({ FilterTypeId: 3, FilterId: 24 });
                  }
              }

              var filtersToDiscard = [{ FilterTypeId: 3, FilterId: 25 }, { FilterTypeId: 3, FilterId: 26 }, { FilterTypeId: 13, FilterId: 76 }];

              if (appdata.selectedSourceFilters().indexOf(25) > 0) {
                  filtersToDiscard = [];
              }

              var tickerUpdateDate = null;
              var tickerLineUpdateDate = null;
              var userId = null;
              if (appdata.currentUser().userType === e.UserType.TickerProducer) {
                  tickerUpdateDate = appdata.tickerLastUpdateDate
                  tickerLineUpdateDate = appdata.tickerLineLastUpdateDate;

              }
              //if (appdata.lpFromDate)
              //{
              //    debugger
              //    var date1 = appdata.lpFromDate;
                  

              //}
              //if (appdata.lpToDate)
              //{

              //}
              var requestObj = {
                  Filters: filterArrayObject,
                  FromStr: appdata.lpFromDate,
                  ToStr: appdata.lpToDate,
                  NewsFilterLastUpdateDateStr: appdata.newsFilterLastUpdateDateStr,
                  NewsLastUpdateDateStr: appdata.newsFilesLastUpdateDate,
                  UserId: appdata.currentUser().id,
                  FiltersToDiscard: filtersToDiscard,
                  ProgramFilterIds: appdata.programFilterIds,
                  FilterLastUpdateDateStr: appdata.filterLastUpdateDateStr(),
                  FolderIds: folderIds,
                  PageSize: appdata.lpPageCount,
                  PageOffset: 0,
                  Term: appdata.searchKeywords
              };

              return requestObj;
          },
           getLongPollingData = function () {



               var filterArrayObject = [];

               if (appdata.isAllNewsFilterSelected()) {
                   if (appdata.currentUser().userType === e.UserType.filterSocial) {
                       filterArrayObject.push({ FilterTypeId: 3, FilterId: 24 });
                   }
                   else {
                       filterArrayObject.push({ FilterTypeId: 16, FilterId: 78 });
                   }

               } else {
                   for (var i = 0; i < appdata.selectedCategoryFilters().length ; i++) {
                       var temp = {};
                       var currentFilter = dc.filters.getLocalById(appdata.selectedCategoryFilters()[i]);
                       temp.FilterTypeId = currentFilter.filterTypeId;
                       temp.FilterId = currentFilter.id;
                       filterArrayObject.push(temp);
                   }

                   for (var i = 0; i < appdata.selectedSourceFilters().length ; i++) {
                       var temp = {};
                       var currentFilter = dc.filters.getLocalById(appdata.selectedSourceFilters()[i]);
                       temp.FilterTypeId = currentFilter.filterTypeId;
                       temp.FilterId = currentFilter.id;
                       filterArrayObject.push(temp);
                   }
                   for (var i = 0; i < appdata.selectedExtraFilters().length ; i++) {
                       var temp = {};
                       var currentFilter = dc.filters.getLocalById(appdata.selectedExtraFilters()[i]);
                       temp.FilterTypeId = currentFilter.filterTypeId;
                       temp.FilterId = currentFilter.id;
                       filterArrayObject.push(temp);
                   }

                   if (appdata.selectedSourceFilters().length == 0 && !appdata.isAllNewsFilterSelected() && appdata.currentUser().userType != e.UserType.filterSocial) {
                       filterArrayObject.push({ FilterTypeId: 16, FilterId: 78 });
                   }
                   if (appdata.currentUser().userType === e.UserType.filterSocial) {
                       filterArrayObject.push({ FilterTypeId: 3, FilterId: 24 });
                   }
               }

               var filtersToDiscard = [{ FilterTypeId: 3, FilterId: 25 }, { FilterTypeId: 3, FilterId: 26 }, { FilterTypeId: 13, FilterId: 76 }];

               if (appdata.selectedSourceFilters().indexOf(25) > 0) {
                   filtersToDiscard = [];
               }

               var tickerUpdateDate = null;
               var tickerLineUpdateDate = null;
               var userId = null;
               if (appdata.currentUser().userType === e.UserType.TickerProducer) {
                   tickerUpdateDate = appdata.tickerLastUpdateDate
                   tickerLineUpdateDate = appdata.tickerLineLastUpdateDate;

               }
               var folderIds = getFolderIds();
               appdata.cachedFilters(filterArrayObject);
               var requestObj = {
                   Filters: filterArrayObject,
                   //FromStr: appdata.lpFromDate,
                   //ToStr: appdata.lpToDate,
                   NewsFilterLastUpdateDateStr: appdata.newsFilterLastUpdateDateStr,
                   NewsLastUpdateDateStr: appdata.newsFilesLastUpdateDate,
                   UserId: appdata.currentUser().id,
                   FiltersToDiscard: filtersToDiscard,
                   ProgramFilterIds: appdata.programFilterIds,
                   FilterLastUpdateDateStr: appdata.filterLastUpdateDateStr(),
                   FolderIds: folderIds,
                   Term: appdata.searchKeywords,
                   PageSize: appdata.lpPageCount,
                   PageOffset: appdata.lpStartOffSet,
                   WorkRoleId: appdata.currentUser().userType
               };

               return requestObj;
           },
           updateProgramSlotTime = function () {

           },
           sortDesc = function (arr) {
               if (arr && arr.length > 1) {
                   arr = arr.sort(function (entityA, entityB) {
                       return new Date(entityB.CreationDateStr) - new Date(entityA.CreationDateStr);
                   });
               }
               return arr;
           },
           sortDescByLastUpdateDate = function (arr) {
               if (arr && arr.length > 1) {
                   arr = arr.sort(function (entityA, entityB) {
                       return new Date(entityB.LastUpdateDateStr) - new Date(entityA.LastUpdateDateStr);
                   });
               }
               return arr;
           },
           saveNewsFileFolderHistory = function (data, isFromPollingOrInitial) {
               if (data && data.length > 0) {
                   var temp = sortDesc(data);
                   if (isFromPollingOrInitial) {
                       appdata.newsFileFolderHistoryLastUpdateDate = temp[0].CreationDateStr || appdata.newsFileFolderHistoryLastUpdateDate;
                   }
                   dc.newsFileFolderHistory.fillData(data, { groupByNewsFileId: true, sort: true });
               }
           },
           saveNewsFileDetails = function (data, isFromPollingOrInitial) {
               if (data && data.length > 0) {
                   var temp = sortDesc(data);
                   if (isFromPollingOrInitial) {
                       appdata.newsFileDetailsLastUpdateDate = temp[0].CreationDateStr || appdata.newsFileDetailsLastUpdateDate;
                   }
                   dc.newsFileDetails.fillData(data, { groupByNewsFileId: true });

               }
           },
           saveNewsFileStatusHistory = function (data, isFromPollingOrInitial) {
               if (data && data.length > 0) {
                   var temp = sortDesc(data);
                   if (isFromPollingOrInitial) {
                       appdata.newsFileStatusHistoryLastUpdateDate = temp[0].CreationDateStr || appdata.newsFileStatusHistoryLastUpdateDate;
                   }
                   dc.newsFileStatusHistory.fillData(data, { groupByNewsFileId: true });


               }
           },
           saveNewsFolders = function (data, isFromPollingOrInitial) {
               if (data && data.length > 0) {
                   var temp = sortDesc(data);
                   if (isFromPollingOrInitial) {
                       appdata.newsFoldersLastUpdateDate = temp[0].CreationDateStr || appdata.newsFoldersLastUpdateDate;
                   }
                   dc.newsFolders.fillData(data, { groupByNewsFileId: true });

               }
           },
           saveProgramData = function (data) {
               if (data) {

                   appdata.programsList(data);
                   for (var i = 0; i < data.length; i++) {
                       _.filter(appdata.foldersList(), function (obj) {
                           if (obj.FolderName === data[i].Name) {
                               obj.MaxStoryCount = data[i].MaxStoryCount ? data[i].MaxStoryCount : 0;
                               obj.MinStoryCount = data[i].MinStoryCount ? data[i].MinStoryCount : 0;
                               obj.Interval = data[i].Interval ? data[i].Interval : 0;
                           }
                       });

                   }
                   updateProgramIntervals();
               }


           },

           updateProgramIntervals = function () {
               for (var i = 0 ; i < appdata.foldersList().length; i++) {
                   var tempDate = new Date();
                   var currDate = new Date();

                   tempDate.setSeconds(0);
                   tempDate.setMinutes(0);

                   tempDate.setSeconds(parseInt(Math.floor(appdata.foldersList()[i].Interval)));

                   var currseconds = parseInt(currDate.getHours()) * 60 * 60 + (currDate.getMinutes() * 60) + (currDate.getSeconds());
                   var tempSeconds = parseInt(tempDate.getHours()) * 60 * 60 + (tempDate.getMinutes() * 60) + (tempDate.getSeconds());
                   if (tempSeconds < currseconds) {
                       tempDate.setHours(tempDate.getHours() + 1);
                   }
                   var formatedTime = moment(tempDate).format('hh:mm:ss');
                   appdata.foldersList()[i].programInterval = formatedTime;
                   $("#interval" + appdata.foldersList()[i].FolderId).text(formatedTime);
               };
           },

           saveNewsFiles = function (data, isFromPollingOrInitial) {
               if (data && data.length > 0) {
                   var clonedArray = JSON.parse(JSON.stringify(data));
                   var temp = sortDescByLastUpdateDate(clonedArray);

                   dc.newsFiles.fillData(data);
                   if (isFromPollingOrInitial) {
                       appdata.newsFilesLastUpdateDate = temp[0].LastUpdateDateStr || appdata.newsFilesLastUpdateDate;
                       appdata.newsFileLastUpdateDateObservable(temp[0].LastUpdateDateStr || appdata.newsFilesLastUpdateDate);
                   }
                   appdata.refreshGenericList(!appdata.refreshGenericList());
               }
           },
           saveNewsFileResources = function (data, isFromPollingOrInitial) {
               if (data && data.length > 0) {
                   var temp = sortDesc(data);
                   if (isFromPollingOrInitial) {
                       appdata.newsFileResourcesLastUpdateDate = temp[0].LastUpdateDateStr || appdata.newsFileResourcesLastUpdateDate;
                   }
                   dc.newsFileResources.fillData(data, { groupByNewsFileId: true });

               }
           },
           saveResources = function (data) {
               if (data && data.length > 0) {
                   dc.resources.fillData(data, { addToObservable: true });
               }
           },
           saveUsersData = function (users) {
               if (users && users.length > 0) {
                   dc.users.fillData(users);
               }
           },
           saveProgramsData = function (programs) {
               if (programs && programs.length > 0)
                   dc.programs.fillData(programs);
           },
           saveUserWorkRoleFolder = function (workrole) {
               if (workrole && workrole.length > 0) {
                   dc.workRoleFolders.fillData(workrole);
               }
           },
           generateUIForFolders = function () {
                    var width = (427 * (appdata.foldersList().length)) + 4;
                    $(".foldersCategoryCssWriter").width(parseInt(width) + 'px');

                   // if(appdata.currentUser().userType === e.UserType.Controlleroutput)
                   // {
                   //     var width = (400 * (appdata.foldersList().length)) + 4;
                   // $(".foldersCategoryCssWriter").width(parseInt(width) + 'px');
                   
                   //}
        },

           removeObsoleteFolderData = function () {

               var arr = dc.newsFolders.getAllLocal();
               var currentDateTime = new Date();
               for (var i = 0, len = arr.length; i < len; i++) {
                   var folder = arr[i];
                   if (folder.isRundown()) {
                       if (currentDateTime > new Date(folder.endTime())) {
                           dc.newsFolders.removeById(folder.id, true);
           }
               }
           }
           },
           createNewsFile = function (reqObj) {
               return $.Deferred(function (d) {
                   $.when(dataservice.createNewsFile(reqObj))
                       .done(function (responseData) {
                           saveNewsFileFolderHistory(responseData.Data.FileFolderHistory, false);
                           saveNewsFileDetails(responseData.Data.FileDetail, false);
                           saveNewsFileStatusHistory(responseData.Data.FileStatusHistory, false);
                           saveNewsFolders(responseData.Data.Folder, false);
                           saveNewsFiles(responseData.Data.NewsFile, false);
                           saveNewsFileResources(responseData.Data.FileResource, false);
                           saveResources(responseData.Data.FileResource, true);

                           d.resolve(responseData);
                   })
                       .fail(function (responseData) {
                           d.reject();
               });
               }).promise();
           },
           updateNewsFileAction = function (newsFileId, statusId, folderId) {
               var reqObj = {
                       NewsFileId: newsFileId,
                       StatusId: statusId,
                       FolderId: folderId
           }
               return $.Deferred(function (d) {
                   $.when(dataservice.updateNewsFileAction(reqObj))
                       .done(function (responseData) {
                           if (responseData.Data) {
                               saveNewsFileFolderHistory(responseData.Data.FileFolderHistory, false);
                               saveNewsFileDetails(responseData.Data.FileDetail, false);
                               saveNewsFileStatusHistory(responseData.Data.FileStatusHistory, false);
                               saveNewsFolders(responseData.Data.Folder, false);
                               saveNewsFiles(responseData.Data.NewsFile, false);
                               saveNewsFileResources(responseData.Data.FileResource, false);
                               saveResources(responseData.Data.FileResource, false);
                               $('.newsfilecontextmenu').hide();
                               config.logger.success("File action completed");
                       }
                           d.resolve(responseData);
                   })
                       .fail(function (responseData) {
                           d.reject();
               });
               }).promise();
           },
           updateNewsFileStatus = function (newsFileId, statusId, createdBy) {

               var item = dc.newsFiles.getLocalById(newsFileId);
               if (item)
                   item.statusId(statusId);

               var reqObj = {
                       NewsFileId: newsFileId,
                       StatusId: statusId,
                       CreatedBy: createdBy
           }
               return $.Deferred(function (d) {
                   $.when(dataservice.updateNewsFileStatus(reqObj))
                       .done(function (responseData) {
                           if (responseData.Data) {
                               saveNewsFileStatusHistory(responseData.Data.FileStatusHistory, false);
                               saveNewsFiles(responseData.Data.NewsFile, false);
                       }
                           d.resolve(responseData);
                   })
                       .fail(function (responseData) {
                           d.reject();
               });
               }).promise();
           },
           upadteNewsFile = function (reqObj) {
               return $.Deferred(function (d) {
                   $.when(dataservice.upadteNewsFile(reqObj))
                       .done(function (responseData) {
                           d.resolve(responseData);
                   })
                       .fail(function (responseData) {
                           d.reject();
               });
               }).promise();
           },
            delteNewsFileResource = function (reqObj) {
                return $.Deferred(function (d) {
                    $.when(dataservice.deletenewsFileResource(reqObj))
                        .done(function (responseData) {
                            d.resolve(responseData);
                    })
                        .fail(function (responseData) {
                            d.reject();
                });
                }).promise();
            },

            getBroadcatedNewsFileDetail = function (ParentNewsFileId) {
                
                return $.Deferred(function (d) {
                    $.when(dataservice.GetBroadcastedNewsFileDetail(ParentNewsFileId))
                        .done(function (responseData) {
                            d.resolve(responseData);
                        })
                        .fail(function (responseData) {
                            d.reject();
                        });
                }).promise();
            },

             UpdateNewsSequence = function (reqObj) {
                 return $.Deferred(function (d) {
                     $.when(dataservice.UpdateNewsSequence(reqObj))
                         .done(function (responseData) {
                             d.resolve(responseData);
                     })
                         .fail(function (responseData) {
                             d.reject();
                 });
                 }).promise();
           },

           createRundownFolder = function (epid) {

               var reqObj = {
                   EpisodeId: epid
           }
               return $.Deferred(function (d) {
                   $.when(dataservice.createRundownFolder(reqObj))
                       .done(function (responseData) {
                           if (responseData.Data) {
                               var temp =[];
                               temp.push(responseData.Data);
                               dc.newsFolders.fillData(temp, { groupByNewsFileId: true, sort: true
                           });
                               config.logger.success("Folder created successfully");
                       }
                           d.resolve(responseData);
                   })
                       .fail(function (responseData) {
                           d.reject();
               });
               }).promise();
           },
           getProgramEpisodes = function (programId, date, episodeList) {
               var requestObj = {
                       ProgramId: programId,
                       DateStr: moment(date).toISOString()
           };
               return $.Deferred(function (d) {
                   $.when(dataservice.getProgramEpisodes(requestObj))
                           .done(function (responseData) {
                               if (responseData && responseData.IsSuccess && responseData.Data) {
                                   var arr =[];
                                   for (var i = 0; i < responseData.Data.length; i++) {
                                       var temp = mapper.episode.fromDto(responseData.Data[i]);
                                       arr.push(temp);
                               }
                                   episodeList(arr);
                                   d.resolve();
                               } else {
                                   d.reject();
                           }
                   })
                           .fail(function (data) {
                               d.reject();
               })
               }).promise();
           },
          sendProgramToBroadcast = function (folderId) {
              logger.info('Please wait ...');
              var requestObj = {
                      NewsStatisticsParam: getNewsStatisticParams(folderId)
          };

              return $.Deferred(function (d) {
                  $.when(dataservice.sendProgramRundownToBroadCast(requestObj))
                      .done(function (responseData) {
                          if (responseData && responseData.IsSuccess && responseData.Data) {
                              logger.success('Program sent to broadcast');
                              d.resolve();
                          } else {
                              logger.error(responseData.Errors[0]);
                              d.reject();
                      }
                  })
                      .fail(function (d) {
                          logger.error('An error occured ');
                          d.reject();
              })
              }).promise();
           },
          markNewsFileStatus = function (reqObj) {
              return $.Deferred(function (d) {
                  $.when(dataservice.markNewsFileStatus(reqObj))
                      .done(function (responseData) {
                          if (responseData.Data) {
                      }
                          d.resolve(responseData);
                  })
                      .fail(function (responseData) {
                          d.reject();
              });
              }).promise();
           },
         markSocialMediaStatus = function (reqObj) {
              return $.Deferred(function (d) {
                  $.when(dataservice.markSocialMediaStatus(reqObj))
                      .done(function (responseData) {
                          if (responseData.Data) {
                      }
                          d.resolve(responseData);
                  })
                      .fail(function (responseData) {
                          d.reject();
              });
              }).promise();
           },

          markNewsFileDeleted = function (reqObj) {
              return $.Deferred(function (d) {
                  $.when(dataservice.markNewsFileDeleted(reqObj))
                      .done(function (responseData) {
                          dc.newsFiles.removeById(reqObj.newsFileId);
                          dc.newsFiles.getAllLocal();
                          appdata.refreshGenericList(!appdata.refreshGenericList());
                          if (responseData.Data) {
                      }
                          d.resolve(responseData);
                  })
                      .fail(function (responseData) {
                          d.reject();
              });
              }).promise();
           },

          updateNewsFilesSequencing = function (reqObj) {
              return $.Deferred(function (d) {
                  $.when(dataservice.updateNewsFilesSequencing(reqObj))
                      .done(function (responseData) {
                          if (responseData.Data && responseData.Data.NewsFile) {
                              saveNewsFiles(responseData.Data.NewsFile, true);
                      }
                          d.resolve(responseData);
                  })
                      .fail(function (responseData) {
                          d.reject();
              });
              }).promise();
           },

          updateProgramRelatedNews = function (reqObj) {
              return $.Deferred(function (d) {
                  $.when(dataservice.updateProgramRelatedNews(reqObj))
                      .done(function (responseData) {
                          if (responseData.Data) {
                      }
                          d.resolve(responseData);
                  })
                      .fail(function (responseData) {
                          d.reject();
              });
              }).promise();
           },
          getnewsFileDetail = function (reqObj) {
              return $.Deferred(function (d) {
                  $.when(dataservice.getnewsFileDetail(reqObj))
                      .done(function (responseData) {
                          d.resolve(responseData.Data);
                  })
                      .fail(function (responseData) {
                          d.reject();
              });
              }).promise();
          },
          markTitleVoiceOverStatus = function (reqObj) {
              return $.Deferred(function (d) {
                  $.when(dataservice.markTitleVoiceOverStatus(reqObj))
                      .done(function (responseData) {
                          d.resolve(responseData);
                  })
                      .fail(function (responseData) {
                          d.reject();
              });
              }).promise();
          },
          
          copyNewsFileToFolder = function (reqObj) {
              return $.Deferred(function (d) {
                  $.when(dataservice.copyNewsFileToFolder(reqObj))
                      .done(function (responseData) {
                          if(responseData && responseData.Data) {
                              saveNewsFiles(responseData.Data.NewsFile, true);
                              appdata.refreshGenericList(!appdata.refreshGenericList());
                      }
                          d.resolve(responseData.Data);
                  })
                      .fail(function (responseData) {
                          d.reject();
              });
              }).promise();
           },

           publishNewsFile = function (reqObj) {
               return $.Deferred(function (d) {
                   $.when(dataservice.publishNewsFile(reqObj))
                       .done(function (responseData) {
                           if (responseData.Data) {
                               var temp =[];
                               temp.push(responseData.Data);
                               dc.newsFileStatusHistory.fillData(temp, { groupByNewsFileId: true, sort: true
                           });
                       }
                           d.resolve(responseData);
                   })
                       .fail(function (responseData) {
                           d.reject();
               });
               }).promise();
    };


        programToPrint = function (reqObj) {
            return $.Deferred(function (d) {
                $.when(dataservice.programToPrint(reqObj))
                    .done(function (responseData) {
                            d.resolve(responseData);
                    })
                    .fail(function (responseData) {
                             d.reject(responseData);
                    });
            }).promise();
        }
        // #endregion

        return {
                loadInitialData: loadInitialData,
                newsFilePolling: newsFilePolling,
                upadteNewsFile: upadteNewsFile,
                createNewsFile: createNewsFile,
                updateNewsFileAction: updateNewsFileAction,
                updateNewsFileStatus: updateNewsFileStatus,
                publishNewsFile: publishNewsFile,
                createRundownFolder: createRundownFolder,
                getProgramEpisodes: getProgramEpisodes,
                removeObsoleteFolderData: removeObsoleteFolderData,
                delteNewsFileResource: delteNewsFileResource,
                getBroadcatedNewsFileDetail: getBroadcatedNewsFileDetail,
                UpdateNewsSequence: UpdateNewsSequence,
                sendProgramToBroadcast: sendProgramToBroadcast,
                markNewsFileStatus: markNewsFileStatus,
                getnewsFileDetail: getnewsFileDetail,
                newsFileProducerPolling: newsFileProducerPolling,
                copyNewsFileToFolder: copyNewsFileToFolder,
                newsFileFilterSelected: newsFileFilterSelected,
                updateProgramRelatedNews: updateProgramRelatedNews,
                markNewsFileDeleted: markNewsFileDeleted,
                updateNewsFilesSequencing: updateNewsFilesSequencing,
                programToPrint: programToPrint,
                markSocialMediaStatus: markSocialMediaStatus,
                OrganizationTagsUpdate: OrganizationTagsUpdate,
                GetNewsFileForTaggReport: GetNewsFileForTaggReport,
                markTitleVoiceOverStatus: markTitleVoiceOverStatus

        };
    });