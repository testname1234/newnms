﻿define('manager.newspaper',
    [
        'jquery',
        'dataservice',
        'datacontext',
        'underscore',
        'config'
    ],
    function ($, dataservice, dc, _, config) {
        var
            newspaperReporterloadInitialData = function () {
                var data = {};
               
                data.toDate = moment(new Date()).format('YYYY-MM-DD');
                data.fromDate = moment(new Date()).format('YYYY-MM-DD');

                return $.Deferred(function (d) {
                    $.when(dataservice.newspaperReporterLoadInitialData(data))
                        .done(function (responseData) {
                            if (responseData.IsSuccess) {

                                var newsPapers = responseData.Data.NewsPappers;
                                var dailyNewsPapers = responseData.Data.DailyNewsPapers;
                                var newsPaperPages = responseData.Data.NewsPaperPages;

                                if (newsPapers && newsPapers.length > 0) {
                                    dc.newsPapers.fillData(newsPapers);
                                }
                                if (dailyNewsPapers && dailyNewsPapers.length > 0) {
                                    dc.dailyNewsPapers.fillData(dailyNewsPapers, { groupByNewsPaperId: true, groupByDailyNewsPaperDate: true });
                                }
                                if (newsPaperPages && newsPaperPages.length > 0) {
                                    dc.newsPaperPages.fillData(newsPaperPages, { groupByDailyNewsPaperId: true });
                                }

                                d.resolve(responseData);
                            }
                            else {
                                d.reject();
                            }
                        })
                        .fail(function (responseData) {
                            d.reject();
                        });
                }).promise();
            },

        getNewsPaperByDateRange = function (data) {
            if (data) {
                data.fromDate = moment(new Date(data.fromDate)).format(config.ServerDateFormat);
                data.toDate = moment(new Date(data.toDate)).format(config.ServerDateFormat);
            }
            return $.Deferred(function (d) {
                $.when(dataservice.getNewsPapersByDateRange(data))
                    .done(function (responseData) {
                        if (responseData.IsSuccess) {

                            var dailyNewsPapers = responseData.Data.DailyNewsPapers;
                            var newsPaperPages = responseData.Data.NewsPaperPages;

                            if (dailyNewsPapers && dailyNewsPapers.length > 0) {

                                dc.dailyNewsPapers.fillData(dailyNewsPapers, { groupByNewsPaperId: true, groupByDailyNewsPaperDate: true });
                            }
                            if (newsPaperPages && newsPaperPages.length > 0) {

                                dc.newsPaperPages.fillData(newsPaperPages, { groupByDailyNewsPaperId: true });
                            }

                            d.resolve(responseData);
                        }
                        else {
                            d.reject(responseData);
                        }
                    })
                    .fail(function (responseData) {
                        d.reject();
                    });
            }).promise();
        };


        return {
            newspaperReporterloadInitialData: newspaperReporterloadInitialData,
            getNewsPaperByDateRange: getNewsPaperByDateRange
           
        }
    });