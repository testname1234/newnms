﻿define('manager.production',
    [
        'jquery',
        'enum',
        'dataservice',
        'datacontext',
        'underscore',
        'utils',
        'appdata',
        'config',
        'model',
        'model.mapper',
        'presenter',
        'router',
        'manager.news'
    ],
    function ($, e, dataservice, dc, _, utils, appdata, config, model, mapper, presenter, router, newsManager) {
        var logger = config.logger;

        var
            getNewsSequenceRequestObj = function () {
                var stories = dc.stories.getByEpisodeId(appdata.currentEpisode().id)();
                var newsSequence = [];

                for (var i = 0; i < stories.length; i++) {
                    var tempObj = {};
                    tempObj["StoryId"] = stories[i].news.id;
                    tempObj["SequenceId"] = stories[i].sequenceId
                    newsSequence.push(tempObj);
                }

                return newsSequence;
            },

            getProductionTeamLongPollingData = function () {
                var requestObj = {
                    DateStr: appdata.lpFromDateStr,
                    LastUpdateDateStr: appdata.lpStoryScreenTemplateLastUpdateDate,
                    UserId: appdata.currentUser().id,
                    TeamRoleId: appdata.currentUser().userType,
                    SlotScreenTemplateId: appdata.currentStoryTemplateId,
                    CommentsLastUpdateStr: appdata.storyTemplateCommentLastUpdateDate,
                    NotificationsLastUpdateDateStr: appdata.lpNotificationsLastUpdateDate
                };

                return requestObj;
            },

            //#region Common Methods

            getNewsStatisticParams = function () {
                var celebrityIds = [];
                var suggestedFlowKeys = [];
                var stories = dc.stories.getByEpisodeId(appdata.currentEpisode().id)();

                for (var i = 0; i < stories.length; i++) {
                    var story = stories[i];
                    var screenTemplates = story.screenFlow().screenTemplates();
                    var templateIds = [];

                    for (var j = 0; j < screenTemplates.length; j++) {
                        var screenTemplate = screenTemplates[j];
                        var templateElements = screenTemplate.templateElements();

                        for (var k = 0; k < templateElements.length; k++) {
                            var templateElement = templateElements[k];
                            if (templateElement.screenElementId === e.ScreenElementType.Guest) {
                                if (templateElement.guestId) {
                                    celebrityIds.push(templateElement.guestId);
                                }
                            }
                        }

                        if (!screenTemplate.isMediaWallTemplate()) {
                            templateIds.push(screenTemplate.id);
                        }
                    }

                    var suggestedFlowKey = templateIds.length > 0 ? S(templateIds).toCSV('_', null).s : '';
                    suggestedFlowKeys.push(suggestedFlowKey);
                }
                var channelId = appdata.currentChannel();

                if (channelId)
                    var channelName = dc.channels.getLocalById(channelId).name;

                return {
                    CelebrityIds: celebrityIds,
                    ProgramId: appdata.currentProgram().id,
                    ChannelId: channelId,
                    ChannelName: channelName,
                    ProgramName: appdata.currentProgram().name,
                    EpisodeId: appdata.currentEpisode().id,
                    EpisodeStartTimeStr: appdata.currentEpisode().startTime(),
                    SuggestedFlowKeys: suggestedFlowKeys,
                    UserId: appdata.currentUser().id
                };
            },
            searchItem = function (item, srchText) {
                if (!srchText) return true;
                var srch = utils.regExEscape(srchText.toLowerCase().trim());
                if (item && item.hasOwnProperty('topNews') && (item.topNews().title().toLowerCase().search(srch) !== -1 || item.topNews().description().toLowerCase().search(srch) !== -1))
                    return true;
                else if (item && item.hasOwnProperty('news') && (item.news.title().toLowerCase().search(srch) !== -1 || item.news.description().toLowerCase().search(srch) !== -1))
                    return true;

                return false;
            },
            getBySearchKeyWord = function (arrayObject) {
                var arr = _.filter(arrayObject, function (item) {
                    return searchItem(item, appdata.searchKeywords);
                });
                return arr;
            },
            getDataBySearchKeyword = function (arrayObject, searchKeyword) {
                var arr = _.filter(arrayObject, function (item) {
                    var tempArray = dc.stories.getByEpisodeId(item.id)();
                    var stories = _.filter(tempArray, function (story) {
                        return searchItem(story, searchKeyword);
                    });
                    return stories.length > 0;
                });
                return arr;
            },
            proceedToSequence = function (data) {
                presenter.toggleActivity(true);
                $.when(saveProgramDataOnServer(data))
                .done(function () {
                    router.navigateTo(config.hashes.production.production);
                    appdata.refreshNewsBucket(!appdata.refreshNewsBucket());
                    appdata.arrangeNewsFlag(!appdata.arrangeNewsFlag());
                    logger.success("Stories saved successfully!");
                })
                .fail(function () {
                    presenter.toggleActivity(false);
                    logger.error("Server returned an error!");
                });
            },
            proceedToProduction = function (flag, data) {
                if (flag) {
                   
                    proceedToSequence(data);
                    appdata.checkTemplates(!appdata.checkTemplates());
                }
                else
                    logger.error(config.toasts.cantProceed);
                    appdata.checkTemplates(!appdata.checkTemplates());
            },
            loadInitialDataProductionTeam = function () {
                return $.Deferred(function (d) {
                    var requestObj = getProductionTeamLongPollingData();

                    $.when(dataservice.loadInitialDataProductionTeam(requestObj))
                        .done(function (responseData) {
                            if (responseData && responseData.IsSuccess && responseData.Data) {
                                newsManager.saveProgramsData(responseData.Data.Programs);
                                saveProgramEpisodes(responseData.Data.Episodes);
                                saveUsersData(responseData.Data.users);
                                presenter.toggleActivity(false);

                                if (dc.programs.getAllLocal().length > 0) {
                                    appdata.currentProgram(dc.programs.getAllLocal()[0]);
                                }
                                d.resolve();
                            }
                            else {
                                d.reject();
                            }
                        })
                        .fail(function (data) {
                            d.reject();
                        })
                }).promise();
            },

            productionTeamPolling = function () {
                return $.Deferred(function (d) {
                    var requestObj = getProductionTeamLongPollingData();

                    $.when(dataservice.productionTeamPolling(requestObj))
                    .done(function (responseData) {
                        if (responseData && responseData.IsSuccess) {
                            newsManager.saveProgramsData(responseData.Data.Programs);
                            saveProgramEpisodes(responseData.Data.Episodes);
                            saveComments(responseData.Data.Messages);
                            saveNotifications(responseData.Data.Notifications);

                            if (responseData.Data.Episodes && responseData.Data.Episodes.length > 0) {
                                appdata.lastChangeTime = moment().toISOString();
                            }

                            presenter.toggleActivity(false);
                            d.resolve();
                        }
                        else {
                            d.reject();
                        }
                    })
                    .fail(function (data) {
                        d.reject();
                    })
                }).promise();
            },

            //#endregion

            //#region Local Data Processing

            saveProgramEpisodes = function (episodes) {
                if (episodes && episodes.length > 0) {
                    for (var i = 0; i < episodes.length; i++) {
                        saveProgramElements(episodes[i].Segments);
                    }
                    dc.episodes.fillData(episodes, { groupByProgramId: true });
                }
            },
            saveScreenTemplates = function (templates) {
                if (templates && templates.length > 0) {
                    dc.screenTemplates.fillData(templates);
                }
            },
            saveUsersData = function (users) {
                if (users && users.length > 0) {
                    dc.users.fillData(users);
                }
            },
            saveTemplateElements = function (templateElements) {
                if (templateElements && templateElements.length) {
                    dc.templateElements.fillData(templateElements, { groupByTemplateId: true });
                }
            },
            saveProgramElements = function (programElements) {
                if (programElements) {
                    for (var i = 0; i < programElements.length; i++) {
                        saveProgramStories(programElements[i].Slots, programElements[i].EpisodeId);
                    }

                    dc.programElements.fillData(programElements, { groupByEpisodeId: true });
                }
            },
            saveProgramStories = function (stories, episodeId) {
                if (stories) {
                    for (var i = 0; i < stories.length; i++) {
                        var isStoryAlreadyExists = false;
                        var tempStory1 = dc.stories.getLocalById(stories[i].SlotId);
                        var tempStory2 = dc.stories.getLocalById(stories[i].NewsGuid);

                        if ((tempStory1 && tempStory1.episodeId === episodeId) || (tempStory2 && tempStory2.episodeId === episodeId)) {
                            isStoryAlreadyExists = true;
                            var arr = dc.stories.getLocalById(stories[i].SlotId);
                            if (arr) {
                                if (appdata.currentUser().userType === e.UserType.NLE || appdata.currentUser().userType === e.UserType.StoryWriter) {
                                    for (var j = 0; j < stories[i].SlotScreenTemplates.length; j++) {
                                        var temp = _.filter(arr.screenFlow().screenTemplates(), function (item) {
                                            return item.storyScreenTemplateId == stories[i].SlotScreenTemplates[j].SlotScreenTemplateId;
                                        });
                                        if (temp && temp.length > 0) {
                                            if (stories[i].SlotScreenTemplates[j].NleStatusId)
                                                temp[0].nleStatusId(stories[i].SlotScreenTemplates[j].NleStatusId);
                                            if (stories[i].SlotScreenTemplates[j].StoryWriterStatusId)
                                                temp[0].storywriterStatusId(stories[i].SlotScreenTemplates[j].StoryWriterStatusId);

                                        }
                                        appdata.isRefreshStoryStatus(!appdata.isRefreshStoryStatus());
                                        if (temp && temp.length <= 0) {
                                            var storyTemplate = stories[i].SlotScreenTemplates[j]
                                            var screenTemplate = mapper.screenTemplate.fromDto(storyTemplate);

                                            if (storyTemplate.NleStatusId) {
                                                screenTemplate.nleStatusId(storyTemplate.NleStatusId);
                                            }

                                            if (storyTemplate.StoryWriterStatusId) {
                                                screenTemplate.storywriterStatusId(storyTemplate.StoryWriterStatusId);
                                            }
                                            if (storyTemplate.SlotTemplateScreenElements && storyTemplate.SlotTemplateScreenElements.length > 0) {
                                                for (var k = 0; k < storyTemplate.SlotTemplateScreenElements.length; k++) {
                                                    var templateElement = mapper.templateElement.fromDto(storyTemplate.SlotTemplateScreenElements[k]);
                                                    if (templateElement) {
                                                        templateElement["screenTemplateId"] = screenTemplate.id;
                                                        screenTemplate.templateElements().push(templateElement);
                                                    }
                                                }
                                            }
                                            if (storyTemplate.SlotScreenTemplatekeys && storyTemplate.SlotScreenTemplatekeys.length > 0) {
                                                var templateKeys = [];
                                                for (var k = 0; k < storyTemplate.SlotScreenTemplatekeys.length; k++) {
                                                    var templateKey = storyTemplate.SlotScreenTemplatekeys[k];
                                                    templateKey["ScreenTemplateId"] = storyTemplate.ScreenTemplateId;
                                                    var temp = mapper.templateKey.fromDto(templateKey);
                                                    screenTemplate.templateKeys().push(temp);
                                                }
                                            }
                                            arr.screenFlow().screenTemplates.push(screenTemplate);
                                        }
                                    }
                                }
                            }
                        }

                        if (!isStoryAlreadyExists) {
                            var news = dc.news.getLocalById(stories[i].NewsGuid);
                            if (!news) {
                                var tempObj = {
                                    Title: stories[i].Title,
                                    Description: stories[i].Description,
                                    ShortDescription: stories[i].Description,
                                    ThumbnailUrl: stories[i].ThumbGuid,
                                    LanguageCode: stories[i].LanguageCode,
                                    TranslatedDescription: stories[i].TranslatedDescription,
                                    TranslatedTitle: stories[i].TranslatedTitle
                                   
                                };

                                if (stories[i].NewsGuid) {
                                    tempObj["_id"] = stories[i].NewsGuid;
                                }

                                news = mapper.news.fromDto(tempObj);
                            }

                            var screenFlow = new model.ScreenFlow();

                            if (stories[i].SlotScreenTemplates && stories[i].SlotScreenTemplates.length > 0) {
                                for (var j = 0; j < stories[i].SlotScreenTemplates.length; j++) {
                                    var storyTemplate = stories[i].SlotScreenTemplates[j];
                                    var screenTemplate = mapper.screenTemplate.fromDto(storyTemplate);

                                    if (screenTemplate) {
                                        if (storyTemplate.SlotTemplateScreenElements && storyTemplate.SlotTemplateScreenElements.length > 0) {
                                            for (var k = 0; k < storyTemplate.SlotTemplateScreenElements.length; k++) {
                                                var templateElement = mapper.templateElement.fromDto(storyTemplate.SlotTemplateScreenElements[k]);
                                                if (templateElement) {
                                                    templateElement["screenTemplateId"] = screenTemplate.id;
                                                    screenTemplate.templateElements().push(templateElement);
                                                }
                                            }
                                        }
                                        if (storyTemplate.NleStatusId) {
                                            screenTemplate.nleStatusId(storyTemplate.NleStatusId);
                                        }

                                        if (storyTemplate.StoryWriterStatusId) {
                                            screenTemplate.storywriterStatusId(storyTemplate.StoryWriterStatusId);
                                        }

                                        if (storyTemplate.SlotScreenTemplatekeys && storyTemplate.SlotScreenTemplatekeys.length > 0) {
                                            var templateKeys = [];

                                            for (var k = 0; k < storyTemplate.SlotScreenTemplatekeys.length; k++) {
                                                var templateKey = storyTemplate.SlotScreenTemplatekeys[k];
                                                templateKey["ScreenTemplateId"] = storyTemplate.ScreenTemplateId;

                                                var temp = mapper.templateKey.fromDto(templateKey);
                                                screenTemplate.templateKeys().push(temp);
                                            }
                                        } else {
                                            //var temp = dc.screenTemplates.getLocalById(storyTemplate.ScreenTemplateId);
                                            //if (temp) {
                                            //screenTemplate.templateKeys(temp.templateKeys().slice(0));
                                            //}
                                        }

                                        screenFlow.screenTemplates.push(screenTemplate);

                                        if (appdata.currentUser().userType === e.UserType.StoryWriter || appdata.currentUser().userType === e.UserType.NLE) {
                                            if (screenTemplate.lastUpdateDate() > appdata.lpStoryScreenTemplateLastUpdateDate) {
                                                appdata.lpStoryScreenTemplateLastUpdateDate = screenTemplate.lastUpdateDate();
                                            }
                                        }
                                    }
                                }
                            }

                            var story = {
                                Id: stories[i].SlotId,
                                StoryId: stories[i].SlotId,
                                EpisodeId: episodeId,
                                CategoryId: stories[i].CategoryId,
                                ProgramElementId: stories[i].SegmentId,
                                SequenceId: stories[i].SequnceNumber,
                                PreviewGuid: stories[i].PreviewGuid,
                                CreationDateStr: stories[i].CreationDateStr,
                                LastUpdateDateStr: stories[i].LastUpdateDateStr,
                                UserId: stories[i].UserId,
                                News: news,
                                TickerId: stories[i].TickerId,
                                ScreenFlow: screenFlow,
                                IncludeInRundown: stories[i].IncludeInRundown
                            }

                            dc.stories.add(story, { addToObservable: true, groupByEpisodeId: true, groupByProgramElementId: true });
                        }
                    }
                }
            },
            saveComments = function (comments) {
                if (comments && comments.length > 0) {
                    var tempDate = dc.comments.fillData(comments, { groupByStoryId: true, sort: true });
                    appdata.storyTemplateCommentLastUpdateDate = tempDate || appdata.storyTemplateCommentLastUpdateDate;
                }
            },
            saveNotifications = function (data) {
                if (data && data.length > 0) {
                    var tempDate = dc.notifications.fillData(data, { sort: true });
                    appdata.lpNotificationsLastUpdateDate = tempDate || appdata.lpNotificationsLastUpdateDate;

                    var arr = _.filter(dc.notifications.getObservableList(), function (tempObj) {
                        return !tempObj.isRead();
                    });

                    if (arr && arr.length > 0)
                        appdata.currentNotificationAlertId(arr[0]);
                }
            },
            saveGuests = function (guests) {
                if (guests && guests.length > 0) {
                    dc.guests.fillData(guests);
                }
            },
            getPendingStoriesByFilter = function () {
                var nleProgramFilter = appdata.NLEProgramFilter();
                var nleDateFilter = appdata.NLEDateFilter();
                var nleTimeFilter = appdata.NLETimeFilter();

                var allStories = _dc.stories.getObservableList();

                if (nleProgramFilter.length == 0 && nleDateFilter.length == 0 && nleTimeFilter.length == 0)
                    return allStories;

                var filteredStoryList = [];

                if (nleProgramFilter.length > 0) {
                    for (var i = 0; i < nleProgramFilter.length; i++) {
                        var program = nleProgramFilter[i];

                        var episodes = _dc.episodes.getByProgramId(program.id)();

                        for (var j = 0; j < episodes.length; j++) {
                            var episode = episodes[j];
                            var stories = _dc.stories.getByEpisodeId(episode.id)();
                            for (var k = 0; k < stories.length; k++) {
                                var object = utils.arrayIsContainObject(filteredStoryList, stories[k], "id");
                                if (!object.flag) {
                                    filteredStoryList.push(stories[k]);
                                }
                            }
                        }
                    }
                }
                if (nleDateFilter.length > 0) {
                    var arr = filteredStoryList.length > 0 ? filteredStoryList : allStories;
                    filteredStoryList = [];

                    for (var i = 0; i < nleDateFilter.length; i++) {
                        for (var j = 0; j < arr.length; j++) {
                            var story = arr[j];

                            if (moment(story.lastUpdateDate()).format('MMMM D, YYYY') == nleDateFilter[i].date) {
                                var object = utils.arrayIsContainObject(filteredStoryList, story, "id");
                                if (!object.flag) {
                                    filteredStoryList.push(story);
                                }
                            }
                        }
                    }
                }

                if (nleTimeFilter.length > 0) {
                    var arr = filteredStoryList.length > 0 ? filteredStoryList : allStories;
                    filteredStoryList = [];
                    for (var i = 0; i < nleTimeFilter.length; i++) {
                        for (var j = 0; j < arr.length; j++) {
                            var story = arr[j];
                            if ((new Date(story.lastUpdateDate()).getHours()) == nleTimeFilter[i].time.split(":")[0]) {
                                var object = utils.arrayIsContainObject(filteredStoryList, story, "id");
                                if (!object.flag) {
                                    filteredStoryList.push(story);
                                }
                            }
                        }
                    }
                }
                return filteredStoryList;
            },
            getFilteredPendingStories = function (calendarOption) {
                return $.Deferred(function (d) {
                    if (appdata.lastUIRefreshTime < appdata.lastChangeTime) {
                        var arr = [];
                        var pageIndex;
                        arr = getPendingStoriesByFilter();

                        if (appdata.searchKeywords.length > 0) {
                            arr = getBySearchKeyWord(arr);
                            appdata.lpStartIndex = 0;
                        } else {
                            appdata.lpStartIndex = arr.length;
                        }
                        arr = arr.sort(function (storyA, storyB) {
                            return new Date(storyB.lastUpdateDate()) - new Date(storyA.lastUpdateDate());
                        });

                        var fromDate = moment(appdata.lpFromDate);
                        var toDate = moment(appdata.lpToDate);

                        if (calendarOption && calendarOption == 'custom') {
                            arr = _.filter(arr, function (story) {
                                return moment(story.lastUpdateDate()) >= fromDate && moment(story.lastUpdateDate()) <= toDate;
                            });
                        } else {
                            arr = _.filter(arr, function (story) {
                                return moment(story.lastUpdateDate()) >= fromDate;
                            });
                        }
                        appdata.lastUIRefreshTime = moment().toISOString();
                        var responseData = {
                            'pendingStories': arr,
                            'PageIndex': pageIndex
                        };
                        d.resolve(responseData);
                    }
                    else
                        d.reject();
                }).promise();
            },
            getHeadlinesByFilter = function () {
                var filterHeadlineList = [];

                var dateFilter = appdata.headLineDateFilter();
                var timeFilter = appdata.headLineTimeFilter();
                var allEpisodes = dc.episodes.getObservableList();

                if (dateFilter.length == 0 && timeFilter.length == 0)
                    return allEpisodes;

                if (allEpisodes && allEpisodes.length > 0) {
                    if (dateFilter.length > 0) {
                        var arr = filterHeadlineList.length > 0 ? filterHeadlineList : allEpisodes;
                        filterHeadlineList = [];
                        for (var i = 0; i < dateFilter.length; i++) {
                            for (var j = 0; j < arr.length; j++) {
                                var episode = arr[j];
                                if (moment(episode.startTime()).format('LL') === dateFilter[i].date) {
                                    var object = utils.arrayIsContainObject(filterHeadlineList, episode, "id");
                                    if (!object.flag) {
                                        filterHeadlineList.push(episode);
                                    }
                                }
                            }
                        }
                    }
                    if (timeFilter.length > 0) {
                        var arr = filterHeadlineList.length > 0 ? filterHeadlineList : allEpisodes;
                        filterHeadlineList = [];
                        for (var i = 0; i < timeFilter.length; i++) {
                            for (var j = 0; j < arr.length; j++) {
                                var episode = arr[j];
                                if (moment(episode.startTime()).format('hh:mm a') === timeFilter[i].time) {
                                    var object = utils.arrayIsContainObject(filterHeadlineList, episode, "id");
                                    if (!object.flag) {
                                        filterHeadlineList.push(episode);
                                    }
                                }
                            }
                        }
                    }
                }

                return filterHeadlineList;
            },
            getFilteredHeadlines = function () {
                return $.Deferred(function (d) {
                    if (appdata.lastUIRefreshTimeHeadline < appdata.lastChangeTimeHeadline) {
                        var pageIndex;
                        var arr = [];
                        if (appdata.searchHeadlineKeywords.length > 0) {
                            arr = dc.episodes.getObservableList();
                            arr = getBySearchKeyWord(arr);
                        }
                        else {
                            arr = getHeadlinesByFilter();

                            var fromDate = moment(appdata.lpHeadlineFromDate);
                            var toDate = moment(appdata.lpHeadlineToDate);

                            arr = _.filter(arr, function (episode) {
                                return moment(episode.startTime()) >= fromDate && moment(episode.startTime()) <= toDate;
                            });

                            appdata.lpStartIndexHeadline = dc.episodes.getAllLocal().length;
                        }
                        arr = arr.sort(function (a, b) {
                            return new Date(b.startTime()) - new Date(a.startTime());
                        });

                        arr = _.filter(arr, function (episode) {
                            return episode.id !== appdata.currentEpisode().id;
                        });
                        appdata.lastUIRefreshTimeHeadline = moment().toISOString();
                        var responseData = {
                            'headlines': arr,
                            'PageIndex': pageIndex
                        };

                        d.resolve(responseData);
                    } else {
                        d.reject();
                    }
                }).promise();
            },
            setDataChangeTime = function () {
                appdata.lastChangeTimeHeadline = moment().toISOString();
            },
            setTickerDataChangeTime = function () {
                appdata.lastChangeTimeHeadline = moment().toISOString();
            },
            toggleHeadlineFilter = function (data) {
                if (data) {
                    var filter;
                    if (data.date) {
                        filter = appdata.headLineDateFilter;
                        filterType = "date";
                    }
                    else if (data.time) {
                        filter = appdata.headLineTimeFilter;
                        filterType = "time";
                    }

                    nlePushFilterOrRemove(filter, data, filterType);
                }
            },
            toggleStoryFilter = function (data) {
                if (data) {
                    var filter;
                    if (data.id) {
                        filter = appdata.NLEProgramFilter;
                        filterType = "id";
                    }

                    if (data.date) {
                        filter = appdata.NLEDateFilter;
                        filterType = "date";
                    }

                    if (data.time) {
                        filter = appdata.NLETimeFilter;
                        filterType = "time";
                    }

                    nlePushFilterOrRemove(filter, data, filterType);
                }
            },
            nlePushFilterOrRemove = function (filter, data, filterType) {
                if (filter && data && filterType) {
                    var object = utils.arrayIsContainObject(filter(), data, filterType);
                    if (!object.flag)
                        filter.push(data);
                    else
                        filter.splice(object.objectIndex, 1);
                }
            },

            //#endregion

            //#region Remote CUD Calls

            generateScreenTemplate = function (requestObj) {
                return $.Deferred(function (d) {
                    $.when(dataservice.generateScreenTemplate(requestObj))
                        .done(function (responseData) {
                            if (responseData && responseData.IsSuccess && responseData.Data) {
                                d.resolve(responseData);
                            } else {
                                d.reject(responseData);
                            }
                        })
                        .fail(function (data) {
                            d.reject(data);
                        })
                }).promise();
            },

            getSuggestedFlow = function (requestObj) {
                return $.Deferred(function (d) {
                    $.when(dataservice.getSuggestedFlow(requestObj))
                        .done(function (responseObj) {
                            d.resolve(responseObj);
                        })
                        .fail(function (responseObj) {
                            d.reject(responseObj);
                        })
                }).promise();
            },

            getProgramScreenFlow = function () {
                return $.Deferred(function (d) {
                    var requestObj = {
                        ProgramId: appdata.currentProgram().id,
                        UserId: appdata.currentUser().id
                    };

                    $.when(dataservice.getProgramScreenFlow(requestObj))
                        .done(function (responseData) {
                            if (responseData && responseData.IsSuccess && responseData.Data) {
                                saveScreenTemplates(responseData.Data.ScreenTemplates);
                                for (var k = 0; k < dc.screenTemplates.getObservableList().length; k++) {
                                    if (dc.screenTemplates.getObservableList()[k].isMediaWallTemplate()) {
                                        var arr = [];
                                        _.filter(responseData.Data.TemplateScreenElements, function (item) {
                                            if (item.ScreenTemplateId == dc.screenTemplates.getObservableList()[k].id) {
                                                var tempElement = mapper.templateElement.fromDto(item);
                                                arr.push(tempElement);
                                            }
                                        });
                                        if (arr && arr.length > 0) {
                                            dc.screenTemplates.getObservableList()[k].templateElements([]);
                                            dc.screenTemplates.getObservableList()[k].templateElements(arr);
                                        }
                                    }
                                }
                                if (responseData.Data.ScreenElementIds && responseData.Data.ScreenElementIds.length > 0) {
                                    for (var i = 0; i < responseData.Data.ScreenElementIds.length; i++) {
                                        var screenElement = dc.screenElements.getLocalById(responseData.Data.ScreenElementIds[i]);
                                        if (screenElement) {
                                            screenElement.isAllowed(true);
                                        } else {
                                        }
                                    }
                                }

                                d.resolve();
                            } else {
                                d.reject();
                            }
                        })
                        .fail(function (data) {
                            d.reject();
                        })
                }).promise();
            },

            getCelebrityByTerm = function (searchKeywords) {
                var requestObj = {
                    term: searchKeywords || ''
                };

                return $.Deferred(function (d) {
                    $.when(dataservice.getCelebrityByTerm(requestObj))
                        .done(function (responseData) {
                            if (responseData && responseData.IsSuccess && responseData.Data) {
                                saveGuests(responseData.Data);

                                d.resolve(responseData);
                            } else {
                                d.reject(responseData);
                            }
                        })
                        .fail(function (data) {
                            d.reject(data);
                        })
                }).promise();
            },
            setBucketAndStoryData = function () {
                var maxCount = appdata.currentProgram().maxStoryCount > 0 ? appdata.currentProgram().maxStoryCount : appdata.currentProgramStoryCount();
                var temp = dc.stories.getByEpisodeId(appdata.currentEpisode().id)();
                var newsCart = dc.newsBucketItems.getAllLocal();
                newsCart = newsCart.sort(function (storyA, storyB) {
                   return new Date(storyB.creationDate()) - new Date(storyA.creationDate());
                });
                if (temp.length === 0) {
                    var tempBucket = newsCart.slice(0, maxCount);
                    for (var i = 0; i < tempBucket.length; i++) {
                        tempBucket[i].isGreen(true);
                        tempBucket[i].isRed(false);
                    }
                    tempBucket = newsCart.slice(maxCount, newsCart.length);
                    for (var i = 0; i < tempBucket.length; i++) {
                        tempBucket[i].isGreen(false);
                        tempBucket[i].isRed(false);
                    }
                }
                else {
                    for (var i = 0; i < newsCart.length; i++) {
                        newsCart[i].isGreen(false);
                        newsCart[i].isRed(false);
                    }
                }
                var list;
                list = temp.slice(0, maxCount);
                for (var i = 0; i < list.length; i++) {
                    list[i].isGreen(true);
                }
                if (temp.length >= maxCount) {
                    list = temp.slice(maxCount, temp.length);
                    for (var i = 0; i < list.length; i++) {
                        list[i].isGreen(false);
                        list[i].isRed(true);
                    }

                }
                appdata.refreshNewsBucket(!appdata.refreshNewsBucket());
            },
            getProgramEpisodes = function (parameters) {
                return $.Deferred(function (d) {
                    if (!appdata.currentProgram().isNullo) {
                        var requestObj = {};
                        if (parameters) {
                            requestObj = {
                                EpisodeId: parameters.episodeId,
                                SlotId: parameters.slotId
                            };
                        } else {
                            requestObj = {
                                ProgramId: appdata.currentProgram().id,
                                DateStr: moment(appdata.programSelectedDate()).toISOString()
                            };
                        }

                        $.when(dataservice.getProgramEpisodes(requestObj))
                            .done(function (responseData) {
                                if (responseData && responseData.IsSuccess && responseData.Data) {
                                    
                                    saveProgramEpisodes(responseData.Data);
                                    var arr = dc.programElements.getByEpisodeId(appdata.currentEpisode().id)();
                                    arr = _.filter(arr, function (tempObj) {
                                        return tempObj.programElementType === e.ProgramElementType.Segment;
                                    });
                                    var tempCount = 0;
                                    for (var i = 0; i < arr.length; i++) {
                                        tempCount += arr[i].storyCount;
                                    }
                                    appdata.currentProgramStoryCount(tempCount);

                                    setBucketAndStoryData();
                                   
                                    d.resolve();
                                } else {
                                    d.reject();
                                }
                            })
                            .fail(function (data) {
                                d.reject();
                            })
                    } else {
                        d.reject();
                    }
                }).promise();
            },
            saveProgramDataOnServer = function (data) {

                var stories = dc.stories.getByEpisodeId(appdata.currentEpisode().id)();
                var input = [];

                var programElements = dc.programElements.getByEpisodeId(appdata.currentEpisode().id)();
                var segmentArray = _.filter(programElements, function (programElement) {
                    return programElement.programElementType === e.ProgramElementType.Segment;
                });
                for (var i = 0; i < stories.length; i++) {
                    var tempObj = {};

                    tempObj["SlotId"] = stories[i].storyId;
                    tempObj["SequnceNumber"] = stories[i].sequenceId;
                    tempObj["SegmentId"] = stories[i].programElementId;
                    tempObj["ThumbGuid"] = stories[i].news.defaultImage();
                    tempObj["IncludeInRundown"] = stories[i].isGreen() ? 1 : 0;

                    if (stories[i].tickerId) {
                        tempObj["TickerId"] = stories[i].tickerId;
                    } else
                        tempObj["NewsGuid"] = stories[i].news.id;

                    if (stories[i].news.parentCategories() && stories[i].news.parentCategories().length > 0) {
                        var catId = null;
                        if (stories[i].news && stories[i].news.categories()) {
                            for (var j = 0; j < stories[i].news.categories().length ; j++) {
                                var obj = stories[i].news.categories()[j];

                                if (obj.Category === stories[i].news.parentCategories()[0]) {
                                    catId = obj.CategoryId;

                                    break;
                                }
                            }
                        }
                        tempObj["CategoryId"] = catId;

                    }

                    if (stories[i].tickerId) {
                        tempObj["CategoryId"] = stories[i].categoryId();
                    }

                    input.push(tempObj);
                }

                var requestObj = {
                    Slots: input,
                    UserId: appdata.currentUser().id,
                    UserRole: appdata.currentUser().userType,
                    NewsStatisticsParam: getNewsStatisticParams()
                };

                return $.Deferred(function (d) {
                    $.when(dataservice.saveProgramData(requestObj))
                        .done(function (responseData) {
                            if (responseData && responseData.IsSuccess && responseData.Data && responseData.Data.length > 0) {
                                var stories = responseData.Data;

                                for (var i = 0; i < stories.length; i++) {

                                    if (stories[i].SlotId) {

                                        var newsBuckets = dc.newsBucketItems.getAllLocal();
                                        for (var j = 0; j < newsBuckets.length; j++) {
                                            if (stories[i].NewsGuid === newsBuckets[j].news.id) {
                                                newsBuckets[j].storyId = stories[i].SlotId;
                                            }
                                        }
                                        var story = dc.stories.getLocalById(stories[i].NewsGuid);
                                        if (stories[i].TickerId)
                                            story = dc.stories.getLocalById('T_' + stories[i].TickerId);

                                        if (story) {
                                            story.storyId = stories[i].SlotId;

                                            var arr = _.filter(dc.screenTemplates.getObservableList(), function (tempTemplate) {
                                                return tempTemplate.programId === appdata.currentProgram().id;
                                            });

                                            if (arr && arr.length == 1) {
                                                if (stories[i].SlotScreenTemplates && stories[i].SlotScreenTemplates.length > 0) {
                                                    var screenFlow = new model.ScreenFlow();
                                                    for (var j = 0; j < stories[i].SlotScreenTemplates.length; j++) {
                                                        var storyTemplate = stories[i].SlotScreenTemplates[j];
                                                        var screenTemplate = mapper.screenTemplate.fromDto(storyTemplate);

                                                        if (screenTemplate) {
                                                            if (storyTemplate.SlotTemplateScreenElements && storyTemplate.SlotTemplateScreenElements.length > 0) {
                                                                for (var k = 0; k < storyTemplate.SlotTemplateScreenElements.length; k++) {
                                                                    var templateElement = mapper.templateElement.fromDto(storyTemplate.SlotTemplateScreenElements[k]);
                                                                    if (templateElement) {
                                                                        templateElement["screenTemplateId"] = screenTemplate.id;
                                                                        screenTemplate.templateElements().push(templateElement);
                                                                    }
                                                                }
                                                            }

                                                            if (storyTemplate.SlotScreenTemplatekeys && storyTemplate.SlotScreenTemplatekeys.length > 0) {
                                                                var templateKeys = [];

                                                                for (var k = 0; k < storyTemplate.SlotScreenTemplatekeys.length; k++) {
                                                                    var templateKey = storyTemplate.SlotScreenTemplatekeys[k];
                                                                    templateKey["ScreenTemplateId"] = storyTemplate.ScreenTemplateId;

                                                                    var temp = mapper.templateKey.fromDto(templateKey);
                                                                    screenTemplate.templateKeys().push(temp);
                                                                }
                                                            }

                                                            screenFlow.screenTemplates.push(screenTemplate);
                                                        }
                                                    }
                                                    story.screenFlow(screenFlow);
                                                }
                                            }
                                        }
                                    }
                                }
                                setBucketAndStoryData();
                                d.resolve();
                            } else {
                                d.reject();
                            }
                        })
                        .fail(function () {
                            d.reject();
                        })
                }).promise();
            },
            saveStoryScreenFlowOnServer = function (story) {
                var input = [];

                for (var i = 0; i < story.screenFlow().screenTemplates().length; i++) {
                    var tempObj = {};

                    tempObj["SlotScreenTemplateId"] = story.screenFlow().screenTemplates()[i].storyScreenTemplateId || 0;
                    tempObj["SlotId"] = story.storyId;
                    tempObj["ScreenTemplateId"] = story.screenFlow().screenTemplates()[i].id;
                    tempObj["SequenceNumber"] = story.screenFlow().screenTemplates()[i].sequenceId;
                    tempObj["ParentId"] = story.screenFlow().screenTemplates()[i].parentId;
                    tempObj["NleStatusId"] = story.screenFlow().screenTemplates()[i].nleStatusId();
                    tempObj["StoryWriterStatusId"] = story.screenFlow().screenTemplates()[i].storywriterStatusId();

                    input.push(tempObj);
                }

                var requestObj = {
                    SlotScreenTemplates: input,
                    ProgramId: appdata.currentProgram().id,
                    EpisodeId: appdata.currentEpisode().id,
                    UserId: appdata.currentUser().id,
                    UserRoleId: appdata.currentUser().userType
                };

                return $.Deferred(function (d) {
                    $.when(dataservice.saveStoryScreenFlow(requestObj))
                        .done(function (responseData) {
                            if (responseData && responseData.IsSuccess && responseData.Data) {
                                var storyTemplates = responseData.Data;

                                if (storyTemplates.length > 0) {
                                    var story = _.find(dc.stories.getByEpisodeId(appdata.currentEpisode().id)(), function (storyObj) {
                                        return storyObj.storyId === storyTemplates[0].SlotId;
                                    });

                                    if (story) {
                                        story.screenFlow(new model.ScreenFlow());
                                        for (var i = 0; i < storyTemplates.length; i++) {
                                            var screenTemplate = dc.screenTemplates.getLocalById(storyTemplates[i].ScreenTemplateId);
                                            if (screenTemplate) {
                                                var tempScreenTemplate = mapper.screenTemplate.getClone(screenTemplate);

                                                tempScreenTemplate.storyScreenTemplateId = storyTemplates[i].SlotScreenTemplateId;
                                                tempScreenTemplate.sequenceId = storyTemplates[i].SequenceNumber;
                                                tempScreenTemplate.script(storyTemplates[i].Script || '');
                                                tempScreenTemplate.isAssignToStoryWriter(storyTemplates[i].IsAssignedToStoryWriter || true);
                                                tempScreenTemplate.isAssignToNLE(storyTemplates[i].IsAssignedToNle || true);

                                                if (storyTemplates[i].ThumbGuid) {
                                                    tempScreenTemplate.thumbGuid(storyTemplates[i].ThumbGuid);
                                                }
                                                if (storyTemplates[i].NleStatusId) {
                                                    tempScreenTemplate.nleStatusId(storyTemplates[i].NleStatusId);
                                                }

                                                if (storyTemplates[i].StoryWriterStatusId) {
                                                    tempScreenTemplate.storywriterStatusId(storyTemplates[i].StoryWriterStatusId);
                                                }
                                                if (storyTemplates[i].ParentId) {
                                                    tempScreenTemplate.parentId = storyTemplates[i].ParentId;
                                                }

                                                if (storyTemplates[i].SlotScreenTemplateResources) {
                                                    for (var j = 0; j < storyTemplates[i].SlotScreenTemplateResources.length; j++) {
                                                        var templateResource = mapper.resource.fromDto(storyTemplates[i].SlotScreenTemplateResources[j]);
                                                        if (templateResource) {
                                                            tempScreenTemplate.contentBucket.push(templateResource);
                                                        }
                                                    }
                                                }

                                                if (storyTemplates[i].SlotTemplateScreenElements && storyTemplates[i].SlotTemplateScreenElements.length > 0) {
                                                    var templateElementArray = [];

                                                    if (tempScreenTemplate.isMediaWallTemplate()) {
                                                        tempScreenTemplate.templateElements([]);
                                                    }
                                                    for (var j = 0; j < storyTemplates[i].SlotTemplateScreenElements.length; j++) {
                                                        var templateElement = storyTemplates[i].SlotTemplateScreenElements[j];
                                                        templateElement["ScreenTemplateId"] = storyTemplates[i].ScreenTemplateId;

                                                        var temp = mapper.templateElement.fromDto(templateElement);
                                                        tempScreenTemplate.templateElements.push(temp);
                                                    }
                                                }

                                                if (storyTemplates[i].SlotScreenTemplatekeys && storyTemplates[i].SlotScreenTemplatekeys.length > 0) {
                                                    var templateKeys = [];
                                                    var tempArr = [];
                                                    for (var j = 0; j < storyTemplates[i].SlotScreenTemplatekeys.length; j++) {
                                                        var templateKey = storyTemplates[i].SlotScreenTemplatekeys[j];
                                                        templateKey["ScreenTemplateId"] = storyTemplates[i].ScreenTemplateId;

                                                        var temp = mapper.templateKey.fromDto(templateKey);
                                                        tempArr.push(temp);
                                                    }
                                                    tempScreenTemplate.templateKeys(tempArr);
                                                }
                                                else {
                                                    var tempArr = [];
                                                    for (var k = 0; k < tempScreenTemplate.templateKeys().length; k++) {
                                                        var tempkeys = mapper.templateKey.getClone(tempScreenTemplate.templateKeys()[k]);
                                                        tempArr.push(tempkeys);
                                                    }
                                                    tempScreenTemplate.templateKeys([]);
                                                    tempScreenTemplate.templateKeys(tempArr);
                                                }

                                                story.screenFlow().screenTemplates.push(tempScreenTemplate);
                                            }
                                        }
                                    }

                                    d.resolve(responseData);
                                }
                            } else {
                                d.reject();
                            }
                        })
                        .fail(function () {
                            d.reject();
                        })
                }).promise();
            },

            saveProductionTeamData = function (screenTemplate, storyId) {
                var input = [];
                var tempObj = {};
                tempObj.SlotScreenTemplateId = screenTemplate.storyScreenTemplateId;
                tempObj.Script = screenTemplate.script();
                tempObj.SlotId = storyId;
                tempObj.StatusId = screenTemplate.statusId();
                if (appdata.currentUser().userType == e.UserType.NLE) {
                    tempObj.NleStatusId = screenTemplate.nleStatusId();
                }

                if (appdata.currentUser().userType == e.UserType.StoryWriter) {
                    tempObj.StoryWriterStatusId = screenTemplate.storywriterStatusId();
                }

                input.push(tempObj);

                var requestObj = {
                    Templates: input,
                    UserId: appdata.currentUser().id,
                    UserRoleId: appdata.currentUser().userType
                };

                return $.Deferred(function (d) {
                    $.when(dataservice.saveProductionTeamData(requestObj))
                    .done(function (responseData) {
                        if (responseData && responseData.IsSuccess && responseData.Data) {
                            d.resolve();
                        }
                        else
                            d.reject();
                    })
                    .fail(function () {
                        d.reject();
                    })
                }).promise();
            },

            sendComments = function (comments, storyScreenTemplateId, toNle, toStoryWriter, toProducer, programId) {
                var requestObj = {
                    Message: comments.trim(),
                    From: appdata.currentUser().id,
                    To: e.UserType.Producer,
                    SlotScreenTemplateId: storyScreenTemplateId,
                    ToNLE: toNle,
                    ToStoryWriter: toStoryWriter,
                    ToProducer: toProducer,
                    ProgramId: programId
                };
                return $.Deferred(function (d) {
                    $.when(dataservice.sendComments(requestObj))
                        .done(function (responseData) {
                            if (responseData && responseData.IsSuccess && responseData.Data) {
                                saveComments([responseData.Data]);
                                d.resolve();
                            } else {
                                d.reject();
                            }
                        })
                        .fail(function () {
                            d.reject();
                        })
                }).promise();
            },

            saveStoryScreenTemplateOnServer = function (screenTemplate, storyId) {
                var templateScreenElements = [];
                var templateResources = [];
                var templateKeyValues = [];

                for (var i = 0; i < screenTemplate.templateElements().length; i++) {
                    var templateElement = screenTemplate.templateElements()[i];
                    var tempElementsResources = [];

                    if (screenTemplate.templateElements()[i].contentBucket && screenTemplate.templateElements()[i].contentBucket().length > 0) {
                        for (var j = 0; j < screenTemplate.templateElements()[i].contentBucket().length; j++) {
                            var resource = screenTemplate.templateElements()[i].contentBucket()[j];
                            var tempResourceObj = {
                                SlotScreenTemplateResourceId: resource.storyScreenTemplateResourceId,
                                SlotScreenTemplateId: screenTemplate.storyScreenTemplateId,
                                ResourceGuid: resource.guid || resource.id,
                                ResourceTypeId: resource.type,
                                Comment: resource.comment(),
                                IsUploadedFromNle: false,
                                SlotTemplateScreenElementId: screenTemplate.templateElements()[i].id
                            };
                            tempElementsResources.push(tempResourceObj);
                        }
                    }

                    var tempObj = {
                        SlotTemplateScreenElementId: templateElement.id,
                        ResourceGuid: templateElement.imageGuid,
                        CelebrityId: templateElement.guestId,
                        SlotTemplateScreenElementResources: tempElementsResources
                    };
                    templateScreenElements.push(tempObj);
                }

                for (var i = 0; i < screenTemplate.contentBucket().length; i++) {
                    var resource = screenTemplate.contentBucket()[i];
                    var tempObj = {
                        SlotScreenTemplateResourceId: resource.storyScreenTemplateResourceId,
                        SlotScreenTemplateId: screenTemplate.storyScreenTemplateId,
                        ResourceGuid: resource.guid || resource.id,
                        Comment: resource.comment(),
                        IsUploadedFromNle: false
                    };
                    templateResources.push(tempObj);
                }

                for (var i = 0; i < screenTemplate.templateKeys().length; i++) {
                    var tempObj = mapper.templateKey.toDto(screenTemplate.templateKeys()[i]);
                    templateKeyValues.push(tempObj);
                }

                var requestObj = {
                    SlotScreenTemplateId: screenTemplate.storyScreenTemplateId,
                    TemplateScreenElementImageUrl: screenTemplate.backgroundUrl(),
                    Script: screenTemplate.script(),
                    Duration: Math.round(screenTemplate.contentDuration()),
                    ScriptDuration: Math.round(screenTemplate.scriptDuration().asSeconds()),
                    VideoDuration: Math.round(screenTemplate.videoDuration()),
                    IsAssignedToNle: screenTemplate.isAssignToNLE(),
                    IsAssignedToStoryWriter: screenTemplate.isAssignToStoryWriter(),
                    SlotTemplateScreenElements: templateScreenElements,
                    SlotScreenTemplatekeys: templateKeyValues,
                    SlotScreenTemplateResources: templateResources,
                    SequenceNumber: screenTemplate.sequenceId,
                    UserId: appdata.currentUser().id,
                    UserRoleId: appdata.currentUser().userType,
                    SlotId: storyId,
                    StatusId: screenTemplate.statusId(),
                    Instructions: screenTemplate.faceAction,
                    IsVideoWallTemplate: screenTemplate.isMediaWallTemplate() ? screenTemplate.isMediaWallTemplate() : false,
                    ProgramId: appdata.currentProgram().id
                };

                if (screenTemplate.isFreshImageGenerated) {
                    screenTemplate.isFreshImageGenerated = false;
                } else {
                    requestObj.TemplateScreenElementImageGuid = screenTemplate.thumbGuid();
                }

                return $.Deferred(function (d) {
                    $.when(dataservice.saveScreenTemplate(requestObj))
                        .done(function (responseData) {
                            if (responseData && responseData.IsSuccess && responseData.Data) {
                                if (responseData.Data.length > 0 && responseData.Data[0].ThumbGuid) {
                                    screenTemplate.thumbGuid(responseData.Data[0].ThumbGuid);
                                }
                                d.resolve();
                            } else {
                                d.reject();
                            }
                        })
                        .fail(function () {
                            d.reject();
                        })
                }).promise();
            },

            saveMediaWallTemplateOnServer = function (mediawallTemplate, storyId) {
                var tempTemplateScreenElements = [];
                var templateResources = [];
                var templateKeyValues = [];

                for (var i = 0; i < mediawallTemplate.templateElements().length; i++) {
                    var templateElement = mediawallTemplate.templateElements()[i];
                    var tempElementsResources = [];

                    if (mediawallTemplate.templateElements()[i].contentBucket && mediawallTemplate.templateElements()[i].contentBucket().length > 0) {
                        for (var j = 0; j < mediawallTemplate.templateElements()[i].contentBucket().length; j++) {
                            var resource = mediawallTemplate.templateElements()[i].contentBucket()[j];
                            var tempResourceObj = {
                                SlotScreenTemplateResourceId: resource.storyScreenTemplateResourceId,
                                SlotScreenTemplateId: mediawallTemplate.storyScreenTemplateId,
                                ResourceGuid: resource.guid || resource.id,
                                ResourceTypeId: resource.type,
                                Comment: resource.comment(),
                                IsUploadedFromNle: false,
                                SlotTemplateScreenElementId: mediawallTemplate.templateElements()[i].storyTemplateScreenElementId
                            };
                            tempElementsResources.push(tempResourceObj);
                        }
                    }

                    var tempObj = {
                        SlotTemplateScreenElementId: mediawallTemplate.templateElements()[i].id ? mediawallTemplate.templateElements()[i].id : mediawallTemplate.templateElements()[i].storyTemplateScreenElementId,
                        ResourceGuid: templateElement.imageGuid,
                        CelebrityId: templateElement.guestId,
                        SlotTemplateScreenElementResources: tempElementsResources
                    };
                    tempTemplateScreenElements.push(tempObj);
                }

                var requestObj = {
                    SlotScreenTemplateId: mediawallTemplate.storyScreenTemplateId,
                    TemplateScreenElementImageUrl: mediawallTemplate.backgroundUrl(),
                    Duration: Math.round(mediawallTemplate.contentDuration()),
                    ScriptDuration: Math.round(mediawallTemplate.scriptDuration().asSeconds()),
                    VideoDuration: Math.round(mediawallTemplate.videoDuration()),
                    IsAssignedToNle: mediawallTemplate.isAssignToNLE(),
                    IsAssignedToStoryWriter: mediawallTemplate.isAssignToStoryWriter(),
                    SlotTemplateScreenElements: tempTemplateScreenElements,
                    SlotScreenTemplatekeys: templateKeyValues,
                    SlotScreenTemplateResources: templateResources,
                    SequenceNumber: mediawallTemplate.sequenceId,
                    UserId: appdata.currentUser().id,
                    ScreenTemplateId: mediawallTemplate.id,
                    UserRoleId: appdata.currentUser().userType,
                    SlotId: storyId,
                    ParentId: mediawallTemplate.parentId,
                    IsVideoWallTemplate: mediawallTemplate.isMediaWallTemplate() ? mediawallTemplate.isMediaWallTemplate() : false
                };

                if (mediawallTemplate.isFreshImageGenerated) {
                    mediawallTemplate.isFreshImageGenerated = false;
                } else {
                    requestObj.TemplateScreenElementImageGuid = mediawallTemplate.thumbGuid();
                }

                return $.Deferred(function (d) {
                    $.when(dataservice.saveMediaWallTemplateOnServer(requestObj))
                        .done(function (responseData) {
                            if (responseData && responseData.IsSuccess && responseData.Data) {
                                mediawallTemplate.thumbGuid(responseData.Data[0].ThumbGuid);
                                d.resolve(responseData);
                            } else {
                                d.reject();
                            }
                        })
                        .fail(function () {
                            d.reject();
                        })
                }).promise();
            },

            removeStoryOnServer = function (story) {


                var newsbucketId = story instanceof model.NewsBucketItem ? story.id : 0;


                var requestObj = {
                    SlotId: story.storyId ? story.storyId : 0,
                    NewsGuid: story.news.id,
                    EpisodeId: appdata.currentEpisode().id,
                    NewsBucketId: newsbucketId
                };

                return $.Deferred(function (d) {
                    presenter.toggleActivity(true);
                    $.when(dataservice.removeStory(requestObj))
                        .done(function (responseData) {
                            if (responseData && responseData.IsSuccess && responseData.Data) {

                                story.news.isUsed(false)
                          

                                if (story instanceof model.NewsBucketItem) {
                                    story.isGreen(false);
                                    story.isRed(false);
                                    dc.newsBucketItems.removeById(story.id, true);
                                    dc.newsBucketItems.removeFromDictionary(story.id, story.programId, 'program');
                                }

                                dc.stories.removeById(story.id, true);
                                dc.stories.removeFromDictionary(story.id, story.episodeId, 'episode');

                                var segment = dc.programElements.getLocalById(story.programElementId);
                                if (segment) {
                                    dc.stories.removeFromDictionary(story.id, segment.id, 'program-element');
                                    dc.programElements.getLocalById(segment.id).stories.valueHasMutated();
                                }
                                logger.success("Story deleted successfully!");
                                presenter.toggleActivity(false);
                                appdata.arrangeNewsFlag(!appdata.arrangeNewsFlag());
                                d.resolve(responseData);
                            } else {
                                logger.error("Unable to delete due to some error!");
                                presenter.toggleActivity(false);
                                d.reject();
                            }
                        })
                        .fail(function () {
                            logger.error("Unable to delete due to some error!");
                            presenter.toggleActivity(false);
                            d.reject();
                        })
                }).promise();
            },

            sendProgramToBroadcast = function () {
                logger.info('Please wait ...');
                var requestObj = {
                    NewsStatisticsParam: getNewsStatisticParams()
                };

                return $.Deferred(function (d) {
                    $.when(dataservice.sendProgramToBroadCast(requestObj))
                        .done(function (responseData) {
                            if (responseData && responseData.IsSuccess && responseData.Data) {
                                logger.success('Program sent to broadcast');
                                d.resolve();
                            } else {
                                logger.error(responseData.Errors[0]);
                                d.reject();
                            }
                        })
                        .fail(function (d) {
                            logger.error('An error occured ');
                            d.reject();
                        })
                }).promise();
            },

            generateEpisodePreview = function (btnAttribute, ppc, isStoryPreview, data) {
                var requestObj = {
                    EpisodeId: appdata.currentEpisode().id
                };

                if (isStoryPreview)
                    logger.success("Generating story preview ...");

                return $.Deferred(function (d) {
                    $.when(dataservice.generateEpisodePreview(requestObj))
                        .done(function (responseData) {
                            if (responseData && responseData.IsSuccess && responseData.Data) {
                                var story;
                                if (isStoryPreview)
                                    story = dc.stories.getLocalById(data.id);

                                appdata.currentEpisode().previewGuid(responseData.Data.EpisodePreview);
                                var stories = dc.stories.getByEpisodeId(appdata.currentEpisode().id)();
                                if (stories && stories.length > 0) {
                                    for (var i = 0; i < stories.length; i++) {
                                        var story = stories[i];
                                        var guid = responseData.Data.SlotPreviews[story.storyId]
                                        if (guid.length > 0)
                                            story.previewGuid(guid);
                                    }
                                }

                                var url = isStoryPreview && story ? story.previewUrl() : appdata.currentEpisode().previewUrl();
                                ppc.programPreviewUrl(url);
                                var message = isStoryPreview ? "Story preview generated!" : "Episode preview generated!";
                                btnAttribute({ text: "Preview", disabled: false, disabledStoryBtn: false });
                                logger.success(message);
                                ppc.isVisible(true);
                                d.resolve();
                            } else {
                                logger.error("An Error occured");
                                d.reject();
                            }
                        })
                        .fail(function (d) {
                            logger.error('An error occured ');
                            d.reject();
                        })
                }).promise();
            },

            getMorePendingStories = function () {
                $.Deferred(function (d) {
                }).promise();
            },

            saveScreenTemplateDataForNLE = function (screenTemplate, resources) {
                var templateScreenElements = [];
                for (var i = 0; i < screenTemplate.templateElements().length; i++) {
                    var templateElement = screenTemplate.templateElements()[i];

                    var tempElementsResources = [];

                    if (screenTemplate.templateElements()[i].contentBucket && screenTemplate.templateElements()[i].contentBucket().length > 0) {
                        for (var j = 0; j < screenTemplate.templateElements()[i].contentBucket().length; j++) {
                            var resource = screenTemplate.templateElements()[i].contentBucket()[j];
                            var tempResourceObj = {
                                SlotScreenTemplateResourceId: resource.storyScreenTemplateResourceId,
                                SlotScreenTemplateId: screenTemplate.storyScreenTemplateId,
                                ResourceGuid: resource.guid || resource.id,
                                IsUploadedFromNle: true,
                                ResourceTypeId: resource.type,
                                SlotTemplateScreenElementId: screenTemplate.templateElements()[i].id
                            };
                            tempElementsResources.push(tempResourceObj);
                        }
                    }

                    var tempObj = {
                        SlotTemplateScreenElementId: templateElement.id,
                        ResourceGuid: templateElement.imageGuid,
                        CelebrityId: templateElement.guestId,
                        SlotTemplateScreenElementResources: tempElementsResources,
                        SlotScreenTemplateId: screenTemplate.storyScreenTemplateId
                    };
                    templateScreenElements.push(tempObj);
                }

                var input = [];
                var tempObj = {};
                tempObj.SlotScreenTemplateId = screenTemplate.storyScreenTemplateId;
                tempObj.SlotScreenTemplateResources = resources;
                tempObj.SlotTemplateScreenElements = templateScreenElements

                input.push(tempObj);

                var requestObj = {
                    Templates: input,
                    UserId: appdata.currentUser().id,
                    UserRoleId: appdata.currentUser().userType
                };

                return $.Deferred(function (d) {
                    $.when(dataservice.saveScreenTemplateDataForNLE(requestObj))
                        .done(function (responseData) {
                            if (responseData && responseData.IsSuccess && responseData.Data) {
                                d.resolve(responseData);
                            } else {
                                d.reject(responseData);
                            }
                        })
                        .fail(function () {
                            d.reject(responseData);
                        })
                }).promise();
            },

            deleteSlotscreentemplate = function (data) {
                var requestObj = {
                    id: data,
                    NewsStatisticsParam: getNewsStatisticParams()
                };
                return $.Deferred(function (d) {
                    $.when(dataservice.deleteslotscreenTemplate(requestObj))
                        .done(function (responseData) {
                            if (responseData && responseData.IsSuccess && responseData.Data) {
                                d.resolve(responseData);
                            } else {
                                d.reject();
                            }
                        })
                        .fail(function () {
                            d.reject();
                        })
                }).promise();
            },

            deleteSlotScreenTemplateResource = function (id) {
                var requestObj = {
                    id: id
                };

                return $.Deferred(function (d) {
                    $.when(dataservice.deleteStoryScreenTemplateResource(requestObj))
                        .done(function (responseData) {
                            if (responseData && responseData.IsSuccess && responseData.Data) {
                                d.resolve(responseData);
                            } else {
                                d.reject();
                            }
                        })
                        .fail(function () {
                            d.reject();
                        })
                }).promise();
            },

            getMoreHeadlines = function () {
                var requestObj = {
                    UserId: appdata.currentUser().id,
                    PageCount: appdata.lpPageCountHeadline,
                    PageIndex: appdata.lpStartIndexHeadline,
                    SearchTerm: appdata.searchHeadlineKeywords,
                    FromDateStr: appdata.lpHeadlineFromDate,
                    ToDateStr: appdata.lpHeadlineToDate,
                }
                return $.Deferred(function (d) {
                    $.when(dataservice.getMoreHeadlines(requestObj))
                        .done(function (responseData) {
                            if (responseData && responseData.IsSuccess && responseData.Data && responseData.Data.Episodes) {

                                saveProgramEpisodes(responseData.Data.Episodes);
                                setDataChangeTime();

                                d.resolve(responseData);
                            } else {
                                d.reject();
                            }
                        })
                        .fail(function () {
                            d.reject();
                        })
                }).promise();
            },

            getSlotScreenTemplateComments = function (id) {
                var requestObj = {
                    id: id
                };

                appdata.currentStoryTemplateId = id;

                return $.Deferred(function (d) {
                    $.when(dataservice.getSlotScreenTemplateComments(requestObj))
                        .done(function (responseData) {
                            if (responseData && responseData.IsSuccess && responseData.Data) {
                                saveComments(responseData.Data);
                                d.resolve(responseData);
                            } else {
                                d.reject();
                            }
                        })
                        .fail(function () {
                            d.reject();
                        })
                }).promise();
            },

            getTaskDoneNLEOrStorywriter = function (templateId) {
                var requestObj = {
                    templateId: templateId
                };
                return $.Deferred(function (d) {
                    $.when(dataservice.getTaskDoneNLEOrStorywriter(requestObj))
                        .done(function (responseData) {
                            if (responseData) {
                                d.resolve(responseData);
                            } else {
                                d.reject();
                            }
                        })
                        .fail(function () {
                            d.reject();
                        })
                }).promise();
            },

            croppImage = function (requestObj) {
                return $.Deferred(function (d) {
                    $.when(dataservice.croppImage(requestObj))
                        .done(function (responseData) {
                            if (responseData && responseData.IsSuccess && responseData.Data) {
                                d.resolve(responseData);
                            } else {
                                d.reject(responseData);
                            }
                        })
                        .fail(function (data) {
                            d.reject(data);
                        })
                }).promise();
            },

            mediaPostResource = function (requestObj) {
                return $.Deferred(function (d) {
                    $.when(dataservice.mediaPostResource(requestObj))
                        .done(function (responseData) {
                            if (responseData && responseData.IsSuccess && responseData.Data) {
                                d.resolve(responseData);
                            } else {
                                d.reject(responseData);
                            }
                        })
                        .fail(function (data) {
                            d.reject(data);
                        })
                }).promise();
            },

            getMediaResourceDownloadStatus = function (requestObj) {
                return $.Deferred(function (d) {
                    $.when(dataservice.getMediaResourceDownloadStatus(requestObj))
                        .done(function (responseData) {
                            if (responseData) {
                                d.resolve(responseData);
                            } else {
                                d.reject(responseData);
                            }
                        })
                        .fail(function (data) {
                            d.reject(data);
                        })
                }).promise();
            },

            searchContentArchive = function (requestObj, archivalServerCount, metaTags, isAdvanceSearchEnable) {
                return $.Deferred(function (d) {

                    if (appdata.currentEpisode && appdata.currentEpisode().programId) {
                        requestObj.bucketId = _dc.programs.getLocalById(appdata.currentEpisode().programId).bucketId;
                        if (!requestObj.bucketId) {
                            requestObj.bucketId = 0;
                        }
                    }
                    else {
                        requestObj.bucketId = 0;
                    }

                    requestObj.bucketId = requestObj.bucketId == 0 ? -1 : requestObj.bucketId;
                    requestObj.resourceTypeId = requestObj.resourceTypeId != 0 ? requestObj.resourceTypeId : null

                    if (isAdvanceSearchEnable) {
                        requestObj.alldata = true;
                        var tempMeta = [];
                        for (var i = 0; i < metaTags.length; i++) {
                            if (i == 0) {
                                tempMeta = metaTags[i].trim();
                            }
                            else {
                                tempMeta = tempMeta + ',' + metaTags[i];
                            }

                        }

                        requestObj.metaTags = tempMeta;

                        if ((requestObj.metaTags && requestObj.metaTags.length > 0) || (requestObj.term && requestObj.term.trim().length > 0)) {
                            $.when(dataservice.searchResourceByMeta(requestObj))
                                            .done(function (responseData) {
                                                if (responseData && responseData.Resources) {
                                                    var arr = [];
                                                    if (responseData.Count && responseData.Count > 0) {
                                                        archivalServerCount(responseData.Count);
                                                        for (var i = 0; i < responseData.Resources.length; i++) {
                                                            var item = mapper.resource.fromDto(responseData.Resources[i]);
                                                            _.filter(appdata.userFavourites(), function (favourite) {
                                                                if (favourite.guid == item.guid) {
                                                                    item.isMarkedFavourite(true);
                                                                }
                                                            });
                                                            arr.push(item);
                                                        }
                                                
                                                        presenter.toggleActivity(false);
                                                    }
                                                    else if (!responseData.Count) {
                                                        archivalServerCount(0);
                                                
                                                        presenter.toggleActivity(false);
                                                    }
                                                    d.resolve(arr);
                                                } else {
                                                 
                                                    presenter.toggleActivity(false);
                                                    d.reject(responseData);
                                                }
                                            })
                                            .fail(function (data) {
                                                
                                                presenter.toggleActivity(false);
                                                d.reject(data);
                                            })
                        }
                    }
                    else {
                        if (requestObj.term && requestObj.term.trim().length > 0) {
                            $.when(dataservice.searchResource(requestObj))
                            .done(function (responseData) {
                                if (responseData && responseData.Resources) {

                                    var arr = [];
                                    if (responseData.Count && responseData.Count > 0) {
                                        archivalServerCount(responseData.Count);
                                        for (var i = 0; i < responseData.Resources.length; i++) {
                                            var item = mapper.resource.fromDto(responseData.Resources[i]);
                                            _.filter(appdata.userFavourites(), function (favourite) {
                                                if (favourite.guid == item.guid) {
                                                    item.isMarkedFavourite(true);
                                                }
                                            });
                                            arr.push(item);

                                        }
                                        presenter.toggleActivity(false);
                                    }
                                    else if (!responseData.Count) {
                                        archivalServerCount(0);
                                        presenter.toggleActivity(false);
                                    }

                                    d.resolve(arr);
                                } else {
                                    d.resolve(responseData);
                                    presenter.toggleActivity(false);
                                }
                            })
                            .fail(function (data) {
                                d.reject(data);
                                presenter.toggleActivity(false);
                            })
                        }
                    }
                }).promise();

            },

            searchCelebrity = function (requestObj) {
                return $.Deferred(function (d) {
                    $.when(dataservice.searchCelebrity(requestObj))
                    .done(function (responseData) {
                        if (responseData && responseData.Data) {
                            d.resolve(responseData);
                        } else {
                            d.reject(responseData);
                        }
                    })
                    .fail(function (data) {
                        d.reject(data);
                    })
                }).promise();
            },
           
            getResourceByDate = function (requestObj, archivalServerCount, metaTags, isAdvanceSearchEnable,  showloader) {
                return $.Deferred(function (d) {
                    $.when(dataservice.getResourceByDate(requestObj))
                    .done(function (responseData) {
                        if (responseData && responseData.Resources) {
                            var arr = [];
                            if (responseData.Count && responseData.Count > 0) {
                                archivalServerCount(responseData.Count);
                                newsManager.saveResources(responseData.Resources, true);
                                presenter.toggleActivity(false);
                                showloader(false);
                            }
                            else if (!responseData.Count) {
                                presenter.toggleActivity(false);
                                archivalServerCount(0);
                                showloader(false);
                            }
                            d.resolve(arr);
                        } else {
                            presenter.toggleActivity(false);
                            d.reject(responseData);
                            showloader(false);
                        }
                    })
                    .fail(function (data) {
                        presenter.toggleActivity(false);
                        d.reject(data);
                        showloader(false);
                    })
                }).promise();
            },
            searchResourceMetaByGuid = function (data) {
                var requestObj = {};

                requestObj.Guid = data.guid;

                return $.Deferred(function (d) {
                    $.when(dataservice.searchResourceMetaByGuid(requestObj))
                    .done(function (responseData) {
                        if (responseData && responseData.ResourceMetas) {
                            var resourceMetas = responseData.ResourceMetas;
                            resourceMetas = utils.arrayOrderBy(resourceMetas, "Name", "asc");

                            for (var i = 0,len = resourceMetas.length; i < len; i++) {
                                if (resourceMetas[i].Name == "HighResolutionFile" || resourceMetas[i].Name == "LowResolutionFile") {
                                    if (resourceMetas[i].Name == "HighResolutionFile") {
                                        data.highResDownloadUrl = resourceMetas[i].Value;
                                    }
                                    else if (resourceMetas[i].Name == "LowResolutionFile") {
                                        data.lowResDownloadUrl = resourceMetas[i].Value;
                                    }
                                    resourceMetas.remove(resourceMetas[i]);
                                }
                            }
                            data.resourceMetas(resourceMetas);
                            if (responseData.Caption) {
                                data.isMetaWithData(true);
                                data.captions(responseData.Caption);
                            }
                               
                            if (responseData.Location) {
                                data.isMetaWithData(true);
                                data.location(responseData.Caption);
                            }
                               
                            if (responseData.Cateogry) {
                                data.isMetaWithData(true);
                                data.category(responseData.Cateogry);
                            }
                               


                            d.resolve(resourceMetas);
                        }
                    })
                    .fail(function (data) {
                        d.reject(data);
                    })
                }).promise();
            },

            updateCelebrityStatistics = function (requestObj) {
                requestObj.NewsStatisticsParam = getNewsStatisticParams();

                return $.Deferred(function (d) {
                    $.when(dataservice.updateCelebrityStatistics(requestObj))
                    .done(function (responseData) {
                        if (responseData && responseData.IsSuccess) {
                            var arr = [];
                            d.resolve(arr);
                        }
                    })
                    .fail(function (data) {
                        d.reject(data);
                    })
                }).promise();
            },

            markNotificaitonAsRead = function (requestObj) {
                return $.Deferred(function (d) {
                    $.when(dataservice.markNotificaitonRead(requestObj))
                        .done(function (responseData) {
                            if (responseData) {
                                d.resolve(responseData);
                            } else {
                                d.reject(responseData);
                            }
                        })
                        .fail(function (data) {
                            d.reject(data);
                        })
                }).promise();
            },

            getUrduUnicodeText = function (data) {
                var requestObj = { Text: data };
                return $.Deferred(function (d) {
                    $.when(dataservice.getUrduUnicodeText(requestObj))
                        .done(function (responseData) {
                            if (responseData && responseData.Data) {
                                d.resolve(responseData.Data);
                            } else {
                                d.reject(responseData);
                            }
                        })
                        .fail(function (data) {
                            d.reject(data);
                        })
                }).promise();
            },

            addUserFavouriteResource = function (data) {
                return $.Deferred(function (d) {
                    var tempObj = {
                        ResourceGuid: data.guid,
                        UserId: appdata.currentUser().id
                    }
                    $.when(dataservice.addUserFavourite(tempObj))
                        .done(function (responseData) {
                            if (responseData) {
                                d.resolve(responseData);
                            } else {
                                d.reject(responseData);
                            }
                        })
                        .fail(function (data) {
                            d.reject(data);
                        })
                }).promise();
            },

            deleteUserFavouriteResource = function (data) {
                return $.Deferred(function (d) {
                    var tempObj = {
                        ResourceGuid: data.guid,
                        UserId: appdata.currentUser().id
                    }
                    $.when(dataservice.deleteUserFavourite(tempObj))
                        .done(function (responseData) {
                            if (responseData) {
                                d.resolve(responseData);
                            } else {
                                d.reject(responseData);
                            }
                        })
                        .fail(function (data) {
                            d.reject(data);
                        })
                }).promise();
            },
            updateNewsBucketSequence = function (reqObj) {
                var listNewsBucket = dc.newsBucketItems.getAllLocal();

                var inputList = [];

                for (var i = 0, len = listNewsBucket.length ; i < len; i++) {
                    var obj = {};
                    obj.SequnceNumber = listNewsBucket[i].sequenceId;
                    obj.NewsBucketId = listNewsBucket[i].id;
                    inputList.push(obj)
                }
                var reqObj = {
                    News: inputList
                }
                return $.Deferred(function (d) {
                    $.when(dataservice.updateNewsBucketSequence(reqObj))
                        .done(function (responseData) {
                            d.resolve(responseData);
                        })
                        .fail(function (responseData) {
                            d.reject();
                        });
                }).promise();
            },
            updateResource = function (reqObj) {
                return $.Deferred(function (d) {
                    $.when(dataservice.updateResource(reqObj))
                        .done(function (responseData) {
                            d.resolve(responseData);
                        })
                        .fail(function (responseData) {
                            d.reject();
                        });
                }).promise();
            },
            markTemplateStatus = function (reqObj) {
                return $.Deferred(function (d) {
                    $.when(dataservice.markTemplateStatus(reqObj))
                        .done(function (responseData) {
                            d.resolve(responseData);
                        })
                        .fail(function (responseData) {
                            d.reject();
                        });
                }).promise();
            };

           changeUserPassword = function (reqObj) {
               return $.Deferred(function (d) {
                   $.when(dataservice.changePassword(reqObj))
                       .done(function (responseData) {
                           d.resolve(responseData);
                       })
                       .fail(function (responseData) {
                           d.reject();
                       });
               }).promise();
           };


        //#endregion

        return {
            loadInitialDataProductionTeam: loadInitialDataProductionTeam,
            generateScreenTemplate: generateScreenTemplate,
            proceedToSequence: proceedToSequence,
            getProgramScreenFlow: getProgramScreenFlow,
            getCelebrityByTerm: getCelebrityByTerm,
            getProgramEpisodes: getProgramEpisodes,
            saveProgramDataOnServer: saveProgramDataOnServer,
            saveStoryScreenFlowOnServer: saveStoryScreenFlowOnServer,
            saveStoryScreenTemplateOnServer: saveStoryScreenTemplateOnServer,
            removeStoryOnServer: removeStoryOnServer,
            sendProgramToBroadcast: sendProgramToBroadcast,
            saveProgramEpisodes: saveProgramEpisodes,
            toggleStoryFilter: toggleStoryFilter,
            getFilteredPendingStories: getFilteredPendingStories,
            getPendingStoriesByFilter: getPendingStoriesByFilter,
            productionTeamPolling: productionTeamPolling,
            saveProductionTeamData: saveProductionTeamData,
            getMorePendingStories: getMorePendingStories,
            sendComments: sendComments,
            proceedToProduction: proceedToProduction,
            deleteSlotscreentemplate: deleteSlotscreentemplate,
            saveScreenTemplateDataForNLE: saveScreenTemplateDataForNLE,
            deleteSlotScreenTemplateResource: deleteSlotScreenTemplateResource,
            getSlotScreenTemplateComments: getSlotScreenTemplateComments,
            croppImage: croppImage,
            searchContentArchive: searchContentArchive,
            markNotificaitonAsRead: markNotificaitonAsRead,
            getSuggestedFlow: getSuggestedFlow,
            generateEpisodePreview: generateEpisodePreview,
            updateCelebrityStatistics: updateCelebrityStatistics,
            getMoreHeadlines: getMoreHeadlines,
            getFilteredHeadlines: getFilteredHeadlines,
            toggleHeadlineFilter: toggleHeadlineFilter,
            setDataChangeTime: setDataChangeTime,
            getTaskDoneNLEOrStorywriter: getTaskDoneNLEOrStorywriter,
            searchCelebrity: searchCelebrity,
            saveMediaWallTemplateOnServer: saveMediaWallTemplateOnServer,
            mediaPostResource: mediaPostResource,
            getMediaResourceDownloadStatus: getMediaResourceDownloadStatus,
            searchResourceMetaByGuid: searchResourceMetaByGuid,
            addUserFavouriteResource: addUserFavouriteResource,
            deleteUserFavouriteResource: deleteUserFavouriteResource,
            getUrduUnicodeText: getUrduUnicodeText,
            updateResource: updateResource,
            markTemplateStatus: markTemplateStatus,
            updateNewsBucketSequence: updateNewsBucketSequence,
            changeUserPassword: changeUserPassword,
            getResourceByDate: getResourceByDate
        };
    });