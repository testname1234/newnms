﻿define('manager.tags',
    [
        'dataservice',
        'datacontext'
    ],
    function (dataservice, dc) {
        var

            loadResourceAgainstTag = function (tag) {
                return $.Deferred(function (d) {
                    $.when(dataservice.loadResourceAgainstTag(tag))
                     .done(function (responseData) {
                         if (responseData.IsSuccess) {
                            
                             d.resolve(responseData.Data);
                         }
                     })
                     .fail(function (responseData) {
                         d.reject(responseData);
                     });
                }).promise();
            },


            loadTags = function () {
            	return $.Deferred(function (d) {
            		$.when(dataservice.loadTags())
                     .done(function (responseData) {
                     	d.resolve(responseData);
                     })
                     .fail(function (responseData) {
                         d.reject(responseData);
                     });
            	}).promise();
            };
    	return {
    	    loadTags: loadTags,
    	    loadResourceAgainstTag: loadResourceAgainstTag
    	}
    });