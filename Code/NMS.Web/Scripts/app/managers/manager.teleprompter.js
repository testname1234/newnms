﻿define('manager.teleprompter',
    [
        'dataservice',
        'datacontext'
    ],
    function (dataservice, dc) {
    	var
            play = function () {
            	return $.Deferred(function (d) {
            		$.when(dataservice.play())
                     .done(function (responseData) {
                     	d.resolve(responseData);
                     })
                     .fail(function (responseData) {
                     	d.reject(responseData);
                     });
            	}).promise();
            };
    	return {
    		play: play
    	}
    });