﻿define('manager.ticker',
    [
        'jquery',
        'enum',
        'dataservice',
        'datacontext',
        'underscore',
        'utils',
        'appdata',
        'config',
        'model',
        'model.mapper',
        'presenter',
        'router'
    ],
    function ($, e, dataservice, dc, _, utils, appdata, config, model, mapper, presenter, router) {

        var
            // #region Local Data Processing

           saveTickerData = function (data, isFromPolling) {
               if (data && data.length > 0) {
                   for (var i = 0; i < data.length; i++) {
                       if (data[i].TickerLines && data[i].TickerLines.length > 0) {
                           var tempDate = dc.tickerLines.fillData(data[i].TickerLines, { sort: true, groupByTickerId: true });
                       }
                   }
                   var tempDate = dc.tickers.fillData(data, { sort: true });
                   setTickerLastUpdateDate();
                   setTickerLineLastUpdateDate();
               }
           },
           setTickerLastUpdateDate = function () {
               var allticker = dc.tickers.getAllLocal();
               var topDate;
                if (allticker && allticker.length > 1) {
                    var temp = allticker.sort(function (entity1, entity2) {
                        return entity1.lastUpdateDate() < entity2.lastUpdateDate() ? 1 : -1;
                    });
                    topDate = temp[0].lastUpdateDate();
                    
                }
                else if (allticker && allticker.length == 1) {
                    topDate = allticker[0].lastUpdateDate();
                }
                appdata.tickerLastUpdateDate = topDate || appdata.tickerLastUpdateDate;
           },
           setTickerLineLastUpdateDate = function () {
               var alltickerLines = dc.tickerLines.getAllLocal();
               var topDate;
               if (alltickerLines && alltickerLines.length > 1) {
                   var temp = alltickerLines.sort(function (entity1, entity2) {
                       return entity1.lastUpdateDate() < entity2.lastUpdateDate() ? 1 : -1;
                   });

                   topDate = temp[0].lastUpdateDate();
               }
               else if (alltickerLines && alltickerLines.length == 1) {
                   topDate = alltickerLines[0].lastUpdateDate();
               }
               appdata.tickerLineLastUpdateDate = topDate || appdata.tickerLastUpdateDate;
           },
         
           saveTickerLine = function (data) {
               if (data && data.length > 0) {
                   
                   for (var i = 0; i < data.length; i++) {
                       var existingLine = checkForExistingTickerLine(data[i].OperatorNumber, data[i].TickerId);
                       if (existingLine && existingLine.length == 1) {
                           //updateTickerLineDataContext(existingLine[0], data[i]);
                           dc.tickerLines.removeById(existingLine[0].id, true);
                       }
                   }
                   dc.tickerLines.fillData(data, { addToObservable: true, sort: true, groupByTickerId: true });
                   setTickerLineLastUpdateDate();
               }
           },
           updateTickerLineDataContext = function (existingLine,newTickerLine) {
               existingLine.sequenceId = newTickerLine.SequenceId;
               existingLine.operatorNo = newTickerLine.OperatorNumber;
               existingLine.lastUpdateDate(newTickerLine.LastUpdatedDateStr);
               existingLine.isShow(newTickerLine.IsShow);
               existingLine.isShow(newTickerLine.CreatedBy);
               existingLine.title(newTickerLine.Text);
               dc.tickerLines.observableList.valueHasMutated();
           },
           saveCategoryData = function (data) {
                if (data && data.length > 0) {
                    var temp = data.sort(function (entity1, entity2) {
                        return entity1.LastUpdateDateStr < entity2.LastUpdateDateStr ? 1 : -1;
                    });

                    appdata.tickerCategoryLastUpdateDate = temp[0].LastUpdateDateStr || appdata.tickerCategoryLastUpdateDate;
                    dc.categories.fillData(data);
                }
            },

            // #endregion

            // #region Remote Calls

            saveCategoryDataOnServer = function () {
                var arr = dc.categories.getObservableList();
                presenter.toggleActivity(true);
                var list = [];
                for (var i = 0, len = arr.length; i < len; i++) {
                    var req = {
                        CategoryId: arr[i].id,
                        SequenceNumber: arr[i].sequenceNumber
                    }
                    list.push(req);
                }
                var reqObj = {
                    TickerCategories: list
                };
                return $.Deferred(function (d) {
                    $.when(dataservice.saveTickerCategoryData(reqObj))
                        .done(function (responseData) {
                            if (responseData && responseData.IsSuccess && responseData.Data) {
                                presenter.toggleActivity(false);
                            } else {
                                presenter.toggleActivity(false);
                                d.reject();
                            }
                        })
                        .fail(function () {
                            config.logger.error("Error occured!");
                            d.reject();
                            presenter.toggleActivity(false);
                        })
                }).promise();
            },
            createSegment = function (groupName, type,tickerId) {
              

                var reqObj = {
                    TickerGroupName: groupName,
                    TickerTypeId: type,
                    UserId: appdata.currentUser().id,
                    CreatedBy: appdata.currentUser().id,
                    TickerId:tickerId > 0  ? tickerId :  null,
                    IsShow: true
                }
                return $.Deferred(function (d) {
                    $.when(dataservice.createSegment(reqObj))
                        .done(function (responseData) {
                            if (responseData.IsSuccess && responseData.Data) {
                                var temp = [];
                                temp.push(responseData.Data);
                                appdata.currentWorkingTicker(responseData.Data.TickerId);
                                saveTickerData(temp);

                                config.logger.success("Segment created successfully!");
                                d.resolve(responseData);
                            } else {
                                d.reject();
                            }
                        })
                        .fail(function () {
                            d.reject();
                        })
                }).promise();
            },
           checkForExistingTickerLine = function (operatorNumber,tickerId) {

               var tickerList = _.filter(dc.tickers.getAllLocal(), function (item) {
                   return item.id === tickerId;
               });
               var removeTickerLine = [];

               for (var i = 0, len = tickerList.length; i < len; i++) {
                   for (var j = 0, len2 = tickerList[i].tickerLines().length; j < len2; j++) {
                       if (tickerList[i].tickerLines()[j].operatorNo === operatorNumber && tickerList[i].tickerLines()[j].tickerId === tickerId) {
                           removeTickerLine.push(tickerList[i].tickerLines()[j]);
                           break;
                       }
                   }
               }
               return removeTickerLine;
           },
           removeExistingTickerLineOnSeqId = function (removeTickerLine) {
               for (var i = 0; i < removeTickerLine.length; i++) {
                   dc.tickerLines.removeById(removeTickerLine[i].id, true);
               }
           },
           insertOnAirTickerLine = function (data,type) {

               var tickerLines = [];
               tickerLines = _.filter(dc.tickerLines.getAllLocal(), function (item) {
                   return item.tickerId === data.id;
               });

               var existingTickerLines = checkForExistingTickerLine(data.currentTickerOperator(),data.id);

               var tempTickerLineObj = {
                   Text: data.title(),
                   LanguageCode: 'ur',
                   SequenceId: existingTickerLines.length == 0 ? tickerLines.length + 1 : existingTickerLines[0].sequenceId,
                   TickerId: data.id,
                   IsShow: true,
                   RepeatCount: 1,
                   Severity: 1,
                   Frequency: 1,
                   CreatedBy: appdata.currentUser().id,
                   OperatorNumber: data.currentTickerOperator()

               }
               var reqObj = {
                   TickerLines: [tempTickerLineObj],
                   TickerTypeId: type
               }

               return $.Deferred(function (d) {
                   $.when(dataservice.insertOnAirTickerLine(reqObj))
                       .done(function (responseData) {
                           if (responseData.IsSuccess && responseData.Data) {
                               var line = responseData.Data;

                               if (existingTickerLines && existingTickerLines.length > 0) {
                                   removeExistingTickerLineOnSeqId(existingTickerLines);
                               }
                               if (line) {
                                   if (appdata.currentWorkingTicker() === line.TickerId) {
                                       var item = dc.tickers.getLocalById(data.id);
                                       if (item)
                                           item.dropDownVisible(true);
                                   }
                                   dc.tickerLines.add(line, { addToObservable: true, sort: true, groupByTickerId: true });
                                   setTickerLineLastUpdateDate();
                               }

                               config.logger.success("Ticker line created successfully!");
                               d.resolve(responseData);
                           } else {
                               d.reject();
                           }
                       })
                       .fail(function () {
                           d.reject();
                       })
               }).promise();
           },
           markOnAirTickerStatus = function (data, type) {
            var tempTickerLineObj = {
                TickerLineId: data.id
            }
            var reqObj = {
                TickerLines: [tempTickerLineObj],
                TickerTypeId: type
            }

            return $.Deferred(function (d) {
                $.when(dataservice.markOnAirTickerStatus(reqObj))
                    .done(function (responseData) {
                        if (responseData.IsSuccess) {
                            dc.tickerLines.getLocalById(data.id).isShow(false);
                            config.logger.success("Ticker line deleted successfully!");
                            d.resolve(responseData);
                        } else {
                            d.reject();
                        }
                    })
                    .fail(function () {
                        d.reject();
                    })
            }).promise();
           },
           
        setTickerTemplate = function (reqObj) {
            return $.Deferred(function (d) {
                $.when(dataservice.setTickerTemplate(reqObj))
                    .done(function (responseData) {
                        if (responseData.IsSuccess) {
                            config.logger.success("Template Changed Successfully!");
                            d.resolve(responseData);
                        } else {
                            d.reject();
                        }
                    })
                    .fail(function () {
                        d.reject();
                    })
            }).promise();
        },
            sendToTwitter = function (reqObj) {
                return $.Deferred(function (d) {
                    $.when(dataservice.sendToTwitter(reqObj))
                        .done(function (responseData) {
                            if (responseData.IsSuccess) {
                                config.logger.success("Ticker Twitted Successfully!");
                                d.resolve(responseData);
                            } else {
                                d.reject();
                            }
                        })
                        .fail(function () {
                            d.reject();
                        })
                }).promise();
            },
        deleteCompleteGroup = function (data) {
            var reqObj = {
                TickerId: data.id,
                TickerTypeId: data.tickerTypeId
            }
            return $.Deferred(function (d) {
                $.when(dataservice.deleteCompleteGroup(reqObj))
                    .done(function (responseData) {
                        if (responseData.IsSuccess) {
                            if (data.tickerLines().length > 0) {
                                for (var i = 0; i < data.tickerLines().length; i++) {
                                    dc.tickerLines.removeById(data.tickerLines()[i].id, true);
                                }
                            }
                            dc.tickers.removeById(data.id, true);

                            config.logger.success("Segment deleted successfully!");
                            d.resolve(responseData);
                        } else {
                            d.reject();
                        }
                    })
                    .fail(function () {
                        d.reject();
                    })
            }).promise();
        },
        mapMCRData = function (response, vmList) {
            if (response.Data && response.Data.length > 0) {
                vmList([]);
                var result = response.Data;
                var temp = [];
                for (var i = 0, len = result.length ; i < len ; i++) {
                    var userId;
                    var existingLine;
                    if (result[i].TickerLineId != 0)
                        existingLine = dc.tickerLines.getLocalById(result[i].TickerLineId);
                    else
                        existingLine = dc.tickers.getLocalById(result[i].TickerId);

                    var line;
                    line = mapper.tickerLine.fromDto(result[i]);
                   
                    if (existingLine)
                        line.userId(existingLine.userId());

                    if (result[i].TickerLineId != 0)
                        line.sequenceId = result[i].SequenceNumber;
                    else
                        line.sequenceId = 0;
                   
                    temp.push(line);
                }
                vmList(temp);
            }
        },
        getMCRBreakingTickers = function (vmList) {
            var reqObj = {}
            return $.Deferred(function (d) {
                $.when(dataservice.getMCRBreakingTickers(reqObj))
                    .done(function (responseData) {
                        if (responseData.IsSuccess && responseData.Data) {
                            mapMCRData(responseData, vmList);
                            d.resolve(responseData);
                        } else {
                            d.reject();
                        }
                    })
                    .fail(function () {
                        d.reject();
                    })
            }).promise();
        },
        getMCRLatestTickers = function (vmList) {
            var reqObj = {}
            return $.Deferred(function (d) {
                $.when(dataservice.getMCRLatestTickers(reqObj))
                    .done(function (responseData) {
                        if (responseData.IsSuccess && responseData.Data) {
                            mapMCRData(responseData, vmList);
                            d.resolve(responseData);
                        } else {
                            d.reject();
                        }
                    })
                    .fail(function () {
                        d.reject();
                    })
            }).promise();
        },
        getMCRCategoryTickers = function (vmList) {
            var reqObj = {}
            return $.Deferred(function (d) {
                $.when(dataservice.getMCRCategoryTickers(reqObj))
                    .done(function (responseData) {
                        if (responseData.IsSuccess && responseData.Data) {
                            mapMCRData(responseData, vmList);
                            d.resolve(responseData);
                        } else {
                            d.reject();
                        }
                    })
                    .fail(function () {
                        d.reject();
                    })
            }).promise();
        },
        updateTickerSequence = function (arr,type) {
            var tempList = [];

            for (var i = 0; i < arr.length; i++) {
                var obj = {};
                obj.TickerId = arr[i].id;
                obj.SequenceId = arr[i].sequenceId;
                obj.TickerTypeId = arr[i].tickerTypeId;
                tempList.push(obj);
            }
            var reqObj = {
                Tickers: tempList
            }

            return $.Deferred(function (d) {
                $.when(dataservice.updateTickerSequence(reqObj))
                    .done(function (responseData) {
                        if (responseData.IsSuccess && responseData.Data) {
                            config.logger.success("Kindly press update sequence button to send updated sequence to MCR!");
                             config.logger.success("Sequence updates successfully.");
                             
                            d.resolve(responseData);
                        } else {
                            d.reject();
                        }
                    })
                    .fail(function () {
                        d.reject();
                    })
            }).promise();

        },
         updateTickerLineSequence = function (arr, type) {
             var tempList = [];

             for (var i = 0; i < arr.length; i++) {
                 var obj = {};
                 obj.TickerLineId = arr[i].id;
                 obj.SequenceId = arr[i].sequenceId;
                 obj.TickerId = arr[i].tickerId;
                 obj.TickerTypeId = arr[i].tickerTypeId;
                 tempList.push(obj);
             }
             var reqObj = {
                 TickerLines: tempList
             }

             return $.Deferred(function (d) {
                 $.when(dataservice.updateTickerLineSequence(reqObj))
                     .done(function (responseData) {
                         if (responseData.IsSuccess && responseData.Data) {
                             config.logger.success("Kindly press update sequence button to send updated sequence to MCR!");
                             config.logger.success("Sequence updates successfully.");

                             d.resolve(responseData);
                         } else {
                             d.reject();
                         }
                     })
                     .fail(function () {
                         d.reject();
                     })
             }).promise();

         },
        
        updateToMCR = function (type) {
            var reqObj = { TickerTypeId: type };

            return $.Deferred(function (d) {
                $.when(dataservice.updateOnAirTickerToMCR(reqObj))
                    .done(function (responseData) {
                        if (responseData.IsSuccess && responseData.Data) {
                            config.logger.success("Successfully updated!");
                            d.resolve(responseData);
                        } else {
                            d.reject();
                        }
                    })
                    .fail(function () {
                        d.reject();
                    })
            }).promise();
        },
        createTickerBynewsFileId = function (data) {

            return $.Deferred(function (d) {
                $.when(dataservice.createTickerBynewsFileId(data))
                    .done(function (responseData) {
                        if (responseData.IsSuccess && responseData.Data) {
                            dc.tickers.add(responseData.Data, { sort: true });
                            d.resolve(responseData);
                        } else {
                            d.reject();
                        }
                    })
                    .fail(function () {
                        d.reject();
                    })
            }).promise();
        },
        
        saveTickerImages = function (responseData) {
            var tickerLst = [];
            dc.tickerImage.fillData(responseData);
            appdata.isTickerImageExist(!appdata.isTickerImageExist());

        },
        saveTickerImagesPollingData = function (responseData) {
            var tickerLst = [];
            var arr = [];
            for (var i = 0; i < responseData.length; i++) {
                arr.push(mapper.tickerimage.fromDto(responseData[i]))
            }
            appdata.tickerImagesList(arr);
            dc.tickerImage.fillData(responseData);
        };


        // #endregion

        return {
            saveTickerData: saveTickerData,
            saveTickerLine:saveTickerLine,
            saveCategoryDataOnServer: saveCategoryDataOnServer,
            saveCategoryData: saveCategoryData,
            createSegment: createSegment,
            insertOnAirTickerLine: insertOnAirTickerLine,
            markOnAirTickerStatus: markOnAirTickerStatus,
            deleteCompleteGroup: deleteCompleteGroup,
            getMCRBreakingTickers: getMCRBreakingTickers,
            getMCRLatestTickers: getMCRLatestTickers,
            getMCRCategoryTickers: getMCRCategoryTickers,
            saveTickerImages: saveTickerImages,
            saveTickerImagesPollingData: saveTickerImagesPollingData,
            updateTickerSequence: updateTickerSequence,
            updateToMCR: updateToMCR,
            updateTickerLineSequence: updateTickerLineSequence,
            sendToTwitter: sendToTwitter,
            setTickerTemplate: setTickerTemplate,
            createTickerBynewsFileId: createTickerBynewsFileId

        };
    });