﻿define('model.audiocommentcontrol',
    ['ko'],
    function (ko) {

        var
            _dc = this,

            AudioCommentControl = function () {

                var self = this;

                self.id,
                self.comment,
                self.userId,
                self.lastUpdateDateStr,
                self.commentTypeId,

                self.isNullo = false;

                return self;
            }

        AudioCommentControl.Nullo = new AudioCommentControl();

        AudioCommentControl.Nullo.isNullo = true;

        AudioCommentControl.datacontext = function (dc) {
            if (dc) { _dc = dc; }
            return _dc;
        };

        return AudioCommentControl;
    });