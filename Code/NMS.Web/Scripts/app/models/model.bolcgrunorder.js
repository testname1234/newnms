﻿define('model.bolcgrunorder',
    ['ko', 'underscore'],
    function (ko, _) {

        var
            _dc = this,

            RunOrder = function () {

                var self = this;

                self.CasparRundownId,
                self.Name = ko.observable(''),
                self.Stories = ko.observableArray([]);
               
                return self;
            }

        return RunOrder;
    });