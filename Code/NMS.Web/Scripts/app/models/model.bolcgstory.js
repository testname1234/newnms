﻿define('model.bolcgstory',
    ['ko', 'underscore'],
    function (ko, _) {

        var
            _dc = this,
            Story = function () {
                var self = this;
                self.StoryId,
                self.StorySlug = ko.observable();
                return self;
            }
        return Story;
    });