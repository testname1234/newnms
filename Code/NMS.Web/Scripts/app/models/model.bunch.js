﻿define('model.bunch',
    ['ko', 'moment', 'appdata', 'utils', 'config','model.news'],
    function (ko, moment, appdata, utils, config,News) {

        var
            _dc = this,

            Bunch = function () {

                var self = this;

                self.id,
                self.bunchNews = ko.observableArray([]),
                self.isGroupName = false,
                self.groupName = '',
                self.forceRefresh = ko.observable(false),
                self.searchKeywords = ko.observable(''),
                self.selectedFilters = ko.observableArray([]),
                self.showSearchBox = ko.observable(false),
                self.showfilters = ko.observable(false),
                self.bunchCount = ko.observable(0),
                self.resourceFilter = ko.observableArray([]),
                self.hasDefaultImage = false,
                self.bunchFilters = ko.observableArray([]),
                self.publishTime = ko.computed({
                    read: function () {
                        if (self.topNews()) {
                            return self.topNews().publishTime;
                        }
                    },
                    deferEvaluation: true
                }),

                self.publishDisplayTime = ko.computed({
                    read: function () {
                        return self.topNews().publishDisplayTime();
                    },
                    deferEvaluation: true
                }),

                self.topNews = ko.computed({
                    read: function () {
                        self.forceRefresh();
                        
                        var bunchNewsList = _dc.news.getByBunchId(self.id);
                        if (bunchNewsList && bunchNewsList().length > 0) {
                            var arr = bunchNewsList.sort(function (entity1, entity2) {
                                return entity1.publishTime < entity2.publishTime;
                            });

                            var newsObject = new News();

                            for (var i = 0; i < arr.length; i++) {
                                var flag = true;
                                for (var j = 0; j < appdata.selectedSourceFilters().length; j++) {
                                    var filter = appdata.selectedSourceFilters()[j];
                                    if (filter !== 78 && arr[i].filterIds().indexOf(filter) === -1) {
                                        flag = false;
                                    } else {
                                        flag = true;
                                        break;
                                    }
                                }
                                if (flag) {
                                    newsObject = arr[i];
                                    return newsObject;
                                }
                            }
                            return newsObject;
                        }
                    },
                    deferEvaluation: true
                }),

                self.topRelatedNews = ko.computed({
                    read: function () {

                        if (self.bunchNews && ko.isObservable(self.bunchNews) && self.bunchNews().length > 0 && self.topNews()) {
                            var arr = [];
                            arr = self.bunchNews().sort(function (entity1, entity2) {
                                return entity1.publishTime < entity2.publishTime;
                            });
                            var tempArray = [];
                            for (var i = 0; i < arr.length; i++) {
                                if (arr[i].id !== self.topNews().id) {
                                    tempArray.push(arr[i]);
                                }
                            }
                            var searchArray = [];
                            if (self.selectedFilters().length > 0) {
                                searchArray = tempArray;
                                tempArray = [];
                                for (var i = 0; i < searchArray.length; i++) {
                                    var news = searchArray[i];
                                    currentNewsFilterIds = news.filterIds();
                                    for (var j = 0; j < currentNewsFilterIds.length; j++) {
                                        var filter = _dc.filters.getLocalById(currentNewsFilterIds[j]);
                                        if (filter.parentId != -1) {
                                            filter = _dc.filters.getLocalById(filter.parentId);
                                        }
                                        for (var k = 0; k < self.selectedFilters().length; k++) {
                                            var obj = utils.arrayIsContainObject(tempArray, news, "id");
                                            if (self.selectedFilters()[k] == filter.id && !obj.flag)
                                                tempArray.push(news);
                                        }
                                    }
                                }
                            }
                            if (self.searchKeywords().length > 0) {
                                var srch;
                                searchArray = tempArray;
                                tempArray = [];
                                srch = utils.regExEscape(self.searchKeywords().toLowerCase().trim());
                                for (var i = 0; i < searchArray.length; i++) {
                                    if (searchArray[i].title().toLowerCase().search(srch) !== -1)
                                        tempArray.push(searchArray[i]);
                                }
                            }
                            return tempArray.slice(0, 3);
                        }
                        return [];
                    },
                    deferEvaluation: true
                }),

                self.relatedNews = ko.computed({
                    read: function () {
                        var arr = [];
                        if (self.bunchNews && ko.isObservable(self.bunchNews) && self.bunchNews().length > 0 && self.topNews()) {
                            var arr = [];
                            arr = self.bunchNews().sort(function (entity1, entity2) {
                                return entity1.publishTime < entity2.publishTime;
                            });
                            var tempArray = [];
                            for (var i = 0; i < arr.length; i++) {
                                if (arr[i].id !== self.topNews().id) {
                                    tempArray.push(arr[i]);
                                }
                            }

                            if (tempArray && tempArray.length > self.bunchCount())
                                self.bunchCount(tempArray.length + 1);

                            return tempArray;
                        } else {
                            return [];
                        }
                    },
                    deferEvaluation: true
                }),
                self.reportedContent = ko.computed({
                    read: function () {
                        var arr = [];
                        var filter = self.resourceFilter();
                        var searchText = '';
                        if (self.topNews()) {
                            for (var i = 0; i < self.topNews().resources().length; i++) {
                                var resource = self.topNews().resources()[i];
                                arr.push(resource);
                            }
                        }

                        if (arr.length > 0 && (searchText.length > 0 || filter.length > 0)) {
                            var tempArray = arr;
                            arr = [];
                            for (var i = 0; i < filter.length; i++) {
                                for (var j = 0; j < tempArray.length; j++) {
                                    var resource = tempArray[j];
                                    if (resource.type === filter[i]) {
                                        arr.push(resource);
                                    }
                                }
                            }
                        }
                        return arr;
                    },
                    deferEvaluation: true
                }),

                self.bunchCountDisplay = ko.computed({
                    read: function () {
                        return self.bunchCount() && self.bunchCount() > 1 ? self.bunchCount() - 1 : 0;
                    },
                    deferEvaluation: true
                }),

                self.relatedNewsResources = ko.computed({
                    read: function () {
                        var arr = [];

                        var filter = self.resourceFilter();
                        var searchText = '';


                        if (self.relatedNews() && self.relatedNews().length > 0) {
                            for (var i = 0; i < self.relatedNews().length; i++) {
                                for (var j = 0; j < self.relatedNews()[i].resources().length; j++) {
                                    arr.push(self.relatedNews()[i].resources()[j]);
                                }
                            }
                        }
                        if (arr.length > 0 && (searchText.length > 0 || filter.length > 0)) {
                            var tempArray = arr;
                            arr = [];
                            for (var i = 0; i < filter.length; i++) {
                                for (var j = 0; j < tempArray.length; j++) {
                                    var resource = tempArray[j];
                                    if (resource.type === filter[i]) {
                                        arr.push(resource);
                                    }
                                }
                            }
                        }

                        return arr;
                    },
                    deferEvaluation: true
                }),
                self.reportedContentCount = ko.computed({
                    read: function () {
                        return self.reportedContent().length;
                    },
                    deferEvaluation: true
                }),
                self.relatedContentCount = ko.computed({
                    read: function () {
                        return self.relatedNewsResources().length;
                    },
                    deferEvaluation: true
                }),
                self.totalContentCount = ko.computed({
                    read: function () {
                        return self.reportedContentCount() + self.relatedContentCount();
                    },
                    deferEvaluation: true
                }),
                self.fillObservableProperties = function () {
                    self.bunchNews = _dc.news.getByBunchId(self.id);
                };

                self.isNullo = false;

                return self;
            }

        Bunch.Nullo = new Bunch();

        Bunch.Nullo.isNullo = true;

        Bunch.datacontext = function (dc) {
            if (dc) { _dc = dc; }
            return _dc;
        };

        return Bunch;
    });