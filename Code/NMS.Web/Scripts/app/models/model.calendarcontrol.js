﻿define('model.calendarcontrol',
    ['underscore', 'ko', 'moment'],
    function (_, ko, moment) {

        var
            _dc = this,

            CalendarControl = function () {

                var self = this;

                self.id,
                self.toBuffer = ko.observable(new Date()),
                self.fromBuffer = ko.observable(new Date()),
                self.currentOption = ko.observable(),
                self.isCalendarChange = ko.observable(false),
                self.isNewsCropVisible = ko.observable(false),
                self.isCalenderVisible = ko.observable(true),

                self.fromDate = ko.computed({
                    read: function () {
                        self.isCalendarChange();
                      
                        if (self.currentOption() == 'today') {
                            var today = moment();
                            self.fromBuffer(today);
                            return today.startOf('day').toISOString();
                        } else if (self.currentOption() == 'yesterday') {
                            var yesterday = moment().subtract('days', 1);
                            self.fromBuffer(yesterday);
                            return yesterday.startOf('day').toISOString();
                        } else if (self.currentOption() == 'lastweek') {
                            var lastWeek = moment().subtract('days', 7);
                            self.fromBuffer(lastWeek);
                            return lastWeek.startOf('day').toISOString();
                        } else if (self.currentOption() == 'lastmonth') {
                            var lastMonth = moment().subtract('days', 30);
                            self.fromBuffer(lastMonth);
                            return lastMonth.startOf('day').toISOString();
                        }
                        else {
                            return moment(self.fromBuffer()).startOf('day').toISOString();
                        }
                      
                       
                    },
                    deferEvaluation: true
                }),
                self.toDate = ko.computed({
                    read: function () {
                        self.isCalendarChange();
                        if (self.currentOption() == 'today') {
                            var today = moment().endOf('day');
                            self.toBuffer(today);
                            return today.toISOString();
                        } else if (self.currentOption() == 'yesterday') {
                            var yesterday = moment().subtract('days', 1);
                            self.toBuffer(yesterday);
                            return yesterday.toISOString();
                        } else if (self.currentOption() == 'lastweek') {
                            var lastWeek = moment().endOf('day');
                            self.toBuffer(lastWeek);
                            return lastWeek.toISOString();
                        } else if (self.currentOption() == 'lastmonth') {
                            var lastMonth = moment().endOf('day');
                            self.toBuffer(lastMonth);
                            return lastMonth.toISOString();
                        }
                        else {
                            return moment(self.toBuffer()).endOf('day').toISOString();
                        }
                   
                      
                    },
                    deferEvaluation: true
                }),
                self.fromDateDisplay = ko.computed({
                    read: function () {
                        return moment(self.fromBuffer()).format('MMMM DD, YYYY');
                    },
                    deferEvaluation: true
                }),

                self.toDateDisplay = ko.computed({
                    read: function () {
                        return moment(self.toBuffer()).format('MMMM DD, YYYY');
                    },
                    deferEvaluation: true
                }),
                self.range = ko.computed({
                    read: function () {
                        self.toBuffer();
                        if (moment(self.toDate()).format('MMMM DD, YYYY') > moment(self.fromDate()).format('MMMM DD, YYYY')) {
                            return moment(self.fromDate()).format('MMMM DD, YYYY') + " - " + moment(self.toDate()).format('MMMM DD, YYYY');
                        }
                        else {
                            return moment(self.toBuffer()).format('MMMM DD, YYYY');
                        }
                    },
                    deferEvaluation: false
                }),

                self.currentDate = ko.observable(moment().toISOString()),
                self.currentDateDisplay = ko.computed({
                    read: function () {
                        return moment(self.currentDate()).format('MMMM DD, YYYY');
                    },
                    deferEvaluation: true
                }),

                self.isNullo = false;

                return self;
            }

        CalendarControl.Nullo = new CalendarControl();

        CalendarControl.Nullo.isNullo = true;

        CalendarControl.datacontext = function (dc) {
            if (dc) { _dc = dc; }
            return _dc;
        };

        return CalendarControl;
    });