﻿define('model.casparaction',
    ['ko', 'underscore'],
    function (ko, _) {

        var
            _dc = this,

            CasparAction = function () {

                var self = this;

                self.id,
                self.name,
                self.shortKey,
                self.command ,
               

                self.isNullo = false;

                self.fillObservableProperties = function () {

                };

                return self;
            }

        CasparAction.Nullo = new CasparAction();

        CasparAction.Nullo.isNullo = true;

        // static member
        CasparAction.datacontext = function (dc) {
            if (dc) { _dc = dc; }
            return _dc;
        };

        return CasparAction;
    });