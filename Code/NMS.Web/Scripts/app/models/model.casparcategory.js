﻿define('model.casparcategory',
    ['ko', 'underscore'],
    function (ko, _) {

        var
            _dc = this,

            CasparCategory = function () {

                var self = this;

                self.id,
                self.name ,
                
                self.isNullo = false;

                self.fillObservableProperties = function () {

                };

                return self;
            }

        CasparCategory.Nullo = new CasparCategory();

        CasparCategory.Nullo.isNullo = true;

        // static member
        CasparCategory.datacontext = function (dc) {
            if (dc) { _dc = dc; }
            return _dc;
        };

        return CasparCategory;
    });