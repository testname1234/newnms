﻿define('model.casparconfigsetting',
    ['ko', 'underscore'],
    function (ko, _) {

        var
            _dc = this,

            CasparConfigSetting = function () {

                var self = this;

                self.id,
                self.configKey ,
                self.configValue ,
                self.creationDate,
                self.lastUpdateDate ,
               
                self.isNullo = false;

                self.fillObservableProperties = function () {

                };

                return self;
            }

        CasparConfigSetting.Nullo = new CasparConfigSetting();

        CasparConfigSetting.Nullo.isNullo = true;

        // static member
        CasparConfigSetting.datacontext = function (dc) {
            if (dc) { _dc = dc; }
            return _dc;
        };

        return CasparConfigSetting;
    });