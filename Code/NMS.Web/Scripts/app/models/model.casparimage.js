﻿define('model.casparimage',
    ['ko', 'underscore'],
    function (ko, _) {

        var
            _dc = this,

            CasparImage = function () {

                var self = this;

                self.id,
                self.casparRundownItemId ,
                self.transition,
                self.duration,
                self.direction , 
                self.userAuto,
                self.triggerOnNext,
                
               

                self.isNullo = false;

                self.fillObservableProperties = function () {

                };

                return self;
            }

        CasparImage.Nullo = new CasparImage();

        CasparImage.Nullo.isNullo = true;

        // static member
        CasparImage.datacontext = function (dc) {
            if (dc) { _dc = dc; }
            return _dc;
        };

        return CasparImage;
    });