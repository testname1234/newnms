﻿define('model.casparitem',
    ['ko', 'underscore'],
    function (ko, _) {

        var
            _dc = this,

            CasparItem= function () {

                var self = this;

                self.id,
                self.name ,
                self.canTarget,
                self.casparItemCategoryId,
                self.iconPath,
              
                
               

                self.isNullo = false;

                self.fillObservableProperties = function () {

                };

                return self;
            }

        CasparItem.Nullo = new CasparItem();

        CasparItem.Nullo.isNullo = true;

        // static member
        CasparItem.datacontext = function (dc) {
            if (dc) { _dc = dc; }
            return _dc;
        };

        return CasparItem;
    });