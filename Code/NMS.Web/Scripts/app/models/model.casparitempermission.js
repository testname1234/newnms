﻿define('model.casparitempermission',
    ['ko', 'underscore'],
    function (ko, _) {

        var
            _dc = this,

            CasparItemPermission = function () {

                var self = this;

                self.id,
                self.userId ,
                self.casparItemId ,
                self.roleId ,
                
                
                self.isNullo = false;

                self.fillObservableProperties = function () {

                };

                return self;
            }

        CasparItemPermission.Nullo = new CasparItemPermission();

        CasparItemPermission.Nullo.isNullo = true;

        // static member
        CasparItemPermission.datacontext = function (dc) {
            if (dc) { _dc = dc; }
            return _dc;
        };

        return CasparItemPermission;
    });