﻿define('model.casparrundown',
    ['ko', 'underscore'],
    function (ko, _) {

        var
            _dc = this,

            CasparRundown = function () {

                var self = this;

                self.id,
                self.name,
              
                self.isNullo = false;

                self.fillObservableProperties = function () {

                };

                return self;
            }

        CasparRundown.Nullo = new CasparRundown();

        CasparRundown.Nullo.isNullo = true;

        // static member
        CasparRundown.datacontext = function (dc) {
            if (dc) { _dc = dc; }
            return _dc;
        };

        return CasparRundown;
    });