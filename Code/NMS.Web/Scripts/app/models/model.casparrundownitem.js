﻿define('model.CasparRundownItemitem',
    ['ko', 'underscore'],
    function (ko, _) {

        var
            _dc = this,

            CasparRundownItem = function () {

                var self = this;

                self.id,
                self.casparItemId ,
                self.casparRundownId ,
                self.flashTemplateWindowId ,
                self.name ,
                self.thumbnail,
                self.server ,
                self.target ,
                self.channel ,
                self.videoLayer ,
                self.delay  ,
                self.duration ,
                self.allowGpi ,
                self.uid ,

              
                self.isNullo = false;

                self.fillObservableProperties = function () {

                };

                return self;
            }

        CasparRundownItem.Nullo = new CasparRundownItem();

        CasparRundownItem.Nullo.isNullo = true;

        // static member
        CasparRundownItem.datacontext = function (dc) {
            if (dc) { _dc = dc; }
            return _dc;
        };

        return CasparRundownItem;
    });