﻿define('model.casparvideo',
    ['ko', 'underscore'],
    function (ko, _) {

        var
            _dc = this,

            CasparVideo= function () {

                var self = this;

                self.id,
                self.casparRundownItemId ,
                self.transition,
                self.duration,
                self.tween,
                self.direction ,
                self.seek,
                self.length,
                self.loop , 
                self.freezeOnLoad,
                self.triggerOnNext,
             

                self.isNullo = false;

                self.fillObservableProperties = function () {

                };

                return self;
            }

        CasparVideo.Nullo = new CasparVideo();

        CasparVideo.Nullo.isNullo = true;

        // static member
        CasparVideo.datacontext = function (dc) {
            if (dc) { _dc = dc; }
            return _dc;
        };

        return CasparVideo;
    });