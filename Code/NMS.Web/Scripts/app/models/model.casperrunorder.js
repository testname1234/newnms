﻿define('model.casperrunorder',
    [
        'ko',
        'underscore'
    ],
    function (ko,_) {

    	var
            _dc = this,

            CasperRunOrder = function () {

            	var self = this;

            	self.id,
                self.roSlug,
                self.roChannel,
                self.casperItems = ko.observableArray([]),

                self.extractData = function () {
                   self.casperItems = _dc.casperitem.getCasperItemsByRunOrderId(self.id);
                },


                self.isNullo = false;

            	return self;
            }

    	CasperRunOrder.Nullo = new CasperRunOrder();

    	CasperRunOrder.Nullo.isNullo = true;

    	CasperRunOrder.datacontext = function (dc) {
    		if (dc) { _dc = dc; }
    		return _dc;
    	};

    	return CasperRunOrder;
    });