﻿define('model.category',
    [
        'ko',
        'underscore'
    ],
    function (ko, _) {

        var
            _dc = this,

            Category = function () {

                var self = this;
                self.id,
                self.name,
                self.sequenceNumber,
                self.creationDate = ko.observable(),
                self.lastUpdateDate = ko.observable(),
                self.creationDateDisplay = ko.computed({
                    read: function () {
                        return moment(self.creationDate()).fromNow();
                    },
                    deferEvaluation: true
                }),
                self.lastUpdateDateDisplay = ko.computed({
                    read: function () {
                        return moment(self.lastUpdateDate()).fromNow();
                    },
                    deferEvaluation: true
                });
                
                return self;
            }

        Category.Nullo = new Category();

        Category.Nullo.isNullo = true;

        Category.datacontext = function (dc) {
            if (dc) { _dc = dc; }
            return _dc;
        };

        return Category;
    });