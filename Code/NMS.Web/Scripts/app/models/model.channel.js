﻿define('model.channel',
    [],
    function () {

        var
            _dc = this,

            Channel = function () {

                var self = this;

                self.id,
                self.name,
                self.isOtherChannel,
                self.programs = ko.observableArray([]),

                self.pendingPrograms = ko.computed({
                    read: function () {
                        var list = self.programs();
                        var pending = 0;
                        for (var i = 0; i < list.length; i++) {
                            if (list[i].channelVideos().length>0 && list[i].processedVideosPercentage()!='100%') {
                                pending++;
                            }
                        }
                        return pending;
                    },
                    deferEvaluation: true
                }),

                self.channelVideos = ko.observableArray([]),

                self.processedVideosPercentage = ko.computed({
                    read: function () {
                        var list = self.channelVideos();
                        var processed = 0;
                        for (var i = 0; i < list.length; i++) {
                            if (list[i].isProcessed()) {
                                processed++;
                            }
                        }
                        return Math.round(((processed / list.length) * 100)) + '%';
                    },
                    deferEvaluation: true
                }),

                self.fillObservableProperties = function () {
                    self.channelVideos = _dc.channelVideos.getByChannelId(self.id);
                    self.programs = _dc.programs.getByChannelId(self.id);
                },

                self.isNullo = false;

                return self;
            }

        Channel.Nullo = new Channel();

        Channel.Nullo.isNullo = true;

        // static member
        Channel.datacontext = function (dc) {
            if (dc) { _dc = dc; }
            return _dc;
        };

        return Channel;
    });