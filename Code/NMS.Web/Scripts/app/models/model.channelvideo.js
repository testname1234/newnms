﻿define('model.channelvideo',
    [],
    function () {

        var
            _dc = this,

            ChannelVideo = function () {

                var self = this;

                self.id,
                self.channelId=ko.observable(),
                self.programId = ko.observable(),
                self.durationInSeconds = ko.observable(0),

                self.channel = ko.computed({
                    read: function () {
                        return _dc.channels.getLocalById(self.channelId());
                    },
                    deferEvaluation: true
                }),

                self.program = ko.computed({
                    read: function () {
                        return _dc.programs.getLocalById(self.programId());
                    },
                    deferEvaluation: true
                }),

                self.logo = ko.computed({
                    read: function () {
                        if (self.channel()) {
                            var name = self.channel().name;
                            return "http://10.3.18.25/logos/samples/" + name + ".jpg";
                        }
                        else {
                            return "";
                        }
                    },
                    deferEvaluation: true
                }),
                
                self.from ,


                self.to,

                self.fileName = ko.computed({
                    read: function () {
                        var result = self.program().name+ '.mp4';
                        return result;
                    },
                    deferEvaluation: true
                }),

                self.url,

                self.thumbImageUrl = ko.computed({
                    read: function () {
                        return '/content/images/producer/noimage.png';
                    },
                    deferEvaluation: true
                }),

                self.isProcessed = ko.observable(false),

                self.processedVideosPercentage = ko.computed({
                    read: function () {
                        if (self.isProcessed()) {
                            return '100%';
                        }                        
                        return '0%';
                    },
                    deferEvaluation: true
                }),

                self.episodeDate,
                self.episodedatedisplay,

                self.isNullo = false;

                return self;
            }

        ChannelVideo.Nullo = new ChannelVideo();

        ChannelVideo.Nullo.isNullo = true;

        ChannelVideo.datacontext = function (dc) {
            if (dc) { _dc = dc; }
            return _dc;
        };

        return ChannelVideo;
    });