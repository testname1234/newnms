﻿define('model.comment',
    ['ko', 'moment','enum'],
    function (ko, moment,e) {

        var
            _dc = this,

            Comment = function () {

                var self = this;

                self.id,

                self.fromId,
                self.toId,
                self.comment,
                self.isReceived,
                self.storyScreenTemplateId,
                self.creationDate,
                self.lastUpdateDate,
                self.toStoryWriter = ko.observable(false),
                self.toProducer = ko.observable(false),
                self.toNle = ko.observable(false),
                self.isRead = ko.observable(false),
                self.sendFromNle = ko.observable(false),
                self.sendFromStoryWriter=ko.observable(false),

                self.displayTime = ko.computed({
                    read: function () {
                        return moment(self.creationDate).fromNow();
                    },
                    deferEvaluation: true
                }),
                
                self.displayName = ko.computed({
                    read: function () {
                        var all = _dc.users.getObservableList();

                        var user = _dc.users.getLocalById(self.fromId);
                        return user ? user.name : "";
                    },
                    deferEvaluation: true
                }),

                self.isNullo = false;

                return self;
            }

        Comment.Nullo = new Comment();

        Comment.Nullo.isNullo = true;

        // static member
        Comment.datacontext = function (dc) {
            if (dc) { _dc = dc; }
            return _dc;
        };

        return Comment;
    });