﻿define('model.configsetting',
    ['ko', 'underscore'],
    function (ko, _) {

        var
            _dc = this,

            ConfigSetting = function () {

                var self = this;

                self.id,
                self.key = ko.observable(),
                self.value = ko.observable(),

                self.isNullo = false;

                self.fillObservableProperties = function () {

                };

                return self;
            }

        ConfigSetting.Nullo = new ConfigSetting();

        ConfigSetting.Nullo.isNullo = true;

        // static member
        ConfigSetting.datacontext = function (dc) {
            if (dc) { _dc = dc; }
            return _dc;
        };

        return ConfigSetting;
    });