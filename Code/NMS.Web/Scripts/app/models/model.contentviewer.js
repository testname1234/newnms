﻿define('model.contentviewercontrol',
    ['ko', 'model.news'],
    function (ko, News) {

        var
            _dc = this,

            ContentViewerControl = function () {

                var self = this;

                self.id,
                self.currentNews = ko.observable(new News()),
                self.isPopUpVisible = ko.observable(false),

                self.isNullo = false;

                return self;
            }

        ContentViewerControl.Nullo = new ContentViewerControl();

        ContentViewerControl.Nullo.isNullo = true;

        ContentViewerControl.datacontext = function (dc) {
            if (dc) { _dc = dc; }
            return _dc;
        };

        return ContentViewerControl;
    });