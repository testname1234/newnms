﻿define('model.crop',
    ['ko', 'underscore'],
    function (ko, _) {

        var
            _dc = this,

            Crop = function () {

                var self = this;

                self.id,
                self.casparRundownItemId,
                self.left ,
                self.width,
                self.top,
                self.height,
                self.duration,
                self.tween,
                self.defer,
                
               

                self.isNullo = false;

                self.fillObservableProperties = function () {

                };

                return self;
            }

        Crop.Nullo = new Crop();

        Crop.Nullo.isNullo = true;

        // static member
        Crop.datacontext = function (dc) {
            if (dc) { _dc = dc; }
            return _dc;
        };

        return Crop;
    });