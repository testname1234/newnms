﻿define('model.dailynewspaper',
    [],
    function () {
        var
            _dc = this,

            DailyNewsPaper = function () {

                var self = this;

                self.id,
                self.newsPaperId,
                self.name,
                self.newsPaperDateTime,
                self.lastUpdateDate,
                self.creationDate,
                self.newsPaperPages = ko.observableArray([]),
                self.newsPaper = ko.observable(),

                self.pendingPages = ko.observable(0),

                self.processedPages = ko.observable(0),

                self.isProcessed = ko.computed({
                    read: function () {
                        if (self.processedPercentage() == '100%') {
                            return true;
                        }
                        return false;
                    },
                    deferEvaluation: true
                }),

                self.processedPercentage = ko.computed({
                    read: function () {
                        var list = self.newsPaperPages();
                        var processed = 0;
                        var pending = 0;
                        for (var i = 0; i < list.length; i++) {
                            if (list[i].isProcessed()) {
                                processed++;
                            }
                            else {
                                pending++;
                            }
                        }

                        self.pendingPages(pending);
                        self.processedPages(processed);

                        return Math.round(((processed / list.length) * 100))+'%';
                    },
                    deferEvaluation: true
                }),


                self.fillObservableProperties = function () {
                    self.newsPaperPages = _dc.newsPaperPages.getByDailyNewsPaperId(self.id);
                    self.newsPaper(_dc.newsPapers.getLocalById(self.newsPaperId));
                },

                self.isNullo = false;

                return self;
            }

        DailyNewsPaper.Nullo = new DailyNewsPaper();

        DailyNewsPaper.Nullo.isNullo = true;

        // static member
        DailyNewsPaper.datacontext = function (dc) {
            if (dc) { _dc = dc; }
            return _dc;
        };

        return DailyNewsPaper;
    });