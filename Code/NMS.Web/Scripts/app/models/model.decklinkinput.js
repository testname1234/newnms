﻿define('model.decklinkinput',
    ['ko', 'underscore'],
    function (ko, _) {

        var
            _dc = this,

            DecklinkInput = function () {

                var self = this;

                self.id,
                self.casparRundownItemId ,
                self.device,
                self.format,
                self.transition ,
                self.duration,
                self.tween,
                self.direction,
                
                
               

                self.isNullo = false;

                self.fillObservableProperties = function () {

                };

                return self;
            }

        DecklinkInput.Nullo = new DecklinkInput();

        DecklinkInput.Nullo.isNullo = true;

        // static member
        DecklinkInput.datacontext = function (dc) {
            if (dc) { _dc = dc; }
            return _dc;
        };

        return DecklinkInput;
    });