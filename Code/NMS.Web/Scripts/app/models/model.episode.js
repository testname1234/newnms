﻿define('model.episode',
    ['ko', 'moment', 'utils', 'config'],
    function (ko, moment, utils, config) {

        var
            _dc = this,

            Episode = function () {

                var self = this;

                self.id,
                self.programId,
                self.suggestedFlow = [],
                self.otherChannelsFlow = [],
                self.startTime = ko.observable(),
                self.endTime = ko.observable(),
                self.previewGuid = ko.observable(),

                self.programElements = ko.computed({
                    read: function () {
                        var arr = _dc.programElements.getByEpisodeId(self.id)();
                        arr = arr.sort(function (entity1, entity2) {
                            return entity1.sequenceId > entity2.sequenceId;
                        });
                        return arr;
                    },
                    deferEvaluation: true
                }),

                self.startTimeDisplay = ko.computed({
                    read: function () {
                        return moment(self.startTime()).format('h:mm a');
                    },
                    deferEvaluation: true
                }),

                self.totalDuration = ko.computed({
                    read: function () {
                        return moment(self.endTime()).diff(self.startTime(), 'second');
                    },
                    deferEvaluation: true
                }),
                self.totalDurationDisplay = ko.computed({
                    read: function () {
                        return utils.getDuration(self.totalDuration(), 'second');
                    },
                    deferEvaluation: true
                }),
                self.contentDuration = ko.computed({
                    read: function () {
                        var tempDuration = 0;
                        var arr = _dc.stories.getByEpisodeId(self.id)();
                        for (var i = 0; i < arr.length; i++) {
                            var temp = 0;
                            if (arr[i].contentDuration) {
                                temp = arr[i].contentDuration();
                            }
                            tempDuration = tempDuration + temp;
                        }
                        return tempDuration;
                    },
                    deferEvaluation: true
                }),
                self.contentDurationDisplay = ko.computed({
                    read: function () {
                        return utils.getDuration(self.contentDuration(), 'second');
                    },
                    deferEvaluation: true
                }),
                self.episodeProgress = ko.computed({
                    read: function () {
                        var obj = {};
                        obj.width = self.contentDuration() / self.totalDuration() * 100;
                        if (obj.width > 100) {
                            obj.width = 100;
                            obj.color = 'red';
                        }
                        else if (obj.width <= 100) {
                            obj.color = '#145dd0';
                        }
                        return obj;
                    },
                    deferEvaluation: true
                }),
                self.previewUrl = ko.computed({
                    read: function () {
                        return config.MediaServerUrl + "getresource/" + self.previewGuid();
                    },
                    deferEvaluation: true
                }),

                self.isNullo = false;

                return self;
            }

        Episode.Nullo = new Episode();

        Episode.Nullo.isNullo = true;

        Episode.datacontext = function (dc) {
            if (dc) { _dc = dc; }
            return _dc;
        };

        return Episode;
    });