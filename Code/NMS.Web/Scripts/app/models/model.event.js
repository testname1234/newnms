﻿define('model.event',
    ['ko', 'moment', 'utils'],
    function (ko, moment, utils) {

        var
            _dc = this,

            Event = function () {

                var self = this;

                self.id,
                self.name,
                self.searchTags,
                self.startTime = ko.observable(),
                self.endTime = ko.observable(),
                self.isSelected = ko.observable(false),

                self.remainingTimeDisplay = ko.computed({
                    read: function () {
                        var remains = moment(self.startTime()).fromNow();
                        remains = remains.replace('in ', '');
                        if(remains.indexOf('days')>-1)
                            remains = remains.replace('days', 'Days left');
                        else if (remains.indexOf('months') > -1)
                            remains = remains.replace('months', 'Months left');

                        return remains;

                    },
                    deferEvaluation: true
                }),

                self.isNullo = false;

                return self;
            }

        Event.Nullo = new Event();

        Event.Nullo.isNullo = true;

        Event.datacontext = function (dc) {
            if (dc) { _dc = dc; }
            return _dc;
        };

        return Event;
    });