﻿define('model.filter', 
    ['ko'],
    function (ko) {

        var
            _dc = this,

            Filter = function () {

                var self = this;

                self.id,
                self.name,
                self.filterTypeId,
                self.isEventTypeFilter = ko.observable(false),
                self.value,
                self.cssClass = ko.observable(),
                self.parentId,
                self.newsCount = ko.observable(1),
                self.childrenIds = ko.observableArray([]),
                self.newsIds = ko.observableArray([]),
                self.bunchIds = [],
                self.isSelected = ko.observable(false),
                self.bunchFilterChildren = ko.observableArray([]),
                self.creationDateStr,
                self.selectedChildCount = ko.observable(0),
                self.lastUpdateDateStr,

                self.children = ko.computed({
                    read: function () {
                        var arr = [];

                        if (self.bunchFilterChildren() && self.bunchFilterChildren().length > 0) {
                            return self.bunchFilterChildren();
                        } else {
                            for (var i = 0; i < self.childrenIds().length; i++) {
                                var tempObj = _dc.filters.getLocalById(self.childrenIds()[i]);
                                arr.push(tempObj);
                            }
                        }

                        return arr;
                    },
                    deferEvaluation: true
                }),

                self.isParentVisible = ko.computed({
                    read: function () {
                        self.isEventTypeFilter();
                        var arr = _.filter(self.children(), function (item) {
                            return item.newsCount() > 0;
                        });
                        if (arr && arr.length > 0)
                            return true;
                        else if (self.id === 7703 || self.id === 1402) {
                            return true;
                        }                        
                        else
                            return false;
                    }
                }),


                self.isNullo = false;

                return self;
            }

        Filter.Nullo = new Filter();

        Filter.Nullo.isNullo = true;

        Filter.datacontext = function (dc) {
            if (dc) { _dc = dc; }
            return _dc;
        };

        return Filter;
    });