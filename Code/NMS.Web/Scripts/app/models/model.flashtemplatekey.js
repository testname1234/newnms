﻿define('model.flashtemplatekey',
    ['ko', 'underscore'],
    function (ko, _) {

        var
            _dc = this,

            FlashTemplateKey = function () {

                var self = this;

                self.id,
                self.flashKey ,
                self.flashTemplateId,
                self.creationDate,
                self.lastUpdateDate ,
              

                self.isNullo = false;

                self.fillObservableProperties = function () {

                };

                return self;
            }

        FlashTemplateKey.Nullo = new FlashTemplateKey();

        FlashTemplateKey.Nullo.isNullo = true;

        // static member
        FlashTemplateKey.datacontext = function (dc) {
            if (dc) { _dc = dc; }
            return _dc;
        };

        return FlashTemplateKey;
    });