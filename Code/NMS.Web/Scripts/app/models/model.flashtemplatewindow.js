﻿define('model.flashtemplatewindow',
    ['ko', 'underscore'],
    function (ko, _) {

        var
            _dc = this,

            FlashTemplateWindow = function () {

                var self = this;

                self.id,
                self.flashTemplateId ,
                self.name,
                self.topLeftx,
                self.bottomRighty ,
                self.width ,
                self.height ,
                self.creationDate ,
                self.lastUpdateDate ,

                self.isNullo = false;

                self.fillObservableProperties = function () {

                };

                return self;
            }

        FlashTemplateWindow.Nullo = new FlashTemplateWindow();

        FlashTemplateWindow.Nullo.isNullo = true;

        // static member
        FlashTemplateWindow.datacontext = function (dc) {
            if (dc) { _dc = dc; }
            return _dc;
        };

        return FlashTemplateWindow;
    });