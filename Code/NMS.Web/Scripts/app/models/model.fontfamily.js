﻿define('model.fontfamily',
    ['ko', 'underscore'],
    function (ko, _) {

        var
            _dc = this,

            fontfamily = function () {

                var self = this;
               
                self.id,
                self.name,
                self.isSelected = ko.observable(),

                self.isNullo = false;

                self.fillObservableProperties = function () {
                   
                };

                return self;
            }

        fontfamily.Nullo = new fontfamily();

        fontfamily.Nullo.isNullo = true;

        // static member
        fontfamily.datacontext = function (dc) {
            if (dc) { _dc = dc; }
            return _dc;
        };

        return fontfamily;
    });