﻿define('model.generalfiltercontrol',
    ['ko', 'enum', 'utils', 'underscore', 'appdata', 'router', 'config'],
    function (ko, e, utils, _, appdata, router, config) {

        var
            _dc = this,

            GeneralFilterControl = function () {

                var self = this;

                self.program = ko.observableArray(),
                self.userId = ko.observable(appdata.currentUser().id),
                self.dataArray = ko.observableArray([]),
                self.type = ko.observable("stories"),
                self.regenerateFilter =false,
                self.isUserHeadlines = ko.computed({
                    read: function () {
                        return router.currentHash() === config.hashes.production.myheadline;
                    },
                    deferEvaluation: true
                }),
                self.dateFilters = ko.computed({
                    read: function () {
                        var arr = [];
                    
                        var fromDate =new Date(utils.getDefaultUTCDate());
                        var toDate = new Date(moment().add('month', 1).toISOString());

                        var calendar = appdata.headlineCalendarControlReference();

                        if (calendar) {
                            fromDate = new Date(calendar.fromDate());
                            toDate = new Date(calendar.toDate());
                        }
                        fromDate.setHours(0, 0, 0, 0);
                        toDate.setHours(0, 0, 0, 0);

                        if (fromDate === toDate)
                            fromDate = toDate;

                        var episodes = _dc.episodes.getObservableList();

                        episodes = episodes.sort(function (item1, item2) {
                            return item1.startTime() < item2.startTime();
                        });

                        if (episodes && episodes.length > 0) {
                            for (var i = 0, len = episodes.length; i < len; i++) {
                                var stories = _dc.stories.getByEpisodeId(episodes[i].id)();
                                var flag = true;
                                if (self.isUserHeadlines()) {

                                    stories = _.filter(stories, function (item) {
                                        return item.userId === appdata.currentUser().id;
                                    });
                                    flag = stories.length > 0;
                                }
                                if (flag) {
                                    var epDate = new Date(episodes[i].startTime());
                                    epDate.setHours(0, 0, 0, 0);

                                    var calendarRangeFlag = (epDate >= fromDate && epDate <= toDate);

                                    var date = moment(episodes[i].startTime()).format("LL");

                                    var newDateFilterObject = { date: date, count: 0 };
                                    var alreadyContain = utils.arrayIsContainObject(arr, newDateFilterObject, "date")
                                    if (!alreadyContain.flag && calendarRangeFlag) {
                                        newDateFilterObject = { date: date, count: stories.length };
                                        arr.push(newDateFilterObject);
                                    }
                                    else if (alreadyContain.flag) {
                                        arr[alreadyContain.objectIndex].count = arr[alreadyContain.objectIndex].count + stories.length;
                                    }

                                }
                            }
                        }
                        
                        return arr;
                    },
                    deferEvaluation: true
                }),
                self.timeFilters = ko.computed({
                    read: function () {
                        var arr = [];

                        var fromDate = new Date(utils.getDefaultUTCDate());
                        var toDate = new Date(moment().add('month', 1).toISOString());

                        var calendar = appdata.headlineCalendarControlReference();

                        if (calendar) {
                            fromDate = new Date(calendar.fromDate());
                            toDate = new Date(calendar.toDate());
                        }
                        fromDate.setHours(0, 0, 0, 0);
                        toDate.setHours(0, 0, 0, 0);

                        if (fromDate === toDate)
                            fromDate = toDate;

                        var episodes = _dc.episodes.getObservableList();

                        episodes = episodes.sort(function (item1, item2) {
                            return item1.startTime() < item2.startTime();
                        });
                        if (episodes && episodes.length > 0) {
                            for (var i = 0, len = episodes.length; i < len; i++) {
                                var flag = true;
                                var stories = _dc.stories.getByEpisodeId(episodes[i].id)();
                                if (self.isUserHeadlines()) {
                                    stories = _.filter(stories, function (item) {
                                        return item.userId === appdata.currentUser().id;
                                    });
                                    flag = stories.length > 0;
                                }
                                if (flag) {
                                    var epDate = new Date(episodes[i].startTime());
                                    epDate.setHours(0, 0, 0, 0);

                                    var calendarRangeFlag = (epDate >= fromDate && epDate <= toDate);

                                    var time = moment(episodes[i].startTime()).format("hh:mm a");

                                    var newTimeFilterObject = { time: time, count: 0 };

                                    var alreadyContain = utils.arrayIsContainObject(arr, newTimeFilterObject, "time");

                                    if (!alreadyContain.flag && calendarRangeFlag) {
                                        newTimeFilterObject = { time: time, count: stories.length };
                                        arr.push(newTimeFilterObject);
                                    }
                                    else if (alreadyContain.flag) {
                                        arr[alreadyContain.objectIndex].count = arr[alreadyContain.objectIndex].count + stories.length;
                                    }
                                }
                            }
                        }
                        return arr;
                    },
                    deferEvaluation: true
                });

                self.isNullo = false;

                return self;
            };

        GeneralFilterControl.Nullo = new GeneralFilterControl();

        GeneralFilterControl.Nullo.isNullo = true;

        GeneralFilterControl.datacontext = function (dc) {
            if (dc) { _dc = dc; }
            return _dc;
        };

        return GeneralFilterControl;
    });