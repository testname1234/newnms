﻿define('model.generalfiltertickercontrol',
    ['ko', 'enum', 'utils', 'underscore', 'appdata', 'router', 'config'],
    function (ko, e, utils, _, appdata, router, config) {

        var
            _dc = this,

            GeneralFilterTickerControl = function () {

                var self = this;

                self.program = ko.observableArray(),
                self.userId = ko.observable(appdata.currentUser().id),
                self.dataArray = ko.observableArray([]),
                self.type = ko.observable("tickerlines"),
                self.regenerateFilter = false,
                self.isUserTicker = ko.computed({
                    read: function () {
                        return router.currentHash() === config.hashes.production.myTicker;
                    },
                    deferEvaluation: true
                }),
                self.dateFilters = ko.computed({
                    read: function () {
                        var arr = [];

                        var fromDate = new Date(utils.getDefaultUTCDate());
                        var toDate = new Date(moment().add('month', 1).toISOString());

                        var calendar = appdata.tickerCalendarControlReference();

                        if (calendar) {
                            fromDate = new Date(calendar.fromDate());
                            toDate = new Date(calendar.toDate());
                        }
                        fromDate.setHours(0, 0, 0, 0);
                        toDate.setHours(0, 0, 0, 0);

                        if (fromDate === toDate)
                            fromDate = toDate;

                        var mainDataArray = _dc.tickers.getObservableList();

                        mainDataArray = mainDataArray.sort(function (item1, item2) {
                            return item1.creationDate() < item2.creationDate();
                        });

                        if (mainDataArray && mainDataArray.length > 0) {
                            for (var i = 0, len = mainDataArray.length; i < len; i++) {

                                var counter = 0;

                                var epDate = new Date(mainDataArray[i].creationDate());
                                epDate.setHours(0, 0, 0, 0);

                                var calendarRangeFlag = (epDate >= fromDate && epDate <= toDate);

                                var date = moment(mainDataArray[i].creationDate()).format("LL");

                                var newDateFilterObject = { date: date, count: 0 };

                                var alreadyContain = utils.arrayIsContainObject(arr, newDateFilterObject, "date")

                                if (!alreadyContain.flag && calendarRangeFlag) {

                                    for (var j = 0, len2 = mainDataArray.length; j < len2; j++) {
                                        if (moment(mainDataArray[j].creationDate()).format("LL") === date)
                                            counter++;
                                    }
                                    newDateFilterObject.count = counter;
                                    arr.push(newDateFilterObject);
                                }

                            }
                        }

                        return arr;
                    },
                    deferEvaluation: true
                }),
                self.timeFilters = ko.computed({
                    read: function () {
                        var arr = [];

                        var fromDate = new Date(utils.getDefaultUTCDate());
                        var toDate = new Date(moment().add('month', 1).toISOString());

                        var calendar = appdata.tickerCalendarControlReference();

                        if (calendar) {
                            fromDate = new Date(calendar.fromDate());
                            toDate = new Date(calendar.toDate());
                        }
                        fromDate.setHours(0, 0, 0, 0);
                        toDate.setHours(0, 0, 0, 0);

                        if (fromDate === toDate)
                            fromDate = toDate;

                        var mainDataArray = _dc.tickers.getObservableList();

                        mainDataArray = mainDataArray.sort(function (item1, item2) {
                            return item1.creationDate() < item2.creationDate();
                        });
                        if (mainDataArray && mainDataArray.length > 0) {
                            for (var i = 0, len = mainDataArray.length; i < len; i++) {
                                var flag = true;

                                var counter = 0;
                                var epDate = new Date(mainDataArray[i].creationDate());
                                epDate.setHours(0, 0, 0, 0);

                                var calendarRangeFlag = (epDate >= fromDate && epDate <= toDate);

                                var time = moment(mainDataArray[i].creationDate()).format("hh") + ":00 " + moment(mainDataArray[i].creationDate()).format("a");

                                var newTimeFilterObject = { time: time, count: 1 };

                                var alreadyContain = utils.arrayIsContainObject(arr, newTimeFilterObject, "time");

                                if (!alreadyContain.flag && calendarRangeFlag) {

                                    for (var j = 0, len2 = mainDataArray.length; j < len2; j++) {
                                        if (moment(mainDataArray[j].creationDate()).format("hh") + ":00 " + moment(mainDataArray[j].creationDate()).format("a") === time)
                                            counter++;
                                    }
                                    newTimeFilterObject.count = counter;
                                    arr.push(newTimeFilterObject);
                                  
                                }
                            }
                        }
                        return arr;
                    },
                    deferEvaluation: true
                });
                self.init = function () {

                },
                self.isNullo = false;
                return self;
            };

        GeneralFilterTickerControl.Nullo = new GeneralFilterTickerControl();

        GeneralFilterTickerControl.Nullo.isNullo = true;

        GeneralFilterTickerControl.datacontext = function (dc) {
            if (dc) { _dc = dc; }
            return _dc;
        };

        return GeneralFilterTickerControl;
    });