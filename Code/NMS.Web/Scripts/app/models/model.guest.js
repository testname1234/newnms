﻿define('model.guest',
    ['ko', 'config', 'enum'],
    function (ko, config, e) {

        var
            _dc = this,

            Guest = function () {

                var self = this;

                self.id,
                self.name,
                self.designation,
                self.guestType,
                self.locationId,
                self.imageGuid = ko.observable(''),

                self.addedToRundownCount = ko.observable(),
                self.onAiredCount = ko.observable(),
                self.executedOnOtherChannelsCount = ko.observable(),

                self.addedToRundownDetails = ko.observableArray([]),
                self.onAiredDetails = ko.observableArray([]),
                self.executedOnOtherChannelsDetails = ko.observableArray([]),

                self.fillProperties = function (stats) {
                    var arr = [];
                    if (stats && stats.length > 0) {
                        var tempArr1 = [];
                        var tempArr2 = [];
                        var tempArr3 = [];

                        for (var i = 0; i < stats.length; i++) {
                            if (stats[i].StatisticType === e.NewsStatistics.AddedToRundown) {
                                stats[i].EpisodeTimeStr = moment.utc(stats[i].TimeStr).format('hh:mm:ss A');
                                tempArr1.push(stats[i]);
                            }
                            else if (stats[i].StatisticType === e.NewsStatistics.OnAired) {
                                stats[i].EpisodeTimeStr = moment.utc(stats[i].TimeStr).format('hh:mm:ss A');
                                tempArr2.push(stats[i]);
                            } else if (stats[i].StatisticType === e.NewsStatistics.ExecutedOnOtherChannels) {
                                stats[i].EpisodeTimeStr = moment.utc(stats[i].TimeStr).format('hh:mm:ss A');
                                tempArr3.push(stats[i]);
                            }
                        }

                        self.addedToRundownDetails(tempArr1);
                        self.onAiredDetails(tempArr2);
                        self.executedOnOtherChannelsDetails(tempArr3);
                    }
                },

                self.faceValue = ko.computed({
                    read: function () {
                        return config.MediaServerUrl + 'getresource/' + self.imageGuid()
                    },
                    deferEvaluation: true
                }),

                self.isNullo = false;

                return self;
            }

        Guest.Nullo = new Guest();

        Guest.Nullo.isNullo = true;

        Guest.datacontext = function (dc) {
            if (dc) { _dc = dc; }
            return _dc;
        };

        return Guest;
    });