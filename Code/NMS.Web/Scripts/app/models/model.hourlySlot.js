﻿define('model.hourlyslot',
    ['ko','underscore'],
    function (ko,_) {

        var
            _dc = this,

            HourlySlot = function () {

                var self = this;

                self.id,
                self.name,
                self.modelId = 3,
                self.radioDescription = 'Tune channels according to your newsfrom the list below:',
                self.radioPendingText = 'Pending Channels: ',

                self.channelVideos = ko.observableArray([]),
                self.radioStreams = ko.observableArray([]),

                self.processedVideosPercentage = ko.computed({
                    read: function () {
                        var list = self.channelVideos();
                        var processed = 0;
                        for (var i = 0; i < list.length; i++) {
                            if (list[i].isProcessed()) {
                                processed++;
                            }
                        }
                        return Math.round(((processed / list.length) * 100)) + '%';
                    },
                    deferEvaluation: true
                }),

                self.processedPercentage = ko.computed({
                    read: function () {
                        var list = self.radioStreams();
                        var processed = 0;
                        for (var i = 0; i < list.length; i++) {
                            if (list[i].isProcessed()) {
                                processed++;
                            }
                        }
                        return Math.round(((processed / list.length) * 100)) + '%';
                    },
                    deferEvaluation: true
                }),

                self.pending = ko.computed({
                    read: function () {
                        var list = self.radioStreams();
                        var pending = 0;
                        for (var i = 0; i < list.length; i++) {
                            if (list[i].processedPercentage() != '100%') {
                                pending++;
                            }
                        }
                        return pending;
                    },
                    deferEvaluation: true
                }),

                self.pendingTVChannels = ko.computed({
                    read: function () {
                        var list = self.channelVideos();
                        var pending = 0;
                        for (var i = 0; i < list.length; i++) {
                            if (list[i].processedVideosPercentage() != '100%') {
                                pending++;
                            }
                        }
                        return pending;
                    },
                    deferEvaluation: true
                }),

                self.isNullo = false;

                self.fillObservableProperties = function () {
                    var cVideos = ko.observable(_dc.channelVideos.getByHourlySlotId(self.id));
                    if (cVideos().length > 0) {
                        cVideos = Object.keys(cVideos()).sort(function (a, b) { return cVideos()[a] - cVideos()[b] })
                    }
                    self.channelVideos = cVideos();
                    self.radioStreams = _dc.radioStreams.getByHourlySlotId(self.id);
                };

                return self;
            }

        HourlySlot.Nullo = new HourlySlot();

        HourlySlot.Nullo.isNullo = true;

        // static member
        HourlySlot.datacontext = function (dc) {
            if (dc) { _dc = dc; }
            return _dc;
        };

        return HourlySlot;
    });