﻿define('model',
    [
        'model.bunch',
        'model.news',
        'model.user',
        'model.resource',
        'model.newssource',
        'model.newscategory',
        'model.channel',
        'model.program',
        'model.episode',
        'model.programelement',
        'model.story',
        'model.screenelement',
        'model.screentemplate',
        'model.screenflow',
        'model.guest',
        'model.newsTags',
        'model.newsbucketitem',
        'model.newslocation',
        'model.filter',
        'model.newsfilter',
        'model.newscomment',
        'model.comment',
        'model.sourcefiltercontrol',
        'model.programselectionandinfocontrol',
        'model.programflowdesignercontrol',
        'model.screentemplatedesignercontrol',
        'model.programpreviewcontrol',
        'model.programrundowncontrol',
        'model.contentviewercontrol',
        'model.templateelement',
        'model.calendarcontrol',
        'model.channelvideo',
        'model.hourlyslot',
        'model.newspaper',
        'model.dailynewspaper',
        'model.newspaperpage',
        'model.radiostation',
        'model.radiostream',
        'model.multiselectitem',
        'model.audiocommentcontrol',
        'model.screentemplatemics',
        'model.screentemplatecameras',
        'model.userrole',
        'model.runorder',
        'model.rostory',
        'model.storyitem',
        'model.casperitem',
        'model.casperrunorder',
        'model.profile',
        'model.fontfamily',
        'model.configsetting',
        'model.casparaction',
        'model.casparimage',
        'model.casparitem',
        'model.casparcategory',
        'model.casparitempermission',
        'model.casparrundown',
        'model.CasparRundownItemitem',
        'model.casparconfigsetting',
        'model.crop',
        'model.decklinkinput',
        'model.flashtemplatekey',
        'model.flashtemplatewindow',
        'model.template',
        'model.templatedata',
        'model.templateitem',
        'model.transformation',
        'model.usercasparaction',
        'model.usertemplatetype',
        'model.casparvideo',
        'model.event',
        'model.newsalert',
        'model.notification',
        'model.templatekey',
        'model.ticker',
        'model.tickerline',
        'model.generalfiltercontrol',
        'model.tickerwritercontrol',
        'model.generalfiltertickercontrol',
        'model.category',
        'model.leftpanel',
        'model.tickerreportercontrol',
        'model.userfilter',
        'model.xmlparser',
        'model.tickerImages',
        'model.newsfile',
        'model.newsfiledetail',
        'model.newsfolder',
        'model.newsfilefolderhistory',
        'model.newsfileresource',
        'model.newsfilestatushistory',
        'model.workrolefolder'
    ],
    function (
        Bunch,
        News,
        User,
        Resource,
        NewsSource,
        NewsCategory,
        Channel,
        Program,
        Episode,
        ProgramElement,
        Story,
        ScreenElement,
        ScreenTemplate,
        ScreenFlow,
        Guest,
        NewsTag,
        NewsBucketItem,
        NewsLocation,
        Filter,
        NewsFilter,
        NewsComment,
        Comment,
        SourceFilterControl,
        ProgramSelectionAndInfoControl,
        ProgramFlowDesignerControl,
        ScreenTemplateDesignerControl,
        ProgramPreviewControl,
        ProgramRundownControl,
        ContentViewerControl,
        TemplateElement,
        CalendarControl,
        ChannelVideo,
        HourlySlot,
        NewsPaper,
        DailyNewsPaper,
        NewsPaperPage,
        RadioStation,
        RadioStream,
        MultiSelectItem,
        AudioCommentControl,
        ScreenTemplateMics,
        ScreenTemplateCameras,
        UserRole,
        RunOrder,
        ROStory,
        StoryItem,
        CasperItem,
        CasperRunOrder,
        Profile,
        Fontfamily,
        ConfigSetting,
        CasparAction,
        CasparImage,
        CasparItem,
        CasparCategory,
        CasparItemPermission,
        CasparRundown,
        CasparRundownItem,
        CasparConfigSetting,
        Crop,
        DecklinkInput,
        FlashTemplateKey,
        FlashTemplateWindow,
        Template,
        TemplateData,
        TemplateItem,
        Transformation,
        UserCasparAction,
        UserTemplateType,
        CasparVideo,
        Event,
        NewsAlert,
        Notification,
        TemplateKey,
        Ticker,
        TickerLine,
        GeneralFilterControl,
        TickerWriterControl,
        GeneralFilterTickerControl,
        Category,
        LeftPanel,
        TickerReporterControl,
        UserFilter,
        XmlParser,
        TickerImage,
        NewsFile,
        NewsFileDetail,
        NewsFolder,
        NewsFileFolderHistory,
        NewsFileResource,
        NewsFileStatusHistory,
        WorkRoleFolder
        ) {
        
        var
            model = {
                Bunch: Bunch,
                News: News,
                User: User,
                Resource: Resource,
                NewsSource: NewsSource,
                NewsCategory: NewsCategory,
                Channel: Channel,
                Program: Program,
                Episode: Episode,
                ProgramElement: ProgramElement,
                Story: Story,
                ScreenElement: ScreenElement,
                ScreenTemplate: ScreenTemplate,
                ScreenFlow: ScreenFlow,
                Guest: Guest,
                NewsBucketItem: NewsBucketItem,
                NewsTag: NewsTag,
                NewsLocation: NewsLocation,
                Filter: Filter,
                NewsFilter: NewsFilter,
                NewsComment: NewsComment,
                Comment: Comment,
                SourceFilterControl: SourceFilterControl,
                ProgramSelectionAndInfoControl: ProgramSelectionAndInfoControl,
                ProgramFlowDesignerControl: ProgramFlowDesignerControl,
                ScreenTemplateDesignerControl: ScreenTemplateDesignerControl,
                ProgramPreviewControl: ProgramPreviewControl,
                ProgramRundownControl: ProgramRundownControl,
                ContentViewerControl: ContentViewerControl,
                TemplateElement: TemplateElement,
                CalendarControl: CalendarControl,
                ChannelVideo: ChannelVideo,
                HourlySlot: HourlySlot,
                NewsPaper: NewsPaper,
                DailyNewsPaper: DailyNewsPaper,
                NewsPaperPage: NewsPaperPage,
                RadioStation: RadioStation,
                RadioStream: RadioStream,
                MultiSelectItem: MultiSelectItem,
                AudioCommentControl: AudioCommentControl,
                ScreenTemplateMics: ScreenTemplateMics,
                ScreenTemplateCameras: ScreenTemplateCameras,
                UserRole: UserRole,
                RunOrder: RunOrder,
                ROStory: ROStory,
                StoryItem: StoryItem,
                CasperItem: CasperItem,
                CasperRunOrder: CasperRunOrder,
                Profile: Profile,
                Fontfamily: Fontfamily,
                ConfigSetting: ConfigSetting,
                CasparAction: CasparAction,
                CasparImage: CasparImage,
                CasparItem: CasparItem,
                CasparCategory: CasparCategory,
                CasparItemPermission: CasparItemPermission,
                CasparRundown: CasparRundown,
                CasparRundownItem: CasparRundownItem,
                CasparConfigSetting: CasparConfigSetting,
                Crop: Crop,
                DecklinkInput: DecklinkInput,
                FlashTemplateKey: FlashTemplateKey,
                FlashTemplateWindow: FlashTemplateWindow,
                Template: Template,
                TemplateData: TemplateData,
                TemplateItem: TemplateItem,
                Transformation: Transformation,
                UserCasparAction: UserCasparAction,
                UserTemplateType: UserTemplateType,
                CasparVideo: CasparVideo,
                Event: Event,
                NewsAlert: NewsAlert,
                Notification: Notification,
                TemplateKey: TemplateKey,
                Ticker: Ticker,
                TickerLine: TickerLine,
                GeneralFilterControl: GeneralFilterControl,
                TickerWriterControl: TickerWriterControl,
                GeneralFilterTickerControl: GeneralFilterTickerControl,
                Category: Category,
                LeftPanel: LeftPanel,
                TickerReporterControl: TickerReporterControl,
                UserFilter: UserFilter,
                XmlParser: XmlParser,
                TickerImage: TickerImage,
                NewsFile:NewsFile,
                NewsFileDetail:NewsFileDetail,
                NewsFolder:NewsFolder,
                NewsFileFolderHistory:NewsFileFolderHistory,
                NewsFileResource: NewsFileResource,
                NewsFileStatusHistory: NewsFileStatusHistory,
                WorkRoleFolder: WorkRoleFolder
            };

        model.setDataContext = function (dc) {
            model.Bunch.datacontext(dc);
            model.User.datacontext(dc);
            model.News.datacontext(dc);
            model.Resource.datacontext(dc);
            model.NewsSource.datacontext(dc);
            model.NewsCategory.datacontext(dc);
            model.Channel.datacontext(dc);
            model.Program.datacontext(dc);
            model.Episode.datacontext(dc);
            model.ProgramElement.datacontext(dc);
            model.Story.datacontext(dc);
            model.ScreenElement.datacontext(dc);
            model.ScreenTemplate.datacontext(dc);
            model.ScreenFlow.datacontext(dc);
            model.Guest.datacontext(dc);
            model.NewsBucketItem.datacontext(dc);
            model.NewsTag.datacontext(dc);
            model.NewsLocation.datacontext(dc);
            model.Filter.datacontext(dc);
            model.NewsFilter.datacontext(dc);
            model.NewsComment.datacontext(dc);
            model.Comment.datacontext(dc);
            model.SourceFilterControl.datacontext(dc);
            model.ProgramSelectionAndInfoControl.datacontext(dc);
            model.ProgramFlowDesignerControl.datacontext(dc);
            model.ScreenTemplateDesignerControl.datacontext(dc);
            model.ProgramPreviewControl.datacontext(dc);
            model.ProgramRundownControl.datacontext(dc);
            model.ContentViewerControl.datacontext(dc);
            model.TemplateElement.datacontext(dc);
            model.CalendarControl.datacontext(dc);
            model.ChannelVideo.datacontext(dc);
            model.HourlySlot.datacontext(dc);
            model.Channel.datacontext(dc);
            model.NewsPaper.datacontext(dc);
            model.DailyNewsPaper.datacontext(dc);
            model.NewsPaperPage.datacontext(dc);
            model.RadioStation.datacontext(dc);
            model.RadioStream.datacontext(dc);
            model.MultiSelectItem.datacontext(dc);
            model.AudioCommentControl.datacontext(dc);
            model.ScreenTemplateMics.datacontext(dc);
            model.ScreenTemplateCameras.datacontext(dc);
            model.UserRole.datacontext(dc);
            model.RunOrder.datacontext(dc);
            model.ROStory.datacontext(dc);
            model.StoryItem.datacontext(dc);
            model.CasperItem.datacontext(dc);
            model.CasperRunOrder.datacontext(dc);
            model.Profile(dc);
            model.Fontfamily(dc);
            model.ConfigSetting(dc);
            model.CasparAction(dc);
            model.CasparImage(dc);
            model.CasparItem(dc);
            model.CasparCategory(dc);
            model.CasparItemPermission(dc);
            model.CasparRundown(dc);
            model.CasparRundownItem(dc);
            model.CasparConfigSetting(dc);
            model.Crop(dc);
            model.DecklinkInput(dc);
            model.FlashTemplateKey(dc);
            model.FlashTemplateWindow(dc);
            model.Template(dc);
            model.TemplateData(dc);
            model.TemplateItem(dc);
            model.Transformation(dc);
            model.UserCasparAction(dc);
            model.UserTemplateType(dc);
            model.CasparVideo(dc);
            model.Event.datacontext(dc);
            model.NewsAlert.datacontext(dc);
            model.Notification.datacontext(dc);
            model.TemplateKey.datacontext(dc);
            model.Ticker.datacontext(dc);
            model.TickerLine.datacontext(dc);
            model.GeneralFilterControl.datacontext(dc);
            model.TickerWriterControl.datacontext(dc);
            model.GeneralFilterTickerControl.datacontext(dc);
            model.Category.datacontext(dc);
            model.LeftPanel.datacontext(dc);
            model.TickerReporterControl.datacontext(dc);
            model.UserFilter.datacontext(dc);
            model.XmlParser.datacontext(dc);
            model.TickerImage.datacontext(dc);

            model.NewsFile.datacontext(dc);
            model.NewsFileDetail.datacontext(dc);
            model.NewsFolder.datacontext(dc);
            model.NewsFileFolderHistory.datacontext(dc);
            model.NewsFileResource.datacontext(dc);
            model.NewsFileStatusHistory.datacontext(dc);
            model.WorkRoleFolder.datacontext(dc);
        };

        return model;
    });