﻿define('model.leftpanel',
    ['ko', 'moment', 'utils', 'config', 'appdata','enum'],
    function (ko, moment, utils, config, appdata,e) {
        var
            _dc = this,

            LeftPanel = function () {

                var self = this;
                self.stories = ko.computed({
                    read: function () {
                        appdata.arrangeNewsFlag();
                        var tempArray = [];
                        tempArray = _dc.stories.getByEpisodeId(appdata.currentEpisode().id)();
                        return tempArray;
                    },
                    deferEvaluation: true
                }),

                self.segments = ko.computed({
                    read: function () {
                        appdata.arrangeNewsFlag();
                        
                        var programElements =[];
                        programElements = _dc.programElements.getByEpisodeId(appdata.currentEpisode().id)();
                        var segmentArray = [];
                        if (programElements.length > 0) {
                            segmentArray = _.filter(programElements, function (programElement) {
                                return programElement.programElementType === e.ProgramElementType.Segment;
                            });
                        }
                        return segmentArray;
                    },
                    deferEvaluation: true
                }),
                self.isStoryBucketVisible = ko.observable(false),
                self.toggleStoryBucket = function () {
                    if (!self.isStoryBucketVisible()) {
                        self.isStoryBucketVisible(true);
                    }
                    else {
                        self.isStoryBucketVisible(false);
                    }
                },
                self.navigateStoryTemplate = function (data) {
                    //console.log(data);
                };
                self.isNullo = false;

                return self;
            }

        LeftPanel.Nullo = new LeftPanel();

        LeftPanel.Nullo.isNullo = true;


        LeftPanel.datacontext = function (dc) {
            if (dc) { _dc = dc; }
            return _dc;
        };

        return LeftPanel;
    });