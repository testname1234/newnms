﻿define('model.mapper',
    ['jquery', 'model', 'config', 'underscore', 'moment', 'datacontext'],
    function ($, model, config, _, moment, _dc) {
        var
            user = {
                getDtoId: function (dto) {
                    return dto.UserId;
                },
                getSortedValue: function (dto) {
                    return dto.time;
                },
                fromDto: function (dto, item) {
                    item = item || new model.User();

                    item.id = dto.UserId,
                    item.name = dto.Name,
                    item.displayName = dto.Name;

                    if (dto.Fullname)
                        item.name = dto.Fullname;

                    if (dto.UserRoles)
                        item.userRoles(dto.UserRoles);

                    if (dto.WorkRoleId)
                        item.userRoles(dto.WorkRoleId);

                    return item;
                }
            },

            userFilter = {
                getDtoId: function (dto) {
                    return dto.FilterId;
                },
                getSortedValue: function (dto) {
                    return dto.LastUpdateDate;
                },
                fromDto: function (dto, item) {
                    item = item || new model.UserFilter();


                    item.id = dto.FilterId,
                    item.name = dto.Name || '',
                    item.value = dto.Value,
                    item.filterTypeId = dto.FilterTypeId,
                    item.cssClass(dto.CssClass || ''),
                    item.parentId = dto.ParentId || -1;

                    return item;
                }
            },

            guest = {
                getDtoId: function (dto) {
                    return dto.CelebrityId;
                },
                getSortedValue: function (dto) {
                    return dto.time;
                },
                fromDto: function (dto, item) {
                    item = item || new model.Guest();

                    item.id = dto.CelebrityId,
                    item.name = dto.Name || '',
                    item.designation = dto.Designation || '',
                    item.imageGuid(dto.ImageGuid || ''),
                    item.guestType = dto.CategoryId,
                    item.addedToRundownCount(dto.AddedToRundown || 0),
                    item.onAiredCount(dto.OnAired || 0),
                    item.executedOnOtherChannelsCount(dto.ExecutedOnOtherChannels || 0),
                    item.locationId = dto.LocationId;

                    item.fillProperties(dto.CelebrityStatistics || []);

                    return item;
                }
            },

            event = {
                getDtoId: function (dto) {
                    return dto.EventInfoId;
                },
                getSortedValue: function (dto) {
                    return dto.StartTimeStr;
                },
                fromDto: function (dto, item) {
                    item = item || new model.Event();
                    item.id = dto.EventInfoId,
                    item.name = dto.Name || '',
                    item.startTime(dto.StartTimeStr),
                    item.endTime(dto.EndTimeStr),
                    item.searchTags = dto.SearchTags || ''

                    return item;
                }
            },

            bunch = {
                getDtoId: function (dto) {
                    return dto.BunchGuid;
                },
                getSortedValue: function (dto) {
                    return dto.LastUpdateDateStr;
                },
                fromDto: function (dto, item) {
                    item = item || new model.Bunch();

                    item.id = dto.BunchGuid,

                    item.fillObservableProperties();

                    return item;
                }
            },

            news = {
                getDtoId: function (dto) {
                    return dto._id;
                },
                getSortedValue: function (dto) {
                    return dto.LastUpdateDateStr;
                },
                fromDto: function (dto, item) {
                    item = item || new model.News();
                    
                    var news = dto;
                    var newstags = [];
                    var newsOrg = [];

                    if (dto.Tags && dto.Tags.length > 0) {
                        newstags = _.pluck(dto.Tags, 'Tag');
                    }
                    if (dto.Organization && dto.Organization.length > 0) {
                        for (var o = 0 ; o < dto.Organization.length ; o++) {
                            newsOrg.push({ id: dto.Organization[o].OrganizationId, name: dto.Organization[o].Name });
                        }
                    }


                    if (news.Progress) {
                        item.progress = news.Progress;
                    }
                    if (dto.Source && !news.Source) {
                        news.Source = dto.Source;
                    }

                    item.id = news._id,
                    item.programId(news.ProgramId);
                    item.assignedTo(news.AssignedTo);
                    item.newsId = news.NewsId,
                    item.bunchId = news.BunchGuid,
                    item.title(news.Title || ''),
                    item.description(news.Description || ''),
                    item.source(news.Source || ''),
                    item.version = news.Version,
                    item.parentNewsId = news.ParentNewsId,
                    item.languageCode = news.LanguageCode || 'en',
                    item.categoyIds = news.CategoryIds,
                    item.locationIds = news.LocationIds,
                    item.otherChannelExecutionCount(news.OtherChannelExecutionCount || 0),
                    item.onAirCount(news.OnAirCount || 0),
                    item.addedToRundownCount(news.AddedToRundownCount || 0),
                    item.newsTickerCount(dto.NewsTickerCount || 0),
                    item.newsStatistics(news.NewsStatistics),
                    item.shortDescription(news.ShortDescription || ''),
                    item.categories(news.Categories);
                    item.bureauId(dto.ReporterBureauId);

                    if (dto.Tags && dto.Tags.length > 0) {
                        item.bunchTags([]);
                        var arr = _.sortBy(dto.Tags, 'Rank')
                        arr.reverse()
                        item.bunchTags.push(arr[0]);
                        item.bunchTags.push(arr[1]);
                        item.bunchTags.push(arr[2]);
                    }
                    if (dto.CategoryId)
                    {
                        item.categoryId=dto.CategoryId;
                    }
                    if (dto.Slug)
                    {
                        item.slug(dto.Slug);
                    }
                    if (dto.Highlight)
                    {
                        item.highlight(dto.Highlight);
                    }
                    if (dto.ReporterId && dto.ReporterId !=0)
                    {
                        ///console.log(dto.ReporterId);
                        item.reporterIdDisplay(parseInt(dto.ReporterId));
                        //console.log(item.reporterIdDisplay());
                    }
                    if (dto.Tickers) {
                        var temp = ticker.fromDto(dto.Tickers[0]);
                        var tempLines = [];
                        for (var i = 0; i < dto.Tickers[0].TickerLines.length; i++) {
                            tempLines.push(tickerLine.fromDto(dto.Tickers[0].TickerLines[i]));
                        }
                        temp["tickerLines"] = tempLines;
                        item.tickers(temp);
                    }
                    if (dto.Suggestions) {
                        item.suggestions(dto.Suggestions);
                    }
                    if (dto.NewsPaperdescription) {
                        item.newsPaperdescription(dto.NewsPaperdescription);
                    }

                    if (news.Programmes && news.Programmes.length > 0) {
                        var temp = new Array();
                        for (var i = 0; i < news.Programmes.length; i++) {
                            temp[i] = program.fromDto(news.Programmes[i], item.programs[i]);
                        }
                        item.programs(temp);
                    }

                    if (news.OtherChannelProgrammes && news.OtherChannelProgrammes.length > 0) {
                        var temp = new Array();
                        for (var i = 0; i < news.OtherChannelProgrammes.length; i++) {
                            temp[i] = program.fromDto(news.OtherChannelProgrammes[i], item.otherPrograms[i]);
                        }
                        item.otherPrograms(temp);
                    }

                    if (news.InternationalProgrammes && news.InternationalProgrammes.length > 0) {
                        var temp = new Array();
                        for (var i = 0; i < news.InternationalProgrammes.length; i++) {
                            temp[i] = program.fromDto(news.InternationalProgrammes[i], item.internationalPrograms[i]);
                        }
                        item.internationalPrograms(temp);
                    }

                    if (news.PublishTimeStr && new Date(news.PublishTimeStr).getFullYear() > 1900) {
                        item.publishTime = news.PublishTimeStr;
                    }
                    else {
                        item.publishTime = news.LastUpdateDateStr;
                    }

                    if (news.Locations && news.Locations.length > 0) {
                        var tempLocations = new Array();
                        for (var i = 0; i < news.Locations.length; i++) {
                            tempLocations[i] = newsLocation.fromDto(news.Locations[i], item.reportedLocations[i]);
                        }
                        item.reportedLocations(tempLocations);
                    }

                    if (news.Resources && news.Resources.length > 0) {
                        if (!dto.ThumbnailUrl) {
                            dto.ThumbnailUrl = news.Resources[0].thumbImageUrl
                        }
                    }

                    item.defaultImage(dto.ThumbnailUrl),
                    item.isVerified(news.isVerified),
                    item.isAired(news.IsAired),
                    item.isRecommended(news.IsRecommended),

                    item.newsTypeId = news.NewsTypeId,

                    item.creationDate = news.CreationDateStr,
                    item.lastUpdateDateStr = news.LastUpdateDateStr,

                    item.resourceEdits = news.ResourceEdit,

                    item.organization(newsOrg);
                    item.tags(newstags),
                    item.sourceTypeId = news.SourceTypeId,
                    item.searchTerm = dto.SearchTerm,
                    item.score = dto.Score;

                    if (dto.Coverage || !dto.Coverage)
                        item.Coverage = dto.Coverage;
                    if (dto.EventResource && dto.EventResource.length > 0)
                        item.EventResource = dto.EventResource;
                    if (dto.EventTypeName)
                        item.EventTypeName = dto.EventTypeName;
                    if (dto.EventType)
                        item.EventType = dto.EventType;
                    if (dto.EventReporter && dto.EventReporter.length > 0)
                    {
                        item.EventReporter = dto.EventReporter;
                        item.EventReporterFilled(true);
                    }




                    item.fillObservableProperties();

                    return item;
                }
            },

            newsAlert = {
                getDtoId: function (dto) {
                    return dto.AlertId;
                },
                getSortedValue: function (dto) {
                    return dto.CreationDateStr;
                },
                fromDto: function (dto, item) {
                    item = item || new model.NewsAlert();

                    item.id = dto.AlertId,
                    item.title = dto.Title,
                    item.url = dto.Url,
                    item.thumbUrl = dto.ThumbUrl || '';

                    return item;
                }
            },

            notification = {
                getDtoId: function (dto) {
                    return dto.NotificationId;
                },
                getSortedValue: function (dto) {
                    return dto.CreationDateStr;
                },
                fromDto: function (dto, item) {

                    item = item || new model.Notification();

                    var tempData;
                    if (dto.Argument) {
                        try {
                            tempData = JSON.parse(dto.Argument);
                        }
                        catch (ex) {
                            //console.log("Exception from error logging");
                        }

                    }
                    if (tempData)
                        item.slug(tempData.StorySlug || '');
                    item.id = dto.NotificationId;
                    item.type = dto.NotificationTypeId;
                    item.title = dto.Title;
                    item.argument = dto.Argument;
                    item.recipientId = dto.RecipientId;
                    item.senderId = dto.SenderId;
                    item.isRead(dto.IsRead || false);
                    item.storyScreenTemplateId = dto.Slotscreentemplateid;
                    item.storyId = dto.SlotId;
                    item.creationDate(dto.CreationDateStr);


                    return item;
                }
            },

            resource = {
                getDtoId: function (dto) {
                    if (dto.Guid) {
                        return dto.Guid;
                    }
                    if (dto.SlotScreenTemplateResourceId) {
                        return dto.SlotScreenTemplateResourceId;
                    }
                },
                getSortedValue: function (dto) {
                    if (dto.Guid) {
                        return dto.Guid;
                    }
                    if (dto.SlotScreenTemplateResourceId) {
                        return dto.SlotScreenTemplateResourceId;
                    }
                },
                fromDto: function (dto, item) {

                    item = item || new model.Resource();

                    if (dto.Guid) {
                        item.id = dto.Guid;
                        item.guid = dto.Guid;
                    }

                    if (dto.ResourceGuid) {
                        item.id = dto.ResourceGuid;
                        item.guid = dto.ResourceGuid;
                    }

                    if (dto.SlotTemplateScreenElementResourceId) {
                        item.guid = dto.ResourceGuid;
                        item.id = dto.SlotTemplateScreenElementResourceId;
                    }
                    if (dto.SlotTemplateScreenElementId) {
                        item.storyTemplateScreenElementId = dto.SlotTemplateScreenElementId
                    }
                    if (dto.IsGoogleResource) {
                        item.isGoogleResource(dto.IsGoogleResource);
                    }
                    if (dto.DirectUlr) {
                        item.directUlr(dto.DirectUlr);
                    }
                    if (dto.DirectThumbUrl) {
                        item.directThumbUrl(dto.DirectThumbUrl);
                    }

                    item.lastUpdateDate(dto.LastUpdateDateStr || dto.LastUpdateDate || ''),

                    item.storyScreenTemplateId = dto.SlotScreenTemplateId,
                    item.creationDateStr = dto.CreationDateStr || dto.CreationDate || '',
                    item.type = dto.ResourceTypeId || dto.ResourceType,
                    
                    item.slug = dto.Slug,
                    item.newsId = dto.NewsId == undefined ? dto.NewsFileId : dto.NewsId,
                    item.caption = dto.Caption,
                    item.duration = dto.Duration || 0,
                    item.creationDate = dto.CreationDateStr || dto.CreationDate || '',
                    item.description = dto.Description || '',
                    item.fileTitle = dto.FileTitle || '',
                    item.comment(dto.Comment || ''),
                    item.resourceDate(dto.ResourceDate || ''),
                 
                    item.isLatest = dto.IsLatest || false;
                    item.setVariable();

                    return item;
                }
            },

            program = {
                getDtoId: function (dto) {
                    return dto.ProgramId;
                },
                getSortedValue: function (dto) {
                    return dto.time;
                },
                fromDto: function (dto, item) {
                    item = item || new model.Program();

                    item.channelId(dto.ChannelId),
                    item.id = dto.ProgramId,
                    item.name = dto.Name || '',
                    item.episodes = dto.Episodes || [],
                    item.maxStoryCount = dto.MaxStoryCount || 0,
                    item.minStoryCount = dto.MinStoryCount || 0,


                    item.fillObservableProperties();
                    if (dto.StartupUrl) {
                        item.startupGuid(dto.StartupUrl);
                        item.startupThumbUrl(config.MediaServerUrl + 'getthumb/' + item.startupGuid());
                        item.startupUrl(config.MediaServerUrl + 'getResource/' + item.startupGuid());
                        item.isStartUpUrlVisible(true);
                    }
                    if (dto.BucketId) {
                        item.bucketId = dto.BucketId || 0;
                    }
                    if (dto.ApiKey) {
                        item.apiKey = dto.ApiKey;
                    }
                    if (dto.FilterId) {
                        item.filterId = dto.FilterId;
                    }

                    if (dto.CreditsUrl) {
                        item.creditsGuid(dto.CreditsUrl);
                        item.creditsThumbUrl(config.MediaServerUrl + 'getthumb/' + item.creditsGuid());
                        item.creditsUrl(config.MediaServerUrl + 'getResource/' + item.creditsGuid());
                        item.isCreaditUrlVisible(true);
                    }
                    return item;
                }
            },

            episode = {
                getDtoId: function (dto) {
                    return dto.EpisodeId;
                },
                getSortedValue: function (dto) {
                    return dto.time;
                },
                fromDto: function (dto, item) {
                    item = item || new model.Episode();

                    item.id = dto.EpisodeId,
                    item.programId = dto.ProgramId,
                    item.startTime(dto.FromStr),
                    item.endTime(moment(dto.FromStr).add(30, 'minute').toISOString()),//dto.ToStr),
                    item.suggestedFlow = dto.SuggestedFlow || [],
                    item.previewGuid(dto.PreviewGuid || ''),
                    item.otherChannelsFlow = dto.OtherChannelsFlow || [];

                    return item;
                }
            },

            channel = {
                getDtoId: function (dto) {
                    return dto.ChannelId;
                },
                getSortedValue: function (dto) {
                    return dto.ChannelId;
                },
                fromDto: function (dto, item) {
                    item = item || new model.Channel();

                    item.id = dto.ChannelId,
                    item.name = dto.Name.toUpperCase() || 'Unknown';
                    if (dto.IsOtherChannel != undefined) {
                        item.isOtherChannel = dto.IsOtherChannel;
                    }
                    item.fillObservableProperties();

                    return item;
                }
            },

            newsPaper = {
                getDtoId: function (dto) {
                    return dto.NewsPaperId;
                },
                getSortedValue: function (dto) {
                    return dto.NewsPaperId;
                },
                fromDto: function (dto, item) {
                    item = item || new model.NewsPaper();

                    item.id = dto.NewsPaperId;
                    item.name = dto.Name || 'Unknown';
                    item.creationDate = dto.CreationDateStr;
                    item.lastUpdateDate = dto.LastUpdateDateStr;

                    item.fillObservableProperties();

                    return item;
                }
            },

            dailyNewsPaper = {
                getDtoId: function (dto) {
                    return dto.DailyNewsPaperId;
                },
                getSortedValue: function (dto) {
                    return dto.DailyNewsPaperId;
                },
                fromDto: function (dto, item) {
                    item = item || new model.DailyNewsPaper();

                    item.id = dto.DailyNewsPaperId;
                    item.newsPaperId = dto.NewsPaperId;
                    item.name = moment(dto.DateStr).format(config.DateFormat);

                    item.episodeDate = item.name;

                    item.newsPaperDateTime = new Date(dto.DateStr);
                    item.creationDate = dto.CreationDateStr;
                    item.lastUpdateDate = dto.LastUpdateDateStr;

                    item.fillObservableProperties();

                    return item;
                }
            },

            newsPaperPage = {
                getDtoId: function (dto) {
                    return dto.NewsPaperPageId;
                },
                getSortedValue: function (dto) {
                    return dto.NewsPaperPageId;
                },
                fromDto: function (dto, item) {
                    item = item || new model.NewsPaperPage();

                    item.id = dto.NewsPaperPageId;
                    item.dailyNewsPaperId = dto.DailyNewsPaperId;
                    item.title = dto.PageTitle;
                    item.imageGuid = dto.ImageGuid;
                    item.creationDate = dto.CreationDateStr;
                    item.lastUpdateDate = dto.LastUpdateDateStr;
                    if (dto.IsProcessed) {
                        item.isProcessed(dto.IsProcessed);
                    }
                    if (dto.NewsPaperPagesPart) {
                        item.newsPaperPagesParts(dto.NewsPaperPagesPart);
                    }

                    item.fillObservableProperties();

                    return item;
                }
            },

            radioStation = {
                getDtoId: function (dto) {
                    return dto.RadioStationId;
                },
                getSortedValue: function (dto) {
                    return dto.RadioStationId;
                },
                fromDto: function (dto, item) {
                    item = item || new model.RadioStation();

                    item.id = dto.RadioStationId,
                    item.name = dto.Name || 'Unknown';
                    item.creationDate = dto.CreationDateStr;
                    item.lastUpdateDate = dto.LastUpdateDateStr;

                    item.fillObservableProperties();

                    return item;
                }
            },

            radioStream = {
                getDtoId: function (dto) {
                    return dto.RadioStreamId;
                },
                getSortedValue: function (dto) {
                    return dto.RadioStreamId;
                },
                fromDto: function (dto, item) {
                    item = item || new model.RadioStream();

                    item.id = dto.RadioStreamId,
                    item.channelId(dto.RadioStationId);
                    item.creationDate = dto.CreationDateStr;
                    item.lastUpdateDate = dto.LastUpdateDateStr;
                    item.from = dto.FromStr;
                    item.to = dto.ToStr;
                    item.fromHour = moment(item.from).format('HH:00');//moment(item.from).format('HH:00');
                    item.toHour = moment(item.to).format('HH:00');
                    item.hourlySlotId = item.fromHour;
                    item.episodeDate = moment(new Date(item.from)).format(config.DateFormat);

                    if (dto.IsProcessed) {
                        item.isProcessed(dto.IsProcessed);
                    }

                    item.url = dto.Url;
                    item.programId = dto.ProgramId;

                    item.fillObservableProperties();

                    return item;
                }
            },

            screenElement = {
                getDtoId: function (dto) {
                    return dto.ScreenElementId;
                },
                getSortedValue: function (dto) {
                    return dto.time;
                },
                fromDto: function (dto, item) {
                    item = item || new model.ScreenElement();

                    item.id = dto.ScreenElementId,
                    item.name = dto.Name || '',
                    item.thumbGuid(dto.ThumbGuid),
                    item.dragIconUrl = dto.DragIconUrl || '',
                    item.cssClass = dto.CssClass || '';

                    return item;
                }
            },
            templateKey = {
                getDtoId: function (dto) {
                    return dto.ScreenTemplatekeyId;
                },
                getSortedValue: function (dto) {
                    return dto.CreationDateStr;
                },
                fromDto: function (dto, item) {
                    item = item || new model.TemplateKey();

                    item.id = dto.ScreenTemplatekeyId,
                    item.keyName = dto.KeyName || '',
                    item.keyValue(dto.KeyValue || ''),
                    item.storyScreenTemplatekeyId = dto.ScreenTemplateId,
                    item.languageCode(dto.Languagecode || 'ur'),
                    item.storyScreenTemplateId = dto.ScreenTemplatekeyId;
                    if (dto.Top)
                        item.top = dto.Top;
                    if(dto.Left)
                        item.left = dto.Left;
                    if (dto.Right)
                        item.right = dto.Right;
                    if (dto.Bottom)
                        item.bottom = dto.Bottom;


                    return item;
                },
                toDto: function (item) {
                    var dto = {
                        KeyName: item.keyName,
                        KeyValue: item.keyValue(),
                        SlotScreenTemplateId: item.storyScreenTemplateId,
                        ScreenTemplatekeyId: item.screentemplateKeyId,
                        ScreenTemplateId: item.screentemplateId,
                        Languagecode: item.languageCode(),
                        Top: item.top,
                        Left: item.left,
                        Right: item.right,
                        Bottom:item.bottom
                    };

                    return dto;
                },
                getClone: function (entity) {
                    var newObj = new model.TemplateKey();
                    newObj.id = entity.id,
                    newObj.keyName = entity.keyName || '',
                    newObj.keyValue(entity.keyValue() || ''),
                    newObj.storyScreenTemplatekeyId = entity.storyScreenTemplatekeyId,
                    newObj.languageCode(entity.languageCode() || 'ur'),
                    newObj.storyScreenTemplateId = entity.storyScreenTemplateId;
                    newObj.top = entity.top;
                    newObj.bottom = entity.bottom;
                    newObj.left = entity.left;
                    newObj.right = entity.right;
                    return newObj;
                }
            },

            ticker = {
                getClone: function (entity) {

                    var newObj = new model.Ticker();

                    //newObj.id = entity.id,
                    //newObj.onAiredTime(entity.onAiredTime),
                    //newObj.severity(entity.severity()),
                    //newObj.sequenceId = entity.sequenceId,
                    //newObj.userId(entity.userId()),
                    //newObj.lastUpdateDate(entity.lastUpdateDate()),
                    //newObj.creationDate(entity.creationDate()),
                    //newObj.breakingStatusId(entity.breakingStatusId()),
                    //newObj.latestStatusId(entity.latestStatusId()),
                    //newObj.categoryStatusId(entity.categoryStatusId()),
                    //newObj.frequency(entity.frequency()),
                    //newObj.breakingSequenceId = entity.breakingSequenceId,
                    //newObj.latestSequenceId = entity.latestSequenceId,
                    //newObj.categorySequenceId = entity.categorySequenceId,
                    //newObj.creationTime(entity.creationTime());
                    //newObj.categoryId = entity.categoryId;
                    //newObj.setDispalyId(entity.id);

                    //if (entity.news) {
                    //    newObj.news(entity.news());
                    //}
                    return newObj;
                },
                getDtoId: function (dto) {
                    return dto.TickerId;
                },
                getSortedValue: function (dto) {
                    return dto.LastUpdatedDateStr;
                },
                fromDto: function (dto, item) {

                    item = item || new model.Ticker();

                    item.id = dto.TickerId,
                    item.onAiredTime(dto.OnAiredTimeStr),
                    item.sequenceId = dto.SequenceId,
                    item.userId(dto.UserId),
                    item.tickerTypeId = dto.TickerTypeId || 0,
                    item.locationId(dto.LocationId || 0),
                    item.createdBy = dto.CreatedBy || 0,
                    item.tickerGroupName(dto.TickerGroupName || ''),
                    item.title(dto.Text || ''),
                    item.lastUpdateDate(dto.LastUpdatedDateStr),
                    item.creationDate(dto.CreationDateStr),
                    item.newsFileId(dto.NewsFileId || 0),
                    item.isActive(dto.IsActive),
                    item.creationTime(dto.CreatedDuration || 0);
                    item.categoryId = dto.CategoryId;
                    item.isShow(dto.IsShow);
                    item.setDispalyId(item.id);
                    
                    if (dto.TickerGroupName) {
                        if (!dto.TickerGroupName.trim())
                            item.emptyTickerBit(true);
                        else
                            item.emptyTickerBit(false);
                    }

                    if (dto.news) {
                        item.news(dto.news());
                    }
                    if (dto.NewsGuid) {
                        item.newsGuid(dto.NewsGuid);
                    }


                    return item;
                }
            },

            tickerLine = {
                getDtoId: function (dto) {
                    return dto.TickerLineId;
                },
                getSortedValue: function (dto) {
                    return dto.LastUpdatedDateStr;
                },
                fromDto: function (dto, item) {

                    item = item || new model.TickerLine();

                    item.id = dto.TickerLineId;
                    item.tickerId = dto.TickerId;
                    item.title(dto.Text || '');
                    item.languageCode(dto.LanguageCode);
                    item.severity(parseInt(dto.Severity) || 1);
                    item.frequency(dto.Frequency || 1);
                    item.sequenceId = dto.SequenceId || 0;
                    item.lastUpdateDate(dto.LastUpdatedDateStr || 0);
                    item.creationDate(dto.CreationDateStr || 0);
                    item.creationTime(dto.CreatedDuration || 0);
                    item.userId(dto.CreatedBy),
                    item.operatorNo = dto.OperatorNumber;
                    item.isShow(dto.IsShow);

                    return item;
                },
            },


            screenTemplate = {
                getDtoId: function (dto) {
                    return dto.ScreenTemplateId;
                },
                getSortedValue: function (dto) {
                    return dto.time;
                },
                fromDto: function (dto, item) {
                    item = item || new model.ScreenTemplate();

                    item.id = dto.ScreenTemplateId;
                    item.storyScreenTemplateId = dto.SlotScreenTemplateId;
                    item.key = dto.GroupId;
                    item.programId = dto.ProgramId,
                    item.name = dto.Name || '';
                    item.sequenceId = dto.SequenceNumber,
                    item.description = dto.Description || '';
                    item.thumbGuid(dto.ThumbGuid);
                    item.templateBackground = dto.BackgroundImageUrl;
                    item.templateHtml = dto.Html || '';
                    item.script(dto.Script || '');
                    item.videoDuration(dto.VideoDuration || 0);
                    item.isSelected(dto.IsSelected || false);
                    item.isAllowed = dto.IsAllowed || false;
                    item.isAssignToStoryWriter(dto.IsAssignedToStoryWriter || false);
                    item.isAssignToNLE(dto.IsAssignedToNle || false);
                    item.lastUpdateDate(dto.LastUpdateDateStr);

                    if (dto.SlotScreenTemplateResources) {
                        for (var i = 0; i < dto.SlotScreenTemplateResources.length; i++) {
                            var templateResource = resource.fromDto(dto.SlotScreenTemplateResources[i]);
                            if (templateResource) {
                                item.contentBucket.push(templateResource);
                            }
                        }
                    }

                    if (dto.StatusId) {
                        item.statusId(dto.StatusId);
                    }

                    if (dto.IsVideoWallTemplate && dto.IsVideoWallTemplate) {
                        item.isMediaWallTemplate(dto.IsVideoWallTemplate);
                    }

                    if (dto.ParentId) {
                        item.parentId = dto.ParentId;
                    }

                    if (dto.Keys && dto.Keys.length > 0) {
                        for (var i = 0; i < dto.Keys.length; i++) {
                            var temp = templateKey.fromDto(dto.Keys[i]);
                            if (temp) {
                                item.templateKeys.push(temp);
                            }
                        }
                    }

                    item.extractData();

                    return item;
                },
                getClone: function (entity) {
                    var newObj = new model.ScreenTemplate();

                    newObj.id = entity.id;
                    newObj.key = entity.key;
                    newObj.storyScreenTemplateId = entity.storyScreenTemplateId;
                    newObj.elements = entity.elements;
                    newObj.name = entity.name;
                    newObj.description = entity.description;
                    newObj.thumbGuid(entity.thumbGuid());
                    newObj.isSelected(entity.isSelected());
                    newObj.sequenceId = entity.sequenceId;
                    newObj.isAllowed = entity.isAllowed;
                    newObj.script(entity.script() || '');
                    newObj.templateBackground = entity.templateBackground;
                    newObj.templateDisplayImage(entity.templateDisplayImage());
                    newObj.templateDefaultImageUrl(entity.templateDisplayImage());
                    newObj.templateKeys(entity.templateKeys().slice(0));
                    newObj.isMediaWallTemplate(entity.isMediaWallTemplate());

                    if (entity.templateElements && entity.templateElements().length > 0)
                    { newObj.templateElements(entity.templateElements()); }

                    if (entity.parentId)
                    { newObj.parentId = entity.parentId; }
                    return newObj;
                }
            },

            templateElement = {
                getDtoId: function (dto) {
                    return dto.SlotTemplateScreenElementId;
                },
                getSortedValue: function (dto) {
                    return dto.time;
                },
                fromDto: function (dto, item) {
                    item = item || new model.TemplateElement();

                    item.id = dto.SlotTemplateScreenElementId,
                    item.storyTemplateScreenElementId = dto.SlotTemplateScreenElementId,

                    item.screenTemplateId = dto.ScreenTemplateId,
                    item.screenElementId = dto.ScreenElementId,
                    item.imageGuid = dto.ResourceGuid,
                    item.top = (dto.Top || 0),
                    item.left = (dto.Left || 0),
                    item.bottom = (dto.Bottom || 0),
                    item.right = (dto.Right || 0),
                    item.zindex = (dto.Zindex || 0),
                    item.guestId = dto.CelebrityId,
                    item.processData();
                    if (dto.ImageGuid) {
                        item.imageGuid = dto.ImageGuid;
                    }
                    if (dto.TemplateScreenElementId) {
                        item.id = dto.TemplateScreenElementId;
                    }
                    if (dto.SlotTemplateScreenElementResources) {
                        for (var i = 0; i < dto.SlotTemplateScreenElementResources.length; i++) {
                            var templateResource = resource.fromDto(dto.SlotTemplateScreenElementResources[i]);
                            if (templateResource) {
                                item.contentBucket.push(templateResource);
                            }
                        }
                    }
                    if (dto.SlotTemplateScreenElementId) {
                        item.storyTemplateScreenElementId = dto.SlotTemplateScreenElementId
                    }

                    return item;
                },
                toDto: function (entity) {
                    var dto = {
                        TemplateScreenElementId: entity.id,
                        SlotTemplateScreenElementId: entity.storyTemplateScreenElementId,
                        ScreenTemplateId: entity.screenTemplateId,
                        ScreenElementId: entity.screenElementId,
                        ImageGuid: entity.imageGuid,
                        Top: entity.top,
                        Left: entity.left,
                        Bottom: entity.bottom,
                        Right: entity.right,
                        Zindex: entity.zindex
                    }

                    return dto;
                },
                getClone: function (entity) {
                    var newObj = new model.TemplateElement();
                    newObj.id = entity.id,
                         newObj.storyTemplateScreenElementId = entity.storyTemplateScreenElementId,
                         newObj.screenTemplateId = entity.screenTemplateId,
                         newObj.screenElementId = entity.screenElementId,
                         newObj.imageGuid = entity.imageGuid,
                         newObj.top = (entity.top || 0),
                         newObj.left = (entity.left || 0),
                         newObj.bottom = (entity.bottom || 0),
                         newObj.right = (entity.right || 0),
                         newObj.zindex = (entity.zindex || 0),
                         newObj.guestId = entity.guestId,
                         newObj.processData();
                    if (entity.imageGuid) {
                        newObj.imageGuid = entity.imageGuid;
                    }
                    if (entity.id) {
                        newObj.id = entity.id;
                    }
                    return newObj;
                }
            },

            screenFlow = {
                getDtoId: function (dto) {
                    return dto.Id;
                },
                getSortedValue: function (dto) {
                    return dto.time;
                },
                fromDto: function (dto, item) {
                    item = item || new model.ScreenFlow();

                    item.id = dto.Id,
                    item.screenTemplates = dto.ScreenTemplates || [];

                    return item;
                }
            },

            programElement = {
                getDtoId: function (dto) {
                    return dto.SegmentId;
                },
                getSortedValue: function (dto) {
                    return dto.time;
                },
                fromDto: function (dto, item) {
                    item = item || new model.ProgramElement();

                    item.id = dto.SegmentId,
                    item.name = dto.Name || 'Segment ' + dto.SequnceNumber,
                    item.episodeId = dto.EpisodeId,
                    item.segmentId = dto.SegmentId,
                    item.programElementType = dto.SegmentTypeId,
                    item.sequenceId = dto.SequnceNumber || 0,
                    item.storyCount = dto.StoryCount || 0,
                    item.totalDuration = 10,//dto.Duration || 0,

                    item.processData();

                    return item;
                }
            },

            story = {
                getDtoId: function (dto) {
                    return dto.Id;
                },
                getSortedValue: function (dto) {
                    return dto.LastUpdateDateStr;
                },
                getClone: function (localEntity) {
                    var item = new model.Story();
                    item.id = localEntity.id,
                    item.storyId = localEntity.storyId,
                    item.tickerId = localEntity.tickerId,
                    item.episodeId = localEntity.episodeId,
                    item.sequenceId = localEntity.sequenceId || 0,
                    item.categoryId(localEntity.categoryId()),
                    item.news = localEntity.news,
                    item.userId = localEntity.userId,
                    item.preview = localEntity.preview,
                    item.lastUpdateDate(localEntity.lastUpdateDate()),
                    item.previewGuid(localEntity.previewGuid()),
                    item.isGreen(localEntity.isGreen()),
                    item.isRed(localEntity.isRed()),
                    item.creationDate(localEntity.creationDate());
                    item.setCategoryDispaly();
                    return item;
                },
                fromDto: function (dto, item) {

                    item = item || new model.Story();
                    item.id = dto.Id,
                    item.storyId = dto.StoryId,
                    item.tickerId = dto.TickerId,
                    item.episodeId = dto.EpisodeId,
                    item.programElementId = dto.ProgramElementId,
                    item.sequenceId = dto.SequenceId || 0,
                    item.categoryId(dto.CategoryId || 0),
                    item.news = dto.News,
                    item.userId = dto.UserId,
                    item.screenFlow(dto.ScreenFlow || new model.ScreenFlow()),
                    item.preview = dto.Preview,
                    item.lastUpdateDate(dto.LastUpdateDateStr),
                    item.previewGuid(dto.PreviewGuid || ''),
                    item.isGreen(dto.IncludeInRundown || false),
                    item.isRed(!dto.IncludeInRundown),
                    item.creationDate(dto.CreationDateStr);
                    item.setCategoryDispaly();
                    return item;

                },
                toDto: function (entity) {
                    var dto = {
                        Id: entity.id,
                        StoryId: entity.storyId,
                        EpisodeId: entity.episodeId,
                        ProgramElementId: entity.programElementId,
                        SequenceId: entity.sequenceId,
                        News: entity.news,
                        ScreenFlow: entity.screenFlow(),
                        Preview: entity.preview,
                        PreviewGuid : entity.previewGuid(),
                        LastUpdateDateStr: entity.lastUpdateDate(),
                        CreationDateStr: entity.creationDate(),
                        IsRed: entity.isRed(),
                        IncludeInRundown: entity.isGreen(),
                        UserId: entity.userId,
                        CategoryId: entity.categoryId(),
                        TickerId: entity.tickerId
                    };

                    return dto;
                }
            },
            category = {
                getDtoId: function (dto) {
                    return dto.CategoryId;
                },
                getSortedValue: function (dto) {

                },
                fromDto: function (dto, item) {

                    item = item || new model.Category();
                    item.id = dto.CategoryId,
                    item.name = dto.Name,
                    item.sequenceNumber = dto.SequenceNumber,
                    item.creationDate(dto.CreationDateStr),
                    item.lastUpdateDate(dto.LastUpdateDateStr);
                    return item;
                }

            },
            newsCategory = {
                getDtoId: function (dto) {
                    return dto.CategoryId;
                },
                getSortedValue: function (dto) {
                    return dto.time;
                },
                fromDto: function (dto, item) {
                    item = item || new model.NewsCategory();

                    item.id = dto.CategoryId;
                    item.name = dto.Category;

                    return item;
                }
            },

            newsTag = {
                getDtoId: function (dto) {
                    return dto._id;
                },
                getSortedValue: function (dto) {
                    return dto.time;
                },
                fromDto: function (dto, item) {
                    item = item || new model.NewsTag();
                    item.id = dto._id
                    item.name = dto.Tags
                    return item;
                }
            },

            newsLocation = {
                getDtoId: function (dto) {
                    return dto.LocationId;
                },
                getSortedValue: function (dto) {
                    return dto.time;
                },
                fromDto: function (dto, item) {
                    item = item || new model.NewsLocation();
                    item.id = dto.LocationId;
                    item.name = dto.Location;

                    return item;
                }
            },

            reporter = {
                getDtoId: function (dto) {
                    return dto.Id;
                },
                getSortedValue: function (dto) {
                    return dto.Id;
                },
                fromDto: function (dto, item) {
                    item = item || new model.reporter();
                    item.id = dto.Id,
                    item.title = dto.Title || 0,
                    item.description = dto.Description || 0,
                    item.date = dto.ReportingDate,
                    item.category = dto.Category,
                    item.location = dto.Location,
                    item.tags = dto.Tags;

                    return item;
                }
            },

            assignment = {
                getDtoId: function (dto) {
                    return dto.Id;
                },
                getSortedValue: function (dto) {
                    return dto.Id;
                },
                fromDto: function (dto, item) {
                    item = item || new model.assignment();
                    item.id = dto.Id,
                    item.title = dto.Title || 0,
                    item.description = dto.Description || 0,
                    item.date = dto.ReportingDate,
                    item.category = dto.Category,
                    item.location = dto.Location,
                    item.tags = dto.Tags;
                    item.program = dto.Program;

                    return item;
                }
            },

            filter = {
                getDtoId: function (dto) {
                    return dto.FilterId;
                },
                fromDto: function (dto, item) {
                    item = item || new model.Filter();

                    item.id = dto.FilterId,
                    item.name = dto.Name || '',
                    item.value = dto.Value,
                    item.filterTypeId = dto.FilterTypeId,
                    item.cssClass(dto.CssClass || ''),
                    item.parentId = dto.ParentId || -1,
                    item.newsCount(dto.Count || 1);
                    if (dto.LastUpdateDateStr)
                        item.lastUpdateDateStr = dto.LastUpdateDateStr;
                    if (dto.CreationDateStr)
                        item.creationDateStr = dto.CreationDateStr;

                    if (dto.FilterTypeId === 21)
                        item.isEventTypeFilter(true);

                    return item;

                }
            },

            newsFilter = {
                getDtoId: function (dto) {
                    return dto._id;
                },
                getSortedValue: function (dto) {
                    return dto.CreationDateStr;
                },
                fromDto: function (dto, item) {
                    item = item || new model.NewsFilter();

                    item.id = dto._id,
                    item.filterId = dto.FilterId,
                    item.newsId = dto.NewsId,
                    item.creationDate = dto.CreationDateStr;

                    item.fillObservableProperties();

                    return item;
                }
            },

            newsBucketItem = {
                getDtoId: function (dto) {
                    return dto.NewsBucketId;
                },
                getSortedValue: function (dto) {
                    return dto.LastUpdateDateStr;
                },
                fromDto: function (dto, item) {
                    item = item || new model.NewsBucketItem();

                    item.id = dto.NewsBucketId,
                    item.tickerId = dto.TickerId,
                    item.storyId = dto.StoryId;
                    item.sequenceId = dto.SequnceNumber || 0,
                    item.categoryId(dto.CategoryId || 0),
                    item.news = dto.News,
                    item.userId = dto.UserId,
                    item.programId = dto.ProgramId,
                    item.preview = dto.Preview,
                    item.isGreen(dto.IsGreen),
                    item.lastUpdateDate(dto.LastUpdateDateStr),
                    item.previewGuid(dto.PreviewGuid || ''),
                    item.creationDate(dto.CreationDateStr);

                    item.setCategoryDisplay();

                    return item;
                },
                toDto: function (entity) {
                    var dto = {
                        NewsBucketId: entity.id,
                        SequenceId: entity.sequenceId,
                        News: entity.news,
                        Preview: entity.preview,
                        LastUpdateDateStr: entity.lastUpdateDate,
                        CreationDateStr: entity.creationDate
                    };

                    return dto;
                }
            },

            newsComment = {
                getDtoId: function (dto) {
                    return dto._id;
                },
                getSortedValue: function (dto) {
                    return dto.LastUpdateDateStr;
                },
                fromDto: function (dto, item) {
                    item = item || new model.NewsComment();

                    item.id = dto._id;
                    item.comment = dto.Comment;
                    item.newsId = dto.NewsId;
                    item.userId = dto.UserId;
                    item.commentTypeId = dto.CommentTypeId;
                    item.lastUpdateDateStr = dto.LastUpdateDateStr;

                    return item;
                }
            },

            comment = {
                getDtoId: function (dto) {
                    return dto.MessageId;
                },
                getSortedValue: function (dto) {
                    return dto.CreationDateStr;
                },
                fromDto: function (dto, item) {
                    item = item || new model.Comment();

                    item.id = dto.MessageId;
                    item.fromId = dto.From;
                    item.toId = dto.To;
                    item.comment = dto.Message;
                    item.isReceived = dto.IsRecieved;
                    item.storyScreenTemplateId = dto.SlotScreenTemplateId;
                    item.creationDate = dto.CreationDateStr;
                    item.lastUpdateDateStr = dto.LastUpdateDateStr;

                    if (dto.ToNLE) {
                        item.toNLE(dto.ToNLE);
                    }
                    if (dto.ToStoryWriter) {
                        item.toStoryWriter(dto.ToStoryWriter);
                    }
                    if (dto.ToProducer) {
                        item.toProducer(dto.ToProducer);
                    }

                    return item;
                }
            },

            channelVideo = {
                getDtoId: function (dto) {
                    return dto.ChannelVideoId;
                },
                getSortedValue: function (dto) {
                    return dto.ChannelVideoId;
                },
                fromDto: function (dto, item) {
                    item = item || new model.ChannelVideo();

                    item.id = dto.ChannelVideoId,
                    item.channelId(dto.ChannelId),
                    item.programId(dto.ProgramId),
                    item.durationInSeconds(dto.DurationSeconds),
                    item.from = dto.FromStr,
                    item.to = dto.ToStr;
                   
                    
                    item.fromHour = moment(dto.FromStr).format('HH:00'); //.add(-5, 'hours').format("HH:mm");
                    item.toHour = moment(dto.ToStr).format('HH:00'); //.add(-5, 'hours').format("HH:mm");
                    item.hourlySlotId = item.fromHour;
                    //console.log(item.hourlySlotId);
                    item.episodeDate = moment(item.from).format(config.DateFormat);
                    item.episodedatedisplay = moment(item.from).format(config.ShortDateFormat);

                    if (dto.IsProcessed) {
                        item.isProcessed(dto.IsProcessed);
                    }

                    item.url = dto.Url;

                    return item;
                }
            },

            hourlySlot = {
                getDtoId: function (dto) {
                    return dto.id;
                },
                getSortedValue: function (dto) {
                    return dto.id;
                },
                fromDto: function (dto, item) {
                    item = item || new model.HourlySlot();

                    item.id = dto.id,
                    item.name = dto.id;

                    item.fillObservableProperties();

                    return item;
                }
            },

            multiselect = {
                getDtoId: function (dto) {
                    return dto.IsActive;
                },
                getSortedValue: function (dto) {
                    return dto.IsActive;
                },
                fromDto: function (dto, item) {
                    item = item || new model.MultiSelectItem();

                    if (dto.CategoryId) {
                        item.id = dto.CategoryId;
                        item.name = dto.Category;
                    }
                    else if (dto.LocationId) {
                        item.id = dto.LocationId;
                        item.name = dto.Location;
                    }
                    else if (dto.TagId) {
                        item.id = dto.TagId;
                        item.name = dto.Tag;
                    }

                    return item;
                }
            },

            screenTemplateCameras = {
                getDtoId: function (dto) {
                    return dto.SlotScreenTemplateCameraId;
                },
                getSortedValue: function (dto) {
                    return dto.SlotScreenTemplateCameraId;
                },
                fromDto: function (dto, item) {
                    item = item || new model.ScreenTemplateCameras();

                    item.id = dto.SlotScreenTemplateCameraId,
                    item.storyScreenTemplateId = dto.SlotScreenTemplateId,
                    item.cameraTypeId = dto.CameraTypeId,
                    item.name = dto.Name,
                    item.isOn = dto.IsOn,
                    item.creationDateStr = dto.CreationDateStr,
                    item.lastUpdateDateStr = dto.LastUpdateDateStr;

                    return item;
                }
            },

            screenTemplateMics = {
                getDtoId: function (dto) {
                    return dto.SlotScreenTemplateMicId;
                },
                getSortedValue: function (dto) {
                    return dto.SlotScreenTemplateMicId;
                },
                fromDto: function (dto, item) {
                    item = item || new model.ScreenTemplateMics();

                    item.id = dto.SlotScreenTemplateMicId,
                    item.storyScreenTemplateId = dto.SlotScreenTemplateId,
                    item.micNo = dto.MicNo,
                    item.isOn = dto.IsOn,
                    item.creationDateStr = dto.CreationDateStr,
                    item.lastUpdateDateStr = dto.LastUpdateDateStr;

                    return item;
                }
            },

            UserRole = {
                getDtoId: function (dto) {
                    return dto.UniqueId;
                },
                fromDto: function (dto, item) {
                    var userrole = dto;

                    item = item || new model.UserRole();
                    item.moduleId = userrole.ModuleId,
                    item.workRoleId = userrole.WorkRoleId,
                    item.moduleUrl = userrole.ModuleUrl;
                    item.userId = userrole.UserId;
                    item.sessionkey = userrole.SessionKey;
                    item.uniqueId = userrole.UniqueId;
                    item.moduleAbbriviation = userrole.ModuleAbbriviation;
                    item.workRoleName = userrole.WorkRoleName;
                    item.urlName = userrole.UrlName;
                    item.fullName = userrole.FullName;

                    return item;
                }
            },

           runorder = {
               getDtoId: function (dto) {
                   return dto.RoId;
               },
               getSortedValue: function (dto) {
                   return dto.RoId;
               },
               fromDto: function (dto, item) {
                   item = item || new model.RunOrder();

                   item.id = dto.RoId,
                   item.slug(dto.Slug),
                   item.roStartTime = dto.StartTime

                   item.extractData();

                   return item;
               }
           },

           rostory = {
               getDtoId: function (dto) {
                   return dto.StoryId;
               },
               getSortedValue: function (dto) {
                   return dto.StoryId;
               },
               fromDto: function (dto, item) {
                   item = item || new model.ROStory();

                   item.id = dto.StoryId,
                   item.roId = dto.RoId,
                   item.orderId = dto.SequenceId,
                   item.isSkipped(dto.IsSkipped),
                   item.isPlayed(dto.IsPlayed),
                   item.slug(dto.Slug)

                   item.extractData();

                   return item;
               }
           },

            storyitem = {
                getDtoId: function (dto) {
                    return dto.StoryItemId;
                },
                getSortedValue: function (dto) {
                    return dto.StoryItemId;
                },
                fromDto: function (dto, item) {
                    item = item || new model.StoryItem();

                    item.id = dto.StoryItemId,
                    item.storyId = dto.StoryId,
                    item.orderId = dto.SequenceId,
                    item.slug = dto.Slug,
                    item.instruction = dto.Instructions,
                    item.details = dto.Detail

                    return item;
                }
            },

          casperrunorder = {
              getDtoId: function (dto) {
                  return dto.roID;
              },
              getSortedValue: function (dto) {
                  return dto.roID;
              },
              fromDto: function (dto, item) {
                  item = item || new model.CasperRunOrder();

                  item.id = dto.roID,
                  item.slug = dto.roSlug,
                  item.roChannel = dto.roChannel

                  item.extractData();

                  return item;
              }
          },

          casperitem = {
              getDtoId: function (dto) {
                  return dto.Id;
              },
              getSortedValue: function (dto) {
                  return dto.Id;
              },
              fromDto: function (dto, item) {
                  item = item || new model.CasperItem();

                  item.id = dto.Id,
                  item.roId = dto.roID,
                  item.type = dto.Type,
                  item.name = dto.Name,
                  item.videoLayer = dto.Videolayer

                  return item;
              }
          },
          profile = {
              getDtoId: function (dto) {
                  return dto.ProfileId;
              },
              getSortedValue: function (dto) {
                  return dto.ProfileId;
              },
              fromDto: function (dto, item) {
                  item = item || new model.Profile();

                  item.id = dto.ProfileId,
                  item.name = dto.Name,
                  item.fontfamilyId = dto.FontfamilyId,
                  item.fontSize = dto.FontSize,
                  item.scrollSpeed = dto.ScrollSpeed

                  return item;
              }
          },

        fontfamily = {
            getDtoId: function (dto) {
                return dto.FontfamilyId;
            },
            getSortedValue: function (dto) {
                return dto.FontfamilyId;
            },
            fromDto: function (dto, item) {
                item = item || new model.Fontfamily();

                item.id = dto.FontfamilyId,
                item.name = dto.Name

                return item;
            }
        },
        configsetting = {
            getDtoId: function (dto) {
                return dto.ConfigSettingId;
            },
            getSortedValue: function (dto) {
                return dto.ConfigSettingId;
            },
            fromDto: function (dto, item) {
                item = item || new model.ConfigSetting();

                item.id = dto.ConfigSettingId,
                item.key(dto.SettingKey),
                item.value(dto.SettingValue)

                return item;
            }
        };

        casparaction = {
            getDtoId: function (dto) {
                return dto.CasparActionId;
            },
            getSortedValue: function (dto) {
                return dto.CasparActionId;
            },
            fromDto: function (dto, item) {
                item = item || new model.CasparAction();

                item.id = dto.CasparActionId,
                item.name = dto.Name,
                item.shortKey = dto.ShortKey,
                item.command = dto.Command

                return item;
            }
        },

        casparimage = {
            getDtoId: function (dto) {
                return dto.CasparImageId;
            },
            getSortedValue: function (dto) {
                return dto.CasparImageId;
            },
            fromDto: function (dto, item) {
                item = item || new model.CasparImage();

                item.id = dto.CasparImageId,
                item.casparRundownItemId = dto.CasparRundownItemId,
                item.transition = dto.Transition,
                item.duration = dto.Duration,
                item.direction = dto.Direction,
                item.userAuto = dto.UserAuto,
                item.triggerOnNext = dto.TriggerOnNext

                return item;
            }
        },

        casparitem = {
            getDtoId: function (dto) {
                return dto.CasparItemId;
            },
            getSortedValue: function (dto) {
                return dto.CasparItemId;
            },
            fromDto: function (dto, item) {
                item = item || new model.CasparItem();

                item.id = dto.CasparItemId,
                item.name = dto.Name,
                item.canTarget = dto.CanTarget,
                item.casparItemCategoryId = dto.CasparItemCategoryId,
                item.iconPath = dto.IconPath

                return item;
            }
        },

        casparcategory = {
            getDtoId: function (dto) {
                return dto.CasparCategoryId;
            },
            getSortedValue: function (dto) {
                return dto.CasparCategoryId;
            },
            fromDto: function (dto, item) {
                item = item || new model.CasparCategory();

                item.id = dto.CasparCategoryId,
                item.name = dto.Name

                return item;
            }
        },

     casparitempermission = {
         getDtoId: function (dto) {
             return dto.CasparItemPermissionId;
         },
         getSortedValue: function (dto) {
             return dto.CasparItemPermissionId;
         },
         fromDto: function (dto, item) {
             item = item || new model.CasparItemPermission();

             item.id = dto.CasparItemPermissionId,
             item.userId = dto.UserId,
             item.casparItemId = dto.CasparItemId,
             item.roleId = dto.RoleId

             return item;
         }
     },

        casparrundown = {
            getDtoId: function (dto) {
                return dto.CasparRundownId;
            },
            getSortedValue: function (dto) {
                return dto.CasparRundownId;
            },
            fromDto: function (dto, item) {
                item = item || new model.CasparRundown();

                item.id = dto.CasparRundownId,
                item.name = dto.Name

                return item;
            }
        },

       casparrundownitem = {
           getDtoId: function (dto) {
               return dto.CasparRundownItemId;
           },
           getSortedValue: function (dto) {
               return dto.CasparRundownItemId;
           },
           fromDto: function (dto, item) {
               item = item || new model.CasparRundownItem();

               item.id = dto.CasparRundownItemId,
               item.casparItemId = dto.CasparItemId,
               item.casparRundownId = dto.CasparRundownId,
               item.flashTemplateWindowId = dto.FlashTemplateWindowId,
               item.name = dto.Name,
               item.thumbnail = dto.Thumbnail,
               item.server = dto.Server,
               item.target = dto.Target,
               item.channel = dto.Channel,
               item.videoLayer = dto.VideoLayer,
               item.delay = dto.Delay,
               item.duration = dto.Duration,
               item.allowGpi = dto.AllowGpi,
               item.uid = dto.Uid

               return item;
           }
       },

       casparconfigsetting = {
           getDtoId: function (dto) {
               return dto.ConfigSettingId;
           },
           getSortedValue: function (dto) {
               return dto.ConfigSettingId;
           },
           fromDto: function (dto, item) {
               item = item || new model.CasparConfigSetting();

               item.id = dto.ConfigSettingId,
               item.configKey = dto.ConfigKey,
               item.configValue = dto.ConfigValue,
               item.creationDate = dto.CreationDate,
               item.lastUpdateDate = dto.LastUpdateDate

               return item;
           }
       },

        crop = {
            getDtoId: function (dto) {
                return dto.CropId;
            },
            getSortedValue: function (dto) {
                return dto.CropId;
            },
            fromDto: function (dto, item) {
                item = item || new model.Crop();

                item.id = dto.CropId,
                item.casparRundownItemId = dto.CasparRundownItemId,
                item.left = dto.Left,
                item.width = dto.Width,
                item.top = dto.Top,
                item.height = dto.Height,
                item.duration = dto.Duration,
                item.tween = dto.Tween,
                item.defer = dto.Defer

                return item;
            }
        },
        decklinkinput = {
            getDtoId: function (dto) {
                return dto.DecklinkInputId;
            },
            getSortedValue: function (dto) {
                return dto.DecklinkInputId;
            },
            fromDto: function (dto, item) {
                item = item || new model.DecklinkInput();

                item.id = dto.DecklinkInputId,
                item.casparRundownItemId = dto.CasparRundownItemId,
                item.device = dto.Device,
                item.format = dto.Format,
                item.transition = dto.Transition,
                item.duration = dto.Duration,
                item.tween = dto.Tween,
                item.direction = dto.Direction

                return item;
            }
        },

        flashtemplatekey = {
            getDtoId: function (dto) {
                return dto.FlashTemplateKeyId;
            },
            getSortedValue: function (dto) {
                return dto.FlashTemplateKeyId;
            },
            fromDto: function (dto, item) {
                item = item || new model.FlashTemplateKey();

                item.id = dto.FlashTemplateKeyId,
                 item.flashTemplateId = dto.FlashTemplateId,
                 item.name = dto.Name,
                 item.topLeftx = dto.TopLeftx,
                 item.bottomRighty = dto.BottomRighty,
                 item.width = dto.Width,
                 item.height = dto.Height,
                 item.creationDate = dto.CreationDate,
                 item.lastUpdateDate = dto.LastUpdateDate

                return item;
            }
        },

        flashtemplatewindow = {
            getDtoId: function (dto) {
                return dto.FlashTemplateWindowId;
            },
            getSortedValue: function (dto) {
                return dto.FlashTemplateWindowId;
            },
            fromDto: function (dto, item) {
                item = item || new model.FlashTemplateWindow();

                item.id = dto.FlashTemplateWindowId,
                item.flashKey = dto.FlashTemplateKeyId,
                item.flashTemplateId = dto.FlashTemplateId,
                item.creationDate = dto.CreationDate,
                item.lastUpdateDate = dto.LastUpdateDate

                return item;
            }
        },

        template = {
            getDtoId: function (dto) {
                return dto.TemplateId;
            },
            getSortedValue: function (dto) {
                return dto.TemplateId;
            },
            fromDto: function (dto, item) {
                item = item || new model.Template();

                item.id = dto.TemplateId,
                item.flashTemplateId = dto.FlashTemplateId,
                item.casparRundownItemId = dto.CasparRundownItemId,
                item.flashLayer = dto.FlashLayer,
                item.invoke = dto.Invoke,
                item.useStoredData = dto.UseStoredData,
                item.useUpperCaseData = dto.UseUpperCaseData

                return item;
            }
        },

        templatedata = {
            getDtoId: function (dto) {
                return dto.TemplateDataId;
            },
            getSortedValue: function (dto) {
                return dto.TemplateDataId;
            },
            fromDto: function (dto, item) {
                item = item || new model.TemplateData();

                item.id = dto.TemplateDataId,
                item.casparRundownItemId = dto.CasparRundownItemId,
                item.templateDataKey = dto.TemplateDataKey,
                item.templateDataValue = dto.TemplateDataValue

                return item;
            }
        },

        templateitem = {
            getDtoId: function (dto) {
                return dto.TemplateItemId;
            },
            getSortedValue: function (dto) {
                return dto.TemplateItemId;
            },
            fromDto: function (dto, item) {
                item = item || new model.TemplateItem();

                item.id = dto.TemplateItemId,
                item.casparRundownItemId = dto.CasparRundownItemId,
                item.templateId = dto.TemplateId

                return item;
            }
        },

        transformation = {
            getDtoId: function (dto) {
                return dto.TransformationId;
            },
            getSortedValue: function (dto) {
                return dto.TransformationId;
            },
            fromDto: function (dto, item) {
                item = item || new model.Transformation();

                item.id = dto.TransformationId,
                item.casparRundownItemId = dto.CasparRundownItemId,
                item.positionx = dto.Positionx,
                item.positiony = dto.Positiony,
                item.scalex = dto.Scalex,
                item.scaley = dto.Scaley,
                item.duration = dto.Duration,
                item.tween = dto.Tween,
                item.triggerOnNext = dto.TriggerOnNext,
                item.defer = dto.Defer

                return item;
            }
        },

        usercasparaction = {
            getDtoId: function (dto) {
                return dto.UserCasparActionId;
            },
            getSortedValue: function (dto) {
                return dto.UserCasparActionId;
            },
            fromDto: function (dto, item) {
                item = item || new model.UserCasparAction();

                item.id = dto.UserCasparActionId,
                item.userId = dto.UserId,
                item.roleId = dto.RoleId,
                item.casparItemId = dto.CasparItemId

                return item;
            }
        },

        usertemplatetype = {
            getDtoId: function (dto) {
                return dto.UserTemplateTypeId;
            },
            getSortedValue: function (dto) {
                return dto.UserTemplateTypeId;
            },
            fromDto: function (dto, item) {
                item = item || new model.UserTemplateType();

                item.id = dto.UserTemplateTypeId,
                item.userId = dto.UserId,
                item.roleId = dto.RoleId,
                item.templateTypeId = dto.TemplateTypeId,
                item.flashTemplateId = dto.FlashTemplateId

                return item;
            }
        },
         tickerimage = {
             getDtoId: function (dto) {
                 return dto.TickerImageId;
             },
             getSortedValue: function (dto) {
                 return dto.TickerImageId;
             },
             fromDto: function (dto, item) {
                 item = item || new model.TickerImage();
                 item.id = dto.TickerImageId,
                 item.channelId = dto.ChannelId,
                 item.imagePath('http:'+dto.ImagePath);
                 if (dto.ChannelTickerLocationId) {
                     item.channelTickerLocationId = dto.ChannelTickerLocationId;
                 }
                 if (dto.LastUpdateDate) {
                     item.lastUpdateDate = dto.LastUpdateDate;
                 }
                 if (dto.tickerImageStatusId) {
                     item.tickerImageStatusId = dto.TickerImageStatusId;
                 }
                 return item;
             }
         },

        newsFile = {
            getDtoId: function (dto) {
                return dto.NewsFileId;
            },
            getSortedValue: function (dto) {
                return dto.CreationDateStr;
            },
            fromDto: function (dto, item) {
                item = item || new model.NewsFile();
                item.id = dto.NewsFileId,
                item.slug(dto.Slug),
                item.folderId = dto.FolderId,
                item.title = dto.Title,
                item.newsPaperDescription = dto.NewsPaperDescription,
                item.languageCode = dto.LanguageCode,
                item.publishTime = dto.PublishTime,
                item.reportedBy(dto.CreatedBy),
                item.parentId = dto.ParentId,
                item.creationDate(dto.CreationDateStr),
                item.statusId(dto.StatusId),
                item.lastUpdateDate(dto.LastUpdateDateStr),
                item.fillObservableProperties(),
                item.newsStatus(dto.NewsStatus || 0),
                item.sequenceNo(dto.SequenceNo),
                item.isDeleted(dto.IsDeleted || false),
                item.broadcastedCount(dto.BroadcastedCount || 0),
                item.sourceTypeId(dto.SourceTypeId || 0),
                item.resourceGuid(dto.ResourceGuid),
                item.isTagged(dto.IsTagged || false),
                item.commentCount(dto.EcCount || 0),
                item.descriptionText(dto.Text || ''),

                item.bureauLocation(dto.BureauLocation || '');

                if (dto.ParentNewsId)
                    item.parentId = dto.ParentNewsId;

                if (dto.ChildNewsStatus)
                    item.isDeleted(dto.ChildNewsStatus);

                if (dto.Category)
                    item.category(dto.Category);
                if (dto.Source)
                    item.source(dto.Source);
                if (dto._id)
                    item.id = dto._id;
                if (dto.ReporterId)
                    item.reportedBy(dto.ReporterId);

                if (dto.Text && dto.Text.length > 0)
                {
                    item.voiceOver(true);
                }
                if (dto.ShortDescription && dto.ShortDescription.length > 0)
                {
                    item.voiceOver(true);
                }
                  
                if (dto.Highlights && dto.Highlights.length > 0)
                {
                    item.heighlighs(dto.Highlights);
                    item.isTitle(true);
                }
                if (dto.Highlight && dto.Highlight.length > 0)
                {
                    item.heighlighs(dto.Highlight);
                    item.isTitle(true);
                }
                    
                if (dto.ProgramTime) {
                    item.programTime(moment(dto.ProgramTime).format('ddd  DD-MMM-YYYY , hh:mm A')); 
                }
                    
                if (dto.ProgramName)
                    item.programName(dto.ProgramName);
                if (dto.Categories && dto.Categories.length > 0)
                    item.category(dto.Categories[0].Category);
                if (dto.Locations && dto.Locations.length > 0)
                    item.source(dto.Locations[0].Location)
                if (dto.lstOrganization && dto.lstOrganization.length > 0)
                    item.newsOrg(dto.lstOrganization)
                if (dto.lstTag && dto.lstTag.length > 0)
                    item.newsTag(dto.lstTag);


                if (parseInt(moment().diff(item.creationDate(), 'days')) == 0) {
                        if (parseInt(moment().diff(item.creationDate(), 'minutes')) < 15) {

                            item.interval(parseInt(15 * 60 * 1000) - parseInt(moment().diff(item.creationDate(), 'milliseconds')));

                            item.getNewsFileDuration();
                            item.newsDurationCss('breakingCss');
                    }
                }

                if (dto.Resources && dto.Resources)
                {
                    for (var i = 0; i < dto.Resources.length; i++) {
                        var entity = resource.fromDto(dto.Resources[i]);
                        item.resources.push(entity);
                    }
                }

                item.isTitle(dto.IsTitle);
                item.voiceOver(dto.VoiceOver);

                if (dto.Description)
                {
                    item.descriptionText(dto.Description);
                }


                if (dto.IsVerified === true || dto.IsVerified === false)
                    item.isVerified(dto.IsVerified);
                else
                    item.isVerified('initial');


                if (item.isVerified() != 'initial' && item.newsStatus() != 0 && item.sourceTypeId !=7)
                    item.isStatusFinal(true);
                else
                    item.isStatusFinal(false);

                if (dto.StatusId)
                    item.statusId(dto.StatusId);
                else
                    item.statusId(0);


                return item;
            }
        },
        newsFolder = {
            getDtoId: function (dto) {
                return dto.FolderId;
            },
            getSortedValue: function (dto) {
                return dto.CreationDateStr;
            },
            fromDto: function (dto, item) {
                item = item || new model.NewsFolder();
                item.id = dto.FolderId,
                item.name = dto.Name,
                item.categoryId = dto.CategoryId,
                item.locationId(dto.LocationId),

                item.creationDate(dto.CreationDateStr),
                item.parentFolderId = dto.ParentFolderId,
                item.isRundown(dto.IsRundown),
                item.startTime(dto.StartTimeStr),
                item.endTime(dto.EndTimeStr),
                item.programId = dto.ProgramId;
                item.episodeId = dto.EpisodeId;
                item.location(dto.Location);
                item.category(dto.Category);
                return item;
            }
        },
        newsFileDetail = {
            getDtoId: function (dto) {
                return dto.FileDetailId;
            },
            getSortedValue: function (dto) {
                return dto.CreationDateStr;
            },
            fromDto: function (dto, item) {
              item = item || new model.NewsFileDetail();
              item.id = dto.FileDetailId,
              item.newsFileId = dto.NewsFileId,
              item.text = dto.Text,
              item.slug(dto.Slug),
              item.createdBy(dto.CreatedBy),
              item.creationDate(dto.CreationDateStr),
              item.lastUpdateDate(dto.LastUpdateDateStr),
              item.reportedBy(dto.ReportedBy);
              return item;
            }
        },
        newsFileFolderHistory = {
            getDtoId: function (dto) {
                return dto.FileFolderHistoryId;
            },
            getSortedValue: function (dto) {
                return dto.CreationDateStr;
            },
            fromDto: function (dto, item) {
                item = item || new model.NewsFileFolderHistory();

                item.id = dto.FileFolderHistoryId,
                item.newsFileId = dto.NewsFileId,
                item.userId = dto.UserId,
                item.folderId(dto.FolderId),
                item.statusId(dto.StatusId),
                item.creationDate(dto.CreationDateStr);

                return item;
            }
        },
        newsFileResource = {
            getDtoId: function (dto) {
                return dto.FileResourceId;
            },
            getSortedValue: function (dto) {
                return dto.CreationDateStr;
            },
            fromDto: function (dto, item) {

                item = item || new model.NewsFileResource();
                
                item.id =  dto.FileResourceId,
                item.newsFileId = dto.NewsFileId,
                item.resourceTypeId = dto.ResourceTypeId,
                item.guid =  dto.Guid,
                item.caption =  dto.Caption,
                item.duration(dto.Duration),
                item.creationDate(dto.CreationDateStr),
                item.lastUpdateDate(dto.LastUpdateDateStr),
                item.userId(dto.UserId);

                return item;
            }
        },
        newsFileStatusHistory = {
            getDtoId: function (dto) {
                return dto.FileStatusHistoryId;
            },
            getSortedValue: function (dto) {
                return dto.CreationDateStr;
            },
            fromDto: function (dto, item) {

                item = item || new model.NewsFileStatusHistory();

                item.id = dto.FileStatusHistoryId,
                item.newsFileId = dto.NewsFileId,
                item.statusId = dto.StatusId,
                item.userId = dto.UserId,
                item.creationDate(dto.CreationDateStr);
                
                return item;
            }

        },
        workRoleFolder = {

            getDtoId: function (dto) {
                return dto.WorkRoleFolderId;
            },
            getSortedValue: function (dto) {
                return dto.CreationDateStr;
            },
            fromDto: function (dto, item) {
                item = item || new model.WorkRoleFolder();

                item.maxColor(dto.MaxColor),
                item.folderId = dto.FolderId,
                item.allowRead = dto.AllowRead,
                item.allowMove = dto.AllowMove,
                item.allowCopy = dto.AllowCopy,
                item.allowCreate = dto.AllowCreate,
                item.allowDelete = dto.AllowDelete,
                item.allowModify = dto.AllowModify,
                item.lastUpdateDate(dto.lastUpdateDateStr),
                item.creationDate(dto.CreationDateStr);

                return item;
            }

        },
        casparvideo = {
            getDtoId: function (dto) {
                return dto.VideoId;
            },
            getSortedValue: function (dto) {
                return dto.VideoId;
            },
            fromDto: function (dto, item) {
                item = item || new model.CasparVideo();

                item.id = dto.VideoId,
                item.casparRundownItemId = dto.CasparRundownItemId,
                item.transition = dto.Transition,
                item.duration = dto.Duration,
                item.tween = dto.Tween
                item.direction = dto.Direction,
                item.seek = dto.Seek,
                item.length = dto.Length,
                item.loop = dto.Loop,
                item.freezeOnLoad = dto.FreezeOnLoad,
                item.triggerOnNext = dto.TriggerOnNext

                return item;
            }
        };

        return {
            user: user,
            guest: guest,
            bunch: bunch,
            news: news,
            resource: resource,
            program: program,
            episode: episode,
            channel: channel,
            programElement: programElement,
            story: story,
            screenElement: screenElement,
            screenTemplate: screenTemplate,
            templateElement: templateElement,
            screenFlow: screenFlow,
            newsCategory: newsCategory,
            reporter: reporter,
            newsBucketItem: newsBucketItem,
            assignment: assignment,
            newsTag: newsTag,
            filter: filter,
            newsFilter: newsFilter,
            newsLocation: newsLocation,
            newsComment: newsComment,
            comment: comment,
            channelVideo: channelVideo,
            hourlySlot: hourlySlot,
            newsPaper: newsPaper,
            dailyNewsPaper: dailyNewsPaper,
            newsPaperPage: newsPaperPage,
            radioStation: radioStation,
            radioStream: radioStream,
            multiselect: multiselect,
            screenTemplateCameras: screenTemplateCameras,
            screenTemplateMics: screenTemplateMics,
            UserRole: UserRole,
            runorder: runorder,
            rostory: rostory,
            storyitem: storyitem,
            casperitem: casperitem,
            casperrunorder: casperrunorder,
            profile: profile,
            fontfamily: fontfamily,
            configsetting: configsetting,
            casparaction: casparaction,
            casparimage: casparimage,
            casparitem: casparitem,
            casparcategory: casparcategory,
            casparitempermission: casparitempermission,
            casparrundown: casparrundown,
            casparrundownitem: casparrundownitem,
            casparconfigsetting: casparconfigsetting,
            crop: crop,
            decklinkinput: decklinkinput,
            flashtemplatekey: flashtemplatekey,
            flashtemplatewindow: flashtemplatewindow,
            template: template,
            templatedata: templatedata,
            templateitem: templateitem,
            transformation: transformation,
            usercasparaction: usercasparaction,
            usertemplatetype: usertemplatetype,
            casparvideo: casparvideo,
            event: event,
            newsAlert: newsAlert,
            notification: notification,
            templateKey: templateKey,
            ticker: ticker,
            tickerLine: tickerLine,
            category: category,
            userFilter: userFilter,
            tickerimage: tickerimage,
            newsFile: newsFile,
            newsFolder: newsFolder,
            newsFileDetail: newsFileDetail,
            newsFileFolderHistory:newsFileFolderHistory,
            newsFileResource: newsFileResource,
            newsFileStatusHistory: newsFileStatusHistory,
            workRoleFolder: workRoleFolder

        };
    });