﻿define('model.multiselectitem',
    ['ko'],
    function (ko) {

    	var
            _dc = this,

            MultiSelectItem = function () {

            	var self = this;

            	self.id,
                self.name,

                self.isNullo = false;

            	return self;
            }

    	MultiSelectItem.Nullo = new MultiSelectItem();

    	MultiSelectItem.Nullo.isNullo = true;

    	MultiSelectItem.datacontext = function (dc) {
    		if (dc) { _dc = dc; }
    		return _dc;
    	};

    	return MultiSelectItem;
    });