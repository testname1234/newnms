﻿define('model.news',
    ['ko', 'jquery', 'enum', 'moment', 'extensions', 'underscore', 'config', 'appdata'],
    function (ko, $, e, moment, ext, _, config, appdata) {

        var
            _dc = this,

            News = function () {

                var self = this;

                self.id,
                self.newsId = ko.computed({
                    read: function () {
                        if (self.id) {
                            return self.id.substring(self.id.length - 5, self.id.length);
                        } else
                            return '';
                    },
                    deferEvaluation: true
                }),
                self.programId = ko.observable(0);
                self.assignedTo = ko.observable(0);
                self.resourceImageCount = ko.observable(0),
                self.resourceAudioCount = ko.observable(0),
                self.resourceVideoCount = ko.observable(0),
                self.bureauId = ko.observable(),
                self.isNewsEdit=ko.observable(false),
                self.resourceDocumentCount = ko.observable(0),
                self.onAirCount = ko.observable(0),
                self.otherChannelExecutionCount = ko.observable(0),
                self.addedToRundownCount = ko.observable(),
                self.tickers = ko.observableArray(),
                self.suggestions = ko.observable(''),
                self.newsPaperdescription = ko.observable(''),
                self.newsStatistics = ko.observableArray([]),
                self.bunchId,
                self.searchTerm,
                self.newsTickerCount = ko.observable(),
                self.score,
                self.title = ko.observable('').extend({
                    required: true,
                    minLength: 10,
                }),
                self.translatedTitle = ko.observable('').extend({
                    required: true,
                    minLength: 10,
                }),
                self.translatedDescription = ko.observable('').extend({
                    required: true,
                    minLength: 10,
                }),
                self.description = ko.observable(''),
                self.shortDescription = ko.observable(''),
                self.filterIds = function () {
                    var filters = self.newsFilters();
                    var arr = _.pluck(_.pluck(_.filter(filters, function (item) {
                        return item.filter != null;
                    }), 'filter'), 'id');
                    var list = [];
                    list = arr.concat.apply(arr, list);
                    return list;
                },
                self.allCategories = function () {
                    var filters = self.newsFilters();
                    if (filters.length > 0) {
                        for (var i = 0; i < filters.length; i++) {
                            if (filters[i].filter && filters[i].filter.filterTypeId == e.NewsFilterType.Category) {
                                return filters[i].filter;
                                break;
                            }
                        }
                    }
                    return '';
                },
                self.parentCategories = function () {
                    var filters = self.newsFilters();
                    var arr = [];
                    if (filters.length > 0) {
                        for (var i = 0; i < filters.length; i++) {
                            if (filters[i].filter && filters[i].filter.filterTypeId == e.NewsFilterType.Category && filters[i].filter.parentId <= 0) {
                                if (arr.indexOf(filters[i].filter.name) == -1) {
                                    arr.push(filters[i].filter.name);
                                }
                            }
                        }
                    }
                    return arr;
                },
                self.locationObjects = function () {
                    var filters = self.newsFilters();
                    if (filters.length > 0) {
                        var arr = _.pluck(_.filter(filters, function (item) {
                            return item.filter != null;
                        }), 'filter');

                        var list = [];
                        list = arr.concat.apply(arr, list);

                        var filteredLocations = _.filter(list, function (item) {
                            return (item.filterTypeId == e.NewsFilterType.FieldReporter) && item.parentId > 0;
                        });

                        return filteredLocations;
                    }
                    return [];
                }
                self.locations = function () {
                    var filters = self.newsFilters();
                    if (filters.length > 0) {
                        var arr = _.pluck(_.filter(filters, function (item) {
                            return item.filter != null;
                        }), 'filter');

                        var list = [];
                        list = arr.concat.apply(arr, list);

                        var filteredLocations = _.filter(list, function (item) {
                            return (item.filterTypeId == e.NewsFilterType.FieldReporter) && item.parentId > 0;
                        });

                        return _.pluck(filteredLocations, 'name');
                    }
                    return [];
                },
                self.source = ko.observable(''),
                self.creationDate,
                self.lastUpdateDateStr,
                self.reporterId,
                self.reporterIdDisplay = ko.observable(''),
                self.reportedLocations = ko.observableArray([]),

                self.now = ko.observable(new Date()),

                self.publishTime,

                self.publishDisplayTime = ko.computed({
                    read: function () {
                        var currentTime = appdata.currentTime();
                        return moment(self.creationDate).fromNow();
                        //return moment(self.publishTime).fromNow();
                    },
                    deferEvaluation: true
                }),

                self.orignalDisplayTime = ko.computed({
                    read: function () {
                        var currentTime = appdata.currentTime();
                        return moment(self.publishTime).format('MMMM Do YYYY, h:mm:ss a');
                    },
                    deferEvaluation: true
                }),

                self.lastUpdateDisplayTime = ko.computed({
                    read: function () {
                        return moment(self.lastUpdateDateStr).fromNow();
                    },
                    deferEvaluation: true
                }),

                self.lastUpdateDatePart = ko.computed({
                    read: function () {
                        return moment(self.lastUpdateDateStr).format('MMMM D, YYYY');
                    },
                    deferEvaluation: true
                }),

                self.publishDatePart = ko.computed({
                    read: function () {
                        return moment(self.publishTime).format('MMMM D, YYYY');
                    },
                    deferEvaluation: true
                }),

                self.lastUpdateTimePart = ko.computed({
                    read: function () {
                        return moment(self.lastUpdateDateStr).format('HH : [00]');
                    },
                    deferEvaluation: true
                }),

                self.publishTimePart = ko.computed({
                    read: function () {
                        return moment(self.publishTime).format('HH : [00]');
                    },
                    deferEvaluation: true
                }),

                self.filtersInfo = ko.computed({
                    read: function () {
                        var pCategories = self.parentCategories();
                        var source = self.source();
                        var info = new Array();
                        var filterinfo = '';
                        if (pCategories
                            && pCategories.length > 0) {
                            info = pCategories.clone();
                        }
                        if (source) {
                            info.push(source);
                        }
                        if (self.publishDisplayTime) {
                            info.push(self.publishDisplayTime());
                        }

                        return info;
                    },
                    deferEvaluation: true
                }),
                self.version,
                self.parentNewsId,
                self.updatesCountMsg = ko.computed({
                    read: function () {
                        if (self.updates) {
                            if (self.updates().length === 1) {
                                return self.updates().length + ' Update';
                            }
                            else if (self.updates().length > 1) {
                                return self.updates().length + ' Updates';
                            }
                        }
                        else {
                            return '';
                        }
                    },
                    deferEvaluation: true
                }),
                self.relatedNews = ko.observableArray([]),
                self.toprelatedNews = ko.computed({
                    read: function () {
                        return self.relatedNews.splice(0, 3);
                    },
                    deferEvaluation: true
                }),
                self.relatedNewsCount = ko.computed({
                    read: function () {
                        return self.relatedNews().length;
                    },
                    deferEvaluation: true
                }),
                self.languageCode,
                self.isVerified = ko.observable(false),
                self.isAired = ko.observable(),
                self.isRecommended = ko.observable(),
                self.isTopNews = ko.observable(),
                self.newsTypeId,
                self.sourceTypeId,
                self.EventResource,
                self.Coverage,
                self.EventTypeName,
                self.EventType,
                self.EventReporter,
                self.EventReporterFilled = ko.observable(false),
                self.updateEventList = ko.computed({
                    read: function () {
                        self.EventReporterFilled();
                        if (self.EventReporter && self.EventReporter.length > 0) {
                            for (var i = 0; i < self.EventReporter.length; i++) {
                                self.EventReporter[i].Name = _dc.users.getLocalById(self.EventReporter[i].ReporterId).name
                            }
                        }
                        console.log
                        return false;
                    },
                    deferEvaluation: true
                }),
                self.newsTypeImage = ko.computed({
                    read: function () {
                        switch (self.sourceTypeId) {
                            case e.NewsFilterType.Website:
                                break;
                            case e.NewsFilterType.Radio:
                                break;
                            case e.NewsFilterType.SocialMedia:
                                break;
                            case e.NewsFilterType.NewsPaper:
                                break;
                            case e.NewsFilterType.PublicReporter:
                                return 'icon-meet';
                                break;
                            case e.NewsFilterType.FieldReporter:
                                return 'icon-express';
                                break;
                            case e.NewsFilterType.Package:
                                break;
                            case e.NewsFilterType.Wire:
                                return 'icon-supply';
                                break;
                            case e.NewsFilterType.Channel:
                                break;
                            case e.NewsFilterType.Records:
                                break;
                            case e.NewsFilterType.PressRelease:
                                break;
                            default:
                                return 'icon-meet';
                                break;
                        }
                    },
                    deferEvaluation: true
                }),
                self.isPackage = ko.computed({
                    read: function () {
                        if (self.newsTypeId == e.NewsType.Package) {
                            return true;
                        }
                        return false;
                    },
                    deferEvaluation: true
                }),
                self.programs = ko.observableArray([]),
                self.programsCount = ko.computed({
                    read: function () {
                        return self.programs.length;
                    },
                    deferEvaluation: true
                }),

                 self.addedToRundownDetail = ko.computed({
                     read: function () {
                         var rundownStories = [];
                         if (self.newsStatistics() && self.newsStatistics().length > 0) {

                             rundownStories = _.filter(self.newsStatistics(), function (item) {
                                 if (item.Type === e.NewsStatistics.AddedToRundown) {
                                     item.TimeStr = moment.utc(item.TimeStr).format('hh:mm:ss A');
                                     return item;
                                 }
                             });

                         }

                         return rundownStories;
                     },
                     deferEvaluation: true
                 }),

                self.onAirDetail = ko.computed({
                    read: function () {
                        var onAirStories = [];
                        if (self.newsStatistics() && self.newsStatistics().length > 0) {
                            onAirStories = _.filter(self.newsStatistics(), function (item) {
                                if (item.Type === e.NewsStatistics.OnAired) {
                                    item.TimeStr = moment.utc(item.TimeStr).format('hh:mm:ss A');
                                    return item;
                                }
                            });

                        }


                        return onAirStories;
                    },
                    deferEvaluation: true
                }),

                 self.otherChannelExecutionDetail = ko.computed({
                     read: function () {
                         var otherChannelexecutions = [];
                         if (self.newsStatistics() && self.newsStatistics().length > 0) {
                             otherChannelexecutions = _.filter(self.newsStatistics(), function (item) {
                                 if (item.Type === e.NewsStatistics.ExecutedOnOtherChannels) {
                                     item.TimeStr = moment.utc(item.TimeStr).format('hh:mm:ss A');
                                     return item;
                                 }
                             });
                         }

                         return otherChannelexecutions;
                     },
                     deferEvaluation: true
                 }),

                self.otherPrograms = ko.observableArray([]),
                self.otherProgramsCount = ko.computed({
                    read: function () {
                        return self.otherPrograms.length;
                    },
                    deferEvaluation: true
                }),
                self.internationalPrograms = ko.observableArray([]),
                self.internationalProgramsCount = ko.computed({
                    read: function () {
                        return self.internationalPrograms.length;
                    },
                    deferEvaluation: true
                }),
                self.tags = [],
                self.organization = [],
                self.isUploading = ko.computed({
                    read: function () {
                        var p = self.progress();
                        if (p != '0%' && p != '100%') {
                            return true;
                        }
                        else {
                            return false;
                        }
                    },
                    deferEvaluation: true
                }),
                self.defaultImage = ko.observable(),
                self.defaultImageUrl = ko.computed({
                    read: function () {
                        return config.MediaServerUrl + 'getthumb/' + self.defaultImage() + "?UserId=" + appdata.currentUser().id;
                    },
                    deferEvaluation: true
                }),
                self.displayName = ko.computed({
                    read: function () {
                        self.reporterIdDisplay();
                        self.defaultImage();
                     
                        if (self.reporterIdDisplay()) {
                            var arr = _dc.users.getLocalById(self.reporterIdDisplay());
                            if (arr && arr.id) {
                                return arr.name.toUpperCase();
                            }
                        }
                    },
                    deferEvaluation: false
                }),
                self.defaultHighResImageUrl = ko.computed({
                    read: function () {
                        return config.MediaServerUrl + 'getresource/' + self.defaultImage() + "?thumb=true" + "?UserId=" + appdata.currentUser().id;
                    },
                    deferEvaluation: true
                }),
                self.isUsed = ko.observable(false),
                self.progress = ko.observable('0%'),
                self.tags = ko.observableArray([]),
                self.organization = ko.observableArray([]),
                self.verificationStatus = ko.computed({
                    read: function () {
                        var ticks = new Date().getTime();
                        var list = _.pluck(_.sortBy(self.newsFilters(), function (item) {
                            return (ticks - new Date(item.creationDate).getTime());
                        }), 'filterId');

                        var filteredList = _.filter(list, function (item) {
                            return (item === e.NewsVerificationStatus.Verified ||
                                item === e.NewsVerificationStatus.NotVerified ||
                                item === e.NewsVerificationStatus.Rejected);
                        });
                        if (filteredList.length > 0) {
                            return filteredList[0];
                        }
                    },
                    deferEvaluation: true
                }),
                self.iterationCount = 0,
                self.slug = ko.observable(''),
                self.highlight = ko.observable(''),
                self.updates = ko.observableArray([]),
                self.resources = ko.observableArray([]),
                self.newsFilters = ko.observableArray([]),
                self.newsComments = ko.observableArray([]),
                self.categories = ko.observableArray([]),
                self.resourceEdits = ko.observable(),
                self.bunchTags = ko.observableArray([]),
                //self.userName=
                self.fillObservableProperties = function () {
                    self.resourceImageCount(0);
                    self.resourceVideoCount(0);
                    self.resourceAudioCount(0);
                    self.resourceDocumentCount(0);

                    self.resources = _dc.resources.getByNewsId(self.id);
                    if (self.resources().length > 0) {

                        for (var i = 0; self.resources().length - 1 >= i; i++) {
                            if (self.resources()[i].type == e.ContentType.Image) {
                                self.resourceImageCount(self.resourceImageCount() + 1);
                            } else if (self.resources()[i].type == e.ContentType.Video) {
                                self.resourceVideoCount(self.resourceVideoCount() + 1);
                            }
                            else if (self.resources()[i].type == e.ContentType.Audio) {
                                self.resourceAudioCount(self.resourceAudioCount() + 1);
                            }
                            else if (self.resources()[i].type == e.ContentType.Document) {
                                self.resourceDocumentCount(self.resourceDocumentCount() + 1);
                            }
                        }
                    }
                    self.updates = _dc.news.getByParentNewsId(self.id);
                    self.newsComments = _dc.newsComments.getByNewsId(self.id);
                    self.newsFilters = _dc.newsFilters.getByNewsId(self.id);

                    var listFilters = self.newsFilters();

                    for (var i = 0; i < listFilters.length; i++) {
                        var filter = listFilters[i].filter;
                        if (filter) {
                            if (filter.bunchIds.indexOf(self.bunchId) === -1) {
                                filter.bunchIds.push(self.bunchId);
                                _dc.filters.updateData(filter);
                            }
                        }
                    }

                    for (var i = 0; i < listFilters.length; i++) {
                        var filter = listFilters[i].filter;
                        if (filter) {
                            var newsIds = filter.newsIds();
                            if (newsIds.indexOf(self.id) == -1) {
                                newsIds.push(self.id);
                                filter.newsIds(newsIds);
                                _dc.filters.updateData(filter);
                            }
                        }
                    }
                },

                self.isNullo = false;

                return self;
            }

        News.Nullo = new News();

        News.Nullo.isNullo = true;

        News.datacontext = function (dc) {
            if (dc) { _dc = dc; }
            return _dc;
        };

        return News;
    });