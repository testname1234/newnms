﻿define('model.newscomment',
    [],
    function () {

        var
            _dc = this,

            NewsComment = function () {

                var self = this;

                self.id,
                self.comment,
                self.userId,
                self.lastUpdateDateStr,
                self.commentTypeId,

                self.isNullo = false;

                return self;
            }

        NewsComment.Nullo = new NewsComment();

        NewsComment.Nullo.isNullo = true;

        NewsComment.datacontext = function (dc) {
            if (dc) { _dc = dc; }
            return _dc;
        };

        return NewsComment;
    });