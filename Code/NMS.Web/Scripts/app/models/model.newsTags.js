﻿define('model.newsTags',
    ['ko', 'jquery', 'enum', 'moment'],
    function (ko, $, e, moment) {

        var
            _dc = this,

            NewsTag = function () {

                var self = this;

                self.id,
                self.name,
                
               self.isNullo = false;

                return self;
            }

        NewsTag.Nullo = new NewsTag();

        NewsTag.Nullo.isNullo = true;

        NewsTag.datacontext = function (dc) {
            if (dc) { _dc = dc; }
            return _dc;
        };

        return NewsTag;
    });