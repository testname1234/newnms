﻿define('model.newsalert',
    ['ko'],
    function (ko) {

        var
            _dc = this,

            NewsAlert = function () {

                var self = this;

                self.id,
                self.title,
                self.url,
                self.thumbUrl,

                self.isNullo = false;

                return self;
            };

        NewsAlert.Nullo = new NewsAlert();

        NewsAlert.Nullo.isNullo = true;

        NewsAlert.datacontext = function (dc) {
            if (dc) { _dc = dc; }
            return _dc;
        };

        return NewsAlert;
    });