﻿define('model.newsbucketitem',
    ['ko', 'moment', 'model.news', 'underscore', 'appdata', 'utils', 'config', 'enum'],
    function (ko, moment, News, _, appdata, utils, config, e) {
        var
            _dc = this,

            NewsBucketItem = function () {
                var self = this;

                self.id,
                self.sequenceId,
                self.programId,
                self.tickerId,
                self.userId,
                self.storyId,
                self.news = new News(),
                self.isUsed = ko.observable(false),
                self.categoryId = ko.observable(0),
                self.categoryDisplay = ko.observable(''),
                self.preview,
                self.isGreen = ko.observable(false),
                self.isRed = ko.observable(false),
                self.previewGuid = ko.observable(),
                self.isTeaserVisible = ko.observable(),
                self.creationDate = ko.observable(),
                self.lastUpdateDate = ko.observable(),
                self.creationDisplayDate = ko.computed({
                    read: function () {
                        return moment(self.creationDate()).fromNow()
                    },
                    deferEvaluation: true
                }),
                self.lastUpdateDisplayDate = ko.computed({
                    read: function () {
                        return moment(self.lastUpdateDate()).fromNow();
                    },
                    deferEvaluation: true
                }),
                self.previewUrl = ko.computed({
                    read: function () {
                        return config.MediaServerUrl + "getresource/" + self.previewGuid();
                    },
                    deferEvaluation: true
                }),

                self.setCategoryDisplay = function () {
                    var filters = _dc.filters.getObservableList();
                    for (var i = 0, len = filters.length; i < len; i++) {
                        if (filters[i].filterTypeId === e.NewsFilterType.Category && filters[i].value === self.categoryId()) {
                            self.categoryDisplay(filters[i].name);
                            break;
                        }
                    }
                },
                
                self.isNullo = false;

                return self;
            }

        NewsBucketItem.Nullo = new NewsBucketItem();

        NewsBucketItem.Nullo.isNullo = true;

        NewsBucketItem.datacontext = function (dc) {
            if (dc) { _dc = dc; }
            return _dc;
        };

        return NewsBucketItem;
    });