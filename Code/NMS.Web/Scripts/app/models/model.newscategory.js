﻿define('model.newscategory',
    ['ko', 'jquery', 'enum', 'moment'],
    function (ko, $, e, moment) {

        var
            _dc = this,

            NewsCategory = function () {

                var self = this;

                self.id,
                self.name,

                self.isNullo = false;

                return self;
            }

        NewsCategory.Nullo = new NewsCategory();

        NewsCategory.Nullo.isNullo = true;

        NewsCategory.datacontext = function (dc) {
            if (dc) { _dc = dc; }
            return _dc;
        };

        return NewsCategory;
    });