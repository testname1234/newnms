﻿define('model.newsfile',
    ['ko', 'underscore', 'appdata', 'enum','config'],
    function (ko, _, appdata, e,config) {

        var

        _dc = this,

        NewsFile = function () {

              var self = this;

              self.id,
              self.slug = ko.observable(''),
              self.title,
              self.newsPaperDescription,
              self.languageCode,
              self.publishTime,
              self.source,
              self.parentId,
              self.folderId,
              self.statusId = ko.observable(),
              self.creationDate = ko.observable(),
              self.reportedBy = ko.observable(),
              self.lastUpdateDate = ko.observable(),
              self.newsFileDetails = null,
              self.newsFileResources = null,
              self.newsFileFolderHistory = null,
              self.newsFileStatusHistory = null,
              self.newsStatus = ko.observable(),
              self.descriptionText = ko.observable(),
              self.resourceGuid = ko.observable(),
              self.editModeON = ko.observable(false),
              self.isProgramNews = ko.observable(false),
              self.voiceOver = ko.observable(false),
              self.isTitle = ko.observable(false),
              self.isDeleted = ko.observable(false),
              self.category = ko.observable(''),
              self.source = ko.observable(''),
              self.heighlighs = ko.observable(''),
              self.interval = ko.observable(0),
              self.broadcastedCount = ko.observable(0),
              self.minStoryCount = ko.observable(0),
              self.maxStoryCount = ko.observable(0),
              self.sequenceNo = ko.observable(''),
              self.updateDisplayDate = ko.observable(false),
              self.isVerified = ko.observable('initial'),
              self.sourceTypeId = ko.observable(''),
              self.newsDurationCss = ko.observable('normalCss'),
              self.resources = ko.observableArray([]),
              self.newsTag = ko.observableArray([]),
              self.newsOrg = ko.observableArray([]),
              self.programName = ko.observable(''),
              self.programTime = ko.observable(''),
              self.isStatusFinal = ko.observable(false),
              self.isTagged = ko.observable(false),
              self.editorialComments = ko.observable(''),
              self.commentCount = ko.observable(0),
              self.bureauLocation = ko.observable(''),
              self.publishTimeStr = ko.observable(''),
              self.ResourceDisplay = ko.computed({
                  read: function () {
                      self.resourceGuid();
                      if (self.resourceGuid()) {
                          return config.MediaServerUrl + 'getthumb/' + self.resourceGuid();
                      }
                      else {
                          return '../Content/images/producer/noimage-730x380.jpg';
                      }
                  },
                  deferEvaluation: true
              }),
              self.reportedByDisplay = ko.computed({
                  read: function () {
                      var name = '';
                      if (self.reportedBy())
                          {
                          name = _dc.users.getLocalById(self.reportedBy()).name;
                      }
                      return name;
                  },
                  deferEvaluation: true
              }),
              self.folderNameDisplay = ko.computed({
                  read: function () {
                      var name = '';
                      self.reportedBy();
                      if (self.folderId) {
                          name = _dc.newsFolders.getLocalById(self.folderId).name;
                      }
                      return name;
                  },
                  deferEvaluation: true
              }),
              self.lastUpdateDateDisplay = ko.computed({
                  read: function () {
                      return moment(self.lastUpdateDate()).fromNow();
                  },
                  deferEvaluation: true
              }),

              self.creationDateDisplay = ko.computed({
                  read: function () {
                      self.updateDisplayDate();
                      return moment(self.creationDate()).fromNow();
                  },
                  deferEvaluation: true
              }),
              self.statusCSS = ko.computed({
                  read: function () {
                      self.statusId();
                      return 'status_' + self.statusId();
                  },
                  deferEvaluation: true
              }),
              self.newsFileStatusHistoryDisplay = ko.computed({
                  read: function () {
                      var arr = [];
                      arr = self.newsFileStatusHistory();
                      if (arr && arr.length > 1) {
                          arr = arr.sort(function (a, b) {
                              return new Date(b.creationDate()) - new Date(a.creationDate());
                          });
                      }
                      return arr[0];
                  },
                  deferEvaluation: true
              }),
              self.newsFileDetailsDisplay = ko.computed({
                  read: function () {
                      var arr = [];
                      arr = self.newsFileDetails();
                      if (arr && arr.length > 1) {
                          arr = arr.sort(function (a, b) {
                              return new Date(b.creationDate()) - new Date(a.creationDate());
                          });
                      }
                      return arr[0];
                  },
                  deferEvaluation: true
              }),
              self.newsFileFolderHistoryDisplay = ko.computed({
                  read: function () {
                      var arr = [];
                      arr = self.newsFileFolderHistory();
                      if (arr && arr.length > 1) {
                          arr = arr.sort(function (a, b) {
                              return new Date(b.creationDate()) - new Date(a.creationDate());
                          });
                      }
                      return arr[0];
                  },
                  deferEvaluation: true
              }),
              self.newsFileResourcesDisplay = ko.computed({
                  read: function () {
                      var arr = [];
                      arr = self.newsFileResources();
                      if (arr && arr.length > 1) {
                          arr = arr.sort(function (a, b) {
                              return new Date(b.creationDate()) - new Date(a.creationDate());
                          });
                      }
                      return arr[0];
                  },
                  deferEvaluation: true
              }),

              self.isNewsInEditableMode = ko.computed({
                  read: function () {
                      self.editModeON();
                      if (self.editModeON()) {
                          return true;
                      }
                      else {
                          return false;
                      }
                  },
                  deferEvaluation: true
              }),
              self.fillObservableProperties = function () {
                  self.newsFileDetails = _dc.newsFileDetails.getByNewsFileId(self.id);
                  self.newsFileFolderHistory = _dc.newsFileFolderHistory.getByNewsFileId(self.id);
                  self.newsFileResources = _dc.newsFileResources.getByNewsFileId(self.id);
                  self.newsFileStatusHistory = _dc.newsFileStatusHistory.getByNewsFileId(self.id);

              };
              self.getNewsFileDuration = function () {
                  if (parseInt(self.interval()) > 0) {
                      setTimeout(function () {
                          console.log('normalcss');
                            self.newsDurationCss('normalCss');
                      }, parseInt(self.interval()));

                  }
              };
              self.isNullo = false;

              return self;
          };

        NewsFile.Nullo = new NewsFile();

        NewsFile.Nullo.isNullo = true;

        // static member
        NewsFile.datacontext = function (dc) {
            if (dc) { _dc = dc; }
            return _dc;
        };

        return NewsFile;
    });