﻿define('model.newsfiledetail',
    ['ko', 'underscore','moment'],
    function (ko, _,moment) {

        var
            _dc = this,

            NewsFileDetail = function () {

                var self = this;

                self.id,
                self.newsFileId,
                self.text,
                self.createdBy = ko.observable(),
                self.slug=ko.observable(),
                self.creationDate = ko.observable(),
                self.lastUpdateDate = ko.observable(),
                self.reportedBy = ko.observable(),

                self.createdByDisplay = ko.computed({
                    read: function () {
                        return _dc.users.getLocalById(self.createdBy()).name;
                    },
                    deferEvaluation: true
                }),

                self.creationDateDisplay = ko.computed({
                    read: function () {
                        return moment(self.creationDate()).fromNow();
                    },
                    deferEvaluation: true
                }),
                self.lastUpdateDateDisplay = ko.computed({
                    read: function () {
                        return moment(self.lastUpdateDate()).fromNow();
                    },
                    deferEvaluation: true
                }),
                self.reportedByDisplay = ko.computed({
                    read: function () {
                        return self.reportedBy();
                    },
                    deferEvaluation: true
                });

                self.isNullo = false;

                self.fillObservableProperties = function () {

                };

                return self;
            }

        NewsFileDetail.Nullo = new NewsFileDetail();

        NewsFileDetail.Nullo.isNullo = true;

        // static member
        NewsFileDetail.datacontext = function (dc) {
            if (dc) { _dc = dc; }
            return _dc;
        };

        return NewsFileDetail;
    });