﻿define('model.newsfilefolderhistory',
    ['ko', 'underscore'],
    function (ko, _) {

        var
            _dc = this,

            NewsFileFolderHistory = function () {

                var self = this;

                self.id,
                self.newsFileId,
                self.userId,
                self.folderId = ko.observable(),
                self.statusId = ko.observable(),
                self.creationDate = ko.observable(),

                self.creationDateDisplay = ko.computed({
                    read: function () {
                        return self.creationDate();
                    },
                    deferEvaluation: true
                }),
                self.folderNameDisplay = ko.computed({
                    read: function () {
                        self.folderId();
                        return _dc.newsFolders.getLocalById(self.folderId()).name;
                    },
                    deferEvaluation: true
                }),

                self.isNullo = false;

                self.fillObservableProperties = function () {

                };

                return self;
            }

        NewsFileFolderHistory.Nullo = new NewsFileFolderHistory();

        NewsFileFolderHistory.Nullo.isNullo = true;

        // static member
        NewsFileFolderHistory.datacontext = function (dc) {
            if (dc) { _dc = dc; }
            return _dc;
        };

        return NewsFileFolderHistory;
    });