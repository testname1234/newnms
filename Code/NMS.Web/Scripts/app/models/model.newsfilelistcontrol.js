﻿define('model.newsfilelistcontrol',
    ['ko'],
    function (ko) {

        var
            _dc = this,

            NewsFileListControl = function () {

                var self = this;



                self.id,
                self.comment,
                self.userId,
                self.lastUpdateDateStr,
                self.commentTypeId,

                self.isNullo = false;

                return self;
            }

        NewsFileListControl.Nullo = new NewsFileListControl();

        NewsFileListControl.Nullo.isNullo = true;

        NewsFileListControl.datacontext = function (dc) {
            if (dc) { _dc = dc; }
            return _dc;
        };

        return NewsFileListControl;
    });