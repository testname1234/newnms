﻿define('model.newsfileresource',
    ['ko', 'underscore','moment','config'],
    function (ko, _,moment,config) {

        var
            _dc = this,

            NewsFileResource = function () {

                var self = this;

                self.id,
                self.newsFileId,
                self.resourceTypeId,
                self.guid,
                self.caption,
                self.duration = ko.observable(),
                self.creationDate = ko.observable(),
                self.lastUpdateDate = ko.observable(),
                self.userId = ko.observable(),

                self.userDisplay = ko.computed({
                    read: function () {
                        return self.userId();
                    },
                    deferEvaluation: true
                }),
                self.lastUpdateDateDisplay = ko.computed({
                    read: function () {
                        return moment(self.lastUpdateDate()).fromNow();
                    },
                    deferEvaluation: true
                });
                self.thumbImageUrl = ko.computed({
                    read: function () {
                        self.userId();
                        if (self.guid)
                            return config.MediaServerUrl + 'getthumb/' + self.guid;

                    },
                    deferEvaluation: true
                }),

                self.creationDateDisplay = ko.computed({
                    read: function () {
                        return moment(self.creationDate()).fromNow();
                    },
                    deferEvaluation: true
                });
               

                self.isNullo = false;

                self.fillObservableProperties = function () {

                };

                return self;
            }

        NewsFileResource.Nullo = new NewsFileResource();

        NewsFileResource.Nullo.isNullo = true;

        // static member
        NewsFileResource.datacontext = function (dc) {
            if (dc) { _dc = dc; }
            return _dc;
        };

        return NewsFileResource;
    });