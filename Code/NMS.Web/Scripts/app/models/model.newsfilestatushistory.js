﻿define('model.newsfilestatushistory',
    ['ko', 'underscore'],
    function (ko, _) {

        var
            _dc = this,

            NewsFileStatusHistory = function () {

                var self = this;

                self.id,
                self.newsFileId,
                self.statusId,
                self.userId,
                self.creationDate = ko.observable(),
              
                self.creationDateDisplay = ko.computed({
                    read: function () {
                        return moment(self.creationDate()).fromNow();
                    },
                    deferEvaluation:true
                }),
                
                self.isNullo = false;

                self.fillObservableProperties = function () {
                  
                };

                return self;
            }

        NewsFileStatusHistory.Nullo = new NewsFileStatusHistory();

        NewsFileStatusHistory.Nullo.isNullo = true;

        // static member
        NewsFileStatusHistory.datacontext = function (dc) {
            if (dc) { _dc = dc; }
            return _dc;
        };

        return NewsFileStatusHistory;
    });