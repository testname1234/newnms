﻿define('model.newsfilter',
    ['ko'],
    function (ko) {

        var
            _dc = this,

            NewsFilter = function () {

                var self = this;

                self.id,
                self.filterId,
                self.newsId,
                self.creationDate,

                self.filter,

                self.isNullo = false;

                
                self.fillObservableProperties = function () {
                    self.filter = _dc.filters.getLocalById(self.filterId);
                    if (!self.filter) {
                        self.filter = null;
                    }
                };

                return self;
            }

        NewsFilter.Nullo = new NewsFilter();

        NewsFilter.Nullo.isNullo = true;

        NewsFilter.datacontext = function (dc) {
            if (dc) { _dc = dc; }
            return _dc;
        };

        return NewsFilter;
    });