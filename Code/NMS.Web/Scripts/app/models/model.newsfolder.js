﻿define('model.newsfolder',
    ['ko', 'underscore'],
    function (ko, _) {

        var
            _dc = this,

            NewsFolder = function () {

                var self = this;

                self.id,
                self.name,
                self.categoryId,
                self.locationId = ko.observable(),
                self.location = ko.observable(),
                self.category=ko.observable(),
                self.creationDate = ko.observable(),
                self.parentFolderId,
                self.markStatusCount = ko.observable(),
                self.isRundown = ko.observable(false),
                self.startTime = ko.observable(),
                self.endTime = ko.observable(),
                self.episodeId ,
                self.programId ,
                self.creationDateDisplay = ko.computed({
                    read: function () {
                        return self.creationDate();
                    },
                    deferEvaluation: true
                }),

                self.isNullo = false;

                self.fillObservableProperties = function () {

                };

                return self;
            }

        NewsFolder.Nullo = new NewsFolder();

        NewsFolder.Nullo.isNullo = true;

        // static member
        NewsFolder.datacontext = function (dc) {
            if (dc) { _dc = dc; }
            return _dc;
        };

        return NewsFolder;
    });