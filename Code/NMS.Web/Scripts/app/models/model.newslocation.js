﻿define('model.newslocation',
    ['ko', 'jquery', 'enum', 'moment'],
    function (ko, $, e, moment) {

        var
            _dc = this,

            NewsLocation = function () {

                var self = this;

                self.id,
                self.name,
                self.tickerUnreadCount = ko.observable(0),

                self.isNullo = false;

                return self;
            }

        NewsLocation.Nullo = new NewsLocation();

        NewsLocation.Nullo.isNullo = true;

        NewsLocation.datacontext = function (dc) {
            if (dc) { _dc = dc; }
            return _dc;
        };

        return NewsLocation;
    });