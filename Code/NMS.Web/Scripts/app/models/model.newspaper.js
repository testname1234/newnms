﻿define('model.newspaper',
    [],
    function () {

        var
            _dc = this,

            NewsPaper = function () {

                var self = this;

                self.id,
                self.name,
                self.lastUpdateDate,
                self.creationDate,
                self.dailyNewsPapers = ko.observableArray([]),

                self.pending = ko.observable(0),

                self.processedPercentage = ko.computed({
                    read: function () {
                        var list = self.dailyNewsPapers();
                        var processedPages = 0;
                        var pendingPapers = 0;
                        var pendingPages = 0;

                        for (var i = 0; i < list.length; i++) {

                            if (list[i].processedPercentage() != '100%') {
                                pendingPapers++;
                            }
                            processedPages += list[i].processedPages();
                            pendingPages += list[i].pendingPages();

                        }
                        var total = processedPages + pendingPages;
                        self.pending(pendingPapers);

                        return Math.round(((processedPages / total) * 100)) + '%';
                    },
                    deferEvaluation: true
                }),

                self.fillObservableProperties = function () {
                    self.dailyNewsPapers = _dc.dailyNewsPapers.getByNewsPaperId(self.id);
                },

                self.isNullo = false;

                return self;
            }

        NewsPaper.Nullo = new NewsPaper();

        NewsPaper.Nullo.isNullo = true;

        NewsPaper.datacontext = function (dc) {
            if (dc) { _dc = dc; }
            return _dc;
        };

        return NewsPaper;
    });