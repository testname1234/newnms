﻿define('model.newspaperpage',
    ['config'],
    function (config) {

        var
            _dc = this,

            NewsPaperPage = function () {

                var self = this;

                self.id,
                self.dailyNewsPaperId,
                self.title,
                self.imageGuid,
                self.lastUpdateDate,
                self.creationDate,
                self.newsPaperPagesParts = ko.observableArray([]),
                self.dailyNewsPaper = ko.observable(),
                
                self.url = ko.computed({
                    read: function () {
                        return config.MediaServerUrl + 'getresource/' + self.imageGuid
                    },
                    deferEvaluation: true
                }),

                self.thumbImageUrl = ko.computed({
                    read: function () {
                        return config.MediaServerUrl + 'getthumb/' + self.imageGuid
                    },
                    deferEvaluation: true
                }),

          
                self.fileName = ko.computed({
                    read: function () {
                        var result = self.id + '.jpg';
                        return result;
                    },
                    deferEvaluation: true
                }),

                self.fillObservableProperties = function () {
                    self.dailyNewsPaper ( _dc.dailyNewsPapers.getLocalById(self.dailyNewsPaperId));
                },

                self.isProcessed=ko.observable(false),

                self.isNullo = false;

                return self;
            }

        NewsPaperPage.Nullo = new NewsPaperPage();

        NewsPaperPage.Nullo.isNullo = true;

        // static member
        NewsPaperPage.datacontext = function (dc) {
            if (dc) { _dc = dc; }
            return _dc;
        };

        return NewsPaperPage;
    });