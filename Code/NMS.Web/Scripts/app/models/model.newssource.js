﻿define('model.newssource',
    [],
    function () {

        var
            _dc = this,

            NewsSource = function () {

                var self = this;

                self.id,
                self.name,
                self.description,
                self.sourceTypeId,

                self.programs = ko.computed({
                    read: function () {
                        // In case of tv-channel
                        return dc.programs.getAllLocal();
                    },
                    deferEvaluation: true
                }),

                self.isNullo = false;

                return self;
            }

        NewsSource.Nullo = new NewsSource();

        NewsSource.Nullo.isNullo = true;

        // static member
        NewsSource.datacontext = function (dc) {
            if (dc) { _dc = dc; }
            return _dc;
        };

        return NewsSource;
    });