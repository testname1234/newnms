﻿define('model.notification',
    ['ko'],
    function (ko) {

        var
            _dc = this,

            Notification = function () {

                var self = this;

                self.id,
                self.type,
                self.title,
                self.argument,
                self.recipientId,
                self.senderId,
                self.slug = ko.observable(),
                self.storyScreenTemplateId,
                self.storyId,
                self.isRead = ko.observable(false),
                self.creationDate = ko.observable(),
                self.creationDateDisplay = ko.computed({
                    read: function () {
                        return moment(self.creationDate()).fromNow();
                    },
                    deferEvaluation: true
                }),
                
                self.isNullo = false;

                return self;
            }

        Notification.Nullo = new Notification();

        Notification.Nullo.isNullo = true;

        Notification.datacontext = function (dc) {
            if (dc) { _dc = dc; }
            return _dc;
        };

        return Notification;
    });