﻿define('model.profile',
    ['ko', 'underscore'],
    function (ko, _) {

        var
            _dc = this,

            Profile = function () {

                var self = this;

                self.id,
                self.name,
                self.fontfamilyId,
                self.fontSize,
                self.scrollSpeed,

                self.isNullo = false;

                self.fillObservableProperties = function () {
                   
                };

                return self;
            }

        Profile.Nullo = new Profile();

        Profile.Nullo.isNullo = true;

        // static member
        Profile.datacontext = function (dc) {
            if (dc) { _dc = dc; }
            return _dc;
        };

        return Profile;
    });