﻿define('model.program',
    ['ko'],
    function (ko) {

        var
            _dc = this,

            Program = function () {

                var self = this;

                self.id,
                self.name,
                self.channelId = ko.observable(),
                self.channel = ko.computed({
                    read: function () {
                        return _dc.channels.getLocalById(self.channelId());
                    },
                    deferEvaluation: true
                }),
                self.episodes = [],
                self.maxStoryCount = 0,
                self.minStoryCount = 0,
                self.channelVideos = ko.observableArray([]),
                self.startupGuid = ko.observable(),
                self.creditsGuid = ko.observable(),
                self.creditsThumbUrl1 = ko.observable(),
                self.startupUrl = ko.observable(),
                self.creditsUrl = ko.observable(),
                self.startupThumbUrl = ko.observable(),
                self.creditsThumbUrl = ko.observable(),
                self.isStartUpUrlVisible = ko.observable(false),
                self.isCreaditUrlVisible = ko.observable(false),
                self.bucketId = 0,
                self.apiKey = 0;
                self.filterId,
                self.pendingVideos = ko.computed({
                    read: function () {
                        var list = self.channelVideos();
                        var pending = 0;
                        for (var i = 0; i < list.length; i++) {
                            if (list[i].processedVideosPercentage() != '100%') {
                                pending++;
                            }
                        }
                        return pending;
                    },
                    deferEvaluation: true
                }),
                self.fromHour = ko.computed({
                    read: function () {
                        var list = self.channelVideos();
                        return list[0].fromHour;
                    },
                    deferEvaluation: true
                }),
                self.toHour = ko.computed({
                    read: function () {
                        var list = self.channelVideos();
                        return list[0].toHour;
                    },
                    deferEvaluation: true
                }),
                self.processedVideosPercentage = ko.computed({
                    read: function () {
                        var list = self.channelVideos();
                        var processed = 0;
                        for (var i = 0; i < list.length; i++) {
                            if (list[i].isProcessed()) {
                                processed++;
                            }
                        }
                        return Math.round(((processed / list.length) * 100)) + '%';
                    },
                    deferEvaluation: true
                }),

                self.fillObservableProperties = function () {
                    self.channelVideos = _dc.channelVideos.getByProgramId(self.id);
                };

                self.isNullo = false;

                return self;
            }

        Program.Nullo = new Program();

        Program.Nullo.isNullo = true;

        // static member
        Program.datacontext = function (dc) {
            if (dc) { _dc = dc; }
            return _dc;
        };

        return Program;
    });