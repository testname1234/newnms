﻿define('model.programelement',
    ['ko', 'moment', 'model.story', 'utils', 'appdata', 'enum'],
    function (ko, moment, Story, utils, appdata, e) {
        var
            _dc = this,

            ProgramElement = function () {
                var self = this;

                self.id,
                self.name,
                self.episodeId,
                self.program = ko.observable(),
                self.programElementType,
                self.sequenceId,
                self.stories = ko.observableArray([]),
                self.storyCount,
                self.minStoryCount = ko.observable(0),
                self.maxStoryCount = ko.observable(0),
                self.totalDuration,

                self.sortedStories = ko.computed({
                    read: function () {
                        var arr = self.stories();
                        arr = arr.sort(function (entity1, entity2) {
                            return utils.sortNumeric(entity1.sequenceId, entity2.sequenceId, 'asc');
                        });
                        return arr;
                    },
                    deferEvaluation: true
                }),

                self.placeHolderCount = ko.computed({
                    read: function () {
                        _dc.episodes.getObservableList();
                        self.populateProgramData();

                        if (self.program() && self.maxStoryCount() > 0) {
                            var count = self.maxStoryCount() - self.stories().length;
                            return count > 0 ? count : 0;
                        } else {
                            var count = self.storyCount - self.stories().length;
                            return count > 0 ? count : 0;
                        }
                    },
                    deferEvaluation: true
                }),
                self.contentDuration = ko.computed({
                    read: function () {
                        var tempDuration = 0;
                        for (var i = 0; i < self.stories().length; i++) {
                            tempDuration = tempDuration + self.stories()[i].contentDuration();
                        }
                        return tempDuration;
                    },
                    deferEvaluation: false
                }),

                self.startTime = ko.observable(),

                self.startTimeDisplay = ko.computed({
                    read: function () {
                        return moment(self.startTime()).format('hh:mm');
                    },
                    deferEvaluation: true
                }),

                self.populateProgramData = function () {
                    var tempEpisode = _dc.episodes.getLocalById(self.episodeId);
                    if (tempEpisode) {
                        var tempProgram = _dc.programs.getLocalById(tempEpisode.programId);
                        if (tempProgram && tempProgram.minStoryCount > 0 && tempProgram.maxStoryCount > 0) {
                            self.program(tempProgram);
                            self.minStoryCount(tempProgram.minStoryCount);
                            self.maxStoryCount(tempProgram.maxStoryCount);
                        }
                    }
                },

                self.processData = function () {
                    self.stories = _dc.stories.getByProgramElementId(self.id);
                },

                self.isNullo = false;

                return self;
            }

        ProgramElement.Nullo = new ProgramElement();

        ProgramElement.Nullo.isNullo = true;

        ProgramElement.datacontext = function (dc) {
            if (dc) { _dc = dc; }
            return _dc;
        };

        return ProgramElement;
    });