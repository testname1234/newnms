﻿define('model.programflowdesignercontrol',
    ['underscore', 'ko', 'enum', 'model.story', 'model.screentemplate', 'model.screenflow', 'utils', 'appdata'],
    function (_, ko, e, Story, ScreenTemplate, ScreenFlow, utils, appdata) {
        var
            _dc = this,

            ProgramFlowDesignerControl = function () {
                var self = this;

                self.isVisible = ko.observable(false),
                self.activeTab = ko.observable(''),
                self.showRelatedTemplate = ko.observable(false),

                self.currentStory = ko.observable(new Story()),
                self.templateIndex,
                self.currentTemplate = ko.observable(new ScreenTemplate()),

                self.isDragging = ko.observable(false),
                self.selectedElementId = ko.observable(0),
                self.isElementDropped = ko.observable(false),

                self.suggestedFlows = ko.observableArray([]),
                self.suggestedFlowCount = ko.observable(0),

                self.isFlowEmpty = ko.computed({
                    read: function () {
                        return true;
                    },
                    deferEvaluation: true
                }),
                self.screenElements = ko.computed({
                    read: function () {
                        var arr = [];
                        arr = _.filter(_dc.screenElements.getObservableList(), function (tempObj) {
                            return tempObj.isAllowed();
                        });
                        return arr;
                    },
                    deferEvaluation: true
                }),
                self.dropTemplate = ko.computed({
                    read: function () {
                    },
                    write: function (elementId, templateId, templateIndex) {

                        if (elementId) {
                            if (!isNaN(templateId) && !isNaN(templateIndex)) {
                                var targetTemplate = _dc.screenTemplates.getLocalById(templateId);
                                var elements = targetTemplate.elements.slice(0);

                                elements.push(elementId);
                                elements.sort(function (num1, num2) { return num1 > num2; });

                                var templateKey = 'T_' + elements.join('_');
                                var templates = _dc.screenTemplates.getAllLocal();
                                var arr = _.filter(templates, function (template) {
                                    return (template.key == templateKey && template.programId == appdata.currentProgram().id && !template.isMediaWallTemplate());
                                });

                                if (arr && arr.length > 0) {
                                    var tempScreenTemplate = new ScreenTemplate();
                                    $.extend(true, tempScreenTemplate, arr[0]);

                                    tempScreenTemplate.sequenceId = self.currentStory().screenFlow().screenTemplates()[templateIndex].sequenceId;
                                    tempScreenTemplate.script(self.currentStory().screenFlow().screenTemplates()[templateIndex].script() || '');
                                    tempScreenTemplate.uniqueId(self.currentStory().screenFlow().screenTemplates()[templateIndex].uniqueId);

                                    self.currentStory().screenFlow().screenTemplates()[templateIndex] = tempScreenTemplate;
                                    self.currentStory().screenFlow().screenTemplates.valueHasMutated();
                                    self.currentStory().screenFlow().isDirty = true;

                                    appdata.updateSuggestedFlowFlag(!appdata.updateSuggestedFlowFlag());
                                }
                            } else {
                                var templateKey = 'T_' + self.selectedElementId().toString();
                                var templates = _dc.screenTemplates.getAllLocal();

                                var arr = _.filter(templates, function (template) {
                                    return (template.key == templateKey && template.programId == appdata.currentProgram().id);
                                });

                                if (arr && arr.length > 0) {
                                    for (var i = 0; i < arr.length; i++) {
                                        if (!arr[i].isMediaWallTemplate()) {
                                            var tempScreenTemplate = new ScreenTemplate();
                                            var entity = arr[i];

                                            tempScreenTemplate.id = entity.id;
                                            tempScreenTemplate.key = entity.key;
                                            tempScreenTemplate.storyScreenTemplateId = entity.storyScreenTemplateId;
                                            tempScreenTemplate.elements = entity.elements;
                                            tempScreenTemplate.name = entity.name;
                                            tempScreenTemplate.description = entity.description;
                                            tempScreenTemplate.thumbGuid(entity.thumbGuid());
                                            tempScreenTemplate.isSelected(entity.isSelected());
                                            tempScreenTemplate.isAllowed = entity.isAllowed;
                                            tempScreenTemplate.script(entity.script() || '');
                                            tempScreenTemplate.templateBackground = entity.templateBackground;
                                            tempScreenTemplate.templateDisplayImage(entity.templateDisplayImage());

                                            tempScreenTemplate.sequenceId = self.getMaxSequenceId();
                                            tempScreenTemplate.uniqueId(_.uniqueId('ST_'));

                                            self.currentStory().screenFlow().screenTemplates.push(tempScreenTemplate);
                                            self.currentStory().screenFlow().isDirty = true;

                                            appdata.updateSuggestedFlowFlag(!appdata.updateSuggestedFlowFlag());
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }),
                self.templateTop = ko.observable(),
                self.templateTopInPixels = ko.computed({
                    read: function () {
                        return self.templateTop() + 'px';
                    },
                    deferEvaluation: true
                }),
                self.templateLeft = ko.observable(),
                self.templateLeftInPixels = ko.computed({
                    read: function () {
                        return self.templateLeft() + 'px';
                    },
                    deferEvaluation: true
                }),

                self.dropScreenTemplate = function (elementId) {
                    if (elementId) {
                        self.selectedElementId(elementId);
                        self.dropTemplate(elementId);
                    }
                },
                self.getMaxSequenceId = function () {
                    if (self.currentStory() && self.currentStory().screenFlow().screenTemplates().length > 0) {
                        var template = _.max(self.currentStory().screenFlow().screenTemplates(), function (template) { return template.sequenceId; });
                        return template ? template.sequenceId + 1 : 1;
                    } else {
                        return 1;
                    }
                },
                self.getSuggestedFlowKey = function () {
                    var templateIds = [];
                    for (var i = 0; i < self.currentStory().screenFlow().screenTemplates().length; i++) {
                        if (!self.currentStory().screenFlow().screenTemplates()[i].isMediaWallTemplate())
                            templateIds.push(self.currentStory().screenFlow().screenTemplates()[i].id);
                    }

                    if (templateIds.length > 0) {
                        return S(templateIds).toCSV('_', null).s
                    } else {
                        return '';
                    }
                },
                self.useSuggestedFlowInCurrentFlow = function (data) {
                    try {
                        if (data && data.screenTemplates() && data.screenTemplates().length > 0) {
                            for (var i = self.currentStory().screenFlow().screenTemplateCount() ; i < data.screenTemplates().length; i++) {
                                var tempScreenTemplate = new ScreenTemplate();
                                var entity = data.screenTemplates()[i];

                                tempScreenTemplate.id = entity.id;
                                tempScreenTemplate.key = entity.key;
                                tempScreenTemplate.storyScreenTemplateId = entity.storyScreenTemplateId;
                                tempScreenTemplate.elements = entity.elements;
                                tempScreenTemplate.name = entity.name;
                                tempScreenTemplate.description = entity.description;
                                tempScreenTemplate.thumbGuid(entity.thumbGuid());
                                tempScreenTemplate.isSelected(entity.isSelected());
                                tempScreenTemplate.isAllowed = entity.isAllowed;
                                tempScreenTemplate.script(entity.script() || '');
                                tempScreenTemplate.templateBackground = entity.templateBackground;
                                tempScreenTemplate.templateDisplayImage(entity.templateDisplayImage());

                                tempScreenTemplate.sequenceId = self.getMaxSequenceId();
                                tempScreenTemplate.uniqueId(_.uniqueId('ST_'));

                                self.currentStory().screenFlow().screenTemplates.push(tempScreenTemplate);
                                self.currentStory().screenFlow().isDirty = true;
                            }
                        }
                    } catch (e) {
                        //console.log(e.message);
                    }
                },
                self.createSuggestedFlows = function (data) {
                    if (data.length > 0) {
                        var tempArr = [];
                        for (var i = 0; i < data.length; i++) {
                            var screenFlow = new ScreenFlow();
                            for (var j = 0; j < data[i].length; j++) {
                                var screenTemplate = _dc.screenTemplates.getLocalById(data[i][j]);
                                if (screenTemplate && !screenTemplate.isMediaWallTemplate()) {
                                    screenFlow.screenTemplates.push(screenTemplate);
                                }
                            }
                            tempArr.push(screenFlow);
                        }

                        tempArr = tempArr.sort(function (entity1, entity2) {
                            return utils.sortNumeric(entity1.screenTemplateCount(), entity2.screenTemplateCount(), 'desc');
                        });

                        self.suggestedFlows(tempArr);
                    } else {
                        self.suggestedFlows([]);
                    }

                    self.suggestedFlowCount(self.suggestedFlows().length);
                },

                self.isNullo = false;

                return self;
            }

        ProgramFlowDesignerControl.Nullo = new ProgramFlowDesignerControl();

        ProgramFlowDesignerControl.Nullo.isNullo = true;

        ProgramFlowDesignerControl.datacontext = function (dc) {
            if (dc) { _dc = dc; }
            return _dc;
        };

        return ProgramFlowDesignerControl;
    });