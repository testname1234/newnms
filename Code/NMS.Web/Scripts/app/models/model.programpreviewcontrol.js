﻿define('model.programpreviewcontrol',
   ['ko', 'moment', 'model.story', 'model.screentemplate', 'utils'],
   function (ko, moment, Story, ScreenTemplate, utils) {
       var
            dc = this,

            ProgramPreviewControl = function () {

                var self = this;

                self.stories = ko.observableArray([new Story()]),
                self.isVisible = ko.observable(false),
                self.stopFlag = ko.observable(false),
                self.pauseFlag = ko.observable(false),

                self.currentTemplateIndex = ko.observable(0),
                self.elapsedTime = ko.observable(1),
                self.elapsedTimeBeforeCurrentTemplate = ko.observable(1),
                self.currentStoryIndex = ko.observable(0),
                self.programPreviewUrl = ko.observable(),
                self.currentTemplate = ko.computed({
                    read: function () {
                        if (self.isVisible()) {
                            var template = self.stories()[self.currentStoryIndex()].screenFlow().screenTemplates()[self.currentTemplateIndex()];
                            if (template) {
                                return template;
                            }
                        }

                        return new ScreenTemplate();
                    },
                    deferEvaluation: true
                }),
                self.previewDuration = ko.computed({
                    read: function () {
                        var totalDurationForAllStories = 0;
                        for (var i = 0; i < self.stories().length; i++) {
                            totalDurationForAllStories += self.stories()[i].screenFlow().contentDuration();
                        }

                        return totalDurationForAllStories;
                    },
                    deferEvaluation: true
                }),
                self.displayTime = ko.computed({
                    read: function () {
                        return utils.getDuration(self.elapsedTime(), 'second') + '/' + utils.getDuration(self.previewDuration(), 'second');
                    },
                    deferEvaluation: true
                }),

                self.start = function () {
                    setTimeout(function renderPreview() {
                        if (self.currentTemplate() && !self.stopFlag()) {
                            if (self.elapsedTime() > (self.elapsedTimeBeforeCurrentTemplate() + self.currentTemplate().contentDuration())) {
                                if (self.currentTemplateIndex() < self.stories()[self.currentStoryIndex()].screenFlow().screenTemplates().length - 1) {
                                    self.currentTemplateIndex(self.currentTemplateIndex() + 1);
                                    self.elapsedTimeBeforeCurrentTemplate(self.elapsedTimeBeforeCurrentTemplate() + self.currentTemplate().contentDuration());
                                } else if ((self.currentStoryIndex() + 1) < self.stories().length) {
                                    self.currentStoryIndex(self.currentStoryIndex() + 1);
                                    self.currentTemplateIndex(0);
                                    self.elapsedTimeBeforeCurrentTemplate(self.elapsedTimeBeforeCurrentTemplate() + self.currentTemplate().contentDuration());
                                }
                                else {
                                    self.stopFlag(true);
                                }
                            }
                        }
                        if (!self.stopFlag() && self.elapsedTime() < (self.previewDuration())) {
                            self.elapsedTime(self.elapsedTime() + 1);
                            setTimeout(renderPreview, 1000);
                        }
                    }, 1000);
                },

                self.reset = function () {
                    self.stories([new Story()]);
                    self.stopFlag(false);
                    self.pauseFlag(false);
                    self.currentStoryIndex(0);
                    self.currentTemplateIndex(0);
                    self.elapsedTime(1);
                    self.elapsedTimeBeforeCurrentTemplate(1);
                    self.programPreviewUrl('');
                },

                self.isNullo = false;

                return self;
            };

       ProgramPreviewControl.Nullo = new ProgramPreviewControl();

       ProgramPreviewControl.Nullo.isNullo = true;

       ProgramPreviewControl.datacontext = function (dc) {
           if (dc) { _dc = dc; }
           return _dc;
       }

       return ProgramPreviewControl;
   });