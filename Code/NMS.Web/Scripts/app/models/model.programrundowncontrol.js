﻿define('model.programrundowncontrol',
    ['ko', 'moment', 'appdata', 'enum', 'utils','underscore'],
    function (ko, moment, appdata, e, utils,_) {

        var
            _dc = this,

            ProgramRundownControl = function () {

                var self = this;

                self.activeTab = ko.observable('tab1'),
                self.isSendToBroadCastVisible = ko.observable(true),
                self.isPreviewButtonVisible = ko.computed({
                    read: function () {
                        var storiesArray = _dc.stories.getByEpisodeId(appdata.currentEpisode().id)();
                        var flag = false;
                        //for (var i = 0; i < storiesArray.length; i++) {
                        //    if (storiesArray[i].screenFlow().screenTemplates().length > 0) {
                        //        flag = true;
                        //        break;
                        //    }
                        //}
                        return flag;
                    },
                    deferEvaluation:true
                }),
                self.isActive = ko.observable(false),

                self.episode = ko.computed({
                    read: function () {
                        return appdata.currentEpisode();
                    },
                    deferEvaluation: true
                }),
                self.program = ko.computed({
                    read: function () {
                       
                        appdata.currentEpisode();
                        if (self.episode && self.episode().programId)
                            return _dc.programs.getLocalById(self.episode().programId)
                        else
                            return false;
                    },
                    deferEvaluation: true
                }),

                self.stories = ko.computed({
                    read: function () {
                        if (self.isActive()) {
                            appdata.arrangeNewsFlag();
                            var storiesArray = _dc.stories.getByEpisodeId(appdata.currentEpisode().id)();

                            storiesArray = _.filter(storiesArray, function (item) {
                                return item.isGreen() === true;
                            });
                            var arr = storiesArray.sort(function (entity1, entity2) {
                                return entity1.sequenceId > entity2.sequenceId ? 1 : -1;
                            });

                            return arr;
                        }
                    },
                    deferEvaluation: true
                }),

                self.programElements = ko.computed({
                    read: function () {
                        if (self.isActive()) {
                            var programElementArray = _dc.programElements.getByEpisodeId(appdata.currentEpisode().id)();
                            programElementArray = programElementArray.sort(function (entity1, entity2) {
                                return utils.sortNumeric(entity1.sequenceId, entity2.sequenceId, 'asc');
                            });
                            return programElementArray;
                        }
                    },
                    deferEvaluation: true
                }),

                self.episodeStartTimeDisplay = ko.computed({
                    read: function () {
                        return moment(self.episode().startTime()).format('hh:mm');
                    },
                    deferEvaluation: true
                }),

                self.computeRundownTime = function () {
                    var programElements = _dc.programElements.getByEpisodeId(appdata.currentEpisode().id)();
                    var startTimeRef = appdata.currentEpisode().startTime();

                    for (var i = 0; i < programElements.length; i++) {

                        programElements[i].startTime(startTimeRef);

                        if (programElements[i].programElementType === e.ProgramElementType.Segment) {

                            var segmentStories = _dc.stories.getByProgramElementId(programElements[i].id)();
                            for (var j = 0; j < segmentStories.length; j++) {
                                segmentStories[j].startTime(startTimeRef);
                                startTimeRef = moment(startTimeRef).add(segmentStories[j].contentDuration(), 'second').toISOString();
                            }

                        } else if (programElements[i].programElementType === e.ProgramElementType.Break) {
                            startTimeRef = moment(startTimeRef).add(programElements[i].contentDuration, 'second').toISOString();
                        } else if (programElements[i].programElementType === e.ProgramElementType.ProgramStartup) {
                            startTimeRef = moment(startTimeRef).add(programElements[i].contentDuration, 'second').toISOString();
                        } else if (programElements[i].programElementType === e.ProgramElementType.ProgramEnd) {
                            startTimeRef = moment(startTimeRef).add(programElements[i].contentDuration, 'second').toISOString();
                        }
                    }
                },

                self.isNullo = false;

                return self;
            }

        ProgramRundownControl.Nullo = new ProgramRundownControl();

        ProgramRundownControl.Nullo.isNullo = true;

        ProgramRundownControl.datacontext = function (dc) {
            if (dc) { _dc = dc; }
            return _dc;
        };

        return ProgramRundownControl;
    });