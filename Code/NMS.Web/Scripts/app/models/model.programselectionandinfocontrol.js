﻿define('model.programselectionandinfocontrol',
    ['ko'],
    function (ko) {

        var
            _dc = this,

            ProgramSelectionAndInfoControl = function () {

                var self = this;

                self.currentPage = ko.observable(''),

                self.isNullo = false;

                return self;
            }

        ProgramSelectionAndInfoControl.Nullo = new ProgramSelectionAndInfoControl();

        ProgramSelectionAndInfoControl.Nullo.isNullo = true;

        // static member
        ProgramSelectionAndInfoControl.datacontext = function (dc) {
            if (dc) { _dc = dc; }
            return _dc;
        };

        return ProgramSelectionAndInfoControl;
    });