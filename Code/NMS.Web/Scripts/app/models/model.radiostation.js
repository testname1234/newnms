﻿define('model.radiostation',
    [],
    function () {

        var
            _dc = this,

            RadioStation = function () {

                var self = this;

                self.id,
                self.name,
                self.radioDescription = 'Switch channel timings according to your news from the list below:',
                self.radioPendingText = 'Pending Time Durations: ',
                self.creationDate ,
                self.lastUpdateDate,
                self.radioStreams = ko.observableArray([]),

                self.pending = ko.computed({
                    read: function () {
                        var list = self.radioStreams();
                        var pending = 0;
                        for (var i = 0; i < list.length; i++) {
                            if (!list[i].isProcessed()) {
                                pending++;
                            }
                        }
                        return pending;
                    },
                    deferEvaluation: true
                }),

                self.processedPercentage = ko.computed({
                    read: function () {
                        var list = self.radioStreams();
                        var processed = 0;
                        for (var i = 0; i < list.length; i++) {
                            if (list[i].isProcessed()) {
                                processed++;
                            }
                        }
                        return Math.round(((processed / list.length) * 100)) + '%';
                    },
                    deferEvaluation: true
                }),

                self.fillObservableProperties = function () {
                    self.radioStreams = _dc.radioStreams.getByChannelId(self.id);
                },

                self.isNullo = false;

                return self;
            }

        RadioStation.Nullo = new RadioStation();

        RadioStation.Nullo.isNullo = true;

        RadioStation.datacontext = function (dc) {
            if (dc) { _dc = dc; }
            return _dc;
        };

        return RadioStation;
    });