﻿define('model.radiostream',
    [],
    function () {

        var
            _dc = this,

            RadioStream = function () {

                var self = this;

                self.id,
                self.channelId=ko.observable(),
                self.creationDate ,
                self.lastUpdateDate ,
                self.from ,
                self.to ,
                self.fromHour,
                self.toHour ,
                self.hourlySlotId ,
                self.episodeDate ,
                self.isProcessed=ko.observable(false),             
                self.url,
                self.programId,

                self.processedPercentage = ko.computed({
                    read: function () {
                        if (self.isProcessed()) {
                            return '100%';
                        }
                        return '0%';
                    },
                    deferEvaluation: true
                }),

                self.radioStation = ko.computed({
                    read: function () {
                        return _dc.radioStations.getLocalById(self.channelId());
                    },
                    deferEvaluation: true
                }),

                self.fileName = ko.computed({
                    read: function () {
                        var result = self.radioStation().name + self.episodeDate + '.mp3';
                        return result;
                    },
                    deferEvaluation: true
                }),

                self.fillObservableProperties = function () {

                };


                self.isNullo = false;

                return self;
            }

        RadioStream.Nullo = new RadioStream();

        RadioStream.Nullo.isNullo = true;

        RadioStream.datacontext = function (dc) {
            if (dc) { _dc = dc; }
            return _dc;
        };

        return RadioStream;
    });