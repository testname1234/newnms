﻿define('model.reportnews',
    ['ko', 'config', 'enum', 'moment', 'manager', 'datacontext', 'control.editorcontrol', 'control.tag', 'control.uploader', 'control.language', 'control.templatepaging', 'control.videoeditor', 'control.multiselectusers', 'reporter.appdata', 'router', 'helper', 'datacontext'],
    function (ko, config, e, moment, manager, dc, EditorControl, Tag, Uploader, Language, PopuptempPaging, VideoEditor, MultiSelect, appdata, router, helper, datacontext) {

        var
            _dc = this,

            ReportNews = function (urduId, englishId, selectors, newsType, btnText, contentType, newsFilterType, btnClass, inputTextWithKeyBoard, newslisttemptext, isAssignment) {

                if (!btnClass) {
                    btnClass = '';
                }
                var self = this;
                self.reporterBureauId = ko.observable(),
                self.duration = ko.observable(0),
                self.reporterSource = ko.observable(),
                self.chnlprogramsList = ko.observableArray([]);
                self.programs = ko.observableArray([]);
                self.reporters = ko.observableArray([]);
                self.isAssignment = ko.observable(isAssignment);
                self.reportedMediaUrl = ko.observable(),
                self.isReportMediaTypeImage = ko.observable(false),
                self.isReportMediaTypeVideo = ko.observable(false),


                self.newsFilterType = newsFilterType,
                self.newsType = newsType,
                self.viewModelId = 'id-' + new Date().getTime(),
                self.Urdu = urduId,
                self.English = englishId,
                self.categorySource = '/api/category/GetCategoryByTerm/',
                self.newsOrg = ko.observable(new Tag('/api/tag/getorganizationbyterm/')),
                self.isEventNewsType = ko.observable(false),
                self.locationSource = '/api/Location/GetLocationByTerm/',
                self.eventSource = '/api/newsfile/GetEventTypeByTerm/',
                self.clear = ko.observable(false),
                self.language = new Language('UR', selectors),
                self.popuptempPaging = new PopuptempPaging(false),
                self.showPopup = ko.observable(false),
                self.isTickerClear = ko.observable(false),
                self.isEdited = ko.observable(false),
                self.tickerLines = ko.observableArray([])
                self.totalImages = ko.observable(''),
                self.parentNewsId,
                self.referenceId,
                self.resourceEdit,
                self.lastTab = 0,
                self.templatePaging = new PopuptempPaging(true),
                self.showFullTemplate = ko.observable(false),
                self.mediaEditor = new VideoEditor(),
                self.newsList = ko.observableArray([]),
                self.stopPlayer = ko.observable(false),
                self.isTickerView = ko.observable(false),
                self.currentFontSize = ko.observable(15),
                self.isTelevisionView = ko.observable(true),
                self.isNewsPaperView = ko.observable(false),
                self.resetDescription = ko.observable(false),
                self.isNewsPaperDetail = ko.observable(true),
                self.isNewsFileHistoryVisible = ko.observable(false),
                self.isReportedMedia = ko.observable(false),
                self.isCreateFile = ko.observable(false),
                self.isGetMediaArchivalData = ko.observable(false),
                self.previousFontsize = ko.observable(0);
                self.programId = ko.observable(0);
                self.onBehalfReporterID = ko.observable(0);
                self.assignedTo = ko.observable(0);
                self.editNewsInfo = ko.observable(''),
                self.tvDescription = ko.observable(''),
                self.npDescription = ko.observable(''),
                self.tcDescription = ko.observable(''),
                self.producerDescription = ko.observable(''),
                self.currentTicker = ko.observable(''),
                self.suggestions = ko.observable(''),
                self.newsObject = ko.observable(''),
                self.newsFileDescription = ko.observable('').extend({ throttle: 500 }),
                self.currentEditableNewsFile = ko.observable(''),
                self.newsFileStatusHistory = ko.observableArray([]),
                self.newsFolderStatusHistory = ko.observableArray([]),
                self.newsFileDetailHistory = ko.observableArray([]),
                self.NewsPaperDetail = ko.observableArray([]),
                self.reporterList = ko.observableArray([]),
                self.isNewsEdit = ko.observable(false),
                self.isFull = ko.observable(false),
                self.currentItem = ko.observable(''),
                self.isEditedApply = ko.observable(true),
                self.isDirty = ko.observable(false),
                self.isDsng = ko.observable(false),
                self.isBeltPack = ko.observable(false),
                self.isLiveFeed = ko.observable(false),
                self.CoverageType = ko.observableArray(["0", "1"]),
                self.isRecordedFeed = ko.observable(false),
                self.isUpdating = ko.observable(false),
                self.Highlight = ko.observable().extend({ throttle: 500 }),
                self.cancelNewsFile = function () {
                    self.isFull(false);
                    self.currentItem('');
                    self.isDirty(false);
                    self.currentEditableNewsFile('');
                    self.newsFileDescription('');
                    self.Slug('');
                    self.Highlight('');
                    self.newFileTitle('');
                    self.selectedCategory({});
                    self.selectedLocation({});
                    self.selectedEvent({});
                    self.isEditedApply(true),
                    self.isEdited(false);
                    self.uploader().resources([]);
                    self.uploader().uploadedResources([]);
                },
                self.closePopUp = function () {
                    $(".newsFileOverlay").removeClass('displayCreateFile');
                    self.isCreateFile(false);
                },

                self.selectedCoverage = function (coverageType) {
                    //if (coverageType === 0)
                    //{
                    //    self.isRecordedFeed(false);
                    //}
                    //if (coverageType === 1)
                    //{
                    //    self.isLiveFeed(false);
                    //}
                },
                self.submitNewFile = function () {
                    var resource = [];
                    for (var i = 0; i < self.uploader().mappedResources().length; i++) {
                        var temp = {
                            NewsFileId: self.currentEditableNewsFile().id,
                            ResourceTypeId: self.uploader().mappedResources()[i].type,
                            Guid: self.uploader().mappedResources()[i].guid,
                            UserId: helper.getCookie('user-id'),
                        }
                        resource.push(temp);
                    }

                    if (self.Slug()) {
                        var tempObj = {
                            FolderId: 12,
                            StatusId: 1,
                            Slug: self.Slug(),
                            Highlight: self.Highlight(),
                            LocationId: parseInt([self.selectedLocation().id]),
                            CategoryId: parseInt([self.selectedCategory().id]),
                            Title: self.newFileTitle(),
                            LanguageCode: 'ur',
                            Text: self.newsFileDescription(),
                            CreatedBy: helper.getCookie('user-id'),
                            FileResource: resource,
                        };
                        if (self.currentEditableNewsFile && self.currentEditableNewsFile() && self.currentEditableNewsFile().id) {
                            tempObj.NewsFolderId = self.currentEditableNewsFile().folderId;
                            tempObj.NewsFileId = self.currentEditableNewsFile().id;
                            tempObj.StatusId = self.currentEditableNewsFile().statusId;
                        }

                        $.when(manager.newsfile.createNewsFile(tempObj))
                            .done(function () {
                                config.logger.success('NewsFile Created Successfully !');
                                self.cancelNewsFile();
                            })
                            .fail(function () {
                                config.logger.success('Error Occurred');
                            });
                        self.closePopUp();
                    }
                    else {
                        config.logger.error("Please Enter Slug!");
                    }
                },
                self.updateCurrentDescription = ko.computed({
                    read: function () {
                        dc.newsFileDetails.getObservableList();
                        if (self.currentEditableNewsFile() && self.currentEditableNewsFile().newsFileDetails().length > 0) {
                            self.currentEditableNewsFile().reportedBy(self.currentEditableNewsFile().newsFileDetails()[self.currentEditableNewsFile().newsFileDetails().length - 1].reportedBy());
                            self.currentEditableNewsFile().slug(self.currentEditableNewsFile().newsFileDetails()[self.currentEditableNewsFile().newsFileDetails().length - 1].slug());
                            self.Slug(self.currentEditableNewsFile().slug());
                            self.Highlight(self.currentEditableNewsFile().highlight());
                            self.newsFileDescription(self.currentEditableNewsFile().newsFileDetails()[self.currentEditableNewsFile().newsFileDetails().length - 1].text);
                        }
                        return '';

                    },
                    deferEvaluation: false
                }),
                self.updateCurrentStatus = ko.computed({
                    read: function () {
                        dc.newsFileStatusHistory.getObservableList();

                        if (self.currentEditableNewsFile() && self.currentEditableNewsFile().newsFileStatusHistory().length > 0) {
                            var arr = dc.newsFileStatusHistory.getByNewsFileId(self.currentEditableNewsFile().id)();
                            arr = arr.sort(function (a, b) {
                                return new Date(b.creationDate()) - new Date(a.creationDate());
                            });

                            self.currentEditableNewsFile().statusId == arr[0].statusId;
                            if (self.currentEditableNewsFile().statusId == e.NewsFileStatus.Red)
                                self.currentEditableNewsFile().statusCSS('newsfileredstatus');
                            if (self.currentEditableNewsFile().statusId == e.NewsFileStatus.Green)
                                self.currentEditableNewsFile().statusCSS('newsfilegreenstatus');
                            if (self.currentEditableNewsFile().statusId == e.NewsFileStatus.Yellow)
                                self.currentEditableNewsFile().statusCSS('newsfileyellowstatus');
                            if (self.currentEditableNewsFile().statusId == e.NewsFileStatus.Blue)
                                self.currentEditableNewsFile().statusCSS('newsfilebluestatus');
                        }
                        return '';
                    },
                    deferEvaluation: false
                }),
                self.updateNewsFile = function () {
                    var resource = [];
                    for (var i = 0; i < self.uploader().mappedResources().length; i++) {
                        var temp = {
                            NewsFileId: self.currentEditableNewsFile().id,
                            ResourceTypeId: self.uploader().mappedResources()[i].type,
                            Guid: self.uploader().mappedResources()[i].guid,
                            UserId: helper.getCookie('user-id'),
                        }
                        resource.push(temp);
                    }
                    if (self.Slug()) {
                        var tempObj = {
                            FolderId: 12,
                            StatusId: 1,
                            Slug: self.Slug(),
                            Highlight: self.Highlight(),
                            LocationId: parseInt([self.selectedLocation().id]),
                            CategoryId: parseInt([self.selectedCategory().id]),
                            Title: self.newFileTitle(),
                            LanguageCode: 'ur',
                            Text: self.newsFileDescription(),
                            CreatedBy: helper.getCookie('user-id'),
                            ReportedBy: helper.getCookie('user-id'),
                            FileResource: resource,
                        };
                        if (self.currentEditableNewsFile && self.currentEditableNewsFile() && self.currentEditableNewsFile().id) {
                            tempObj.NewsFolderId = self.currentEditableNewsFile().folderId;
                            tempObj.NewsFileId = self.currentEditableNewsFile().id;
                            tempObj.StatusId = self.currentEditableNewsFile().statusId;
                        }
                        $.when(manager.newsfile.createNewsFile(tempObj))
                            .done(function () {
                                if (tempObj.NewsFileId) {
                                    self.isDirty(false);
                                    config.logger.success('NewsFile Updated Successfully !');
                                }
                                else
                                {
                                    config.logger.success('NewsFile Created Successfully !');
                                }
                                self.isDirty(false);
                                self.cancelNewsFile();
                            })
                            .fail(function () {
                                config.logger.success('Error Occurred');
                            });
                        self.closePopUp();
                    }
                    else {
                        config.logger.error("Please Enter Slug!");
                    }
                },

                self.createNewFile = function () {
                    $(".newsFileOverlay").addClass('displayCreateFile');
                    self.isCreateFile(true);
                },
                self.isTeamB = ko.computed({
                    read: function () {
                        return self.newsFilterType == e.NewsFilterType.Channel
                        || self.newsFilterType == e.NewsFilterType.Radio
                        || self.newsFilterType == e.NewsFilterType.NewsPaper;
                    },
                    deferEvaluation: false
                }),
                self.mediaEditor.src = ko.computed({
                    read: function () {
                        var currentVideo = self.templatePaging.currentItem();
                        if (currentVideo && self.templatePaging.currentItem().isProcessed) {
                            self.mediaEditor.isProcessed(self.templatePaging.currentItem().isProcessed());
                            return currentVideo.url;
                        } else if (currentVideo && currentVideo.resourceEdits) {
                            self.mediaEditor.isProcessed(true);
                            return currentVideo.resourceEdits.Url;
                        }
                        return '';
                    },
                    deferEvaluation: false
                }),
                self.suggestedNews = ko.observableArray([]),
                self.channelId,
                self.channelName,
                self.radioStationId,
                self.dailyNewsPaperId,
                self.coordinates = ko.observable(),
                self.memory = {},

                // input - fields - start
                self.Slug = ko.observable().extend({ throttle: 500 }),
                //                self.Highlight = ko.observable().extend({ throttle: 500 }),
                self.title = ko.observable('').extend({ throttle: 1000 }),
                self.newFileTitle = ko.observable(''),
                self.inputTextWithKeyBoard = !!inputTextWithKeyBoard,
                self.comments = ko.observable(),
                self.selectedCategory = ko.observable({}),
                self.category = ko.computed({
                    read: function () {
                        if (self.selectedCategory() && self.selectedCategory().id) {
                            return self.selectedCategory().value;
                        }
                        return '';
                    },
                    deferEvaluation: true

                }),
                self.selectedLocation = ko.observable({}),
                self.selectedEvent = ko.observable({}),
                self.location = ko.computed({
                    read: function () {
                        if (self.selectedLocation() && self.selectedLocation().id) {
                            return self.selectedLocation().value;
                        }
                        return '';
                    },
                    deferEvaluation: true

                }),
                self.eventType = ko.computed({
                    read: function () {                        
                        if (self.selectedEvent() && self.selectedEvent().value) {
                            return self.selectedEvent().value;
                        }
                        return '';
                    },
                    deferEvaluation: true

                }),
                self.editor = ko.observable(new EditorControl('Urdu', '')),
                self.newsTag = ko.observable(new Tag('/api/tag/gettagbyterm/')),
                self.uploader = ko.observable(new Uploader()),
                self.newsDateTime = ko.observable(moment(new Date()).format('DD MMMM YYYY HH:mm:SS')),

                self.multiSelectUsers = ko.observable(new MultiSelect()),
                self.cameraMenList = ko.observableArray([]),
                self.cameraMenIds = [],
                self.buttonText = btnText,
                self.buttonClass = btnClass,
                self.newslisttemplate = newslisttemptext,
                self.videouploadcheck,

                self.description = ko.computed({
                    read: function () {
                        var comments = self.comments();
                        var content = self.editor().content();
                        self.tvDescription();
                        self.resetDescription();
                        var script = $('.leftPanel .editor .jqte .jqte_editor').html();
                        if (router.currentHash() === config.views.fieldreporter.updatenews.url || (router.currentHash() === '#/report-news' && appdata.currentUser().userType == e.UserType.Creporter) || (router.currentHash() === '#/report-news' && appdata.currentUser().userType == e.UserType.NpReporter)) {
                            if (comments && comments.length > 0) {
                                return comments;
                            }
                            else if (content && content.length > 0) {
                                return content;
                            }
                        }
                        else {
                            var desc = self.isTelevisionView() ? script : self.tvDescription();
                            return self.tvDescription(desc);
                        }
                        return '';
                    },
                    deferEvaluation: false
                }),

                self.currentNewsTab = ko.computed({
                    read: function () {
                        self.isTickerView();
                        self.isNewsPaperView();
                        self.isTelevisionView();
                        if (self.isTickerView())
                            return 'Ticker';
                        if (self.isNewsPaperView())
                            return 'NewsPaper';
                        if (self.isTelevisionView())
                            return 'Television';

                        //console.log('here');
                    },
                    deferEvaluation: true
                }),
                self.newsPaperDescription = ko.computed({
                    read: function () {
                        var comments = self.comments();
                        var script = $('.leftPanel .editor .jqte .jqte_editor').html();
                        var content = self.editor().content();
                        self.resetDescription();
                        if (router.currentHash() != config.views.fieldreporter.reportNews.url) {
                            if (comments && comments.length > 0) {
                                return comments;
                            }
                            else if (content && content.length > 0) {
                                return content;
                            }
                        }
                        else {
                            var desc = self.isNewsPaperView() ? script : self.npDescription();
                            return self.npDescription(desc);
                        }
                        return '';
                    },
                    deferEvaluation: false
                }),
                self.pendingItems = ko.computed({
                    read: function () {
                        var list = self.templatePaging.list();
                        var value = self.templatePaging.currentItem();
                        var count = 0;
                        if (list.length > 0) {
                            for (var i = 0; i < list.length; i++) {
                                var item = list[i];
                                if (!item.isProcessed()) {
                                    count++;
                                }
                            }
                            if (self.newsFilterType == e.NewsFilterType.NewsPaper) {
                                return count;
                            }
                            else {
                                return moment.utc(count * 1000 * 60 * 60).format("HH:mm:ss");
                            }
                        }
                    },
                    deferEvaluation: true
                }),
                self.subscribeEvents = function () {
                    self.Slug.subscribe(function (value) {
                        if (self.currentItem() && self.currentItem().slug() !== self.Slug()) {
                            self.isDirty(true);
                            self.isEditedApply(true);
                        }
                        else {
                            if (self.Slug() && !self.isEdited()) {
                                self.isDirty(true);
                                self.isEditedApply(true);
                            }
                        }
                    });
                    self.newsFileDescription.subscribe(function (value) {
                        if (self.currentItem() && self.currentItem().newsFileDetails().length > 0) {
                            if (self.currentItem().newsFileDetails()[self.currentItem().newsFileDetails().length - 1].text !== self.newsFileDescription()) {
                                self.isDirty(true);
                                self.isEditedApply(true);
                            }
                        }
                        else {
                            if (self.newsFileDescription()) {
                                self.isDirty(true);
                                self.isEditedApply(true);
                            }
                        }

                    });
                },
            self.displayCameraMans = ko.computed({
                read: function () {
                    dc.users.getObservableList();
                    if (appdata.isCameramanFill) {
                        appdata.isCameramanFill();
                    }

                    if (dc.users.getObservableList().length > 0) {
                        var temp = dc.users.getObservableList();
                        var unique = temp.filter((v, i, a) => a.indexOf(v) === i);
                        self.cameraMenList([]);
                        self.cameraMenList(unique);
                        var list = [];
                        for (i = 0; i < self.cameraMenList().length; i++) {
                            if (self.cameraMenList()[i].userRoles() === e.UserType.FReporter) {
                                var arr = {
                                    id: self.cameraMenList()[i].id,
                                    name: self.cameraMenList()[i].name,
                                    img: self.cameraMenList()[i].profilePhotoUrl == '' ? 'content/images/common/default-profile.png' : self.cameraMenList()[i].profilePhotoUrl,
                                    designation: '',
                                    isSelected: ko.observable(false),
                                };
                                list.push(arr);
                            }
                        }

                        self.multiSelectUsers().items(list);
                        self.multiSelectUsers().filteredItems(list);
                    }
                },
                deferEvaluation: false
            }),

                // input - fields - end

                // helper - methods - start

                self.showErrorMessage = function (msg) {
                    config.logger.error(msg);
                    return false;
                },

                self.clearViewModel = function () {
                    // saving last values in memory
                    self.memory.referenceId = self.referenceId,
                    self.memory.title = self.title();
                    self.memory.comments = self.comments();
                    if (self.producerDescription() != "") {
                        self.memory.editorContent = self.producerDescription();
                    } else {

                        self.memory.editorContent = self.editor().content();
                    }
                    self.memory.selectedCategory = self.selectedCategory();
                    self.memory.selectedLocation = self.selectedLocation();
                    self.memory.organization = self.newsOrg().tags();
                    self.memory.tags = self.newsTag().tags();
                    self.memory.slug = self.Slug();
                    self.memory.highlight = self.Highlight();

                    self.memory.newsDateTime = self.newsDateTime();
                    self.memory.mediaEditortotalSelections = self.mediaEditor.totalSelections();
                    //clear
                    self.referenceId = null;
                    self.title('');
                    self.Slug('');
                    self.Highlight('');
                    self.tickerLines([]);
                    self.npDescription('');
                    self.tvDescription('');
                    cameraMenIds = [];
                    self.multiSelectUsers().selectedItems([]);
                    self.comments('');
                    self.resourceEdit = null;
                    self.clear(true);
                    self.selectedCategory({
                    });
                    self.selectedLocation({
                    });
                    self.newsTag().tags([]);
                    self.newsOrg().tags([]);
                    self.newsDateTime('');
                    self.newsDateTime(moment(new Date()).format('DD MMMM YYYY HH:mm:SS'));
                    self.mediaEditor.totalSelections([]);
                    self.suggestions('');
                    self.uploader().started(false);
                    self.uploader().finished(false);
                    self.uploader().src('');
                    self.uploader().resources([]);
                    self.uploader().uploadedResources([]);
                    self.producerDescription('');
                    self.isNewsPaperDetail(false);
                    self.NewsPaperDetail([]);
                    self.programId(0);
                    self.onBehalfReporterID(0);
                    self.assignedTo(0);
                    $('.leftPanel .editor .jqte .jqte_editor').html('');

                    //clear selected Cameramans
                    _.filter(self.multiSelectUsers().items(), function (cameraMan) {
                        return cameraMan.isSelected(false);


                    });
                    _.filter(self.multiSelectUsers().filteredItems(), function (cameraMan) {
                        return cameraMan.isSelected(false);
                    });


                    //Clear tabs
                    self.isTickerView(false);
                    self.isNewsPaperView(false);
                    self.isTelevisionView(true);

                    var filterdReporters = _.filter(dc.users.getObservableList(), function (obj) {
                        return obj.userRoles() == e.UserType.FReporter;
                    });
                    var scannedReporters = _.sortBy(filterdReporters, 'displayName');
                    self.reporterList(scannedReporters);


                    if (self.language.langCode().toLowerCase() == 'ur') {
                        setTimeout(function () {
                            var eID = "#" + self.Urdu;
                            $(eID).click();
                        }, 500);
                    } else {
                        setTimeout(function () {
                            var eID = "#" + self.English;
                            $(eID).click();
                        }, 500);
                    }
                },

                self.resetViewModel = function () {
                    self.referenceId = self.memory.referenceId,
                    self.title(self.memory.title);
                    self.comments(self.memory.comments);

                    self.clear(false);
                    var obj = self.editor();
                    obj.content(self.memory.editorContent);

                    obj.isContentSet(true);
                    self.editor(obj);
                    self.Slug(self.memory.slug);
                    self.Highlight(self.memory.highlight);
                    self.newsOrg().tags(self.memory.organization);
                    self.producerDescription(self.memory.editorContent);

                    self.selectedCategory(self.memory.selectedCategory);
                    self.selectedLocation(self.memory.selectedLocation);
                    self.newsTag().tags(self.memory.tags);
                    self.newsDateTime(self.memory.newsDateTime);
                    self.mediaEditor.totalSelections(self.memory.mediaEditortotalSelections);
                },

                self.displayCurrentThumb = function (data, b) {
                    if (data && b.currentTarget.id) {
                        var id = b.currentTarget.id;
                        var list = self.uploader().mappedResources();
                        for (var i = 0; i < list.length; i++) {
                            if (id == list[i].id) {
                                var index = list.indexOf(list[i]);
                                self.popuptempPaging.list = self.uploader().mappedResources;
                                self.popuptempPaging.currentItem(list[i]);
                                self.popuptempPaging.currentIndex(index);
                                self.totalImages((list.length) - 1);
                            }
                        }
                    } else if (data && b.currentTarget.id == "") {
                        var id = data.id;
                        var list = self.currentItemReources();
                        var index = list.indexOf(data);
                        self.popuptempPaging.list = self.currentItemReources;
                        self.popuptempPaging.currentItem(data);
                        self.popuptempPaging.currentIndex(index);
                        self.totalImages((list.length) - 1);
                    } else {
                    }
                    self.showPopup(true);
                },

                self.deleteThumbnail = function (a, b) {
                    if (b) {
                        var id = b.currentTarget.id;
                        self.removeResourcesFromUploader(id);
                        $.when(manager.news.deletevideofile(id))
                          .done(function (responseData) {
                          })
                          .fail(function () {
                          });

                    }
                },

                self.removeResourcesFromUploader = function (id) {
                    var list = self.uploader().uploadedResources();
                    var resources = self.uploader().resources();
                    for (var i = 0; i < list.length; i++) {
                        if (list[i].Guid == id) {
                            list.remove(list[i]);
                            for (var j = 0; j < resources.length; j++) {
                                if (resources[j].Guid == id) {
                                    resources.remove(resources[j]);
                                }
                            }
                        }

                    }
                    self.uploader().uploadedResources(list);
                },

                self.hidePopup = function () {
                    self.showPopup(false);
                    self.stopPlayer(true);
                },




                self.deletevideofile = function () {
                    var id = self.uploader().mappedResources()[0].id;
                    $.when(manager.news.deletevideofile(id))
                           .done(function (responseData) {
                               self.uploader(new Uploader());
                           })
                           .fail(function () {
                               config.logger.error("Error : Could not delete.");
                           });
                },

                // helper - methods - end
                //self.testUploader = ko.computed({
                //    read: function () {
                //        var arr = self.uploader().resources();
                //        console.log(arr.length);
                //    },
                //    deferEvaluation: false

                //}),


                self.autoSuggestNews = ko.computed({
                    read: function () {
                        if (self.isTeamB() && !self.showFullTemplate()) {

                            var text = $.trim(self.title());
                            var result = [];

                            //if (text.length > 0 && text.length % 3 == 0) {
                            if (text.length > 0) {
                                $.when(manager.news.getNewsByTerm(text))
                                    .done(function (responseData) {
                                        if (responseData.IsSuccess) {

                                            self.suggestedNews(responseData.Data);

                                        } else {
                                            self.suggestedNews([]);
                                        }
                                    })
                                    .fail(function () {
                                        self.suggestedNews([]);
                                    });
                            }
                        }
                        else {
                            self.suggestedNews([]);
                        }
                    },
                    deferEvaluation: false

                }),

                self.fillViewModel = function (item) {

                    self.clear(false);

                    self.referenceId = item._id;

                    self.title(item.Title);

                    self.Highlight(item.Highlight);

                    self.producerDescription(item.Description);

                    var obj = self.editor();
                    obj.content(item.Description);
                    obj.isContentSet(true);
                    self.editor(obj);


                    self.language.langCode(item.LanguageCode);

                    var categories = item.Categories;
                    if (categories && categories.length > 0) {
                        self.selectedCategory({
                            id: categories[0].CategoryId, value: categories[0].Category
                        });
                    }

                    var locations = item.Locations;
                    if (locations && locations.length > 0) {
                        self.selectedLocation({
                            id: locations[0].LocationId, value: locations[0].Location
                        });
                    }

                    if (item.Slug) {
                        self.Slug(item.Slug);
                    }

                    var tags = item.Tags;
                    array = [];
                    if (tags) {
                        for (var i = 0; i < tags.length; i++) {
                            array.push(tags[i].Tag);
                        }
                        self.newsTag().tags(array);
                    }



                    self.newsDateTime(moment(new Date(item.PublishTimeStr)).format('DD MMMM YYYY HH:MM:SS'));

                    if (item.languageCode.toLowerCase() == 'ur') {
                        setTimeout(function () {
                            var eID = "#" + self.Urdu;
                            $(eID).click();
                        }, 500);
                    } else {
                        setTimeout(function () {
                            var eID = "#" + self.English;
                            $(eID).click();
                        }, 500);
                    }

                },

                self.fillViewModelInput = function (item) {
                    self.reporterBureauId(item.bureauId());
                    self.reporterSource(item.source());
                    self.newsFilterType = item.sourceTypeId;
                    },


                self.fillViewModelProducer = function (item) {

                    self.editNewsInfo(item);
                    self.clear(false);

                    // self.referenceId = item.id;
                    self.id = item.id;
                    self.title(item.title());
                    self.programId(item.programId());
                    self.assignedTo(item.assignedTo());
                    self.producerDescription(item.description());

                    self.language.langCode(item.languageCode);

                    var categories = item.categories();
                    if (categories && categories.length > 0) {
                        self.selectedCategory({
                            id: categories[0].CategoryId, value: categories[0].Category
                        });
                    }
                    var locations = item.reportedLocations();
                    if (locations != null && locations.length > 0)
                        self.selectedLocation({
                            id: locations[0].id, value: locations[0].name
                        });
                    //if (locations && locations.length > 0) {
                    //    for (var i = 0; i < item.reportedLocations().length; i++) {
                    //        var Id = '';
                    //        if (item.reportedLocations()[i].name == locations[0]) {
                    //            Id = item.reportedLocations()[i].value;
                    //        }
                    //    }
                    //    self.selectedLocation({ id: Id, value: locations[0] });
                    //}
                    // self.location(item.reportedLocations());
                    var tags = item.tags();
                    array = [];
                    if (tags) {

                        if (parseInt($.trim(helper.getCookie('role-id'))) == e.UserType.Producer || parseInt($.trim(helper.getCookie('role-id'))) == e.UserType.FReporter || parseInt($.trim(helper.getCookie('role-id'))) == e.UserType.Creporter || parseInt($.trim(helper.getCookie('role-id'))) == e.UserType.controllerInput) {
                            for (var i = 0; i < tags.length; i++) {
                                array.push(tags[i]);
                            }
                        }
                        else {
                            for (var i = 0; i < tags.length; i++) {
                                array.push(tags[i].Tag);
                            }
                        }
                        self.newsTag().tags(array);
                    }
                    self.newsDateTime(moment(new Date(item.publishTime)).format('DD MMMM YYYY HH:MM:SS'));
                    if (item.slug()) {
                        self.Slug(item.slug());
                    }
                    if (item.highlight()) {
                        self.Highlight(item.highlight());
                    }

                    if (item.isNewsEdit()) {
                        self.isNewsEdit(true);
                    }


                    if (item.languageCode.toLowerCase() == 'ur') {
                        setTimeout(function () {
                            var eID = "#" + self.Urdu;
                            $(eID).click();
                        }, 500);
                    } else {
                        setTimeout(function () {
                            var eID = "#" + self.English;
                            $(eID).click();
                        }, 500);
                    }


                    if (item.organization) {
                        var tagsO = item.organization();
                        array = [];
                        if (tagsO) {

                            for (var i = 0; i < tagsO.length; i++) {
                                if (tagsO[i].hasOwnProperty("name")) {
                                    array.push(tagsO[i].name);
                                }
                                else {
                                    array.push(tagsO[i]);
                                }

                            }
                            self.newsOrg().tags(array);
                        }

                    }

                    if (item.EventType && item.EventType) {
                        self.isEventNewsType(true);
                        self.EventType = item.EventType;
                        if (item.Coverage)
                            $("#toggle-off").click();
                        if (!item.Coverage)
                            $("#toggle-on").click();

                        if (item.EventResource && item.EventResource.length > 0) {
                            for (var x = 0; x < item.EventResource.length; x++) {
                                if (item.EventResource[x].EventResourceType == 0)
                                    self.isDsng(true);
                                if (item.EventResource[x].EventResourceType == 1)
                                    self.isBeltPack(true);
                            }
                        }

                        self.selectedEvent({ id: item.EventType, value: item.EventTypeName });

                        if (item.EventReporter && item.EventReporter.length > 0) {
                            for (var k = 0; k < item.EventReporter.length; k++) {
                                _.filter(self.multiSelectUsers().filteredItems(), function (obj) {
                                    if (item.EventReporter[k].ReporterId == obj.id) {
                                        self.multiSelectUsers().selectUser(obj);
                                    }
                                    return obj;
                                });

                            }
                        }
                    }
                },

                self.newsSelected = function (item) {
                    self.showFullTemplate(true);
                    $('.reportNewsChannelReporter .jqte_tool_22').hide();

                    if (item && item._id) {
                        self.suggestedNews([]);
                        self.fillViewModel(item);
                    }


                    self.toggleLanguage();
                    $('.reportNewsChannelReporter .jqte_tool_22').hide();

                },


                  self.newsSelectedProduction = function (item) {
                      if (item && item.id) {
                          self.fillViewModelProducer(item);
                      }
                  },

                self.showTemplate = function () {
                    self.showFullTemplate(true);
                    $('.reportNewsChannelReporter .jqte_tool_22').hide();
                    self.toggleLanguage();
                },

                self.toggleLanguage = function () {
                    if (self.language.langCode().toLowerCase() == 'ur') {
                        if (self.language.urduElement) {
                            self.language.urduElement().click();
                        }
                    }
                    else {
                        if (self.language.englishElement) {
                            self.language.englishElement().click();
                        }
                    }
                },

                self.setCurrentItem = function (data) {
                    self.clearViewModel();
                    self.showFullTemplate(true);
                    if (data && data[0] && data[0].durationInSeconds()) {
                        self.duration(data[0].durationInSeconds());
                    }
                    self.templatePaging.list(data);
                    self.templatePaging.currentIndex(0);
                    self.templatePaging.currentItem(self.templatePaging.list()[0]);

                },

                self.markMediaProcessed = function () {

                    var obj = {
                        id: self.templatePaging.currentItem().id, typeId: contentType
                    };
                    if (!self.templatePaging.currentItem().isProcessed()) {
                        $.when(manager.channelVideos.markVideoProcessed(obj))
                        .done(function () {
                            //var obj = self.templatePaging.currentItem();
                            //obj.isProcessed(true);
                            //self.templatePaging.currentItem(obj);
                            //config.logger.success("Media Processed");
                        })
                            .fail(function () {
                                config.logger.error("failed");

                            });
                    } else {
                    }
                },

                self.hideFullTemplate = function () {
                    self.showFullTemplate(false);
                },
                self.menuClick = function (currentNewsTab) {
                    var script = $('.leftPanel .editor .jqte .jqte_editor').html();
                    if (currentNewsTab == 1) {
                        if (!self.isTelevisionView()) {
                            self.isTickerView(false);
                            self.isNewsPaperView(false);
                            self.npDescription(script);
                            $('.leftPanel .editor .jqte .jqte_editor').html(self.tvDescription());
                            self.lastTab = currentNewsTab;
                            self.isTelevisionView(true);
                        }

                    }
                    else if (currentNewsTab == 2) {
                        if (!self.isNewsPaperView()) {
                            self.isTickerView(false);
                            self.isTelevisionView(false);
                            self.tvDescription(script);
                            $('.leftPanel .editor .jqte .jqte_editor').html(self.npDescription());
                            self.lastTab = currentNewsTab;
                            self.isNewsPaperView(true);
                        }
                    }
                    else if (currentNewsTab == 3) {

                        self.isTickerView(true);
                        self.isTelevisionView(false);
                        self.isNewsPaperView(false);
                        if (self.lastTab == 1) {
                            self.tvDescription(script);
                            $('.leftPanel .editor .jqte .jqte_editor').html(self.tvDescription());
                        }
                        if (self.lastTab == 2) {
                            self.npDescription(script);
                            $('.leftPanel .editor .jqte .jqte_editor').html(self.npDescription());
                        }
                    }
                    self.resetDescription(!self.resetDescription());
                },

                self.changeFontSize = function () {

                    if ($("#fontSelector").hasClass('font_' + self.previousFontsize())) {
                        $("#fontSelector").removeClass('font_' + self.previousFontsize())
                    }
                    self.previousFontsize(self.currentFontSize());
                    $("#fontSelector").addClass('font_' + self.currentFontSize());
                },
                self.submit = function (callback) {
                    var channelName = '';
                    var selectedProgram = '';

                    if (self.channelId > 0) {
                        channelName = dc.channels.getLocalById(self.channelId).name;
                    }
                    else {
                        channelName = '';
                    }

                    if (self.programId() > 0) {
                        selectedProgram = dc.programs.getLocalById(self.programId()).name;
                    }
                    else {
                        selectedProgram = '';
                    }

                    if (self.Slug() != '' && channelName != '' && selectedProgram != '') {
                        var slug = self.Slug() + ' - ' + channelName + ' - ' + selectedProgram;
                    }
                    else if (self.Slug() == '' && channelName != '' && selectedProgram != '') {
                        var slug = channelName + ' - ' + selectedProgram;
                    }
                    else {
                        var slug = channelName;
                    }

                    var isTitleEmpty = self.title().trim() !== '';
                    //var isDescriptionEmpty = self.producerDescription().trim() !== '';
                    //var isHighlightEmpty = self.Highlight().trim() !== '';
                    //var isSlugEmpty = self.Slug().trim() !== '';
                    var isNotAllowed = appdata.currentUser().userType !== e.UserType.FReporter;

                    if (isTitleEmpty == false) {
                        return self.showErrorMessage("Enter Title ");
                    }
                    //    if (isDescriptionEmpty == false) {
                    //        return self.showErrorMessage("Enter Description ");
                    //}
                    //    if (isHighlightEmpty == false && appdata.currentUser().userType != e.UserType.Creporter) {
                    //        return self.showErrorMessage("Enter Highlight ");
                    //}
                    //    if (isSlugEmpty == false) {
                    //        return self.showErrorMessage("Enter Slug ");
                    //}

                    var locationIds = [];
                    if (self.multiSelectUsers && self.multiSelectUsers().selectedItems && self.multiSelectUsers().selectedItems().length > 0) {
                        for (i = 0; i < self.multiSelectUsers().selectedItems().length; i++) {
                            self.cameraMenIds.push(self.multiSelectUsers().selectedItems()[i].id);
                        }
                    }

                    //if (self.newsType !== e.NewsType.Story && (new Date(self.newsDateTime()).getTime() > (new Date()).getTime())) {
                    //if (new Date(self.newsDateTime()).getTime() > (new Date()).getTime()) {
                    //    return self.showErrorMessage("Select valid date");
                    //}

                    //if (self.isTeamB() && !self.resourceEdit && appdata.currentUser().userType != e.UserType.NpReporter) {
                    //    return self.showErrorMessage("Select content for news.");
                    //}
                    if (appdata.currentUser().userType == e.UserType.NpReporter && !self.isNewsPaperDetail()) {
                        return self.showErrorMessage("Select NewsPaper Page Part");
                    }
                    if (appdata.currentUser().userType == e.UserType.NpReporter && !self.NewsPaperDetail() && self.NewsPaperDetail().length < 1) {
                        return self.showErrorMessage("Select NewsPaper Page Part");
                    }

                    if (!self.title() && !self.isTickerView()) {
                        return self.showErrorMessage("Enter Title ");
                    }
                    if (self.title().length > 200) {
                        return self.showErrorMessage("Title should be less then 200 charachters");
                    }
                    //    if (self.Slug().length > 200) {
                    //        return self.showErrorMessage("Slug should be less then 200 charachters");
                    //}
                    if (self.videouploadcheck === 'submitpackage' && self.uploader().resources() <= 0) {
                        return self.showErrorMessage("Upload Video");
                    }
                    //if (self.description().length <= 0 || self.npDescription().length <= 0 || self.tickerLines().length<=0) {
                    //    return self.showErrorMessage("Please Provide atleast one type of content");
                    //}

                    //if (self.videouploadcheck === 'submitpackage' && self.uploader().resources() <= 0) {
                    //    return self.showErrorMessage("Upload Video");
                    //}

                    //if (self.newsTag().tags().length <= 0) {
                    //    return self.showErrorMessage("Please Provide Tags");
                    //}
                    //if (!self.selectedCategory() || !self.selectedCategory().id) {
                    //    return self.showErrorMessage("Please Provide Valid Category");
                    //}

                    //if (self.newsFilterType !== e.NewsFilterType.PublicReporter && (!self.selectedLocation() || !self.selectedLocation().id)) {
                    //    return self.showErrorMessage("Please Provide News Location");
                    //}

                    if (self.newsFilterType == e.NewsFilterType.PublicReporter) {
                        locationIds = [1];
                    }
                        //if (self.isTickerView() && self.tickerLines && self.tickerLines().length <= 0) {
                        //    return self.showErrorMessage("Please Insert Atleast One ticker");
                        //}
                    else {
                        locationIds = [self.selectedLocation().id];
                    }

                    var categoryIds = [self.selectedCategory().id];

                    var newsTagsArray = [];
                    for (var i = 0; i < self.newsTag().tags().length; i++) {
                        var item = {
                            _id: null, Tag: self.newsTag().tags()[i]
                        }
                        newsTagsArray.push(item);
                    }
                    if (!self.Slug() && appdata.currentUser().userType === e.UserType.FReporter && !self.isTickerView()) {
                        return self.showErrorMessage("Enter Slug");
                    }
                        //if (self.multiSelectUsers().selectedItems().length <= 0 && appdata.currentUser().userType === e.UserType.FReporte && !self.isTickerView()) {
                        //    return self.showErrorMessage("Select Cameraman");
                        //}
                    else {
                        for (i = 0; i < self.multiSelectUsers().selectedItems().length; i++) {
                            self.cameraMenIds.push(self.multiSelectUsers().selectedItems()[i].id);
                        }
                    }

                    var requestData = {
                        // parentNewsId: self.parentNewsId,
                        NewsFileId: self.id,
                        LanguageCode: self.language.langCode(),
                        title: self.title(),
                        description: router.currentHash() == config.views.fieldreporter.reportNews.url ? self.tvDescription() : self.description(),
                        newsDateStr: new Date(self.newsDateTime()).toISOString(),
                        categoryIds: categoryIds,
                        locationIds: locationIds,
                        tags: newsTagsArray,
                        filterTypeId: self.newsFilterType,
                        reporterId: appdata.userId,
                        newsTypeId: self.newsType,
                        resources: self.uploader().resources(),
                        referenceNewsId: self.referenceId,
                        ResourceEdit: self.resourceEdit,
                        channelId: self.channelId,
                        radioStationId: self.radioStationId,
                        newsPaperId: self.dailyNewsPaperId,
                        //slug: self.Slug() == "" ? "No Slug": self.Slug(),
                        slug: slug,
                        highlight: self.Highlight(),
                        newsCameraMen: self.cameraMenIds,
                        tickerLines: self.tickerLines && self.tickerLines().length > 0 ? self.tickerLines() : null,
                        NewsPaperdescription: router.currentHash() == config.views.fieldreporter.reportNews.url ? self.npDescription() : self.newsPaperDescription(),
                        Tickers: self.tickerLines(),
                        currentTicker: self.currentTicker(),
                        Suggestions: self.suggestions(),
                        //ProgramId: self.programId(),
                        ProgramId: 0,
                        AssignedTo: self.assignedTo(),
                        BureauId: appdata.locationId
                    };

                    if (appdata.currentUser().userType == e.UserType.Creporter) {
                        if (self.language.langCode() == 'EN') {
                            requestData.LanguageCode = 'en';
                        }
                        else {
                            requestData.LanguageCode = 'ur';
                        }
                    }

                    if (router.currentHash() === config.views.fieldreporter.updatenews.url || (router.currentHash() === '#/report-news' && appdata.currentUser().userType == e.UserType.Creporter) || (router.currentHash() === '#/report-news' && appdata.currentUser().userType == e.UserType.NpReporter)) {
                        requestData.description = self.description();
                    }
                    if (parseInt($.trim(helper.getCookie('role-id'))) == e.UserType.Producer || parseInt($.trim(helper.getCookie('role-id'))) == e.UserType.FReporter || parseInt($.trim(helper.getCookie('role-id'))) == e.UserType.HeadlineProducer || parseInt($.trim(helper.getCookie('role-id'))) == e.UserType.Creporter || parseInt($.trim(helper.getCookie('role-id'))) == e.UserType.TickerProducer || parseInt($.trim(helper.getCookie('role-id'))) == e.UserType.controllerInput) {
                        requestData.description = self.producerDescription();
                    }
                    if (appdata.currentUser().userType == e.UserType.NpReporter) {

                        for (var i = 0; i < self.NewsPaperDetail().length; i++) {
                            requestData.resources.push({
                                ResourceId: null, ResourceTypeId: 1, Guid: self.NewsPaperDetail()[i].ImageGuid
                            });
                        }
                    }
                    config.logger.info('News is being reported');

                    var timeSpan = [self.mediaEditor.editedVideoLength()].clone();;


                    if (self.isNewsEdit()) {
                        requestData.BunchGuid = self.editNewsInfo().bunchId;
                        requestData.Guid = self.editNewsInfo().id;
                    }

                    self.isNewsEdit(false);
                    if (parseInt($.trim(helper.getCookie('role-id'))) == e.UserType.Producer || parseInt($.trim(helper.getCookie('role-id'))) == e.UserType.FReporter) {
                        if (window.parent && window.parent.document.location.hash == "#/home") {
                            requestData.isIframe = true;
                        }
                    }


                    $.when(manager.news.onsubmit(requestData))
                            .done(function (responseData) {
                                if (responseData && responseData.IsSuccess) {
                                    self.isTickerClear(false);
                                    self.isTickerClear(true);
                                    self.editNewsInfo('');
                                    responseData.Data.Progress = self.uploader().uploadProgress;

                                    if (!isNotAllowed) {
                                        var resources = responseData.Data.Resources;
                                        var newsId = responseData.Data._id;
                                        if (responseData.Data.ParentNewsId) {
                                            newsId = responseData.Data.ParentNewsId;
                                        }
                                        //if (!newsId) {
                                        //    newsId = responseData.Data.NewsFileId;
                                        //}

                                        dc.resources.fillData(resources, {
                                            newsId: newsId
                                        });
                                        var options = {
                                            sort: true, returnList: true
                                        };
                                        if (self.parentNewsId) {
                                            options.groupByParentNewsId = true;
                                        }

                                        var news = dc.news.fillData([responseData.Data], options);
                                        self.newsObject(news);
                                    }

                                    self.uploader().started(false);
                                    self.uploader().finished(false);
                                    self.uploader().src('');
                                    self.uploader().resources([]);
                                    self.uploader().uploadedResources([]);

                                    if (!isNotAllowed) {
                                        if (self.newsFilterType == e.NewsFilterType.Channel || self.newsFilterType == e.NewsFilterType.Radio) {
                                            news[0].totalTimeSpan = timeSpan[0];
                                        }
                                    }
                                    if (self.isTeamB()) {
                                        var newsId = responseData.Data._id;
                                        if (!newsId) {
                                            newsId = responseData.Data.NewsFileId;
                                        }
                                        if (responseData.Data.ParentNewsId) {
                                            newsId = responseData.Data.ParentNewsId;
                                        }

                                        dc.resources.fillData(responseData.Data.Resources, {
                                            newsId: newsId
                                        });
                                        var options = {
                                            sort: true, returnList: true
                                        };
                                        if (self.parentNewsId) {
                                            options.groupByParentNewsId = true;
                                        }
                                        var news = dc.news.fillData([responseData.Data], options);
                                        //$(parent.window.document.getElementById('hiddenData')).html(JSON.stringify(responseData.Data));
                                        if (appdata.currentUser().userType == e.UserType.Creporter && (!news[0].defaultImage() || !news[0].defaultImage().length == 0)) {
                                            if (news[0]._id) {
                                                news[0].id = news[0]._id;
                                            }
                                            if (responseData.Data.Resources && responseData.Data.Resources[0] && responseData.Data.Resources[0].Guid) {
                                                news[0].defaultImage(responseData.Data.Resources[0].Guid);
                                            }
                                            if ($.browser.mozilla) {
                                                config.logger.success("News Reported");
                                                self.clearViewModel();
                                                // self.newsObject(responseData.Data);
                                                self.newsList.push(news[0]);
                                                if (callback) {
                                                    callback();
                                                }
                                            }

                                        }
                                        if ($.browser.chrome) {
                                            //    self.newsObject(responseData.Data);
                                            self.newsList.push(news[0]);
                                        }

                                        //self.newsObject(responseData.Data);

                                        //self.newsList.push(news[0]);
                                    }
                                    if ($.browser.chrome) {
                                        config.logger.success("News Reported");
                                        self.clearViewModel();

                                        if (callback) {
                                            callback();
                                        }
                                    }

                                    $(parent.window.document.getElementById('hiddenData')).html(JSON.stringify(responseData.Data));
                                    if (parent.window && parent.window.document && $(parent.window.document.getElementById('createNewsPopup')).hasClass('isNews')) {
                                        setTimeout(function () {
                                            $(parent.window.document.getElementById('createNewsPopup')).removeClass('isNews');
                                            $(parent.window.document.getElementById('closeNewsPopUp')).click();
                                            $(parent.window.document.getElementById('hiddenData')).click();
                                            window.parent.postMessage($(parent.window.document.getElementById('hiddenData')).html(), "*");
                                        }, 5000);

                                    }
                                } else {
                                    if (responseData.Errors && responseData.Errors[0].indexOf('Duplicate') > -1) {

                                    }
                                    else {
                                        config.logger.error("Error occured.");
                                        self.resetViewModel();
                                    }
                                }
                            })
                            .fail(function () {
                                config.logger.error("Error occured: Your internet connection seems down.");
                                self.resetViewModel();
                                return false;
                            });

                    self.clearViewModel();
                    $('.reportNewsChannelReporter .jqte_tool_22').hide();
                },

                self.submitNewsForUpdate = function (callback) {
                    var isNotAllowed = appdata.currentUser().userType !== e.UserType.FReporter;

                    var locationIds = [];

                    if (self.isTeamB() && !self.resourceEdit && appdata.currentUser().userType != e.UserType.NpReporter) {
                        return self.showErrorMessage("Select content for news.");
                    }
                    if (appdata.currentUser().userType == e.UserType.NpReporter && !self.isNewsPaperDetail()) {
                        return self.showErrorMessage("Select NewsPaper Page Part");
                    }
                    if (appdata.currentUser().userType == e.UserType.NpReporter && !self.NewsPaperDetail() && self.NewsPaperDetail().length < 1) {
                        return self.showErrorMessage("Select NewsPaper Page Part");
                    }

                    if (!self.title() && !self.isTickerView()) {
                        return self.showErrorMessage("Enter Title ");
                    }
                    if (self.title().length > 200) {
                        return self.showErrorMessage("Title should be less then 200 charachters");
                    }
                    if (self.Slug().length > 200) {
                        return self.showErrorMessage("Slug should be less then 200 charachters");
                    }
                    if (self.videouploadcheck === 'submitpackage' && self.uploader().resources() <= 0) {
                        return self.showErrorMessage("Upload Video");
                    }

                    if (!self.selectedCategory() || !self.selectedCategory().id) {
                        return self.showErrorMessage("Please Provide Valid Category");
                    }

                    if (self.newsFilterType !== e.NewsFilterType.PublicReporter && (!self.selectedLocation() || !self.selectedLocation().id)) {
                        return self.showErrorMessage("Please Provide News Location");
                    }

                    if (self.newsFilterType == e.NewsFilterType.PublicReporter) {
                        locationIds = [1];
                    }

                    else {
                        locationIds = [self.selectedLocation().id];
                    }

                    var categoryIds = [self.selectedCategory().id];

                    var newsTagsArray = [];
                    for (var i = 0; i < self.newsTag().tags().length; i++) {
                        var item = {
                            _id: null, Tag: self.newsTag().tags()[i]
                        }
                        newsTagsArray.push(item);
                    }
                    if (!self.Slug() && appdata.currentUser().userType === e.UserType.FReporter && !self.isTickerView()) {
                        return self.showErrorMessage("Enter Slug");
                    }

                    if (self.isEventNewsType() && !self.eventType()) {
                        return self.showErrorMessage("Please Select Event Type");
                    }

                    var entities = [];
                    for (var i = 0; i < self.newsOrg().tags().length; i++) {
                        var entity = self.newsOrg().tags()[i];
                        entities.push(entity);
                    }
                    var reporterIds = [];
                    for (var i = 0; i < self.multiSelectUsers().selectedItems().length; i++) {
                        reporterIds.push(self.multiSelectUsers().selectedItems()[i].id);
                    }

                    var eventResourceIds = [];

                    if (self.isDsng())
                        eventResourceIds.push(0);
                    if (self.isBeltPack())
                        eventResourceIds.push(1);
                    if (self.isEventNewsType())
                        self.newsFilterType = 21;


                    var otherReporter = parseInt(self.onBehalfReporterID());
                    var requestData = {

                        NewsFileId: self.id,
                        LanguageCode: self.language.langCode(),
                        title: self.title(),
                        description: router.currentHash() == config.views.fieldreporter.reportNews.url ? self.tvDescription() : self.description(),
                        newsDateStr: new Date(self.newsDateTime()).toISOString(),
                        categoryIds: categoryIds,
                        locationIds: locationIds,
                        tags: newsTagsArray,
                        Organizations: entities,
                        filterTypeId: self.newsFilterType, //--
                        reporterId: otherReporter == 0 ? appdata.userId : otherReporter,
                        newsTypeId: self.newsType,
                        resources: self.uploader().resources(),
                        referenceNewsId: self.referenceId,
                        ResourceEdit: self.resourceEdit,
                        channelId: self.channelId,
                        radioStationId: self.radioStationId,
                        newsPaperId: self.dailyNewsPaperId,
                        slug: self.Slug(),
                        highlight: self.Highlight(),
                        newsCameraMen: self.cameraMenIds,
                        tickerLines: self.tickerLines && self.tickerLines().length > 0 ? self.tickerLines() : null,
                        NewsPaperdescription: router.currentHash() == config.views.fieldreporter.reportNews.url ? self.npDescription() : self.newsPaperDescription(),
                        Tickers: self.tickerLines(),
                        currentTicker: self.currentTicker(),
                        Suggestions: self.suggestions(),
                        ProgramId: self.programId(),
                        AssignedTo: self.assignedTo(),
                        EventType: (self.selectedEvent() && self.selectedEvent().value) ? self.selectedEvent().value : null,
                        Coverage: self.isLiveFeed() ? true : false,
                        EventResourceId: eventResourceIds,
                        EventReporterId: reporterIds,
                        BureauId: self.reporterBureauId(),
                        Source: self.reporterSource(),
                    };

                    requestData.description = self.producerDescription();


                    var info_Txt = 'News is being reported';
                    if (self.isEventNewsType())
                        info_Txt = 'Event is being added';

                    var info_reported = 'News Updated';

                    if (self.isEventNewsType())
                        info_reported = 'Event added';

                    config.logger.info(info_Txt);

                    var timeSpan = [self.mediaEditor.editedVideoLength()].clone();;


                    self.isNewsEdit(false);




                    //
                    $.when(manager.news.UpdateNewsFileWithHistory(requestData))
                            .done(function (responseData) {
                                if (responseData && responseData.IsSuccess) {
                                    self.isTickerClear(false);
                                    self.isTickerClear(true);
                                    self.editNewsInfo('');
                                    responseData.Data.Progress = self.uploader().uploadProgress;
                                    self.isLiveFeed(false);
                                    self.isRecordedFeed(false);
                                    self.isBeltPack(false);
                                    self.isDsng(false);
                                    self.multiSelectUsers().selectedItems([]);
                                    self.selectedEvent([]);

                                    self.id = null;

                                    var newsId = responseData.Data._id;
                                    latestNewsFileId = newsId;


                                    dc.news.clearAllNews(true);
                                    dc.resources.clearAllNews(true);



                                    config.logger.success(info_reported);
                                    if (callback)
                                        callback();

                                    self.clearViewModel();

                                }
                            })
                            .fail(function () {
                                config.logger.error("Error occured: Your internet connection seems down.");
                                self.resetViewModel();

                            });

                    self.clearViewModel();
                    $('.reportNewsChannelReporter .jqte_tool_22').hide();
                },

                self.submitNewsFile = function (callback) {

                    var isNotAllowed = appdata.currentUser().userType !== e.UserType.FReporter;

                    var locationIds = [];
                    if (self.multiSelectUsers && self.multiSelectUsers().selectedItems && self.multiSelectUsers().selectedItems().length > 0) {
                        for (i = 0; i < self.multiSelectUsers().selectedItems().length; i++) {
                            self.cameraMenIds.push(self.multiSelectUsers().selectedItems()[i].id);
                        }
                    }

                    if (self.isTeamB() && !self.resourceEdit && appdata.currentUser().userType != e.UserType.NpReporter) {
                        return self.showErrorMessage("Select content for news.");
                    }
                    if (appdata.currentUser().userType == e.UserType.NpReporter && !self.isNewsPaperDetail()) {
                        return self.showErrorMessage("Select NewsPaper Page Part");
                    }

                    if (self.isEventNewsType() && !self.eventType())
                    {
                        return self.showErrorMessage("Please Select Event Type");
                    }
                    if (appdata.currentUser().userType == e.UserType.NpReporter && !self.NewsPaperDetail() && self.NewsPaperDetail().length < 1) {
                        return self.showErrorMessage("Select NewsPaper Page Part");
                    }
                    if (!self.title() && !self.isTickerView()) {
                        return self.showErrorMessage("Enter Title ");
                    }
                    if (self.title().length > 200) {
                        return self.showErrorMessage("Title should be less then 200 charachters");
                    }
                    if (self.Slug().length > 200) {
                        return self.showErrorMessage("Slug should be less then 200 charachters");
                    }
                    if (self.videouploadcheck === 'submitpackage' && self.uploader().resources() <= 0) {
                        return self.showErrorMessage("Upload Video");
                    }

                    if (!self.selectedCategory() || !self.selectedCategory().id) {
                        return self.showErrorMessage("Please Provide Valid Category");
                    }

                    if (self.newsFilterType !== e.NewsFilterType.PublicReporter && (!self.selectedLocation() || !self.selectedLocation().id)) {
                        return self.showErrorMessage("Please Provide News Location");
                    }

                    if (self.newsFilterType == e.NewsFilterType.PublicReporter) {
                        locationIds = [1];
                    }

                    else {
                        locationIds = [self.selectedLocation().id];
                    }

                    var categoryIds = [self.selectedCategory().id];

                    var newsTagsArray = [];
                    for (var i = 0; i < self.newsTag().tags().length; i++) {
                        var item = {
                            _id: null, Tag: self.newsTag().tags()[i]
                        }
                        newsTagsArray.push(item);
                    }
                    if (!self.Slug() && appdata.currentUser().userType === e.UserType.FReporter && !self.isTickerView()) {
                        return self.showErrorMessage("Enter Slug");
                    }

                    else {
                        for (i = 0; i < self.multiSelectUsers().selectedItems().length; i++) {
                            self.cameraMenIds.push(self.multiSelectUsers().selectedItems()[i].id);
                        }
                    }

                    var entities = [];
                    for (var i = 0; i < self.newsOrg().tags().length; i++) {
                        var entity = self.newsOrg().tags()[i];
                        entities.push(entity);
                    }
                    var reporterIds = [];
                    for (var i = 0; i < self.multiSelectUsers().selectedItems().length; i++) {
                        reporterIds.push(self.multiSelectUsers().selectedItems()[i].id);
                    }

                    var eventResourceIds = [];

                    if (self.isDsng())
                        eventResourceIds.push(0);
                    if (self.isBeltPack())
                        eventResourceIds.push(1);
                    if (self.isEventNewsType())
                        self.newsFilterType = 21;

                    var event_coverage = null;
                    if (document.getElementById("toggle-on") && $("#toggle-on")[0].checked)
                        event_coverage = false;
                    if (document.getElementById("toggle-on") && $("#toggle-off")[0].checked)
                        event_coverage = true;


                    var otherReporter = parseInt(self.onBehalfReporterID());
                    var requestData = {

                        NewsFileId: self.id,
                        LanguageCode: self.language.langCode(),
                        title: self.title(),
                        description: router.currentHash() == config.views.fieldreporter.reportNews.url ? self.tvDescription() : self.description(),
                        newsDateStr: new Date(self.newsDateTime()).toISOString(),
                        categoryIds: categoryIds,
                        locationIds: locationIds,
                        tags: newsTagsArray,
                        Organizations: entities,
                        filterTypeId: self.newsFilterType,
                        reporterId: otherReporter == 0 ? appdata.userId : otherReporter,
                        newsTypeId: self.newsType,
                        resources: self.uploader().resources(),
                        referenceNewsId: self.referenceId,
                        ResourceEdit: self.resourceEdit,
                        channelId: self.channelId,
                        radioStationId: self.radioStationId,
                        newsPaperId: self.dailyNewsPaperId,
                        slug: self.Slug(),
                        highlight: self.Highlight(),
                        newsCameraMen: self.cameraMenIds,
                        tickerLines: self.tickerLines && self.tickerLines().length > 0 ? self.tickerLines() : null,
                        NewsPaperdescription: router.currentHash() == config.views.fieldreporter.reportNews.url ? self.npDescription() : self.newsPaperDescription(),
                        Tickers: self.tickerLines(),
                        currentTicker: self.currentTicker(),
                        Suggestions: self.suggestions(),
                        ProgramId: self.programId(),
                        AssignedTo: self.assignedTo(),
                        BureauId: appdata.locationId,
                        EventType: (self.selectedEvent() && self.selectedEvent().value) ? self.selectedEvent().value.trim() : null,
                        Coverage: event_coverage,
                        EventResourceId: eventResourceIds,
                        EventReporterId: reporterIds,
                        BureauId: appdata.locationId,
                        InsertedBy: appdata.userId

                    };

                    //if (appdata.currentUser().userType == e.UserType.Creporter) {
                    //    requestData.LanguageCode = 'ur';
                    //}

                    if (router.currentHash() === config.views.fieldreporter.updatenews.url || (router.currentHash() === '#/report-news' && appdata.currentUser().userType == e.UserType.Creporter) || (router.currentHash() === '#/report-news' && appdata.currentUser().userType == e.UserType.NpReporter)) {
                        requestData.description = self.description();
                    }
                    if (parseInt($.trim(helper.getCookie('role-id'))) == e.UserType.Producer || parseInt($.trim(helper.getCookie('role-id'))) == e.UserType.FReporter || parseInt($.trim(helper.getCookie('role-id'))) == e.UserType.HeadlineProducer || parseInt($.trim(helper.getCookie('role-id'))) == e.UserType.Creporter || parseInt($.trim(helper.getCookie('role-id'))) == e.UserType.TickerProducer || parseInt($.trim(helper.getCookie('role-id'))) == e.UserType.controllerInput || parseInt($.trim(helper.getCookie('role-id'))) == e.UserType.Controlleroutput) {
                        requestData.description = self.producerDescription();
                    }
                    if (appdata.currentUser().userType == e.UserType.NpReporter) {

                        for (var i = 0; i < self.NewsPaperDetail().length; i++) {
                            requestData.resources.push({
                                ResourceId: null, ResourceTypeId: 1, Guid: self.NewsPaperDetail()[i].ImageGuid
                            });
                        }
                    }

                    var info_Txt = 'News is being reported';
                    if (self.isEventNewsType())
                        info_Txt = 'Event is being added';

                    var info_reported = 'News Reported';

                    if (self.isEventNewsType())
                        info_reported = 'Event added';

                    config.logger.info(info_Txt);

                    var timeSpan = [self.mediaEditor.editedVideoLength()].clone();;


                    if (self.isNewsEdit()) {
                        requestData.BunchGuid = self.editNewsInfo().bunchId;
                        requestData.Guid = self.editNewsInfo().id;
                    }

                    self.isNewsEdit(false);
                    if (parseInt($.trim(helper.getCookie('role-id'))) == e.UserType.Producer || parseInt($.trim(helper.getCookie('role-id'))) == e.UserType.FReporter) {
                        if (window.parent && window.parent.document.location.hash == "#/home") {
                            requestData.isIframe = true;
                        }
                    }


                    $.when(manager.news.onsubmitNewsFile(requestData))
                            .done(function (responseData) {
                                if (responseData && responseData.IsSuccess) {
                                    self.isTickerClear(false);
                                    self.isTickerClear(true);
                                    self.editNewsInfo('');
                                    responseData.Data.Progress = self.uploader().uploadProgress;
                                    self.isLiveFeed(false);
                                    self.isRecordedFeed(false);
                                    self.isBeltPack(false);
                                    self.isDsng(false);
                                    self.multiSelectUsers().selectedItems([]);
                                    self.selectedEvent([]);


                                    var newsId = responseData.Data._id;
                                    latestNewsFileId = newsId;

                                    if (self.isEventNewsType()) {
                                        self.isEventNewsType(false);
                                        router.navigateTo(config.hashes.nmsBol.home);
                                    }


                                    if (!isNotAllowed) {


                                        //var resources = responseData.Data.Resources;
                                        //var newsId = responseData.Data._id;
                                        //self.id(newsId);
                                        //if (responseData.Data.ParentNewsId) {
                                        //    newsId = responseData.Data.ParentNewsId;
                                        //}


                                        //dc.resources.fillData(resources, { newsId: newsId });
                                        //var options = { sort: true, returnList: true };
                                        //if (self.parentNewsId) {
                                        //    options.groupByParentNewsId = true;
                                        //}

                                        //var news = dc.news.fillData([responseData.Data], options);
                                        //self.newsObject(news);
                                    }

                                    dc.news.clearAllNews(true);
                                    dc.resources.clearAllNews(true);

                                    var reqObj = {
                                        rid: appdata.userId, pageCount: 20, startIndex: 0
                                    };

                                    $.when(manager.news.LoadMyNewsList(reqObj))
                                    .done(function (responseData) {

                                        self.uploader().started(false);
                                        self.uploader().finished(false);
                                        self.uploader().src('');
                                        self.uploader().resources([]);
                                        self.uploader().uploadedResources([]);

                                        if (!isNotAllowed) {
                                            if (self.newsFilterType == e.NewsFilterType.Channel || self.newsFilterType == e.NewsFilterType.Radio) {
                                                news[0].totalTimeSpan = timeSpan[0];
                                            }
                                        }
                                        //if (self.isTeamB()) {
                                        //    var newsId = responseData.Data._id;
                                        //    if (responseData.Data.ParentNewsId) {
                                        //        newsId = responseData.Data.ParentNewsId;
                                        //    }

                                        //    dc.resources.fillData(resources, { newsId: newsId });
                                        //    var options = { sort: true, returnList: true };
                                        //    if (self.parentNewsId) {
                                        //        options.groupByParentNewsId = true;
                                        //    }
                                        //    var news = dc.news.fillData([responseData.Data], options);
                                        //    $(parent.window.document.getElementById('hiddenData')).html(JSON.stringify(responseData.Data));
                                        //    self.newsObject(news);
                                        //    self.newsList.push(news[0]);
                                        //}

                                        config.logger.success(info_reported);

                                        self.isEventNewsType(false);

                                        if (callback)
                                            callback(latestNewsFileId);

                                        self.clearViewModel();



                                    });



                                    $(parent.window.document.getElementById('hiddenData')).html(JSON.stringify(responseData.Data));
                                    if (parent.window && parent.window.document && $(parent.window.document.getElementById('createNewsPopup')).hasClass('isNews')) {
                                        setTimeout(function () {
                                            $(parent.window.document.getElementById('createNewsPopup')).removeClass('isNews');
                                            $(parent.window.document.getElementById('closeNewsPopUp')).click();
                                            $(parent.window.document.getElementById('hiddenData')).click();
                                            window.parent.postMessage($(parent.window.document.getElementById('hiddenData')).html(), "*");
                                        }, 5000);

                                    }
                                } else {
                                    if (responseData.Errors && responseData.Errors[0].indexOf('Duplicate') > -1) {
                                        config.logger.success("News Updated.");
                                    }
                                    else {
                                        config.logger.error("Error occured.");
                                        self.resetViewModel();
                                    }
                                }
                            })
                            .fail(function () {
                                config.logger.error("Error occured: Your internet connection seems down.");
                                self.resetViewModel();

                            });

                    self.clearViewModel();
                    $('.reportNewsChannelReporter .jqte_tool_22').hide();
                },

                self.isNullo = false;

                return self;
            };

        ReportNews.Nullo = new ReportNews();

        ReportNews.Nullo.isNullo = true;

        ReportNews.datacontext = function (dc) {
            if (dc) { _dc = dc; }
            return _dc;
        };

        return ReportNews;
    });