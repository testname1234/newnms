﻿define('model.resource',
    ['enum', 'config', 'string', 'appdata'],
    function (e, config, S, appdata) {

        var
            _dc = this,

            Resource = function () {

                var self = this;

                self.id,
				self.guid,
                self.newsId,
                self.newsFileId
                self.storyScreenTemplateResourceId,
                self.storyTemplateScreenElementResourceId,
                self.storyScreenTemplateId,
                self.creationDateStr,
                self.type,
                self.location = ko.observable(''),
                self.category = ko.observable(''),
                self.captions = ko.observable(''),
                self.isMetaWithData = ko.observable(false),
                self.isLatest,
                self.comment = ko.observable('').extend({               
                    maxLength: 500, 
                }),
                self.lastUpdateDate = ko.observable(),
               
                self.creationDate,
                self.isSelected = ko.observable(false),
                self.duration,
                self.storyTemplateScreenElementId,
                self.croppedImageguid = ko.observable(),
                self.searchterm = ko.observable(),
                self.isArchival = ko.observable(false),
                self.description,
                self.isGoogleResource=ko.observable(false),
                self.fileTitle,
                self.resourceDate = ko.observable(),
                self.caption,
                self.durationDisplay = ko.observable(),
                self.creationDateDisplay = ko.observable(),
                self.slug,
                self.directUlr = ko.observable(''),
                self.directThumbUrl = ko.observable(''),
                self.highResDownloadUrl,
                self.lowResDownloadUrl,
                self.resourceMetas = ko.observableArray([]),
                self.isMarkedFavourite = ko.observable(false),
                self.lastUpdateDateDisplay = ko.computed({
                    read: function () {
                           return moment(self.lastUpdateDate()).fromNow();
                    },
                    deferEvaluation: true
                }),
                self.resourceMetasDisplay = ko.computed({
                    read: function () {
                        var metas = self.resourceMetas();
                        var temp = [];

                        for (var i = 0, len = metas.length ; i < len; i++) {
                            temp.push(metas[i].Name);
                        }
                        return temp.join(' , ');
                    },
                    deferEvaluation:true
                }),
                
                self.cssClass = ko.computed({
                    read: function () {
                        switch (self.type) {
                            case e.ContentType.Image:
                                return 'image';
                                break;
                            case e.ContentType.Audio:
                                return 'audio';
                                break;
                            case e.ContentType.Video:
                                return 'video';
                                break;
                            case e.ContentType.Document:
                                return 'document';
                                break;
                            default:
                                return '';
                                break;
                        }
                    },
                    deferEvaluation: true
                }),
                self.resourceDateDisplay = ko.computed({
                    read: function () {
                        if (self.resourceDate())
                        return moment(self.resourceDate()).format('Do MMMM YYYY');
                    },
                    deferEvaluation: true
                }),
                self.thumbImageUrl = ko.computed({
                    read: function () {
                       
                        //if (self.croppedImageguid() )
                        //    return config.MediaServerUrl + 'getthumb/' + self.croppedImageguid();
                        if (self.guid)
                            return config.MediaServerUrl + 'getthumb/' + self.guid + "?UserId=" + appdata.currentUser().id;

                    },
                    deferEvaluation: true
                }),

                self.embededUrl = ko.computed({
                    read: function () {
                        self.directUlr();
                        if (self.directUlr()) {
                            return "http://www.dailymotion.com/embed/video/" + self.guid + "?autoPlay=1&loadRelatedInPlace=1";
                        }

                    },
                    deferEvaluation: true
                }),
                self.url = ko.computed({
                    read: function () {
                        

                            if (self.croppedImageguid())
                                return config.MediaServerUrl + 'getresource/' + self.croppedImageguid() + "?UserId=" + appdata.currentUser().id;
                            else if (appdata.currentUser().id) {
                                return config.MediaServerUrl + 'getresource/' + self.guid + "?UserId=" + appdata.currentUser().id;
                            } else {
                                return config.MediaServerUrl + 'getresource/' + self.guid + "?UserId=" + rAppdata.currentUser().userId;
                            }
                                
                      
                    },
                    deferEvaluation: true
                }),
                self.setVariable = function () {
                    if (self.duration > 0) {
                        var tempDuration = moment.duration(self.duration, 'seconds');
                        tempDuration = S(tempDuration.hours()).padLeft(2, '0') + ':' + S(tempDuration.minutes()).padLeft(2, '0') + ':' + S(tempDuration.seconds()).padLeft(2, '0');
                        self.durationDisplay(tempDuration);
                    }
                    self.creationDateDisplay(moment(self.creationDate).format('Do MMMM YYYY'));


                };
                self.isNullo = false;

                return self;
            }

        Resource.Nullo = new Resource();

        Resource.Nullo.isNullo = true;

        Resource.datacontext = function (dc) {
            if (dc) { _dc = dc; }
            return _dc;
        };

        return Resource;
    });