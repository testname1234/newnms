﻿define('model.rostory',
    ['ko', 'underscore'],
    function (ko, _) {

        var
            _dc = this,

            ROStory = function () {

                var self = this;

                self.id,
                self.roId,
                self.orderId,
                self.slug=ko.observable(),
                self.isPlaying = ko.observable(false),
                self.isPlayed = ko.observable(false),
                self.isSkipped = ko.observable(false),
                self.unSkipped = ko.observable(true),

                self.storyItems = ko.observableArray([]),

                self.extractData = function () {
                    self.storyItems = _dc.storyitem.getROStoryItemsByStoryId(self.id);
                },


                self.isNullo = false;

                self.fillObservableProperties = function () {

                };

                return self;
            }

        ROStory.Nullo = new ROStory();

        ROStory.Nullo.isNullo = true;

        // static member
        ROStory.datacontext = function (dc) {
            if (dc) { _dc = dc; }
            return _dc;
        };

        return ROStory;
    });