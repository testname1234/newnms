﻿define('model.runorder',
    ['ko', 'underscore'],
    function (ko, _) {

        var
            _dc = this,

            RunOrder = function () {

                var self = this;

                self.id,
                self.slug = ko.observable(),
                self.roStartTime,
                self.roStories = ko.observableArray([]),
               

                
                self.extractData = function () {
                    self.roStories = _dc.rostory.getROStoriesByRunOrderId(self.id);
                },


                self.isNullo = false;

                self.fillObservableProperties = function () {
                   
                };

                return self;
            }

        RunOrder.Nullo = new RunOrder();

        RunOrder.Nullo.isNullo = true;

        // static member
        RunOrder.datacontext = function (dc) {
            if (dc) { _dc = dc; }
            return _dc;
        };

        return RunOrder;
    });