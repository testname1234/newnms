﻿define('model.screenelement',
    ['ko', 'config'],
    function (ko, config) {

        var
            _dc = this,

            ScreenElement = function () {

                var self = this;

                self.id,
				self.name,
                self.thumbGuid = ko.observable(),
				self.dragIconUrl,
				self.cssClass,
				self.isAllowed = ko.observable(false),
             
                self.iconUrl = ko.computed({
                    read: function () {
                        return config.MediaServerUrl + 'getresource/' + self.thumbGuid();
                    },
                    deferEvaluation: true
                }),

                self.isNullo = false;

                return self;
            }

        ScreenElement.Nullo = new ScreenElement();

        ScreenElement.Nullo.isNullo = true;

        ScreenElement.datacontext = function (dc) {
            if (dc) { _dc = dc; }
            return _dc;
        };

        return ScreenElement;
    });