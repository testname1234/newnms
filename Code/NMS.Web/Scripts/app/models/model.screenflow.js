﻿define('model.screenflow',
    ['utils'],
    function (utils) {

        var
            _dc = this,

            ScreenFlow = function () {

                var self = this;

                self.id,
                self.isDirty = false,
                self.screenTemplates = ko.observableArray(),

                self.screenTemplatesDisplay = ko.computed({
                    read: function () {
                        var arr = self.screenTemplates();
                        arr = arr.sort(function (entity1, entity2) {
                            return utils.sortNumeric(entity1.sequenceId, entity2.sequenceId, 'asc');
                        });

                        return arr;
                    },
                    deferEvaluation: true
                }),

                self.screenTemplateCount = ko.computed({
                    read: function () {
                        if (self.screenTemplates()) {
                            return self.screenTemplates().length;
                        } else {
                            return 0;
                        }
                    },
                    deferEvaluation: true
                }),

                self.canApplyCycle = ko.computed({
                    read: function () {
                        return self.screenTemplateCount() > 5;
                    },
                    deferEvaluation: true
                }),

                self.contentDuration = ko.computed({
                    read: function () {
                        var tempDuration = 0;
                        for (var i = 0; i < self.screenTemplates().length; i++) {
                            tempDuration = tempDuration + self.screenTemplates()[i].contentDuration();
                        }
                        return tempDuration;
                    },
                    deferEvaluation: false
                }),

                self.contentDurationDisplay = ko.computed({
                    read: function () {
                        return utils.getDuration(self.contentDuration(), 'second');
                    },
                    deferEvaluation: false
                }),
                self.totalBucketLength = ko.computed({
                    read: function () {
                        var count = 0;
                        for (var i = 0; i < self.screenTemplates().length; i++) {
                            count += self.screenTemplates()[i].contentBucket().length;
                        }
                        return count;
                    },
                    deferEvaluation: true
                }),

                self.isNullo = false;

                return self;
            };

        ScreenFlow.Nullo = new ScreenFlow();

        ScreenFlow.Nullo.isNullo = true;

        ScreenFlow.datacontext = function (dc) {
            if (dc) { _dc = dc; }
            return _dc;
        };

        return ScreenFlow;
    });