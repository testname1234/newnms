﻿define('model.screentemplate',
	['ko', 'string', 'moment', 'config', 'enum'],
	function (ko, S, moment, config, e) {
	    var
			_dc = this,

			ScreenTemplate = function () {

			    var self = this;

			    self.id,
				self.key,
				self.storyScreenTemplateId,
                self.uniqueId = ko.observable(''),
				self.elements,
				self.name,
				self.description,
				self.thumbGuid = ko.observable(),
				self.isSelected = ko.observable(),
				self.sequenceId,
				self.isAllowed,
                self.templateKeys = ko.observableArray([]),
				self.script = ko.observable(''),
				self.templateBackground,
				self.templateDisplayImage = ko.observable(''),
				self.templateElements = ko.observableArray([]),
				self.contentBucket = ko.observableArray([]),
				self.isAssignToStoryWriter = ko.observable(false),
                self.isAnchorOnlyTemplate=ko.observable(false),
				self.isAssignToNLE = ko.observable(false),
                self.isMediaTemplate=ko.observable(false),
				self.mics = ko.observableArray([]),
				self.cameras = ko.observableArray([]),
				self.resources = ko.observableArray([]),
                self.videoDuration = ko.observable(0),
                self.faceAction,
				self.lastUpdateDate = ko.observable(),
                self.programId,
                self.isFreshImageGenerated = false,
                self.isMediaWallTemplate = ko.observable(false),
                self.parentId,
                self.isGuestSelected = ko.observable(false),
                self.templateDefaultImageUrl = ko.observable(),
                self.statusId = ko.observable(e.ScreenTemplateStatus.InProcess),
                self.nleStatusId=ko.observable(e.ScreenTemplateStatus.InProcess),
			    self.storywriterStatusId=ko.observable(e.ScreenTemplateStatus.InProcess),


                self.refreshTemplateStatus = ko.observable(false),
				self.thumbImageUrl = ko.computed({
				    read: function () {
				        return config.MediaServerUrl + 'getthumb/' + self.thumbGuid();
				    },
				    deferEvaluation: true
				}),

                self.isMediaTypeTemplate = ko.computed({
                    read: function () {
                        self.templateElements();
                        if (self.templateElements && self.templateElements().length > 0) {
                            for (var i = 0; i < self.templateElements().length; i++) {
                                if (self.templateElements()[i].screenElementId == e.ScreenElementType.Video || self.templateElements()[i].screenElementId == e.ScreenElementType.Picture) {
                                    self.isAssignToNLE(true);
                                    break;
                                }
                            }
                        }
                    },
                    deferEvaluation: false
                }),
				self.backgroundUrl = ko.computed({
				    read: function () {
				        if (self.templateDisplayImage().length > 0) {
				            return self.templateDisplayImage();
				        } else {
				            return config.MediaServerUrl + 'getresource/' + self.thumbGuid();
				        }
				    },
				    deferEvaluation: true
				}),

				self.relatedTemplatesCount = ko.computed({
				    read: function () {
				        return self.relatedTemplates().length;
				    },
				    deferEvaluation: true
				}),

				self.relatedTemplates = ko.computed({
				    read: function () {
				        var arr = _.filter(_dc.screenTemplates.getObservableList(), function (template) {
				            return template.key === self.key && template.id !== self.id && template.isMediaWallTemplate() === self.isMediaWallTemplate();
				        });
				        return arr;
				    },
				    deferEvaluation: true
				}),

                self.videoDurationDisplay = ko.computed({
                    read: function () {
                        var tempDuration = moment.duration(self.videoDuration(), 'seconds');
                        return S(tempDuration.hours()).padLeft(2, '0') + ':' + S(tempDuration.minutes()).padLeft(2, '0') + ':' + S(tempDuration.seconds()).padLeft(2, '0');
                    },
                    deferEvaluation: true
                }),

				self.scriptDuration = ko.computed({
				    read: function () {
				        var wordCount = self.script().length > 0 && self.script().split(' ').length > 0 ? self.script().split(' ').length - 1 : self.script().length > 0 ? self.script() : 0;
				        return moment.duration(wordCount / 75, 'minute');
				    },
				    deferEvaluation: true
				}),

				self.scriptDurationDisplay = ko.computed({
				    read: function () {
				        var wordCount = self.script().length > 0 && self.script().split(' ').length > 0 ? self.script().split(' ').length - 1 : self.script().length > 0 ? self.script() : 0;
				        var tempDuration = moment.duration(wordCount / 75, 'minute');
				        return S(tempDuration.hours()).padLeft(2, '0') + ':' + S(tempDuration.minutes()).padLeft(2, '0') + ':' + S(tempDuration.seconds()).padLeft(2, '0');
				    },
				    deferEvaluation: true
				}),

				self.contentDuration = ko.computed({
				    read: function () {
				        var tempDuration = self.scriptDuration();
				        if (tempDuration.asSeconds() > self.videoDuration()) {
				        }
				        else {
				            tempDuration = moment.duration(self.videoDuration(), 'seconds');
				        }
				        return tempDuration.asSeconds()
				    },
				    deferEvaluation: false
				}),

                self.contentDurationDisplay = ko.computed({
                    read: function () {
                        var wordCount = self.script().length > 0 && self.script().split(' ').length > 0 ? self.script().split(' ').length - 1 : self.script().length > 0 ? self.script() : 0;
                        var tempDuration = moment.duration(wordCount / 75, 'minute');
                        if (tempDuration.asSeconds() > self.videoDuration()) {
                        }
                        else {
                            tempDuration = moment.duration(self.videoDuration(), 'seconds');
                        }
                        return S(tempDuration.hours()).padLeft(2, '0') + ':' + S(tempDuration.minutes()).padLeft(2, '0') + ':' + S(tempDuration.seconds()).padLeft(2, '0');
                    },
                    deferEvaluation: false
                }),
             
                self.uid = ko.computed({
                    read: function () {
                        if (self.storyScreenTemplateId)
                            return self.storyScreenTemplateId;
                        else if (self.uniqueId())
                            return self.uniqueId();
                        else {
                            self.uniqueId(_.uniqueId('ST_'));
                            return self.uniqueId();
                        }
                    },
                    deferEvaluation: true
                });

			    self.extractData = function () {
			        try {
			            var tempArr = self.key.split('_');
			            tempArr.splice(0, 1);
			            self.elements = tempArr;
			            self.cameras = _dc.screenTemplateCameras.getCamerasByScreenTemplateId(self.storyScreenTemplateId);
			            self.mics = _dc.screenTemplateMics.getMicsByScreenTemplateId(self.storyScreenTemplateId);
			            self.resources = _dc.resources.getResourceByScreenTemplateId(self.storyScreenTemplateId);
			        } catch (e) {
			            //console.log(e.message);
			        }
			    },

				self.isNullo = false;

			    return self;
			}

	    ScreenTemplate.Nullo = new ScreenTemplate();

	    ScreenTemplate.Nullo.isNullo = true;

	    ScreenTemplate.datacontext = function (dc) {
	        if (dc) { _dc = dc; }
	        return _dc;
	    };

	    return ScreenTemplate;
	});