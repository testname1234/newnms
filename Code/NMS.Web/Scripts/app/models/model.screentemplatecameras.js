﻿define('model.screentemplatecameras',
    [],
    function () {

        var
            _dc = this,

          ScreenTemplateCameras = function () {

                var self = this;
                self.id,
                self.storyScreenTemplateId,
                self.cameraTypeId,
                self.name,
                self.isOn,
                self.creationDateStr,
                self.lastUpdateDateStr,

                self.isNullo = false;

                return self;
            }

        ScreenTemplateCameras.Nullo = new ScreenTemplateCameras();

        ScreenTemplateCameras.Nullo.isNullo = true;

        // static member
        ScreenTemplateCameras.datacontext = function (dc) {
            if (dc) { _dc = dc; }
            return _dc;
        };

        return ScreenTemplateCameras;
    });