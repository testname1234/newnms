﻿define('model.screentemplatedesignercontrol',
    ['underscore', 'ko', 'enum', 'model.screentemplate', 'model.templateelement', 'model.resource', 'config', 'appdata', 'utils'],
    function (_, ko, e, ScreenTemplate, TemplateElement, Resource, config, appdata, utils) {
        var
            _dc = this,
            logger = config.logger,

            ScreenTemplateDesignerControl = function () {

                var self = this;

                self.templateIndex = ko.observable(),
                self.currentTemplate = ko.observable(new ScreenTemplate()),
                self.currentTemplateElement = ko.observable(new TemplateElement()),
                self.isGalleryVisible = ko.observable(false),
                self.isResourceCommentBoxVisible = ko.observable(false),
                self.isTemplateKeyPopupVisible = ko.observable(false),
                self.isTemplateSelected = ko.observable(false),
                self.isMediaUploaderVisible = ko.observable(false),
                self.isTemplateCommentBoxVisible = ko.observable(false),
                self.isTemplateKeyVisible = ko.observable(false),
                self.currentBucketData = ko.observable(),
                self.currentResource = ko.observable(new Resource()),
                self.filteredCommets = ko.observableArray([]),
                self.isChatPopUpVisible = ko.observable(false),
                self.isProducerChat = ko.observable(false),
                self.isNleChat = ko.observable(false),
                self.isStorywriterChat = ko.observable(false),
                self.nleMessageCount = ko.observable(0),
                self.storyWriterMessageCount = ko.observable(0),

                self.mediaWallTemplatePool = ko.observableArray([]),
                self.mediaWallTemplateIndex = ko.observable(0),
                self.isMediaWallVisible = ko.observable(false),
                self.lastSelectedTemplateIndex = 0,

                self.comments = ko.computed({
                    read: function () {
                        self.markCommetIsRead();
                        if (appdata.currentUser().userType == e.UserType.StoryWriter) {
                            var arr = _.filter(_dc.comments.getObservableList(), function (comment) {
                                if (comment.storyScreenTemplateId === self.currentTemplate().storyScreenTemplateId && (comment.fromId == appdata.currentUser().id || comment.toId == appdata.currentUser().id))
                                    return comment;
                            });
                        }
                        else if (appdata.currentUser().userType == e.UserType.NLE) {
                            var arr = _.filter(_dc.comments.getObservableList(), function (comment) {
                                if (comment.storyScreenTemplateId === self.currentTemplate().storyScreenTemplateId && (comment.fromId == appdata.currentUser().id || comment.toId == appdata.currentUser().id))
                                    return comment;
                            });
                        }
                        else if (self.isNleChat() && appdata.currentUser().userType == e.UserType.Producer) {
                            var users = [];
                            _.filter(_dc.users.getObservableList(), function (user) {
                                _.filter(user.userRoles(), function (role) {
                                    if (role.WorkRoleId === e.UserType.NLE)
                                        users.push(user);
                                });
                            });
                            var arr = [];
                            for (var i = 0; i < users.length; i++) {
                                for (var k = 0; k < _dc.comments.getObservableList().length; k++) {
                                    if (_dc.comments.getObservableList()[k].storyScreenTemplateId === self.currentTemplate().storyScreenTemplateId && ((_dc.comments.getObservableList()[k].fromId == appdata.currentUser().id && _dc.comments.getObservableList()[k].toId == users[i].id) || (_dc.comments.getObservableList()[k].fromId == users[i].id && _dc.comments.getObservableList()[k].toId == appdata.currentUser().id))) {
                                        arr.push(_dc.comments.getObservableList()[k]);
                                    }
                                    if (_dc.comments.getObservableList()[k].storyScreenTemplateId === self.currentTemplate().storyScreenTemplateId && _dc.comments.getObservableList()[k].fromId == users[i].id && _dc.comments.getObservableList()[k].toId == appdata.currentUser().id) {
                                        _dc.comments.getObservableList()[k].isRead(true);
                                        _dc.comments.getObservableList()[k].sendFromNle(true);
                                    }
                                }
                            }
                        }
                        else if (self.isStorywriterChat() && appdata.currentUser().userType == e.UserType.Producer) {

                            var users = [];
                            _.filter(_dc.users.getObservableList(), function (user) {
                                _.filter(user.userRoles(), function (role) {
                                    if (role.WorkRoleId === e.UserType.StoryWriter)
                                        users.push(user);
                                });
                            });
                            var arr = [];
                            for (var i = 0; i < users.length; i++) {
                                for (var k = 0; k < _dc.comments.getObservableList().length; k++) {
                                    if (_dc.comments.getObservableList()[k].storyScreenTemplateId === self.currentTemplate().storyScreenTemplateId && ((_dc.comments.getObservableList()[k].fromId == appdata.currentUser().id && _dc.comments.getObservableList()[k].toId == users[i].id) || (_dc.comments.getObservableList()[k].fromId == users[i].id && _dc.comments.getObservableList()[k].toId == appdata.currentUser().id))) {
                                        arr.push(_dc.comments.getObservableList()[k]);
                                    }
                                    if (_dc.comments.getObservableList()[k].storyScreenTemplateId === self.currentTemplate().storyScreenTemplateId && _dc.comments.getObservableList()[k].fromId == users[i].id && _dc.comments.getObservableList()[k].toId == appdata.currentUser().id) {
                                        _dc.comments.getObservableList()[k].isRead(true);
                                        _dc.comments.getObservableList()[k].sendFromStoryWriter(true);
                                    }
                                }
                            }
                        }
                        else {
                            var arr = _.filter(_dc.comments.getObservableList(), function (comment) {
                                return comment.storyScreenTemplateId === self.currentTemplate().storyScreenTemplateId;
                            });
                        }

                        arr = arr.sort(function (entity1, entity2) {
                            return utils.sortDateTime(entity1.creationDate, entity2.creationDate, 'desc');
                        });

                        self.commentsCounter();
                        return arr;
                    },
                    deferEvaluation: true
                }),
                self.currenTemplateDisplay = ko.computed({
                    read: function () {
                        if (self.currentTemplate())
                            return self.currentTemplate();
                    },
                    deferEvaluation: true
                }),
                self.computeFlags = ko.computed({
                    read: function () {
                        self.resetFlags();
                        self.currentTemplate()

                        for (var i = 0; i < self.currentTemplate().templateElements().length; i++) {
                            var element = self.currentTemplate().templateElements()[i];

                            if (element.screenElementId === e.ScreenElementType.Video ||
                               element.screenElementId === e.ScreenElementType.Graphic ||
                               element.screenElementId === e.ScreenElementType.MediaWall ||
                               element.screenElementId === e.ScreenElementType.Picture) {
                                self.isGalleryVisible(false);
                            }
                        }
                    },
                    deferEvaluation: true
                }),
                self.mediaWallExist = ko.computed({
                    read: function () {
                        //self.currentTemplate();
                        var arr = _dc.screenTemplates.getObservableList();

                        for (var i = 0; i < arr.length; i++) {
                            if (arr[i].isMediaWallTemplate()) {
                                return true;
                            }
                        }

                        return false;
                    },
                    deferEvaluation: true
                }),

                self.markCommetIsRead = function () {
                    if (self.isChatPopUpVisible() && appdata.currentUser().userType == e.UserType.StoryWriter) {
                        _.filter(_dc.comments.getObservableList(), function (comment) {
                            if (comment.storyScreenTemplateId === self.currentTemplate().storyScreenTemplateId && comment.toId == appdata.currentUser().id)
                                comment.isRead(true);
                        });
                    }
                    else if (self.isChatPopUpVisible() && appdata.currentUser().userType == e.UserType.NLE) {
                        _.filter(_dc.comments.getObservableList(), function (comment) {
                            if (comment.storyScreenTemplateId === self.currentTemplate().storyScreenTemplateId && comment.toId == appdata.currentUser().id)
                                comment.isRead(true);
                        });
                    }
                    else if (!self.isChatPopUpVisible() && appdata.currentUser().userType == e.UserType.Producer) {
                        var storyusers = [];
                        var nleusers = [];
                        _.filter(_dc.users.getObservableList(), function (user) {
                            _.filter(user.userRoles(), function (role) {
                                if (role.WorkRoleId === e.UserType.StoryWriter)
                                    storyusers.push(user);
                            });
                        });
                        _.filter(_dc.users.getObservableList(), function (user) {
                            _.filter(user.userRoles(), function (role) {
                                if (role.WorkRoleId === e.UserType.NLE)
                                    nleusers.push(user);
                            });
                        });

                        for (var i = 0; i < storyusers.length; i++) {
                            for (var k = 0; k < _dc.comments.getObservableList().length; k++) {
                                if (_dc.comments.getObservableList()[k].storyScreenTemplateId === self.currentTemplate().storyScreenTemplateId && _dc.comments.getObservableList()[k].fromId == storyusers[i].id && _dc.comments.getObservableList()[k].toId == appdata.currentUser().id) {
                                    _dc.comments.getObservableList()[k].sendFromStoryWriter(true);
                                }
                            }
                        }

                        for (var i = 0; i < nleusers.length; i++) {
                            for (var k = 0; k < _dc.comments.getObservableList().length; k++) {
                                if (_dc.comments.getObservableList()[k].storyScreenTemplateId === self.currentTemplate().storyScreenTemplateId && _dc.comments.getObservableList()[k].fromId == nleusers[i].id && _dc.comments.getObservableList()[k].toId == appdata.currentUser().id) {
                                    _dc.comments.getObservableList()[k].sendFromNle(true);
                                }
                            }
                        }
                    }
                },
                self.commentsCounter = function () {
                    if (appdata.currentUser().userType == e.UserType.StoryWriter) {
                        var arr = _.filter(_dc.comments.getObservableList(), function (comment) {
                            if (comment.storyScreenTemplateId === self.currentTemplate().storyScreenTemplateId && comment.toId == appdata.currentUser().id && !comment.isRead())
                                return comment;
                        });
                        self.storyWriterMessageCount(arr.length);

                    }
                    else if (appdata.currentUser().userType == e.UserType.NLE) {
                        var arr = _.filter(_dc.comments.getObservableList(), function (comment) {
                            if (comment.storyScreenTemplateId === self.currentTemplate().storyScreenTemplateId && comment.toId == appdata.currentUser().id && !comment.isRead())
                                return comment;
                        });
                        self.nleMessageCount(arr.length);
                    }
                    else if (appdata.currentUser().userType == e.UserType.Producer) {
                        var tempObjnle = [];
                        var tempObjstory = [];
                        _.filter(_dc.comments.getObservableList(), function (comment) {
                            if (comment.storyScreenTemplateId === self.currentTemplate().storyScreenTemplateId && comment.toId == appdata.currentUser().id && comment.sendFromNle() && !comment.isRead()) {
                                tempObjnle.push(comment);
                            }
                            else if (comment.storyScreenTemplateId === self.currentTemplate().storyScreenTemplateId && comment.toId == appdata.currentUser().id && comment.sendFromStoryWriter() && !comment.isRead()) {
                                tempObjstory.push(comment);
                                self.storyWriterMessageCount(tempObjnle.length);
                            }
                            self.nleMessageCount(tempObjnle.length);
                        });

                    }
                },
                self.populateMediaWallTemplates = function () {
                    self.lastSelectedTemplateIndex = self.templateIndex();
                    self.mediaWallTemplatePool([]);
                    var arr = _.filter(_dc.screenTemplates.getObservableList(), function (template) {
                        
                        return template.isMediaWallTemplate();
                    });

                    self.mediaWallTemplatePool(arr);
                    self.mediaWallTemplateIndex(0);
                },
                self.removeContentFromBucket = function (data) {
                    if (self.currentTemplate().contentBucket()) {
                        for (var i = 0; i < self.currentTemplate().contentBucket().length; i++) {
                            if (self.currentTemplate().contentBucket()[i].id === data.id) {
                                self.currentTemplate().contentBucket.remove(self.currentTemplate().contentBucket()[i]);
                                logger.success("Successfully deleted!");
                                break;
                            }
                        }
                        if (self.isMediaWallVisible()) {
                            self.currentTemplateElement().storyTemplateScreenElementResources(self.currentTemplate().contentBucket());
                        }
                    }


                },
                
                self.toggleCommentBox = function (data) {
                    if (data) {
                        self.currentBucketData(data);
                        if (self.isResourceCommentBoxVisible())
                            self.isResourceCommentBoxVisible(false);
                        else {
                            self.currentResource(data);
                            self.isResourceCommentBoxVisible(true);
                        }
                    }
                },
                self.toggleAssignmentFlag = function (flagType) {
                    if (flagType == 'storywriter') {
                        self.currentTemplate().isAssignToStoryWriter(!self.currentTemplate().isAssignToStoryWriter());
                    } else {
                        self.currentTemplate().isAssignToNLE(!self.currentTemplate().isAssignToNLE());
                    }
                },
                self.toggleTemplateKeyPopup = function () {
                    for (var i = 0; i < self.currentTemplate().templateKeys().length; i++) {
                        if (self.currentTemplate().templateKeys()[i].languageCode()) {
                            if (self.currentTemplate().templateKeys()[i].languageCode() == 'ur')
                                $("#RadioUr" + i).click();
                            else
                                $("#RadioEn" + i).click();
                        }
                        else {
                            self.currentTemplate().templateKeys()[i].languageCode('ur');
                            $("#RadioUr" + i).click();
                        }
                    }
                    self.isTemplateKeyPopupVisible(!self.isTemplateKeyPopupVisible());
                },
                self.resetFlags = function () {
                    self.isGalleryVisible(false);
                    self.isResourceCommentBoxVisible(false);
                },
                self.reset = function () {
                    self.currentTemplate(new ScreenTemplate());
                    self.currentTemplateElement(new TemplateElement());
                    self.isGalleryVisible(false);
                    self.isResourceCommentBoxVisible(false);
                    self.isTemplateSelected(false);
                    self.isTemplateCommentBoxVisible(false);
                    self.isMediaUploaderVisible(false);
                },

                self.isNullo = false;

                return self;
            }

        ScreenTemplateDesignerControl.Nullo = new ScreenTemplateDesignerControl();

        ScreenTemplateDesignerControl.Nullo.isNullo = true;

        ScreenTemplateDesignerControl.datacontext = function (dc) {
            if (dc) { _dc = dc; }
            return _dc;
        };

        return ScreenTemplateDesignerControl;
    });