﻿define('model.screentemplatemics',
    ['moment'],
    function (moment) {

        var
            _dc = this,

          ScreenTemplateMics = function () {

                var self = this;
                self.id,
                self.storyScreenTemplateId,
                self.micNo,
                self.isOn,
                self.creationDateStr,
                self.lastUpdateDateStr,

                self.isNullo = false;

                return self;
            }

        ScreenTemplateMics.Nullo = new ScreenTemplateMics();

        ScreenTemplateMics.Nullo.isNullo = true;

        // static member
        ScreenTemplateMics.datacontext = function (dc) {
            if (dc) { _dc = dc; }
            return _dc;
        };

        return ScreenTemplateMics;
    });