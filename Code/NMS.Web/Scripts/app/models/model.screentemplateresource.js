﻿define('model.screentemplateresource',
    [],
    function () {

        var
            _dc = this,

          ScreenTemplateResource = function () {

              var self = this;
              self.id,
              self.slotScreenTemplateId,
              self.resourceGuid,
              self.CreationDateStr,

              self.isNullo = false;

              return self;
          }

        ScreenTemplateResource.Nullo = new ScreenTemplateResource();

        ScreenTemplateResource.Nullo.isNullo = true;

        // static member
        ScreenTemplateResource.datacontext = function (dc) {
            if (dc) { _dc = dc; }
            return _dc;
        };

        return ScreenTemplateResource;
    });