﻿define('model.shortnews', ['ko', 'control.uploader'],
    function (ko, uploader, mapper) {

        var shortNews = function () {
            var self = this;

            self.id = ko.observable('');
            self.isVerified = ko.observable(false);
            self.title = ko.observable('');
            self.defaultImage = ko.observable(''),
            self.description = ko.observable('');
            self.language = '';
            self.selectedLanguage = ko.observable('EN'),
            self.newsDate = '';            
            self.uploader = ko.observable(new uploader()),
            self.selectedCategory = ko.observable({}),
            self.selectedLocation = ko.observable({});
            
            self.newsTag = ko.observableArray();
            self.languageToggle = ko.observable();
            self.clear = function () { };
            self.singleInputText = '';
            self.resources = ko.observableArray([]);
            self.newsComments = ko.observable('');
            self.newsDateTime = ko.observable('');
            self.updates = ko.observableArray([]);

            self.newsFilterType;
            self.newsTypeId;
            self.ReporterId;

            self.categories = ko.computed({
                read: function () {
                    if (self.selectedCategory() && self.selectedCategory().id) {
                        return self.selectedCategory().name;
                    }
                    return '';
                },
                deferEvaluation: true

            }),

            self.locations = ko.computed({
                read: function () {
                    if (self.selectedLocation() && self.selectedLocation().id) {
                        return self.selectedLocation().name;
                    }
                    return '';
                },
                deferEvaluation: true

            });
        };
        return shortNews;
    })