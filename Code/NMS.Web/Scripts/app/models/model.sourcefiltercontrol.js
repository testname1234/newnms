﻿define('model.sourcefiltercontrol',
    ['ko', 'enum', 'utils', 'model.program', 'model.filter', 'underscore', 'model.bunch', 'appdata'],
    function (ko, e, utils, Program, Filter, _, Bunch, appdata) {

        var
            _dc = this,

            SourceFilterControl = function () {

                var self = this;

                self.id,
                self.settingsActiveTab = ko.observable(-1),
                self.currentFilter = ko.observable(),
                self.selectedFilterIds = ko.observableArray([]),
                self.isPopulated = ko.observable(0),
                self.showExtraFilters = ko.observable(false),
                self.isMediaSectionVisible = ko.observable(false),
                self.isGuestSectionVisible = ko.observable(false),
                self.isSourceFiltersVisible = ko.observable(true),
                self.currentBunch = ko.observable(new Bunch()),
                self.applyBunchFilters = ko.observable(false),
                self.startDate = ko.observable(),
                self.endDate = ko.observable(),
                self.newsSourceFilters = ko.computed({
                    read: function () {
                       
                        if (self.applyBunchFilters() && self.currentBunch()) {
                         
                            if (self.currentBunch().bunchFilters() && self.currentBunch().bunchFilters().length > 0) {
                                var filtersArray = [];
                                for (var i = 0; i < self.currentBunch().bunchFilters().length; i++) {

                                    setTimeout(function () { }, 5000);

                                   
                                    var filter = _dc.filters.getLocalById(self.currentBunch().bunchFilters()[i].FilterId);
                                    if (filter && filter.filterTypeId < e.NewsFilterType.Category && filter.parentId === -1) {

                                        var parentFilter = self.getFilterClone(filter, self.currentBunch().bunchFilters()[i].Count);

                                        for (var j = 0; j < self.currentBunch().bunchFilters().length; j++) {
                                            var tempFilter = _dc.filters.getLocalById(self.currentBunch().bunchFilters()[j].FilterId);

                                            if (tempFilter && tempFilter.filterTypeId < e.NewsFilterType.Category && tempFilter.parentId === parentFilter.id) {
                                                var childFilter = self.getFilterClone(tempFilter, self.currentBunch().bunchFilters()[j].Count);
                                                parentFilter.bunchFilterChildren.push(childFilter);
                                            }
                                        }

                                        filtersArray.push(parentFilter);
                                    }
                                }

                                return filtersArray;
                            }
                        } else {
                            
                            var allFilters = _dc.filters.getAllLocal();
                            var newsSources = _.filter(allFilters, function (filterObj) {
                                return ((filterObj.filterTypeId !== e.NewsFilterType.Category && 
                                    filterObj.filterTypeId !== 15 && 
                                    filterObj.filterTypeId !== 16 && 
                                    filterObj.filterTypeId !== 17) && filterObj.parentId === -1) ? true : false;
                            });
                            console.log(newsSources[10].children());
                            return newsSources;
                        }
                    },
                    deferEvaluation: true
                }),
                self.categoryFilters = ko.computed({
                    read: function () {
                        var newsSources;
                        var allFilters = _dc.filters.getAllLocal();
                        newsSources = _.filter(allFilters, function (filterObj) {
                            return (filterObj.filterTypeId === e.NewsFilterType.Category && filterObj.parentId === -1) ? true : false;
                        });
                        if (newsSources.length > 0) {
                            x = newsSources.length - 2, y = newsSources.length - 1;
                            newsSources[x] = newsSources.splice(y, 1, newsSources[x])[0];
                        }
                        return newsSources;
                    },
                    deferEvaluation: true
                }),
                 self.getProgramByStory = function (story) {

                     if (story) {
                         var episode = _dc.episodes.getLocalById(story.episodeId);
                         return _dc.programs.getLocalById(episode.programId);
                     }

                 },

                self.check = function (array, value) {
                    return array.indexOf(value) == -1;
                },
                self.dateFilters = ko.computed({
                    read: function () {

                        var arr = [];
                        var fromDate = new Date(utils.getDefaultUTCDate());
                        var toDate = new Date(moment().add('month', 1).toISOString());

                        var calendar = appdata.teamCalendarControlReference();

                        if (calendar) {
                            fromDate = new Date(calendar.fromDate());
                            toDate = new Date(calendar.toDate());
                        }
                        fromDate.setHours(0, 0, 0, 0);
                        toDate.setHours(0, 0, 0, 0);

                        if (fromDate === toDate)
                            fromDate = toDate;


                        var programFilter = appdata.NLEProgramFilter();

                        var stories = _dc.stories.getObservableList();

                        if (stories && stories.length > 0) {

                            for (var i = 0, storylen = stories.length; i < storylen; i++) {
                                var story = stories[i];

                                var storyDate = new Date(story.lastUpdateDate());
                                storyDate.setHours(0, 0, 0, 0);

                                var calendarRangeFlag = (storyDate >= fromDate && storyDate <= toDate);

                                var formatedStoryDate = moment(new Date(story.lastUpdateDate())).format('MMMM D, YYYY');
                                var newDateFilterObject = { date: formatedStoryDate, count: 0 };
                                var alreadyContain = utils.arrayIsContainObject(arr, newDateFilterObject, "date");


                                if (programFilter.length == 0) {
                                    if (!alreadyContain.flag && calendarRangeFlag) {
                                        newDateFilterObject = { date: formatedStoryDate, count: stories.length };
                                        arr.push(newDateFilterObject);
                                    }
                                    else if (alreadyContain.flag) {
                                        arr[alreadyContain.objectIndex].count = arr[alreadyContain.objectIndex].count + stories.length;
                                    }
                                }
                            }
                        }

                        arr.reverse();
                        return arr;

                    },
                    deferEvaluation: true
                }),

                self.timeFilters = ko.computed({
                    read: function () {

                        var dateFilter = appdata.NLEDateFilter();
                        var programFilter = appdata.NLEProgramFilter();

                        var timeArray = [];
                        var timeslots = utils.getTimeSlots(60);

                        var storyArray = _dc.stories.getObservableList();
                        if (storyArray.length > 0) {
                            for (var i = 0; i < timeslots.length; i++) {
                                var obj = {};
                                obj.time = "";
                                obj.count = 0;

                                var count = 0;
                                for (var j = 0; j < storyArray.length; j++) {

                                    var story = storyArray[j];
                                    var storyDate = new Date(story.lastUpdateDate());
                                    if (dateFilter.length > 0) {
                                        for (var k = 0; k < dateFilter.length; k++) {

                                            if (timeslots[i].split(":")[0] == storyDate.getHours() && dateFilter[k].date == moment(story.lastUpdateDate()).format('MMMM D, YYYY')) {

                                                obj.time = timeslots[i];
                                                obj.count = obj.count + 1;
                                            }
                                        }
                                    }
                                    else if (programFilter.length > 0) {

                                        for (var k = 0; k < programFilter.length; k++) {
                                            var storyProgram = self.getProgramByStory(story, programFilter[k]);
                                            var storyProgramId = storyProgram.id;
                                            if (timeslots[i].split(":")[0] == storyDate.getHours() && storyProgramId === programFilter[k].id) {

                                                obj.time = timeslots[i];
                                                obj.count = obj.count + 1;
                                            }
                                        }

                                    }
                                    else {
                                        if (timeslots[i].split(":")[0] == storyDate.getHours()) {

                                            obj.time = timeslots[i];
                                            obj.count = obj.count + 1;


                                        }

                                    }
                                }
                                if (obj.count > 0)
                                    timeArray.push(obj);
                            }

                        }
                        return timeArray;
                    },
                    deferEvaluation: true
                }),
                self.program = ko.computed({
                    read: function () {
                        return _dc.programs.getObservableList();

                    },
                    deferEvaluation: true
                }),
                self.events = ko.computed({
                    read: function () {
                        self.eventsCount(_dc.events.getObservableList().length);
                        return _dc.events.getObservableList();
                    },
                    deferEvaluation: true
                }),
                self.newsAlerts = ko.computed({
                    read: function () {
                        self.newsAlertsCount(_dc.newsAlerts.getObservableList().length);
                        return _dc.newsAlerts.getObservableList();
                    },
                    deferEvaluation: true
                }),
                self.newsAlertsCount = ko.observable(0),
                self.eventsCount = ko.observable(0),
                self.toggleIsPopulated = function () {
                    self.isPopulated(self.isPopulated() + 1);
                },
                self.getFilterClone = function (filter, count) {
                    var clone = new Filter();

                    clone.id = filter.id;
                    clone.name = filter.name;
                    clone.cssClass = filter.cssClass();
                    clone.filterTypeId = filter.filterTypeId;
                    clone.parentId = filter.parentId;
                    clone.newsCount(count);

                    return clone;
                },

                self.isNullo = false;

                return self;
            }

        SourceFilterControl.Nullo = new SourceFilterControl();

        SourceFilterControl.Nullo.isNullo = true;

        SourceFilterControl.datacontext = function (dc) {
            if (dc) { _dc = dc; }
            return _dc;
        };

        return SourceFilterControl;
    });