﻿define('model.story',
    ['ko', 'moment', 'model.screenflow', 'model.news', 'underscore', 'appdata', 'utils', 'config', 'enum'],
    function (ko, moment, ScreenFlow, News, _, appdata, utils, config, e) {
        var
            _dc = this,

            Story = function () {

                var self = this;

                self.id,
                self.storyId,
                self.episodeId,
                self.programElementId,
                self.sequenceId,
                self.programId,
                self.tickerId,
                self.userId,
                self.isGreen = ko.observable(false),
                self.isRed = ko.observable(false),
                self.news = new News(),
                self.isUsed = ko.observable(false),
                self.categoryId = ko.observable(0),
                self.categoryDisplay = ko.observable(''),
                self.screenFlow = ko.observable(new ScreenFlow()),
                self.preview,
                self.workProgress = ko.observable(0),
                self.progressBarColor = ko.observable('#145dd0'),
                self.previewGuid = ko.observable(),

                self.creationDate = ko.observable(),
                self.lastUpdateDate = ko.observable(),
                self.creationDisplayDate = ko.computed({
                    read: function () {
                        return moment(self.creationDate()).fromNow()
                    },
                    deferEvaluation: true
                }),
                self.lastUpdateDisplayDate = ko.computed({
                    read: function () {
                        return moment(self.lastUpdateDate()).fromNow();
                    },
                    deferEvaluation: true
                }),

                self.startTime = ko.observable(),
                self.startTimeDisplay = ko.computed({
                    read: function () {
                        return moment(self.startTime()).format('hh:mm');
                    },
                    deferEvaluation: true
                }),
                self.contentDurationDisplay = ko.computed({
                    read: function () {
                        var durationInSec = Math.round(self.screenFlow().contentDuration());

                        if (durationInSec < 60) {
                            return durationInSec + ' Sec';
                        } else {
                            return Math.round(moment.duration(durationInSec, 'seconds').asMinutes()) + ' Min';
                        }
                    },
                    deferEvaluation: true
                }),
                self.episodeStartTime = ko.computed({
                    read: function () {
                        var episode = _dc.episodes.getLocalById(self.episodeId);
                        if (episode) {
                            return episode.startTime();
                        }
                    },
                    deferEvaluation: true
                }),
                self.remainingTime = ko.computed({
                    read: function () {
                        if (self.episodeStartTime()) {
                            if (new Date(self.episodeStartTime()) > appdata.currentTime()) {
                                try {
                                    var assignedTime = utils.getTimeDifference(self.lastUpdateDate(), self.episodeStartTime()).TotalSeconds;
                                    var remainingTime = utils.getTimeDifference(appdata.currentTime(), self.lastUpdateDate()).TotalSeconds;
                                    var progress = Math.round((remainingTime / assignedTime) * 100)

                                    if (progress > 100) {
                                        progress = 100;
                                        self.progressBarColor('#DC0000');
                                    } else if (progress < 0) {
                                        progress = 0;
                                    }

                                    self.workProgress(progress);
                                } catch (e) {
                                    //console.log(e.message);
                                }

                                var timeObj = utils.getTimeDifference(appdata.currentTime(), self.episodeStartTime());
                                return utils.getFormattedTime(timeObj);
                            } else {
                                self.workProgress(100);
                                self.progressBarColor('#DC0000');
                                return '00:00:00';
                            }
                        }
                    },
                    deferEvaluation: true
                }),

                self.isTeaserVisible = ko.computed({
                    read: function () {
                        if (appdata.currentUser().userType == e.UserType.HeadlineProducer) {
                            return false;
                        } else {
                            var programSegment = _dc.programElements.getLocalById(self.programElementId);
                            var maxSequenceId = _.max(programSegment.stories(), function (item) {
                                return item.sequenceId;
                            });
                            return maxSequenceId.sequenceId === self.sequenceId;
                        }
                    },
                    deferEvaluation: true
                }),
                self.contentDuration = ko.computed({
                    read: function () {
                        return self.screenFlow().contentDuration();
                    },
                    deferEvaluation: false
                }),
                self.canApplyCycle = ko.computed({
                    read: function () {
                        return self.screenFlow().screenTemplates().length > 4;
                    },
                    deferEvaluation: true
                }),

                self.previewUrl = ko.computed({
                    read: function () {
                        return config.MediaServerUrl + "getresource/" + self.previewGuid();
                    },
                    deferEvaluation: true
                }),

                self.setCategoryDispaly = function () {
                    var filters = _dc.filters.getObservableList();
                    for (var i = 0, len = filters.length; i < len; i++) {
                        if (filters[i].filterTypeId === e.NewsFilterType.Category && filters[i].value === self.categoryId()) {
                            self.categoryDisplay(filters[i].name);
                            break;
                        }
                    }

                },
                self.storyStatus = ko.computed({
                    read: function () {
                        self.screenFlow();
                        appdata.isRefreshStoryStatus();
                        var isApprovedPending = false;
                        var isApproved = false;
                        var inProcess = false;

                        if (self.screenFlow().screenTemplates && self.screenFlow().screenTemplates().length > 0) {

                            var arr = _.filter(self.screenFlow().screenTemplates(), function (item) {
                                return !item.isMediaWallTemplate();
                            });
                            if (arr && arr.length > 0) {
                                for (var j = 0; j < arr.length; j++) {

                                    if (arr[j].nleStatusId() == e.ScreenTemplateStatus.InProcess && arr[j].storywriterStatusId() == e.ScreenTemplateStatus.InProcess) {
                                        inProcess = true;
                                        return 'inProcess';
                                        break;
                                    }

                                }
                                if (!inProcess) {
                                    for (var j = 0; j < arr.length; j++) {
                                        if (arr[j].nleStatusId() == e.ScreenTemplateStatus.ApprovalPending && arr[j].storywriterStatusId() == e.ScreenTemplateStatus.ApprovalPending) {
                                            isApprovedPending = true;
                                            return 'approvedPending';
                                            break;
                                        }
                                    }
                                    if (!isApprovedPending) {
                                        for (var j = 0; j < arr.length; j++) {
                                            if (arr[j].nleStatusId() == e.ScreenTemplateStatus.Approved && arr[j].storywriterStatusId() == e.ScreenTemplateStatus.Approved) {
                                                isApproved = true;
                                                return 'approved';
                                                break;
                                            }

                                        }

                                    }
                                }
                            }
                        }
                        else {
                            return 'default';
                        }
                    },
                    deferEvaluation: true
                }),
                self.isNullo = false;

                return self;
            }

        Story.Nullo = new Story();

        Story.Nullo.isNullo = true;

        Story.datacontext = function (dc) {
            if (dc) { _dc = dc; }
            return _dc;
        };

        return Story;
    });