﻿define('model.storyitem',
    ['ko', 'underscore'],
    function (ko, _) {

        var
            _dc = this,

            StoryItem = function () {

                var self = this;

                self.id,
                self.storyId,
                self.orderId,
                self.slug,
                self.instruction,
                self.details,
                self.isPlaying = ko.observable(false),
                self.isPlayed = ko.observable(false),
                self.isSkipped = ko.observable(false),

                self.isNullo = false;

                self.fillObservableProperties = function () {

                };

                return self;
            }

        StoryItem.Nullo = new StoryItem();

        StoryItem.Nullo.isNullo = true;

        // static member
        StoryItem.datacontext = function (dc) {
            if (dc) { _dc = dc; }
            return _dc;
        };

        return StoryItem;
    });