﻿define('model.template',
    ['ko', 'underscore'],
    function (ko, _) {

        var
            _dc = this,

            Template = function () {

                var self = this;

                self.id,
                self.flashTemplateId ,
                self.casparRundownItemId,
                self.flashLayer,
                self.invoke,
                self.useStoredData,
                self.useUpperCaseData,
                
                
               

                self.isNullo = false;

                self.fillObservableProperties = function () {

                };

                return self;
            }

        Template.Nullo = new Template();

        Template.Nullo.isNullo = true;

        // static member
        Template.datacontext = function (dc) {
            if (dc) { _dc = dc; }
            return _dc;
        };

        return Template;
    });