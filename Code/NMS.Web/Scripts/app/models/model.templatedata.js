﻿define('model.templatedata',
    ['ko', 'underscore'],
    function (ko, _) {

        var
            _dc = this,

            TemplateData = function () {

                var self = this;

                self.id,
                self.casparRundownItemId,
                self.templateDataKey ,
                self.templateDataValue,
                
              
                
                
               

                self.isNullo = false;

                self.fillObservableProperties = function () {

                };

                return self;
            }

        TemplateData.Nullo = new TemplateData();

        TemplateData.Nullo.isNullo = true;

        // static member
        TemplateData.datacontext = function (dc) {
            if (dc) { _dc = dc; }
            return _dc;
        };

        return TemplateData;
    });