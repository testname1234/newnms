﻿define('model.templateelement',
    ['ko', 'config'],
    function (ko, config) {

        var
            _dc = this,

            TemplateElement = function () {

                var self = this;

                self.id,
                self.storyTemplateScreenElementId,
                self.screenTemplateId,
                self.screenElementId,
                self.imageGuid,
                self.contentBucket=ko.observableArray([]),

                self.top,
                self.left,
                self.bottom,
                self.right,
                self.width,
                self.height,
                self.zindex,
                self.coordinates = [],
                self.guestId,
                self.processData = function () {
                    self.width = self.right - self.left;
                    self.height = self.top - self.bottom;

                    self.coordinates.push(self.left);
                    self.coordinates.push(self.top);

                    self.coordinates.push(self.right);
                    self.coordinates.push(self.bottom);
                },

                self.isNullo = false;

                return self;
            }

        TemplateElement.Nullo = new TemplateElement();

        TemplateElement.Nullo.isNullo = true;

        TemplateElement.datacontext = function (dc) {
            if (dc) { _dc = dc; }
            return _dc;
        };

        return TemplateElement;
    });