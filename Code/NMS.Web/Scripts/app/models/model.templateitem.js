﻿define('model.templateitem',
    ['ko', 'underscore'],
    function (ko, _) {

        var
            _dc = this,

            TemplateItem = function () {

                var self = this;

                self.id,
                self.casparRundownItemId,
                self.templateId ,
               
                self.isNullo = false;
                self.fillObservableProperties = function () {

                };

                return self;
            }

        TemplateItem.Nullo = new TemplateItem();

        TemplateItem.Nullo.isNullo = true;

        // static member
        TemplateItem.datacontext = function (dc) {
            if (dc) { _dc = dc; }
            return _dc;
        };

        return TemplateItem;
    });