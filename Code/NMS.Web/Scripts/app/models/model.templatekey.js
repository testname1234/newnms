﻿define('model.templatekey',
    ['underscore', 'ko', 'enum', 'config'],
    function (_, ko, e, config) {

        var
            _dc = this,
            logger = config.logger,

            TemplateKey = function () {

                var self = this;

                self.id,
                self.keyName,
                self.keyValue = ko.observable(''),
                self.screenTemplateId,
                self.storyScreenTemplatekeyId,
                self.languageCode = ko.observable('ur'),
                self.storyScreenTemplateId,
                self.left=0,
                self.bottom=0,
                self.top=0,
                self.right=0

                self.isNullo = false;

                return self;
            };

        TemplateKey.Nullo = new TemplateKey();

        TemplateKey.Nullo.isNullo = true;

        TemplateKey.datacontext = function (dc) {
            if (dc) { _dc = dc; }
            return _dc;
        };

        return TemplateKey;
    });