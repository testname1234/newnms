﻿define('model.ticker',
    ['ko', 'model.news', 'utils', 'enum', 'moment', 'appdata','underscore'],
    function (ko, News, utils, e, moment, appdata,_) {
        var
            _dc = this,

            Ticker = function () {
                var self = this;

                self.id,
                self.displayId = ko.observable(),
                self.creationTime = ko.observable(),
                self.news = ko.observable(new News()),
                self.categoryId = '',
                self.title = ko.observable(''),
                self.onAiredTime = ko.observable(),
                self.isShow = ko.observable(),
                self.sequenceId=1,
                self.userId = ko.observable(),
                self.lastUpdateDate = ko.observable(),
                self.creationDate = ko.observable(),
                self.tickerLinesCount=ko.observable(0),
                self.locationId = ko.observable(),
                self.tickerTypeId ,
                self.createdBy,
                self.tickerGroupName = ko.observable(),
                self.severity  = ko.observable(),
                self.newsGuid = ko.observable(),
                self.isUsed = ko.observable(false),
                self.currentTickerLineCount = ko.observable(0),
                self.dropDownVisible = ko.observable(false),
                self.currentTickerOperator = ko.observable(-1),
                self.emptyTickerBit = ko.observable(),
                self.newsFileId = ko.observable(0),
                self.isActive = ko.observable(false),
                self.creationDateDisplay = ko.computed({
                    read: function () {
                        return moment(self.creationDate()).fromNow();
                    },
                    deferEvaluation: true
                }),
                self.lastUpdateDateDisplay = ko.computed({
                    read: function () {
                        return moment(self.lastUpdateDate()).fromNow();
                    },
                    deferEvaluation: true
                }),
                
                self.tickerLines = ko.computed({
                    read: function () {
                        var arr = [];

                        arr = _.filter(_dc.tickerLines.getObservableList(), function (line) {
                            return line.tickerId == self.id;
                        });
                        if (arr.length > 0) {
                            arr = arr.sort(function (entityA, entityB) {
                                return entityA.sequenceId - entityB.sequenceId;
                            });
                            
                            var temp = _.filter(arr,function (entityA) {
                                return entityA.isShow() == true;
                            });
                            self.currentTickerLineCount(temp.length);
                        }

                        return arr;
                    },
                    deferEvaluation: true
                }),
               
                self.userName = ko.computed({
                    read: function () {
                        var users = _dc.users.getObservableList();
                        var name = '';
                        for (var i = 0, len = users.length ; i < len; i++) {
                            if (users[i].id === self.userId()) {
                                name = users[i].name;
                                break;
                            }
                        }
                        return name;
                    },
                    deferEvaluation: true
                }),

                self.categoryName = ko.computed({
                    read: function () {
                        
                        var category = _dc.categories.getLocalById(self.categoryId);
                        var tempName;
                        if (category) {
                            tempName = category.name;
                        }
                        if (!tempName) {
                            var filters = _dc.filters.getObservableList();
                            for (var i = 0, len = filters.length; i < len; i++) {
                                if (filters[i].filterTypeId === e.NewsFilterType.Category && filters[i].value === self.categoryId) {
                                    tempName = filters[i].name;
                                    break;
                                }
                            }
                        }
                        return tempName;
                    },
                    deferEvaluation: true
                }),
                
                self.displayDate = ko.computed({
                    read: function () {
                        return false
                    },
                    deferEvaluation: true
                }),

                self.setDispalyId = function (id) {
                    var formatedId = String.format("{0}{1}", "000", id);
                    self.displayId(formatedId);

                 
                };

                self.isNullo = false;

                return self;
            }

        Ticker.Nullo = new Ticker();

        Ticker.Nullo.isNullo = true;

        Ticker.datacontext = function (dc) {
            if (dc) { _dc = dc; }
            return _dc;
        };

        return Ticker;
    });