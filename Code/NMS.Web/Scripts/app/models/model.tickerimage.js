﻿define('model.tickerImages',
    ['ko', 'underscore'],
    function (ko, _) {

        var
            _dc = this,

            TickerImage = function () {

                var self = this;

                self.id,
                self.channelId,
                self.groupId,
                self.imagePath=ko.observable(''),
                self.channelTickerLocationId,
                self.lastUpdateDate,
                self.tickerImageStatusId,
                self.isNullo = false;
                return self;
            }

        TickerImage.Nullo = new TickerImage();

        TickerImage.Nullo.isNullo = true;

        // static member
        TickerImage.datacontext = function (dc) {
            if (dc) { _dc = dc; }
            return _dc;
        };

        return TickerImage;
    });