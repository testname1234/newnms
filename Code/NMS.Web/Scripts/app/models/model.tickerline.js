﻿define('model.tickerline',
    ['ko', 'enum'],
    function (ko, e) {

        var
            _dc = this,

            TickerLine = function () {

                var self = this;

                self.id,

                self.tickerId,
                self.title = ko.observable(''),
                self.languageCode = ko.observable(),
                self.sequenceId = 1,
                self.lastUpdateDate = ko.observable(),
                self.creationDate = ko.observable(),
                self.severity = ko.observable(1),
                self.frequency = ko.observable(1),
                self.repeatCount = ko.observable(),
                self.isShow = ko.observable(true),
                self.creationTime = ko.observable(),
                self.userId = ko.observable(),
                self.operatorNo,
                self.creationDateDisplay = ko.computed({
                    read: function () {
                        return moment(self.creationDate()).fromNow();
                    },
                    deferEvaluation: true
                }),
                self.lastUpdateDateDisplay = ko.computed({
                    read: function () {
                        return moment(self.lastUpdateDate()).fromNow();
                    },
                    deferEvaluation: true
                }),
                self.userName = ko.computed({
                     read: function () {
                         var users = _dc.users.getObservableList();
                         var name = '';
                         for (var i = 0, len = users.length ; i < len; i++) {
                             if (users[i].id === self.userId()) {
                                 name = users[i].name;
                                 break;
                             }
                         }
                         return name;
                     },
                     deferEvaluation: true
                 }),
                self.isNullo = false;

                return self;
            }

        TickerLine.Nullo = new TickerLine();

        TickerLine.Nullo.isNullo = true;

        TickerLine.datacontext = function (dc) {
            if (dc) { _dc = dc; }
            return _dc;
        };

        return TickerLine;
    });