﻿define('model.tickerreportercontrol',
    ['ko', 'model.ticker', 'appdata', 'enum', 'string'],
    function (ko, Ticker, appdata, e, S) {

        var

        _dc = this,

        TickerReporterControl = function () {

                var self = this;

                self.tickersCount = ko.observable(0),
                self.currentTicker = ko.observable(new Ticker()),
                self.isReset = ko.observable(false),
                self.tickerLineList = ko.observableArray([]),
                self.lastTicker=ko.observable(),
                self.groupSuggestionList = ko.observableArray([]),
                self.setGroupName = function (data) {
                    self.groupName(data);
                },
                self.addOrDeleteFromTickerList = function (data, isDeleted) {
                    if (isDeleted) {
                        for (var i = 0; i < self.tickerLineList().length; i++) {
                            if (data.children()[1].value === self.tickerLineList()[i].Text) {
                                self.tickerLineList.remove(self.tickerLineList()[i]);
                            }
                        }
                    }
                    else {
                        var freq = 1;
                        var sev = 1;
                        var repeatCount = 1;

                        if (data.children()[1].value.trim()) {
                            self.tickerLineList.push({ Text: data.children()[1].value, LanguageCode: "ur", SequenceId: self.tickerLineList().length == 0 ? 1 : self.tickerLineList().length + 1, Severity: sev, Frequency: freq, RepeatedCount: repeatCount });
                        }
                    }
                },
                self.setGroupName = function (data) {
                    self.currentTicker().groupName(data);
                },
                self.resetControl = function () {
                    self.tickersCount(0);
                    self.currentTicker(new Ticker());
                    self.tickerLineList([]);
                    self.isReset(false);

                };

                self.isNullo = false;

                return self;
            }

        TickerReporterControl.Nullo = new TickerReporterControl();

        TickerReporterControl.Nullo.isNullo = true;

        TickerReporterControl.datacontext = function (dc) {
            if (dc) { _dc = dc; }
            return _dc;
        };

        return TickerReporterControl;
    });