﻿define('model.tickerwritercontrol',
    ['ko', 'model.ticker', 'appdata', 'enum', 'config', 'model.tickerline'],
    function (ko, ticker, appdata, e, config, tickerLine) {

        var
            _dc = this,

            TickerWriterControl = function () {

                var self = this;


                self.tickerCount = ko.observable(0),
                self.currentNews = ko.observable(),
                self.tickerLanguageCode = ko.observable(),
                self.currentTickerType = ko.observable(3),
                self.tickerTypes = [{ name: e.TickerType.Breaking.Text, value: e.TickerType.Breaking.Value }, { name: e.TickerType.Latest.Text, value: e.TickerType.Latest.Value }, { name: e.TickerType.Category.Text, value: e.TickerType.Category.Value }],
                self.currentTicker = ko.observable(new ticker()),
                self.currentTickerCategory = ko.observable(3),
                self.isManager = ko.observable(false),
                self.isEdit = ko.observable(false),
                self.groupName = ko.observable(),
                self.cssClass = ko.observable('showleftpanel'),
                self.tickerLineList = ko.observableArray([]),
                self.isTickerClear = ko.observable(false),
                self.removedTickers = ko.observableArray([]),
                self.isFromBureau = ko.observable(false),
                self.tickerPopupInputVisible = ko.observable(false),
                self.isDisabled = ko.observable(false),
                self.manager,

                self.tickerCategories = ko.computed({
                    read: function () {
                        return _dc.categories.getObservableList();
                    },
                    deferEvaluation: true
                }),

                self.insertTicker = function () {
                    var data = self.validateData();
                    if (data) {
                        self.manager.ticker.saveTickerOnServer(self.currentTicker(), data, self.isFromBureau());
                        self.currentTicker(new ticker());
                        self.tickerLineList([]);
                        appdata.istickerPopupVisible(false);
                        self.isEdit(false);
                        self.tickerPopupInputVisible(false);
                        self.isDisabled(false);
                    }
                },

                self.validateData = function () {

                    self.currentTicker().tickerGroupName("بسم اللہ الرحمان الرحیم");

                    if (!self.currentTicker().categoryId)
                        self.currentTicker().categoryId = self.currentTickerCategory();

                    //if (!self.currentTicker().tickerGroupName().trim()) {
                    //    config.logger.error("Please provide Group Name !");
                    //    return null;
                    //}


                    //if (self.currentTicker().severity() === e.Severity.Low) {
                    //    if ($('#ddlcategory').is(':visible') && appdata.currentUser().userType === e.UserType.TickerProducer && (!self.currentTicker().categoryId || self.currentTicker().categoryId == null)) {
                    //        config.logger.error("Please select a category !");
                    //        return null;
                    //    }
                    //}
                    var arrLTickerist = [];
                    if (self.tickerLineList().length > 0) {
                        for (var i = 0; i < self.tickerLineList().length; i++) {

                            if (self.tickerLineList()[i].title().trim()) {
                                arrLTickerist.push(
                                {
                                    Text: self.tickerLineList()[i].title(),
                                    LanguageCode: "ur",
                                    SequenceId: self.tickerLineList().length == 0 ? 1 : self.tickerLineList().length + 1,
                                    Severity: self.currentTicker().severity(),
                                    Frequency: self.tickerLineList()[i].frequency(),
                                    CreatedBy: appdata.currentUser().id,
                                    TickerLineId: self.tickerLineList()[i].id,
                                    TickerId: self.tickerLineList()[i].tickerId,
                                    CategoryStatusId: !self.isDisabled() ? e.TickerStatus.Approved : self.tickerLineList()[i].categoryStatusId()


                                });
                            }
                        }
                        return arrLTickerist;
                    }
                    else {
                        config.logger.error("Please provide Ticker Lines !");
                        return null;
                    }
                },

                self.editTicker = function () {
                    var data = self.validateData();
                    if (data) {
                        if (self.isFromBureau() == true) {
                            self.manager.ticker.saveTickerOnServer(self.currentTicker(), data, self.isFromBureau());
                        }
                        else {
                            self.manager.ticker.updateTickerAndTickerLines(self.currentTicker(), data, self.removedTickers(),e.TickerType.Category.Value);
                        }
                        self.currentTicker(new ticker());
                        self.tickerLineList([]);
                        appdata.istickerPopupVisible(false);
                        self.tickerPopupInputVisible(false);
                        self.isDisabled(false);
                    }
                },
                self.addTickerLine = function (data) {
                    if (self.tickerLineList()[self.tickerLineList().length - 1].title().trim()) {
                        var line = new tickerLine();
                        self.tickerLineList.push(line);
                    }
                },

                self.removeTickerLine = function (data) {
                    if (data.title().trim()) {
                        if (data.id) {
                            self.removedTickers.push(data.id);
                        }
                        self.tickerLineList.remove(data);
                    }
                    if (self.tickerLineList().length == 0) {
                        var line = new tickerLine();
                        self.tickerLineList.push(line);
                    }
                },

                self.isTickerTypeVisible = ko.computed({
                    read: function () {
                        if (appdata.currentUser().userType === e.UserType.TickerProducer || appdata.currentUser().userType === e.UserType.TickerManger) {
                            if (appdata.currentUser().userType === e.UserType.TickerManger)
                                self.isManager(true);

                            return true;
                        }

                    },
                    deferEvaluation: true
                });

                self.isNullo = false;

                return self;
            }

        TickerWriterControl.Nullo = new TickerWriterControl();

        TickerWriterControl.Nullo.isNullo = true;

        TickerWriterControl.datacontext = function (dc) {
            if (dc) { _dc = dc; }
            return _dc;
        };

        return TickerWriterControl;
    });