﻿define('model.transformation',
    ['ko', 'underscore'],
    function (ko, _) {

        var
            _dc = this,

            Transformation = function () {

                var self = this;

                self.id,
                self.casparRundownItemId,
                self.positionx,
                self.positiony ,
                self.scalex ,
                self.scaley ,
                self.duration ,
                self.tween ,
                self.triggerOnNext , 
                self.defer ,
               
                self.isNullo = false;
                self.fillObservableProperties = function () {

                };

                return self;
            }

        Transformation.Nullo = new Transformation();

        Transformation.Nullo.isNullo = true;

        // static member
        Transformation.datacontext = function (dc) {
            if (dc) { _dc = dc; }
            return _dc;
        };

        return Transformation;
    });