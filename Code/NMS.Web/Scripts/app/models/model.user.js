﻿define('model.user', [],
    function () {

        var
            _dc = this,

            User = function () {

                var self = this;

                self.id,
                self.name,
                self.displayName,
                self.profilePhotoUrl = '',
                self.userType,
                self.userRoles=ko.observableArray([]),

                self.isNullo = false;

                return self;
            }

        User.Nullo = new User();

        User.Nullo.isNullo = true;

        // static member
        User.datacontext = function (dc) {
            if (dc) { _dc = dc; }
            return _dc;
        };

        return User;
    });