﻿define('model.usercasparaction',
    ['ko', 'underscore'],
    function (ko, _) {

        var
            _dc = this,

            UserCasparAction = function () {

                var self = this;

                self.id,
                self.userId ,
                self.roleId ,
                self.casparItemId ,
                
                
                self.isNullo = false;

                self.fillObservableProperties = function () {

                };

                return self;
            }

        UserCasparAction.Nullo = new UserCasparAction();

        UserCasparAction.Nullo.isNullo = true;

        // static member
        UserCasparAction.datacontext = function (dc) {
            if (dc) { _dc = dc; }
            return _dc;
        };

        return UserCasparAction;
    });