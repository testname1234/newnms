﻿define('model.userfilter',
    ['ko','appdata'],
    function (ko, appdata) {

        var
            _dc = this,

            UserFilter = function () {

                var self = this;

                self.id,
                self.name,
                self.filterTypeId,
                self.value,
                self.cssClass = ko.observable(),
                self.parentId,
                self.childrenIds = ko.observableArray([]),
                self.isSelected = ko.observable(false),
                self.isDiscarded = ko.observable(false),
                self.isNotAllowed = ko.observable(false),
                self.refresh = ko.observable(false),
                self.children =ko.observableArray([]),
                self.isVisible = ko.observable(false),
                self.changeSign = ko.observable('+'),

                self.visibleChild = function (data) {                   
                    if (data.children().length > 0) {
                        for (var i = 0; i < data.children().length; i++) {
                            data.children()[i].isVisible(!data.children()[i].isVisible());
                            if (data.children()[i].isVisible()) {
                                data.changeSign('-');
                            } else {
                                data.changeSign('+');
                            }
                        }
                    }                   
                },
                self.allowed = function (data) {
                    if (data.parentId === -1) {
                        data.isNotAllowed(!data.isNotAllowed());
                        for (var i = 0; i < data.children().length; i++) {
                            data.children()[i].isNotAllowed(data.isNotAllowed());
                        }
                    }
                    else if (data.parentId !== -1) {
                        data.isNotAllowed(!data.isNotAllowed());
                    }                 
                },
                self.discard = function (data) {
                    if (data.parentId === -1) {
                        data.isDiscarded(!data.isDiscarded());
                        for (var i = 0; i < data.children().length; i++) {
                            data.children()[i].isDiscarded(data.isDiscarded());
                        }
                    }
                    else if (data.parentId !== -1) {
                        data.isDiscarded(!data.isDiscarded());
                    }
                };

                self.isNullo = false;

                return self;
            }

        UserFilter.Nullo = new UserFilter();

        UserFilter.Nullo.isNullo = true;

        UserFilter.datacontext = function (dc) {
            if (dc) { _dc = dc; }
            return _dc;
        };

        return UserFilter;
    });