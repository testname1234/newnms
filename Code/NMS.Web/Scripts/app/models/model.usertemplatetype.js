﻿define('model.usertemplatetype',
    ['ko', 'underscore'],
    function (ko, _) {

        var
            _dc = this,

            UserTemplateType = function () {

                var self = this;

                self.id,
                self.userId ,
                self.roleId ,
                self.templateTypeId ,
                self.flashTemplateId ,
                
                
                self.isNullo = false;

                self.fillObservableProperties = function () {

                };

                return self;
            }

        UserTemplateType.Nullo = new UserTemplateType();

        UserTemplateType.Nullo.isNullo = true;

        // static member
        UserTemplateType.datacontext = function (dc) {
            if (dc) { _dc = dc; }
            return _dc;
        };

        return UserTemplateType;
    });