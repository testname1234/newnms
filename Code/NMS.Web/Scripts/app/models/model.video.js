﻿define('model.video',
    [],
    function () {

        var
            _dc = this,

            Video = function () {

                var self = this;

                self.id,
                self.channelId,
                self.programId,
                self.from,
                self.to,
                self.url,
                self.isProcessed=ko.observable(false),

                self.isNullo = false;

                return self;
            }

        Video.Nullo = new Video();

        Video.Nullo.isNullo = true;

        // static member
        Video.datacontext = function (dc) {
            if (dc) { _dc = dc; }
            return _dc;
        };

        return Video;
    });