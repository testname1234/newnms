﻿define('model.workrolefolder',
    ['ko', 'underscore', 'enum', 'moment'],
    function (ko, _, e, moment) {

        var
            _dc = this,

            WorkRoleFolder = function () {

                var self = this;

                self.id,

                self.maxColor = ko.observable(),
                self.folderId,
                self.allowRead,
                self.allowMove,
                self.allowCopy,
                self.allowCreate,
                self.allowDelete,
                self.allowModify,
                self.lastUpdateDate = ko.observable(),
                self.creationDate = ko.observable(),

                self.colorList = ko.computed({
                    read: function () {
                        var temp = [];
                        
                        var enumObj = e.NewsFileStatus;
                        var count = 1;
                        for (var name in enumObj) {
                            if (count > self.maxColor()) {
                                break;
                            }
                            temp.push({ key: name, value: enumObj[name] });
                            count ++;
                        }
                        return temp;
                    },
                    deferEvaluation: true
                }),
                self.lastUpdateDateDisplay = ko.computed({
                    read: function () {
                        return moment(self.lastUpdateDate()).fromNow();
                    },
                    deferEvaluation: true
                }),

                self.creationDateDisplay = ko.computed({
                    read: function () {
                        return moment(self.creationDateDisplay()).fromNow();
                    },
                    deferEvaluation: true
                }),

                self.isNullo = false;

                return self;
            }

        WorkRoleFolder.Nullo = new WorkRoleFolder();

        WorkRoleFolder.Nullo.isNullo = true;

        // static member
        WorkRoleFolder.datacontext = function (dc) {
            if (dc) { _dc = dc; }
            return _dc;
        };

        return WorkRoleFolder;
    });