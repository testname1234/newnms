﻿define('model.xmlparser',
    [],
    function () {

        var
            _dc = this,

            XMLParser = function () {

                var self = this;
                self.xml;
                self.Array = [];
                self.link=0;
                self.j=0;
                self.tempArray = ko.observableArray([]);
                self.newArray = ko.observableArray([]);
                self.xmlDocumentFile = ko.observable();
                self.Element = function() { 
                    var
                    Tagname,
                    ComponentType,
                    Label,
                    Value = ko.observable(),
                    children = ko.observableArray([]);

                    return {Tagname:Tagname, ComponentType:ComponentType, Label:Label,Value:Value ,children:children}
                };
                self.btnclick = function () {
                    var jsonstring = ko.toJSON(self.tempArray());
                    xw = new XMLWriter('UTF-8');
                    xw.writeStartDocument();
                    xw.writeStartElement('DOC');

                    for (var i = 0; i < self.tempArray().length; i++) {


                        self.JSONToXML(self.tempArray()[i]);

                    }
                    self.xmlDocumentFile(xw.flush());
                };
                self.JSONToXML = function (Node) {

                    xw.writeStartElement(Node.Tagname);
                    if (Node.ComponentType) {
                        xw.writeAttributeString('componentType', Node.ComponentType);

                        xw.writeAttributeString('label', Node.Label);

                        xw.writeString(Node.Value);
                        xw.writeEndElement();
                    }


                    if (Node.children().length > 0) {

                        var children = Node.children();
                        var i = 0;
                        for (; i < children.length; i++) {
                            self.JSONToXML(children[i]);
                        }
                        if (i == children.length) {

                            xw.writeEndElement();

                        }
                    }


                },
                self.makeXml = function (Node, link) {
                    var obj;
                    Array[link] = document.createElement(Node.Tagname);
                    Array[link].setAttribute("componentType", Node.ComponentType);
                    if (Node.children) {

                        for (var i = 0; i < Node.children().length; i++) {
                            //     link = link + 1;
                            //      obj = self.makeXml(Node.children()[i], link);

                            self.appChild(Node.children()[i], link);


                        }


                    }
                    return obj;
                };
                self.appChild = function (node, val) {
                    Array[link].appendChild(document.createElement(node.Tagname));
                    Array[link].setAttribute("componentType", Node.ComponentType);

                    if (node.children) {
                        for (var i = 0; i < node.children().length; i++) {
                            self.appChild(node.children()[i], link);
                        }
                    }
                }

                self.parseNode = function (Node, check) {
                    var newElement = new self.Element();
                    if (Node.childNodes.length == 1) {
                        newElement.Value = Node.textContent;

                    }
                    else {
                        newElement.Value = "";
                    }
                    newElement.Tagname = Node.nodeName;
                    newElement.ComponentType = Node.getAttribute('componentType');
                    newElement.Label = Node.getAttribute('label');
                    //newElement.Value = Node.textContent;

                    if (Node.childNodes) {
                        var children = Node.childNodes;
                        for (var i = 0; i < children.length; i++) {
                            if (children[i].nodeType != 3)
                                newElement.children.push(self.parseNode(children[i], false));
                        }
                    }

                    return newElement;

                },
                self.makeXml1 = function (Node, check) {
                    var newEle = new Ele();
                    if (Node.children().length == 1) {
                        newEle.Value = Node.Value;

                    }
                    else {
                        newEle.Value = "";
                    }
                    newEle.nodeName = Node.Tagname;
                    newEle.ComponentType = Node.ComponentType;
                    newEle.Label = Node.Label;
                    //newElement.Value = Node.textContent;

                    if (Node.children()) {
                        var children = Node.children();
                        for (var i = 0; i < children.length; i++) {
                            // if (children[i].nodeType != 3)
                            newEle.children.push(self.makeXml1(children[i], false));
                        }
                    }

                    return newEle;

                },


                self.ProcessDocument = function () {
                    //if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
                    //    xmlhttp = new XMLHttpRequest();
                    //}
                    //else {// code for IE6, IE5
                    //    xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                    //}
                    //xmlhttp.open("GET", "Scripts/XMLFiles/data.xml", false);
                    //xmlhttp.send();
                    //var xmlDoc = new window.ActiveXObject("Microsoft.XMLDOM");
                    //xmlDoc.async = "false";
                    //xmlDoc.loadXML(self.xmlDocumentFile());
                    //  var temp = self.xmlToJson(xmlDoc);
                    //  xDoc[0].children[0].textContent = "asd";
                    
                    var xmlDoc = new window.DOMParser().parseFromString(self.xmlDocumentFile(), "text/xml");
                    var xDoc = xmlDoc.childNodes;
                    var tempList = [];
                    for (i = 0; i < xDoc[0].children.length; i++) {
                        var element = self.parseNode(xDoc[0].children[i], true);
                        tempList.push(element);
                       
                    }
                    self.tempArray(tempList);
                };
                self.init = function () {
                    self.ProcessDocument();
                };
                self.isNullo = false;

                return self;
            }

        XMLParser.Nullo = new XMLParser();

        XMLParser.Nullo.isNullo = true;

        // static member
        XMLParser.datacontext = function (dc) {
            if (dc) { _dc = dc; }
            return _dc;
        };

        return XMLParser;
    });