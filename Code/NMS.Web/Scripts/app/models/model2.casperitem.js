﻿define('model.casperitem',
    [
        'ko',
        'underscore'
    ],
    function (ko,_) {

    	var
            _dc = this,

            CasperItem = function () {

            	var self = this;

                self.id,
                self.roId,
                self.type
                self.name,
                self.videoLayer,

                self.isNullo = false;

            	return self;
            }

    	CasperItem.Nullo = new CasperItem();

    	CasperItem.Nullo.isNullo = true;

    	CasperItem.datacontext = function (dc) {
    		if (dc) { _dc = dc; }
    		return _dc;
    	};

    	return CasperItem;
    });