﻿define('binder',
    [
        'jquery',
        'ko',
        'config',
        'enum',
        'presenter',
        'vm',
        'appdata',
        'manager',
        'datacontext',
        'model',
        'dataservice'
    ],
    function ($, ko, config, e, presenter, vm, appdata, manager, dc, model, dataservice) {
        var logger = config.logger;
        
        var
            ids = config.viewIds.nmsBol,
            
            bindPreLoginViews = function () {
                ko.applyBindings(vm.shell, getView(ids.shellTopNavView));
                ko.applyBindings(vm.shell, getView(ids.shellLeftView));
                ko.applyBindings(vm.contentviewer, getView(ids.contentViewer));
                if (appdata.currentUser().userType != e.UserType.taggedUser) {
                    ko.applyBindings(vm.reportnews, getView(ids.reportnews));
                    //ko.applyBindings(vm.updatenews, getView(ids.updatenews));
                }
            },

            bindPostLoginViews = function () {
                if (appdata.currentUser().userType === e.UserType.Controlleroutput) {
                    ko.applyBindings(vm.home, getView(ids.controllerOutput));
                }

                if (appdata.currentUser().userType === e.UserType.controllerInput) {
                    ko.applyBindings(vm.home, getView(ids.homeView));
                    ko.applyBindings(vm.home, getView(ids.topicSelectionView));
                }

                if (appdata.currentUser().userType === e.UserType.taggedUser) {
                    ko.applyBindings(vm.taggedusers, getView(ids.taggerInput));
                    ko.applyBindings(vm.tagnews, getView(ids.tagnews));
                    ko.applyBindings(vm.taggedusers, getView(ids.taggedPending));
                }

                if (appdata.currentUser().userType === e.UserType.filterSocial) {
                    ko.applyBindings(vm.taggedusers, getView(ids.filterSocial));
                }

                if (appdata.currentUser().userType === e.UserType.tagreport) {
                    ko.applyBindings(vm.tagreport, getView(ids.tagreport));
                    ko.applyBindings(vm.tagnews, getView(ids.tagnews));
                }

                if (appdata.currentUser().userType === e.UserType.copyWriter || appdata.currentUser().userType === e.UserType.editorial) {
                    ko.applyBindings(vm.copyWritereditorial, getView(ids.copywriter));
                }
            },

            deleteExtraViews = function () {
            },

            bindStartUpEvents = function () {

                $(window).on('hashchange', function () {
                    appdata.currentHash = window.location.hash;
                });
            },

            getView = function (viewName) {
                return $(viewName).get(0);
            };

        return {
            bindPreLoginViews: bindPreLoginViews,
            bindPostLoginViews: bindPostLoginViews,
            bindStartUpEvents: bindStartUpEvents,
            deleteExtraViews: deleteExtraViews
        }
    });