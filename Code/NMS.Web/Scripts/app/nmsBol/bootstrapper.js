﻿define('bootstrapper',
    [
        'enum',
        'config',
        'binder',
        'route-config',
        'manager',
        'timer',
        'vm',
        'presenter',
        'appdata',
        'reporter.appdata',
        'helper',
        'model.user',
        'ko',
        'datacontext',
        'router'
    ],
    function (e, config, binder, routeConfig, manager, timer, vm, presenter, appdata, rappdata, helper, usermodel, ko, dc, router) {
        if ($.trim(helper.getCookie('user-id')) == '') {
            window.location.href = '/Login#/index';
        }
        var
            run = function () {
                config.dataserviceInit();
                appdata.extractUserInformation();
                rappdata.extractUserInformation();
                binder.bindStartUpEvents();
                routeConfig.register();
                presenter.toggleActivity(true);

                if (router.currentHash() === config.hashes.nmsBol.reportnews && appdata.currentUser().userType === e.UserType.Controlleroutput)
                {
                    window.location.href = '/login#/index';
                    document.cookie = config.sessionKeyName + '=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';
                    document.cookie = config.roleid + '=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';
                    document.cookie = config.userid + '=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';
                    document.cookie = config.locationId + '=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';
                    document.cookie = config.newsTypeId + '=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';
                    document.cookie = config.UserName + '=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';
                    document.cookie = "Password" + '=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';
                    document.cookie = "EncriptedName" + '=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';
                    document.cookie = "FoldersInfo" + '=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';

                }

                if ($.trim(helper.getCookie('user-id')) == '') {
                    window.location.href = '/Login#/index';
                }
                $.when(manager.news.loadInitialDataNMS())
                     .done(function (responseData) {
                         binder.bindPreLoginViews();
                         vm.shell.init();
                         vm.home.init();
                         vm.taggedusers.init();
                         vm.tagnews.init();
                         vm.copyWritereditorial.init();
                         vm.reportnews.init();
                         presenter.toggleActivity(true);
                         var f_list = JSON.parse(helper.getCookie('FoldersInfo'));
                         for (var i = 0; i < f_list.length ; i++)
                         {
                             f_list[i].isProgramVisible = ko.observable(true);
                         }
                         if (helper.getCookie('CachedFilters') && helper.getCookie('CachedFilters').length > 0) {
                             setTimeout(function () {
                                 var cache_filters = JSON.parse(helper.getCookie('CachedFilters'));
                                 for (var i = 0; i < cache_filters.length; i++) {
                                     $("#FID" + parseInt(cache_filters[i].FilterId)).click();
                                 }
                             }, 500);
                         }

                         appdata.foldersList(f_list);
                         if (appdata.foldersList().length > 0) {
                             var width = (400 * (appdata.foldersList().length + 1))+4;
                             $(".foldersCategoryCss").width(parseInt(width) + 'px');
                         }

                         switch (appdata.currentUser().userType) {
                             case e.UserType.controllerInput:
                                 
                                 vm.topicselection.init();
                             
                                 break;
                          
                             case e.UserType.Controlleroutput:

                                 vm.topicselection.init();
                               
                                 break;
                                 
                                 if (appdata.currentUser().userType != e.UserType.copyWriter && appdata.currentUser().userType != e.UserType.editorial) {
                                     setTimeout(function () {
                                         presenter.toggleActivity(false);
                                     }, 100);
                         }
                                 break;
                         }
                         $.when(manager.newsfile.loadInitialData()).done(function (responseData) {
                             if (appdata.currentUser().userType != e.UserType.tagreport) {
                                 manager.newsfile.newsFileProducerPolling();
                                 timer.start();
                             }
                             appdata.refreshGenericList(!appdata.refreshGenericList());
                         });
                         
                         setTimeout(function () {
                             binder.bindPostLoginViews();
                             if (appdata.currentUser().userType != e.UserType.copyWriter && appdata.currentUser().userType != e.UserType.editorial) {
                                 presenter.toggleActivity(false);
                             }
                         }, 100);
                     })
                     .fail(function (responseData) {
                     });
                
   
                $.when(manager.usermanagement.getUsersByWorkRoleId(appdata.currentUser().userType))
                         .done(function (responseData) {
                             dc.users.fillData(responseData.Data);
                         })
                         .fail(function () {
                             config.logger.error("No Data Fetch");
                         });

                $.when(manager.usermanagement.getUsersByWorkRoleId(e.UserType.FReporter))
                        .done(function (responseData) {
                            dc.users.fillData(responseData.Data);
                        })
                        .fail(function () {
                            config.logger.error("No Data Fetch");
                        });
            };

        return {
            run: run
        }
    });