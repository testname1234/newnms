﻿define('route-config',
    ['config', 'router', 'vm', 'appdata', 'enum'],
    function (config, router, vm, appdata, e) {
        var
            logger = config.logger,

            register = function () {
                var routeData;
                
                if (appdata.currentUser().userType === e.UserType.controllerInput || appdata.currentUser().userType === e.UserType.Controlleroutput || appdata.currentUser().userType === e.UserType.taggedUser || appdata.currentUser().userType === e.UserType.filterSocial || appdata.currentUser().userType === e.UserType.tagreport || appdata.currentUser().userType === e.UserType.copyWriter || appdata.currentUser().userType === e.UserType.editorial) {
                    routeData = [

                      // Home routes
                      {
                          view: config.viewIds.nmsBol.homeView,
                          routes: [
                              {
                                  isDefault: true,
                                  route: config.hashes.nmsBol.home,
                                  title: 'Axact Media Solution',
                                  callback: vm.home.activate,
                                  group: '.route-top'
                              }
                          ]
                      },
                      {
                          view: config.viewIds.nmsBol.reportnews,
                          routes: [
                              {
                                  isDefault: true,
                                  route: config.hashes.nmsBol.reportnews,
                                  title: 'Axact Media Solution',
                                  callback: vm.reportnews.activate,
                                  //callback: vm.home.activate,
                                  group: '.route-top'
                              }
                          ]
                      },

                      // Top Selection routes
                      {
                          view: config.viewIds.nmsBol.topicSelectionView,
                          routes:
                              [{
                                  route: config.hashes.nmsBol.topicSelection,
                                  title: 'Axact Media Solution',
                                  callback: vm.home.activate,
                                  group: '.route-top'
                              }]
                      },

                                            // Home routes
                      {
                          view: config.viewIds.nmsBol.controllerOutput,
                          routes: [
                              {
                                  isDefault: true,
                                  route: config.hashes.nmsBol.controllerOutput,
                                  title: 'Axact Media Solution',
                                  callback: vm.home.activate,
                                  group: '.route-top'
                              }
                          ]
                      },

                                    // Tagger User
                      {
                          view: config.viewIds.nmsBol.taggerInput,
                          routes: [
                              {
                                  isDefault: true,
                                  route: config.hashes.nmsBol.taggerInput,
                                  title: 'Axact Media Solution',
                                  callback: vm.taggedusers.activate,
                                  group: '.route-top'
                              }
                          ]
                      },//

                       {
                           view: config.viewIds.nmsBol.tagnews,
                           routes: [
                               {
                                   isDefault: true,
                                   route: config.hashes.nmsBol.tagnews,
                                   title: 'Axact Media Solution',
                                   callback: vm.tagnews.activate,
                                   group: '.route-top'
                               }
                           ]
                       },

                      {
                          view: config.viewIds.nmsBol.taggedPending,
                          routes: [
                              {
                                  isDefault: true,
                                  route: config.hashes.nmsBol.taggedPending,
                                  title: 'Axact Media Solution',
                                  callback: vm.taggedusers.activate,
                                  group: '.route-top'
                              }
                          ]
                      },

                                   // FilterSocial
                      {
                          view: config.viewIds.nmsBol.filterSocial,
                          routes: [
                              {
                                  isDefault: true,
                                  route: config.hashes.nmsBol.filterSocial,
                                  title: 'Axact Media Solution',
                                  callback: vm.taggedusers.activate,
                                  group: '.route-top'
                              }
                          ]
                      
                      }, 
                          {
                                view: config.viewIds.nmsBol.tagreport,
                            routes: [
                                {
                                    isDefault: true,
                                    route: config.hashes.nmsBol.tagreport,
                                    title: 'Axact Media Solution',
                                    callback: vm.tagreport.activate,
                                    group: '.route-top'
                                }
                            ]
                          },
                           {
                                view: config.viewIds.nmsBol.copywriter,
                            routes: [
                                {
                                    isDefault: true,
                                    route: config.hashes.nmsBol.copywriter,
                                    title: 'Axact Media Solution',
                                    callback: vm.copyWritereditorial.activate,
                                    group: '.route-top'
                                }
                            ]
                        },

                    ];

                }

                // Invalid routes
                routeData.push({
                    view: '',
                    route: /.*/,
                    title: '',
                    callback: function () {
                        logger.error(config.toasts.invalidRoute);
                    }
                });

                for (var i = 0; i < routeData.length; i++) {
                    router.register(routeData[i]);
                }

                // Crank up the router
                router.run();
            };

        return {
            register: register
        };
    });