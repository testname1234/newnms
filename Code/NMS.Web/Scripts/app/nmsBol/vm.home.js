﻿define('vm.home',
    [
        'underscore',
        'jquery',
        'ko',
        'datacontext',
        'vm.topicselection',
        'router',
        'config',
        'model',
        'presenter',
        'manager',
        'appdata',
        'presenter',
        'utils',
        'moment',
        'messenger',
        'enum',
        'vm.contentviewer',
        'model.mapper',
        'model.userfilter',
        'model.reportnews',
        'timer',
        'vm.reportnews'
     
    ],
    function (_, $, ko, dc, topicSelectionVM, router, config, model, presenter, manager, appdata, presenter, utils, moment, messenger, e, contentviewer, mapper, userfilter, ReportNews, timer, VMreport) {
        var logger = config.logger;

        var
            // Properties
            // ------------------------

            hashes = config.hashes.production,
            rightTmplName = 'producer.home',
            imageFooterTmplName = 'producer.home',
            contentFooterTmplName = 'producer.home',
            contentTmplName = 'producer.home',
            templates = config.templateNames,
            pageIndex = ko.observable(1),
            selectedNewsId = appdata.selectedNewsId,
            searchKeywords = ko.observable('').extend({ throttle: config.throttle }),
            sourceFilterControl = new model.SourceFilterControl(),
            calendarControl = new model.CalendarControl(),
            bunchArray = ko.observableArray(),
            pendingStoriesArray = ko.observableArray(),
            searchGroupName = ko.observable(),
            currentBunch = ko.observable(new model.Bunch()),
            currentView = ko.observable('listView'),
            selectedAudioClip = ko.observable(),
            audioComments = ko.observable(),
            isUpdateScroll = ko.observable(false),
            isStoreScroll = ko.observable(false),
            viewBreakingNews = ko.observable(false),
            currentBreakingNews = ko.observable(),
            xmlParserObj = new model.XmlParser(),
            scrollPos = 0,
            reportnewsView = new ReportNews('urduRN', 'englishRN', ['.leftPanel .editor input[type=text]', '.leftPanel .editor .jqte_editor', '.reportNewsEditor'], e.NewsType.Story, 'Report News', null, e.NewsFilterType.FieldReporter),
            tickerReporterControl = new model.TickerReporterControl(),
            newsFiles = ko.observableArray(),
            isNewsMarked = ko.observable(false),
            parentWindowCss = ko.observable(''),
            selectedNewsTicker = ko.observable(''),
            addNewTicker = ko.observable(false),
            foldersListCopyTo = ko.observableArray(),
            lastnewsFileID = '',
            currentNewsFile = ko.observable(),
            refreshAllNews = ko.observable(false),
            freshNewsFilesListOutput = ko.observableArray([]),
            freshNewsFilesListPortal = ko.observableArray([]),
            freshNewsFilesListReviewed = ko.observableArray([]),
            totalFilterSelected = ko.observable(7),
            tooltipData = ko.observable("Empty"),
            isClearALLSelected = ko.observable(false),
            isSortableRefresh = ko.observable(false),


            // Computed Properties
            // ------------------------

            newsCount = ko.computed({
                read: function () {
                    var allFilters = dc.filters.getObservableList();
                    var filter = _.filter(allFilters, function (obj) {
                        return obj.id === 78;
                    });

                    if (filter && filter.length)
                        return filter[0].newsCount();
                },
                deferEvaluation: true
            }),

           refreshSortAbleList = ko.computed({
                            read: function () {
                                isSortableRefresh();

                                setTimeout(function () {
                                    $(".ui-sortable").sortable('refresh');
                                }, 1000);

                                return '';
                            },
                            deferEvaluation: false
                        }),
            genericNewsFilesList = ko.computed({
                read: function () {
                    appdata.refreshGenericList();
                    if (appdata.foldersList()) {
                        var arr = [];
                        var dataArray = dc.newsFiles.getAllLocal();
                        var list = ko.observableArray();
                        for (var i = 0; i < appdata.foldersList().length; i++) {
                            if (appdata.foldersList()[i].FolderId != 0) {
                                var result = $.grep(dc.newsFiles.getAllLocal(), function (e) {
                                    if (e.heighlighs() && e.heighlighs().length > 0)
                                        e.isTitle(true);
                                    if (e.descriptionText() && e.descriptionText().length > 0)
                                        e.voiceOver(true);

                                    if (e.folderId == parseInt(appdata.foldersList()[i].FolderId)) {
                                        e.maxStoryCount(appdata.foldersList()[i].MaxStoryCount);
                                        e.minStoryCount(appdata.foldersList()[i].MinStoryCount);
                                        e.isProgramNews(true);
                                    }

                                    return (e.folderId == parseInt(appdata.foldersList()[i].FolderId) && !e.isDeleted());
                                });
                                result = sortBySequenceNo(result);
                                arr[i] = result;
                            }
                            resizeWindow();
                        }
                        isSortableRefresh(!isSortableRefresh());
                        return arr;
                    }
                },
                deferEvaluation: true
            }),
            newsGroups = ko.computed({
                read: function () {
                    var newsBunchArray = bunch();
                    if (newsBunchArray.length > 0) {
                        var arr = [];
                        for (var i = 0; i < appdata.dateCategories().length; i++) {
                            var tempObj = {};
                            var parsedDate = moment(appdata.dateCategories()[i]).calendar();
                            var bunchArray = _.filter(newsBunchArray, function (obj) {
                                return moment(obj.lastUpdateDate()).format('l') == moment(appdata.dateCategories()[i]).format('l');
                            });
                            tempObj["groupName"] = parsedDate;
                            tempObj["bunch"] = bunchArray;
                            if (bunchArray.length > 0)
                                arr.push(tempObj);
                        }
                        return arr;
                    } else {
                        return [];
                    }
                },
                deferEvaluation: true
            }),
            bunch = ko.computed({
                read: function () {
                    var arr;
                    if (appdata.currentUser().userType == e.UserType.TickerWriter || appdata.currentUser().userType == e.UserType.TickerProducer || appdata.currentUser().userType == e.UserType.TickerManager || appdata.currentUser().userType == e.UserType.HeadlineProducer || appdata.currentUser().userType == e.UserType.controllerInput || appdata.currentUser().userType == e.UserType.FReporter || appdata.currentUser().userType === e.UserType.Controlleroutput) {
                        arr = bunchArray();
                    }
                    else {
                        var arr = bunchArray().slice(3, pageIndex() * 5);
                    }

                    var currentGroup = '';
                    var groupArray = [];
                    for (var i = 0; i < arr.length; i++) {
                        var temp = moment(moment(arr[i].publishTime()).format('l')).calendar();
                        if (currentGroup !== temp) {
                            currentGroup = temp;
                            var tempGroup = {
                                groupName: currentGroup,
                                index: i
                            };
                            if (i > 0) {
                                groupArray.push(tempGroup);
                            }
                            else {
                                searchGroupName(tempGroup.groupName);
                            }
                        }
                    }
                    for (var i = 0; i < groupArray.length; i++) {
                        var tempObj = new model.Bunch();
                        tempObj.isGroupName = true;
                        tempObj.groupName = groupArray[i].groupName;
                        arr.splice(groupArray[i].index + i, 0, tempObj);
                    }
                    if (arr.length <= 0) {
                        searchGroupName('');
                    }
                    //if (parent.window && parent.window.document && $(parent.window.document.getElementById('createNewsPopup')).hasClass('isNews')) {
                    //    addToNewsBucket(arr[0].topNews());
                    //}
                    return arr;
                },
                deferEvaluation: true
            }),
            newsDragIsReadOnly = ko.computed({
                read: function () {
                    if (appdata.currentUser().userType === e.UserType.TickerProducer || appdata.currentUser().userType === e.UserType.TickerWriter) {
                        return true;
                    }
                    return false;
                },
                deferEvaluation: true
            }),
            headlines = ko.computed({
                read: function () {
                    if (bunchArray().length > 0) {
                        var arr = bunchArray();
                        arr = arr.slice(0, 3);

                        var tempObj = {};
                        var defaultNewsObj = new model.News();
                        defaultNewsObj.isNullo = true;

                        if (arr && arr.length > 0) {
                            if (arr[0] && arr[0].topNews()) {
                                arr[0].forceRefresh(!arr[0].forceRefresh());
                                tempObj["first"] = arr[0].topNews();
                            }
                            else
                                tempObj["first"] = defaultNewsObj;

                            if (arr[1] && arr[1].topNews()) {
                                arr[1].forceRefresh(!arr[1].forceRefresh());
                                tempObj["second"] = arr[1].topNews();
                            }
                            else
                                tempObj["second"] = defaultNewsObj;

                            if (arr[2] && arr[2].topNews()) {
                                arr[2].forceRefresh(!arr[2].forceRefresh());
                                tempObj["third"] = arr[2].topNews();
                            }
                            else
                                tempObj["third"] = defaultNewsObj;
                        } else {
                            tempObj["first"] = defaultNewsObj;
                            tempObj["second"] = defaultNewsObj;
                            tempObj["third"] = defaultNewsObj;
                        }

                        presenter.toggleActivity(false);

                        return tempObj;
                    } else {
                        presenter.toggleActivity(false);
                    }
                },
                deferEvaluation: true
            }),
            groupedPendingStories = ko.computed({
                read: function () {
                    lastUIRefreshDisplayTime = moment().toISOString();

                    var arr = [];
                    arr = pendingStoriesArray();

                    var timeCategories = utils.getTimeSlots(60);

                    var stories = [];

                    if (arr && arr.length > 0) {
                        for (var j = 0, len = arr.length; j < len; j++) {
                            var objectMap = {};
                            objectMap.GroupName = moment(arr[j].lastUpdateDate()).format("LL, hh:mm a");
                            objectMap.pendingStories = [];

                            var exist = utils.arrayIsContainObject(stories, objectMap, "GroupName");
                            if (!exist.flag) {
                                objectMap.pendingStories.push(arr[j]);
                                stories.push(objectMap);
                            }
                            else
                                stories[exist.objectIndex].pendingStories.push(arr[j]);
                        }
                    }

                    return stories;
                },
                deferEvaluation: true
            }),
            pendingStoryCount = ko.computed({
                read: function () {
                    var count = 0;
                    for (var i = 0; i < groupedPendingStories().length; i++) {
                        count += groupedPendingStories()[i].pendingStories.length;
                    }
                    return count;
                },
                deferEvaluation: true
            }),
            userPreferenceFilter = ko.computed({
                read: function () {
                    var arr = dc.UserFilter.getObservableList();
                    var temparr = [];
                    if (arr && arr.length > 0) {
                        for (var i = 0; i < arr.length; i++) {
                            if (arr[i].parentId === -1 && (arr[i].filterTypeId === e.NewsFilterType.Category || arr[i].filterTypeId < e.NewsFilterType.Category)) {
                                temparr.push(arr[i]);
                            }
                        }
                    }

                    return temparr;
                },
                deferEvaluation: true
            }),

            userCategorizedFilter = ko.computed({
                read: function () {
                    var arr = dc.UserFilter.getObservableList();
                    var temparr = [];
                    if (arr && arr.length > 0) {
                        for (var i = 0; i < arr.length; i++) {
                            if (arr[i].parentId === -1 && arr[i].filterTypeId === 13) {
                                arr[i].isVisible(true);
                                if (arr[i].children().length == 0) {
                                    arr[i].changeSign('');
                                }
                                temparr.push(arr[i]);
                            }
                        }
                    }
                    return temparr;
                },
                deferEvaluation: true
            }),

            userSourceFilter = ko.computed({
                read: function () {
                    appdata.isUserSourceFilterChange();
                    var arr = dc.UserFilter.getObservableList();
                    var temparr = [];
                    if (arr && arr.length > 0) {
                        for (var i = 0; i < arr.length; i++) {
                            if (arr[i].parentId === -1 && arr[i].filterTypeId != 13) {
                                arr[i].isVisible(true);
                                if (arr[i].children().length == 0) {
                                    arr[i].changeSign('');
                                }
                                temparr.push(arr[i]);
                            }
                        }
                    }
                    return temparr;
                },
                deferEvaluation: true
            }),

            lastUIRefreshDisplayTime = ko.observable(moment(appdata.lastUIRefreshTime).fromNow()),

            // Methods
            // ------------------------

            submitNews = function () {

                reportnewsView.tickerLines(tickerReporterControl.tickerLineList());
                reportnewsView.currentTicker(tickerReporterControl.currentTicker());
                if (tickerReporterControl.lastTicker && tickerReporterControl.lastTicker()) {
                    tickerReporterControl.tickerLineList.push({ Text: tickerReporterControl.lastTicker(), LanguageCode: "ur", SequenceId: tickerReporterControl.tickerLineList().length == 0 ? 1 : tickerReporterControl.tickerLineList().length + 1 });
                }
                reportnewsView.submit();
                tickerReporterControl.tickerLineList([])

            },

            sendToBroadCast = function () {
                //alert();
            },

            printProgram = function (data) {
                var reqObj = data;
                $.when(manager.newsfile.programToPrint(reqObj))
                .done(function (responseData) {
                    if (responseData.IsSuccess && responseData.Data && responseData.Data.length > 0) {
                        createPrintView(data, responseData.Data);
                    } else { logger.error("No Data Found"); }
                })
                .fail(function (responseData) {
                    logger.error("Request Failed");
                });
            },

        createPrintView = function (data, ResponseData) {
            var timeInterval = "00:00:00";
            $.each(appdata.foldersList(), function (i, v) {
                if (data.FolderId == v.FolderId) {
                    timeInterval = v.programInterval
                }
            })
            var resultHtml = createHtml(data, ResponseData, timeInterval);
            var mywindow = window.open('', 'BOL', 'width=850,height=600,centerscreen**');
            mywindow.document.write('<html><head><title>BOL</title>');
            mywindow.document.write("<style type='text/css'>@font-face{font-family:'Jameel Noori Nastaleeq';src:url(/content/fonts/jameel-noori-nastaleeq-regular.ttf) format('truetype')}</style></head><body onload='window.print();'>");
            //mywindow.document.write('<input type="button" id="btnPrint" value="Print" class="no-print" style="width:100px" onclick="window.print()" />');
            //mywindow.document.write('<input type="button" id="btnCancel" value="Cancel" class="no-print"  style="width:100px" onclick="window.close()" />');
            mywindow.document.write(resultHtml.innerHTML);
            mywindow.document.write('</body></html>');
            mywindow.document.close(); // necessary for IE >= 10
            mywindow.focus(); // necessary for IE >= 10
            //mywindow.print();
            //mywindow.close();
            return true;
        },

        createHtml = function (data, ResponseData, timeInterval) {
            var mainDiv = document.createElement("div");
            $(mainDiv).append('<h1 style="display: inline-block; margin:0px">' + data.FolderName + ' : ' + timeInterval + '</h1><span style="float:right; display: inline; line-height: 40px;"> ' + moment().format('MM/DD/YYYY h:mm a') + ' </span>');
            var innerDiv = document.createElement("div");
            for (var i = 0; i < ResponseData.length; i++) {
                var TitleHtml = document.createElement('h2')
                var DetailsHtml = document.createElement("p");
                TitleHtml.setAttribute("style", "font-family: 'Jameel Noori Nastaleeq', 'Nafees nastaleeq', 'Nafees Web Naskh';direction: rtl;font-size:30px");
                DetailsHtml.setAttribute("style", "font-family: 'Jameel Noori Nastaleeq', 'Nafees nastaleeq', 'Nafees Web Naskh'; direction: rtl; font-size:22px");
                if (ResponseData[i].Highlights) {
                    TitleHtml.appendChild(document.createTextNode((i + 1) + ") " + ResponseData[i].Highlights));
                } else {
                    TitleHtml.appendChild(document.createTextNode((i + 1) + ") No title found"))
                }
                if (ResponseData[i].FileDetails && ResponseData[i].FileDetails[0].Text) {
                    DetailsHtml.innerHTML = ResponseData[i].FileDetails[0].Text;
                } else { DetailsHtml.innerHTML = " No details found" }

                innerDiv.appendChild(TitleHtml);
                innerDiv.appendChild(DetailsHtml);
            }
            mainDiv.appendChild(innerDiv);
            return mainDiv;
        },

            setCurrentNews = function (data, isRelated) {
                if (data) {
                    if (isRelated)
                        topicSelectionVM.setCurrentNews(data);
                    else
                        topicSelectionVM.setCurrentNews(data.topNews());

                    router.navigateTo(config.hashes.production.topicSelection);
                    appdata.lastRoute(config.hashes.production.topicSelection);
                }

            },
            addToBucketNews = function () {
                var data = 1;
            },

            ShowBroadCastedDetails = function (data) {
                
                tooltipData("PLease wait..");

                if (data.parentId) {
                    $.when(manager.newsfile.getBroadcatedNewsFileDetail(data.parentId))
                   .done(function (successdata) {
                       var a = '';
                       if (successdata.Data) {
                           for (var i = 0; i <= successdata.Data.length - 1; i++) {
                               a += successdata.Data[i].ProgramName + ' ' + moment(new Date(successdata.Data[i].CreationDateStr)).format('lll') + '\n\n';
                           }
                           //debugger;
                           
                           tooltipData(a);
                       }

                   })
                }
                else {
                    $.when(manager.newsfile.getBroadcatedNewsFileDetail(data.id))
                   .done(function (successdata) {
                       if (successdata) {
                           tooltipData(successdata);
                       }

                   })
                }
            },

            addToNewsBucket = function (data) {
                if (appdata.currentUser().userType == e.UserType.TickerProducer || appdata.currentUser().userType == e.UserType.TickerWriter) {
                    fillTicker(data);
                    return;
                }
                if (data.isUsed()) {
                    var temp;
                    var allNewsBucket = dc.newsBucketItems.getAllLocal();
                    for (var i = 0; i < allNewsBucket.length; i++) {
                        if (allNewsBucket[i].news.id === data.id) {
                            temp = allNewsBucket[i];
                            break;
                        }
                    }
                    if (temp)
                        manager.production.removeStoryOnServer(temp);
                }
                else
                    manager.news.addToNewsBucket(data);
            },
            setCurrentPendingStory = function (data, parentContext) {
                if (data) {
                    7
                    var arr = groupedPendingStories();
                    var sortedPending = [];
                    for (var i = 0; i < arr.length; i++) {
                        for (var j = 0; j < arr[i].pendingStories.length; j++) {
                            sortedPending.push(arr[i].pendingStories[j]);
                        }
                    }
                    var currentIndex = 0;
                    for (var i = 0; i < sortedPending.length; i++) {
                        pendingStory = sortedPending[i];
                        if (data.id === pendingStory.id) {
                            currentIndex = i;
                            break;
                        }
                    }
                }
            },
            AddNewEvent = function () {
                VMreport.addEvent();
                config.hashes.nmsBol.reportnewsName = 'Event Add';
                router.navigateTo(config.hashes.nmsBol.reportnews);
                
            },
            initializeBunchGenerator = function () {
                setTimeout(function polling() {
                    $.when(manager.news.getFilteredBunches(calendarControl.currentOption()))
                    .done(function (data) {
                        if (data) {
                            if (data.PageIndex)
                                pageIndex(data.PageIndex);
                            if (data.Bunches)
                                bunchArray(data.Bunches);
                    }
                        if (lastUIRefreshDisplayTime)
                            lastUIRefreshDisplayTime(moment(appdata.lastUIRefreshTime).fromNow());

                        setTimeout(polling, config.displayPollingInterval * 1000);
                    })
                    .fail(function () {
                        if (lastUIRefreshDisplayTime)
                            lastUIRefreshDisplayTime(moment(appdata.lastUIRefreshTime).fromNow());

                        setTimeout(polling, config.displayPollingInterval * 1000);
                });
                }, config.displayPollingInterval * 1000);
            },
            initializePendingStoryGenerator = function () {
                setTimeout(function pendingStoryPolling() {
                    $.when(manager.production.getFilteredPendingStories(calendarControl.currentOption()))
                    .done(function (data) {
                        if (data) {
                            if (data.PageIndex)
                                pageIndex(data.PageIndex);
                            if (data.pendingStories)
                                pendingStoriesArray(data.pendingStories);
                    }
                        setTimeout(pendingStoryPolling, config.displayPollingInterval * 1000);
                    })
                    .fail(function () {
                        setTimeout(pendingStoryPolling, config.displayPollingInterval * 1000);
                });
                }, config.displayPollingInterval * 1000);
            },
            selectNews = function (data) {
                if (appdata.currentUser().userType == e.UserType.TickerProducer || appdata.currentUser().userType == e.UserType.TickerWriter) {
                    fillTicker(data);
                }
                else
                    addToNewsBucket(data);

                // manager.news.selectNews(data);
                // appdata.isStorySavedDirty(true);
            },
            fillTicker = function (data) {
                var reqObj =[];
                reqObj.news = ko.observable(data);
                var ticker = mapper.ticker.fromDto(reqObj);
                contentviewer.twc.tickerPopupInputVisible(false);
                contentviewer.twc.cssClass('showleftpanel');
                contentviewer.setCurrentTicker(ticker);
            },
            toggleStoryFilter = function (data) {
                manager.production.toggleStoryFilter(data);
                pageIndex(1);
                appdata.lpStartIndex = 0;
                manager.news.setDataChangeTime();
            },
            toggleFilter = function (data, isExtraFilter) {

                if (data != -1) {
                    dc.newsFiles.clearAllNews(true);
                    appdata.clearAll(!appdata.clearAll());
            }

                if (appdata.selectedNewsFile())
                {
                    closeDetailView();
                }
                    
                appdata.newsFilesLastUpdateDate = '';
                manager.news.toggleFilter(data, isExtraFilter);

                appdata.lpPageCount = 50;
                appdata.lpStartOffSet = 0;

                pageIndex(1);
                appdata.lpStartIndex = 0;
                manager.news.setDataChangeTime();
                appdata.refreshGenericList(!appdata.refreshGenericList());
                if (data != -1) {
                manager.newsfile.newsFileFilterSelected();
                }


                //// total count of selected filters /////
                var sFilters = appdata.selectedSourceFilters().length + appdata.selectedCategoryFilters().length;
                if (sFilters == 1 && appdata.selectedSourceFilters() == 78)
                    sFilters = 0;
                totalFilterSelected(sFilters);
            },
            resetFilters = function () {

                freshNewsFilesListPortal([]);
                freshNewsFilesListReviewed([]);
                freshNewsFilesListOutput([]);

                freshNewsFilesListPortal.valueHasMutated();
                freshNewsFilesListReviewed.valueHasMutated();
                freshNewsFilesListOutput.valueHasMutated();
                dc.newsFiles.clearAllNews(true);

                isClearALLSelected(true);
                if (appdata.selectedNewsFile())
                {
                    $("#newsDetailsViewIDd").toggle("slide");
                    $("#newsDetailsLableIDd").toggle("slide");
                    setTimeout(function () {
                        var width = (400 * (appdata.foldersList().length + 1)) + 4;
                        $(".foldersCategoryCss").width(parseInt(width) + 'px');
                    }, 500);
                    lastnewsFileID = '';

                    appdata.selectedNewsFile('');
                }

                //$("#newsDetailsViewIDd").toggle("slide");
                //$("#newsDetailsLableIDd").toggle("slide");


                //setTimeout(function () {
                //    var width = (400 * (appdata.foldersList().length + 1)) + 4;
                //    $(".foldersCategoryCss").width(parseInt(width) + 'px');
                //}, 500);


                appdata.refreshGenericList(!appdata.refreshGenericList());


                appdata.selectedCategoryFilters([]);
                appdata.selectedExtraFilters([]);
                appdata.selectedSourceFilters([]);
                appdata.newsFilesLastUpdateDate = '';
                sourceFilterControl.currentFilter(null);
                searchKeywords('');
                appdata.lpPageCount = 50;
                appdata.lpStartOffSet = 0;
                calendarControl.currentOption('lastmonth');

                appdata.lpFromDate = calendarControl.fromDate();
                appdata.lpToDate = calendarControl.toDate();

                $('.nav-step1 li a').removeClass('active-category-filter');  //Category
                $('.mainFilter.hideFilters').removeClass('newsActive'); //Source
                $('.navigationNews li ul li').removeClass('active-filter'); //Source Children
                $('.onoffswitch .onoffswitch-label').removeClass('active'); //Switches
                $('#top-rated-filter').removeClass('active'); //top Rated
                $('.calenderRight').find('.active').removeClass('active');
                $('.calenderRight').find('.lastMonth').addClass('active');

                _.filter(dc.filters.getAllLocal(), function (item) {
                    return item.selectedChildCount(0);
                });

                appdata.selectedSourceFilters.push(78);
                appdata.isAllNewsFilterSelected(true);
                manager.news.setDataChangeTime();
                manager.newsfile.newsFileProducerPolling();



                //setTimeout(function () {
                //    var width = (400 * (appdata.foldersList().length + 1)) + 4;
                //    $(".foldersCategoryCss").width(parseInt(width) + 'px');
                //}, 500);
                lastnewsFileID = '';

                //// total count of selected filters /////
                var sFilters = appdata.selectedSourceFilters().length + appdata.selectedCategoryFilters().length;
                if (sFilters == 1 && appdata.selectedSourceFilters() == 78)
                    sFilters = 0;
                totalFilterSelected(sFilters);

                document.cookie = "CachedFilters" + '=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';
            },
            setCurrentBunchFilters = function (data) {
                if (currentBunch().selectedFilters().indexOf(data) == -1)
                    currentBunch().selectedFilters.push(data);
                else {
                    var index = currentBunch().selectedFilters.indexOf(data);
                    currentBunch().selectedFilters.splice(index, 1);
            }
            },
            setCurrentBunch = function (data) {
                currentBunch(data);
                if (currentBunch().showfilters())
                    currentBunch().showfilters(false);
                else
                    currentBunch().showfilters(true);

                manager.news.getNewsBunch(currentBunch());
            },
            clickAttachment = function () {
                if (appdata.currentUser().userType == e.UserType.FReporter) {
                    $('.editorUploader')[1].click();
                }
                else {
                    $('.editorUploader').click();
            }

            },
            clickCameramen = function () {
                event.stopPropagation();
                $('.cameraman').toggleClass('active');
            },
            openMediaSelection = function () {
                contentviewer.mediaSelectionUrl(config.hashes.production.topicSelection + '?isiframe');
                $("#content-viewer-main").children('#contentviewer-overlay').addClass('warningOverlay');
                contentviewer.isMediaViewVisibleExplorer(true);
                appdata.isDefaultTab(!appdata.isDefaultTab());
                // document.getElementById('popupIframeFExp').contentWindow.myFunction();
            },
            loadNextPage = function () {
                presenter.toggleActivity(true);
                appdata.newsFilesLastUpdateDate = '';
                if (appdata.currentUser().userType == e.UserType.controllerInput || appdata.currentUser().userType === e.UserType.Controlleroutput) {
                    manager.newsfile.newsFileProducerPolling(true);
            }
            },
            switchViewTicker = function () {
                if (router.currentHash().indexOf('home') != -1) {
                    router.navigateTo(config.hashes.production.bureauTicker);
                } else if (router.currentHash().indexOf('bureauticker') != -1) {
                    router.navigateTo(config.hashes.production.home);
            }
            },
            switchView = function myfunction() {
                if (appdata.currentUser().userType == e.UserType.TickerProducer || appdata.currentUser().userType == e.UserType.TickerWriter) {
                    if (router.currentHash().indexOf('home') != -1) {
                        router.navigateTo(config.hashes.production.myTicker);
                }
                } else {
                    if (router.currentHash().indexOf('home') != -1) {
                        //router.navigateTo(config.hashes.production.headlineUpdates);
                    } else if (router.currentHash().indexOf('headlineUpdates') != -1) {
                        router.navigateTo(config.hashes.production.home);
                }
            }
            },
            setCalendarDate = function () {

                appdata.lpFromDate = calendarControl.fromDate();
                appdata.lpToDate = calendarControl.toDate();
                appdata.lpStartIndex = 0;

                appdata.selectedNewsFile('');
                dc.newsFiles.clearAllNews(true);
                appdata.clearAll(!appdata.clearAll());
                appdata.newsFilesLastUpdateDate = '';

                if(parseInt(moment().diff(appdata.lpToDate, 'days')) == 0) {
                    timer.start();
            }

                if (appdata.currentUser().userType === e.UserType.NLE || appdata.currentUser().userType === e.UserType.StoryWriter) {
                    appdata.NLEfromDate(appdata.lpFromDate);
                    appdata.NLEtoDate(appdata.lpToDate);
            }
                manager.news.setDataChangeTime();
                manager.newsfile.newsFileFilterSelected();

            },
            subscribeEvents = function () {
                searchKeywords.subscribe(function (value) {
                    appdata.searchKeywords = value;
                    appdata.lpStartIndex = 0;
                    appdata.newsFilesLastUpdateDate = '';
                    dc.newsFiles.clearAllNews(true);
                    appdata.refreshGenericList(!appdata.refreshGenericList());
                    appdata.clearAll(!appdata.clearAll());
                    if (appdata.selectedNewsFile())
                    {
                        closeDetailView();
                    }

                    manager.newsfile.newsFileProducerPolling();
                    manager.news.setDataChangeTime();
            });

                pageIndex.subscribe(function (value) {
                    appdata.pageIndex = value;
                    manager.news.setDataChangeTime();
            });
                appdata.currentResource.subscribe(function (value) {
                    if (value && value != '') {

                        if (topicSelectionVM.isIframeVisible()) {
                            var resource = value;
                            window.parent.window.postMessage(value, "*");
                        }
                        else {
                            var value = JSON.parse(value);
                            if (value.Guid) {
                                var res = mapper.resource.fromDto(value);
                                var arr = _.filter(reportnewsView.uploader().mappedResources(), function (resource) {
                                    return resource.guid === res.guid;
                            });
                                if (arr && arr.length > 0) {
                                    reportnewsView.uploader().mappedResources().remove(arr[0]);
                                    var arr1 = _.filter(reportnewsView.uploader().uploadedResources(), function (upresource) {
                                        return upresource.Guid === res.guid;
                                });
                                    if (arr1 && arr1.length > 0) {
                                        reportnewsView.uploader().uploadedResources().remove(arr1[0]);
                                }
                                    reportnewsView.uploader().uploadedResources.valueHasMutated();
                                    reportnewsView.uploader().resources(reportnewsView.uploader().uploadedResources());
                                }
                                else {
                                    reportnewsView.uploader().mappedResources().push(res);
                                    reportnewsView.uploader().uploadedResources().push(value);
                                    reportnewsView.uploader().mappedResources()[reportnewsView.uploader().mappedResources().length - 1].type = res.type;
                                    reportnewsView.uploader().mappedResources()[reportnewsView.uploader().mappedResources().length -1].IgnoreMeta = 1;
                                    reportnewsView.uploader().uploadedResources.valueHasMutated();
                                    reportnewsView.uploader().resources(reportnewsView.uploader().uploadedResources());
                            }
                            }
                            else {
                                var obj = {
                                        Guid: value.guid,
                                        ResourceId: value.id,
                                        ResourceTypeId: value.type,
                                    IgnoreMeta: 1
                            }
                                var arr = _.filter(reportnewsView.uploader().mappedResources(), function (resource) {
                                    return resource.guid === value.guid;
                            });
                                if (arr && arr.length > 0) {
                                    reportnewsView.uploader().mappedResources().remove(arr[0]);
                                    var arr1 = _.filter(reportnewsView.uploader().uploadedResources(), function (upresource) {
                                        return upresource.Guid === value.guid;
                                });
                                    if (arr1 && arr1.length > 0) {
                                        reportnewsView.uploader().uploadedResources().remove(arr1[0]);
                                }
                                    reportnewsView.uploader().uploadedResources.valueHasMutated();
                                    reportnewsView.uploader().resources(reportnewsView.uploader().uploadedResources());
                                }
                                else {
                                    reportnewsView.uploader().mappedResources().push(value);
                                    reportnewsView.uploader().uploadedResources().push(obj);
                                    reportnewsView.uploader().mappedResources()[reportnewsView.uploader().mappedResources().length - 1].type = value.type;
                                    reportnewsView.uploader().mappedResources()[reportnewsView.uploader().mappedResources().length -1].IgnoreMeta = 1;
                                    reportnewsView.uploader().uploadedResources.valueHasMutated();
                                    reportnewsView.uploader().resources(reportnewsView.uploader().uploadedResources());
                            }
                        }
                    }
                }
            });

            },
            displayAudioToolTip = function (data) {
                selectedAudioClip('../../wildlife.mp3');
                audioComments(data.title());
            },
            closeAudioToolTip = function () {
                selectedAudioClip('');
            },
            resetContentFilter = function () {
                $('#allNews').removeClass('active');
                $('#allNews').addClass('active');
                $('#news').removeClass('active');
                $('#packages').removeClass('active');
                $('#verified-filter').removeClass('active');
                $('#recommended-filter').removeClass('active');
                $('#top-rated-filter').removeClass('active');
                $('#top-executed-filter').removeClass('active');
                $('#most-recent-filter').removeClass('active');

                appdata.selectedExtraFilters([]);
                toggleFilter(-1, true);
            },
            searchEventNews = function (data) {
                if (data) {
                    if (data.isSelected()) {
                        data.isSelected(false);
                        appdata.searchKeywords = '';
                        manager.news.setDataChangeTime();
                    } else {
                        _.filter(sourceFilterControl.events(), function (obj) {
                            return obj.isSelected(false);
                    });
                        data.isSelected(true);
                        appdata.searchKeywords = data.searchTags;
                        manager.news.setDataChangeTime();
                }
            }
            },
            setCurrenBreakingNews = function (data) {
                contentviewer.setCurrentAlertContent(data, 2);
            },
            setCurrentContent = function (data, options) {
                try {
                    if (data && options) {
                        if (options.isReportedContent) {
                            contentviewer.setCurrentContent(reportnewsView.uploader().mappedResources(), data);
                    }
                }
                } catch (e) {
                    //console.log(e.message);
            }
            },
            hideBreakingNews = function () {
                viewBreakingNews(false);
                currentBreakingNews('');
            },
            setFullScreenMode = function () {
                //  var el = document.body;
                //  var requestMethod = el.requestFullScreen || el.webkitRequestFullScreen
                //|| el.mozRequestFullScreen || el.msRequestFullScreen;
                //  if (requestMethod) {
                //      requestMethod.call(el);
                //  } else if (typeof window.ActiveXObject !== "undefined") {
                //      var wscript = new ActiveXObject("WScript.Shell");
                //      if (wscript !== null) {
                //          wscript.SendKeys("{F11}");
                //      }
                //  }
            },
            storeScrollPosition = function () {
                //appdata.producerScrollTop($('#home-view').children('div').children('.mCSB_container').css('top'));
            },
            setScrollPosition = function () {
                //scrollPos = appdata.producerScrollTop();
                //scrollPos = scrollPos.replace('px', '');
                //$('#home-view').children('div').children('.mCSB_container').css('top', parseInt(scrollPos));
            },

            ReportNewsAgainstProgram = function (data) {
                var d = data;
                VMreport.copyAgainstFolder(d.FolderId);
                //config.hashes.nmsBol.reportnewsName = 'Event Add';
                router.navigateTo(config.hashes.nmsBol.reportnews);

            },

            hideNextPrevNLE = function () {
                $(".prev").css({ 'display': 'none'
            });
                $(".next").css({ 'display': 'none'
            });
            },
            toggleArchivalSearch = function () {
                appdata.isArchival(!appdata.isArchival());
                pageIndex(1);
                appdata.lpStartIndex = 0;
                manager.news.getMoreNews();
                manager.news.setDataChangeTime();
            },
            downLoadResource = function (Url) {
                var link = document.createElement("a");
                link.download = Url()
                link.href = Url();
                link.click();
            },
            submitMetaDataByUser = function () {
                var MetaTypeId = e.MetaTypeId.Filters;
                var result = dc.UserFilter.getAllLocal();
                var arry =[];
                for (var i = 0; i < result.length ; i++) {
                    var obj = {
                            MetaTypeId: MetaTypeId,
                            MetaName: result[i].name,
                            MetaValue: result[i].id,
                            UserId: appdata.currentUser().id,
                            isAllowed: result[i].isNotAllowed(),
                            isDiscarded: result[i].isDiscarded()
                };
                    arry.push(obj);
            }

                var MetaData = {
                MetaData: arry
            };
                manager.news.submitMetaDataByUser(MetaData);
            },
            getAllFilters = function () {
                manager.news.getAllFilters();
            },

            editSelected = function (data) {
                router.navigateTo(config.views.fieldreporter.reportNews.url);
                data.topNews().isNewsEdit(true);
                reportnewsView.newsSelectedProduction(data.topNews());

            },
           markVerificationStatus = function (isVerified, data) {
               isNewsMarked(true);
               var reqObj = {
                   NewsFileId: data.id,
                       IsVerified: isVerified,
                       CreatedBy: appdata.currentUser().id
           };
               $.when(manager.newsfile.markNewsFileStatus(reqObj))
                .done(function (responseData) {
                    if (router.currentHash() === config.hashes.nmsBol.home) {

                        dc.newsFiles.getLocalById(data.id).isVerified(isVerified);

                        var curr_status = dc.newsFiles.getLocalById(data.id).newsStatus();
                        var curr_isVerified = dc.newsFiles.getLocalById(data.id).isVerified();
                        dc.newsFiles.getLocalById(data.id).editModeON(false);

                        if ((curr_status != null && curr_status != 0 && curr_isVerified != 'initial')) {
                            $("#" +data.id).fadeOut(1000, function () {
                                dc.newsFiles.getLocalById(data.id).isStatusFinal(true);
                                refreshAllNews(!refreshAllNews());
                        });
                            appdata.selectedNewsFile('');
                    }
                    }
                    else {
                        dc.newsFiles.getLocalById(data.id).isVerified(isVerified);
                        dc.newsFiles.getLocalById(data.id).editModeON(false);
                        refreshAllNews(!refreshAllNews());
                }

               })
                .fail(function (responseData) {
           });
            },
            markNewsFileStatus = function (data, newsfilestatus) {


                isNewsMarked(true);
                var reqObj = {
                    NewsFileId: data.id,
                        StatusId: newsfilestatus,
                        CreatedBy: appdata.currentUser().id
            };
                $.when(manager.newsfile.markNewsFileStatus(reqObj))
                 .done(function (responseData) {

                     if (router.currentHash() === config.hashes.nmsBol.home) {

                         dc.newsFiles.getLocalById(data.id).newsStatus(parseInt(newsfilestatus));
                         dc.newsFiles.getLocalById(data.id).editModeON(false);

                         var curr_status = dc.newsFiles.getLocalById(data.id).newsStatus();
                         var curr_isVerified = dc.newsFiles.getLocalById(data.id).isVerified();

                         if (curr_status != null && curr_status != 0 && (curr_isVerified != 'initial') && data.sourceTypeId() != 7 && data.sourceTypeId() != 10) {
                             $("#" +data.id).fadeOut(600, function () {
                                 dc.newsFiles.getLocalById(data.id).isStatusFinal(true);
                                 refreshAllNews(!refreshAllNews());
                         });
                         }
                         else {
                             if (data.sourceTypeId() === 7 || data.sourceTypeId() === 10) {
                             $("#" +data.id).fadeOut(600, function () {
                                 dc.newsFiles.getLocalById(data.id).isStatusFinal(true);
                                 refreshAllNews(!refreshAllNews());
                             });
                         }
                     }
                     }
                     else {
                         dc.newsFiles.getLocalById(data.id).newsStatus(parseInt(newsfilestatus));
                         dc.newsFiles.getLocalById(data.id).editModeON(false);
                         refreshAllNews(!refreshAllNews());
                 }


                     if (appdata.currentUser().userType === e.UserType.Controlleroutput || router.currentHash() === config.hashes.nmsBol.home) {
                         refreshAllNews(!refreshAllNews());
                 }


                     appdata.selectedNewsFile('');
                })
                 .fail(function (responseData) {
            });
            },
            setNewsInEditMode = function (data) {
                if (data && data.id) {
                    isNewsMarked(true);
                    dc.newsFiles.getLocalById(data.id).editModeON(true);

            }

            },
            copyNewsFileToSelectedFolder = function (newsfile, folderId) {
                var reqObj = {
                        NewsFileId: newsfile.id,
                        FolderId: folderId,
                        CreatedBy: appdata.currentUser().id
            }

                $.when(manager.newsfile.copyNewsFileToFolder(reqObj))
                  .done(function (responseData) {
                      if (responseData) {
                  }
                })
                  .fail(function (responseData) {
            });
            },

            EditNewsFileFromCinput = function (data) {
                if (data && data.id) {
                    var newsId = data.id;
                    //VMupdatenews
                    var mNews = new model.News();
                    mNews.id = newsId;
                    config.hashes.nmsBol.reportnewsName = 'Update News';
                    router.navigateTo(config.hashes.nmsBol.reportnews);
                    VMreport.model.isUpdating(true);
                    VMreport.ActAsUpdateNews(newsId);
                }
            },


            editNewsFile = function (newsFile) {
                if (!isNewsMarked()) {
                    var checkProgramNewsFile = _.filter(appdata.foldersList(), function (obj) {
                        return obj.FolderId == newsFile.folderId;
                });
                    if (checkProgramNewsFile.length === 0) {
                        var reqObj = {
                                newsFileId: newsFile.id
                    };

                        if (lastnewsFileID != newsFile.id) {
                            $.when(manager.newsfile.getnewsFileDetail(reqObj))
                             .done(function (responseData) {
                                 if (responseData) {
                                     newsFiles(dc.newsFiles.getAllLocal())
                                     var newsEntity = mapper.newsFile.fromDto(responseData);

                                     if (responseData.Coverage || !responseData.Coverage)
                                         newsEntity.Coverage = responseData.Coverage;
                                     if (responseData.EventResource && responseData.EventResource.length > 0)
                                         newsEntity.EventResource = responseData.EventResource;
                                     if (responseData.EventTypeName)
                                         newsEntity.EventTypeName = responseData.EventTypeName;
                                     if (responseData.EventType)
                                         newsEntity.EventType = responseData.EventType;
                                     if (responseData.EventReporter && responseData.EventReporter.length > 0)
                                     {
                                         newsEntity.EventReporter = responseData.EventReporter;
                                         for (var i = 0; i < newsEntity.EventReporter.length; i++)
                                         {
                                             newsEntity.EventReporter[i].Name = dc.users.getLocalById(newsEntity.EventReporter[i].ReporterId).name
                                         }
                                     }
                                     if (responseData.PublishTimeStr)
                                     {
                                         newsEntity.publishTimeStr(moment(responseData.PublishTimeStr).format('MMMM Do YYYY, h:mm:ss a')); // December 6th 2016, 5:57:18 pm
                                     }
                                     if (responseData.Tags && responseData.Tags.length)
                                         newsEntity.Tags = responseData.Tags;
                                     if (responseData.TaggedBy && responseData.TaggedBy)
                                         newsEntity.TaggedUser = ko.observable(dc.users.getLocalById(responseData.TaggedBy).name);
                                     if (responseData.Organization && responseData.Organization)
                                         newsEntity.Organization = responseData.Organization;
                                    
                                     
                                     appdata.selectedNewsFile(newsEntity);

                                     if (lastnewsFileID === '') {
                                         var width = (400 * (appdata.foldersList().length +2)) +6;
                                         $(".foldersCategoryCss").width(parseInt(width) + 'px');

                                         $("#newsDetailsViewIDd").toggle("slide");
                                         $("#newsDetailsLableIDd").toggle("slide");

                                 }
                                     if (lastnewsFileID != '' && lastnewsFileID != newsFile.id && !$("#newsDetailsViewIDd").is(':Visible')) {
                                         $("#newsDetailsViewIDd").toggle("slide");
                                         $("#newsDetailsLableIDd").toggle("slide");
                                 }
                                     lastnewsFileID = newsFile.id;
                             }
                            })
                             .fail(function (responseData) {
                        });
                        }
                        else {
                            $("#newsDetailsViewIDd").toggle("slide");
                            $("#newsDetailsLableIDd").toggle("slide");

                            setTimeout(function () {
                                var width = (400 * (appdata.foldersList().length +1)) +4;
                                $(".foldersCategoryCss").width(parseInt(width) + 'px');
                        }, 500);
                            lastnewsFileID = '';
                    }

                    }
                    else {
                        contentviewer.setCurrentProgramNewsFile(newsFile);

                }
            }
                isNewsMarked(false);
            },

          closeDetailView = function () {
              $("#newsDetailsViewIDd").toggle("slide");
              $("#newsDetailsLableIDd").toggle("slide");

              setTimeout(function () {
                    var width =(400 *(appdata.foldersList().length +1)) +4;
                    $(".foldersCategoryCss").width(parseInt(width) + 'px');
          }, 500);
              lastnewsFileID = '';

              appdata.selectedNewsFile('');
            },
          addNewsTikerByNewsFile = function (newsFile) {
              var data = {
                      newsFileId: newsFile.id
          }

              $.when(manager.ticker.createTickerBynewsFileId(data))
                 .done(function (responseData) {
                     if (responseData) {
                         addNewTicker(true);
                 }
                     addNewTicker(false);
              })
                 .fail(function (responseData) {
          });

            },

          sortDesc = function (arr) {
                      if (arr && arr.length > 1) {
                          arr = arr.sort(function (entityA, entityB) {
                              return new Date(entityB.creationDate()) -new Date(entityA.creationDate());
                      });
          }
                      return arr;
            },

         sortBySequenceNo = function (arr) {
                      if (arr && arr.length > 1) {
                          arr = arr.sort(function (entityA, entityB) {
                              return (entityA.sequenceNo()) -(entityB.sequenceNo());
                      });
         }
                      return arr;
            },
          newsFilesList = ko.computed({
                  read: function () {
                  var timeStamp = new Date();
                  appdata.clearAll();
                  appdata.refreshNewsFileList();
                      refreshAllNews();
                  appdata.newsFileLastUpdateDateObservable();
                  dc.newsFiles.getAllLocal();

                  if (router.currentHash() === config.hashes.nmsBol.home) {
                      appdata.lpStartOffSet = dc.newsFiles.getAllLocal().length;
                      var result = $.grep(dc.newsFiles.getAllLocal(), function (e) { return (!e.isStatusFinal() && !e.isDeleted());
                  });
                      freshNewsFilesListPortal(sortDesc(result));
              }

                  if (router.currentHash() === config.hashes.nmsBol.topicSelection) {
                      appdata.lpStartOffSet = dc.newsFiles.getAllLocal().length;
                      var result = $.grep(dc.newsFiles.getAllLocal(), function (e) { return (e.isStatusFinal() && !e.isDeleted());
                  });
                      freshNewsFilesListReviewed(sortDesc(result));
              }

                  if (appdata.currentUser().userType === e.UserType.Controlleroutput) {
                      for (var i = 0; i < appdata.foldersList().length; i++) {
                          if (i == 0)
                              var result = $.grep(dc.newsFiles.getAllLocal(), function (e) {
                                  if (e.folderId != parseInt(appdata.foldersList()[i].FolderId) && !e.isDeleted() && (e.programId == null || (typeof e.programId != 'undefined'))) {
                                      return e;
                              }

                          });
                          else
                              result = $.grep(result, function (e) {
                                  if (e.folderId != parseInt(appdata.foldersList()[i].FolderId) && !e.isDeleted() && (e.programId == null || (typeof e.programId != 'undefined'))) {
                                      return e;
                              }
                      });
                  }
                      if (result)
                          appdata.lpStartOffSet = result.length;

                      var tempArr = _.filter(result, function (obj) {
                          var existDate = new Date(obj.creationDate());

                          var hours = ((existDate.getSeconds() / 60) / 60) +(existDate.getMinutes() / 60) +(existDate.getHours());
                          var t_hours = ((timeStamp.getSeconds() / 60) / 60) +(timeStamp.getMinutes() / 60) +(timeStamp.getHours());

                          if (existDate.getDate() === timeStamp.getDate() && existDate.getMonth() === timeStamp.getMonth() && existDate.getYear() === timeStamp.getYear() && (t_hours - hours <= 4) && (obj.newsStatus() === 2 || obj.newsStatus() === 1)) {
                              return obj;
                          } else if(existDate.getDate() < timeStamp.getDate()) {
                              var hours = ((existDate.getSeconds() / 60) / 60) +(existDate.getMinutes() / 60) +(existDate.getHours());
                              var t_hours = (((timeStamp.getSeconds()) / 60) / 60) +(timeStamp.getMinutes() / 60) +(timeStamp.getHours() +24);

                              if ((t_hours - hours) <= 4 && (obj.newsStatus() === 2 || obj.newsStatus() === 1))
                                  return obj;

                      }
                  });

                  var excludedData = _.filter(result, function (obj) {
                          return (tempArr.indexOf(obj) < 0);
                  });
                  if (excludedData.length > 0) {
                   //   debugger;
                  }
                      freshNewsFilesListOutput([]);
                      tempArr.sort(function (a, b) {
                          var n = a.newsStatus() -b.newsStatus();
                          if (n !== 0) {
                                return n;
                          }
                          return new Date(b.creationDate()) -new Date(a.creationDate());
                       });
                      freshNewsFilesListOutput.push.apply(freshNewsFilesListOutput, tempArr);
                      freshNewsFilesListOutput.push.apply(freshNewsFilesListOutput, sortDesc(excludedData));
                      

                      
                  }
                  else {
                      _.filter(result, function (obj) {
                          return obj;
                  });
              }

                  var newsFiles = sortDesc(result);
                      newsFiles =[];
                  return newsFiles;
          },
              deferEvaluation: true
            }),

          copyFolderList = ko.computed({
                  read: function () {

          },
              deferEvaluation: true
            }),
          tickerslist = ko.computed({
                  read: function () {
                  var alltickers = dc.tickers.getAllLocal();
                      addNewTicker();
                  appdata.refreshTickers();
                  if (alltickers && alltickers.length) {
                      return alltickers;
              }
          },
              deferEvaluation: true
            }),

          displayResourceOnPopup = function (data, resourcesList) {
                var arr =[];
                arr.push(resourcesList);
                contentviewer.setCurrentContent(arr[0], data);
            },
          UpdateAllFoldersCss = function () {
              if (router.currentHash() === config.views.nmsBol.newsReviewed.url) {
                  var obj = {
                      freshNewsDivWidth: '55%',
                      headlineDivWidth: '40%',
                      bulletinADivWidth: '40%',
                      bulletinBDivWidth: '40%',
                      breakingDivWidth: '40%',
                      detailedView: '38.5%'
              }
                  parentWindowCss(obj);
          }
              if (router.currentHash() === config.views.nmsBol.NewsPortal.url) {
                  var obj = {
                      freshNewsDivWidth: '55%',
                      headlineDivWidth: '40%',
                      bulletinADivWidth: '40%',
                      bulletinBDivWidth: '40%',
                      breakingDivWidth: '40%',
                      detailedView: '38.5%'
              }
                  parentWindowCss(obj);
          }

              if (router.currentHash() === config.views.nmsBol.controllerOutput.url) {
                  var obj = {
                          freshNewsDivWidth: '721.99',
                      headlineDivWidth: '40%',
                      bulletinADivWidth: '40%',
                      bulletinBDivWidth: '40%',
                      breakingDivWidth: '40%',
                      detailedView: '34%'
              }
                  parentWindowCss(obj);
          }

            },

          arrangeNewsFilesofProgram = function (arrayPositions) {

              var SequenceList =[];
              for (var i = 0; i < arrayPositions.length ; i++) {
                  SequenceList.push(parseInt(arrayPositions[i]));
          }
              var reqObj = {
                      SequenceList: SequenceList
          }
              manager.newsfile.updateNewsFilesSequencing(reqObj);
              updateNewsFileSequencing();
            },
          updateNewsFileSequencing = function () {
              setTimeout(function () {
                  $(".ui-sortable").sortable('refresh');
          }, 500);
            },

          resizeWindow = function () {
              var toBeWidth = $($(".freshNewsWindow.newsplacementWindow")[0]).width();
              if (appdata.selectedNewsFile())
                  toBeWidth = toBeWidth * (2 + appdata.foldersList().length) + 20;
              if (!appdata.selectedNewsFile())
              {
                  if (isClearALLSelected() && appdata.selectedNewsFile()) {
                      $("#newsDetailsViewIDd").toggle("slide");
                      $("#newsDetailsLableIDd").toggle("slide");
                  }
                  isClearALLSelected(false);
                  toBeWidth = toBeWidth * (1 + appdata.foldersList().length) + 20;
              }
              if ($(".nmsFolderCategoryClass.clearfix.main-navsec.foldersCategoryCss.hdng").width != toBeWidth) {
                  setTimeout(function () {
                      $(".foldersCategoryCss").width(parseInt(toBeWidth) + 'px');
                  }, 500);                  
               }
            },

            markNewsFileDeleted = function (newsfile) {
                var reqObj = {
                        newsFileId: newsfile.id
            }
                manager.newsfile.markNewsFileDeleted(reqObj);
            },
            activate = function (routeData, callback) {

                appdata.selectedNewsFile('');
                messenger.publish.viewModelActivated({ canleaveCallback: canLeave
            });
                UpdateAllFoldersCss();

                var obj =[{ Name: 'Headline', FolderId: 1 }, { Name: 'BulletinA', FolderId: 1 }, { Name: 'BulletinB', FolderId: 1 }, { Name: 'Breaking', FolderId: 1
            }]
                dc.newsFolders.fillData(obj, {
                sort: true
            });
                var arr = dc.newsFolders.getAllLocal();
                foldersListCopyTo.push(arr[0]);

                window.setInterval(function () {
                    //$('.programInterval').toggle();
            }, 250);

            },
            canLeave = function () {
                if (appdata.currentHash != hashes.bureauTicker && appdata.currentHash != hashes.headlineUpdates && appdata.currentHash != hashes.home) {
                    if (appdata.isStorySavedDirty()) {
                        if (dc.stories.getByEpisodeId(appdata.currentEpisode().id) ().length > 0) {
                            manager.production.proceedToSequence();
                            appdata.isStorySavedDirty(false);
                    }
                }
            }
                //storeScrollPosition();
                return true;
            },
            init = function () {
                if (appdata.foldersList().length > 0) {
                    var width = (400 * (appdata.foldersList().length +1)) +4;
                    $(".foldersCategoryCss").width(parseInt(width) + 'px');

            }

                sourceFilterControl.currentFilter(-1);
                sourceFilterControl.isPopulated(1);
                sourceFilterControl.showExtraFilters(false);
                calendarControl.currentOption('lastmonth');

                if (appdata.currentUser().userType == e.UserType.controllerInput || appdata.currentUser().userType === e.UserType.Controlleroutput || appdata.currentUser().userType == e.UserType.FReporter || appdata.currentUser().userType === e.UserType.HeadlineProducer || appdata.currentUser().userType === e.UserType.TickerWriter || appdata.currentUser().userType === e.UserType.TickerProducer)
                    calendarControl.isNewsCropVisible(true);

                subscribeEvents();

                //toggleFilter(450, true);
                toggleFilter(-1, true);

                if (appdata.currentUser().userType == e.UserType.controllerInput || appdata.currentUser().userType == e.UserType.FReporter || appdata.currentUser().userType === e.UserType.HeadlineProducer || appdata.currentUser().userType === e.UserType.TickerWriter || appdata.currentUser().userType === e.UserType.TickerProducer)
                    initializeBunchGenerator();
                else {
                    initializePendingStoryGenerator();
                    appdata.teamCalendarControlReference(calendarControl);
            }

                if (appdata.currentUser().userType === e.UserType.HeadlineProducer) {
                    switchView();
            }

                UpdateAllFoldersCss();

            }
            function receivedParentData(event) {
            if (topicSelectionVM.isIframeVisible() && router.currentHash() != "#/production" && !appdata.isNewsCreated()) {
                var tempRes = event.data;
                var res = mapper.resource.fromDto(tempRes);
                reportnewsView.uploader().mappedResources().push(res);
                reportnewsView.uploader().uploadedResources().push(tempRes);
                reportnewsView.uploader().mappedResources()[reportnewsView.uploader().mappedResources().length - 1].type = res.type;
                reportnewsView.uploader().mappedResources()[reportnewsView.uploader().mappedResources().length -1].IgnoreMeta = 1;
                reportnewsView.uploader().uploadedResources.valueHasMutated();
                reportnewsView.uploader().resources(reportnewsView.uploader().uploadedResources());
            }
    };

    addEventListener("message", receivedParentData, false);

        function receivedParentNews(event) {
            if (appdata.isNewsCreated()) {
                var data = event.data;
                var item = JSON.parse(data);
                var arr = _.filter(bunch(), function (bItem) {
                    return bItem.id === item.BunchGuid;
            });
                if (arr && arr.length > 0) {
                    addToNewsBucket(arr[0].topNews());
            }
        }
    };

    addEventListener("message", receivedParentNews, false);

    $("iframe").load(function () {
            var frameContents;
            frameContents = $("#popupIframeNewsAdd").contents();
            frameContents.find("#shell-top-nav-view").hide();
            frameContents.find("#main").find('.reportNews').find('.editingSection').children('.mainnewstmpl').children('.bodySection').find('.editor').find('.newEditor').find('.iframeFE').addClass('hideExp');
            frameContents.find("#main").find('.reportNews').find('.editingSection').children('.mainnewstmpl').children('.bodySection').find('.editor').find('.newEditor').find('.iframeFE').hide()
    });

    $(window).resize(function () {
            setTimeout(resizeWindow, 500);
    });

        //$(window).hover(function (event, contextMenu) {
        //    //var mousePosition = {};
        //    //var menuPostion = {};
        //    //var menuDimension = {};

        //    //menuDimension.x = contextMenu.outerWidth();
        //    //menuDimension.y = contextMenu.outerHeight();
        //    //mousePosition.x = event.pageX;
        //    //mousePosition.y = event.pageY;

        //    //if (mousePosition.x + menuDimension.x > $(window).width() + $(window).scrollLeft()) {
        //    //    menuPostion.x = mousePosition.x - menuDimension.x;
        //    //} else {
        //    //    menuPostion.x = mousePosition.x;
        //    //}

        //    //if (mousePosition.y + menuDimension.y > $(window).height() + $(window).scrollTop()) {
        //    //    menuPostion.y = mousePosition.y - menuDimension.y;
        //    //} else {
        //    //    menuPostion.y = mousePosition.y;
        //    //}


        //    $(follow).offset({ left: event.pageX, top: event.pageY });
        //});


        // 
        //

        function allowDrop(ev) {
            ev.preventDefault();
    }

        function drag(ev) {
            ev.dataTransfer.setData("text", ev.target.id);
    }

        function drop(ev) {
            ev.preventDefault();
            var data = ev.dataTransfer.getData("text");
            ev.target.appendChild(document.getElementById(data));
    }

    return {
            activate: activate,
            canLeave: canLeave,
            appdata: appdata,
            bunch: bunch,
            selectNews: selectNews,
            setCurrentNews: setCurrentNews,
            setCurrentPendingStory: setCurrentPendingStory,
            templates: templates,
            init: init,
            sourceFilterControl: sourceFilterControl,
            calendarControl: calendarControl,
            hashes: hashes,
            headlines: headlines,
            loadNextPage: loadNextPage,
            toggleFilter: toggleFilter,
            toggleStoryFilter: toggleStoryFilter,
            searchKeywords: searchKeywords,
            selectedNewsId: selectedNewsId,
            newsGroups: newsGroups,
            newsCount: newsCount,
            resetFilters: resetFilters,
            lastUIRefreshDisplayTime: lastUIRefreshDisplayTime,
            searchGroupName: searchGroupName,
            pendingStoryCount: pendingStoryCount,
            userPreferenceFilter: userPreferenceFilter,
            groupedPendingStories: groupedPendingStories,
            setCalendarDate: setCalendarDate,
            currentBunch: currentBunch,
            setCurrentBunch: setCurrentBunch,
            setCurrentBunchFilters: setCurrentBunchFilters,
            currentView: currentView,
            selectedAudioClip: selectedAudioClip,
            displayAudioToolTip: displayAudioToolTip,
            closeAudioToolTip: closeAudioToolTip,
            audioComments: audioComments,
            resetContentFilter: resetContentFilter,
            isUpdateScroll: isUpdateScroll,
            isStoreScroll: isStoreScroll,
            searchEventNews: searchEventNews,
            setCurrenBreakingNews: setCurrenBreakingNews,
            viewBreakingNews: viewBreakingNews,
            currentBreakingNews: currentBreakingNews,
            hideBreakingNews: hideBreakingNews,
            setFullScreenMode: setFullScreenMode,
            switchView: switchView,
            e: e,
            utils: utils,
            newsDragIsReadOnly: newsDragIsReadOnly,
            toggleArchivalSearch: toggleArchivalSearch,
            downLoadResource: downLoadResource,
            reportnewsView: reportnewsView,
            submitNews: submitNews,
            switchViewTicker: switchViewTicker,
            setCurrentContent: setCurrentContent,
            submitMetaDataByUser: submitMetaDataByUser,
            getAllFilters: getAllFilters,
            tickerReporterControl: tickerReporterControl,
            userSourceFilter: userSourceFilter,
            userCategorizedFilter: userCategorizedFilter,
            router: router,
            config: config,
            clickAttachment: clickAttachment,
            clickCameramen: clickCameramen,
            xmlParserObj: xmlParserObj,
            addToNewsBucket: addToNewsBucket,
            editSelected: editSelected,
            openMediaSelection: openMediaSelection,
            addToBucketNews: addToBucketNews,
            newsFiles: newsFiles,
            markNewsFileStatus: markNewsFileStatus,
            editNewsFile: editNewsFile,
            moment: moment,
            setNewsInEditMode: setNewsInEditMode,
            parentWindowCss: parentWindowCss,
            selectedNewsTicker: selectedNewsTicker,
            addNewsTikerByNewsFile: addNewsTikerByNewsFile,
            tickerslist: tickerslist,
            newsFilesList: newsFilesList,
            copyFolderList: copyFolderList,
            foldersListCopyTo: foldersListCopyTo,
            copyNewsFileToSelectedFolder: copyNewsFileToSelectedFolder,
            currentNewsFile: currentNewsFile,
            genericNewsFilesList: genericNewsFilesList,
            displayResourceOnPopup: displayResourceOnPopup,
            closeDetailView: closeDetailView,
            sendToBroadCast: sendToBroadCast,
            arrangeNewsFilesofProgram: arrangeNewsFilesofProgram,
            markNewsFileDeleted: markNewsFileDeleted,
            printProgram: printProgram,
            freshNewsFilesListOutput: freshNewsFilesListOutput,
            freshNewsFilesListPortal: freshNewsFilesListPortal,
            freshNewsFilesListReviewed: freshNewsFilesListReviewed,
            totalFilterSelected:totalFilterSelected,
            markVerificationStatus: markVerificationStatus,
            tooltipData: tooltipData,
            ShowBroadCastedDetails: ShowBroadCastedDetails,
            AddNewEvent: AddNewEvent,
            ReportNewsAgainstProgram: ReportNewsAgainstProgram,
            EditNewsFileFromCinput: EditNewsFileFromCinput
        };
    });