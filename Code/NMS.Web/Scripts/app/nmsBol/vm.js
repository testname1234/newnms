﻿define('vm',
    [
        'vm.shell',
        'vm.home',
        'vm.topicselection',
        'vm.contentviewer',
        'vm.reportnews',

        'vm.taggedusers',
        'vm.tagreport',
        'vm.tagnews',
        'vm.copywritereditorial'
    ],

    function (shell, home, topicselection, contentviewer, reportnews,  taggedusers, tagreport, tagnews, copyWritereditorial) {
        return {
            shell: shell,
            home: home,
            topicselection: topicselection,
            contentviewer: contentviewer,
            reportnews: reportnews,
    
            taggedusers: taggedusers,
            tagreport: tagreport,
            tagnews: tagnews,
            copyWritereditorial: copyWritereditorial
        };
    });