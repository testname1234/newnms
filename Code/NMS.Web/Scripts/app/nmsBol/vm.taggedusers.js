﻿define('vm.taggedusers',
    [
        'underscore',
        'jquery',
        'ko',
        'datacontext',
        'vm.topicselection',
        'router',
        'config',
        'model',
        'presenter',
        'manager',
        'appdata',
        'presenter',
        'utils',
        'moment',
        'messenger',
        'enum',
        'vm.contentviewer',
        'model.mapper',
        'model.userfilter',
        'model.reportnews',
        'vm.tagnews',
        'timer'
    ],
    function (_, $, ko, dc, topicSelectionVM, router, config, model, presenter, manager, appdata, presenter, utils, moment, messenger, e, contentviewer, mapper, userfilter, ReportNews, tagnews, timer) {
        var logger = config.logger;

        var
            // Properties
            // ------------------------

            hashes = config.hashes.production,
            rightTmplName = 'producer.home',
            imageFooterTmplName = 'producer.home',
            contentFooterTmplName = 'producer.home',
            contentTmplName = 'producer.home',
            templates = config.templateNames,
            pageIndex = ko.observable(1),
            selectedNewsId = appdata.selectedNewsId,
            searchKeywords = ko.observable('').extend({ throttle: config.throttle }),
            sourceFilterControl = new model.SourceFilterControl(),
            calendarControl = new model.CalendarControl(),
            bunchArray = ko.observableArray(),
            pendingStoriesArray = ko.observableArray(),
            searchGroupName = ko.observable(),
            currentBunch = ko.observable(new model.Bunch()),
            currentView = ko.observable('listView'),
            selectedAudioClip = ko.observable(),
            audioComments = ko.observable(),
            isUpdateScroll = ko.observable(false),
            isStoreScroll = ko.observable(false),
            viewBreakingNews = ko.observable(false),
            currentBreakingNews = ko.observable(),
            xmlParserObj = new model.XmlParser(),
            scrollPos = 0,
            reportnewsView = new ReportNews('urduRN', 'englishRN', ['.leftPanel .editor input[type=text]', '.leftPanel .editor .jqte_editor', '.reportNewsEditor'], e.NewsType.Story, 'Report News', null, e.NewsFilterType.FieldReporter),
            tickerReporterControl = new model.TickerReporterControl(),
            newsFiles = ko.observableArray(),
            isNewsMarked = ko.observable(false),
            parentWindowCss = ko.observable(''),
            selectedNewsTicker = ko.observable(''),
            addNewTicker = ko.observable(false),
            foldersListCopyTo = ko.observableArray(),
            lastnewsFileID = '',
            currentNewsFile = ko.observable(),
            refreshAllNews = ko.observable(false),
            freshNewsFilesListOutput = ko.observableArray([]),
            freshNewsFilesListPortal = ko.observableArray([]),
            freshNewsFilesListReviewed = ko.observableArray([]),


            // Computed Properties
            // ------------------------

            newsCount = ko.computed({
                read: function () {
                    var allFilters = dc.filters.getObservableList();
                    var filter = _.filter(allFilters, function (obj) {
                        return obj.id === 78;
                    });

                    if (filter && filter.length)
                        return filter[0].newsCount();
                },
                deferEvaluation: true
            }),

            userPreferenceFilter = ko.computed({
                read: function () {
                    var arr = dc.UserFilter.getObservableList();
                    var temparr = [];
                    if (arr && arr.length > 0) {
                        for (var i = 0; i < arr.length; i++) {
                            if (arr[i].parentId === -1 && (arr[i].filterTypeId === e.NewsFilterType.Category || arr[i].filterTypeId < e.NewsFilterType.Category)) {
                                temparr.push(arr[i]);
                            }
                        }
                    }

                    return temparr;
                },
                deferEvaluation: true
            }),

            userCategorizedFilter = ko.computed({
                read: function () {
                    var arr = dc.UserFilter.getObservableList();
                    var temparr = [];
                    if (arr && arr.length > 0) {
                        for (var i = 0; i < arr.length; i++) {
                            if (arr[i].parentId === -1 && arr[i].filterTypeId === 13) {
                                arr[i].isVisible(true);
                                if (arr[i].children().length == 0) {
                                    arr[i].changeSign('');
                                }
                                temparr.push(arr[i]);
                            }
                        }
                    }
                    return temparr;
                },
                deferEvaluation: true
            }),

            userSourceFilter = ko.computed({
                read: function () {
                    appdata.isUserSourceFilterChange();
                    var arr = dc.UserFilter.getObservableList();
                    var temparr = [];
                    if (arr && arr.length > 0) {
                        for (var i = 0; i < arr.length; i++) {
                            if (arr[i].parentId === -1 && arr[i].filterTypeId != 13) {
                                arr[i].isVisible(true);
                                if (arr[i].children().length == 0) {
                                    arr[i].changeSign('');
                                }
                                temparr.push(arr[i]);
                            }
                        }
                    }
                    return temparr;
                },
                deferEvaluation: true
            }),

            lastUIRefreshDisplayTime = ko.observable(moment(appdata.lastUIRefreshTime).fromNow()),

            // Methods
            // ------------------------

            submitNews = function () {

                reportnewsView.tickerLines(tickerReporterControl.tickerLineList());
                reportnewsView.currentTicker(tickerReporterControl.currentTicker());
                if (tickerReporterControl.lastTicker && tickerReporterControl.lastTicker()) {
                    tickerReporterControl.tickerLineList.push({ Text: tickerReporterControl.lastTicker(), LanguageCode: "ur", SequenceId: tickerReporterControl.tickerLineList().length == 0 ? 1 : tickerReporterControl.tickerLineList().length + 1 });
                }
                reportnewsView.submit();
                tickerReporterControl.tickerLineList([])

            },

            sendToBroadCast = function () {
                //alert();
            },
            markSocialMediaStatus = function (newsfile, status) {
                var reqObj = {
                    NewsFileId: newsfile.id,
                    Status: status
                }
                $.when(manager.newsfile.markSocialMediaStatus(reqObj))
                 .done(function (responseData) {
                     if (responseData.IsSuccess) {
                         if (!status) {
                             freshNewsFilesListPortal.remove(newsfile);
                             freshNewsFilesListPortal.valueHasMutated();
                             dc.newsFiles.removeById(newsfile.id);
                         }
                         if (status) {
                             dc.newsFiles.getLocalById(newsfile.id).isVerified(status);
                             freshNewsFilesListPortal.valueHasMutated();
                             dc.newsFiles.removeById(newsfile.id);
                         }
                         logger.success("Marked Successfully");
                     } else {
                         logger.error("No Data Found");
                     }
                 })
                 .fail(function (responseData) {
                     logger.error("Request Failed");
                 });


            },

            printProgram = function (data) {
                var reqObj = data;
                $.when(manager.newsfile.programToPrint(reqObj))
                .done(function (responseData) {
                    if (responseData.IsSuccess && responseData.Data && responseData.Data.length > 0) {
                        createPrintView(data, responseData.Data);
                    } else { logger.error("No Data Found"); }
                })
                .fail(function (responseData) {
                    logger.error("Request Failed");
                });
            },

        createPrintView = function (data, ResponseData) {
            var timeInterval = "00:00:00";
            $.each(appdata.foldersList(), function (i, v) {
                if (data.FolderId == v.FolderId) {
                    timeInterval = v.programInterval
                }
            })
            var resultHtml = createHtml(data, ResponseData, timeInterval);
            var mywindow = window.open('', 'BOL', 'width=850,height=600,centerscreen**');
            mywindow.document.write('<html><head><title>BOL</title>');
            mywindow.document.write("<style type='text/css'>@font-face{font-family:'Jameel Noori Nastaleeq';src:url(/content/fonts/jameel-noori-nastaleeq-regular.ttf) format('truetype')}</style></head><body onload='window.print();'>");
            //mywindow.document.write('<input type="button" id="btnPrint" value="Print" class="no-print" style="width:100px" onclick="window.print()" />');
            //mywindow.document.write('<input type="button" id="btnCancel" value="Cancel" class="no-print"  style="width:100px" onclick="window.close()" />');
            mywindow.document.write(resultHtml.innerHTML);
            mywindow.document.write('</body></html>');
            mywindow.document.close(); // necessary for IE >= 10
            mywindow.focus(); // necessary for IE >= 10
            //mywindow.print();
            //mywindow.close();
            return true;
        },

        createHtml = function (data, ResponseData, timeInterval) {
            var mainDiv = document.createElement("div");
            $(mainDiv).append('<h1 style="display: inline-block; margin:0px">' + data.FolderName + ' : ' + timeInterval + '</h1><span style="float:right; display: inline; line-height: 40px;"> ' + moment().format('MM/DD/YYYY h:mm a') + ' </span>');
            var innerDiv = document.createElement("div");
            for (var i = 0; i < ResponseData.length; i++) {
                var TitleHtml = document.createElement('h2')
                var DetailsHtml = document.createElement("p");
                TitleHtml.setAttribute("style", "font-family: 'Jameel Noori Nastaleeq', 'Nafees nastaleeq', 'Nafees Web Naskh';direction: rtl;font-size:30px");
                DetailsHtml.setAttribute("style", "font-family: 'Jameel Noori Nastaleeq', 'Nafees nastaleeq', 'Nafees Web Naskh'; direction: rtl; font-size:22px");
                if (ResponseData[i].Highlights) {
                    TitleHtml.appendChild(document.createTextNode((i + 1) + ") " + ResponseData[i].Highlights));
                } else {
                    TitleHtml.appendChild(document.createTextNode((i + 1) + ") No title found"))
                }
                if (ResponseData[i].FileDetails && ResponseData[i].FileDetails[0].Text) {
                    DetailsHtml.innerHTML = ResponseData[i].FileDetails[0].Text;
                } else { DetailsHtml.innerHTML = " No details found" }

                innerDiv.appendChild(TitleHtml);
                innerDiv.appendChild(DetailsHtml);
            }
            mainDiv.appendChild(innerDiv);
            return mainDiv;
        },

        setCurrentNews = function (data, isRelated) {
            if (data) {
                if (isRelated)
                    topicSelectionVM.setCurrentNews(data);
                else
                    topicSelectionVM.setCurrentNews(data.topNews());

                router.navigateTo(config.hashes.production.topicSelection);
                appdata.lastRoute(config.hashes.production.topicSelection);
            }

        },
        addToBucketNews = function () {
            var data = 1;
        },


        toggleStoryFilter = function (data) {
            manager.production.toggleStoryFilter(data);
            pageIndex(1);
            appdata.lpStartIndex = 0;
            manager.news.setDataChangeTime();
        },
        toggleFilter = function (data, isExtraFilter) {

            if (data != -1) {
                dc.newsFiles.clearAllNews(true);
                appdata.clearAll(!appdata.clearAll());
            }

            appdata.newsFilesLastUpdateDate = '';
            manager.news.toggleFilter(data, isExtraFilter);

            appdata.lpPageCount = 50;
            appdata.lpStartOffSet = 0;

            pageIndex(1);
            appdata.lpStartIndex = 0;
            manager.news.setDataChangeTime();

            if (appdata.selectedNewsFile())
                closeDetailView();

            appdata.selectedNewsFile('');
            if (data != -1) {
                manager.newsfile.newsFileFilterSelected();
            }
        },
        resetFilters = function () {
            if (appdata.selectedNewsFile())
                closeDetailView();

            freshNewsFilesListPortal([]);
            freshNewsFilesListReviewed([]);
            freshNewsFilesListOutput([]);

            freshNewsFilesListPortal.valueHasMutated();
            freshNewsFilesListReviewed.valueHasMutated();
            freshNewsFilesListOutput.valueHasMutated();

            dc.newsFiles.clearAllNews(true);
            appdata.selectedCategoryFilters([]);
            appdata.selectedExtraFilters([]);
            appdata.selectedSourceFilters([]);
            appdata.newsFilesLastUpdateDate = '';
            sourceFilterControl.currentFilter(null);
            searchKeywords('');
            appdata.lpPageCount = 50;
            appdata.lpStartOffSet = 0;
            calendarControl.currentOption('lastmonth');

            appdata.lpFromDate = calendarControl.fromDate();
            appdata.lpToDate = calendarControl.toDate();

            $('.nav-step1 li a').removeClass('active-category-filter');  //Category
            $('.mainFilter.hideFilters').removeClass('newsActive'); //Source
            $('.navigationNews li ul li').removeClass('active-filter'); //Source Children
            $('.onoffswitch .onoffswitch-label').removeClass('active'); //Switches
            $('#top-rated-filter').removeClass('active'); //top Rated
            $('.calenderRight').find('.active').removeClass('active');
            $('.calenderRight').find('.lastMonth').addClass('active');

            appdata.selectedSourceFilters.push(78);
            appdata.isAllNewsFilterSelected(true);
            manager.news.setDataChangeTime();
            manager.newsfile.newsFileProducerPolling();

            document.cookie = "CachedFilters" + '=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';
        },
        loadNextPage = function () {
            presenter.toggleActivity(true);
            appdata.newsFilesLastUpdateDate = '';
            if (appdata.currentUser().userType == e.UserType.taggedUser || appdata.currentUser().userType == e.UserType.filterSocial || appdata.currentUser().userType == e.UserType.taggedPending) {
                manager.newsfile.newsFileProducerPolling(true);
            }
        },
            setCalendarDate = function () {

                appdata.lpFromDate = calendarControl.fromDate();
                appdata.lpToDate = calendarControl.toDate();
                appdata.lpStartIndex = 0;

                closeDetailView();
                dc.newsFiles.clearAllNews(true);
                appdata.clearAll(!appdata.clearAll());
                appdata.newsFilesLastUpdateDate = '';

                if (parseInt(moment().diff(appdata.lpToDate, 'days')) == 0) {
                    timer.start();
                }

                if (appdata.currentUser().userType === e.UserType.NLE || appdata.currentUser().userType === e.UserType.StoryWriter) {
                    appdata.NLEfromDate(appdata.lpFromDate);
                    appdata.NLEtoDate(appdata.lpToDate);
                }
                manager.news.setDataChangeTime();
                manager.newsfile.newsFileFilterSelected();

            },
            subscribeEvents = function () {
                searchKeywords.subscribe(function (value) {
                    appdata.searchKeywords = value;
                    appdata.lpStartIndex = 0;
                    appdata.newsFilesLastUpdateDate = '';
                    dc.newsFiles.clearAllNews(true);
                    appdata.refreshGenericList(!appdata.refreshGenericList());
                    appdata.clearAll(!appdata.clearAll());
                    appdata.selectedNewsFile('');

                    manager.newsfile.newsFileProducerPolling();
                    manager.news.setDataChangeTime();
                });

                pageIndex.subscribe(function (value) {
                    appdata.pageIndex = value;
                    manager.news.setDataChangeTime();
                });
                appdata.currentResource.subscribe(function (value) {
                    if (value && value != '') {

                        if (topicSelectionVM.isIframeVisible()) {
                            var resource = value;
                            window.parent.window.postMessage(value, "*");
                        }
                        else {
                            var value = JSON.parse(value);
                            if (value.Guid) {
                                var res = mapper.resource.fromDto(value);
                                var arr = _.filter(reportnewsView.uploader().mappedResources(), function (resource) {
                                    return resource.guid === res.guid;
                                });
                                if (arr && arr.length > 0) {
                                    reportnewsView.uploader().mappedResources().remove(arr[0]);
                                    var arr1 = _.filter(reportnewsView.uploader().uploadedResources(), function (upresource) {
                                        return upresource.Guid === res.guid;
                                    });
                                    if (arr1 && arr1.length > 0) {
                                        reportnewsView.uploader().uploadedResources().remove(arr1[0]);
                                    }
                                    reportnewsView.uploader().uploadedResources.valueHasMutated();
                                    reportnewsView.uploader().resources(reportnewsView.uploader().uploadedResources());
                                }
                                else {
                                    reportnewsView.uploader().mappedResources().push(res);
                                    reportnewsView.uploader().uploadedResources().push(value);
                                    reportnewsView.uploader().mappedResources()[reportnewsView.uploader().mappedResources().length - 1].type = res.type;
                                    reportnewsView.uploader().mappedResources()[reportnewsView.uploader().mappedResources().length - 1].IgnoreMeta = 1;
                                    reportnewsView.uploader().uploadedResources.valueHasMutated();
                                    reportnewsView.uploader().resources(reportnewsView.uploader().uploadedResources());
                                }
                            }
                            else {
                                var obj = {
                                    Guid: value.guid,
                                    ResourceId: value.id,
                                    ResourceTypeId: value.type,
                                    IgnoreMeta: 1
                                }
                                var arr = _.filter(reportnewsView.uploader().mappedResources(), function (resource) {
                                    return resource.guid === value.guid;
                                });
                                if (arr && arr.length > 0) {
                                    reportnewsView.uploader().mappedResources().remove(arr[0]);
                                    var arr1 = _.filter(reportnewsView.uploader().uploadedResources(), function (upresource) {
                                        return upresource.Guid === value.guid;
                                    });
                                    if (arr1 && arr1.length > 0) {
                                        reportnewsView.uploader().uploadedResources().remove(arr1[0]);
                                    }
                                    reportnewsView.uploader().uploadedResources.valueHasMutated();
                                    reportnewsView.uploader().resources(reportnewsView.uploader().uploadedResources());
                                }
                                else {
                                    reportnewsView.uploader().mappedResources().push(value);
                                    reportnewsView.uploader().uploadedResources().push(obj);
                                    reportnewsView.uploader().mappedResources()[reportnewsView.uploader().mappedResources().length - 1].type = value.type;
                                    reportnewsView.uploader().mappedResources()[reportnewsView.uploader().mappedResources().length - 1].IgnoreMeta = 1;
                                    reportnewsView.uploader().uploadedResources.valueHasMutated();
                                    reportnewsView.uploader().resources(reportnewsView.uploader().uploadedResources());
                                }
                            }
                        }
                    }
                });

            },
            displayAudioToolTip = function (data) {
                selectedAudioClip('../../wildlife.mp3');
                audioComments(data.title());
            },
            closeAudioToolTip = function () {
                selectedAudioClip('');
            },
            resetContentFilter = function () {
                $('#allNews').removeClass('active');
                $('#allNews').addClass('active');
                $('#news').removeClass('active');
                $('#packages').removeClass('active');
                $('#verified-filter').removeClass('active');
                $('#recommended-filter').removeClass('active');
                $('#top-rated-filter').removeClass('active');
                $('#top-executed-filter').removeClass('active');
                $('#most-recent-filter').removeClass('active');

                appdata.selectedExtraFilters([]);
                toggleFilter(-1, true);
            },
            searchEventNews = function (data) {
                if (data) {
                    if (data.isSelected()) {
                        data.isSelected(false);
                        appdata.searchKeywords = '';
                        manager.news.setDataChangeTime();
                    } else {
                        _.filter(sourceFilterControl.events(), function (obj) {
                            return obj.isSelected(false);
                        });
                        data.isSelected(true);
                        appdata.searchKeywords = data.searchTags;
                        manager.news.setDataChangeTime();
                    }
                }
            },
            setCurrenBreakingNews = function (data) {
                contentviewer.setCurrentAlertContent(data, 2);
            },
            setCurrentContent = function (data, options) {
                try {
                    if (data && options) {
                        if (options.isReportedContent) {
                            contentviewer.setCurrentContent(reportnewsView.uploader().mappedResources(), data);
                        }
                    }
                } catch (e) {
                    //console.log(e.message);
                }
            },
            hideBreakingNews = function () {
                viewBreakingNews(false);
                currentBreakingNews('');
            },
            setFullScreenMode = function () {
              //  var el = document.body;
              //  var requestMethod = el.requestFullScreen || el.webkitRequestFullScreen
              //|| el.mozRequestFullScreen || el.msRequestFullScreen;
              //  if (requestMethod) {
              //      requestMethod.call(el);
              //  } else if (typeof window.ActiveXObject !== "undefined") {
              //      var wscript = new ActiveXObject("WScript.Shell");
              //      if (wscript !== null) {
              //          wscript.SendKeys("{F11}");
              //      }
              //  }
            },
            storeScrollPosition = function () {
                //appdata.producerScrollTop($('#home-view').children('div').children('.mCSB_container').css('top'));
            },
            setScrollPosition = function () {
                //scrollPos = appdata.producerScrollTop();
                //scrollPos = scrollPos.replace('px', '');
                //$('#home-view').children('div').children('.mCSB_container').css('top', parseInt(scrollPos));
            },
            hideNextPrevNLE = function () {
                $(".prev").css({ 'display': 'none' });
                $(".next").css({ 'display': 'none' });
            },
            submitMetaDataByUser = function () {
                var MetaTypeId = e.MetaTypeId.Filters;
                var result = dc.UserFilter.getAllLocal();
                var arry = [];
                for (var i = 0; i < result.length ; i++) {
                    var obj = {
                        MetaTypeId: MetaTypeId,
                        MetaName: result[i].name,
                        MetaValue: result[i].id,
                        UserId: appdata.currentUser().id,
                        isAllowed: result[i].isNotAllowed(),
                        isDiscarded: result[i].isDiscarded()
                    };
                    arry.push(obj);
                }

                var MetaData = { MetaData: arry };
                manager.news.submitMetaDataByUser(MetaData);
            },
            getAllFilters = function () {
                manager.news.getAllFilters();
            },

            editSelected = function (data) {
                router.navigateTo(config.views.fieldreporter.reportNews.url);
                data.topNews().isNewsEdit(true);
                reportnewsView.newsSelectedProduction(data.topNews());

            },
           markVerificationStatus = function (isVerified, data) {

           },
            markNewsFileStatus = function (data, newsfilestatus) {
            },
            setNewsInEditMode = function (data) {

                if (data && data.id) {
                    isNewsMarked(true);
                    tagnews.setNews(data);
                    
                    router.navigateTo(config.views.nmsBol.tagnews.url);
                }

            },
            copyNewsFileToSelectedFolder = function (newsfile, folderId) {

            },
            editNewsFile = function (newsFile, isDetailView) {
                if (router.currentHash() != config.hashes.nmsBol.taggerInput && appdata.currentUser().userType != e.UserType.filterSocial) {
                    if (isDetailView && !isNewsMarked()) {
                        var reqObj = { newsFileId: newsFile.id };
                        if (lastnewsFileID != newsFile.id) {

                            $.when(manager.newsfile.getnewsFileDetail(reqObj))
                                 .done(function (responseData) {
                                     if (responseData) {
                                         newsFiles(dc.newsFiles.getAllLocal())
                                         var newsEntity = mapper.newsFile.fromDto(responseData);
                                         appdata.selectedNewsFile(newsEntity);

                                         if (lastnewsFileID === '') {

                                             $(".taggedPendingParent").width("55%");
                                             $(".nmsBolPencil").width("3.5%");
                                             $(".adjustCssTagged").width("90%");
                                             setTimeout(function () {

                                                 $(".pendingDetail").toggle("slide", "left", 500);
                                             }, 200);


                                         }
                                         if (lastnewsFileID != '' && lastnewsFileID != newsFile.id && !$(".pendingDetail").is(':Visible')) {
                                             $(".pendingDetail").toggle("slide", "left", 500);
                                         }
                                         lastnewsFileID = newsFile.id;
                                     }
                                 })
                                 .fail(function (responseData) {
                                 });
                        }

                        else {
                            $(".pendingDetail").toggle("slide", "left", 500);
                            setTimeout(function () {
                                $(".taggedPendingParent").width("94%");
                                $(".nmsBolPencil").width("2.5%");
                                $(".adjustCssTagged").width("95%");
                            }, 600);
                            appdata.selectedNewsFile('');
                            lastnewsFileID = '';
                        }
                    }
                    else {
                        isNewsMarked(false);
                    }
                }
            },

          closeDetailView = function () {
              $(".pendingDetail").toggle("slide", "left", 500);
              setTimeout(function () {
                  $(".taggedPendingParent").width("94%");
                  $(".adjustCssTagged").width("95%");
                  $(".nmsBolPencil").width("2.5%");
                  appdata.selectedNewsFile('');
              }, 600);
              lastnewsFileID = '';

          },
          addNewsTikerByNewsFile = function (newsFile) {
              var data = {
                  newsFileId: newsFile.id
              }

              $.when(manager.ticker.createTickerBynewsFileId(data))
                 .done(function (responseData) {
                     if (responseData) {
                         addNewTicker(true);
                     }
                     addNewTicker(false);
                 })
                 .fail(function (responseData) {
                 });

          },

          sortDesc = function (arr) {
              if (arr && arr.length > 1) {
                  arr = arr.sort(function (entityA, entityB) {
                      return new Date(entityB.creationDate()) - new Date(entityA.creationDate());
                  });
              }
              return arr;
          },

         sortBySequenceNo = function (arr) {
             if (arr && arr.length > 1) {
                 arr = arr.sort(function (entityA, entityB) {
                     return (entityA.sequenceNo()) - (entityB.sequenceNo());
                 });
             }
             return arr;
         },
          newsFilesList = ko.computed({
              read: function () {

                  appdata.clearAll();
                  appdata.refreshNewsFileList();
                  refreshAllNews();
                  appdata.newsFileLastUpdateDateObservable();
                  dc.newsFiles.getAllLocal();

                  if (appdata.currentUser().userType == e.UserType.taggedUser) {

                      if (router.currentHash() === config.views.nmsBol.taggerInput.url) {
                          appdata.lpStartOffSet = dc.newsFiles.getAllLocal().length;
                          var result = $.grep(dc.newsFiles.getAllLocal(), function (e) { return (e.isTagged()); });
                          freshNewsFilesListPortal(sortDesc(result));
                      }

                      if (router.currentHash() === config.views.nmsBol.taggedPending.url) {
                          appdata.lpStartOffSet = dc.newsFiles.getAllLocal().length;
                          var result = $.grep(dc.newsFiles.getAllLocal(), function (e) { return (!e.isTagged()); });
                          freshNewsFilesListPortal(sortDesc(result));
                      }

                  }

                  if (appdata.currentUser().userType == e.UserType.filterSocial) {
                      appdata.lpStartOffSet = dc.newsFiles.getAllLocal().length;
                      var result = $.grep(dc.newsFiles.getAllLocal(), function (e) {
                          return ((e.isVerified() === 'initial') && !e.isDeleted());
                      });
                      freshNewsFilesListPortal(sortDesc(result));
                  }
                  var newsFiles = sortDesc(result);
                  newsFiles = [];
                  return newsFiles;
              },
              deferEvaluation: true
          }),

          copyFolderList = ko.computed({
              read: function () {

              },
              deferEvaluation: true
          }),
          tickerslist = ko.computed({
              read: function () {
                  var alltickers = dc.tickers.getAllLocal();
                  addNewTicker();
                  appdata.refreshTickers();
                  if (alltickers && alltickers.length) {
                      return alltickers;
                  }
              },
              deferEvaluation: true
          }),

          displayResourceOnPopup = function (data, resourcesList) {
              var arr = [];
              arr.push(resourcesList);
              contentviewer.setCurrentContent(arr[0], data);
          },
          UpdateTaggedAllFoldersCss = function () {
              if (router.currentHash() === config.views.nmsBol.taggerInput.url || router.currentHash() === config.views.nmsBol.FilterSocial.url || router.currentHash() === config.views.nmsBol.taggedPending.url) {
                  var obj = {
                      freshNewsDivWidth: '94%',
                  }
                  parentWindowCss(obj);
              }

          },

          arrangeNewsFilesofProgram = function (arrayPositions) {

              var SequenceList = [];
              for (var i = 0; i < arrayPositions.length ; i++) {
                  SequenceList.push(parseInt(arrayPositions[i]));
              }
              var reqObj = {
                  SequenceList: SequenceList
              }
              manager.newsfile.updateNewsFilesSequencing(reqObj);
              updateNewsFileSequencing();
          },
          updateNewsFileSequencing = function () {
              setTimeout(function () {
                  $(".ui-sortable").sortable('refresh');
              }, 500);

          },
            markNewsFileDeleted = function (newsfile) {
                var reqObj = {
                    newsFileId: newsfile.id
                }
                manager.newsfile.markNewsFileDeleted(reqObj);
            },
            activate = function (routeData, callback) {
                //appdata.selectedNewsFile('');
                messenger.publish.viewModelActivated({ canleaveCallback: canLeave });
                UpdateTaggedAllFoldersCss();
                if (appdata.selectedNewsFile() && router.currentHash() === config.hashes.nmsBol.taggedPending) {
                    console.log('here');
                    $(".taggedPendingParent").width("55%");
                    $(".nmsBolPencil").width("3.5%");
                    $(".adjustCssTagged").width("90%");
                }

            },
            canLeave = function () {
                return true;
            },
            init = function () {
                if (appdata.foldersList().length > 0) {
                    var width = (400 * (appdata.foldersList().length + 1)) + 4;
                    $(".foldersCategoryCss").width(parseInt(width) + 'px');
                }

                sourceFilterControl.currentFilter(-1);
                sourceFilterControl.isPopulated(1);
                sourceFilterControl.showExtraFilters(false);
                calendarControl.currentOption('lastmonth');

                if (appdata.currentUser().userType == e.UserType.taggedUser || appdata.currentUser().userType == e.UserType.filterSocial || appdata.currentUser().userType == e.UserType.taggedPending)
                    calendarControl.isNewsCropVisible(true);

                subscribeEvents();

                //toggleFilter(450, true);
                toggleFilter(-1, true);

                if (appdata.currentUser().userType === e.UserType.HeadlineProducer) {
                    switchView();
                }

                UpdateTaggedAllFoldersCss();

            };


        return {
            activate: activate,
            canLeave: canLeave,
            appdata: appdata,
            setCurrentNews: setCurrentNews,
            templates: templates,
            init: init,
            sourceFilterControl: sourceFilterControl,
            calendarControl: calendarControl,
            hashes: hashes,
            loadNextPage: loadNextPage,
            toggleFilter: toggleFilter,
            toggleStoryFilter: toggleStoryFilter,
            searchKeywords: searchKeywords,
            selectedNewsId: selectedNewsId,
            newsCount: newsCount,
            resetFilters: resetFilters,
            lastUIRefreshDisplayTime: lastUIRefreshDisplayTime,
            searchGroupName: searchGroupName,
            userPreferenceFilter: userPreferenceFilter,
            setCalendarDate: setCalendarDate,
            selectedAudioClip: selectedAudioClip,
            displayAudioToolTip: displayAudioToolTip,
            closeAudioToolTip: closeAudioToolTip,
            audioComments: audioComments,
            resetContentFilter: resetContentFilter,
            isUpdateScroll: isUpdateScroll,
            isStoreScroll: isStoreScroll,
            searchEventNews: searchEventNews,
            setCurrenBreakingNews: setCurrenBreakingNews,
            viewBreakingNews: viewBreakingNews,
            currentBreakingNews: currentBreakingNews,
            hideBreakingNews: hideBreakingNews,
            setFullScreenMode: setFullScreenMode,
            e: e,
            utils: utils,
            reportnewsView: reportnewsView,
            submitNews: submitNews,
            setCurrentContent: setCurrentContent,
            submitMetaDataByUser: submitMetaDataByUser,
            getAllFilters: getAllFilters,
            tickerReporterControl: tickerReporterControl,
            userSourceFilter: userSourceFilter,
            userCategorizedFilter: userCategorizedFilter,
            router: router,
            config: config,
            xmlParserObj: xmlParserObj,
            addToNewsBucket: addToNewsBucket,
            editSelected: editSelected,
            addToBucketNews: addToBucketNews,
            newsFiles: newsFiles,
            markNewsFileStatus: markNewsFileStatus,
            editNewsFile: editNewsFile,
            moment: moment,
            setNewsInEditMode: setNewsInEditMode,
            parentWindowCss: parentWindowCss,
            selectedNewsTicker: selectedNewsTicker,
            addNewsTikerByNewsFile: addNewsTikerByNewsFile,
            tickerslist: tickerslist,
            newsFilesList: newsFilesList,
            copyFolderList: copyFolderList,
            foldersListCopyTo: foldersListCopyTo,
            copyNewsFileToSelectedFolder: copyNewsFileToSelectedFolder,
            currentNewsFile: currentNewsFile,
            displayResourceOnPopup: displayResourceOnPopup,
            sendToBroadCast: sendToBroadCast,
            arrangeNewsFilesofProgram: arrangeNewsFilesofProgram,
            markNewsFileDeleted: markNewsFileDeleted,
            printProgram: printProgram,
            freshNewsFilesListOutput: freshNewsFilesListOutput,
            freshNewsFilesListPortal: freshNewsFilesListPortal,
            freshNewsFilesListReviewed: freshNewsFilesListReviewed,
            markVerificationStatus: markVerificationStatus,
            markSocialMediaStatus: markSocialMediaStatus,
            closeDetailView: closeDetailView
        };
    });