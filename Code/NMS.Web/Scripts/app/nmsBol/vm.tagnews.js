﻿define('vm.tagnews',
   ['config', 'enum', 'model.tagReporter', 'model', 'appdata', 'router', 'vm.contentviewer', 'datacontext', 'control.templatepaging', 'model.mapper', 'manager', 'datacontext', 'utils'],
    function (config, e, ReportNews, parentModel, appdata, router, contentviewer, datacontext, TemplatePaging, mapper, manager, dc, utils) {

        var tmodel = new ReportNews('urduRNT', 'englishRNT', ['.leftPanel .editor .textboxCss', '.reportNewsEditor', '.textboxCss .textboxHighLight'], e.NewsType.Story, 'Report News', null, e.NewsFilterType.FieldReporter),

            tickerReporterControl = new parentModel.TickerReporterControl(),
            newsId,
            sType = ko.observable(1),

            templatePaging = new TemplatePaging(true),
              currentResource = ko.observable(''),
             // reportnewsView = new ReportNews('urduRN', 'englishRN', ['.leftPanel .editor input[type=text]', '.leftPanel .editor .jqte_editor', '.reportNewsEditor'], e.NewsType.Story, 'Report News', null, e.NewsFilterType.FieldReporter),
                submitNews = function () {
                    console.log(sType());
                    tmodel.submitNewsFile(RedirectToTagger);
                },

                RedirectToTagger = function () {
                    tmodel.clearViewModel();
                    dc.newsFiles.getLocalById(newsId).isTagged(true);
                    appdata.refreshNewsFileList();
                    router.navigateTo(config.views.nmsBol.taggedPending.url);
                },

                
            setNews = function (data) {
                loadFromServer(data.id);
                var showSuggestions = (router.currentHash() == config.views.nmsBol.taggedPending.url) ? true : false;
                if (showSuggestions) {
                    tmodel.showSuggestions(true);
                } else {
                    tmodel.showSuggestions(false);
                }
            },

              loadFromServer = function (id) {
                  tmodel.parentNewsId = id;
                  var obj = { id: id };
                  dc.news.clearAllNews(true);
                  $.when(manager.news.GetNews(obj))
                      .done(function (responseData) {
                          
                          if (responseData) {
                              var thisNews = [responseData.Data];
                              manager.news.saveNewsResources(thisNews);
                              updateModel(id);
                              fillImages(id);
                          } else {

                          }
                      })
                  .fail(function () {
                  });
              },


            updateModel = function (id) {
                newsId = id;
                var allProgs = dc.programs.getAllLocal();
                if (allProgs.length > 0) {
                    tmodel.programs(allProgs());
                }
                tmodel.reporters(dc.users.getAllLocal());
                var news = dc.news.getAllLocalByIds([id])[0];
                
                tmodel.fillViewModelProducer(news);
            },

             fillImages = function (id) {
                 var news = dc.news.getAllLocalByIds([id])[0];
                 var res = news.resources();
                 var arr = [];
                 for (var i = 0; i < res.length; i++) {
                     res[i].Guid = res[i].guid;
                 }
                 tmodel.uploader().uploadedResources(res);
             },

             setCurrentContent = function (data) {
                 var filterItem = tmodel.uploader().mappedResources().filter(function (e) {
                     return e.guid == data.id
                 })[0];
                 templatePaging.currentItem({ url: '' }); // jugaaar
                 var item = templatePaging;
                 item.currentItem().url = filterItem.url();
                 item.currentItem()["isProcessed"] = ko.observable(false);
                 item.currentItem().type = filterItem.type;
                 tmodel.popuptempPaging.currentItem(item.currentItem());
                 tmodel.showPopup(true);

             },
        


        selectSuggest = function (data) {
            var value = data;
            if (value.Guid) {
                var res = mapper.resource.fromDto(value);
                var arr = _.filter(tmodel.uploader().mappedResources(), function (resource) {
                    return resource.guid === res.guid;
                });
                {
                    tmodel.uploader().mappedResources().push(res);
                    tmodel.uploader().uploadedResources().push(value);
                    tmodel.uploader().mappedResources()[tmodel.uploader().mappedResources().length - 1].IgnoreMeta = 1;
                    tmodel.uploader().uploadedResources.valueHasMutated();
                    tmodel.uploader().resources(tmodel.uploader().uploadedResources());
                }
            }
        },

             clickCameramen = function () {
                 event.stopPropagation();
                 $('.cameraman').toggleClass('active');
             },


        
       SetSuggestionTypeTag = ko.computed({
           read: function () {
               var tags = tmodel.newsTag().tags();
               var rType = sType();
               if (tags.length > 0) {
                   var index = tags.length - 1;
                   var obj = { term: tags[index], type: rType };
                   CallMediaServer(obj);
               } else {
                   tmodel.suggestedresources([]);
               }
               return true;
                    },
                    deferEvaluation: false
       }),


        CallMediaServer = function (obj) {
            $.when(manager.tags.loadResourceAgainstTag(obj))
                    .done(function (responseData) {
                        if (responseData) {
                            var arr = [];
                            var result = responseData;
                            for (var i = 0; i < result.length; i++) {
                                var res = result[i];
                                res.ThumbUrl = config.MediaServerUrl + 'getthumb/' + res.Guid + "?UserId=" + appdata.currentUser().id;
                                arr.push(res);
                            }
                            tmodel.suggestedresources(arr);
                        } else {
                            tmodel.suggestedresources([]);
                        }
                    })
                .fail(function () {
                });
        },

        SetSuggestionTypeOrg = ko.computed({
            read: function () {
                var orgs = tmodel.newsOrg().tags();
                var rType = sType();
                if (orgs.length > 0) {
                    var index = orgs.length - 1;
                    var obj = { term: orgs[index], type: rType };
                    CallMediaServer(obj);
                } else {
                    tmodel.suggestedresources([]);
                }
                return true;
            },
            deferEvaluation: false
        }),

        activate = function (routeData, callback) {
            tmodel.isTelevisionView(false);
            //tmodel.suggestedresources([{ "Guid": "42866d73-6ac1-4b00-a21e-7ef3e3032597", "ResourceTypeId": 1, "CreationDateStr": "2015-01-03T15:43:20.223Z", "LastUpdateDateStr": "2015-05-21T06:02:15.563Z", "Duration": 0.0 }, { "Guid": "d10d549f-5ebf-4f87-858c-366aeee033f7", "ResourceTypeId": 1, "CreationDateStr": "2015-01-03T15:43:18.690Z", "LastUpdateDateStr": "2015-05-21T05:59:50.453Z", "Duration": 0.0 }, { "Guid": "c6b0ea4b-111a-4959-bcad-6491f599f07e", "ResourceTypeId": 1, "CreationDateStr": "2015-01-03T15:43:17.190Z", "LastUpdateDateStr": "2015-05-21T05:56:17.953Z", "Duration": 0.0 }, { "Guid": "7cd4e98e-53ec-4a00-bec0-e0511f6d1c17", "ResourceTypeId": 1, "CreationDateStr": "2015-01-03T14:51:36.393Z", "LastUpdateDateStr": "2015-03-21T06:49:39.230Z", "Duration": 0.0 }, { "Guid": "d22930e9-ea05-45ee-a705-801f2d22f98b", "ResourceTypeId": 1, "CreationDateStr": "2015-01-03T14:51:29.787Z", "LastUpdateDateStr": "2015-03-20T07:43:58.723Z", "Duration": 0.0 }, { "Guid": "25b9586a-1cbc-4d4d-af68-19c09a6c7c44", "ResourceTypeId": 1, "CreationDateStr": "2015-01-03T14:51:28.300Z", "LastUpdateDateStr": "2015-03-20T07:41:47.693Z", "Duration": 0.0 }, { "Guid": "c09ce69f-1d5e-4e79-9785-466a64ceb985", "ResourceTypeId": 1, "CreationDateStr": "2015-01-03T14:51:17.160Z", "LastUpdateDateStr": "2015-03-20T06:56:06.990Z", "Duration": 0.0 }, { "Guid": "44ec1006-a8b2-43f5-8216-c10128a5c36d", "ResourceTypeId": 1, "CreationDateStr": "2015-01-03T15:43:26.237Z", "LastUpdateDateStr": "2015-05-21T06:19:58.550Z", "Duration": 0.0 }, { "Guid": "54c222ce-7f1e-4aca-bf8d-19faf1c210c5", "ResourceTypeId": 1, "CreationDateStr": "2015-01-03T16:03:12.160Z", "LastUpdateDateStr": "2015-06-10T10:29:21.023Z", "Duration": 0.0 }, { "Guid": "89e3af99-1049-44d3-a78b-a62039b34789", "ResourceTypeId": 1, "CreationDateStr": "2015-01-03T16:03:11.613Z", "LastUpdateDateStr": "2015-06-10T10:28:15.600Z", "Duration": 0.0 }, { "Guid": "b2fc531c-3b3e-4917-989d-5b91f9c87884", "ResourceTypeId": 1, "CreationDateStr": "2015-01-03T15:59:54.380Z", "LastUpdateDateStr": "2015-06-06T07:45:59.920Z", "Duration": 0.0 }, { "Guid": "96217209-0fa7-42cd-b6dc-ed19281172f5", "ResourceTypeId": 1, "CreationDateStr": "2015-01-03T15:50:19.190Z", "LastUpdateDateStr": "2015-05-27T10:10:53.513Z", "Duration": 0.0 }, { "Guid": "b4a4404d-da76-4808-a4e4-74035b2aa0b5", "ResourceTypeId": 1, "CreationDateStr": "2015-01-03T15:50:19.473Z", "LastUpdateDateStr": "2015-05-27T10:13:18.293Z", "Duration": 0.0 }, { "Guid": "49ba7088-b6cf-46a9-9232-2c0084106d38", "ResourceTypeId": 1, "CreationDateStr": "2015-01-03T15:50:18.020Z", "LastUpdateDateStr": "2015-05-27T09:59:05.480Z", "Duration": 0.0 }, { "Guid": "05609f51-ec52-4fde-a3ca-d249cd0cd084", "ResourceTypeId": 1, "CreationDateStr": "2015-01-03T15:50:18.317Z", "LastUpdateDateStr": "2015-05-27T10:04:07.460Z", "Duration": 0.0 }, { "Guid": "9063f263-eb80-4ef5-92ed-75f2f44a8f42", "ResourceTypeId": 1, "CreationDateStr": "2015-01-03T15:50:18.630Z", "LastUpdateDateStr": "2015-05-27T10:08:03.280Z", "Duration": 0.0 }, { "Guid": "adefe3b5-c9c6-4563-9403-46b7cae81528", "ResourceTypeId": 1, "CreationDateStr": "2015-01-03T15:50:18.893Z", "LastUpdateDateStr": "2015-05-27T10:09:39.840Z", "Duration": 0.0 }, { "Guid": "5432304f-84b7-4ead-adab-a95c4da50b58", "ResourceTypeId": 1, "CreationDateStr": "2015-01-03T15:43:31.613Z", "LastUpdateDateStr": "2015-05-21T06:39:39.443Z", "Duration": 0.0 }, { "Guid": "940d97ac-1c29-4f9f-b937-820a4645f76b", "ResourceTypeId": 1, "CreationDateStr": "2015-01-03T15:43:30.050Z", "LastUpdateDateStr": "2015-05-21T06:37:37.240Z", "Duration": 0.0 }, { "Guid": "c90bf7e2-68fa-4b03-b478-3ed936d2635d", "ResourceTypeId": 1, "CreationDateStr": "2015-01-03T15:43:30.847Z", "LastUpdateDateStr": "2015-05-21T06:38:33.753Z", "Duration": 0.0 }]);
            
        },

        init = function () {
            tmodel.reporters(datacontext.users.getAllLocal());
        };


        return {
            activate: activate,
            config: config,
            e: e,
            submitNews: submitNews,
            tmodel: tmodel,
            tickerReporterControl: tickerReporterControl,
            appdata: appdata,
            router: router,
            clickCameramen: clickCameramen,
            currentResource: currentResource,
            setCurrentContent: setCurrentContent,
            templatePaging: templatePaging,
            setNews: setNews,
            utils:utils,
            init: init,
            selectSuggest: selectSuggest,
            sType:sType
            
        };

    });