﻿define('vm.tagreport',
  [
        'underscore',
        'jquery',
        'ko',
        'datacontext',
        'vm.topicselection',
        'router',
        'config',
        'model',
        'presenter',
        'manager',
        'appdata',
        'presenter',
        'utils',
        'moment',
        'messenger',
        'enum',
        'vm.contentviewer',
        'model.mapper',
        'model.userfilter',
        'model.reportnews',
        'vm.tagnews',
  ],
    function (_, $, ko, dc, topicSelectionVM, router, config, model, presenter, manager, appdata, presenter, utils, moment, messenger, e, contentviewer, mapper, userfilter, ReportNews, tagnews) {
        var logger = config.logger;

        var
            // Properties
            // ------------------------

            hashes = config.hashes.production,
            templates = config.templateNames,
            pageIndex = ko.observable(1),
            selectedNewsId = appdata.selectedNewsId,
            searchKeywords = ko.observable('').extend({ throttle: config.throttle }),
            sourceFilterControl = new model.SourceFilterControl(),
            calendarControl = new model.CalendarControl(),
            bunchArray = ko.observableArray(),
            pendingStoriesArray = ko.observableArray(),
            searchGroupName = ko.observable(),
            currentBunch = ko.observable(new model.Bunch()),
            currentView = ko.observable('listView'),
            selectedAudioClip = ko.observable(),
            audioComments = ko.observable(),
            isUpdateScroll = ko.observable(false),
            isStoreScroll = ko.observable(false),
            viewBreakingNews = ko.observable(false),
            currentBreakingNews = ko.observable(),
            xmlParserObj = new model.XmlParser(),
            scrollPos = 0,
            newsFiles = ko.observableArray(),
            isNewsMarked = ko.observable(false),
            parentWindowCss = ko.observable(''),
            selectedNewsTicker = ko.observable(''),
            addNewTicker = ko.observable(false),
            foldersListCopyTo = ko.observableArray(),
            lastnewsFileID = '',
            currentNewsFile = ko.observable(),
            testEnt = ko.observable("askdlhsad"),
            refreshAllNews = ko.observable(false),
            freshNewsFilesListOutput = ko.observableArray([]),
            freshNewsFilesListPortal = ko.observableArray([]),
            freshNewsFilesListReviewed = ko.observableArray([]),
            maxDt = null,


            // Computed Properties
            // ------------------------

       
        loadNextPage = function () {
            presenter.toggleActivity(true);
            appdata.newsFilesLastUpdateDate = '';
            if (appdata.currentUser().userType == e.UserType.controllerInput || appdata.currentUser().userType === e.UserType.Controlleroutput) {
                manager.newsfile.newsFileProducerPolling(true);
            }
        },

        pollingTReport = function () {
            presenter.toggleActivity(true);
            var newDt = JSON.parse(JSON.stringify(maxDt));
            newDt = newDt.replace('T', ' ').replace('Z','');
            GetInitialData(newDt);
        },

          populateNews = function () {
              var nFiles = dc.newsFiles.getAllLocal();
              var list = [];
              if (nFiles.length > 0) {
                  for (var i = 0 ; i < nFiles.length; i++) {
                      //nFiles[i].programTimeFormatted = moment(nFiles[i].ProgramTime).format('ddd  DD-MMM-YYYY , hh:mm A');
                      if (new Date(maxDt) < new Date(nFiles[i].lastUpdateDate()))
                          maxDt = nFiles[i].lastUpdateDate();
                      if (!nFiles[i].isDeleted()) {
                          list.push(nFiles[i]);
                      }
                  }
                  list = list.sort(function (a, b) {
                      return new Date(a.programTime()) - new Date(b.programTime());
                  });
                  if (freshNewsFilesListPortal().length != list.length) {
                      freshNewsFilesListPortal('');
                      freshNewsFilesListPortal(list);
                  }
              }
          },
        
        searchEventNews = function (data) {
            if (data) {
                if (data.isSelected()) {
                    data.isSelected(false);
                    appdata.searchKeywords = '';
                    manager.news.setDataChangeTime();
                } else {
                    _.filter(sourceFilterControl.events(), function (obj) {
                        return obj.isSelected(false);
                    });
                    data.isSelected(true);
                    appdata.searchKeywords = data.searchTags;
                    manager.news.setDataChangeTime();
                }
            }
        },
        

      sortDesc = function (arr) {
          if (arr && arr.length > 1) {
              arr = arr.sort(function (entityA, entityB) {
                  return new Date(entityB.creationDate()) - new Date(entityA.creationDate());
              });
          }
          return arr;
      },

     sortBySequenceNo = function (arr) {
         if (arr && arr.length > 1) {
             arr = arr.sort(function (entityA, entityB) {
                 return (entityA.sequenceNo()) - (entityB.sequenceNo());
             });
         }
         return arr;
     },
     

        GetInitialData = function (dt) {
            $.when(manager.newsfile.GetNewsFileForTaggReport(dt))
             .done(function (responseData) {
                 if (responseData.IsSuccess && responseData.Data && responseData.Data.length > 0) {
                     populateNews();
                 }
             })
             .fail(function (responseData) {
                 logger.error("Request Failed");
             })
            .always(function (responseData) {
                presenter.toggleActivity(false);
                setTimeout(pollingTReport, 5000);
            });
        },


        ViewNews = function (data) {
            if (data && data.parentId) {
                data.id = data.parentId;
                tagnews.setNews(data);
                tagnews.tmodel.isNewsEdit(true);
                router.navigateTo(config.views.nmsBol.tagnews.url);
            }
        },


        activate = function (routeData, callback) {
            appdata.selectedNewsFile('');
            messenger.publish.viewModelActivated({ canleaveCallback: canLeave });
            GetInitialData();
        },

        canLeave = function () {
            return true;
        },

        init = function () {
           

        };
     
       

        return {
            activate: activate,
            canLeave: canLeave,
            appdata: appdata,
            init: init,
            hashes: hashes,
            pollingTReport: pollingTReport,
            templates:templates,
            e: e,
            utils: utils,
            router: router,
            config: config,
            freshNewsFilesListPortal: freshNewsFilesListPortal,
            ViewNews: ViewNews
         
        };
    });