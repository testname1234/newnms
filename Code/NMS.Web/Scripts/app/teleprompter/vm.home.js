﻿define('vm.home',
    [
        'manager',
        'model',
        'datacontext',
        'enum',
        'config',
        'utils',
        'control.editorcontrol',
		'moment',
        'helper',
        'control.runorder',
        'control.story',
        'control.storyitem'
    ],
    function (manager, model, dc, e, config, utils, EditorControl, moment, helper, Rorder, Rstory, RstoryItem) {
        var
            // Properties
            // ------------------------------
        userName = helper.getCookie('UserName');
        sessionId = helper.getCookie(config.sessionKeyName);
        userId = helper.getCookie(config.userid);
        allRunOrders = ko.observableArray([]),
        currentRunOrder = ko.observable(new model.RunOrder()),
        previousStory = ko.observable(new model.ROStory()),
        previousStoryItem = ko.observable(new model.StoryItem()),
        updatedSequence = ko.observableArray([]),
        alertCounter = ko.observable(0),
        jumpToStoryId = ko.observable(null),
        skippedUnskippedSId = ko.observable(null),
        RunOrderToDeleteId = ko.observable(null),
        autoLoad = ko.observable(),
        searchText = ko.observable(),
        mosId = ko.observable(new model.ConfigSetting()),
        mosPort = ko.observable(new model.ConfigSetting()),
        ncsId = ko.observable(new model.ConfigSetting()),
        ncsPort = ko.observable(new model.ConfigSetting()),
        hubIp = ko.observable(new model.ConfigSetting()),
        showEdit = ko.observable(false),
        showStory = ko.observable(true),
        FullStory = ko.observable(""),
        editor = ko.observable(new EditorControl('English', '')),
        clear = ko.observable(),
        tolerance = ko.observable(new model.ConfigSetting()),
        threadDelay = ko.observable(new model.ConfigSetting()),
        deletionSpanTimeSpan = ko.observable(new model.ConfigSetting()),
        cScrollSpeed = ko.observable({ value: null, isSelected: ko.observable() }),
        FontSizes = ko.observableArray([{ value: 50, isSelected: ko.observable(false) }, { value: 55, isSelected: ko.observable(false) }, { value: 60, isSelected: ko.observable(false) }, { value: 65, isSelected: ko.observable(false) }, { value: 70, isSelected: ko.observable(false) }, { value: 75, isSelected: ko.observable(false) }, { value: 80, isSelected: ko.observable(false) }, { value: 84, isSelected: ko.observable(false) }, { value: 88, isSelected: ko.observable(false) }, { value: 92, isSelected: ko.observable(false) }, { value: 96, isSelected: ko.observable(false) }, { value: 102, isSelected: ko.observable(false) }, { value: 106, isSelected: ko.observable(false) }, { value: 108, isSelected: ko.observable(false) }, { value: 110, isSelected: ko.observable(false) }, { value: 112, isSelected: ko.observable(false) }, { value: 114, isSelected: ko.observable(false) }, { value: 116, isSelected: ko.observable(false) }, { value: 118, isSelected: ko.observable(false) }, { value: 120, isSelected: ko.observable(false) }]),
        FontStyles = ko.observableArray([]),
        scrollSpeeds = ko.observableArray([{ value: 1.0, isSelected: ko.observable(false) }, { value: 2.0, isSelected: ko.observable(false) }, { value: 3.0, isSelected: ko.observable(false) }, { value: 4.0, isSelected: ko.observable(false) }, { value: 5.0, isSelected: ko.observable(false) }, { value: 6.0, isSelected: ko.observable(false) }, { value: 7.0, isSelected: ko.observable(false) }, { value: 8.0, isSelected: ko.observable(false) }, { value: 9.0, isSelected: ko.observable(false) }]),
        inputScrollSpeed = ko.observable(),
        cFontSize = ko.observable({ value: null, isSelected: ko.observable() }),
        inputFont = ko.observable(),
        cFontStyle = ko.observable(new model.Fontfamily()),
        cSelectedProfileId = ko.observable(),
        profiles = ko.observableArray([]),
        profileName = ko.observable(),
        playBtn = ko.observable(true),
        pauseBtn = ko.observable(false),
        promptONBtn = ko.observable(true),
        promptOFFBtn = ko.observable(false),
        previewONBtn = ko.observable(true),
        previewOFFBtn = ko.observable(false),
        cleanUpBtnText = ko.observable('Start'),
        mosBtnFlag = ko.observable(false),
        hubBtnFlag = ko.observable(false),
        availableActions = ko.observableArray(["PlayPause", "NextStory", "PrevStory", "RunorderTop"]),
        firstSelectedAction = ko.observable("Select Any Action"),
        secondSelectedAction = ko.observable("Select Any Action"),
        thirdSelectedAction = ko.observable("Select Any Action"),
        mappingUiBoxFlag = ko.observable(false);
        mappingBtnText = ko.observable("Configure Controller"),
        actionToMap = ko.observable(),
        cProfileIdToDelete = ko.observable(),
        playPauseFlag = ko.observable(true),
        forwardBackward = ko.observable(true),
        browseFileName = ko.observable(),
        RunDownToInsert = ko.observable(new Rorder()),
        listeningStatus = ko.observable(false),
        promptOnScreenStatus = ko.observable(false),
        jumpToStoryItemId = ko.observable();
        selectedLanguage = ko.observable(),

        // Computed Properties
        // ------------------------------
        filterRunOrders = ko.computed({
            read: function () {
                var runOrders = allRunOrders();
                var srchText = searchText();

                var arr = _.filter(runOrders, function (runOrder) {
                    if (filter(runOrder, srchText)) {
                        return true;
                    } else {
                        return false;
                    }
                });

                return arr;
            },
            deferEvaluation: true
        }),
        // Methods
        // ------------------------------

        filter = function (runOrder, srchText) {
            if (!srchText) return true;

            var srch = utils.regExEscape(srchText.toLowerCase());

            if (runOrder.slug().toLowerCase().search(srch) !== -1)
                return true;

            return false;
        },
        cMenuAction = function (key, id, roId, childstoryItemId) {
            if (key && ((id && id != null) || (roId && roId != null))) {
                if (key == "skip") {
                    storySkipUnSkip("isSkipped", id);
                } else if (key == "unSkip") {
                    storySkipUnSkip("unSkipped", id);
                } else if (key == "delete") {
                    deleteRundown(roId);
                } else if (key = "load") {
                    playJumpedStory(id, childstoryItemId);
                } else { }
            }
        },
        playJumpedStory = function (id, childstoryItemId) {
            if (id && id != null && currentRunOrder().id) {
                jumpToStoryId(id);
                jumpToStoryItemId(childstoryItemId);
                sendCommand(e.CommandType.JumpToStory_Prompter);
            } else {
                return false;
            }
        },
        storySkipUnSkip = function (value, id) {
            if (value == "unSkipped") {
                skippedUnskippedSId(id);
                sendCommand(e.CommandType.UnSkip_Prompter);
            }
            if (value == "isSkipped") {
                skippedUnskippedSId(id);
                sendCommand(e.CommandType.Skip_Prompter);
            }
        },
        deleteRundown = function (id) {
            var runOrderlist = allRunOrders();
            for (var i = 0; i < runOrderlist.length; i++) {
                if (id == runOrderlist[i].id) {
                    if (new Date(runOrderlist[i].roStartTime).getTime() > (new Date()).getTime()) {
                        var answer = confirm("you are deleting run down which is not played yet")
                        if (!answer) {
                            return false;
                        }
                    }
                    RunOrderToDeleteId(id);
                    sendCommand(e.CommandType.Delete_Prompter);
                }
            }
        },
        addRunOrder = function (data) {
            data.roStories.sort(function (a, b) {
                return a.orderId - b.orderId;
            });

            if (data) {
                var factor = (new Date()).getTime() - new Date(data.roStartTime).getTime();
                var duration = moment.duration(factor);
                var timeInMinutes = (duration.hours() * 60) + duration.minutes();
                if (timeInMinutes > tolerance() || timeInMinutes < (-tolerance())) {
                    var msg = "";
                    if (timeInMinutes > tolerance()) {
                        msg = "you are selecting rundown whose time is already expired if You Want to select Click Ok "

                    } else {
                        msg = "you are selecting rundow which is to be played in future if You Want to select Click Ok "
                    }

                    jConfirm(msg + data.roStartTime, 'Confirmation Dialog', function (answer) {
                        if (!answer) {
                            return false;
                        }
                        else {
                            currentRunOrder(data);
                            //sendCommand(e.CommandType.CurrentSelectedRunOrder_Prompter);
                        }
                    });

                } else {

                    currentRunOrder(data);
                    // sendCommand(e.CommandType.CurrentSelectedRunOrder_Prompter);
                }
            }
        },
        arrangeStories = function (d) {
            if (currentRunOrder.roId) {

                updatedSequence(d);
                sendCommand(e.CommandType.UpdateSequence_Prompter);
            }
        },
        removeStory = function (d) {
            // TODO------
        },
        resetCounter = function () {
            alertCounter(0);
        },
        sendCommand = function (commandType) {
            var cType = commandType;

            var obj = {};
            if (cType) {
                if (cType == e.CommandType.GetAllRunDowns_Prompter) {
                    obj = { RoId: 1, ThreadDelayTime: 2 };
                }
                //if (cType == e.CommandType.CurrentSelectedRunOrder_Prompter && currentRunOrder().id && currentRunOrder().id != null) {
                //    obj = { RoId: currentRunOrder().id };
                //}
                if (cType == e.CommandType.LoadToPrompter_Prompter && currentRunOrder().id && currentRunOrder().id != null) {
                    obj = { RoId: currentRunOrder().id };
                    currentRunOrder().isLoaded(true);
                }
                if (cType == e.CommandType.JumpToStory_Prompter && currentRunOrder().id && currentRunOrder().id != null) {
                    obj = { RoId: currentRunOrder().id, StoryId: jumpToStoryId(), StoryItemId: jumpToStoryItemId() };
                }
                if (cType == e.CommandType.Skip_Prompter && currentRunOrder().id && currentRunOrder().id != null) {
                    obj = { RoId: currentRunOrder().id, StoryId: skippedUnskippedSId() };
                }
                if (cType == e.CommandType.UnSkip_Prompter && currentRunOrder().id && currentRunOrder().id != null) {
                    obj = { RoId: currentRunOrder().id, StoryId: skippedUnskippedSId() };
                }
                if (cType == e.CommandType.Delete_Prompter && RunOrderToDeleteId() != null) {
                    obj = { RoId: RunOrderToDeleteId() };
                }
                if (cType == e.CommandType.UpdateSequence_Prompter && currentRunOrder()) {
                    obj = { RoId: currentRunOrder().id, UpdatedSequence: updatedSequence() };
                }
                if (cType == e.CommandType.StartListening_Prompter) {
                    obj = { MosId: mosId().value(), MosPort: mosPort().value(), SettingIdA: mosId().id, SettingIdB: mosPort().id };
                }
                if (cType == e.CommandType.ConnectHub_Prompter) {
                    obj = { MosId: mosId().value(), NcsId: ncsId().value(), NcsPort: ncsPort().value(), HubIp: hubIp().value(), SettingIdA: mosId().id, SettingIdB: ncsId().id, SettingIdC: ncsPort().id, SettingIdD: hubIp().id };
                }
                if (cType == e.CommandType.EditText_Save) {
                    obj = { RoId: currentRunOrder().id, StoryId: skippedUnskippedSId() };
                }
                if (cType == e.CommandType.EditText_Prompter) {
                    obj = { RoId: currentRunOrder().id, StoryId: skippedUnskippedSId() };
                }
                if (cType == e.CommandType.StartCleanUp_Prompter) {
                    obj = { ThreadDelayTime: threadDelay().value(), DeletionSpan: deletionSpanTimeSpan().value(), SettingIdA: threadDelay().id, SettingIdB: deletionSpanTimeSpan().id };
                }
                if (cType == e.CommandType.SetSpeed_Prompter) {
                    obj = { Step: cScrollSpeed().value };
                }
                if (cType == e.CommandType.CurrentFontSize_Prompter) {
                    obj = { SelectedFontSize: cFontSize().value };
                }
                if (cType == e.CommandType.CurrentFontFamily_Prompter) {
                    obj = { SelectedFontStyleId: cFontStyle().id };
                }
                if (cType == e.CommandType.NewProfile_Prompter) {
                    obj = { ProfileName: profileName(), Step: cScrollSpeed().value, SelectedFontSize: cFontSize().value, SelectedFontStyleId: cFontStyle().id };
                }
                if (cType == e.CommandType.SetProfile_Prompter) {
                    obj = { RoId: cSelectedProfileId() };
                }
                if (cType == e.CommandType.MapActionToKey_Prompter) {
                    obj = { ActionToMap: actionToMap() };
                }
                if (cType == e.CommandType.DeleteProfile_Prompter) {

                    obj = { RoId: cProfileIdToDelete() };
                }
                if (cType == e.CommandType.PlayPauseToggle_Prompter) {

                    obj = { PlayPuaseToggle: playPauseFlag() };
                }
                if (cType == e.CommandType.PlayPauseToggleBackward_Prompter) {

                    obj = { PlayPuaseToggle: forwardBackward() };
                }
                if (cType == e.CommandType.Tolerance_Prompter) {

                    obj = { Tolerance: tolerance().value(), SettingIdA: tolerance().id };
                }
                if (cType == e.CommandType.BrowseXML_Prompter) {

                    obj = { ActionToMap: browseFileName() };
                }

                app.SendCommand(e.CommandSenderType.Teleprompter, cType, JSON.stringify(obj));
            } else { return false; }

        };

        saveSettings = function () {
            if (mosId && mosPort && mosId().value() && mosPort().value) {
                mosBtnFlag(true);
                sendCommand(e.CommandType.StartListening_Prompter)
            } else {
                config.logger.error("enter Mos id and port");
                return false;
            }
        };
        connectHub = function () {

            if (mosId && ncsId && ncsPort && hubIp && mosId && ncsId().value() && ncsPort().value() && hubIp().value()) {
                hubBtnFlag(true);
                sendCommand(e.CommandType.ConnectHub_Prompter)
            } else {
                config.logger.error("enter NCS id and port");
                return false;
            }
        };
        activate = function (routeData, callback) {
            sendCommand(e.CommandType.GetAllRunDowns_Prompter);
        },
        canLeave = function () {

        },
        init = function () {

        };

        window.recieveRundowns = function (d, status) {
            d = JSON.parse(d);
            if (d && d != null && d != "") {
                if (status == "NEW") {      //1 means new

                    alertCounter(alertCounter() + 1);
                    loadData(d);
                    config.logger.success("New Rundown Recieved");

                }
                else if (status == "OLD") {   //2 means old
                    loadData(d);
                    config.logger.success("Rundowns Loaded");

                }
                else if (status == "UPDATED") {   //3 updated

                    var updatedRunOrder = new model.RunOrder();
                    if (d[0] && allRunOrders().length > 0) {
                        for (var i = 0; i < allRunOrders().length; i++) {
                            if (allRunOrders()[i].id == d[0].RoId) {
                                updatedRunOrder = allRunOrders()[i];
                            }
                        }
                    }
                    loadData(d);
                    if (currentRunOrder() && currentRunOrder().id != null) {
                        currentRunOrder(updatedRunOrder);
                    }

                    config.logger.success("Rundowns Updated");
                } else { }

            }
        };

        loadData = function (d) {
            if (d) {


                var tempStories = [];
                var tempStoryItems = [];

                for (var i = 0; i < d.length; i++) {
                    for (var j = 0; j < d[i].Stories.length; j++) {
                        if (d[i].Stories[j].StoryItems && d[i].Stories[j].StoryItems !== null) {
                            for (var k = 0; k < d[i].Stories[j].StoryItems.length; k++) {
                                tempStoryItems.push(d[i].Stories[j].StoryItems[k]);
                            }
                        }
                    }
                }
                dc.storyitem.fillData(tempStoryItems, { groupROStoryItemsByStoryId: true });

                for (var i = 0; i < d.length; i++) {
                    for (var j = 0; j < d[i].Stories.length; j++) {
                        tempStories.push(d[i].Stories[j]);
                    }
                }

                dc.rostory.fillData(tempStories, { groupROStoryByRunOrderId: true });

                dc.runorder.fillData(d);

                var allRunDowns = dc.runorder.getObservableList();
                allRunOrders(allRunDowns);

                if (autoLoad()) {
                    for (var i = 0; i < allRunOrders().length; i++) {
                        if (d[0].RoId == allRunOrders()[i].id) {
                            currentRunOrder(allRunOrders()[i]);
                        }
                    }
                }

            } else {
                alert('Run Downs Cant be fetched');
            }
        };
        window.playToggleCallBack = function (d) {
            if (d == "True") {
                playPauseFlag(true);
            } else {
                playPauseFlag(false);
            }
        };
        window.forwardBackwardCallBack = function (d) {
            if (d == "True") {
                forwardBackward(true);
            } else {
                forwardBackward(false);
            }
        };

        window.skipCallBack = function (d) {
            if (d) {
                for (var i = 0; i < currentRunOrder().roStories().length; i++) {
                    if (skippedUnskippedSId() == currentRunOrder().roStories()[i].id) {
                        currentRunOrder().roStories()[i].isSkipped(true);
                    }
                }
            }

        };
        window.unSkipCallBack = function (d) {
            if (d) {
                for (var i = 0; i < currentRunOrder().roStories().length; i++) {
                    if (skippedUnskippedSId() == currentRunOrder().roStories()[i].id) {
                        currentRunOrder().roStories()[i].isSkipped(false);
                    }
                }
            }
        };
        window.startCallBack = function (d) {
            if (d == "True") {
                listeningStatus(true);
            } else {
                listeningStatus(false);
            }
        };
        window.stopCallBack = function (d) {
            if (d == "Error") {
                mosBtnFlag(true)
            }
        };
        window.promptOFF = function (d) {
        };
        window.promptON = function (d) {
            if (d == "True") {
                promptOnScreenStatus(true);
            } else {
                promptOnScreenStatus(false);
            }
        };
        window.flipTextCallBack = function (d) {

        };
        window.deleteCallBack = function (d) {
            if (d) {
                var runOrderlist = allRunOrders();
                for (var i = 0; i < runOrderlist.length; i++) {
                    if (d == runOrderlist[i].id) {
                        runOrderlist.remove(runOrderlist[i]);
                        allRunOrders.valueHasMutated();
                    }
                }
            }
        };
        window.storyJumpCallBack = function (d) {
            if (d == "Error") {

            }
            else if (d == "Success") {

            }
        };
        window.storyStarted = function (d, a) {
            if (previousStory() || previousStoryItem()) {
                previousStory().isPlaying(false);
                previousStoryItem().isPlaying(false);
                previousStory().isPlayed(true);
                previousStoryItem().isPlayed(true);
            }

            for (var k = 0; k < allRunOrders().length; k++) {
                if (allRunOrders()[k].isLoaded()) {
                    for (var i = 0; i < allRunOrders()[k].roStories().length; i++) {
                        if (d == allRunOrders()[k].roStories()[i].id) {
                            previousStory(allRunOrders()[k].roStories()[i]);
                            allRunOrders()[k].roStories()[i].isPlaying(true);
                            for (var j = 0; j < allRunOrders()[k].roStories()[i].storyItems().length; j++) {
                                if (a == allRunOrders()[k].roStories()[i].storyItems()[j].id) {
                                    previousStoryItem(allRunOrders()[k].roStories()[i].storyItems()[j]);
                                    allRunOrders()[k].roStories()[i].storyItems()[j].isPlaying(true);
                                }
                            }
                            break;
                        }
                    }
                }
            }
            //for (var i = 0; i < currentRunOrder().roStories().length; i++) {
            //    if (d == currentRunOrder().roStories()[i].id) {
            //        previousStory(currentRunOrder().roStories()[i]);
            //        currentRunOrder().roStories()[i].isPlaying(true);
            //        for (var j = 0; j < currentRunOrder().roStories()[i].storyItems().length; j++) {
            //            if (a == currentRunOrder().roStories()[i].storyItems()[j].id) {
            //                previousStoryItem(currentRunOrder().roStories()[i].storyItems()[j]);
            //                currentRunOrder().roStories()[i].storyItems()[j].isPlaying(true);
            //            }
            //        }
            //        break;
            //    }
            //}
        };
        window.currentRunOrderCallBack = function () {
        },
        window.updatedSequenceCallBack = function () {
            config.logger.success("Sequence Updated");

        };
        window.hubConnectionCallBack = function (d) {
            if (d == "Error") {
                hubBtnFlag(false);
            }
        };
        window.StartCleanUpCallBack = function (d) {
            if (d == "Error") {
                cleanUpBtnText("Start")
                return false;
            }
            config.logger.success("cleanUp Started");
        };
        window.StopCleanUpCallBack = function (d) {
            if (d == "Error") {
                cleanUpBtnText("Stop");
                return false;
            }
            config.logger.success("cleanUp Stopped");
        };
        window.LoadForEdit = function (str) {

            var story = '';
            var obj = editor();

            if (str) {
                for (var i = 0; i < currentRunOrder().roStories().length; i++) {
                    story += "<b>" + currentRunOrder().roStories()[i].slug() + "</b></br>";
                    story += "<b>" + currentRunOrder().roStories()[i].storyItems()[0].instruction + "</b></br>";
                    story += "<b>" + currentRunOrder().roStories()[i].storyItems()[0].details + "</b></br>";
                }

                obj.content(story);
                obj.isContentSet(true);
                editor(obj);

                showEdit(true);
                showStory(false);
            }
            else {
                showEdit(false);
                showStory(true);
                config.logger.error("Please Select a RunOrder First.");
            }

        };
        window.previewON = function (d) {
            if (d == "Error") {
                previewOFFBtn(false);
                previewONBtn(true);
            }
        };
        window.previewOFF = function (d) {
            if (d == "Error") {
                previewONBtn(false);
                previewOFFBtn(true);
            }
        };
        window.resetRunOrderCallback = function (d) {
            if (d == "Error") {
                //to do
                return false;
            }
            if (d == "Success") {
                config.logger.success("RunOrder Reset");
            }
        };

        //try catch exception
        window.exceptionOccured = function (d) {
            config.logger.error(d);
        };

        //Load Font and Family
        window.loadProfileCallBack = function (d) {
            dc.profile.fillData(d);
            var allProfiles = dc.profile.getObservableList();
            profiles(allProfiles);
        };
        window.loadFontfamilyCallBack = function (d) {
            dc.fontfamily.fillData(d);
            var allFontfamilies = dc.fontfamily.getObservableList();
            FontStyles(allFontfamilies);
        };
        window.loadSettingsCallBack = function (d) {
            dc.configsetting.fillData(d);
            var allSettings = dc.configsetting.getObservableList();
            for (var i = 0; i < allSettings.length; i++) {
                if (allSettings[i].id == e.ConfigurationSetting.MosId) {
                    mosId(allSettings[i]);
                }
                else if (allSettings[i].id == e.ConfigurationSetting.MosPort) {
                    mosPort(allSettings[i]);
                }
                else if (allSettings[i].id == e.ConfigurationSetting.NcsId) {
                    ncsId(allSettings[i]);
                }
                else if (allSettings[i].id == e.ConfigurationSetting.NcsPort) {
                    ncsPort(allSettings[i]);
                }
                else if (allSettings[i].id == e.ConfigurationSetting.IpAdress) {
                    hubIp(allSettings[i]);
                }
                else if (allSettings[i].id == e.ConfigurationSetting.Tolerance) {
                    tolerance(allSettings[i]);
                }
                else if (allSettings[i].id == e.ConfigurationSetting.AutoCheck) {
                    threadDelay(allSettings[i]);
                }
                else if (allSettings[i].id == e.ConfigurationSetting.AutoDelete) {
                    deletionSpanTimeSpan(allSettings[i]);
                } else { }
            }
        }

        //set settings
        window.setProfileCallBack = function (d) {
            if (d == "Success") {
                profileName('');
                config.logger.success("selected profile is set");

                var cFontValue;
                var cSSpeed;
                var cFontfamilyId;
                var fontSizeExistFlag = false;
                var scrollSpeedExistFlag = false;

                for (var i = 0; i < profiles().length; i++) {
                    if (profiles()[i].id == cSelectedProfileId()) {
                        cFontValue = profiles()[i].fontSize;
                        cSSpeed = profiles()[i].scrollSpeed;
                        cFontfamilyId = profiles()[i].fontfamilyId
                    }
                }

                //for family
                for (var i = 0; i < FontStyles().length; i++) {
                    if (FontStyles()[i].id == cFontfamilyId) {
                        cFontStyle().isSelected(false);
                        FontStyles()[i].isSelected(true);
                        cFontStyle(FontStyles()[i]);
                    }
                }

                //for fontsize
                for (var i = 0; i < FontSizes().length; i++) {
                    if (FontSizes()[i].value == cFontValue) {
                        cFontSize().isSelected(false);
                        FontSizes()[i].isSelected(true);
                        cFontSize(FontSizes()[i]);
                        fontSizeExistFlag = true;
                    }

                }

                //for scrollspeed
                for (var i = 0; i < scrollSpeeds().length; i++) {
                    if (scrollSpeeds()[i].value == cSSpeed) {
                        cScrollSpeed().isSelected(false);
                        scrollSpeeds()[i].isSelected(true);
                        cScrollSpeed(scrollSpeeds()[i]);
                        scrollSpeedExistFlag = true;
                    }
                }

                // if fontSize donot exist
                if (!fontSizeExistFlag) {
                    var obj = { value: cFontValue, isSelected: ko.observable(true) };
                    cFontSize().isSelected(false);
                    FontSizes.push(obj);
                    cFontSize(obj)
                }

                // if scroll speed donot exist
                if (!scrollSpeedExistFlag) {
                    var obj = { value: cSSpeed, isSelected: ko.observable(true) };
                    cScrollSpeed().isSelected(false);
                    scrollSpeeds.push(obj);
                    cScrollSpeed(obj);
                }

                return false;
            }
        };
        window.setSpeedCallBack = function (d) {
            if (d == "Success") {
                inputScrollSpeed('');
                config.logger.success("selected scrollSpeed is set");
                return false;
            }
            if (d == "Error") {
                cScrollSpeed().isSelected(false);
                config.logger.error("Pormpt On First");
            }
        };
        window.setfontSizeCallBack = function (d) {
            if (d == "Success") {
                inputFont('');
                config.logger.success("selected fontsize is set");
                return false;
            }
            if (d == "Error") {
                cFontSize().isSelected(false);
                config.logger.error("Pormpt On First");
            }
        };
        window.setfontfamilyCallBack = function (d) {
            if (d == "Success") {
                config.logger.success("selected fontFamily is set");
                return false;
            }
            if (d == "Error") {
                cFontStyle().isSelected(false);
                config.logger.error("Pormpt On First");
            }
        };
        window.newProfileAdderCallBack = function (d) {
            if (d == "Success") {
                config.logger.success("New Profile Added");
            }
        };

        window.onMappingModeCallBack = function (d) {
            if (d == "Error") {
                mappingUiBoxFlag(false);
                mappingBtnText("Configure Controller");
                config.logger.error("Prompt ON First");
                return false;
            }
            if (d == "Success") {
                config.logger.success("Start Mapping Keys");
            }
        };
        window.offMappingModeCallBack = function (d) {
            if (d == "Error") {
                mappingUiBoxFlag(true);
                mappingBtnText("Done");
                config.logger.error("Prompt ON First");
                return false;
            }
            if (d = "Success") {
                config.logger.success("Mapping Done");
            }

        };
        window.mappingKeyCallBack = function (d) {
            if (d) {
                config.logger.success(d + " Key Mapped Successfully");
            }
        };
        window.profileDeleteCallBack = function (d) {
            if (d == "Success") {
                for (var i = 0; i < profiles().length; i++) {
                    if (profiles()[i].id == cProfileIdToDelete()) {
                        profiles.remove(profiles()[i]);
                    }
                }
                config.logger.success("Profile Deleted");
            }

        };

        //settings functions
        cleanUp = function () {
            if (cleanUpBtnText() == "Start") {
                if (threadDelay && deletionSpanTimeSpan && threadDelay().value != null && deletionSpanTimeSpan().value != null) {
                    cleanUpBtnText("Stop");
                    sendCommand(e.CommandType.StartCleanUp_Prompter);
                } else {
                    config.logger('Enter TimeDelay and TimeSpan');
                }
                return false;
            }
            if (cleanUpBtnText() == "Stop") {
                cleanUpBtnText("Start");
                sendCommand(e.CommandType.StopCleanUp_Prompter)
            }
        }

        setFont = function (data) {

            if (cFontSize && cFontSize().value != null) {
                cFontSize().isSelected(false);
            }
            if (data && data.value != undefined) {
                data.isSelected(true);
            }
            if (data != null && data.value > 0 || inputFont() != null && inputFont() > 0) {
                if (data.value > 0) {
                    inputFont('');
                    cFontSize(data)
                }
                else if (inputFont() > 0) {
                    var cValue = parseInt(inputFont());
                    var obj = { value: cValue, isSelected: ko.observable() }
                    cFontSize(obj);
                } else {
                    return false;
                }
            } else { return false; }
            sendCommand(e.CommandType.CurrentFontSize_Prompter);
        };
        setScrollSpeed = function (data) {

            if (cScrollSpeed && cScrollSpeed().value != null) {
                cScrollSpeed().isSelected(false);
            }
            if (data && data.value != undefined) {
                data.isSelected(true);
            }
            if (data.value != null && data.value > 0 || inputScrollSpeed() != null && inputScrollSpeed() > 0) {
                if (data.value > 0) {
                    inputScrollSpeed('');
                    cScrollSpeed(data)
                }
                else if (inputScrollSpeed() > 0) {
                    var cValue = parseFloat(inputScrollSpeed());
                    var obj = { value: cValue, isSelected: ko.observable() }
                    cScrollSpeed(obj);
                } else { return false; }
            } else { return false; }
            sendCommand(e.CommandType.SetSpeed_Prompter);
        };
        setFontStyle = function (data) {
            cFontStyle().isSelected(false);
            data.isSelected(true);
            if (data && data.id) {
                cFontStyle(data);
                sendCommand(e.CommandType.CurrentFontFamily_Prompter);
            }
        };
        setProfile = function (data) {
            if (data && data.id) {
                cSelectedProfileId(data.id);
                sendCommand(e.CommandType.SetProfile_Prompter);
            }
        };
        newProfile = function () {
            if (profileName && cFontSize && cFontStyle && cScrollSpeed && profileName() != null && cFontSize() != null && cFontStyle != null && cScrollSpeed() != null) {
                sendCommand(e.CommandType.NewProfile_Prompter);
            } else {
                config.logger.error("select and fill Fields :[profile name]: [fontsize]: [fontstyle] :[scrollSpedd]");
            }

        };
        onMappingMode = function () {
            if (mappingBtnText() == "Configure Controller") {
                mappingBtnText("Done");
                mappingUiBoxFlag(true);
                sendCommand(e.CommandType.MappingModeOn_Prompter);
                return false;
            }
            if (mappingBtnText() == "Done") {
                mappingBtnText("Configure Controller");
                mappingUiBoxFlag(false);
                sendCommand(e.CommandType.MappingModeOFF_Prompter)
            }
        };

        deleteProfile = function (data) {
            if (data && data.id) {
                cProfileIdToDelete(data.id);
                sendCommand(e.CommandType.DeleteProfile_Prompter)
            }

        };

        //subscribers for setting Actions
        firstSelectedAction.subscribe(function () {
            if (firstSelectedAction && firstSelectedAction() != null && firstSelectedAction() != "Select Action..") {
                actionToMap(firstSelectedAction());
                sendCommand(e.CommandType.MapActionToKey_Prompter);
            }
        });
        secondSelectedAction.subscribe(function () {
            if (secondSelectedAction && secondSelectedAction() != null && secondSelectedAction() != "Select Action..") {
                actionToMap(secondSelectedAction());
                sendCommand(e.CommandType.MapActionToKey_Prompter);
            }
        });
        thirdSelectedAction.subscribe(function () {
            if (thirdSelectedAction && thirdSelectedAction() != null && thirdSelectedAction() != "Select Action..") {
                actionToMap(thirdSelectedAction());
                sendCommand(e.CommandType.MapActionToKey_Prompter);
            }
        });


        //LogOut function
        redirectToLogin = function () {
            window.location.href = '/login#/index';
            document.cookie = config.sessionKeyName + '=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';
            document.cookie = config.roleid + '=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';
            document.cookie = config.userid + '=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';
        };

        //Toaster
        window.showToaster = function (a, b) {
            if (b == "True") {
                config.logger.success(a);
                return false;
            }
            if (b == "False") {
                config.logger.error(a);
            }
        };

        //reset ui dependent on prompt OFF and ON
        resetUI = function () {
            pauseBtn(false);
            playBtn(true);
        };
        browseFile = function (data) {
            browseFileName(data);
            sendCommand(e.CommandType.BrowseXML_Prompter);
        }

        addNewStory = function (data) {
            var obj = new Rstory();
            data.Stories.push(obj);
        }
        addNewStoryItem = function (data) {
            var obj = new RstoryItem();
            data.StoryItems.push(obj);
        }

        removeStory = function (data) {
            RunDownToInsert().Stories.remove(data);
        }
        removeStoryItem = function (data) {
            for (var i = 0; i < RunDownToInsert().Stories().length; i++) {
                RunDownToInsert().Stories()[i].StoryItems.remove(data);
            }
        }
        insertRundown = function (data) {
            if (data) {

                app.insertRundown(ko.toJSON(RunDownToInsert()));
            }
        }

        scanDevice = function () {
            if (app) {
                app.scanForDevice();
            }
        }

        init();

        return {
            activate: activate,
            canLeave: canLeave,
            sendCommand: sendCommand,
            allRunOrders: allRunOrders,
            addRunOrder: addRunOrder,
            currentRunOrder: currentRunOrder,
            alertCounter: alertCounter,
            e: e,
            autoLoad: autoLoad,
            searchText: searchText,
            filterRunOrders: filterRunOrders,
            cMenuAction: cMenuAction,
            arrangeStories: arrangeStories,
            removeStory: removeStory,
            mosId: mosId,
            mosPort: mosPort,
            ncsId: ncsId,
            ncsPort: ncsPort,
            hubIp: hubIp,
            showEdit: showEdit,
            showStory: showStory,
            editor: editor,
            clear: clear,
            resetCounter: resetCounter,
            tolerance: tolerance,
            threadDelay: threadDelay,
            deletionSpanTimeSpan: deletionSpanTimeSpan,
            cScrollSpeed: cScrollSpeed,
            FontSizes: FontSizes,
            FontStyles: FontStyles,
            profiles: profiles,
            userName: userName,
            playBtn: playBtn,
            pauseBtn: pauseBtn,
            promptONBtn: promptONBtn,
            promptOFFBtn: promptOFFBtn,
            setFontStyle: setFontStyle,
            inputFont: inputFont,
            newProfile: newProfile,
            setProfile: setProfile,
            setFont: setFont,
            scrollSpeeds: scrollSpeeds,
            setScrollSpeed: setScrollSpeed,
            inputScrollSpeed: inputScrollSpeed,
            profileName: profileName,
            previewONBtn: previewONBtn,
            previewOFFBtn: previewOFFBtn,
            cleanUp: cleanUp,
            cleanUpBtnText: cleanUpBtnText,
            mosBtnFlag: mosBtnFlag,
            hubBtnFlag: hubBtnFlag,
            availableActions: availableActions,
            firstSelectedAction: firstSelectedAction,
            secondSelectedAction: secondSelectedAction,
            thirdSelectedAction: thirdSelectedAction,
            onMappingMode: onMappingMode,
            mappingUiBoxFlag: mappingUiBoxFlag,
            mappingBtnText: mappingBtnText,
            redirectToLogin: redirectToLogin,
            deleteProfile: deleteProfile,
            playPauseFlag: playPauseFlag,
            forwardBackward: forwardBackward,
            browseFile: browseFile,
            insertRundown: insertRundown,
            RunDownToInsert: RunDownToInsert,
            addNewStory: addNewStory,
            addNewStoryItem: addNewStoryItem,
            removeStory: removeStory,
            removeStoryItem: removeStoryItem,
            listeningStatus: listeningStatus,
            scanDevice: scanDevice,
            promptOnScreenStatus: promptOnScreenStatus,
            selectedLanguage: selectedLanguage
        };
    });