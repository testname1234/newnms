﻿(function () {
    var root = this;

    define3rdPartyModules();
    loadPluginsAndBoot();

    function define3rdPartyModules() {
        define('jquery', [], function () { return root.jQuery; });
        define('ko', [], function () { return root.ko; });
        define('amplify', [], function () { return root.amplify; });
        define('infuser', [], function () { return root.infuser; });
        define('moment', [], function () { return root.moment; });
        define('sammy', [], function () { return root.Sammy; });
        define('toastr', [], function () { return root.toastr; });
        define('underscore', [], function () { return root._; });
        define('string', [], function () { return root.S; });
    }

    function loadPluginsAndBoot() {
        requirejs([
                'ko.bindingHandlers',
                'ko.debug.helpers'
        ], boot);
    }

    function boot() {
        require(['bootstrapper'], function (bs) { bs.run(); });
    }
})();

//$(document).ready(function () {
//    // Keyboard Language Selection
//    $('.slctkeybrd').delegate('i', 'click', function (e) {
//        $(this).closest('.slctkeybrd').children('.languages').slideToggle();
//        $(".languages ul").mCustomScrollbar('destroy');
//        $(".languages ul:not(.mCustomScrollbar)").mCustomScrollbar({
//            autoDraggerLength: true,
//            autoHideScrollbar: true,
//            scrollInertia: 100,
//            advanced: {
//                updateOnBrowserResize: true,
//                updateOnContentResize: true
//            }
//        });
//        e.stopPropagation();
//    });
//    $('.slctkeybrd').delegate('.languages', 'click', function (e) {
//        e.stopPropagation();
//    });
//    $('body, html').click(function () {
//        $('.languages').slideUp();
//        $('.optionsList').slideUp();
//    });


//});