﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NMS.Core.Entities;

namespace NMS.Web.SignalRChat
{
    public class UserDetail
    {
        public string ConnectionId { get; set; }
        public int UserId { get; set; }
        public string UserName { get; set; }
        public Boolean Online { get; set; }
        public List<ClientSideMessageEntity> Messages { get; set; }
        public Boolean isSelected = false;
        public int NewCount = 0;
        public int Scroll = 0;
    }

   
}