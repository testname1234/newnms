﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.SignalR;
using SignalRChat.Common;
using MMS.Integration.UserManagement;
using System.Security.Cryptography;
using System.IO;
using System.Text;

namespace SignalRChat
{
    public class ChatHub : Hub
    {
        #region Data Members

        static List<UserDetail> ConnectedUsers = new List<UserDetail>();
        static List<MessageDetail> CurrentMessage = new List<MessageDetail>();

        #endregion

        #region Methods

        public void Connect(string userName)
        {
            if (ConnectedUsers.Count ==0)
                AddUserstoCurrentUsers();

            userName = userName.Replace(' ', '+');
            string encrypted = userName; 
            userName = EncryptionHelper.Decrypt(encrypted); 

            UserDetail thisUser = ConnectedUsers.Where(x => x.UserName == userName).FirstOrDefault();
            if (thisUser != null)
            {
                thisUser.ConnectionId = Context.ConnectionId;
                var id = thisUser.ConnectionId;
                //ConnectedUsers.Add(new UserDetail { ConnectionId = id, UserName = userName });

                thisUser.Online = true;
                ConnectedUsers = ConnectedUsers.OrderByDescending(x => x.Online==true).ToList();
                // send to caller
                Clients.Caller.onConnected(thisUser.UserId, userName, ConnectedUsers, CurrentMessage);

                // send to all except caller client
                Clients.AllExcept(id).onNewUserConnected(thisUser.UserId, thisUser.ConnectionId);

            }

        }


        public void AddUserstoCurrentUsers()
        {
            UserManagement userInt = new UserManagement();
            var userOut = userInt.UsersWithWorkRole(1);
            var users = userOut.Data;
            for (int i = 0; i < users.Count; i++)
            {
                var alreadyExist = ConnectedUsers.Where(x => x.UserId == users[i].UserId).FirstOrDefault();
                if(alreadyExist==null)
                    ConnectedUsers.Add(new UserDetail { UserId = users[i].UserId, UserName = users[i].Fullname });
            }
            
        }

        public void SendMessageToAll(string userName, string message)
        {
            // store last 100 messages in cache
            AddMessageinCache(userName, message);

            // Broad cast message
            Clients.All.messageReceived(userName, message);
        }

        public void SendPrivateMessage(string toConnId, string message)
        {

            string fromUserId = Context.ConnectionId;

            var toUser = ConnectedUsers.FirstOrDefault(x => x.ConnectionId == toConnId) ;
            var fromUser = ConnectedUsers.FirstOrDefault(x => x.ConnectionId == fromUserId);

            if (toUser != null && fromUser!=null)
            {
                // send to 
                Clients.Client(toConnId).sendPrivateMessage(fromUser.UserId, fromUser.UserName, fromUser.ConnectionId, message); 

                // send to caller user
                Clients.Caller.sendPrivateMessage(fromUser.UserId, fromUser.UserName, fromUser.ConnectionId, message); 
            }

        }

        public override System.Threading.Tasks.Task OnDisconnected()
        {
            var item = ConnectedUsers.FirstOrDefault(x => x.ConnectionId == Context.ConnectionId);
            if (item != null)
            {
                item.Online = false;

                var id = Context.ConnectionId;
                Clients.All.onUserDisconnected(item.UserId , item.ConnectionId , item.UserName);
            }
            return base.OnDisconnected();
        }

     
        #endregion

        #region private Messages

        private void AddMessageinCache(string userName, string message)
        {
            CurrentMessage.Add(new MessageDetail { UserName = userName, Message = message });

            if (CurrentMessage.Count > 100)
                CurrentMessage.RemoveAt(0);
        }

        #endregion
    }

}