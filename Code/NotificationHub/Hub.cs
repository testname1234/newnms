﻿using System;

namespace NotificationHub
{
    public class Hub
    {
        private int _port = 10541;

        public Hub(int port)
        {
            _port = port;
        }
    }
}
