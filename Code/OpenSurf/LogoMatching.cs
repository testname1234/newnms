﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenSURFcs;
using TMS.Core.Entities;

namespace OpenSurf
{
    public class LogoMatch
    {
        public LogoMatch()
        {
            this.IsMatch = false;
            Channels = new List<Channel>();
        }
        public bool IsMatch { get; set; }
        public double Ratio { get; set; }
        public int? ChannelId { get; set; }
        public int ChannelPointsCount { get; set; }

        public float MatchX1 { get; set; }
        public float MatchY1 { get; set; }
        public float MatchX2 { get; set; }
        public float MatchY2 { get; set; }

        public List<Channel> Channels { get; set; }
    }

    public class LogoMatching
    {
        private const float FLT_MAX = 3.402823466e+38F;

        public double Match(Bitmap bmp1, Bitmap bmp2)
        {
            List<IPoint> iPoints1 = GetImagePoints(bmp1);
            return Match(iPoints1, bmp2);
        }

        public double Match(List<IPoint> iPoints1, Bitmap bmp2)
        {
            List<IPoint> iPoints2 = GetImagePoints(bmp2);
            return Match(iPoints1, iPoints2);
       
        }

        public List<IPoint>[] GetDistinctMatches(List<IPoint>[] matches)
        {
            for (var i = 0; i < matches.Length; i++)
            {
                matches[i] = GetDistinctMatch(matches[i]);
            }
            return matches;
        }

          public List<IPoint> GetDistinctMatch(List<IPoint> points)
          {
              List<IPoint> distinctMatches = new List<IPoint>();
              foreach (var point in points)
              {
                  if (!distinctMatches.Contains(point))
                      distinctMatches.Add(point);
              }
              return distinctMatches;

          }
        public double Match(List<IPoint> iPoints1, List<IPoint> iPoints2)
        {
            double minLength = (double)Math.Min(iPoints1.Count, iPoints2.Count);
            double maxLength = (double)Math.Max(iPoints1.Count, iPoints2.Count);
            if ((minLength / maxLength) > 0.6)
            {
                var matches = GetMatches(iPoints1, iPoints2);
                double maxMatchLength = matches[0].Count > matches[1].Count ? matches[0].Count : matches[1].Count;
                return maxMatchLength / minLength;
            }
            else
            {
                return 0;
            }

        }

        public double MatchLogo(List<IPoint> logoPoints, List<IPoint> iPoints2)
        {
            double minLength = (double)Math.Min(logoPoints.Count, iPoints2.Count);
            double maxLength = (double)Math.Max(logoPoints.Count, iPoints2.Count);
            if ((logoPoints.Count > iPoints2.Count && (minLength / maxLength) > 0.6) || logoPoints.Count >= 8)
            {
                var matches = GetMatches(logoPoints, iPoints2);
                double maxMatchLength = matches[0].Count > matches[1].Count ? matches[0].Count : matches[1].Count;
                return maxMatchLength / minLength;
            }
            else
            {
                return 0;
            }

        }

        public bool Match(Bitmap bmp1, Bitmap bmp2, double threshold)
        {
            double ratio = Match(bmp1, bmp2);
            if (ratio >= threshold)
                return true;
            return false;
      
        }

        public List<IPoint> GetImagePoints(Bitmap bmp, float thresh = 0.0002f)
        {
            IntegralImage iimg = IntegralImage.FromImage(bmp);

            // Extract the interest points
            List<IPoint> ipts = FastHessian.getIpoints(thresh, 5, 2, iimg);
            // Describe the interest points
            SurfDescriptor.DecribeInterestPoints(ipts, false, false, iimg);
            return ipts;
        }

      

        public List<IPoint>[] GetMatchesOptimized2(List<IPoint> ipts1, List<IPoint> ipts2)
        {
            int arrayCount = 64;
            float sumX = 90;
            float sumY = 72;
            float padding = 20;
            List<IPoint>[] leftPoints = new List<IPoint>[arrayCount];
            List<IPoint>[] rightPoints = new List<IPoint>[arrayCount];
            float x = 0;
            float y = 0;
            for (var i = 0; i < arrayCount; i++)
            {
                if (x > 720)
                {
                    y += sumY;
                    x = 0;
                }
                float x2 = x + sumX;
                float y2 = y + sumY;
                leftPoints[i] = ipts1.Where(u => u.x >= (x - padding) && u.x <= (x2 + padding) && u.y >= (y - padding) && u.y <= (y2 + padding)).ToList();
                rightPoints[i] = ipts2.Where(u => u.x >= (x - padding) && u.x <= (x2 + padding) && u.y >= (y - padding) && u.y <= (y2+padding)).ToList();
                x += sumX;
            }
            int count1 = rightPoints.Sum(w => w.Count);
            int count2 = leftPoints.Sum(w => w.Count);
            List<IPoint>[] matches = new List<IPoint>[2];
            matches[0] = new List<IPoint>();
            matches[1] = new List<IPoint>();
            for (var i = 0; i < arrayCount; i++)
            {
                var mches = GetMatches(leftPoints[i], rightPoints[i]);
                matches[0].AddRange(mches[0]);
                matches[1].AddRange(mches[1]);
            }
            return matches;
        }

        public List<IPoint>[] GetMatchesOptimized(List<IPoint> ipts1, List<IPoint> ipts2,int padding=20)
        {
            bool different = false;
            if (ipts1.Count < ipts2.Count)
            {
                List<IPoint> temp = new List<IPoint>();
                temp.AddRange(ipts1);
                ipts1 = ipts2;
                ipts2 = temp;
                different = true;
            }
            double dist;
            double d1, d2;
            IPoint match = new IPoint();

            List<IPoint>[] matches = new List<IPoint>[2];
            matches[0] = new List<IPoint>();
            matches[1] = new List<IPoint>();
            for (int i = 0; i < ipts1.Count; i++)
            {
                d1 = d2 = FLT_MAX;
                bool flag = false;
                for (int j = 0; j < ipts2.Count; j++)
                {
                    if ((ipts1[i].x - padding) < ipts2[j].x && (ipts1[i].x + padding) > ipts2[j].x && (ipts1[i].y - padding) < ipts2[j].y && (ipts1[i].y + padding) > ipts2[j].y)
                    {
                        dist = GetDistance(ipts1[i], ipts2[j]);
                       // Console.WriteLine(ipts2[j].x);
                        if (dist < d1) // if this feature matches better than current best  
                        {
                            d2 = d1;
                            d1 = dist;
                            match = ipts2[j];
                            flag = true;
                        }
                        else if (dist < d2) // this feature matches better than second best  
                        {
                            d2 = dist;
                            flag = true;
                        }
                    }
                    //else d1 = d2 = FLT_MAX;
                }
                // If match has a d1:d2 ratio < 0.65 ipoints are a match  
                if (d1 / d2 < 0.77 && flag) //Match  
                {
                    matches[0].Add(ipts1[i]);
                    matches[1].Add(match);
                }
            }

            matches = GetDistinctMatches(matches);
            matches = GetMatches(matches[0], matches[1]);

            if (different)
            {
                List<IPoint> temp = new List<IPoint>();
                temp.AddRange(matches[0]);
                matches[0] = matches[1];
                matches[1] = temp;
            }

            return matches;
        }


        public List<IPoint>[] GetMatches(List<IPoint> ipts1, List<IPoint> ipts2)
        {
            double dist;
            double d1, d2;
            IPoint match = new IPoint();

            List<IPoint>[] matches = new List<IPoint>[2];
            matches[0] = new List<IPoint>();
            matches[1] = new List<IPoint>();
            for (int i = 0; i < ipts1.Count; i++)
            {
                d1 = d2 = FLT_MAX;

                for (int j = 0; j < ipts2.Count; j++)
                {
                    dist = GetDistance(ipts1[i], ipts2[j]);

                    if (dist < d1) // if this feature matches better than current best  
                    {
                        d2 = d1;
                        d1 = dist;
                        match = ipts2[j];
                    }
                    else if (dist < d2) // this feature matches better than second best  
                    {
                        d2 = dist;
                    }
                }
                // If match has a d1:d2 ratio < 0.65 ipoints are a match  
                if (d1 / d2 < 0.77) //Match  
                {
                    matches[0].Add(ipts1[i]);
                    matches[1].Add(match);
                }
            }
            return matches;
        }

        public List<IPoint>[] GetMatchesTaskParallal(List<IPoint> ipts1, List<IPoint> ipts2)
        {
            IPoint match = new IPoint();
            List<IPoint>[] matches = new List<IPoint>[2];
            matches[0] = new List<IPoint>();
            matches[1] = new List<IPoint>();
            int pageSize = (ipts1.Count * ipts2.Count) / 60000;
            if (pageSize == 0) pageSize = 1;
            int pages = (int)Math.Ceiling((float)ipts1.Count / (float)pageSize);
            Parallel.For(0, pages, x =>
            {
                for (int i = x * pageSize; i < ((x * pageSize) + pageSize) && i < ipts1.Count; i++)
                {
                    double dist;
                    double d1, d2;
                    d1 = d2 = FLT_MAX;
                    for (int j = 0; j < ipts2.Count; j++)
                    {
                        dist = GetDistance(ipts1[i], ipts2[j]);
                        if (dist < 0.001)
                            d1 = d2 = FLT_MAX;
                        if (dist < d1) // if this feature matches better than current best  
                        {
                            d2 = d1;
                            d1 = dist;
                            match = ipts2[j];
                        }
                        else if (dist < d2) // this feature matches better than second best  
                        {
                            d2 = dist;
                        }
                    }
                    // If match has a d1:d2 ratio < 0.65 ipoints are a match  
                    if (d1 / d2 < 0.77) //Match  
                    {
                        lock (matches)
                        {
                            matches[0].Add(ipts1[i]);
                            matches[1].Add(match);
                        }
                    }
                }
            });
            return matches;
        }

     
        private static double GetDistance(IPoint ip1, IPoint ip2)
        {
            float sum = 0.0f;
            for (int i = 0; i < 64; ++i)
                sum += (ip1.descriptor[i] - ip2.descriptor[i]) * (ip1.descriptor[i] - ip2.descriptor[i]);
            return Math.Sqrt(sum);
        }


     
        

    }
}
