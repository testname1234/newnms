﻿using System.Web;
using System.Web.Optimization;

namespace PCRConfigAPI
{
    public class BundleConfig
    {
        // For more information on Bundling, visit http://go.microsoft.com/fwlink/?LinkId=254725
        public static void RegisterBundles(BundleCollection bundles)
        {           
            bundles.Add(new ScriptBundle("~/bundles/app/")
                .IncludeDirectory("~/Scripts/app/", "*.js", searchSubdirectories: true));

            bundles.Add(new ScriptBundle("~/bundles/app/bindCommon/")
                .IncludeDirectory("~/Scripts/app/bindCommon/", "*.js", searchSubdirectories: false));

            bundles.Add(new ScriptBundle("~/bundles/app/common/")
                .IncludeDirectory("~/Scripts/app/common/", "*.js", searchSubdirectories: false));

            bundles.Add(new ScriptBundle("~/bundles/app/manager/")
                .IncludeDirectory("~/Scripts/app/manager/", "*.js", searchSubdirectories: false));

            bundles.Add(new ScriptBundle("~/bundles/app/models/")
                .IncludeDirectory("~/Scripts/app/models/", "*.js", searchSubdirectories: false));

            bundles.Add(new ScriptBundle("~/bundles/app/notificationHub/")
                .IncludeDirectory("~/Scripts/app/notificationHub/", "*.js", searchSubdirectories: false));
        }
    }
}