﻿using System;
using System.Collections.Generic;
using PcrConfiguration.Core;
using PcrConfiguration.Core.IService;
using PcrConfiguration.Core.DataTransfer;
using PcrConfiguration.Core.DataTransfer.Device;
using System.Web.Http;
using PcrConfiguration.WebAPI.ActionFilters;
using PcrConfiguration.Core.Entities;


namespace PcrConfiguration.WebAPI.Api
{
    public class DeviceController : ApiController
    {
        IDeviceService deviceService= IoC.Resolve<IDeviceService>("DeviceService");

         #region GET

        [HttpGet]
        [ActionName("GetAllDevices")]
        public DataTransfer<List<GetOutput>> GetAllDevices()
        {
            DataTransfer<List<GetOutput>> transfer = new DataTransfer<List<GetOutput>>();
            try
            {
                return deviceService.GetAll();
            }
            catch (Exception ex)
            {
                transfer.IsSuccess = false;
                transfer.Errors = new string[1];
                transfer.Errors[0] = ex.Message;
                return transfer;
            }
        }

        [HttpGet]
        [ActionName("GetDeviceById")]
        public DataTransfer<GetOutput> GetDeviceById(string id)
        {
            DataTransfer<GetOutput> transfer = new DataTransfer<GetOutput>();
            try
            {
                return deviceService.Get(id);
            }
            catch (Exception ex)
            {
                transfer.IsSuccess = false;
                transfer.Errors = new string[1];
                transfer.Errors[0] = ex.Message;
                return transfer;
            }
        }

        [HttpGet]
        [ActionName("GetDevicesByGroupId")]  
        public DataTransfer<List<PcrConfiguration.Core.DataTransfer.Device.GetOutput>> GetDevicesByGroupId(string id)
        {
            IDeviceService _iDeviceService = IoC.Resolve<IDeviceService>("DeviceService");

            DataTransfer<List<PcrConfiguration.Core.DataTransfer.Device.GetOutput>> transfer = new DataTransfer<List<PcrConfiguration.Core.DataTransfer.Device.GetOutput>>();
            try
            {
                int groupId = 0;
                if (int.TryParse(id, out groupId))
                {
                    transfer.Data = new List<PcrConfiguration.Core.DataTransfer.Device.GetOutput>();
                    List<Device> devices = _iDeviceService.GetDeviceByGroupId(int.Parse(id));
                    if (devices.Count > 0)
                    {
                        transfer.Data.CopyFrom(devices);
                    }

                }
                else
                {
                    transfer.IsSuccess = false;
                    transfer.Errors = new string[1];
                    transfer.Errors[0] = "Error: Id should be integer";
                }
            }
            catch (Exception ex)
            {
                transfer.IsSuccess = false;
                transfer.Errors = new string[1];
                transfer.Errors[0] = ex.Message;
            }
            return transfer;
        }

        #endregion

        #region POST

        [HttpPost]
        [ActionName("InsertDevice")]
        public DataTransfer<PostOutput> InsertDevice(PostInput Input)
        {
            DataTransfer<PostOutput> transfer = new DataTransfer<PostOutput>();
            try
            {
                return deviceService.Insert(Input);
            }
            catch (Exception ex)
            {
                transfer.IsSuccess = false;
                transfer.Errors = new string[1];
                transfer.Errors[0] = ex.Message;
                return transfer;
            }

        }

        #endregion

        #region PUT
        
        public DataTransfer<PutOutput> Put(PutInput Input)
        {
            DataTransfer<PutOutput> transfer = new DataTransfer<PutOutput>();
            try
            {
                return deviceService.Update(Input);
            }
            catch (Exception ex)
            {
                transfer.IsSuccess = false;
                transfer.Errors = new string[1];
                transfer.Errors[0] = ex.Message;
                return transfer;
            }
        }

        #endregion

        #region DELETE

        public DataTransfer<string> Delete(string id)
        {
            DataTransfer<string> transfer = new DataTransfer<string>();
            try
            {
                return deviceService.Delete(id);
            }
            catch (Exception ex)
            {
                transfer.IsSuccess = false;
                transfer.Errors = new string[1];
                transfer.Errors[0] = ex.Message;
                return transfer;
            }
        }

        #endregion
    }
}