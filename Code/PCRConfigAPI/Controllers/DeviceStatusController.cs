﻿using System;
using System.Collections.Generic;
using PcrConfiguration.Core;
using PcrConfiguration.Core.IService;
using PcrConfiguration.Core.DataTransfer;
using PcrConfiguration.Core.DataTransfer.DeviceStatus;
using System.Web.Http;
using PcrConfiguration.WebAPI.ActionFilters;
using PcrConfiguration.Core.Entities;


namespace PcrConfiguration.WebAPI.Api
{
    public class DeviceStatusController : ApiController
    {
        IDeviceStatusService deviceStatusService = IoC.Resolve<IDeviceStatusService>("DeviceStatusService");

        [HttpGet]
        [ActionName("GetAllDevicesStatuses")]
        public DataTransfer<List<GetOutput>> GetAllDevicesStatuses()
        {
            DataTransfer<List<GetOutput>> transfer = new DataTransfer<List<GetOutput>>();
            try
            {
                return deviceStatusService.GetAll();
            }
            catch (Exception ex)
            {
                transfer.IsSuccess = false;
                transfer.Errors = new string[1];
                transfer.Errors[0] = ex.Message;
                return transfer;
            }
        }

       
    }
}