﻿using System;
using System.Collections.Generic;
using PcrConfiguration.Core;
using PcrConfiguration.Core.IService;
using PcrConfiguration.Core.DataTransfer;
using PcrConfiguration.Core.DataTransfer.DeviceType;
using System.Web.Http;
using PcrConfiguration.WebAPI.ActionFilters;
using PcrConfiguration.Core.Entities;


namespace PcrConfiguration.WebAPI.Api
{
    public class DeviceTypeController : ApiController
    {
        IDeviceTypeService deviceTypeService= IoC.Resolve<IDeviceTypeService>("DeviceTypeService");

        [HttpGet]
        [ActionName("GetAllDevicesTypes")]
        public DataTransfer<List<GetOutput>> GetAllDevicesTypes()
        {
            DataTransfer<List<GetOutput>> transfer = new DataTransfer<List<GetOutput>>();
            try
            {
                return deviceTypeService.GetAll();
            }
            catch (Exception ex)
            {
                transfer.IsSuccess = false;
                transfer.Errors = new string[1];
                transfer.Errors[0] = ex.Message;
                return transfer;
            }
        }

       
    }
}