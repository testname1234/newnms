﻿using System;
using System.Collections.Generic;
using PcrConfiguration.Core;
using PcrConfiguration.Core.IService;
using PcrConfiguration.Core.DataTransfer;
using PcrConfiguration.Core.DataTransfer.Group;
using PcrConfiguration.WebAPI.ActionFilters;
using System.Web.Http;
using PcrConfiguration.Core.Entities;


namespace PcrConfiguration.WebAPI.Api
{
    public class GroupController : ApiController
    {
        IGroupService groupService= IoC.Resolve<IGroupService>("GroupService");

         #region GET

        [HttpGet]
        [ActionName("GetAllGroups")]
        public DataTransfer<List<GetOutput>> GetAllGroups()
        {
            DataTransfer<List<GetOutput>> transfer = new DataTransfer<List<GetOutput>>();
            try
            {
                return groupService.GetAll();
            }
            catch (Exception ex)
            {
                transfer.IsSuccess = false;
                transfer.Errors = new string[1];
                transfer.Errors[0] = ex.Message;
                return transfer;
            }
        }

        [HttpGet]
        [ActionName("GetGroupById")]
        public DataTransfer<GetOutput> GetGroupById(string id)
        {
            DataTransfer<GetOutput> transfer = new DataTransfer<GetOutput>();
            try
            {
                return groupService.Get(id);
            }
            catch (Exception ex)
            {
                transfer.IsSuccess = false;
                transfer.Errors = new string[1];
                transfer.Errors[0] = ex.Message;
                return transfer;
            }
        }

        [HttpGet]
        [ActionName("GetAllPcr")]
        public DataTransfer<List<PcrConfiguration.Core.DataTransfer.Group.GetOutput>> GetAllPcr()
        {
            IGroupService _iGroupService = IoC.Resolve<IGroupService>("GroupService");

            DataTransfer<List<PcrConfiguration.Core.DataTransfer.Group.GetOutput>> transfer = new DataTransfer<List<PcrConfiguration.Core.DataTransfer.Group.GetOutput>>();
            try
            {
                transfer.Data = new List<PcrConfiguration.Core.DataTransfer.Group.GetOutput>();
                List<Group> groups = _iGroupService.GetGroupByKeyValue("Name", "'PCR-101-Karachi'", Operands.Equal, "GroupId, Name");
                if (groups.Count > 0)
                {
                    transfer.Data.CopyFrom(groups);
                }

                else
                {
                    transfer.IsSuccess = true;
                    transfer.Errors = new string[1];
                    transfer.Errors[0] = "No Device Found";
                }
            }
            catch (Exception ex)
            {
                transfer.IsSuccess = false;
                transfer.Errors = new string[1];
                transfer.Errors[0] = ex.Message;
            }
            return transfer;
        }

        #endregion

        #region POST


        [HttpPost]
        [ActionName("InsertGroup")]
        public DataTransfer<PostOutput> InsertGroup(PostInput Input)
        {
            DataTransfer<PostOutput> transfer = new DataTransfer<PostOutput>();
            try
            {
                return groupService.Insert(Input);
            }
            catch (Exception ex)
            {
                transfer.IsSuccess = false;
                transfer.Errors = new string[1];
                transfer.Errors[0] = ex.Message;
                return transfer;
            }

        }

        #endregion

        #region PUT
        
        public DataTransfer<PutOutput> Put(PutInput Input)
        {
            DataTransfer<PutOutput> transfer = new DataTransfer<PutOutput>();
            try
            {
                return groupService.Update(Input);
            }
            catch (Exception ex)
            {
                transfer.IsSuccess = false;
                transfer.Errors = new string[1];
                transfer.Errors[0] = ex.Message;
                return transfer;
            }
        }

        #endregion

        #region DELETE

        public DataTransfer<string> Delete(string id)
        {
            DataTransfer<string> transfer = new DataTransfer<string>();
            try
            {
                return groupService.Delete(id);
            }
            catch (Exception ex)
            {
                transfer.IsSuccess = false;
                transfer.Errors = new string[1];
                transfer.Errors[0] = ex.Message;
                return transfer;
            }
        }

        #endregion
    }
}