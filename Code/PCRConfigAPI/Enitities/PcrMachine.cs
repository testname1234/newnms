﻿using PCRConfigAPI.Enum;
using System;

namespace PCRConfigAPI.Enitities
{
    public class PcrMachine
    {
        public string IP { get; set; }

        public string Id { get; set; }

        public int Port { get; set; }

        public int PcrId { get; set; }

        public bool IsMOSDevice { get; set; }

        public int DeviceTypeId { get; set; }

        public DeviceType DeviceType 
        {
            get
            {
                return (DeviceType)DeviceTypeId;
            }
            set
            {
                DeviceTypeId = (int)value;
            }
        }
    }
}