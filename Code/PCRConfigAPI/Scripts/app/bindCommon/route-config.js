﻿define('route-config',
    ['config', 'router', 'vm'],
    function (config, router, vm) {
        var
            logger = config.logger,

            register = function (views) {

                var routeData = [];

                for (var view in views) {
                    var tempView = views[view];
                        var obj = {
                            view: tempView.view,
                            routes:
                            [{
                                route: tempView.url,
                                title: tempView.title,
                                callback: vm.dictionary[tempView.view].activate,
                                group: '.route-top'
                            }]

                        };
                        router.register(obj);                
                }
                // Invalid routes
                var obj = {
                    view: '',
                    route: /.*/,
                    title: '',
                    callback: function () {
                        logger.error(config.toasts.invalidRoute);
                    }
                };
                router.register(obj);

                router.run();
                
            };


        return {
            register: register
           
        };
    });