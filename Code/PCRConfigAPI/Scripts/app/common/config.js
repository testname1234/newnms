﻿define('config',
    ['toastr'],
    function ( toastr) {
        var
            // Properties
            //------------
            ApiBaseUrl = '/api/',

            messages = {
                viewModelActivated: 'viewmodel-activation'
            },

            stateKeys = {
                lastView: 'state.active-hash'
            },         

            views = {
               
                notificationHub: {
                    group: { title: 'Groups', url: '#/Groups', isInTopMenu: true, view: "#Notifier-Groups-View" },
                    device: { title: 'Devices', url: '#/Devices', isInTopMenu: true, view: "#Notifier-Devices-View" },
                    shell: { title: '', url: '', isInTopMenu: false, view: "#shell-top-nav-view" }
                }
            },

            templateNames = {

            },

            toasts = {

            },    

            storageType = {
                localStorage: 0,
                sessionStorage: 1
            },

            localStorageKeys = {
                credentials: "credentials"
            },

            stateKeys = {
                lastView: 'state.active-hash'
            },

            toasts = {
                changesPending: 'Please save or cancel your changes before leaving the page.',
                errorSavingData: 'Data could not be saved. Please check the logs.',
                errorGettingData: 'Could not retrieve data.  Please check the logs.',
                invalidRoute: 'Cannot navigate. Invalid route',
                retreivedData: 'Data retrieved successfully',
                savedData: 'Data saved successfully'
            },

            dataserviceInit = function () {
            },
            configureExternalTemplates = function () {
                infuser.defaults.templatePrefix = "_";
                infuser.defaults.templateSuffix = ".tmpl.html";
                infuser.defaults.templateUrl = "/Tmpl";
                //infuser.defaults.ajax.cache = false;
                //infuser.defaults.ajax.async = false;
            },
            init = function () {
                dataserviceInit();
                configureExternalTemplates();
            };

        init();

        return {
            ApiBaseUrl: ApiBaseUrl,
            init: init,
            views: views,
            messages: messages,
            stateKeys: stateKeys,
            storageType: storageType,
            toasts: toasts
        }
    });