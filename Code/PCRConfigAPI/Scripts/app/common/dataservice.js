﻿define('dataservice',
    [
        'config'
    ],
    function (config) {
        var
            init = function () {
                
                amplify.request.define('get-all-pcr', 'ajax', {
                    url: config.ApiBaseUrl + 'Group/GetAllGroups',
                    dataType: 'json',
                    type: 'GET'
                });

                amplify.request.define('get-all-device', 'ajax', {
                    url: config.ApiBaseUrl + 'Device/GetAllDevices',
                    dataType: 'json',
                    type: 'GET'
                });

                amplify.request.define('update-group', 'ajax', {
                    url: config.ApiBaseUrl + 'Group',
                    dataType: 'json',
                    type: 'PUT'

                });

                amplify.request.define('update-device', 'ajax', {
                    url: config.ApiBaseUrl + 'Device',
                    dataType: 'json',
                    type: 'PUT'

                });

                amplify.request.define('delete-group', 'ajax', {
                    url: config.ApiBaseUrl + 'Group/Delete/{id}',
                    dataType: 'json',
                    type: 'DELETE'

                });

                amplify.request.define('delete-device', 'ajax', {
                    url: config.ApiBaseUrl + 'Device/Delete/{id}',
                    dataType: 'json',
                    type: 'DELETE'

                });

                amplify.request.define('insert-group', 'ajax', {
                    url: config.ApiBaseUrl + 'Group/InsertGroup/',
                    dataType: 'json',
                    type: 'POST'

                });

                amplify.request.define('insert-device', 'ajax', {
                    url: config.ApiBaseUrl + 'Device/InsertDevice/',
                    dataType: 'json',
                    type: 'POST'

                });

                amplify.request.define('get-all-device-type', 'ajax', {
                    url: config.ApiBaseUrl + 'DeviceType/GetAllDevicesTypes',
                    dataType: 'json',
                    type: 'GET'
                });

                amplify.request.define('get-all-device-Status', 'ajax', {
                    url: config.ApiBaseUrl + 'DeviceStatus/GetAllDevicesStatuses',
                    dataType: 'json',
                    type: 'GET'
                });

            },

            login = function (data) {
                return $.Deferred(function (deferred) {
                    amplify.request({
                        resourceId: 'login',
                        data: data,
                        success: deferred.resolve,
                        error: deferred.reject
                    })
                }).promise();
            },
            getAllPcr = function () {
                return $.Deferred(function (deferred) {
                    amplify.request({
                        resourceId: 'get-all-pcr',
                        success: deferred.resolve,
                        error: deferred.reject
                    });
                }).promise();
            },
            getAllDevice = function () {
                return $.Deferred(function (deferred) {
                    amplify.request({
                        resourceId: 'get-all-device',
                        success: deferred.resolve,
                        error: deferred.reject
                    });
                }).promise();
            },
        
            updateGroup = function (data) {
                return $.Deferred(function (deferred) {
                    amplify.request({
                        resourceId: 'update-group',
                        data: data,
                        success: deferred.resolve,
                        error: deferred.reject
                    })
                }).promise();
            };
            updateDevice = function (data) {
                return $.Deferred(function (deferred) {
                    amplify.request({
                        resourceId: 'update-device',
                        data: data,
                        success: deferred.resolve,
                        error: deferred.reject
                    })
                }).promise();
            };

            deleteGroup = function (data) {
                return $.Deferred(function (deferred) {
                    amplify.request({
                        resourceId: 'delete-group',
                        data: data,
                        success: deferred.resolve,
                        error: deferred.reject
                    })
                }).promise();
            };

            deleteDevice = function (data) {
                return $.Deferred(function (deferred) {
                    amplify.request({
                        resourceId: 'delete-device',
                        data: data,
                        success: deferred.resolve,
                        error: deferred.reject
                    })
                }).promise();
            };

            insertGroup = function (data) {
                return $.Deferred(function (deferred) {
                    amplify.request({
                        resourceId: 'insert-group',
                        data: data,
                        success: deferred.resolve,
                        error: deferred.reject
                    })
                }).promise();
            };

            insertDevice = function (data) {
                return $.Deferred(function (deferred) {
                    amplify.request({
                        resourceId: 'insert-device',
                        data: data,
                        success: deferred.resolve,
                        error: deferred.reject
                    })
                }).promise();
            };

            getAllDeviceType = function () {
                return $.Deferred(function (deferred) {
                    amplify.request({
                        resourceId: 'get-all-device-type',
                        success: deferred.resolve,
                        error: deferred.reject
                    });
                }).promise();
            },

            getAllDeviceStatus = function () {
                return $.Deferred(function (deferred) {
                    amplify.request({
                        resourceId: 'get-all-device-Status',
                        success: deferred.resolve,
                        error: deferred.reject
                    });
                }).promise();
            },

        init();
        
        return {
            getAllPcr: getAllPcr,
            getAllDevice: getAllDevice,
            updateGroup: updateGroup,
            updateDevice: updateDevice,
            deleteGroup: deleteGroup,
            deleteDevice: deleteDevice,
            insertGroup: insertGroup,
            insertDevice: insertDevice,
            getAllDeviceType: getAllDeviceType,
            getAllDeviceStatus: getAllDeviceStatus
        }
    });