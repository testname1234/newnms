﻿define('enum',
    [],
    function () {

        var
          DeviceType = {
              Teleprompter:1,
              WindowGraphics:2,
              NameBrandingGraphic:3,
              Playout:4,
              PCRUser:49
          };




        return {
            DeviceType: DeviceType
        }
    });