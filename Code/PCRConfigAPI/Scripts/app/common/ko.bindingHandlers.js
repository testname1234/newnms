﻿define('ko.bindingHandlers',
['jquery', 'ko', 'extensions', 'datacontext', 'underscore', 'moment', 'config'],
function ($, ko, ext, dc, _, moment, config) {
    var unwrap = ko.utils.unwrapObservable;

    // escape
    //---------------------------
    ko.bindingHandlers.escape = {
        update: function (element, valueAccessor, allBindingsAccessor, viewModel) {
            var command = valueAccessor();
            $(element).keyup(function (event) {
                if (event.keyCode === 27) { // <ESC>
                    command.call(viewModel, viewModel, event);
                }
            });
        }
    };
    ko.bindingHandlers.defaultClickOnItem = {
        init: function (element, valueAccessor, allBindingsAccessor) {
            var options = valueAccessor();
            if (options.canClick) {
                $(element).click();
            }
        },
        update: function (element, valueAccessor, allBindingsAccessor) {
            var options = valueAccessor();
            if (options.canClick) {
                $(element).click();
            }
        }
    };
    ko.bindingHandlers.datepicker = {
        init: function (element, valueAccessor, allBindingsAccessor) {
            //initialize datepicker with some optional options
            var options = allBindingsAccessor().datepickerOptions || {},
                $el = $(element);

            $el.datepicker(options);
            //handle the field changing
            ko.utils.registerEventHandler(element, "change", function () {
                var observable = valueAccessor();
                observable($el.datepicker("getDate"));
            });
            //handle disposal (if KO removes by the template binding)
            ko.utils.domNodeDisposal.addDisposeCallback(element, function () {
                $el.datepicker("destroy");
            });
        },
        update: function (element, valueAccessor) {
            var value = ko.utils.unwrapObservable(valueAccessor()),
                $el = $(element);

            //handle date data coming via json from Microsoft
            if (String(value).indexOf('/Date(') == 0) {
                value = new Date(parseInt(value.replace(/\/Date\((.*?)\)\//gi, "$1")));
            }

            var current = $el.datepicker("getDate");

            if (value - current !== 0) {
                $el.datepicker("setDate", value);
            }
        }
    };

    ko.bindingHandlers._tagIt = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            var source = allBindingsAccessor().source;
            var selectedObservableArrayInViewModel = valueAccessor();

            $(element).tagit({
                availableTags: source,
                allowSpaces: true,
                removeConfirmation: true,
                afterTagAdded: function (event, ui) {
                    selectedObservableArrayInViewModel.push($('.tagit-hidden-field', ui.tag).val());
                }
            });
        }
    };

    ko.bindingHandlers._multiSelect = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            var selectedObservableArrayInViewModel = valueAccessor();
            var source = selectedObservableArrayInViewModel.source;
            selectedObservableArrayInViewModel.onChange = function (option, checked) {
                var selectedItem = $(option).val();
                if (checked) {
                    source.push(selectedItem);
                }
                else {
                    source.remove(selectedItem);
                }
            };
            $(element).multiselect(selectedObservableArrayInViewModel);
        }
    };

    ko.bindingHandlers.editor = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            var selectedLanguage = $.trim(valueAccessor().language);
            var content = valueAccessor().content;
            var flag = false;
            var PlaceHolder = 'Enter Description...';

            var isContentSet = valueAccessor().isContentSet;
            if (selectedLanguage != '') {
                $(element).jqte({
                    change: function () {
                        if (isContentSet() && !flag) {
                            $(element).val(content());
                            flag = true;
                        }
                        var editorContent = $(element).val();
                        content(editorContent);
                    },

                    tabindex: '5'
                });
                $(element).jqteVal(content());
                $('.jqte_editor').attr('tabindex', '6');
            }
        }
    };

    ko.bindingHandlers.videoUploader = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            var valueAcce = new valueAccessor();
            var url = valueAcce.url;
            var options = valueAcce.options;
            var src = valueAcce.src;
            var started = valueAcce.start;
            var finished = valueAcce.done;
            var resources = valueAcce.resources;
            var uploadProgress = valueAcce.progress;
            var tokenReceived = valueAcce.tokenReceived;
            var filesUploadedCount = 0;
            var oFilesUploadedCount = valueAcce.filesUploadedCount;
            var uploadedResources = valueAcce.uploadedResources;

            $(element).fileupload({
                url: url,
                add: function (e, data) {
                    $.getJSON('/api/resource/GetResourceIds/' + data.files[0].name, function (result) {
                        if (result && result.IsSuccess) {
                            tokenReceived(true);
                            var resultdata = result.Data[0];
                            resources.push(resultdata);

                            options.headers = {};
                            options.headers['ResourceGuid'] = result.Data[0].Guid;

                            $(element).fileupload('option', options).bind('fileuploadstarted', function () {
                                if (started) {
                                    started(true);
                                    finished(true);
                                }
                            }).bind('fileuploadprogress', function (e, data) {
                                var progress = parseInt(data.loaded / data.total * 100, 10);
                                uploadProgress(progress + '%');
                            });
                            data.submit()
                                    .success(function (result, textStatus, jqXHR) {
                                    })
                                    .error(function (jqXHR, textStatus, errorThrown) {
                                    })
                                    .complete(function (result, textStatus, jqXHR) {
                                        filesUploadedCount++;
                                        oFilesUploadedCount(filesUploadedCount);
                                        uploadedResources.push(resultdata);

                                        if (finished()) {
                                            src(result.responseJSON[0].url);
                                        }

                                        finished(false);
                                    });
                        }
                    });
                }
            });
        }
    };

    ko.bindingHandlers.loadingBar = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            valueAccessor().showbar(false);
        },
    };

    ko.bindingHandlers.filterHover = {
        update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            var value = valueAccessor();
            var val = ko.unwrap(value);

            if (val == true) {
                $(element).find("ul > li").hover(function () {
                    $(this).find('ul:first').stop(true, true).fadeIn();
                }, function () {
                    $(this).find('ul:first').stop(true, true).fadeOut();
                });
            }
        }
    };

    ko.bindingHandlers.categoryHover = {
        update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            var value = valueAccessor();
            var val = ko.unwrap(value);
            if (val == true) {
                $(element).mouseenter(function () {
                    $(this).addClass('active');
                    $(this).find('ul').stop(true, true).animate({ left: '50px' }, 200, 'linear');
                }).mouseleave(function () {
                    $(this).removeClass('active');
                    $(this).find('ul').stop(true, true).animate({ left: '-185px' }, 200, 'linear');
                });
            }
        }
    };

    ko.bindingHandlers.categoryClick = {
        update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            //$(element).on('click', function () {
            //    $(this).find('ul').parent('li').addClass("active");
            //    $(this).find('ul').stop(true, true).animate({ left: '50px' }, 200, 'linear');
            //    $(this).on('mouseleave', function () {
            //        $(this).find('ul').parent('li').removeClass("active");
            //        $(this).find('ul').stop(true, true).animate({ left: '-185px' }, 200, 'linear');
            //    });
            //});
        }
    };

    ko.bindingHandlers.dateTimePicker = {
        init: function (element, valueAccessor, allBindingsAccessor) {

            var selectedObservableArrayInViewModel = valueAccessor();
            var source = selectedObservableArrayInViewModel;
            //  for initial time
            var date = new Date(source());
            var hours = date.getHours();
            var minutes = date.getMinutes();
            var ampm = hours >= 12 ? 'pm' : 'am';
            hours = hours % 12;
            hours = hours ? hours : 12; // the hour '0' should be '12'
            minutes = minutes < 10 ? '0' + minutes : minutes;
            var cTime = hours + ':' + minutes + ' ' + ampm;
            $('select', element).each(function () {
                var $this = $(this), val = $this.children('option:first-child').text();
                if (!$this.parent().hasClass('xSelect')) {
                    $this.wrap('<div class="xSelect" style="position:relative;" />');
                    $this.parent().append('<p>' + val + '</p>');
                    $this.css({ 'position': 'absolute', 'top': '0px', 'left': '0px', 'width': '100%', 'height': '100%', 'z-index': '10', 'opacity': 0 });
                    $this.on('change', function () {
                        val = $(this).val();

                        
                        $(this).parent().children('p').text(val);
                    });
                }
            });

            var myDate = new Date(source());
            var hours = myDate.getHours();
            var minutes = myDate.getMinutes();
            var ampm = hours >= 12 ? 'pm' : 'am';
            hours = hours % 12;
            hours = hours ? hours : 12; // the hour '0' should be '12'
            minutes = minutes < 10 ? '0' + minutes : minutes;
            var strTime = hours + ':' + minutes + ' ' + ampm;
            var monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
            var currentDate = myDate.getDate() + ' ' + (monthNames[myDate.getMonth()]) + ' ' + myDate.getFullYear();
            $("div.head > span:not(.time)", element).text(currentDate);
            $("div.head > span.time", element).text(', ' + strTime);

            $('.timepicker input.hour', element).val(hours);
            $('.timepicker input.min', element).val(minutes);
            if (ampm == 'am') {
                $('.timepicker select>option:eq(0)', element).attr('selected', true);
                $('.timepicker .xSelect p', element).text('am')
            } else {
                $('.timepicker select>option:eq(1)', element).attr('selected', true);
                $('.timepicker .xSelect p', element).text('pm')
            }

            $('.timepicker input[type=button]', element).click(function () {
                var hours = $('.timepicker input.hour', element).val();
                hours = hours < 10 ? '0' + hours : hours;
                var minutes = $('.timepicker input.min', element).val();
                minutes = minutes < 10 ? '0' + minutes : minutes;
                var amPm = $('.timepicker select.ampm', element).val();
                $("div.head > span.time", element).text(', ' + hours + ':' + minutes + ' ' + amPm);
                cTime = hours + ':' + minutes + ' ' + ampm;
                var date = $('.dates', element).text();
                source(date + ' ' + cTime);

            });

            $('div.head', element).click(function () {
                $(".optionsList .datepicker", element).datepicker({
                    showOn: "both",
                    inline: true,
                    showOtherMonths: true,
                    dayNamesMin: ['S', 'M', 'T', 'W', 'T', 'F', 'S'],
                    nextText: ' ',
                    prevText: ' ',
                    dateFormat: "dd MM yy",
                    onSelect: function (dateText, inst) {
                        var date = $(this).val();
                        source(date + ' ' + cTime);
                      
                        $(".options.dateTime div.head > span:not(.time)").text(date);
                    }
                });
            });

 
            

            $('.timepicker').click(function (e) {
                e.stopPropagation();
            });
        }
    };

    ko.bindingHandlers.itemSelector = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            var selectedObservableArrayInViewModel = valueAccessor();
            var isSelected = selectedObservableArrayInViewModel.isSelected;
            var item = selectedObservableArrayInViewModel.item;
            var selectedItems = selectedObservableArrayInViewModel.selectedItems;

            if (isSelected()) {
                selectedItems.push(item);
            }

            else {
                selectedItems.remove(item);
            }
            $(element).click(function (e) {
                isSelected(!isSelected())
                if (isSelected()) {
                    selectedItems.push(item);
                }
                else {
                    selectedItems.remove(item);
                }
                e.stopPropagation();
            });
        }
    };

    ko.bindingHandlers.dropdown = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            $('div.head', element).click(function (e) {
                $(this).parent().addClass('active');
                $(".optionsList").mCustomScrollbar('destroy');
                $(".optionsList:not(.mCustomScrollbar)").mCustomScrollbar({
                    autoDraggerLength: true,
                    autoHideScrollbar: true,
                    scrollInertia: 100,
                    advanced: {
                        updateOnBrowserResize: true,
                        updateOnContentResize: true,
                        autoScrollOnFocus: false
                    }
                });
                $(this).closest('.options').siblings('.options').children('.optionsList').slideUp();
                $(this).closest('.options').children('.optionsList').slideToggle();
                $(this).toggleClass('active');
                e.stopPropagation();
            });
            $('body').click(function (e) {
                $('div.head', element).closest('.options').children('.optionsList').slideUp();
                //e.stopPropagation();
            });
        }
    };

    ko.bindingHandlers.dropDown = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            var selectedObservableArrayInViewModel = valueAccessor();
            var selector = selectedObservableArrayInViewModel.selector;
            $(element).click(function (e) {
                $(this).parent().children(selector).slideToggle();
                e.stopPropagation();
            });

            $('body').click(function () {
                $(element).parent().children(selector).slideUp();
            });
        }
    };

    ko.bindingHandlers.customHover = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            var selectedObservableArrayInViewModel = valueAccessor();
            var selector = selectedObservableArrayInViewModel.selector;

            $(element).mouseover(function (e) {
                if (selector == '.tooltip') {
                    $(element).children(selector).fadeIn();
                    e.stopPropagation();
                }
                else {
                    $(this).parent().children(selector).fadeIn();
                    e.stopPropagation();
                }
            });

            $('body').click(function () {
                $(selector).fadeOut();
            });

            $(selector).click(function (e) {
                e.stopPropagation();
            });
        }
    };

    ko.bindingHandlers.addTag = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            var selectedObservableArrayInViewModel = valueAccessor();
            var tags = selectedObservableArrayInViewModel.tags;

            $(element).keyup(function (event) {
                if (event.keyCode === 13) {
                    var value = $(this).val() + '  ';
                    $(this).val('');
                    if ($.trim(value) != '' && tags.indexOf(value) < 0) {
                        tags.push(value);
                        $('.ui-autocomplete').attr('style', 'display:none');
                        $(element).removeClass('not_valid');
                    }
                    else {
                        $(element).addClass('not_valid');
                    }
                }
            });
        }
    };

    ko.bindingHandlers.removeTag = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            var selectedObservableArrayInViewModel = valueAccessor();
            var tags = selectedObservableArrayInViewModel.tags;
            var tag = selectedObservableArrayInViewModel.tag;

            $(element).click(function (event) {
                tags.remove(tag);
            });
        }
    };

    ko.bindingHandlers.removeAllTag = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            var selectedObservableArrayInViewModel = valueAccessor();
            var tags = selectedObservableArrayInViewModel.tags;

            $(element).click(function (event) {
                tags([]);
            });
        }
    };

    ko.bindingHandlers.newsDrag = {
        init: function (element, valueAccessor) {
            var options = valueAccessor();

            $(element).draggable({
                start: function (e) {
                    $('.latestCol').stop(true, true).animate({ width: '190px' }, 200, 'linear');
                },
                stop: function () {
                    $('.latestCol').stop(true, true).animate({ width: '60px' }, 200, 'linear');
                },
                drag: function (e, ui) {
                    if (ui.position.left > 700) {
                        if (options.callback && options.news.isUsed() == false) {
                            options.callback(options.news);
                           // $(e.target).hide('medium', function () { $(this).remove(); });
                        }
                    }
                },
                revert: "invalid",
                scroll: true,
                //cursorAt: { top: 121, right: 533 }
            });
        },
        update: function (element, valueAccessor) {
            var options = valueAccessor();

            if (options.canAddNews())
                $(element).draggable("enable");
            else
                $(element).draggable("disable");
        }
    };

    ko.bindingHandlers.screenElementDrag = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            var options = valueAccessor();

            $(element).draggable({
                zIndex: 999,
                start: function (e) {
                    options.isDragging(true);
                    var elementId = $(e.currentTarget).data('elementid');
                    if (elementId)
                        options.selectedElementId(elementId);
                },
                stop: function (e, ui) {
                    options.isDragging(false);
                },
                helper: function (e) {
                    var dragIcon = $(e.currentTarget).data('dragicon')
                    return '<img class="screen-element-dragicon" src="' + dragIcon + '">';
                }
            });
        }
    };

    ko.bindingHandlers.screenElementDrop = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            var options = valueAccessor();

            $(element).droppable({
                drop: function (e, ui) {
                    var elementId = $(ui.draggable[0]).data('elementid');
                    var templateId = $(e.target).data('templateid');
                    var templateIndex = $(e.target).data('index');

                    options.dropTemplate(elementId, templateId, templateIndex);
                },
                addClasses: options.addClasses,
                hoverClass: 'ui-state-highlight screentemplate-hover',
                tolerance: "pointer"
            });
        }
    };

    ko.bindingHandlers.contentDrag = {
        init: function (element, valueAccessor) {
            var options = valueAccessor();

            if (options && options.enabled) {
                $(element).draggable({
                    start: function (e) {
                    },
                    stop: function () {
                    },
                    drag: function (e, ui) {
                        if (ui.offset.left > 700) {
                            if (options.callback)
                                options.callback(options.content);
                        }
                    },
                    revert: "invalid",
                    scroll: true
                });
            }
        },
        update: function (element, valueAccessor) {
        }
    };

    ko.bindingHandlers.elementPosition = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            var options = valueAccessor();

            $(element).click(function () {
                var context = ko.contextFor(this);
                if (options && context) {
                    options.top($(this).parent().position().top);
                    options.left($(this).parent().position().left);
                    options.callback(context.$data, context.$index());
                }
            });
        }
    };

    ko.bindingHandlers.newsBucketHover = {
        init: function (element, valueAccessor) {
            $(element).hover(
            function () {
                $(this).stop(true, true).animate({ width: '190px' }, 200, 'linear');
            },
            function () {
                $(this).stop(true, true).animate({ width: '75px' }, 200, 'linear');
            });
        }
    };

    ko.bindingHandlers.autoComplete = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            var selectedObservableArrayInViewModel = valueAccessor();
            var source = selectedObservableArrayInViewModel.source;
            var selected = selectedObservableArrayInViewModel.selected;
            var tags = selectedObservableArrayInViewModel.tags;

            var addTag = function (item) {
                if (tags) {
                    if ($.trim(item.value) != '' && tags.indexOf(item) < 0) {
                        tags.push(item);
                        $(element).val('');

                        $(element).removeClass('not_valid');
                    }
                    else {
                        $(element).addClass('not_valid');
                    }
                }
            };

            var select = function (item) {
                if (selected) {
                    selected(item);
                }
            };

            $(element).autocomplete({
                minLength: 1,
                autoFocus: false,
                select: function (event, ui) {
                    select(ui.item);
                    // addTag(ui.item);
                },
                change: function (event, ui) {
                    select(ui.item);
                    //addTag(ui.item);
                },
                source: function (request, response) {
                    var term = $.trim(request.term);
                    if (term != '' && term.length > 3) {
                        jQuery.get(source + term, {
                            query: request.term
                        }, function (data) {
                            var result = [];
                            var suggestion = data.Data;

                            if (suggestion.length > 0) {
                                if (suggestion[0].hasOwnProperty('Location')) {
                                    for (var i = 0; i < data.Data.length; i++) {
                                        result.push({ value: data.Data[i].Location, id: data.Data[i].LocationId, name: data.Data[i].Location });
                                    }
                                }
                                else if (suggestion[0].hasOwnProperty('Category')) {
                                    for (var i = 0; i < data.Data.length; i++) {
                                        result.push({ value: data.Data[i].Category, id: data.Data[i].CategoryId, name: data.Data[i].Category });
                                    }
                                }
                                else if (suggestion[0].hasOwnProperty('Tag')) {
                                    for (var i = 0; i < data.Data.length; i++) {
                                        result.push({ value: data.Data[i].Tag, id: data.Data[i].TagId, name: data.Data[i].Tag });
                                    }
                                }
                                response(result);
                            }
                        })
                    }
                }
            }).data('ui-autocomplete')._renderItem = function (ul, item) {
                return $("<li></li>")
                    .append($("<a>").text(item.value))
                    .appendTo(ul);
            };
        }
    };

    ko.bindingHandlers.sortableList = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            var options = valueAccessor();

            if (options && options.stories) {
                $(element).sortable({
                    start: function (event, ui) {
                        var dustBin = $('.dustbin');
                        dustBin.stop(true, true).fadeIn();
                        dustBin.css({
                            'top': ui.originalPosition.top + 20
                        });
                    },
                    stop: function (event, ui) {
                        $('.dustbin').hide();
                    },
                    update: function (event, ui) {
                        var arrayPositions = $(this).sortable('toArray');
                        options.callback(arrayPositions);
                    },
                    remove: function (event, ui) {
                        var index = $(ui.item).attr('id');
                        options.removeStory(index);
                        $('#trash').children().remove();
                    },
                    revert: false,
                    connectWith: "#trash"
                });
            }
        },
        update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        },
    };

    ko.bindingHandlers.jqCycle = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {

        },
        update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            var options = valueAccessor();

            if (options.canApplyCycle()) {
                $(element).cycle({
                    fx: "carousel",
                    carouselVisible: 4,
                    pauseOnHover: true,
                    speed: 700,
                    timeout: 0,
                    slides: "li",
                    prev: $(element).siblings('.control-prev')[0],
                    next: $(element).siblings('.control-next')[0],
                    log: false,
                    allowWrap: false,
                    swipe: true
                });
            }
        }
    };

    ko.bindingHandlers.languageChange = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            var selectedObservableArrayInViewModel = valueAccessor();
            var selector = selectedObservableArrayInViewModel.selector();
            var selectedLanguage = selectedObservableArrayInViewModel.selectedLanguage;
            var _element = selectedObservableArrayInViewModel.element;
            _element(element);
            var tempObject = {};
            var urduEditors = [];

            $(element).click();
            $(element).click(function () {
                var language = $(this).data('lang');

                if (language == "ur") {
                    selectedLanguage('UR');


                    for (var i = 0; i < selector.length; i++) {
                        if (urduEditors.indexOf(selector[i]) < 0) {
                            var elements = $(selector[i]).not($('.uploadVideo .editingSection .aside .notAdd'));

                            $(elements).UrduEditor();

                            if (elements.length > 0) {
                                urduEditors.push(selector[i]);
                            }
                        } else {
                            $(selector[i]).not($('.uploadVideo .editingSection .aside .notAdd')).attr("UrduEditorId", "UrduEditor");
                            $(selector[i]).not($('.uploadVideo .editingSection .aside .notAdd')).attr("dir", "rtl");
                            $(selector[i]).not($('.uploadVideo .editingSection .aside .notAdd')).attr("style", "font-family: 'Jameel Noori Nastaleeq', 'Nafees nastaleeq', 'Nafees Web Naskh', Helvetica, sans-serif; background-color: rgb(245, 245, 245);");
                            $(selector[i]).not($('.uploadVideo .editingSection .aside .notAdd')).attr("lang", "ur");
                        }

                        $(selector[i]).css({ 'background-color': '#f5f5f5' });
                        if (selector[i] == '.updateReportnews .editingSection .editor input[type=text]' || '.uploadVideo .editingSection .aside input[type=text]' || '.reportNews .editingSection .editor input[type=text]' || '.reportNewsChannelReporter .editingSection .editor input[type=text]') {
                            $(selector[i]).not($('.uploadVideo .editingSection .aside .notAdd')).not($('.uploadVideo .editingSection .aside .notAdd')).attr("placeholder", "عنوان");
                            if (selector[i] == '.uploadVideo .editingSection .aside input[type=text],textarea') {
                                $('.uploadVideo .editingSection .aside textarea').attr('placeholder', 'تفصیل ')
                            }

                            if (selector[i] == '.reportNews .editingSection .editor .jqte_editor') {

                                var index = $('.reportNews .editingSection .editor .jqte_editor')[0].innerHTML;

                                if (index == '<p class="placeholderclass"> Enter Description</p>') {
                                    $('.reportNews .editingSection .editor .jqte_editor').html('');
                                }
                            }


                        }
                    }
                }

                if (language == "en") {
                    selectedLanguage('EN');
                    for (var i = 0; i < selector.length; i++) {
                        $(selector[i]).not($('.uploadVideo .editingSection .aside .notAdd')).removeAttr('urdueditorid dir lang style');
                        if (selector[i] == '.updateReportnews .editingSection .editor input[type=text]' || '.uploadVideo .editingSection .aside input[type=text]' || '.reportNews .editingSection .editor input[type=text]' || '.reportNewsChannelReporter .editingSection .editor input[type=text]') {
                            $(selector[i]).not($('.uploadVideo .editingSection .aside .notAdd')).attr("placeholder", "Enter Title");
                            if (selector[i] == '.uploadVideo .editingSection .aside input[type=text],textarea') {
                                $('.uploadVideo .editingSection .aside textarea').attr('placeholder', 'Enter Comments ')
                            }
                        }
                    }
                }
            });
        },
    };

    ko.bindingHandlers.channelDropdown = {
        update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            var value = valueAccessor();
            var val = ko.unwrap(value);

            if (val == true) {
                $(element).each(function () {
                    var
                        $this = $(this),
                        val = $this.children('option:first-child').text();

                    if (!$this.parent().hasClass('xSelect')) {
                        $this.wrap('<div class="xSelect" style="position:relative;" />');
                        $this.parent().append('<p>' + val + '</p>');
                        $this.css({ 'position': 'absolute', 'top': '0px', 'left': '0px', 'width': '100%', 'height': '100%', 'z-index': '10', 'opacity': 0 });
                        $this.on('change', function () {
                            val = $('#' + this.id + ' :selected').text();
                            $(this).parent().children('p').text(val);
                        });
                    }
                });
            }
        }
    };

    ko.bindingHandlers.programDropdown = {
        update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            var value = valueAccessor();
            var val = ko.unwrap(value);

            if (val == true) {
                var thist = 0;
                $(element).hover(function () {
                    if (thist == 1) {
                        $(this).stop(true, true).animate({
                            width: '297px'
                        }, 400, 'linear');
                    }
                    thist = 1;
                }, function () {
                    $(this).children('li').find('ul').stop(0, 0).slideUp('fast');
                    $(this).children('li').find('span#datepickerhome').stop(0, 0).slideUp('fast');
                    $(this).stop(true, true).animate({ width: '75px' }, 400, 'linear');
                });
            };
        }
    }

    ko.bindingHandlers.dropdownSlide = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            $('html').click(function () {
                $(element).children('ul').hide();
            });
        },
        update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            var value = valueAccessor();
            var val = ko.unwrap(value);

            if (val == true) {
                $(element).click(function (event) {
                    event.stopPropagation();
                    $(this).siblings().children('ul').hide();
                    $(this).children('ul').show();
                });
            };
        }
    };

    ko.bindingHandlers.tabActive = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        },
        update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            var value = valueAccessor();
            var val = ko.unwrap(value);

            if (val == true) {
                $(element).children().click(function () {
                    $(element).children().removeClass("active");
                    $(this).addClass("active");
                });
            };
        }
    };

    ko.bindingHandlers.tabClose = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            $(element).hide();
        },
        update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            var value = valueAccessor();
            var val = ko.unwrap(value);

            if (val == true) {
                $(".close-Icon").click(function () {
                    $(this).parent('.flowChart').hide();
                    $(".tabs li").removeClass("active");
                });
            };
        }
    };

    ko.bindingHandlers.togglePopup = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        },
        update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            var value = valueAccessor();
            var val = ko.unwrap(value);

            if (val == true) {
                $(element).show();
            } else {
                $(element).hide();
            }
        }
    }

    ko.bindingHandlers.displayTab = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        },
        update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            var value = valueAccessor();
            var val = ko.unwrap(value);
            if (val) {
                $(element).children().css("display", "none");
                $("#" + val).fadeToggle();
            };
        }
    };

    ko.bindingHandlers.activate = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            var value = valueAccessor();
            var cssClass = ko.unwrap(value);

            if (cssClass) {
                $(element).click(function () {
                    $(this).toggleClass(cssClass);
                });
            };
        }
    };

    ko.bindingHandlers.customScrollBar = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            var options = valueAccessor();
            var onTotalScrollOffset = 0;
            if (options && options.onTotalScrollOffset) {
                onTotalScrollOffset = options.onTotalScrollOffset;
            }

            if (options && options.fixResolutionMyNews) {
                $(element).css('max-height', ($(window).height() - 180) + 'px');
            }

            $(element).mCustomScrollbar({
                autoDraggerLength: true,
                autoHideScrollbar: true,
                scrollInertia: 100,
                advanced: {
                    updateOnBrowserResize: true,
                    autoScrollOnFocus: false,
                    updateOnContentResize: true
                },
                callbacks: {
                    onTotalScroll: function (element) {
                        if (options && options.callback)
                            options.callback();
                    },
                    onTotalScrollOffset: onTotalScrollOffset
                }
            });
        }
    };

    ko.bindingHandlers.adjustHeight = {
        init: function (element, valueAccessor) {
            var height = $(window).height() - $('header.header').outerHeight() - $('.singlebar-menu').outerHeight() - $('.footer').outerHeight() - 40;
            $(element).height(height);

            $(window).resize(function () {
                var height = $(window).height() - $('header.header').outerHeight() - $('.singlebar-menu').outerHeight() - $('.footer').outerHeight() - 40;
                $(element).height(height);
            });
        }
    };

    ko.bindingHandlers.selectedNews = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        },
        update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            var value = valueAccessor();
            var val = ko.unwrap(value);
            if (val == true)
                $(element).addClass('active');
            else
                $(element).removeClass('active');
        }
    };

    ko.bindingHandlers.comments = {
        update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            // Keyboard Language Selection
            $(element).delegate('i', 'click', function (e) {
                $(this).closest(element).children('.dropdown').slideToggle();
                e.stopPropagation();
            });
            $(element).delegate('.dropdown', 'click', function (e) {
                e.stopPropagation();
            });
        },
    };

    ko.bindingHandlers.clearContent = {
        update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            var clear = valueAccessor().clear;
            if (clear()) {
                $(".jqte_editor").empty(' ');
                clear(false);
            }
        },
    };

    ko.bindingHandlers.hoverStyle = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            var value = valueAccessor();
            var val = ko.unwrap(value);

            if (val) {
                $(element).hover(function () {
                    $(this).toggleClass(val);
                });
            };
        }
    };

    ko.bindingHandlers.buttonHover = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            var value = valueAccessor();
            var val = ko.unwrap(value);

            if (val) {
                
                $(element).hover(function () {
                    $(this).stop(true, true).animate({ width: '275px' }, 400, 'linear');
                    $(this).find('img').css('z-index','-1');
                }, function () {
                    $(this).stop(true, true).animate({ width: '30px' }, 400, 'linear');
                    $(this).find('img').css('z-index', '99');
                });
            };
        }
    };

    ko.bindingHandlers.lastCharacters = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel) {
            var options = valueAccessor();

            if (options) {
                var lastChars = ko.computed(function () {
                    var text = options.text;
                    var length = options.length || 5;
                    var result;
                    if (text.length > 0)
                        result = text.substring(text.length - length, text.length);
                    else
                        result = '';
                    return result;
                });
                ko.applyBindingsToNode(element, {
                    text: lastChars
                }, viewModel);

                return {
                    controlsDescendantBindings: true
                };
            }
        }
    };

    ko.bindingHandlers.trimText = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel) {
            var options = valueAccessor();

            if (options) {
                var trimmedText = ko.computed(function () {
                    var untrimmedText = options.text;
                    var maxLength = options.length || 25;
                    var minLength = 5;
                    if (maxLength < minLength) maxLength = minLength;
                    var text = untrimmedText.length > maxLength ? untrimmedText.substring(0, maxLength - 1) + '...' : untrimmedText;
                    return text;
                });
                ko.applyBindingsToNode(element, {
                    text: trimmedText
                }, viewModel);

                return {
                    controlsDescendantBindings: true
                };
            }
        }
    };

    ko.bindingHandlers.newscropper = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            var selectedObservableArrayInViewModel = valueAccessor();
            var coordinates = selectedObservableArrayInViewModel.coordinates;
            var markVideoProcessed = selectedObservableArrayInViewModel.markVideoProcessed;
            var jcrop_api, boundx, boundy;
            var flag = true;

            function updatePreview(c) {
                if (parseInt(c.w) > 0) {
                    var rx = 220 / c.w, ry = 220 / c.h;
                }
            }
            function bindCoordinates(c) {
                if (flag) {
                    markVideoProcessed.call();
                    flag = false;
                }

                var croppedpoints = { Bottom: c.y2, Top: c.y, Right: c.x2, Left: c.x, width: c.w, height: c.h, ResourceTypeId: '', FileName: '' };
                coordinates(croppedpoints);
            }
            $(element).Jcrop({
                onChange: updatePreview,
                onSelect: bindCoordinates,
                bgFade: true,
                bgOpacity: .50,
            },
           function () {
               jcrop_api = this;
           });
        }
    };

    ko.bindingHandlers.newspapercropping = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            var coordinates = valueAccessor().coordinates;
        },
        update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            var Coordinates = valueAccessor().coordinates;

            function ShowCoordinates(c) {
                var croppedpoints = { Bottom: c.y2, Top: c.y, Right: c.x2, Left: c.x };
                coordinates(croppedpoints);
            }

            if (Coordinates().length > 0) {
                $(element).Jcrop({
                    bgColor: 'black',
                    bgOpacity: .5,
                    onSelect: ShowCoordinates,
                    setSelect: [Coordinates()[0].left, Coordinates()[0].top, Coordinates()[0].right, Coordinates()[0].bottom]
                });
            } else { }
        }
    };

    ko.bindingHandlers.rightSectionNews = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            var rightsection = valueAccessor();
            var rightsectionelement = element;
            var totalheight = $('.rightSection').height();
            var correctedheight = totalheight - 335;
            if (rightsection == true) {
                $(rightsectionelement).height(correctedheight);
            }

            else { }
        }
    };

    ko.bindingHandlers.imagecropped = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            var selectedObservableArrayInViewModel = valueAccessor();
            var coordinates = selectedObservableArrayInViewModel.coordinates;
            var canvasImage = selectedObservableArrayInViewModel.canvasImage;
            var img = canvasImage();
            var context = element.getContext("2d");
            $img = $(img),
            imgW = 906,
            imgH = 679;

            var ratioY = imgH / 679,
                ratioX = imgW / 906;

            var getX = coordinates().Left * ratioX,
                getY = coordinates().Bottom * ratioY,
                getWidth = coordinates().width * ratioX,
                getHeight = coordinates().height * (ratioY);

            context.drawImage(img, getX, getY, getWidth, getHeight, 0, 0, 50, 42);
        }
    };

    ko.bindingHandlers.calendarControl = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            var options = valueAccessor();

            var to = new Date();
            var from = new Date(to.getTime() - 1000 * 60 * 60 * 24 * 14);

            options.toDate.subscribe(function (value) {
                $(element).DatePickerSetDate([options.fromDate(), options.toDate()]);
            });

            options.fromDate.subscribe(function (value) {
                $(element).DatePickerSetDate([options.fromDate(), options.toDate()]);
            });

            $(element).DatePicker({
                inline: true,
                date: [options.fromDate(), options.toDate()],
                calendars: 3,
                mode: 'range',
                current: new Date(to.getFullYear(), to.getMonth() - 1, 1),
                onChange: function (dates, el) {
                    options.fromDate(dates[0]);
                    options.toDate(dates[1]);
                }
            });
        }
    };

    ko.bindingHandlers.customCalendar = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            var options = valueAccessor();
            var to = options.to;
            var from = options.from;
            var isSetCustom = options.isSetCustom;
            var clear = options.clear;
            var mode = options.mode;
            var dateObj = {};

            if (!mode) {
                mode = 'single';
                dateObj = from();
            }
            else {
                dateObj = [from(), to()];
            }

            to.subscribe(function (value) {
                if (mode == 'range') {
                    $(element).DatePickerSetDate([from(), to()]);
                }
            });

            from.subscribe(function (value) {
                if (mode == 'single') {
                    $(element).DatePickerSetDate(value);
                }
                else {
                    $(element).DatePickerSetDate([from(), to()]);
                }
            });

            $(element).DatePicker({
                inline: true,
                date: dateObj,
                calendars: 3,
                mode: mode,
                current: new Date(to().getFullYear(), to().getMonth() - 1, 1),
                onChange: function (dates, el) {
                    if (dates instanceof Array) {
                        to(dates[1]);
                        from(dates[0]);
                        clear();
                        isSetCustom(true);
                    }
                    else {
                        from(dates);
                        clear();
                    }
                }
            });
        }
    };

    ko.bindingHandlers.smallCalendar = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            var options = valueAccessor();
            var currentDate = options.currentDate;

            $(element).datepicker({
                onSelect: function (dateValue) {
                    options.currentDate(moment(dateValue).toISOString());
                    $(this).datepicker().hide();
                   

                }
            });
            if (options.bindToggle) {
                $(element).click(function () {
                    $(this).toggle();
                });
            }
        }
    };

    ko.bindingHandlers.activeElement = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            var options = valueAccessor();

            $(element).click(function () {
                if ($(this).hasClass(options)) {
                    $('.' + options).removeClass();
                }
                else {
                    $('.' + options).removeClass();
                    $(this).addClass(options);
                }
            });
        }
    };

    ko.bindingHandlers.numericBox = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            var options = valueAccessor();
            var min = valueAccessor().min;
            var max = valueAccessor().max;

            $(element).keyup(function (e) {
                var val = $(element).val();
                if (!isNaN(val) && (val >= min && val <= max)) {
                    return true;
                }
                else {
                    $(element).val(val.substring(0, val.length - 1));
                }
            });
        }
    };

    ko.bindingHandlers.videoEditor = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            var markVideoProcessed = valueAccessor().markVideoProcessed;
            var videoControlElement = valueAccessor().videoControlElement;
            var video = $(element)[0];
            videoControlElement(video);
            video.onended = function () {
                markVideoProcessed.call();
            };
        }
    };

    ko.bindingHandlers.videoEditorVolumeBar = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            var volumeBar = $(element)[0];
            var videoControlElement = valueAccessor().videoControlElement();
            volumeBar.addEventListener("change", function () {
                videoControlElement.volume = volumeBar.value;
            });
        }
    };

    ko.bindingHandlers.videoEditorBar = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            var progressBar = $('.progressbar', element)[0];
            var videoControlElement = valueAccessor().videoControlElement();
            var totalSelections = valueAccessor().totalSelections;
            var startBuffering = valueAccessor().startBuffering;
            var videoCurrentTime = valueAccessor().videoCurrentTime;
            var videoDuration = valueAccessor().videoDuration;
            var divInnerOffset = '';
            var start = ko.observable(0);
            var end = ko.observable(0);
            var TotalSelections = [];
            var factor = valueAccessor().factor;
            var fact;
            var durationPercent;

            videoControlElement.oncanplay = function () {
                factor(videoControlElement.duration / $(element).width());
                fact = factor();
                videoDuration(videoControlElement.duration);
                durationPercent = (10 / 100) * videoDuration();
                startBuffering(true);
            }

            $(element).click(function (e) {
                divInnerOffset = e.pageX - $(element).offset().left;
                videoControlElement.currentTime = divInnerOffset * fact;
                var ctrlPressed = 0;
                if (parseInt(navigator.appVersion) > 3) {
                    var evt = e ? e : window.event;
                    ctrlPressed = evt.ctrlKey;
                    if (ctrlPressed) {
                        if (totalSelections().length == 0) {
                            TotalSelections = [];
                        }
                        videoPoints();
                    }
                    else { }
                }
            });

            videoControlElement.addEventListener("timeupdate", function () {
                videoCurrentTime(videoControlElement.currentTime);
                var value = (100 / videoControlElement.duration) * videoControlElement.currentTime;
                progressBar.style.width = value + 3.5 + '%';
            });

            progressBar.addEventListener("mousedown", function () {
                if (videoControlElement.paused == true) {
                    videoControlElement.pause();
                } else { videoControlElement.play(); }
            });

            // functions to be called-------------------------------------------------------
            function clearOffsetValue() {
                divInnerOffset = ' ';
            }
            function clearValues() {
                start('');
                end('');
            }
            function videoPoints() {
                var noDuplicaiton = false;
                start(videoControlElement.currentTime);
                end(start() + durationPercent);

                if (totalSelections().length > 0 && TotalSelections.length == 0)
                { TotalSelections = totalSelections(); }

                if (TotalSelections.length > 0) {
                    var s = start();
                    var t = end();
                    for (var i = 0; i < TotalSelections.length; i++) {
                        if ((s + end()) < TotalSelections[i].From || s > (TotalSelections[i].From + (TotalSelections[i].To - TotalSelections[i].From))) {
                            noDuplicaiton = true;
                        } else {
                            return false;
                        }
                    }
                }
                if (noDuplicaiton || TotalSelections.length == 0) {
                    if ((start() + (end() - start())) > (videoControlElement.duration - 3)) {
                        end(start() + ((0.5 / 100) * videoControlElement.duration));
                    }
                    TotalSelections.push({ From: start(), To: end(), Left: start() / fact, width: (end() / fact) - (start() / fact) });
                    totalSelections(TotalSelections);
                    noDuplicaiton = false;
                }
                clearValues();
            }
        }
    };

    ko.bindingHandlers.updateRangeValue = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            var options = valueAccessor();
            $(element).click(function () {
            });
        },
        update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            var options = valueAccessor();
            var value = (options.timeUpdateDuration / options.totalPreviewTime) * 100;
            element.value = value > 0 ? value + 6 : 0;
        }
    }

    ko.bindingHandlers.textAnimate = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            var options = valueAccessor();
            var animationDuration = options.animationDuration;
            //element.style.webkitAnimationDuration = animationDuration + 's';
            //element.style.animationDuration = animationDuration + 's';
        },
        update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            var options = valueAccessor();
            var animationDuration = options.animationDuration;

            //var keyframes;

            //var allCssFile = document.styleSheets;

            //if (allCssFile.length > 0) {
            //    for (var i = 0; i < allCssFile.length; ++i) {
            //        // loop through all the rules
            //        if (allCssFile[i].cssRules) {
            //            for (var j = 0; j < allCssFile[i].cssRules.length; ++j) {
            //                // find the -webkit-keyframe rule whose name matches our passed over parameter and return that rule
            //                if (allCssFile[i].cssRules[j].type == window.CSSRule.WEBKIT_KEYFRAMES_RULE && allCssFile[i].cssRules[j].name == '@-webkit-keyframes')
            //                    return allCssFile[i].cssRules[j];
            //            }
            //        }
            //    }
            //}

            //element.style.webkitAnimationDuration = animationDuration + 's';
            //element.style.animationDuration = animationDuration + 's';
        }
    }

    ko.bindingHandlers.buffering = {
        update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            var videoControlElement = valueAccessor().videoControlElement();
            var startBuffering = valueAccessor().startBuffering;
            if (startBuffering()) {
                videoControlElement.addEventListener("progress", function () {
                    $(element)[0].style.width = ((videoControlElement.buffered.end(i) / videoControlElement.duration) - (videoControlElement.buffered.start(i) / videoControlElement.duration)) * 100 + '%';
                });
            }
        }
    };

    ko.bindingHandlers.resizable = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            var videoControlElement = valueAccessor().videoControlElement();
            var totalSelections = valueAccessor().totalSelections;
            var item = valueAccessor().item;
            var factor = valueAccessor().factor();
            var handle = ko.observable('');
            var parent = $(element).parent('.bar');
            var eleWidth = '';//$(element).width();
            var eleLeft = '';
            $(element).resizable({
                containment: "parent",
                handles: "e,w",
                start: function (event, ui) {
                    eleWidth = $(this).width();
                    eleLeft = $(this).offset().left;
                    var handleTarget = $(event.originalEvent.target);
                    if (handleTarget.hasClass('ui-resizable-e')) {
                        handle('To');
                    } else if (handleTarget.hasClass('ui-resizable-w')) {
                        handle('From');
                    } else { }
                },
                stop: function (event, ui) {
                    var divOffset = event.pageX - $(element).parent('.bar').offset().left;
                    var handlecurrentTime = videoControlElement.currentTime = divOffset * factor;
                    if (handle() == 'To') {
                        var index = totalSelections().indexOf(item);
                        if (totalSelections().length > 1) {
                            var prevVal = item.TO;
                            item.To = handlecurrentTime;
                            if (totalSelections()[index + 1]) {
                                if (handlecurrentTime >= totalSelections()[index + 1].From) {
                                    item.To = prevVal;
                                    $(this).offset({ left: eleLeft });
                                    $(this).width(eleWidth);
                                }
                            }
                        } else {
                            item.To = handlecurrentTime;
                        }
                    }
                    else if (handle() == 'From') {
                        var index = totalSelections().indexOf(item);
                        if (totalSelections().length > 1) {
                            var prevVal = item.From;
                            item.From = handlecurrentTime;
                            if (totalSelections()[index - 1]) {
                                if (handlecurrentTime <= totalSelections()[index - 1].To) {
                                    item.From = prevVal;
                                    $(this).offset({ left: eleLeft });
                                    $(this).width(eleWidth);
                                }
                            }
                        } else {
                            item.From = handlecurrentTime;
                        }
                    } else { }
                }
            });
        }
    };

    ko.bindingHandlers.linearHover = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            var value = valueAccessor();
            var width1 = value.width1;
            var width2 = value.width2;

            $(element).hover(function () {
                $(this).stop(true, true).animate({ width: width1 }, 400, 'linear');
            }, function () {
                $(this).stop(true, true).animate({ width: width2 }, 400, 'linear');
            });
        }
    };

    ko.bindingHandlers.toggleState = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            //$(element).click(function () {
            //    $(this).siblings().find('.nav-step1').toggleClass('active');
            //});

            $(element).click(function () {
                if (!valueAccessor()) {
                    if ($(this).parent().children().hasClass('active')) {
                        $(this).parent().children().removeClass('active');
                    }
                }
                $(this).toggleClass('active');
            });
        }
    };

    ko.bindingHandlers.showContentGallery = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            var resetvar2 = 0;
            $(element).click(function () {
                if (resetvar2 == 0) {
                    $(this).parents('.galleryView').addClass('active').stop(true, true).animate({ left: '0px' }, 200, 'linear');
                    resetvar2 = 1;
                } else {
                    $(this).parents('.galleryView').removeClass('active').stop(true, true).animate({ left: '-142px' }, 200, 'linear');
                    resetvar2 = 0;
                }
            });
        }
    };

    ko.bindingHandlers.myNewsLeftNavHover = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            $('li', element).on('mouseover', function () {
                $(this).find('ul').parent('li').addClass("active");
                $(this).find('ul').stop(true, true).animate({
                    left: '50px'
                }, 200, 'linear');
                $(this).on('mouseleave', function () {
                    $(this).find('ul').parent('li').removeClass("active");
                    $(this).find('ul').stop(true, true).animate({
                        left: '-340px'
                    }, 200, 'linear');
                });
            });
        }
    };

    ko.bindingHandlers.navigator = {
        init: function (element, valueAccessor, allBindingAccessor, viewModel, bindingContext) {
            $(window).keyup(function (evt) {
                if (event.keyCode == 37) {
                    viewModel.previous();
                }
                else if (event.keyCode == 39) {
                    viewModel.next();
                }
                else if (event.keyCode == 27) {
                    viewModel.contentViewerControl.isPopUpVisible(false);
                }
            });
        },
        update: function (element, valueAccessor, allBindingsAccessor, viewModel) {
            // $(element).show("slide", { direction: "right" }, 1000);
        }
    };

    ko.bindingHandlers.thumScrollPrev = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            var currentPos = 0;
            $(element).click(function () {
                var totalWidth = $(element).siblings('.cycle-slideshow')[0].scrollWidth;
                var offset = $(element).siblings('.cycle-slideshow').offset();
                var factorPrev = (offset.left / totalWidth) * 100;
                var factorNext = (totalWidth + offset.left);

                if (factorPrev < -2.6) {
                    var pos = $(element).parent('.customthumb').scrollLeft();
                    $(element).parent('.customthumb').animate({ scrollLeft: pos - 300 }, 600);
                }
                else {
                    $(element).hide();
                }
                if (factorNext <= 1300) {
                    $(element).siblings('a').show();
                }
            });
        }
    };

    ko.bindingHandlers.thumScrollNex = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            var currentPos = 0;
            $(element).click(function () {
                var totalWidth = $(element).siblings('.cycle-slideshow')[0].scrollWidth;
                var offset = $(element).siblings('.cycle-slideshow').offset();
                var factorPrev = (offset.left / totalWidth) * 100;
                var factorNext = (totalWidth + offset.left);

                if (factorNext > 1300) {
                    var pos = $(element).parent('.customthumb').scrollLeft();
                    $(element).parent('.customthumb').animate({ scrollLeft: pos + 300 }, 600);
                }
                else {
                    $(element).hide();
                }
                if (factorPrev >= 2.6) {
                    $(element).siblings('a').show();
                }
            });
        }
    };

    ko.bindingHandlers.scrollPrev = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            var currentPos = 0;
            $(element).click(function () {
                var totalWidth = $(element).siblings('.cycle-slideshow')[0].scrollWidth;
                var offset = $(element).siblings('.cycle-slideshow').offset();
                var factorPrev = (offset.left / totalWidth) * 100;
                var factorNext = (totalWidth + offset.left);

                if (factorPrev < 3) {
                    var pos = $(element).parent('.customthumb').scrollLeft();
                    $(element).parent('.customthumb').animate({ scrollLeft: pos - 220 }, 600);
                }
                else {
                    $(element).hide();
                }
                if (factorNext <= 970) {
                    $(element).siblings('a').show();
                }
            });
        }
    };

    ko.bindingHandlers.scrollNex = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            var currentPos = 0;
            $(element).click(function () {
                var totalWidth = $(element).siblings('.cycle-slideshow')[0].scrollWidth;
                var offset = $(element).siblings('.cycle-slideshow').offset();
                var factorPrev = (offset.left / totalWidth) * 100;
                var factorNext = (totalWidth + offset.left);

                if (factorNext > 970) {
                    var pos = $(element).parent('.customthumb').scrollLeft();
                    $(element).parent('.customthumb').animate({ scrollLeft: pos + 220 }, 600);
                }
                else {
                    $(element).hide();
                }
                if (factorPrev <= 9.6) {
                    $(element).siblings('a').show();
                }
            });
        }
    };

    ko.bindingHandlers.cycleArrows = {
        init: function (element, valueAccessor) {
            $(element).bind("DOMSubtreeModified", function (e, i) {
                var innerwidth = 0;
                var totalwidth = $(element)[0].scrollWidth;
                $('.cycle-slide', element).each(function () {
                    innerwidth += $(this).width();
                });

                if (innerwidth < totalwidth) {
                    $(element).siblings('.arrows').hide();
                }
                else {
                    $(element).siblings('.arrows').show();
                }
            });
        }
    };

    ko.bindingHandlers.textEditor = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            var options = valueAccessor();

            $(element).jqte({
                format: false,
                sub: false,
                sup: false,
                rule: false,
                outdent: false,
                indent: false,
                source: false,
                strike: false,
                linktypes: false,
                color: false,
                link: false,
                remove: false,
                unlink: false,
                change: function () {
                    if (options) {
                        if (options.valueField && options.isChangeAllowed()) {
                            options.valueField($(element).val());
                        }
                        if (!options.isChangeAllowed()) {
                            options.isChangeAllowed(true);
                        }
                    }
                }
            });
        },
        update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            var options = valueAccessor();

            if (options && options.currentTemplate && options.currentTemplate()) {
                //$(element).jqteVal(options.currentTemplate().script());
            }
        }
    };

    ko.bindingHandlers.stop = {
        update: function (element, valueAccessor) {
            var player = $(element)[0];
            var stopPlayer = valueAccessor().stopPlayer;
            if (player && player.play && stopPlayer()) {
                player.pause();
                stopPlayer(false);
            } else { }
        }
    };

    ko.bindingHandlers.cssOverlay = {
        init: function (element, valueAccessor) {
            var options = valueAccessor();
        },
        update: function (element, valueAccessor) {
        }
    };

    ko.bindingHandlers.keyBoardControl = {
        init: function (element, valueAccessor) {
            var flag = true;
            var scrolllength = 30;
            $(element).keyup(
            function (e) {
                var elefChild = $(element).siblings('.suggestions').children('.mCustomScrollBox').children('.mCSB_container').children('ul').children('li:first-child');
                var elelChild = $(element).siblings('.suggestions').children('.mCustomScrollBox').children('.mCSB_container').children('ul').children('li:last-child');
                var curr = $(element).siblings('.suggestions').children('.mCustomScrollBox').children('.mCSB_container').find('.currentItemKeyBoardControl');

                if (e.keyCode == 40) {
                    if (flag) {
                        elefChild.attr('class', 'display_box currentItemKeyBoardControl');
                        flag = false;
                    }

                    if (curr.length) {
                        scrolllength -= 41;
                        $(element).siblings('.suggestions').children('.mCustomScrollBox').children('.mCSB_container').attr('style', 'position:relative;Top:' + scrolllength + 'px');
                        $(curr).attr('class', 'display_box');
                        $(curr).next().attr('class', 'display_box currentItemKeyBoardControl');
                    } else {
                        elefChild.attr('class', 'display_box currentItemKeyBoardControl');
                    }
                }
                if (e.keyCode == 38) {
                    if (flag) {
                        elelChild.attr('class', 'display_box currentItemKeyBoardControl');
                        flag = false;
                    }
                    if (curr.length) {
                        scrolllength += 41;

                        if (scrolllength != 30 || scrolllength != 0) {
                            $(element).siblings('.suggestions').children('.mCustomScrollBox').children('.mCSB_container').attr('style', 'Top:' + scrolllength + 'px;position:relative');
                        }
                        $(curr).attr('class', 'display_box');
                        $(curr).prev().attr('class', 'display_box currentItemKeyBoardControl');
                    } else {
                        elelChild.attr('class', 'display_box currentItemKeyBoardControl');
                    }
                }
                if (e.keyCode == 13) {
                    curr.click();
                }
            });

            $(element).siblings('.suggestions').mouseenter(function () {
                var curr = $(element).siblings('.suggestions').children('.mCustomScrollBox').children('.mCSB_container').children('ul').children('li').removeClass('currentItemKeyBoardControl')
                flag = true
            });

            $('body', 'html').click(function (e) {
                $(element).siblings('.suggestions').removeClass('showForcely');
                e.stopPropagation();
            });

            $(element).click(function (e) {
                e.stopPropagation();
            });

            $(element).siblings('.suggestions').click(function (e) {
                e.stopPropagation();
            });
        },
    };

    ko.bindingHandlers.shellLeftArrowTrigger = {
        init: function (element, valueAccessor) {
            $(element).click(
          function () {
              if ($(this).hasClass('reportNewsChannelReporter')) {
                  $(element).click(
                  function () {
                      $(".reportNewsChannelReporter.left").stop(true, true).animate({
                          left: '0px'
                      }, 200, 'linear');
                      $(".reportNewsChannelReporter.left").show();
                  });
              } else if ($(this).hasClass('updateNewsLeftRight')) {
                  $(".updateNewsLeftRight.left").stop(true, true).animate({
                      left: '0px'
                  }, 200, 'linear');
                  $(".updateNewsLeftRight.left").show();
              }
          });
        }
    };

    ko.bindingHandlers.shellRightArrowTrigger = {
        init: function (element, valueAccessor) {
            $(element).click(
            function () {
                if ($(this).hasClass('reportNewsChannelReporter')) {
                    $(".reportNewsChannelReporter.right").stop(true, true).animate({
                        right: '0px'
                    }, 200, 'linear');
                    $(".reportNewsChannelReporter.right").show();
                } else if ($(this).hasClass('updateNewsLeftRight')) {
                    $(".updateNewsLeftRight.right").stop(true, true).animate({
                        right: '0px'
                    }, 200, 'linear');
                    $(".updateNewsLeftRight.right").show();
                }
            });
        }
    };

    ko.bindingHandlers.closePanel = {
        init: function (element, valueAccessor) {
            $(element).click(function () {
                $(element).parent('h2').parent().hide();
                $(element).parent('h2').parent().stop(true, true).animate({
                    right: '-253px'
                }, 200, 'linear');
            });
        }
    };

    ko.bindingHandlers.preview = {
        update: function (element, valueAccessor) {
            if (valueAccessor().totalTemplates()) {
                var autoforward = valueAccessor().autoforward;
                var templates = valueAccessor().totalTemplates();
                var allDurations = [];
                for (var i = 0; i < templates.length; i++) {
                    allDurations[i] = templates[i].contentDuration();
                }
                startPreview();
            }
            function startPreview() {
                var i = 0;
                setTimeout(function test() {
                    $(element).parents('.mainsection')
                                            .siblings('header')
                                            .children('.playlist')
                                            .children('.container')
                                            .children('.playlistIco')
                                            .children('ul')
                                            .children('li').eq(i)
                                            .children('a')
                                            .click();
                    if (i <= allDurations.length - 1) {
                        setTimeout(test, 2000); //*allDurations[i+1]);
                        i++;
                    } else {
                        clearInterval(test);
                        autoforward();
                    }
                }, 2000);//*allDurations[0]);
            }
        }
    };

    ko.bindingHandlers.OpenFileUpload = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, context) {
            var handler = ko.utils.unwrapObservable(valueAccessor()),
                newValueAccessor = function () {
                    return function (data, event) {
                        $('body').find('.editorUploader').click();
                    };
                };

            ko.bindingHandlers.click.init(element, newValueAccessor, allBindingsAccessor, viewModel, context);
        }
    };


    ko.bindingHandlers.checkBox = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, context) {

        },
        update: function (element, valueAccessor, allBindingsAccessor, viewModel, context) {
            var options = valueAccessor();
            if (options.apply) {
                var checkBox = $(element);
                if (!$(element).parent().hasClass('xCheckbox')) {
                    $(element).fadeTo(0, 0).wrap('<div class="xCheckbox" style="position:relative;" />');
                    $(element).css({ 'position': 'absolute', 'width': '100%', 'height': '100%', 'left': '0', 'top': '0', 'z-index': '10' });
                    if ($(checkBox).is(':checked')) { $(checkBox).parent().addClass('checked'); }
                    $(checkBox).on('change', function () {
                        if ($(this).is(':checked')) {
                            $(this).parent().addClass('checked');
                        } else {
                            $(this).parent().removeClass('checked');
                        }
                    });
                }
            }
        }
    };

    ko.bindingHandlers.timelineBar = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, context) {

        },
        update: function (element, valueAccessor, allBindingsAccessor, viewModel) {

            var showtimelinebar = ko.observable();
            showtimelinebar(valueAccessor().showdata());
            if (showtimelinebar != '') {
                var totalwidth = $(element).width();
                var totalchilds = ($(element).find('li').length) - 1;
                var diff = totalwidth / totalchilds;

                var marginleft = 0;
                $(element).find('li').each(function (index, element) {

                    var $this = $(this);
                    $this.width($this.data('width'));



                    if ($this.find('span').length == 2) {
                        if (index == 1) {

                            marginleft = marginleft + diff + 100;
                            $this.css('left', marginleft + 'px');
                            $this.find('span').first().css('left', '-12px');
                            $this.find('span').eq(1).css('left', '2px');

                        }
                        else {
                            marginleft = marginleft + diff;
                            $this.css('left', marginleft + 'px');
                            $this.find('span').first().css('left', '-12px');
                            $this.find('span').eq(1).css('left', '2px');
                        }
                    }

                    if ($this.find('span').length == 1) {
                        if (index == 1) {

                            marginleft = marginleft + diff + 100;
                            $this.css('left', marginleft + 'px');
                            $this.find('span').first().css('left', '-12px');
                            $this.find('span').eq(1).css('left', '2px');

                        }
                        else {
                            marginleft = marginleft + diff;
                            $this.css('left', marginleft + 'px');
                            $this.find('span').first().css('left', '-12px');
                            $this.find('span').eq(1).css('left', '2px');
                        }
                    }


                    if ($this.find('span').length == 4) {
                        if (index == 1) {

                            marginleft = marginleft + diff + 100;
                            $this.css('left', marginleft + 'px');
                            $this.find('span').first().css('left', '-16px');
                            $this.find('span').eq(1).css('left', '4px');
                            $this.find('span').eq(2).css('left', '22px');
                            $this.find('span').eq(3).css('left', '40px');

                        }
                        else {
                            marginleft = marginleft + diff;
                            $this.css('left', marginleft + 'px');
                            $this.find('span').first().css('left', '-16px');
                            $this.find('span').eq(1).css('left', '4px');
                            $this.find('span').eq(2).css('left', '22px');
                            $this.find('span').eq(3).css('left', '40px');
                        }
                    }
                });
            }
           
        }
    };


    ko.bindingHandlers.tickeranimation = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, context) {

            var count = 0;
            var countforwardBackward = 0;
            var speedslow = 20;
            var speedfast = 4;

            $(element).click(
       function () {
           if ($(element)[0].attributes[0].nodeValue == 'playpause') {
               if ((count % 2) == 0) {
                   document.getElementById('myMarquee').stop();
                   $('#myMarquee').stop();
                   count = count + 1;
               }
               else {

                   document.getElementById('myMarquee').start();
                   count = count + 1;
               }
           }

           if ($(element)[0].attributes[0].nodeValue == 'forwardbackword') {
               if ((countforwardBackward % 2) == 0) {

                   document.all.myMarquee.direction = "down";
                   countforwardBackward = countforwardBackward + 1;

               }
               else {
                   document.all.myMarquee.direction = "up";
                   countforwardBackward = countforwardBackward + 1;
               }
           }

           if ($(element)[0].attributes[0].nodeValue == 'slow') {



               if (speedslow < 0) {

               }
               else {
                   speedslow = document.getElementById('myMarquee').getAttribute('scrollamount') - 4;
                   document.getElementById('myMarquee').setAttribute('scrollamount', speedslow, 0);
               }
           }

           if ($(element)[0].attributes[0].nodeValue == 'fast') {

               if (speedfast > 30) {

               }
               else {
                   speedfast = document.getElementById('myMarquee').getAttribute('scrollamount') + 4;
                   document.getElementById('myMarquee').setAttribute('scrollamount', speedfast, 0);
               }
           }


       });

        }
    };


    ko.bindingHandlers.showPanel = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, context) {
          
            $(element).click(function () {
               $(this).siblings().find('.nav-step1').toggleClass('active', 500, 'linear');
            });
        }
    };
    ko.bindingHandlers.expandCollapse = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, context) {
            $(element).click(function () {
                    growingDiv=$(this).next('div');
                    if (growingDiv.height() > 0) {
                        growingDiv.css("height", 0);
                    } else {
                        var wrapper = $(this).next('div').children('div').height();
                        growingDiv.css("height", wrapper);
                }
            });
        }
    };

    ko.bindingHandlers.rigthMenu = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, context) {
            
            var wd = $('.settingsmenu').outerWidth();
            $(element).click(function (el) {
                if ($(this).hasClass('active')) {
                    if ($(this).attr('title') == 'alerts') {
                        $(".stngstmenu").css('display', 'none');
                        $(".alertmenu").css('display', 'block');
                    } else {
                        $(".alertmenu").css('display', 'none');
                        $(".stngstmenu").css('display', 'block');
                    }
                } else {
                    if ($(this).attr('title') == 'settings' || $(this).attr('title') == 'alerts') {
                        var wd = $('.settingsmenu').outerWidth();
                        tgt = $(this).attr('href');
                        if ($(this).attr('title')=='settings') {
                            $(".stngstmenu").css('display', 'block');
                        } else {
                            $(".alertmenu").css('display', 'block');
                        }
                        $('.btn-alerts,.btn-settings').addClass('active');
                        $('.settingsmenu').stop(true, true).animate({ right: 0 }, 1000, eas);
                        $('.container').stop(true, true).css({ 'left': 0 }).animate({ left: -wd + 17 }, 1000, eas);
                        $('.headerMenu').stop(true, true).css({ 'margin-right': 290 }).animate({ left: -wd + 17 }, 1000, eas);
                        $('.headerMenu, .container').stop(true, true).css({ 'left': 0 }).animate({ left: -wd }, 1000, eas, function () { $('.settingsmenu ' + tgt).fadeIn(); });
                    }
                }

            });

            //function closemenu() {
            //    $('.btn-alerts,.btn-settings').removeClass('active');
            //    $('.settingsmenu').animate({ right: -wd }, 1000, eas, function () { $(this).removeAttr('style'); });
            //    $('.container').stop(true, true).css({ 'left': -wd }).animate({ left: 0 }, 1000, eas);
            //    $('.headerMenu').stop(true, true).css({ 'margin-right': 0 }).animate({ left: -wd + 17 }, 1000, eas);
            //    $('.headerMenu, .container').stop(true, true).css({ 'left': -wd }).animate({ left: 0 }, 1000, eas, function () { $(this).not('.container').removeAttr('style');});
            //}
        }
    };
    ko.bindingHandlers.contextMenu = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, context) {
            $.contextMenu({
                selector: '.hasmenu',
                callback: function (key, options) {
                    var sId = $(this).attr("sId");
                    var roId = $(this).attr("roId");
                    context.$root.cMenuAction(key, sId, roId);
                },
                items: {
                    "play": { name: "play", icon: "edit" },
                    "skip": { name: "skip", icon: "edit" },
                    "unSkip": { name: "unSkip", icon: "edit" },
                    "delete": { name: "delete", icon: "delete" },
                    "sep1": "---------",
                    "quit": { name: "Quit", icon: "quit" }
                }
            });

            $(element).on('click', function (data) {
                //alert(data);
            })
        }
    };
    ko.bindingHandlers.closeRightPanel = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, context) {
            var eas = 'easeOutExpo',
            loader = '<img src="/Content/images/coordinator/loader.gif" id="temploader" alt="" width="25">',
            anmdur = 1000;
            $(element).click(function () {
                var wd = $('.settingsmenu').outerWidth();
                $('.btn-alerts,.btn-settings').removeClass('active');
                $(".stngstmenu , .alertmenu").css("display", "none")
                $('.settingsmenu').animate({ right: -wd }, 1000, eas, function () { $(this).removeAttr('style'); });
                $('.container').stop(true, true).css({ 'left': -wd }).animate({ left: 0 }, 1000, eas);
                $('.headerMenu').stop(true, true).css({ 'margin-right': 0 }).animate({ left: -wd + 17 }, 1000, eas);
                $('.headerMenu, .container').stop(true, true).css({ 'left': -wd }).animate({ left: 0 }, 1000, eas, function () { $(this).not('.container').removeAttr('style'); });
            });
        }
    };

});






