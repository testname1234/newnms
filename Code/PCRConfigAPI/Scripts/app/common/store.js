﻿define('store',
    ['jquery', 'amplify', 'config'],
    function ($, amplify, config) {
        var
            expires = { expires: config.storeExpirationMs },

            clear = function (key) {
                return amplify.store(key, null);
            },

            fetch = function (key, storageType) {
                storageType = storageType || config.storageType.localStorage;

                var value;

                switch (storageType) {
                    case config.storageType.localStorage:
                        value = amplify.store(key);
                        break;
                    case config.storageType.sessionStorage:
                        value = amplify.store.sessionStorage(key);
                        break;
                }

                return value;
            },

            save = function (key, value, storageType) {
                storageType = storageType || config.storageType.localStorage;

                switch (storageType) {
                    case config.storageType.localStorage:
                        amplify.store(key, value, expires);
                        break;
                    case config.storageType.sessionStorage:
                        amplify.store.sessionStorage(key, value, expires);
                        break;
                }
            };

        return {
            clear: clear,
            fetch: fetch,
            save: save
        };
    });