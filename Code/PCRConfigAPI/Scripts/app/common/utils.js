﻿define('utils',
    ['underscore', 'moment', 'string'],
    function (_, moment, S) {
        var
            endOfDay = function (day) {
                return moment(new Date(day))
                    .add('days', 1)
                    .add('seconds', -1)
                    .toDate();
            },
            getFirstTimeslot = function (timeslots) {
                return moment(timeslots()[0].start()).format('MM-DD-YYYY');
            },
            hasProperties = function (obj) {
                for (var prop in obj) {
                    if (obj.hasOwnProperty(prop)) {
                        return true;
                    }
                }
                return false;
            },
            invokeFunctionIfExists = function (callback) {
                if (_.isFunction(callback)) {
                    callback();
                }
            },
            mapMemoToArray = function (items) {
                var underlyingArray = [];
                for (var prop in items) {
                    if (items.hasOwnProperty(prop)) {
                        underlyingArray.push(items[prop]);
                    }
                }
                return underlyingArray;
            },
            regExEscape = function (text) {
                // Removes regEx characters from search filter boxes in our app
                return text.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, "\\$&");
            },
            restoreFilter = function (filterData) {
                var stored = filterData.stored,
                    filter = filterData.filter,
                    dc = filterData.datacontext;

                // Create a list of the 5 filters to process
                var filterList = [
                    { raw: stored.favoriteOnly, filter: filter.favoriteOnly },
                    { raw: stored.searchText, filter: filter.searchText },
                    { raw: stored.speaker, filter: filter.speaker, fetch: dc.persons.getLocalById },
                    { raw: stored.timeslot, filter: filter.timeslot, fetch: dc.timeslots.getLocalById },
                    { raw: stored.track, filter: filter.track, fetch: dc.tracks.getLocalById }
                ];

                // For each filter, set the filter to the stored value, or get it from the DC
                _.each(filterList, function (map) {
                    var rawProperty = map.raw, // POJO
                        filterProperty = map.filter, // observable
                        fetchMethod = map.fetch;
                    if (rawProperty && filterProperty() !== rawProperty) {
                        if (fetchMethod) {
                            var obj = fetchMethod(rawProperty.id);
                            if (obj) {
                                filterProperty(obj);
                            }
                        } else {
                            filterProperty(rawProperty);
                        }
                    }
                });
            },

            getDefaultUTCDate = function () {
                return moment().subtract('days', 700).utc().toISOString();
            },

            getCurrentDate = function () {
                return moment().utc().toISOString();
            },

            fillArray = function (dtoArray, memArray) {
                if (dtoArray && dtoArray.length > 0) {
                    for (var j = 0; j < dtoArray.length; j++) {
                        memArray.push(dtoArray[j]);
                    }
                }
            },

            extendObservable = function (target, source) {
                var prop, srcVal, tgtProp, srcProp,
                    isObservable = false;

                for (prop in source) {

                    if (!source.hasOwnProperty(prop)) {
                        continue;
                    }

                    if (ko.isWriteableObservable(source[prop])) {
                        isObservable = true;
                        srcVal = source[prop]();
                    } else if (typeof (source[prop]) !== 'function') {
                        srcVal = source[prop];
                    }

                    if (ko.isWriteableObservable(target[prop])) {
                        target[prop](srcVal);
                    } else if (target[prop] === null || target[prop] === undefined) {

                        target[prop] = isObservable ? ko.observable(srcVal) : srcVal;

                    } else if (typeof (target[prop]) !== 'function') {
                        target[prop] = srcVal;
                    }

                    isObservable = false;
                }
                console.log(target.resources());
                console.log(source.resources());
            },

            getDuration = function (time, unit) {
                var tempDuration = moment.duration(time, unit);
                return S(tempDuration.hours()).padLeft(2, '0') + ':' + S(tempDuration.minutes()).padLeft(2, '0') + ':' + S(tempDuration.seconds()).padLeft(2, '0');
            },

            clone = function (obj, emptyObj) {
                var json = ko.toJSON(obj);
                var js = JSON.parse(json);
                return extendObservable(emptyObj, js);
            },

            getTimeSlots = function (noOfDays) {

                var timeslots = [];
                var add = 0;
                var hourDiff = 24 / noOfDays;

                for (var i = 0; i < 24; i++) {
                    var time;
                    if (i < 10)
                        time = "0" + i + ":00"
                    else
                        time = add + i + ":00"

                    timeslots.push(time);
                }

                return timeslots;


            },
            getDifferenceBetweenDates = function (string1, string2) {
                var date1 = new Date(string1);
                var date2 = new Date(string2);
                var one_day = 1000 * 60 * 60 * 24;
                var date1_ms = date1.getTime();
                var date2_ms = date2.getTime();
                var difference_ms = date2_ms - date1_ms;
                var diffDays = Math.round(difference_ms / one_day);
                return diffDays;
            },
            arrayIsContainObject = function (array, objectToCheck, againstTheProperty) {
                var detailObject = { flag: false, objectIndex: -1 };
                if (array && objectToCheck && array instanceof Array && objectToCheck instanceof Object) {
                    for (var i = 0; i < array.length; i++) {
                        var tempObject = array[i];
                        if (tempObject.hasOwnProperty(againstTheProperty) && objectToCheck.hasOwnProperty(againstTheProperty)) {
                            if (tempObject[againstTheProperty] === objectToCheck[againstTheProperty]) {
                                detailObject.flag = true;
                                detailObject.objectIndex = i;
                                break;
                            }
                        }
                    }
                }
                return detailObject;
            },
            getDatesByRange = function (fromString, toString, formatString) {
                var d1 = new Date(fromString);
                var d2 = new Date(toString);
                arr = new Array();
                if (((d2 - d1) / (1000 * 3600 * 24)) < 0)
                    return arr;
                if (((d2 - d1) / (1000 * 3600 * 24)) == 0)
                    arr.push(d1);
                if (((d2 - d1) / (1000 * 3600 * 24)) > 0) {
                    while (d1 <= d2) {
                        arr.push(new Date(d1));
                        d1.setDate(d1.getDate() + 1);
                    }
                }
                return arr;
            };

        return {
            endOfDay: endOfDay,
            getFirstTimeslot: getFirstTimeslot,
            hasProperties: hasProperties,
            invokeFunctionIfExists: invokeFunctionIfExists,
            mapMemoToArray: mapMemoToArray,
            regExEscape: regExEscape,
            restoreFilter: restoreFilter,
            getDefaultUTCDate: getDefaultUTCDate,
            getCurrentDate: getCurrentDate,
            fillArray: fillArray,
            getDuration: getDuration,
            clone: clone,
            getDatesByRange: getDatesByRange,
            getTimeSlots: getTimeSlots,
            getDifferenceBetweenDates: getDifferenceBetweenDates,
            arrayIsContainObject: arrayIsContainObject
        };
    });

