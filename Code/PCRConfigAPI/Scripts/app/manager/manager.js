﻿define('manager',
    [
        'manager.notificationhub'
    ],
    function (notificationhub) {

        return {
            notificationhub: notificationhub
    }
});