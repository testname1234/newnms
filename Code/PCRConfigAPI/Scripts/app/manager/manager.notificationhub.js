﻿define('manager.notificationhub',
    [
        'jquery',
        'dataservice',
        'datacontext',
        'underscore',
        'model.mapper'
    ],
    function ($, dataservice, dc, _,mapper) {
        var
            getAllPcr = function () {
                var d = new jQuery.Deferred();
                return $.Deferred(function () {
                    $.when(dataservice.getAllPcr())
                     .done(function (responseData) {
                         if (responseData.Data) {
                             dc.groups.fillData(responseData.Data);
                         }
                         d.resolve(responseData);
                     })
                     .fail(function (responseData) {
                         d.reject(responseData);
                     });
                }).promise();
            };

            getAllDevice = function () {
                var d = new jQuery.Deferred();
                return $.Deferred(function () {
                    $.when(dataservice.getAllDevice())
                     .done(function (responseData) {
                         if (responseData.Data) {
                             dc.devices.fillData(responseData.Data);
                         }
                         d.resolve(responseData);
                     })
                     .fail(function (responseData) {
                         d.reject(responseData);
                     });
                }).promise();
            };

            updateGroup = function (data) {
                return $.Deferred(function (d) {
                    $.when(dataservice.updateGroup(data))
                     .done(function (responseData) {
                         if (responseData.IsSuccess) {
                             console.log(responseData.Data);
                             dc.groups.fillData([responseData.Data]);
                             d.resolve(responseData);
                         }
                         else {
                             d.reject(responseData);
                         }
                     })
                     .fail(function (responseData) {
                         d.reject(responseData);
                     });
                }).promise();
            };

            updateDevices = function (data) {
                return $.Deferred(function (d) {
                    console.log(data);
                    $.when(dataservice.updateDevice(data))
                     .done(function (responseData) {
                         if (responseData.IsSuccess) {
                             console.log(responseData.Data);
                             dc.devices.fillData([responseData.Data]);
                             d.resolve(responseData);
                         }
                         else {
                             d.reject(responseData);
                         }
                     })
                     .fail(function (responseData) {
                         d.reject(responseData);
                     });
                }).promise();
            };

            deleteGroup = function (data) {
                return $.Deferred(function (d) {
                    console.log(data);
                    $.when(dataservice.deleteGroup(data))
                     .done(function (responseData) {
                         if (responseData.IsSuccess) {
                             d.resolve(responseData);
                         }
                         else {
                             d.reject(responseData);
                         }
                     })
                     .fail(function (responseData) {
                         d.reject(responseData);
                     });
                }).promise();
            };

            deleteDevice = function (data) {
                return $.Deferred(function (d) {
                    debugger
                    console.log(data);
                    $.when(dataservice.deleteDevice(data))
                     .done(function (responseData) {
                         if (responseData.IsSuccess) {
                             d.resolve(responseData);
                         }
                         else {
                             d.reject(responseData);
                         }
                     })
                     .fail(function (responseData) {
                         d.reject(responseData);
                     });
                }).promise();
            };

            insertGroup = function (data) {
                return $.Deferred(function (d) {
                    console.log(data);
                    $.when(dataservice.insertGroup(data))
                     .done(function (responseData) {
                         if (responseData.IsSuccess) {
                             dc.groups.fillData([responseData.Data]);
                             d.resolve(responseData);
                         }
                         else {
                             d.reject(responseData);
                         }
                     })
                     .fail(function (responseData) {
                         d.reject(responseData);
                     });
                }).promise();
            };

            insertDevice = function (data) {
                return $.Deferred(function (d) {
                    console.log(data);
                    $.when(dataservice.insertDevice(data))
                     .done(function (responseData) {
                         if (responseData.IsSuccess) {
                             dc.devices.fillData([responseData.Data]);
                             d.resolve(responseData);
                         }
                         else {
                             d.reject(responseData);
                         }
                     })
                     .fail(function (responseData) {
                         d.reject(responseData);
                     });
                }).promise();
            };

            getAllDeviceType = function () {
                var d = new jQuery.Deferred();
                return $.Deferred(function () {
                    $.when(dataservice.getAllDeviceType())
                     .done(function (responseData) {
                         if (responseData.Data) {
                             dc.deviceTypes.fillData(responseData.Data);
                         }
                         d.resolve(responseData);
                     })
                     .fail(function (responseData) {
                         d.reject(responseData);
                     });
                }).promise();
            };

            getAllDeviceStatus = function () {
                var d = new jQuery.Deferred();
                return $.Deferred(function () {
                    $.when(dataservice.getAllDeviceStatus())
                     .done(function (responseData) {
                         if (responseData.Data) {
                             dc.deviceStatus.fillData(responseData.Data);
                         }
                         d.resolve(responseData);
                     })
                     .fail(function (responseData) {
                         d.reject(responseData);
                     });
                }).promise();
            };

        return {
            getAllPcr: getAllPcr,
            getAllDevice: getAllDevice,
            updateGroup: updateGroup,
            updateDevices: updateDevices,
            deleteGroup: deleteGroup,
            deleteDevice: deleteDevice,
            insertDevice: insertDevice,
            insertGroup: insertGroup,
            getAllDeviceType: getAllDeviceType,
            getAllDeviceStatus: getAllDeviceStatus
        }
    });