﻿define('model.device',
    ['ko'],
    function (ko) {

        var
            _dc = this,

            Device = function () {

                var self = this;

                self.id,
                self.name = ko.observable(''),
                self.creationDate,
                self.groupId = ko.observable(),
                self.deviceKey = ko.observable(''),
                self.deviceStatusId = ko.observable(),
                self.isMosDevice = ko.observable(false),
                self.lastHeartBeatTimeStr,
                self.deviceTypeId = ko.observable(),
                self.ip = ko.observable(''),
                self.lastUpdatedDateStr,
                self.port = ko.observable(''),

                self.Editable = ko.observable(false),

                self.isNullo = false;

                return self;
            }

        Device.Nullo = new Device();

        Device.Nullo.isNullo = true;

        // static member
        Device.datacontext = function (dc) {
            if (dc) { _dc = dc; }
            return _dc;
        };

        return Device;
    });