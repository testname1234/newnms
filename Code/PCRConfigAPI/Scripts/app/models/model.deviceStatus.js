﻿define('model.deviceStatus',
    ['ko'],
    function (ko) {

        var
            _dc = this,

            DeviceStatus = function () {

                var self = this;

                self.id,
                self.name = ko.observable(''),

                self.isNullo = false;

                return self;
            }

        DeviceStatus.Nullo = new DeviceStatus();

        DeviceStatus.Nullo.isNullo = true;

        // static member
        DeviceStatus.datacontext = function (dc) {
            if (dc) { _dc = dc; }
            return _dc;
        };

        return DeviceStatus;
    });