﻿define('model.deviceType',
    ['ko'],
    function (ko) {

        var
            _dc = this,

            DeviceType = function () {

                var self = this;

                self.id,
                self.name = ko.observable(''),

                self.isNullo = false;

                return self;
            }

        DeviceType.Nullo = new DeviceType();

        DeviceType.Nullo.isNullo = true;

        // static member
        DeviceType.datacontext = function (dc) {
            if (dc) { _dc = dc; }
            return _dc;
        };

        return DeviceType;
    });