﻿define('model.group',
    [
        'ko'
    ],
    function (ko) {

        var
            _dc = this,

            Group = function () {

                var self = this;

                self.id,
                self.name=ko.observable(''),
                self.creationDate,
                self.groupKey = ko.observable(''),
                self.parentId = ko.observable(),
                
                self.Editable = ko.observable(false),

                self.isNullo = false;

                return self;
            }

        Group.Nullo = new Group();

        Group.Nullo.isNullo = true;

        // static member
        Group.datacontext = function (dc) {
            if (dc) { _dc = dc; }
            return _dc;
        };

        return Group;
    });