﻿define('model',
    [
        'model.device',
        'model.group',
        'model.deviceType',
        'model.deviceStatus'
    ],
    function (Device, Group,DeviceType,DeviceStatus) {

        var
            model = {
                Device: Device,
                Group: Group,
                DeviceType: DeviceType,
                DeviceStatus: DeviceStatus
            };

        model.setDataContext = function (dc) {
            model.Device.datacontext(dc);
            model.Group.datacontext(dc);
            model.DeviceType.datacontext(dc);
            model.DeviceStatus.datacontext(dc);
        };

        return model;
    });