﻿define('model.mapper',
    ['jquery', 'model', 'config', 'underscore', 'moment'],
    function ($, model, config, _, moment) {
        var
            device = {
                getDtoId: function (dto) {
                    return dto.DeviceId;
                },
                getSortedValue: function (dto) {
                    return dto.DeviceId;
                },
                fromDto: function (dto, item) {
                    item = item || new model.Device();

                    item.id = dto.DeviceId;
                    item.name ( dto.Name || 'Unknown');
                    item.creationDate = dto.CreationDateStr;
                    item.groupId( dto.GroupId);
                    item.deviceKey( dto.DeviceKey);
                    item.deviceStatusId (dto.DeviceStatusId);
                    item.deviceTypeId ( dto.DeviceTypeId);
                    item.ip( dto.Ip);
                    item.isMosDevice ( dto.IsMosDevice);
                    item.lastHeartBeatTimeStr = dto.LastHeartBeatTimeStr;
                    item.lastUpdatedDateStr = dto.LastUpdatedDateStr;
                    item.port(dto.Port);

                    return item;
                }
            },
            deviceType = {
                getDtoId: function (dto) {
                    return dto.DeviceTypeId;
                },
                getSortedValue: function (dto) {
                    return dto.DeviceTypeId;
                },
                fromDto: function (dto, item) {
                    item = item || new model.DeviceType();

                    item.id = dto.DeviceTypeId;
                    item.name(dto.Name || 'Unknown');

                    return item;
                }
            },
            deviceStatus = {
                getDtoId: function (dto) {
                    return dto.DeviceStatusId;
                },
                getSortedValue: function (dto) {
                    return dto.DeviceStatusId;
                },
                fromDto: function (dto, item) {
                    item = item || new model.DeviceStatus();

                    item.id = dto.DeviceStatusId;
                    item.name(dto.Name || 'Unknown');

                    return item;
                }
            },
            group = {
                getDtoId: function (dto) {
                    return dto.GroupId;
                },
                getSortedValue: function (dto) {
                    return dto.GroupId;
                },
                fromDto: function (dto, item) {
                    item = item || new model.Group();

                    item.id = dto.GroupId;
                    item.name (dto.Name || 'Unknown');
                    item.creationDate = dto.CreationDateStr;
                    item.groupKey (dto.GroupKey);
                    item.parentId (dto.ParentId || 0);

                    return item;
                }
        };

        return {
            device: device,
            group: group,
            deviceType: deviceType,
            deviceStatus: deviceStatus
        };
    });