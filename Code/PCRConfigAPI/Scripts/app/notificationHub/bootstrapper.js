﻿define('bootstrapper',
    [
        'config',
        'binder',
        'route-config',
        'manager',
        'notifier.appdata'
    ],
    function (config, binder, routeConfig, manager,appdata) {

        var
            run = function () {

                var views = config.views.notificationHub;

                appdata.views = views;

                binder.bindPreLoginViews(views);

                routeConfig.register(views);

                manager.notificationhub.getAllPcr();
                manager.notificationhub.getAllDevice();
                manager.notificationhub.getAllDeviceType();
                manager.notificationhub.getAllDeviceStatus();
            };

        return {
            run: run
        };
    });
