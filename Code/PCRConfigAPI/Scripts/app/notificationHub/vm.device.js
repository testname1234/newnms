﻿define('vm.device',
    [
        'ko',
        'datacontext',
        'model.group',
        'model.device',
        'manager',
        'enum'
    ],
    function (ko,dc,group,device,manager,e) {

        var
        allDeviceTypes = ko.computed({
            read: function () {
                return dc.deviceTypes.getObservableList();
            },
            deferEvaluation: true
        }),
        allDeviceStatuses = ko.computed({
            read: function () {
                return dc.deviceStatus.getObservableList();
            },
            deferEvaluation: true
        }),
        allDevices = ko.computed({
            read: function () {
                return dc.devices.getObservableList();
            },
            deferEvaluation: true
        }),

        allGroups = ko.computed({
            read: function () {
                return dc.groups.observableList();
            },
            deferEvaluation: true
        }),

        Edit = function (data) {
            data.Editable(!data.Editable());
        },

        Update = function (data) {
            var requestObj = {};
            requestObj.DeviceId = data.id;
            requestObj.Name = data.name;
            requestObj.GroupId = data.groupId;
            requestObj.DeviceKey = data.deviceKey;
            requestObj.DeviceStatusId = data.deviceStatusId;
            requestObj.DeviceTypeId=data.deviceTypeId;
            requestObj.Ip=data.ip;
            requestObj.IsMosDevice=data.isMosDevice;
            requestObj.Port = data.port;
            console.log(requestObj);


            $.when(manager.notificationhub.updateDevices(requestObj))
            .done(function (response) {
                data.Editable(!data.Editable());
            })
            .fail(function (error) {
                console.log(error);
                alert('could not update');
            })

        },

        deleteDevice = function (data) {
            var answer = true; // confirm('Are you sure you want to delete this Device? ' + data.name());
            if (answer) {

                var requestObj = { id: data.id };

                $.when(manager.notificationhub.deleteDevice(requestObj))
                .done(function (response) {
                    //(dc.devices.observableList()).remove(data);
                    dc.devices.removeById(requestObj.id, true);
                })
                .fail(function (error) {
                    console.log(error);
                    alert('could not delete');
                })
            }
        },

        addDevice = function (data) {
            console.log(data);
            var requestObj = {};
            requestObj.Name = data.name;
            requestObj.GroupId = data.groupId;
            requestObj.DeviceKey = data.deviceKey;
            requestObj.DeviceStatusId = data.deviceStatusId;
            requestObj.DeviceTypeId = data.deviceTypeId;
            requestObj.Ip = data.ip;
            requestObj.IsMosDevice = data.isMosDevice;
            requestObj.Port = data.port;
            requestObj.creationDate = new Date().getDate();

            $.when(manager.notificationhub.insertDevice(requestObj))
            .done(function (response) {
                showAdd(!showAdd());
            })
            .fail(function (error) {
                console.log(error);
                alert('could not Add');
            })

        },

        showAdd = ko.observable(false),
         addedRow=new device(),
        showrow = function () {
            showAdd(!showAdd());
            addedRow = new device();
        },

        hiderow = function () {
            showAdd(!showAdd());
        },

        activate = function (routeData, callback) {

        };

        return {
            activate: activate,
            allDevices: allDevices,
            allGroups:allGroups,
            Update: Update,
            Edit: Edit,
            deleteDevice: deleteDevice,
            addDevice: addDevice,
            hiderow: hiderow,
            showrow: showrow,
            addedRow: addedRow,
            showAdd: showAdd,
            e: e,
            allDeviceTypes: allDeviceTypes,
            allDeviceStatuses: allDeviceStatuses
        };
    });