﻿define('vm.group',
    [
        'datacontext',
        'model.group',
        'manager'
    ],
    function (dc,group,manager) {
        var
            count = 0;
            allGroups = ko.computed({
                read: function () {
                    if (count == 0) {
                        var ra = new group();
                        ra.id = 0;
                        ra.name('None');
                        ra.creationDate = '';
                        ra.groupKey('');
                        ra.parentId('');
                        (dc.groups.observableList()).push(ra);
                        count = 1;
                    }
                    return dc.groups.observableList();
                },
                deferEvaluation: true
            }),

            Edit = function (data) {
                data.Editable(!data.Editable());
            },

            Update = function (data) {
                var requestObj = {};
                requestObj.GroupId = data.id;
                requestObj.Name = data.name;
                requestObj.GroupKey = data.groupKey;
                requestObj.ParentId = data.parentId;
                
                $.when(manager.notificationhub.updateGroup(requestObj))
                .done(function (response) {
                    data.Editable(!data.Editable());
                })
                .fail(function (error) {
                    console.log(error);
                    alert('could not update');
                })

            },

            deleteGroup = function (data) {
                var answer = true; //confirm('Are you sure you want to delete this Group? ' + data.name());
                if (answer) {

                    var requestObj = { id: data.id };

                    $.when(manager.notificationhub.deleteGroup(requestObj))
                    .done(function (response) {
                        //(dc.groups.observableList()).remove(data);
                        dc.groups.removeById(requestObj.id, true);
                    })
                    .fail(function (error) {
                        console.log(error);
                        alert('could not delete');
                    })
                }
            },

            addGroup = function (data) {
                console.log(data);
                var requestObj = {};
                requestObj.Name = data.name;
                requestObj.GroupKey = data.groupKey;
                requestObj.ParentId = data.parentId;
                requestObj.creationDate = new Date().toString();

                $.when(manager.notificationhub.insertGroup(requestObj))
                .done(function (response) {
                    showAdd(!showAdd());
                })
                .fail(function (error) {
                    console.log(error);
                    alert('could not Add');
                })
            },
            

            showAdd = ko.observable(false),
            addedRow = new group(),
            showrow = function () {
                showAdd(!showAdd());
                addedRow = new group();
            },

            hiderow = function () {
                showAdd(!showAdd());
            },

            activate = function (routeData, callback) {

            };


        return {
            activate: activate,
            allGroups: allGroups,
            Update: Update,
            Edit: Edit,
            deleteGroup: deleteGroup,
            addGroup: addGroup,
            hiderow: hiderow,
            showrow: showrow,
            addedRow: addedRow,
            showAdd: showAdd
        };
    });