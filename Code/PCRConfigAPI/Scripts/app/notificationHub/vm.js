﻿define('vm',
    [
        'vm.shell',
        'vm.device',
        'vm.group'
    ],

    function (shell, device, group)
    {
        var vmDictionary = {};

        vmDictionary['#Notifier-Groups-View'] = group;
        vmDictionary['#Notifier-Devices-View'] = device;
        vmDictionary['#shell-top-nav-view'] = shell;

        return {
            dictionary: vmDictionary
        };
    });