﻿define('vm.shell',
     [
         'ko',
         'config',
         'vm',
         'router',
         'notifier.appdata'
     ],
    function (ko, config, vm, router, appdata) {

        var
            navigations = function () {
                var menuHashes = appdata.views;
                var navs = [];

                for (var item in menuHashes) {
                    var tempItem = menuHashes[item];
                    if (tempItem.isInTopMenu) {
                        var obj = {};
                        obj.title = tempItem.title;
                        obj.href = tempItem.url;
                        navs.push(obj);
                    }
                }

                return navs;

            },

            currentPage = ko.computed({
                read: function () {
                    return router.currentHash();
                },
                deferEvaluation: true
            }),

            // Methods
            //-------------------

            activate = function (routeData) {

            },

            init = function () {
                activate();
            };
        $(".bdl").click(function () {

            window.location.href = '/login#/index';
            document.cookie = config.sessionKeyName + '=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';
            document.cookie = config.roleid + '=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';
            document.cookie = config.userid + '=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';
            
        });

        init();

        return {
            currentPage: currentPage,
            activate: activate,
            navigations: navigations
        };
    });
