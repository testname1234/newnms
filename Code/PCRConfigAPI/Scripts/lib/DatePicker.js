﻿/*==== DATE PICKER SCRIPT ====*/
(function (e) { var t = function () { var t = {}, n = { years: "datepickerViewYears", moths: "datepickerViewMonths", days: "datepickerViewDays" }, r = { wrapper: '<div class="datepicker"><div class="datepickerBorderT" /><div class="datepickerBorderB" /><div class="datepickerBorderL" /><div class="datepickerBorderR" /><div class="datepickerBorderTL" /><div class="datepickerBorderTR" /><div class="datepickerBorderBL" /><div class="datepickerBorderBR" /><div class="datepickerContainer"><table cellspacing="0" cellpadding="0"><tbody><tr></tr></tbody></table></div></div>', head: ['<td class="datepickerBlock">', '<table cellspacing="0" cellpadding="0">', "<thead>", "<tr>", '<th colspan="7"><a class="datepickerGoPrev" href="#"><span><%=prev%></span></a>', '<a class="datepickerMonth" href="#"><span></span></a>', '<a class="datepickerGoNext" href="#"><span><%=next%></span></a></th>', "</tr>", '<tr class="datepickerDoW">', "<th><span><%=day1%></span></th>", "<th><span><%=day2%></span></th>", "<th><span><%=day3%></span></th>", "<th><span><%=day4%></span></th>", "<th><span><%=day5%></span></th>", "<th><span><%=day6%></span></th>", "<th><span><%=day7%></span></th>", "</tr>", "</thead>", "</table></td>"], space: '<td class="datepickerSpace"><div></div></td>', days: ['<tbody class="datepickerDays">', "<tr>", '<td class="<%=weeks[0].days[0].classname%>"><a href="#"><span><%=weeks[0].days[0].text%></span></a></td>', '<td class="<%=weeks[0].days[1].classname%>"><a href="#"><span><%=weeks[0].days[1].text%></span></a></td>', '<td class="<%=weeks[0].days[2].classname%>"><a href="#"><span><%=weeks[0].days[2].text%></span></a></td>', '<td class="<%=weeks[0].days[3].classname%>"><a href="#"><span><%=weeks[0].days[3].text%></span></a></td>', '<td class="<%=weeks[0].days[4].classname%>"><a href="#"><span><%=weeks[0].days[4].text%></span></a></td>', '<td class="<%=weeks[0].days[5].classname%>"><a href="#"><span><%=weeks[0].days[5].text%></span></a></td>', '<td class="<%=weeks[0].days[6].classname%>"><a href="#"><span><%=weeks[0].days[6].text%></span></a></td>', "</tr>", "<tr>", '<td class="<%=weeks[1].days[0].classname%>"><a href="#"><span><%=weeks[1].days[0].text%></span></a></td>', '<td class="<%=weeks[1].days[1].classname%>"><a href="#"><span><%=weeks[1].days[1].text%></span></a></td>', '<td class="<%=weeks[1].days[2].classname%>"><a href="#"><span><%=weeks[1].days[2].text%></span></a></td>', '<td class="<%=weeks[1].days[3].classname%>"><a href="#"><span><%=weeks[1].days[3].text%></span></a></td>', '<td class="<%=weeks[1].days[4].classname%>"><a href="#"><span><%=weeks[1].days[4].text%></span></a></td>', '<td class="<%=weeks[1].days[5].classname%>"><a href="#"><span><%=weeks[1].days[5].text%></span></a></td>', '<td class="<%=weeks[1].days[6].classname%>"><a href="#"><span><%=weeks[1].days[6].text%></span></a></td>', "</tr>", "<tr>", '<td class="<%=weeks[2].days[0].classname%>"><a href="#"><span><%=weeks[2].days[0].text%></span></a></td>', '<td class="<%=weeks[2].days[1].classname%>"><a href="#"><span><%=weeks[2].days[1].text%></span></a></td>', '<td class="<%=weeks[2].days[2].classname%>"><a href="#"><span><%=weeks[2].days[2].text%></span></a></td>', '<td class="<%=weeks[2].days[3].classname%>"><a href="#"><span><%=weeks[2].days[3].text%></span></a></td>', '<td class="<%=weeks[2].days[4].classname%>"><a href="#"><span><%=weeks[2].days[4].text%></span></a></td>', '<td class="<%=weeks[2].days[5].classname%>"><a href="#"><span><%=weeks[2].days[5].text%></span></a></td>', '<td class="<%=weeks[2].days[6].classname%>"><a href="#"><span><%=weeks[2].days[6].text%></span></a></td>', "</tr>", "<tr>", '<td class="<%=weeks[3].days[0].classname%>"><a href="#"><span><%=weeks[3].days[0].text%></span></a></td>', '<td class="<%=weeks[3].days[1].classname%>"><a href="#"><span><%=weeks[3].days[1].text%></span></a></td>', '<td class="<%=weeks[3].days[2].classname%>"><a href="#"><span><%=weeks[3].days[2].text%></span></a></td>', '<td class="<%=weeks[3].days[3].classname%>"><a href="#"><span><%=weeks[3].days[3].text%></span></a></td>', '<td class="<%=weeks[3].days[4].classname%>"><a href="#"><span><%=weeks[3].days[4].text%></span></a></td>', '<td class="<%=weeks[3].days[5].classname%>"><a href="#"><span><%=weeks[3].days[5].text%></span></a></td>', '<td class="<%=weeks[3].days[6].classname%>"><a href="#"><span><%=weeks[3].days[6].text%></span></a></td>', "</tr>", "<tr>", '<td class="<%=weeks[4].days[0].classname%>"><a href="#"><span><%=weeks[4].days[0].text%></span></a></td>', '<td class="<%=weeks[4].days[1].classname%>"><a href="#"><span><%=weeks[4].days[1].text%></span></a></td>', '<td class="<%=weeks[4].days[2].classname%>"><a href="#"><span><%=weeks[4].days[2].text%></span></a></td>', '<td class="<%=weeks[4].days[3].classname%>"><a href="#"><span><%=weeks[4].days[3].text%></span></a></td>', '<td class="<%=weeks[4].days[4].classname%>"><a href="#"><span><%=weeks[4].days[4].text%></span></a></td>', '<td class="<%=weeks[4].days[5].classname%>"><a href="#"><span><%=weeks[4].days[5].text%></span></a></td>', '<td class="<%=weeks[4].days[6].classname%>"><a href="#"><span><%=weeks[4].days[6].text%></span></a></td>', "</tr>", "<tr>", '<td class="<%=weeks[5].days[0].classname%>"><a href="#"><span><%=weeks[5].days[0].text%></span></a></td>', '<td class="<%=weeks[5].days[1].classname%>"><a href="#"><span><%=weeks[5].days[1].text%></span></a></td>', '<td class="<%=weeks[5].days[2].classname%>"><a href="#"><span><%=weeks[5].days[2].text%></span></a></td>', '<td class="<%=weeks[5].days[3].classname%>"><a href="#"><span><%=weeks[5].days[3].text%></span></a></td>', '<td class="<%=weeks[5].days[4].classname%>"><a href="#"><span><%=weeks[5].days[4].text%></span></a></td>', '<td class="<%=weeks[5].days[5].classname%>"><a href="#"><span><%=weeks[5].days[5].text%></span></a></td>', '<td class="<%=weeks[5].days[6].classname%>"><a href="#"><span><%=weeks[5].days[6].text%></span></a></td>', "</tr>", "</tbody>"], months: ['<tbody class="<%=className%>">', "<tr>", '<td colspan="2"><a href="#"><span><%=data[0]%></span></a></td>', '<td colspan="2"><a href="#"><span><%=data[1]%></span></a></td>', '<td colspan="2"><a href="#"><span><%=data[2]%></span></a></td>', '<td colspan="1"><a href="#"><span><%=data[3]%></span></a></td>', "</tr>", "<tr>", '<td colspan="2"><a href="#"><span><%=data[4]%></span></a></td>', '<td colspan="2"><a href="#"><span><%=data[5]%></span></a></td>', '<td colspan="2"><a href="#"><span><%=data[6]%></span></a></td>', '<td colspan="1"><a href="#"><span><%=data[7]%></span></a></td>', "</tr>", "<tr>", '<td colspan="2"><a href="#"><span><%=data[8]%></span></a></td>', '<td colspan="2"><a href="#"><span><%=data[9]%></span></a></td>', '<td colspan="2"><a href="#"><span><%=data[10]%></span></a></td>', '<td colspan="1"><a href="#"><span><%=data[11]%></span></a></td>', "</tr>", "</tbody>"] }, i = { date: null, current: null, inline: false, mode: "single", calendars: 1, starts: 0, prev: "&#9664;", next: "&#9654;", view: "days", position: "bottom", showOn: "focus", onRenderCell: function () { return {} }, onChange: function () { }, onBeforeShow: function () { return true }, onAfterShow: function () { }, onBeforeHide: function () { return true }, onAfterHide: function () { }, locale: { daysMin: ["S", "M", "T", "W", "T", "F", "S"], months: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"], monthsShort: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"] }, extraHeight: false, extraWidth: false, lastSel: false }, s = function (t) { var n = e(t).data("datepicker"); var i = e(t); var s = Math.floor(n.calendars / 2), o, u, a, f, l = 0, c, h, p, d, v; i.find("td>table tbody").remove(); for (var m = 0; m < n.calendars; m++) { o = new Date(n.current); o.addMonths(-s + m); v = i.find("table").eq(m + 1); if (m == 0) v.addClass("datepickerFirstView"); if (m == n.calendars - 1) v.addClass("datepickerLastView"); if (v.hasClass("datepickerViewDays")) { a = o.getMonthName(true) + ", " + o.getFullYear() } else if (v.hasClass("datepickerViewMonths")) { a = o.getFullYear() } else if (v.hasClass("datepickerViewYears")) { a = o.getFullYear() - 6 + " - " + (o.getFullYear() + 5) } v.find("thead tr:first th a:eq(1) span").text(a); a = o.getFullYear() - 6; u = { data: [], className: "datepickerYears" }; for (var g = 0; g < 12; g++) { u.data.push(a + g) } d = tmpl(r.months.join(""), u); o.setDate(1); u = { weeks: [], test: 10 }; f = o.getMonth(); var a = (o.getDay() - n.starts) % 7; o.addDays(-(a + (a < 0 ? 7 : 0))); l = 0; while (l < 42) { h = parseInt(l / 7, 10); p = l % 7; if (!u.weeks[h]) { u.weeks[h] = { days: [] } } u.weeks[h].days[p] = { text: o.getDate(), classname: [] }; var y = new Date; if (y.getDate() == o.getDate() && y.getMonth() == o.getMonth() && y.getYear() == o.getYear()) { u.weeks[h].days[p].classname.push("datepickerToday") } if (o > y) { u.weeks[h].days[p].classname.push("datepickerFuture") } if (f != o.getMonth()) { u.weeks[h].days[p].classname.push("datepickerNotInMonth"); u.weeks[h].days[p].classname.push("datepickerDisabled") } if (o.getDay() == 0) { u.weeks[h].days[p].classname.push("datepickerSunday") } if (o.getDay() == 6) { u.weeks[h].days[p].classname.push("datepickerSaturday") } var b = n.onRenderCell(t, o); var w = o.valueOf(); if (n.date && (!e.isArray(n.date) || n.date.length > 0)) { if (b.selected || n.date == w || e.inArray(w, n.date) > -1 || n.mode == "range" && w >= n.date[0] && w <= n.date[1]) { u.weeks[h].days[p].classname.push("datepickerSelected") } } if (b.disabled) { u.weeks[h].days[p].classname.push("datepickerDisabled") } if (b.className) { u.weeks[h].days[p].classname.push(b.className) } u.weeks[h].days[p].classname = u.weeks[h].days[p].classname.join(" "); l++; o.addDays(1) } d = tmpl(r.days.join(""), u) + d; u = { data: n.locale.monthsShort, className: "datepickerMonths" }; d = tmpl(r.months.join(""), u) + d; v.append(d) } }, o = function (e) { if (Date.prototype.tempDate) { return } Date.prototype.tempDate = null; Date.prototype.months = e.months; Date.prototype.monthsShort = e.monthsShort; Date.prototype.getMonthName = function (e) { return this[e ? "months" : "monthsShort"][this.getMonth()] }; Date.prototype.addDays = function (e) { this.setDate(this.getDate() + e); this.tempDate = this.getDate() }; Date.prototype.addMonths = function (e) { if (this.tempDate == null) { this.tempDate = this.getDate() } this.setDate(1); this.setMonth(this.getMonth() + e); this.setDate(Math.min(this.tempDate, this.getMaxDays())) }; Date.prototype.addYears = function (e) { if (this.tempDate == null) { this.tempDate = this.getDate() } this.setDate(1); this.setFullYear(this.getFullYear() + e); this.setDate(Math.min(this.tempDate, this.getMaxDays())) }; Date.prototype.getMaxDays = function () { var e = new Date(Date.parse(this)), t = 28, n; n = e.getMonth(); t = 28; while (e.getMonth() == n) { t++; e.setDate(t) } return t - 1 } }, u = function (t) { var n = e(t).data("datepicker"); var r = e("#" + n.id); if (n.extraHeight === false) { var i = e(t).find("div"); n.extraHeight = i.get(0).offsetHeight + i.get(1).offsetHeight; n.extraWidth = i.get(2).offsetWidth + i.get(3).offsetWidth } var s = r.find("table:first").get(0); var o = s.offsetWidth; var u = s.offsetHeight; r.css({ width: o + n.extraWidth + "px", height: u + n.extraHeight + "px" }).find("div.datepickerContainer").css({ width: o + "px", height: u + "px" }) }, a = function (t) { if (e(t.target).is("span")) { t.target = t.target.parentNode } var n = e(t.target); if (n.is("a")) { t.target.blur(); if (n.hasClass("datepickerDisabled")) { return false } var r = e(this).data("datepicker"); var i = n.parent(); var o = i.parent().parent().parent(); var u = e("table", this).index(o.get(0)) - 1; var a = new Date(r.current); var l = false; var c = false; var h = Math.floor(r.calendars / 2); if (i.is("th")) { if (n.hasClass("datepickerMonth")) { a.addMonths(u - h); if (r.mode == "range") { r.date[0] = a.setHours(0, 0, 0, 0).valueOf(); a.addDays(a.getMaxDays() - 1); a.setHours(23, 59, 59, 0); r.date[1] = a.valueOf(); c = true; l = true; r.lastSel = false } else if (r.calendars == 1) { if (o.eq(0).hasClass("datepickerViewDays")) { o.eq(0).toggleClass("datepickerViewDays datepickerViewMonths"); n.find("span").text(a.getFullYear()) } else if (o.eq(0).hasClass("datepickerViewMonths")) { o.eq(0).toggleClass("datepickerViewMonths datepickerViewYears"); n.find("span").text(a.getFullYear() - 6 + " - " + (a.getFullYear() + 5)) } else if (o.eq(0).hasClass("datepickerViewYears")) { o.eq(0).toggleClass("datepickerViewYears datepickerViewDays"); n.find("span").text(a.getMonthName(true) + ", " + a.getFullYear()) } } } else if (i.parent().parent().is("thead")) { if (o.eq(0).hasClass("datepickerViewDays")) { r.current.addMonths(n.hasClass("datepickerGoPrev") ? -1 : 1) } else if (o.eq(0).hasClass("datepickerViewMonths")) { r.current.addYears(n.hasClass("datepickerGoPrev") ? -1 : 1) } else if (o.eq(0).hasClass("datepickerViewYears")) { r.current.addYears(n.hasClass("datepickerGoPrev") ? -12 : 12) } c = true } } else if (i.is("td") && !i.hasClass("datepickerDisabled")) { if (o.eq(0).hasClass("datepickerViewMonths")) { r.current.setMonth(o.find("tbody.datepickerMonths td").index(i)); r.current.setFullYear(parseInt(o.find("thead th a.datepickerMonth span").text(), 10)); r.current.addMonths(h - u); o.eq(0).toggleClass("datepickerViewMonths datepickerViewDays") } else if (o.eq(0).hasClass("datepickerViewYears")) { r.current.setFullYear(parseInt(n.text(), 10)); o.eq(0).toggleClass("datepickerViewYears datepickerViewMonths") } else { var p = parseInt(n.text(), 10); a.addMonths(u - h); if (i.hasClass("datepickerNotInMonth")) { a.addMonths(p > 15 ? -1 : 1) } a.setDate(p); switch (r.mode) { case "multiple": p = a.setHours(0, 0, 0, 0).valueOf(); if (e.inArray(p, r.date) > -1) { e.each(r.date, function (e, t) { if (t == p) { r.date.splice(e, 1); return false } }) } else { r.date.push(p) } break; case "range": if (!r.lastSel) { r.date[0] = a.setHours(0, 0, 0, 0).valueOf() } p = a.setHours(23, 59, 59, 0).valueOf(); if (p < r.date[0]) { r.date[1] = r.date[0] + 86399e3; r.date[0] = p - 86399e3 } else { r.date[1] = p } r.lastSel = !r.lastSel; break; default: r.date = a.valueOf(); break } l = true } c = true } if (c) { s(this) } if (l) { r.onChange.apply(this, f(r)) } } return false }, f = function (t) { var n = null; if (t.mode == "single") { if (t.date) n = new Date(t.date) } else { n = new Array; e(t.date).each(function (e, t) { n.push(new Date(t)) }) } return [n, t.el] }, l = function () { var e = document.compatMode == "CSS1Compat"; return { l: window.pageXOffset || (e ? document.documentElement.scrollLeft : document.body.scrollLeft), t: window.pageYOffset || (e ? document.documentElement.scrollTop : document.body.scrollTop), w: window.innerWidth || (e ? document.documentElement.clientWidth : document.body.clientWidth), h: window.innerHeight || (e ? document.documentElement.clientHeight : document.body.clientHeight) } }, c = function (e, t, n) { if (e == t) { return true } if (e.contains) { return e.contains(t) } if (e.compareDocumentPosition) { return !!(e.compareDocumentPosition(t) & 16) } var r = t.parentNode; while (r && r != n) { if (r == e) return true; r = r.parentNode } return false }, h = function (t) { var n = e("#" + e(this).data("datepickerId")); if (!n.is(":visible")) { var r = n.get(0); var i = n.data("datepicker"); var o = i.onBeforeShow.apply(this, [r]); if (i.onBeforeShow.apply(this, [r]) == false) { return } s(r); var a = e(this).offset(); var f = l(); var c = a.top; var h = a.left; var d = e.curCSS(r, "display"); n.css({ visibility: "hidden", display: "block" }); u(r); switch (i.position) { case "top": c -= r.offsetHeight; break; case "left": h -= r.offsetWidth; break; case "right": h += this.offsetWidth; break; case "bottom": c += this.offsetHeight; break } if (c + r.offsetHeight > f.t + f.h) { c = a.top - r.offsetHeight } if (c < f.t) { c = a.top + this.offsetHeight + r.offsetHeight } if (h + r.offsetWidth > f.l + f.w) { h = a.left - r.offsetWidth } if (h < f.l) { h = a.left + this.offsetWidth } n.css({ visibility: "visible", display: "block", top: c + "px", left: h + "px" }); i.onAfterShow.apply(this, [n.get(0)]); e(document).bind("mousedown", { cal: n, trigger: this }, p) } return false }, p = function (t) { if (t.target != t.data.trigger && !c(t.data.cal.get(0), t.target, t.data.cal.get(0))) { if (t.data.cal.data("datepicker").onBeforeHide.apply(this, [t.data.cal.get(0)]) != false) { t.data.cal.hide(); t.data.cal.data("datepicker").onAfterHide.apply(this, [t.data.cal.get(0)]); e(document).unbind("mousedown", p) } } }, d = function (t, n) { if (t != "single" && !n) n = []; if (n && (!e.isArray(n) || n.length > 0)) { if (t != "single") { if (!e.isArray(n)) { n = [(new Date(n)).setHours(0, 0, 0, 0).valueOf()]; if (t == "range") { n.push((new Date(n[0])).setHours(23, 59, 59, 0).valueOf()) } } else { for (var r = 0; r < n.length; r++) { n[r] = (new Date(n[r])).setHours(0, 0, 0, 0).valueOf() } if (t == "range") { if (n.length == 1) n.push(new Date(n[0])); n[1] = (new Date(n[1])).setHours(23, 59, 59, 0).valueOf() } } } else { n = (new Date(n)).setHours(0, 0, 0, 0).valueOf() } } return n }; return { init: function (t) { t = e.extend({}, i, t || {}); o(t.locale); t.calendars = Math.max(1, parseInt(t.calendars, 10) || 1); t.mode = /single|multiple|range/.test(t.mode) ? t.mode : "single"; return this.each(function () { if (!e(this).data("datepicker")) { t.el = this; t.date = d(t.mode, t.date); if (!t.current) { t.current = new Date } else { t.current = new Date(t.current) } t.current.setDate(1); t.current.setHours(0, 0, 0, 0); var i = "datepicker_" + parseInt(Math.random() * 1e3), o; t.id = i; e(this).data("datepickerId", t.id); var f = e(r.wrapper).attr("id", i).bind("click", a).data("datepicker", t); if (t.className) { f.addClass(t.className) } var l = ""; for (var c = 0; c < t.calendars; c++) { o = t.starts; if (c > 0) { l += r.space } l += tmpl(r.head.join(""), { prev: t.prev, next: t.next, day1: t.locale.daysMin[o++ % 7], day2: t.locale.daysMin[o++ % 7], day3: t.locale.daysMin[o++ % 7], day4: t.locale.daysMin[o++ % 7], day5: t.locale.daysMin[o++ % 7], day6: t.locale.daysMin[o++ % 7], day7: t.locale.daysMin[o++ % 7] }) } f.find("tr:first").append(l).find("table").addClass(n[t.view]); s(f.get(0)); if (t.inline) { f.appendTo(this).show().css("position", "relative"); u(f.get(0)) } else { f.appendTo(document.body); e(this).bind(t.showOn, h) } } }) }, showPicker: function () { return this.each(function () { if (e(this).data("datepickerId")) { var t = e("#" + e(this).data("datepickerId")); var n = t.data("datepicker"); if (!n.inline) { h.apply(this) } } }) }, hidePicker: function () { return this.each(function () { if (e(this).data("datepickerId")) { var t = e("#" + e(this).data("datepickerId")); var n = t.data("datepicker"); if (!n.inline) { e("#" + e(this).data("datepickerId")).hide() } } }) }, setDate: function (t, n) { return this.each(function () { if (e(this).data("datepickerId")) { var r = e("#" + e(this).data("datepickerId")); var i = r.data("datepicker"); i.date = d(i.mode, t); if (n) { i.current = new Date(i.mode != "single" ? i.date[0] : i.date) } s(r.get(0)) } }) }, getDate: function () { if (this.size() > 0) { return f(e("#" + e(this).data("datepickerId")).data("datepicker")) } }, clear: function () { return this.each(function () { if (e(this).data("datepickerId")) { var t = e("#" + e(this).data("datepickerId")); var n = t.data("datepicker"); if (n.mode == "single") { n.date = null } else { n.date = [] } s(t.get(0)) } }) }, fixLayout: function () { return this.each(function () { if (e(this).data("datepickerId")) { var t = e("#" + e(this).data("datepickerId")); var n = t.data("datepicker"); if (n.inline) { u(t.get(0)) } } }) } } }(); e.fn.extend({ DatePicker: t.init, DatePickerHide: t.hidePicker, DatePickerShow: t.showPicker, DatePickerSetDate: t.setDate, DatePickerGetDate: t.getDate, DatePickerClear: t.clear, DatePickerLayout: t.fixLayout }) })(jQuery); (function () { var e = {}; this.tmpl = function t(n, r) { var i = !/\W/.test(n) ? e[n] = e[n] || t(document.getElementById(n).innerHTML) : new Function("obj", "var p=[],print=function(){p.push.apply(p,arguments);};" + "with(obj){p.push('" + n.replace(/[\r\t\n]/g, " ").split("<%").join("	").replace(/((^|%>)[^\t]*)'/g, "$1\r").replace(/\t=(.*?)%>/g, "',$1,'").split("	").join("');").split("%>").join("p.push('").split("\r").join("\\'") + "');}return p.join('');"); return r ? i(r) : i } })();

/*! jQuery Migrate v1.2.1 | (c) 2005, 2013 jQuery Foundation, Inc. and other contributors | jquery.org/license 
http://code.jquery.com/jquery-migrate-1.2.1.min.js */
jQuery.migrateMute === void 0 && (jQuery.migrateMute = !0), function (e, t, n) { function r(n) { var r = t.console; i[n] || (i[n] = !0, e.migrateWarnings.push(n), r && r.warn && !e.migrateMute && (r.warn("JQMIGRATE: " + n), e.migrateTrace && r.trace && r.trace())) } function a(t, a, i, o) { if (Object.defineProperty) try { return Object.defineProperty(t, a, { configurable: !0, enumerable: !0, get: function () { return r(o), i }, set: function (e) { r(o), i = e } }), n } catch (s) { } e._definePropertyBroken = !0, t[a] = i } var i = {}; e.migrateWarnings = [], !e.migrateMute && t.console && t.console.log && t.console.log("JQMIGRATE: Logging is active"), e.migrateTrace === n && (e.migrateTrace = !0), e.migrateReset = function () { i = {}, e.migrateWarnings.length = 0 }, "BackCompat" === document.compatMode && r("jQuery is not compatible with Quirks Mode"); var o = e("<input/>", { size: 1 }).attr("size") && e.attrFn, s = e.attr, u = e.attrHooks.value && e.attrHooks.value.get || function () { return null }, c = e.attrHooks.value && e.attrHooks.value.set || function () { return n }, l = /^(?:input|button)$/i, d = /^[238]$/, p = /^(?:autofocus|autoplay|async|checked|controls|defer|disabled|hidden|loop|multiple|open|readonly|required|scoped|selected)$/i, f = /^(?:checked|selected)$/i; a(e, "attrFn", o || {}, "jQuery.attrFn is deprecated"), e.attr = function (t, a, i, u) { var c = a.toLowerCase(), g = t && t.nodeType; return u && (4 > s.length && r("jQuery.fn.attr( props, pass ) is deprecated"), t && !d.test(g) && (o ? a in o : e.isFunction(e.fn[a]))) ? e(t)[a](i) : ("type" === a && i !== n && l.test(t.nodeName) && t.parentNode && r("Can't change the 'type' of an input or button in IE 6/7/8"), !e.attrHooks[c] && p.test(c) && (e.attrHooks[c] = { get: function (t, r) { var a, i = e.prop(t, r); return i === !0 || "boolean" != typeof i && (a = t.getAttributeNode(r)) && a.nodeValue !== !1 ? r.toLowerCase() : n }, set: function (t, n, r) { var a; return n === !1 ? e.removeAttr(t, r) : (a = e.propFix[r] || r, a in t && (t[a] = !0), t.setAttribute(r, r.toLowerCase())), r } }, f.test(c) && r("jQuery.fn.attr('" + c + "') may use property instead of attribute")), s.call(e, t, a, i)) }, e.attrHooks.value = { get: function (e, t) { var n = (e.nodeName || "").toLowerCase(); return "button" === n ? u.apply(this, arguments) : ("input" !== n && "option" !== n && r("jQuery.fn.attr('value') no longer gets properties"), t in e ? e.value : null) }, set: function (e, t) { var a = (e.nodeName || "").toLowerCase(); return "button" === a ? c.apply(this, arguments) : ("input" !== a && "option" !== a && r("jQuery.fn.attr('value', val) no longer sets properties"), e.value = t, n) } }; var g, h, v = e.fn.init, m = e.parseJSON, y = /^([^<]*)(<[\w\W]+>)([^>]*)$/; e.fn.init = function (t, n, a) { var i; return t && "string" == typeof t && !e.isPlainObject(n) && (i = y.exec(e.trim(t))) && i[0] && ("<" !== t.charAt(0) && r("$(html) HTML strings must start with '<' character"), i[3] && r("$(html) HTML text after last tag is ignored"), "#" === i[0].charAt(0) && (r("HTML string cannot start with a '#' character"), e.error("JQMIGRATE: Invalid selector string (XSS)")), n && n.context && (n = n.context), e.parseHTML) ? v.call(this, e.parseHTML(i[2], n, !0), n, a) : v.apply(this, arguments) }, e.fn.init.prototype = e.fn, e.parseJSON = function (e) { return e || null === e ? m.apply(this, arguments) : (r("jQuery.parseJSON requires a valid JSON string"), null) }, e.uaMatch = function (e) { e = e.toLowerCase(); var t = /(chrome)[ \/]([\w.]+)/.exec(e) || /(webkit)[ \/]([\w.]+)/.exec(e) || /(opera)(?:.*version|)[ \/]([\w.]+)/.exec(e) || /(msie) ([\w.]+)/.exec(e) || 0 > e.indexOf("compatible") && /(mozilla)(?:.*? rv:([\w.]+)|)/.exec(e) || []; return { browser: t[1] || "", version: t[2] || "0" } }, e.browser || (g = e.uaMatch(navigator.userAgent), h = {}, g.browser && (h[g.browser] = !0, h.version = g.version), h.chrome ? h.webkit = !0 : h.webkit && (h.safari = !0), e.browser = h), a(e, "browser", e.browser, "jQuery.browser is deprecated"), e.sub = function () { function t(e, n) { return new t.fn.init(e, n) } e.extend(!0, t, this), t.superclass = this, t.fn = t.prototype = this(), t.fn.constructor = t, t.sub = this.sub, t.fn.init = function (r, a) { return a && a instanceof e && !(a instanceof t) && (a = t(a)), e.fn.init.call(this, r, a, n) }, t.fn.init.prototype = t.fn; var n = t(document); return r("jQuery.sub() is deprecated"), t }, e.ajaxSetup({ converters: { "text json": e.parseJSON } }); var b = e.fn.data; e.fn.data = function (t) { var a, i, o = this[0]; return !o || "events" !== t || 1 !== arguments.length || (a = e.data(o, t), i = e._data(o, t), a !== n && a !== i || i === n) ? b.apply(this, arguments) : (r("Use of jQuery.fn.data('events') is deprecated"), i) }; var j = /\/(java|ecma)script/i, w = e.fn.andSelf || e.fn.addBack; e.fn.andSelf = function () { return r("jQuery.fn.andSelf() replaced by jQuery.fn.addBack()"), w.apply(this, arguments) }, e.clean || (e.clean = function (t, a, i, o) { a = a || document, a = !a.nodeType && a[0] || a, a = a.ownerDocument || a, r("jQuery.clean() is deprecated"); var s, u, c, l, d = []; if (e.merge(d, e.buildFragment(t, a).childNodes), i) for (c = function (e) { return !e.type || j.test(e.type) ? o ? o.push(e.parentNode ? e.parentNode.removeChild(e) : e) : i.appendChild(e) : n }, s = 0; null != (u = d[s]) ; s++) e.nodeName(u, "script") && c(u) || (i.appendChild(u), u.getElementsByTagName !== n && (l = e.grep(e.merge([], u.getElementsByTagName("script")), c), d.splice.apply(d, [s + 1, 0].concat(l)), s += l.length)); return d }); var Q = e.event.add, x = e.event.remove, k = e.event.trigger, N = e.fn.toggle, T = e.fn.live, M = e.fn.die, S = "ajaxStart|ajaxStop|ajaxSend|ajaxComplete|ajaxError|ajaxSuccess", C = RegExp("\\b(?:" + S + ")\\b"), H = /(?:^|\s)hover(\.\S+|)\b/, A = function (t) { return "string" != typeof t || e.event.special.hover ? t : (H.test(t) && r("'hover' pseudo-event is deprecated, use 'mouseenter mouseleave'"), t && t.replace(H, "mouseenter$1 mouseleave$1")) }; e.event.props && "attrChange" !== e.event.props[0] && e.event.props.unshift("attrChange", "attrName", "relatedNode", "srcElement"), e.event.dispatch && a(e.event, "handle", e.event.dispatch, "jQuery.event.handle is undocumented and deprecated"), e.event.add = function (e, t, n, a, i) { e !== document && C.test(t) && r("AJAX events should be attached to document: " + t), Q.call(this, e, A(t || ""), n, a, i) }, e.event.remove = function (e, t, n, r, a) { x.call(this, e, A(t) || "", n, r, a) }, e.fn.error = function () { var e = Array.prototype.slice.call(arguments, 0); return r("jQuery.fn.error() is deprecated"), e.splice(0, 0, "error"), arguments.length ? this.bind.apply(this, e) : (this.triggerHandler.apply(this, e), this) }, e.fn.toggle = function (t, n) { if (!e.isFunction(t) || !e.isFunction(n)) return N.apply(this, arguments); r("jQuery.fn.toggle(handler, handler...) is deprecated"); var a = arguments, i = t.guid || e.guid++, o = 0, s = function (n) { var r = (e._data(this, "lastToggle" + t.guid) || 0) % o; return e._data(this, "lastToggle" + t.guid, r + 1), n.preventDefault(), a[r].apply(this, arguments) || !1 }; for (s.guid = i; a.length > o;) a[o++].guid = i; return this.click(s) }, e.fn.live = function (t, n, a) { return r("jQuery.fn.live() is deprecated"), T ? T.apply(this, arguments) : (e(this.context).on(t, this.selector, n, a), this) }, e.fn.die = function (t, n) { return r("jQuery.fn.die() is deprecated"), M ? M.apply(this, arguments) : (e(this.context).off(t, this.selector || "**", n), this) }, e.event.trigger = function (e, t, n, a) { return n || C.test(e) || r("Global events are undocumented and deprecated"), k.call(this, e, t, n || document, a) }, e.each(S.split("|"), function (t, n) { e.event.special[n] = { setup: function () { var t = this; return t !== document && (e.event.add(document, n + "." + e.guid, function () { e.event.trigger(n, null, t, !0) }), e._data(this, n, e.guid++)), !1 }, teardown: function () { return this !== document && e.event.remove(document, n + "." + e._data(this, n)), !1 } } }) }(jQuery, window);


(function (e) {
    /*Outside Click Reset Plugin*/e.fn.outside = function (t, n) { return this.each(function () { var r = e(this), i = this; e(document).bind(t, function s(r) { if (r.target !== i && !e.contains(i, r.target)) { n.apply(i, [r]); if (!i.parentNode) e(document.body).unbind(t, s) } }) }) }
    /*X-Tooltip Plugin*/e.fn.xtooltip = function (t) { var n = { fadedelay: 300, custom_class: "xdefault" }; var t = e.extend(n, t); return this.each(function () { var n = t, r = e(this); r.delegate("[data-tooltip]", "mouseover", function () { var t = e(this), r = t.attr("data-tooltip"), i = t.attr("data-tooltip-offset"); e(".xtooltip").remove(); e("body").append('<div class="xtooltip ' + n.custom_class + '">' + r + "</div>"); var s = t.offset().top + t.outerHeight() - e(window).scrollTop(), u = t.offset().left, u = t.offset().left - e(".xtooltip").outerWidth() / 2 + t.outerWidth() / 2; if (i == "top") { s = s - e(".xtooltip").outerHeight() - t.outerHeight(); e(".xtooltip").addClass("tooltop") } else if (i == "left" || i == "right") { s = s - e(".xtooltip").outerHeight() / 2 - t.outerHeight() / 2; if (i == "left") { u = u - e(".xtooltip").outerWidth() / 2 - t.outerWidth() / 2; e(".xtooltip").addClass("toolleft") } else if (i == "right") { u = u + e(".xtooltip").outerWidth() / 2 + t.outerWidth() / 2; e(".xtooltip").addClass("toolright") } } if (t.attr("data-tooltip-hposition")) { var a = parseInt(t.attr("data-tooltip-hposition")); a = parseInt(a / 100 * (t.outerWidth() / 2)); u = u + a } if (t.attr("data-tooltip-vposition")) { var f = parseInt(t.attr("data-tooltip-vposition")); f = parseInt(f / 100 * t.outerHeight() / 2); s = s + f } e(".xtooltip").css({ left: u + "px", top: s + "px" }).fadeIn(n.fadedelay) }); r.delegate("[data-tooltip]", "mouseout", function () { e(".xtooltip").stop(true, true).fadeOut(0, "", function () { e(this).remove() }) }) }) }
})(jQuery);


//Tags Input

/*

	jQuery Tags Input Plugin 1.3.3
	
	Copyright (c) 2011 XOXCO, Inc
	
	Documentation for this plugin lives here:
	http://xoxco.com/clickable/jquery-tags-input
	
	Licensed under the MIT license:
	http://www.opensource.org/licenses/mit-license.php

	ben@xoxco.com

*/

(function ($) {

    var delimiter = new Array();
    var tags_callbacks = new Array();
    $.fn.doAutosize = function (o) {
        var minWidth = $(this).data('minwidth'),
	        maxWidth = $(this).data('maxwidth'),
	        val = '',
	        input = $(this),
	        testSubject = $('#' + $(this).data('tester_id'));

        if (val === (val = input.val())) { return; }

        // Enter new content into testSubject
        var escaped = val.replace(/&/g, '&amp;').replace(/\s/g, ' ').replace(/</g, '&lt;').replace(/>/g, '&gt;');
        testSubject.html(escaped);
        // Calculate new width + whether to change
        var testerWidth = testSubject.width(),
	        newWidth = (testerWidth + o.comfortZone) >= minWidth ? testerWidth + o.comfortZone : minWidth,
	        currentWidth = input.width(),
	        isValidWidthChange = (newWidth < currentWidth && newWidth >= minWidth)
	                             || (newWidth > minWidth && newWidth < maxWidth);

        // Animate width
        if (isValidWidthChange) {
            input.width(newWidth);
        }


    };
    $.fn.resetAutosize = function (options) {
        // alert(JSON.stringify(options));
        var minWidth = $(this).data('minwidth') || options.minInputWidth || $(this).width(),
            maxWidth = $(this).data('maxwidth') || options.maxInputWidth || ($(this).closest('.tagsinput').width() - options.inputPadding),
            val = '',
            input = $(this),
            testSubject = $('<tester/>').css({
                position: 'absolute',
                top: -9999,
                left: -9999,
                width: 'auto',
                fontSize: input.css('fontSize'),
                fontFamily: input.css('fontFamily'),
                fontWeight: input.css('fontWeight'),
                letterSpacing: input.css('letterSpacing'),
                whiteSpace: 'nowrap'
            }),
            testerId = $(this).attr('id') + '_autosize_tester';
        if (!$('#' + testerId).length > 0) {
            testSubject.attr('id', testerId);
            testSubject.appendTo('body');
        }

        input.data('minwidth', minWidth);
        input.data('maxwidth', maxWidth);
        input.data('tester_id', testerId);
        input.css('width', minWidth);
    };

    $.fn.addTag = function (value, options) {
        options = jQuery.extend({ focus: false, callback: true }, options);
        this.each(function () {
            var id = $(this).attr('id');

            var tagslist = $(this).val().split(delimiter[id]);
            if (tagslist[0] == '') {
                tagslist = new Array();
            }

            value = jQuery.trim(value);

            if (options.unique) {
                var skipTag = $(this).tagExist(value);
                if (skipTag == true) {
                    //Marks fake input as not_valid to let styling it
                    $('#' + id + '_tag').addClass('not_valid');
                }
            } else {
                var skipTag = false;
            }

            if (value != '' && skipTag != true) {
                $('<span>').addClass('tag').append(
                    $('<span>').text(value).append('&nbsp;&nbsp;'),
                    $('<a>', {
                        href: '#',
                        title: 'Removing tag',
                        text: 'x'
                    }).click(function () {
                        return $('#' + id).removeTag(escape(value));
                    })
                ).insertBefore('#' + id + '_addTag');

                tagslist.push(value);

                $('#' + id + '_tag').val('');
                if (options.focus) {
                    $('#' + id + '_tag').focus();
                } else {
                    $('#' + id + '_tag').blur();
                }

                $.fn.tagsInput.updateTagsField(this, tagslist);

                if (options.callback && tags_callbacks[id] && tags_callbacks[id]['onAddTag']) {
                    var f = tags_callbacks[id]['onAddTag'];
                    f.call(this, value);
                }
                if (tags_callbacks[id] && tags_callbacks[id]['onChange']) {
                    var i = tagslist.length;
                    var f = tags_callbacks[id]['onChange'];
                    f.call(this, $(this), tagslist[i - 1]);
                }
            }

        });

        return false;
    };

    $.fn.removeTag = function (value) {
        value = unescape(value);
        this.each(function () {
            var id = $(this).attr('id');

            var old = $(this).val().split(delimiter[id]);

            $('#' + id + '_tagsinput .tag').remove();
            str = '';
            for (i = 0; i < old.length; i++) {
                if (old[i] != value) {
                    str = str + delimiter[id] + old[i];
                }
            }

            $.fn.tagsInput.importTags(this, str);

            if (tags_callbacks[id] && tags_callbacks[id]['onRemoveTag']) {
                var f = tags_callbacks[id]['onRemoveTag'];
                f.call(this, value);
            }
        });

        return false;
    };

    $.fn.tagExist = function (val) {
        var id = $(this).attr('id');
        var tagslist = $(this).val().split(delimiter[id]);
        return (jQuery.inArray(val, tagslist) >= 0); //true when tag exists, false when not
    };

    // clear all existing tags and import new ones from a string
    $.fn.importTags = function (str) {
        id = $(this).attr('id');
        $('#' + id + '_tagsinput .tag').remove();
        $.fn.tagsInput.importTags(this, str);
    }

    $.fn.tagsInput = function (options) {
        var settings = jQuery.extend({
            interactive: true,
            defaultText: 'Enter Tags...',
            minChars: 0,
            width: '300px',
            height: '87px',
            autocomplete: { selectFirst: false },
            'hide': true,
            'delimiter': ',',
            'unique': true,
            removeWithBackspace: true,
            placeholderColor: '#b4b4b4',
            autosize: true,
            comfortZone: 20,
            inputPadding: 6 * 2,
            autocomplete_url: 'assets/js/fake_json_endpoint.html'
        }, options);

        this.each(function () {
            if (settings.hide) {
                $(this).hide();
            }
            var id = $(this).attr('id');
            if (!id || delimiter[$(this).attr('id')]) {
                id = $(this).attr('id', 'tags' + new Date().getTime()).attr('id');
            }

            var data = jQuery.extend({
                pid: id,
                real_input: '#' + id,
                holder: '#' + id + '_tagsinput',
                input_wrapper: '#' + id + '_addTag',
                fake_input: '#' + id + '_tag'
            }, settings);

            delimiter[id] = data.delimiter;

            if (settings.onAddTag || settings.onRemoveTag || settings.onChange) {
                tags_callbacks[id] = new Array();
                tags_callbacks[id]['onAddTag'] = settings.onAddTag;
                tags_callbacks[id]['onRemoveTag'] = settings.onRemoveTag;
                tags_callbacks[id]['onChange'] = settings.onChange;
            }

            var markup = '<div id="' + id + '_tagsinput" class="tagsinput"><div id="' + id + '_addTag">';

            if (settings.interactive) {
                markup = markup + '<input id="' + id + '_tag" value="" data-default="' + settings.defaultText + '" />';
            }

            markup = markup + '</div><div class="tags_clear"></div></div>';

            $(markup).insertAfter(this);

            $(data.holder).css('width', settings.width);
            $(data.holder).css('min-height', settings.height);

            if ($(data.real_input).val() != '') {
                $.fn.tagsInput.importTags($(data.real_input), $(data.real_input).val());
            }
            if (settings.interactive) {
                $(data.fake_input).val($(data.fake_input).attr('data-default'));
                $(data.fake_input).css('color', settings.placeholderColor);
                $(data.fake_input).resetAutosize(settings);

                $(data.holder).bind('click', data, function (event) {
                    $(event.data.fake_input).focus();
                });

                $(data.fake_input).bind('focus', data, function (event) {
                    if ($(event.data.fake_input).val() == $(event.data.fake_input).attr('data-default')) {
                        $(event.data.fake_input).val('');
                    }
                    $(event.data.fake_input).css('color', '#b4b4b4');
                });
                $(data.fake_input).bind('blur', data, function (event) {
                    $(event.data.fake_input).val('Enter Tags...');
                });

                if (settings.autocomplete_url != undefined) {
                    autocomplete_options = { source: settings.autocomplete_url };
                    for (attrname in settings.autocomplete) {
                        autocomplete_options[attrname] = settings.autocomplete[attrname];
                    }

                    if (jQuery.Autocompleter !== undefined) {
                        $(data.fake_input).autocomplete(settings.autocomplete_url, settings.autocomplete);
                        $(data.fake_input).bind('result', data, function (event, data, formatted) {
                            if (data) {
                                $('#' + id).addTag(data[0] + "", { focus: true, unique: (settings.unique) });
                            }
                        });
                    } else if (jQuery.ui.autocomplete !== undefined) {
                        $(data.fake_input).autocomplete(autocomplete_options);
                        $(data.fake_input).bind('autocompleteselect', data, function (event, ui) {
                            $(event.data.real_input).addTag(ui.item.value, { focus: true, unique: (settings.unique) });
                            return false;
                        });
                    }


                } else {
                    // if a user tabs out of the field, create a new tag
                    // this is only available if autocomplete is not used.
                    $(data.fake_input).bind('blur', data, function (event) {
                        var d = $(this).attr('data-default');
                        if ($(event.data.fake_input).val() != '' && $(event.data.fake_input).val() != d) {
                            if ((event.data.minChars <= $(event.data.fake_input).val().length) && (!event.data.maxChars || (event.data.maxChars >= $(event.data.fake_input).val().length)))
                                $(event.data.real_input).addTag($(event.data.fake_input).val(), { focus: true, unique: (settings.unique) });
                        } else {
                            $(event.data.fake_input).val($(event.data.fake_input).attr('data-default'));
                            $(event.data.fake_input).css('color', settings.placeholderColor);
                        }
                        return false;
                    });

                }
                // if user types a comma, create a new tag
                $(data.fake_input).bind('keypress', data, function (event) {
                    if (event.which == event.data.delimiter.charCodeAt(0) || event.which == 13) {
                        event.preventDefault();
                        if ((event.data.minChars <= $(event.data.fake_input).val().length) && (!event.data.maxChars || (event.data.maxChars >= $(event.data.fake_input).val().length)))
                            $(event.data.real_input).addTag($(event.data.fake_input).val(), { focus: true, unique: (settings.unique) });
                        $(event.data.fake_input).resetAutosize(settings);
                        return false;
                    } else if (event.data.autosize) {
                        $(event.data.fake_input).doAutosize(settings);

                    }
                });
                //Delete last tag on backspace
                data.removeWithBackspace && $(data.fake_input).bind('keydown', function (event) {
                    if (event.keyCode == 8 && $(this).val() == '') {
                        event.preventDefault();
                        var last_tag = $(this).closest('.tagsinput').find('.tag:last').text();
                        var id = $(this).attr('id').replace(/_tag$/, '');
                        last_tag = last_tag.replace(/[\s]+x$/, '');
                        $('#' + id).removeTag(escape(last_tag));
                        $(this).trigger('focus');
                    }
                });
                $(data.fake_input).blur();

                //Removes the not_valid class when user changes the value of the fake input
                if (data.unique) {
                    $(data.fake_input).keydown(function (event) {
                        if (event.keyCode == 8 || String.fromCharCode(event.which).match(/\w+|[áéíóúÁÉÍÓÚñÑ,/]+/)) {
                            $(this).removeClass('not_valid');
                        }
                    });
                }
            } // if settings.interactive
        });

        return this;

    };

    $.fn.tagsInput.updateTagsField = function (obj, tagslist) {
        var id = $(obj).attr('id');
        $(obj).val(tagslist.join(delimiter[id]));
    };

    $.fn.tagsInput.importTags = function (obj, val) {
        $(obj).val('');
        var id = $(obj).attr('id');
        var tags = val.split(delimiter[id]);
        for (i = 0; i < tags.length; i++) {
            $(obj).addTag(tags[i], { focus: false, callback: false });
        }
        if (tags_callbacks[id] && tags_callbacks[id]['onChange']) {
            var f = tags_callbacks[id]['onChange'];
            f.call(obj, obj, tags[i]);
        }
    };

})(jQuery);

