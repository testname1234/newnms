﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using PcrConfiguration.Core.Entities;

namespace PcrConfiguration.Core.DataInterfaces
{
		
	public interface IDeviceRepositoryBase
	{
        
        Dictionary<string, string> GetDeviceBasicSearchColumns();
        List<SearchColumn> GetDeviceSearchColumns();
        List<SearchColumn> GetDeviceAdvanceSearchColumns();
        

		List<Device> GetDeviceByGroupId(System.Int32 GroupId,string SelectClause=null);
		List<Device> GetDeviceByDeviceStatusId(System.Int32 DeviceStatusId,string SelectClause=null);
		List<Device> GetDeviceByDeviceTypeId(System.Int32 DeviceTypeId,string SelectClause=null);
		Device GetDevice(System.Int32 DeviceId,string SelectClause=null);
		Device UpdateDevice(Device entity);
		bool DeleteDevice(System.Int32 DeviceId);
		Device DeleteDevice(Device entity);
		List<Device> GetPagedDevice(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<Device> GetAllDevice(string SelectClause=null);
		Device InsertDevice(Device entity);
		List<Device> GetDeviceByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);	}
	
	
}
