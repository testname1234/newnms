﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using PcrConfiguration.Core.Entities;

namespace PcrConfiguration.Core.DataInterfaces
{
		
	public interface IDeviceStatusHistoryRepositoryBase
	{
        
        Dictionary<string, string> GetDeviceStatusHistoryBasicSearchColumns();
        List<SearchColumn> GetDeviceStatusHistorySearchColumns();
        List<SearchColumn> GetDeviceStatusHistoryAdvanceSearchColumns();
        

		List<DeviceStatusHistory> GetDeviceStatusHistoryByDeviceId(System.Int32 DeviceId,string SelectClause=null);
		List<DeviceStatusHistory> GetDeviceStatusHistoryByDeviceStatusId(System.Int32 DeviceStatusId,string SelectClause=null);
		List<DeviceStatusHistory> GetDeviceStatusHistoryByGroupId(System.Int32 GroupId,string SelectClause=null);
		DeviceStatusHistory GetDeviceStatusHistory(System.Int32 DeviceStatusHistoryId,string SelectClause=null);
		DeviceStatusHistory UpdateDeviceStatusHistory(DeviceStatusHistory entity);
		bool DeleteDeviceStatusHistory(System.Int32 DeviceStatusHistoryId);
		DeviceStatusHistory DeleteDeviceStatusHistory(DeviceStatusHistory entity);
		List<DeviceStatusHistory> GetPagedDeviceStatusHistory(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<DeviceStatusHistory> GetAllDeviceStatusHistory(string SelectClause=null);
		DeviceStatusHistory InsertDeviceStatusHistory(DeviceStatusHistory entity);
		List<DeviceStatusHistory> GetDeviceStatusHistoryByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);	}
	
	
}
