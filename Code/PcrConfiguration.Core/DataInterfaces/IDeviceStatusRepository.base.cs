﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using PcrConfiguration.Core.Entities;

namespace PcrConfiguration.Core.DataInterfaces
{
		
	public interface IDeviceStatusRepositoryBase
	{
        
        Dictionary<string, string> GetDeviceStatusBasicSearchColumns();
        List<SearchColumn> GetDeviceStatusSearchColumns();
        List<SearchColumn> GetDeviceStatusAdvanceSearchColumns();
        

		DeviceStatus GetDeviceStatus(System.Int32 DeviceStatusId,string SelectClause=null);
		DeviceStatus UpdateDeviceStatus(DeviceStatus entity);
		bool DeleteDeviceStatus(System.Int32 DeviceStatusId);
		DeviceStatus DeleteDeviceStatus(DeviceStatus entity);
		List<DeviceStatus> GetPagedDeviceStatus(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<DeviceStatus> GetAllDeviceStatus(string SelectClause=null);
		DeviceStatus InsertDeviceStatus(DeviceStatus entity);
		List<DeviceStatus> GetDeviceStatusByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);	}
	
	
}
