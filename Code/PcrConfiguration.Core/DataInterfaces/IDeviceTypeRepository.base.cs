﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using PcrConfiguration.Core.Entities;

namespace PcrConfiguration.Core.DataInterfaces
{
		
	public interface IDeviceTypeRepositoryBase
	{
        
        Dictionary<string, string> GetDeviceTypeBasicSearchColumns();
        List<SearchColumn> GetDeviceTypeSearchColumns();
        List<SearchColumn> GetDeviceTypeAdvanceSearchColumns();
        

		DeviceType GetDeviceType(System.Int32 DeviceTypeId,string SelectClause=null);
		DeviceType UpdateDeviceType(DeviceType entity);
		bool DeleteDeviceType(System.Int32 DeviceTypeId);
		DeviceType DeleteDeviceType(DeviceType entity);
		List<DeviceType> GetPagedDeviceType(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<DeviceType> GetAllDeviceType(string SelectClause=null);
		DeviceType InsertDeviceType(DeviceType entity);
		List<DeviceType> GetDeviceTypeByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);	}
	
	
}
