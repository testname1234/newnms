﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using PcrConfiguration.Core.Entities;

namespace PcrConfiguration.Core.DataInterfaces
{
		
	public interface IGroupRepositoryBase
	{
        
        Dictionary<string, string> GetGroupBasicSearchColumns();
        List<SearchColumn> GetGroupSearchColumns();
        List<SearchColumn> GetGroupAdvanceSearchColumns();
        

		Group GetGroup(System.Int32 GroupId,string SelectClause=null);
		Group UpdateGroup(Group entity);
		bool DeleteGroup(System.Int32 GroupId);
		Group DeleteGroup(Group entity);
		List<Group> GetPagedGroup(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<Group> GetAllGroup(string SelectClause=null);
		Group InsertGroup(Group entity);
		List<Group> GetGroupByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);	}
	
	
}
