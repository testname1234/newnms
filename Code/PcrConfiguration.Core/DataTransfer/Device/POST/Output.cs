﻿using System;
using System.Runtime.Serialization;
using Validation;

namespace PcrConfiguration.Core.DataTransfer.Device
{
    [DataContract]
	public class PostOutput
	{
			
		[DataMember (EmitDefaultValue=false)]
		public System.Int32 DeviceId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.String Name{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.String DeviceKey{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Boolean IsMosDevice{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.String Ip{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32 Port{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32 GroupId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32 DeviceStatusId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32 DeviceTypeId{ get; set; }

		[IgnoreDataMember]
		public System.DateTime LastHeartBeatTime{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public string LastHeartBeatTimeStr
		{
			 get { return LastHeartBeatTime.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { LastHeartBeatTime = date.ToUniversalTime();  }  } 
		}

		[IgnoreDataMember]
		public System.DateTime LastUpdatedDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public string LastUpdatedDateStr
		{
			 get { return LastUpdatedDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { LastUpdatedDate = date.ToUniversalTime();  }  } 
		}

		[IgnoreDataMember]
		public System.DateTime CreationDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public string CreationDateStr
		{
			 get { return CreationDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime();  }  } 
		}

	}	
}
