﻿using System;
using System.Runtime.Serialization;
using Validation;

namespace PcrConfiguration.Core.DataTransfer.DeviceStatus
{
    [DataContract]
	public class PutInput
	{
			
		[FieldTypeValidation(DataType=DataTypes.Integer)]
		[FieldNullable(IsNullable = false)]
		[DataMember (EmitDefaultValue=false)]
		public string DeviceStatusId{ get; set; }

		[FieldLength(MaxLength = 30)]
		[DataMember (EmitDefaultValue=false)]
		public string Name{ get; set; }

	}	
}
