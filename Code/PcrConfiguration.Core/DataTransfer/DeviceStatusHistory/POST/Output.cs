﻿using System;
using System.Runtime.Serialization;
using Validation;

namespace PcrConfiguration.Core.DataTransfer.DeviceStatusHistory
{
    [DataContract]
	public class PostOutput
	{
			
		[DataMember (EmitDefaultValue=false)]
		public System.Int32 DeviceStatusHistoryId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32 DeviceId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32 DeviceStatusId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32 GroupId{ get; set; }

		[IgnoreDataMember]
		public System.DateTime CreationDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public string CreationDateStr
		{
			 get { return CreationDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime();  }  } 
		}

	}	
}
