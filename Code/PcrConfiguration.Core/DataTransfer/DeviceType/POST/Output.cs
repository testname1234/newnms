﻿using System;
using System.Runtime.Serialization;
using Validation;

namespace PcrConfiguration.Core.DataTransfer.DeviceType
{
    [DataContract]
	public class PostOutput
	{
			
		[DataMember (EmitDefaultValue=false)]
		public System.Int32 DeviceTypeId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.String Name{ get; set; }

	}	
}
