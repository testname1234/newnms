﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace PcrConfiguration.Core.Entities
{
    [DataContract]
	public abstract partial class DeviceBase:EntityBase, IEquatable<DeviceBase>
	{
			
		[PrimaryKey]
		[FieldNameAttribute("DeviceId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 DeviceId{ get; set; }

		[FieldNameAttribute("Name",false,false,50)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Name{ get; set; }

		[FieldNameAttribute("DeviceKey",false,false,25)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String DeviceKey{ get; set; }

		[FieldNameAttribute("IsMOSDevice",false,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean IsMosDevice{ get; set; }

		[FieldNameAttribute("IP",false,false,15)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Ip{ get; set; }

		[FieldNameAttribute("Port",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 Port{ get; set; }

		[FieldNameAttribute("GroupId",false,true,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 GroupId{ get; set; }

		[FieldNameAttribute("DeviceStatusId",false,true,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 DeviceStatusId{ get; set; }

		[FieldNameAttribute("DeviceTypeId",false,true,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 DeviceTypeId{ get; set; }

		[FieldNameAttribute("LastHeartBeatTime",false,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime LastHeartBeatTime{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string LastHeartBeatTimeStr
		{
			 get { return LastHeartBeatTime.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { LastHeartBeatTime = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("LastUpdatedDate",false,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime LastUpdatedDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string LastUpdatedDateStr
		{
			 get { return LastUpdatedDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { LastUpdatedDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("CreationDate",false,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime CreationDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string CreationDateStr
		{
			 get { return CreationDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime();  }  } 
		}

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<DeviceBase> Members

        public virtual bool Equals(DeviceBase other)
        {
			if(this.DeviceId==other.DeviceId  && this.Name==other.Name  && this.DeviceKey==other.DeviceKey  && this.IsMosDevice==other.IsMosDevice  && this.Ip==other.Ip  && this.Port==other.Port  && this.GroupId==other.GroupId  && this.DeviceStatusId==other.DeviceStatusId  && this.DeviceTypeId==other.DeviceTypeId  && this.LastHeartBeatTime==other.LastHeartBeatTime  && this.LastUpdatedDate==other.LastUpdatedDate  && this.CreationDate==other.CreationDate )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(Device other)
        {
			if(other!=null)
			{
				this.DeviceId=other.DeviceId;
				this.Name=other.Name;
				this.DeviceKey=other.DeviceKey;
				this.IsMosDevice=other.IsMosDevice;
				this.Ip=other.Ip;
				this.Port=other.Port;
				this.GroupId=other.GroupId;
				this.DeviceStatusId=other.DeviceStatusId;
				this.DeviceTypeId=other.DeviceTypeId;
				this.LastHeartBeatTime=other.LastHeartBeatTime;
				this.LastUpdatedDate=other.LastUpdatedDate;
				this.CreationDate=other.CreationDate;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
