﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace PcrConfiguration.Core.Entities
{
    [DataContract]
	public abstract partial class DeviceStatusBase:EntityBase, IEquatable<DeviceStatusBase>
	{
			
		[PrimaryKey]
		[FieldNameAttribute("DeviceStatusId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 DeviceStatusId{ get; set; }

		[FieldNameAttribute("Name",false,false,30)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Name{ get; set; }

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<DeviceStatusBase> Members

        public virtual bool Equals(DeviceStatusBase other)
        {
			if(this.DeviceStatusId==other.DeviceStatusId  && this.Name==other.Name )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(DeviceStatus other)
        {
			if(other!=null)
			{
				this.DeviceStatusId=other.DeviceStatusId;
				this.Name=other.Name;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
