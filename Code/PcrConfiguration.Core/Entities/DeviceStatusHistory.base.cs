﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace PcrConfiguration.Core.Entities
{
    [DataContract]
	public abstract partial class DeviceStatusHistoryBase:EntityBase, IEquatable<DeviceStatusHistoryBase>
	{
			
		[PrimaryKey]
		[FieldNameAttribute("DeviceStatusHistoryId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 DeviceStatusHistoryId{ get; set; }

		[FieldNameAttribute("DeviceId",false,true,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 DeviceId{ get; set; }

		[FieldNameAttribute("DeviceStatusId",false,true,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 DeviceStatusId{ get; set; }

		[FieldNameAttribute("GroupId",false,true,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 GroupId{ get; set; }

		[FieldNameAttribute("CreationDate",false,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime CreationDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string CreationDateStr
		{
			 get { return CreationDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime();  }  } 
		}

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<DeviceStatusHistoryBase> Members

        public virtual bool Equals(DeviceStatusHistoryBase other)
        {
			if(this.DeviceStatusHistoryId==other.DeviceStatusHistoryId  && this.DeviceId==other.DeviceId  && this.DeviceStatusId==other.DeviceStatusId  && this.GroupId==other.GroupId  && this.CreationDate==other.CreationDate )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(DeviceStatusHistory other)
        {
			if(other!=null)
			{
				this.DeviceStatusHistoryId=other.DeviceStatusHistoryId;
				this.DeviceId=other.DeviceId;
				this.DeviceStatusId=other.DeviceStatusId;
				this.GroupId=other.GroupId;
				this.CreationDate=other.CreationDate;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
