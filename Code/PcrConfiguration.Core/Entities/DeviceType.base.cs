﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace PcrConfiguration.Core.Entities
{
    [DataContract]
	public abstract partial class DeviceTypeBase:EntityBase, IEquatable<DeviceTypeBase>
	{
			
		[PrimaryKey]
		[FieldNameAttribute("DeviceTypeId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 DeviceTypeId{ get; set; }

		[FieldNameAttribute("Name",false,false,30)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Name{ get; set; }

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<DeviceTypeBase> Members

        public virtual bool Equals(DeviceTypeBase other)
        {
			if(this.DeviceTypeId==other.DeviceTypeId  && this.Name==other.Name )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(DeviceType other)
        {
			if(other!=null)
			{
				this.DeviceTypeId=other.DeviceTypeId;
				this.Name=other.Name;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
