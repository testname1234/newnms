﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace PcrConfiguration.Core.Entities
{
    [DataContract]
	public abstract partial class GroupBase:EntityBase, IEquatable<GroupBase>
	{
			
		[PrimaryKey]
		[FieldNameAttribute("GroupId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 GroupId{ get; set; }

		[FieldNameAttribute("Name",false,false,50)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Name{ get; set; }

		[FieldNameAttribute("GroupKey",false,false,25)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String GroupKey{ get; set; }

		[FieldNameAttribute("ParentId",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? ParentId{ get; set; }

		[FieldNameAttribute("CreationDate",false,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime CreationDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string CreationDateStr
		{
			 get { return CreationDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime();  }  } 
		}

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<GroupBase> Members

        public virtual bool Equals(GroupBase other)
        {
			if(this.GroupId==other.GroupId  && this.Name==other.Name  && this.GroupKey==other.GroupKey  && this.ParentId==other.ParentId  && this.CreationDate==other.CreationDate )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(Group other)
        {
			if(other!=null)
			{
				this.GroupId=other.GroupId;
				this.Name=other.Name;
				this.GroupKey=other.GroupKey;
				this.ParentId=other.ParentId;
				this.CreationDate=other.CreationDate;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
