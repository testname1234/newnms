﻿using System;


namespace PcrConfiguration.Core.Enum
{
    public enum DeviceStatus
    {
        Alive=1,
        Dead=2,
        NotAcknowledging=3,
        NotResponding=4,
        None=5
    }
}
