﻿using System;
using PcrConfiguration.Core.Entities;
using PcrConfiguration.Core.Entities;

namespace PcrConfiguration.Core.Extensions
{
    public static class OperandExtension        
    {
        public static string ToOperandString(this Operands operand)
        {
            return  getOperandString(operand);
        }

        private static string getOperandString(Operands operand)
        {
            if (operand.Equals(Operands.Equal))
            {
                return "=";
            }
            else if (operand.Equals(Operands.GreaterEqual))
            {
                return ">=";
            }
            else if (operand.Equals(Operands.LesserEqual))
            {
                return "<=";
            }
            else if (operand.Equals(Operands.Greater))
            {
                return ">";
            }
            else if (operand.Equals(Operands.Lesser))
            {
                return "<";
            }
            else if (operand.Equals(Operands.Like))
            {
                return "like";
            }
            else if (operand.Equals(Operands.Between))
            {
                return "between";
            }
            else if (operand.Equals(Operands.Is))
            {
                return "is";
            }
            else
            {
                return "=";
            }
        }
    }
}
