﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using PcrConfiguration.Core.Entities;
using PcrConfiguration.Core.DataTransfer;
using PcrConfiguration.Core.DataTransfer.Device;

namespace PcrConfiguration.Core.IService
{
		
	public interface IDeviceService
	{
        Dictionary<string, string> GetDeviceBasicSearchColumns();
        
        List<SearchColumn> GetDeviceAdvanceSearchColumns();

		List<Device> GetDeviceByGroupId(System.Int32 GroupId);
		List<Device> GetDeviceByDeviceStatusId(System.Int32 DeviceStatusId);
		List<Device> GetDeviceByDeviceTypeId(System.Int32 DeviceTypeId);
		Device GetDevice(System.Int32 DeviceId);
		DataTransfer<List<GetOutput>> GetAll();
		Device UpdateDevice(Device entity);
		bool DeleteDevice(System.Int32 DeviceId);
		List<Device> GetAllDevice();
		Device InsertDevice(Device entity);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);
	}
	
	
}
