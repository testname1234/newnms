﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using PcrConfiguration.Core.Entities;
using PcrConfiguration.Core.DataTransfer;
using PcrConfiguration.Core.DataTransfer.DeviceStatusHistory;

namespace PcrConfiguration.Core.IService
{
		
	public interface IDeviceStatusHistoryService
	{
        Dictionary<string, string> GetDeviceStatusHistoryBasicSearchColumns();
        
        List<SearchColumn> GetDeviceStatusHistoryAdvanceSearchColumns();

		List<DeviceStatusHistory> GetDeviceStatusHistoryByDeviceId(System.Int32 DeviceId);
		List<DeviceStatusHistory> GetDeviceStatusHistoryByDeviceStatusId(System.Int32 DeviceStatusId);
		List<DeviceStatusHistory> GetDeviceStatusHistoryByGroupId(System.Int32 GroupId);
		DeviceStatusHistory GetDeviceStatusHistory(System.Int32 DeviceStatusHistoryId);
		DataTransfer<List<GetOutput>> GetAll();
		DeviceStatusHistory UpdateDeviceStatusHistory(DeviceStatusHistory entity);
		bool DeleteDeviceStatusHistory(System.Int32 DeviceStatusHistoryId);
		List<DeviceStatusHistory> GetAllDeviceStatusHistory();
		DeviceStatusHistory InsertDeviceStatusHistory(DeviceStatusHistory entity);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);
	}
	
	
}
