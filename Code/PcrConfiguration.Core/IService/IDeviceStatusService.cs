﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using PcrConfiguration.Core.Entities;
using PcrConfiguration.Core.DataTransfer;
using PcrConfiguration.Core.DataTransfer.DeviceStatus;

namespace PcrConfiguration.Core.IService
{
		
	public interface IDeviceStatusService
	{
        Dictionary<string, string> GetDeviceStatusBasicSearchColumns();
        
        List<SearchColumn> GetDeviceStatusAdvanceSearchColumns();

		DeviceStatus GetDeviceStatus(System.Int32 DeviceStatusId);
		DataTransfer<List<GetOutput>> GetAll();
		DeviceStatus UpdateDeviceStatus(DeviceStatus entity);
		bool DeleteDeviceStatus(System.Int32 DeviceStatusId);
		List<DeviceStatus> GetAllDeviceStatus();
		DeviceStatus InsertDeviceStatus(DeviceStatus entity);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);
	}
	
	
}
