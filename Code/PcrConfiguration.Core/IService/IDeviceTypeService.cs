﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using PcrConfiguration.Core.Entities;
using PcrConfiguration.Core.DataTransfer;
using PcrConfiguration.Core.DataTransfer.DeviceType;

namespace PcrConfiguration.Core.IService
{
		
	public interface IDeviceTypeService
	{
        Dictionary<string, string> GetDeviceTypeBasicSearchColumns();
        
        List<SearchColumn> GetDeviceTypeAdvanceSearchColumns();

		DeviceType GetDeviceType(System.Int32 DeviceTypeId);
		DataTransfer<List<GetOutput>> GetAll();
		DeviceType UpdateDeviceType(DeviceType entity);
		bool DeleteDeviceType(System.Int32 DeviceTypeId);
		List<DeviceType> GetAllDeviceType();
		DeviceType InsertDeviceType(DeviceType entity);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);
	}
	
	
}
