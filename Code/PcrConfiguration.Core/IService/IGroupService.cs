﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using PcrConfiguration.Core.Entities;
using PcrConfiguration.Core.DataTransfer;
using PcrConfiguration.Core.DataTransfer.Group;

namespace PcrConfiguration.Core.IService
{
		
	public interface IGroupService
	{
        Dictionary<string, string> GetGroupBasicSearchColumns();
        
        List<SearchColumn> GetGroupAdvanceSearchColumns();

		Group GetGroup(System.Int32 GroupId);
		DataTransfer<List<GetOutput>> GetAll();
		Group UpdateGroup(Group entity);
		bool DeleteGroup(System.Int32 GroupId);
		List<Group> GetAllGroup();
		Group InsertGroup(Group entity);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);

        List<Group> GetGroupByKeyValue(string Key, string Value, Operands operand, string SelectClause = null);
	}
	
	
}
