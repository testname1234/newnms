﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using PcrConfiguration.Core.Entities;
using PcrConfiguration.Core.DataInterfaces;
using PcrConfiguration.Core.IService;
using PcrConfiguration.Core.DataTransfer;
using PcrConfiguration.Core.DataTransfer.DeviceStatusHistory;
using Validation;
using System.Linq;

namespace PcrConfiguration.Core.Service
{
		
	public class DeviceStatusHistoryService : IDeviceStatusHistoryService 
	{
		private IDeviceStatusHistoryRepository _iDeviceStatusHistoryRepository;
        
		public DeviceStatusHistoryService(IDeviceStatusHistoryRepository iDeviceStatusHistoryRepository)
		{
			this._iDeviceStatusHistoryRepository = iDeviceStatusHistoryRepository;
		}
        
        public Dictionary<string, string> GetDeviceStatusHistoryBasicSearchColumns()
        {
            
            return this._iDeviceStatusHistoryRepository.GetDeviceStatusHistoryBasicSearchColumns();
           
        }
        
        public List<SearchColumn> GetDeviceStatusHistoryAdvanceSearchColumns()
        {
            
            return this._iDeviceStatusHistoryRepository.GetDeviceStatusHistoryAdvanceSearchColumns();
           
        }
        

		public virtual List<DeviceStatusHistory> GetDeviceStatusHistoryByDeviceId(System.Int32 DeviceId)
		{
			return _iDeviceStatusHistoryRepository.GetDeviceStatusHistoryByDeviceId(DeviceId);
		}

		public virtual List<DeviceStatusHistory> GetDeviceStatusHistoryByDeviceStatusId(System.Int32 DeviceStatusId)
		{
			return _iDeviceStatusHistoryRepository.GetDeviceStatusHistoryByDeviceStatusId(DeviceStatusId);
		}

		public virtual List<DeviceStatusHistory> GetDeviceStatusHistoryByGroupId(System.Int32 GroupId)
		{
			return _iDeviceStatusHistoryRepository.GetDeviceStatusHistoryByGroupId(GroupId);
		}

		public DeviceStatusHistory GetDeviceStatusHistory(System.Int32 DeviceStatusHistoryId)
		{
			return _iDeviceStatusHistoryRepository.GetDeviceStatusHistory(DeviceStatusHistoryId);
		}

		public DeviceStatusHistory UpdateDeviceStatusHistory(DeviceStatusHistory entity)
		{
			return _iDeviceStatusHistoryRepository.UpdateDeviceStatusHistory(entity);
		}

		public bool DeleteDeviceStatusHistory(System.Int32 DeviceStatusHistoryId)
		{
			return _iDeviceStatusHistoryRepository.DeleteDeviceStatusHistory(DeviceStatusHistoryId);
		}

		public List<DeviceStatusHistory> GetAllDeviceStatusHistory()
		{
			return _iDeviceStatusHistoryRepository.GetAllDeviceStatusHistory();
		}

		public DeviceStatusHistory InsertDeviceStatusHistory(DeviceStatusHistory entity)
		{
			 return _iDeviceStatusHistoryRepository.InsertDeviceStatusHistory(entity);
		}


        public DataTransfer<GetOutput> Get(string _id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 devicestatushistoryid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out devicestatushistoryid))
            {
				DeviceStatusHistory devicestatushistory = _iDeviceStatusHistoryRepository.GetDeviceStatusHistory(devicestatushistoryid);
                if(devicestatushistory!=null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(devicestatushistory);
                    tranfer.Data=output ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }   
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<DeviceStatusHistory> devicestatushistorylist = _iDeviceStatusHistoryRepository.GetAllDeviceStatusHistory();
            if (devicestatushistorylist != null && devicestatushistorylist.Count>0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(devicestatushistorylist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
           DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if(errors.Count==0)
            {
                DeviceStatusHistory devicestatushistory = new DeviceStatusHistory();
                PostOutput output = new PostOutput();
                devicestatushistory.CopyFrom(Input);
                devicestatushistory = _iDeviceStatusHistoryRepository.InsertDeviceStatusHistory(devicestatushistory);
                output.CopyFrom(devicestatushistory);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                DeviceStatusHistory devicestatushistoryinput = new DeviceStatusHistory();
                DeviceStatusHistory devicestatushistoryoutput = new DeviceStatusHistory();
                PutOutput output = new PutOutput();
                devicestatushistoryinput.CopyFrom(Input);
                DeviceStatusHistory devicestatushistory = _iDeviceStatusHistoryRepository.GetDeviceStatusHistory(devicestatushistoryinput.DeviceStatusHistoryId);
                if (devicestatushistory!=null)
                {
                    devicestatushistoryoutput = _iDeviceStatusHistoryRepository.UpdateDeviceStatusHistory(devicestatushistoryinput);
                    if(devicestatushistoryoutput!=null)
                    {
                        output.CopyFrom(devicestatushistoryoutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    } 
                }
                else                
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

         public DataTransfer<string> Delete(string _id)
         {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 devicestatushistoryid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out devicestatushistoryid))
            {
				 bool IsDeleted = _iDeviceStatusHistoryRepository.DeleteDeviceStatusHistory(devicestatushistoryid);
                if(IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data=IsDeleted.ToString().ToLower() ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }
	}
	
	
}
