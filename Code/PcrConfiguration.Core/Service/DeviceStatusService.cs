﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using PcrConfiguration.Core.Entities;
using PcrConfiguration.Core.DataInterfaces;
using PcrConfiguration.Core.IService;
using PcrConfiguration.Core.DataTransfer;
using PcrConfiguration.Core.DataTransfer.DeviceStatus;
using Validation;
using System.Linq;

namespace PcrConfiguration.Core.Service
{
		
	public class DeviceStatusService : IDeviceStatusService 
	{
		private IDeviceStatusRepository _iDeviceStatusRepository;
        
		public DeviceStatusService(IDeviceStatusRepository iDeviceStatusRepository)
		{
			this._iDeviceStatusRepository = iDeviceStatusRepository;
		}
        
        public Dictionary<string, string> GetDeviceStatusBasicSearchColumns()
        {
            
            return this._iDeviceStatusRepository.GetDeviceStatusBasicSearchColumns();
           
        }
        
        public List<SearchColumn> GetDeviceStatusAdvanceSearchColumns()
        {
            
            return this._iDeviceStatusRepository.GetDeviceStatusAdvanceSearchColumns();
           
        }
        

		public DeviceStatus GetDeviceStatus(System.Int32 DeviceStatusId)
		{
			return _iDeviceStatusRepository.GetDeviceStatus(DeviceStatusId);
		}

		public DeviceStatus UpdateDeviceStatus(DeviceStatus entity)
		{
			return _iDeviceStatusRepository.UpdateDeviceStatus(entity);
		}

		public bool DeleteDeviceStatus(System.Int32 DeviceStatusId)
		{
			return _iDeviceStatusRepository.DeleteDeviceStatus(DeviceStatusId);
		}

		public List<DeviceStatus> GetAllDeviceStatus()
		{
			return _iDeviceStatusRepository.GetAllDeviceStatus();
		}

		public DeviceStatus InsertDeviceStatus(DeviceStatus entity)
		{
			 return _iDeviceStatusRepository.InsertDeviceStatus(entity);
		}


        public DataTransfer<GetOutput> Get(string _id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 devicestatusid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out devicestatusid))
            {
				DeviceStatus devicestatus = _iDeviceStatusRepository.GetDeviceStatus(devicestatusid);
                if(devicestatus!=null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(devicestatus);
                    tranfer.Data=output ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }   
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<DeviceStatus> devicestatuslist = _iDeviceStatusRepository.GetAllDeviceStatus();
            if (devicestatuslist != null && devicestatuslist.Count>0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(devicestatuslist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
           DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if(errors.Count==0)
            {
                DeviceStatus devicestatus = new DeviceStatus();
                PostOutput output = new PostOutput();
                devicestatus.CopyFrom(Input);
                devicestatus = _iDeviceStatusRepository.InsertDeviceStatus(devicestatus);
                output.CopyFrom(devicestatus);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                DeviceStatus devicestatusinput = new DeviceStatus();
                DeviceStatus devicestatusoutput = new DeviceStatus();
                PutOutput output = new PutOutput();
                devicestatusinput.CopyFrom(Input);
                DeviceStatus devicestatus = _iDeviceStatusRepository.GetDeviceStatus(devicestatusinput.DeviceStatusId);
                if (devicestatus!=null)
                {
                    devicestatusoutput = _iDeviceStatusRepository.UpdateDeviceStatus(devicestatusinput);
                    if(devicestatusoutput!=null)
                    {
                        output.CopyFrom(devicestatusoutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    } 
                }
                else                
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

         public DataTransfer<string> Delete(string _id)
         {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 devicestatusid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out devicestatusid))
            {
				 bool IsDeleted = _iDeviceStatusRepository.DeleteDeviceStatus(devicestatusid);
                if(IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data=IsDeleted.ToString().ToLower() ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }
	}
	
	
}
