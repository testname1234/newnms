﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using PcrConfiguration.Core.Entities;
using PcrConfiguration.Core.DataInterfaces;
using PcrConfiguration.Core.IService;
using PcrConfiguration.Core.DataTransfer;
using PcrConfiguration.Core.DataTransfer.DeviceType;
using Validation;
using System.Linq;

namespace PcrConfiguration.Core.Service
{
		
	public class DeviceTypeService : IDeviceTypeService 
	{
		private IDeviceTypeRepository _iDeviceTypeRepository;
        
		public DeviceTypeService(IDeviceTypeRepository iDeviceTypeRepository)
		{
			this._iDeviceTypeRepository = iDeviceTypeRepository;
		}
        
        public Dictionary<string, string> GetDeviceTypeBasicSearchColumns()
        {
            
            return this._iDeviceTypeRepository.GetDeviceTypeBasicSearchColumns();
           
        }
        
        public List<SearchColumn> GetDeviceTypeAdvanceSearchColumns()
        {
            
            return this._iDeviceTypeRepository.GetDeviceTypeAdvanceSearchColumns();
           
        }
        

		public DeviceType GetDeviceType(System.Int32 DeviceTypeId)
		{
			return _iDeviceTypeRepository.GetDeviceType(DeviceTypeId);
		}

		public DeviceType UpdateDeviceType(DeviceType entity)
		{
			return _iDeviceTypeRepository.UpdateDeviceType(entity);
		}

		public bool DeleteDeviceType(System.Int32 DeviceTypeId)
		{
			return _iDeviceTypeRepository.DeleteDeviceType(DeviceTypeId);
		}

		public List<DeviceType> GetAllDeviceType()
		{
			return _iDeviceTypeRepository.GetAllDeviceType();
		}

		public DeviceType InsertDeviceType(DeviceType entity)
		{
			 return _iDeviceTypeRepository.InsertDeviceType(entity);
		}


        public DataTransfer<GetOutput> Get(string _id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 devicetypeid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out devicetypeid))
            {
				DeviceType devicetype = _iDeviceTypeRepository.GetDeviceType(devicetypeid);
                if(devicetype!=null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(devicetype);
                    tranfer.Data=output ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }   
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<DeviceType> devicetypelist = _iDeviceTypeRepository.GetAllDeviceType();
            if (devicetypelist != null && devicetypelist.Count>0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(devicetypelist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
           DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if(errors.Count==0)
            {
                DeviceType devicetype = new DeviceType();
                PostOutput output = new PostOutput();
                devicetype.CopyFrom(Input);
                devicetype = _iDeviceTypeRepository.InsertDeviceType(devicetype);
                output.CopyFrom(devicetype);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                DeviceType devicetypeinput = new DeviceType();
                DeviceType devicetypeoutput = new DeviceType();
                PutOutput output = new PutOutput();
                devicetypeinput.CopyFrom(Input);
                DeviceType devicetype = _iDeviceTypeRepository.GetDeviceType(devicetypeinput.DeviceTypeId);
                if (devicetype!=null)
                {
                    devicetypeoutput = _iDeviceTypeRepository.UpdateDeviceType(devicetypeinput);
                    if(devicetypeoutput!=null)
                    {
                        output.CopyFrom(devicetypeoutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    } 
                }
                else                
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

         public DataTransfer<string> Delete(string _id)
         {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 devicetypeid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out devicetypeid))
            {
				 bool IsDeleted = _iDeviceTypeRepository.DeleteDeviceType(devicetypeid);
                if(IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data=IsDeleted.ToString().ToLower() ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }
	}
	
	
}
