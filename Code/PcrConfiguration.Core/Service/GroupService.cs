﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using PcrConfiguration.Core.Entities;
using PcrConfiguration.Core.DataInterfaces;
using PcrConfiguration.Core.IService;
using PcrConfiguration.Core.DataTransfer;
using PcrConfiguration.Core.DataTransfer.Group;
using Validation;
using System.Linq;

namespace PcrConfiguration.Core.Service
{
		
	public class GroupService : IGroupService 
	{
		private IGroupRepository _iGroupRepository;
        
		public GroupService(IGroupRepository iGroupRepository)
		{
			this._iGroupRepository = iGroupRepository;
		}
        
        public Dictionary<string, string> GetGroupBasicSearchColumns()
        {
            
            return this._iGroupRepository.GetGroupBasicSearchColumns();
           
        }
        
        public List<SearchColumn> GetGroupAdvanceSearchColumns()
        {
            
            return this._iGroupRepository.GetGroupAdvanceSearchColumns();
           
        }
        

		public Group GetGroup(System.Int32 GroupId)
		{
			return _iGroupRepository.GetGroup(GroupId);
		}

		public Group UpdateGroup(Group entity)
		{
			return _iGroupRepository.UpdateGroup(entity);
		}

		public bool DeleteGroup(System.Int32 GroupId)
		{
			return _iGroupRepository.DeleteGroup(GroupId);
		}

		public List<Group> GetAllGroup()
		{
			return _iGroupRepository.GetAllGroup();
		}

		public Group InsertGroup(Group entity)
		{
			 return _iGroupRepository.InsertGroup(entity);
		}


        public DataTransfer<GetOutput> Get(string _id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 groupid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out groupid))
            {
				Group group = _iGroupRepository.GetGroup(groupid);
                if(group!=null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(group);
                    tranfer.Data=output ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }   
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<Group> grouplist = _iGroupRepository.GetAllGroup();
            if (grouplist != null && grouplist.Count>0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(grouplist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
           DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if(errors.Count==0)
            {
                Group group = new Group();
                PostOutput output = new PostOutput();
                group.CopyFrom(Input);
                group.CreationDate = System.DateTime.UtcNow;
                group = _iGroupRepository.InsertGroup(group);
                output.CopyFrom(group);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                Group groupinput = new Group();
                Group groupoutput = new Group();
                PutOutput output = new PutOutput();
               

                Group group = _iGroupRepository.GetGroup(int.Parse( Input.GroupId));
                if (group!=null)
                {
                    groupinput.CopyFrom(group);
                    groupinput.CopyFrom(Input);

                    groupoutput = _iGroupRepository.UpdateGroup(groupinput);
                    if(groupoutput!=null)
                    {
                        output.CopyFrom(groupoutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    } 
                }
                else                
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

         public DataTransfer<string> Delete(string _id)
         {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 groupid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out groupid))
            {
				 bool IsDeleted = _iGroupRepository.DeleteGroup(groupid);
                if(IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data=IsDeleted.ToString().ToLower() ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }

         public List<Group> GetGroupByKeyValue(string Key, string Value, Operands operand, string SelectClause = null)
         {
             return _iGroupRepository.GetGroupByKeyValue(Key,Value,operand,SelectClause);
         }
	}
	
	
}
