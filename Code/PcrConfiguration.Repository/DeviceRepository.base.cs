﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using PcrConfiguration.Core;
using PcrConfiguration.Core.Entities;
using PcrConfiguration.Core.DataInterfaces;
using PcrConfiguration.Core.Extensions;

namespace PcrConfiguration.Repository
{
		
	public abstract partial class DeviceRepositoryBase : Repository, IDeviceRepositoryBase 
	{
        
        public DeviceRepositoryBase()
        {   
            this.SearchColumns=new Dictionary<string, SearchColumn>();

			this.SearchColumns.Add("DeviceId",new SearchColumn(){Name="DeviceId",Title="DeviceId",SelectClause="DeviceId",WhereClause="AllRecords.DeviceId",DataType="System.Int32",IsForeignColumn=false,PropertyName="DeviceId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Name",new SearchColumn(){Name="Name",Title="Name",SelectClause="Name",WhereClause="AllRecords.Name",DataType="System.String",IsForeignColumn=false,PropertyName="Name",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("DeviceKey",new SearchColumn(){Name="DeviceKey",Title="DeviceKey",SelectClause="DeviceKey",WhereClause="AllRecords.DeviceKey",DataType="System.String",IsForeignColumn=false,PropertyName="DeviceKey",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("IsMOSDevice",new SearchColumn(){Name="IsMOSDevice",Title="IsMOSDevice",SelectClause="IsMOSDevice",WhereClause="AllRecords.IsMOSDevice",DataType="System.Boolean",IsForeignColumn=false,PropertyName="IsMosDevice",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("IP",new SearchColumn(){Name="IP",Title="IP",SelectClause="IP",WhereClause="AllRecords.IP",DataType="System.String",IsForeignColumn=false,PropertyName="Ip",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Port",new SearchColumn(){Name="Port",Title="Port",SelectClause="Port",WhereClause="AllRecords.Port",DataType="System.Int32",IsForeignColumn=false,PropertyName="Port",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("GroupId",new SearchColumn(){Name="GroupId",Title="GroupId",SelectClause="GroupId",WhereClause="AllRecords.GroupId",DataType="System.Int32",IsForeignColumn=false,PropertyName="GroupId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("DeviceStatusId",new SearchColumn(){Name="DeviceStatusId",Title="DeviceStatusId",SelectClause="DeviceStatusId",WhereClause="AllRecords.DeviceStatusId",DataType="System.Int32",IsForeignColumn=false,PropertyName="DeviceStatusId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("DeviceTypeId",new SearchColumn(){Name="DeviceTypeId",Title="DeviceTypeId",SelectClause="DeviceTypeId",WhereClause="AllRecords.DeviceTypeId",DataType="System.Int32",IsForeignColumn=false,PropertyName="DeviceTypeId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("LastHeartBeatTime",new SearchColumn(){Name="LastHeartBeatTime",Title="LastHeartBeatTime",SelectClause="LastHeartBeatTime",WhereClause="AllRecords.LastHeartBeatTime",DataType="System.DateTime",IsForeignColumn=false,PropertyName="LastHeartBeatTime",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("LastUpdatedDate",new SearchColumn(){Name="LastUpdatedDate",Title="LastUpdatedDate",SelectClause="LastUpdatedDate",WhereClause="AllRecords.LastUpdatedDate",DataType="System.DateTime",IsForeignColumn=false,PropertyName="LastUpdatedDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("CreationDate",new SearchColumn(){Name="CreationDate",Title="CreationDate",SelectClause="CreationDate",WhereClause="AllRecords.CreationDate",DataType="System.DateTime",IsForeignColumn=false,PropertyName="CreationDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});        
        }
        
		public virtual List<SearchColumn> GetDeviceSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                searchColumns.Add(keyValuePair.Value);
            }
            return searchColumns;
        }
		
		
		
        public virtual Dictionary<string, string> GetDeviceBasicSearchColumns()
        {
			Dictionary<string, string> columnList = new Dictionary<string, string>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsBasicSearchColumm)
                {
					keyValuePair.Value.Value = string.Empty;
                    columnList.Add(keyValuePair.Key, keyValuePair.Value.Title);
                }
            }
            return columnList;
        }

        public virtual List<SearchColumn> GetDeviceAdvanceSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsAdvanceSearchColumn)
                {
					keyValuePair.Value.Value = string.Empty;
					searchColumns.Add(keyValuePair.Value);
                }
            }
            return searchColumns;
        }
        
        
        public virtual string GetDeviceSelectClause()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            string selectQuery=string.Empty;
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (!keyValuePair.Value.IgnoreForDefaultSelect)
                {
					if (keyValuePair.Value.IsForeignColumn)
                	{
						if(string.IsNullOrEmpty(selectQuery))
						{
							selectQuery = "("+keyValuePair.Value.SelectClause+") as \""+keyValuePair.Key+"\"";
						}
						else
						{
							selectQuery += ",(" + keyValuePair.Value.SelectClause + ") as \"" + keyValuePair.Key + "\"";
						}
                	}
                	else
                	{
                    	if (string.IsNullOrEmpty(selectQuery))
                    	{
                        	selectQuery =  "[Device]."+keyValuePair.Key;
                    	}
                    	else
                    	{
                        	selectQuery += ",[Device]."+keyValuePair.Key;
                    	}
                	}
            	}
            }
            return "Select "+selectQuery+" ";
        }
        

		public virtual List<Device> GetDeviceByGroupId(System.Int32 GroupId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetDeviceSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [Device] with (nolock)  where GroupId=@GroupId  ";
			SqlParameter parameter=new SqlParameter("@GroupId",GroupId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<Device>(ds,DeviceFromDataRow);
		}

		public virtual List<Device> GetDeviceByDeviceStatusId(System.Int32 DeviceStatusId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetDeviceSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [Device] with (nolock)  where DeviceStatusId=@DeviceStatusId  ";
			SqlParameter parameter=new SqlParameter("@DeviceStatusId",DeviceStatusId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<Device>(ds,DeviceFromDataRow);
		}

		public virtual List<Device> GetDeviceByDeviceTypeId(System.Int32 DeviceTypeId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetDeviceSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [Device] with (nolock)  where DeviceTypeId=@DeviceTypeId  ";
			SqlParameter parameter=new SqlParameter("@DeviceTypeId",DeviceTypeId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<Device>(ds,DeviceFromDataRow);
		}

		public virtual Device GetDevice(System.Int32 DeviceId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetDeviceSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [Device] with (nolock)  where DeviceId=@DeviceId ";
			SqlParameter parameter=new SqlParameter("@DeviceId",DeviceId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
			return DeviceFromDataRow(ds.Tables[0].Rows[0]);
		}

		public  List<Device> GetDeviceByKeyValue(string Key,string Value,Operands operand,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetDeviceSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+= string.Format("from [Device] with (nolock)  where {0} {1} {2} ",Key,operand.ToOperandString(),Value);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<Device>(ds,DeviceFromDataRow);
		}

		public virtual List<Device> GetAllDevice(string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetDeviceSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [Device] with (nolock)  ";
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,null);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<Device>(ds, DeviceFromDataRow);
		}

		public virtual List<Device> GetPagedDevice(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null)
		{

			string whereClause = base.GetAdvancedWhereClauseByColumn(searchColumns, GetSearchColumns());
               if (!String.IsNullOrEmpty(orderByClause))
               {
                   KeyValuePair<string, string> parsedOrderByClause = base.ParseOrderByClause(orderByClause);
                   orderByClause = base.GetBasicSearchOrderByClauseByColumn(parsedOrderByClause.Key, parsedOrderByClause.Value, this.SearchColumns);
               }

            count=GetDeviceCount(whereClause, searchColumns);
			if(count>0)
			{
			if (count < startIndex) startIndex = (count / pageSize) * pageSize;			
			
           	int PageLowerBound = startIndex;
            int PageUpperBound = PageLowerBound + pageSize;
            string sql = @"CREATE TABLE #PageIndex
				            (
				                [IndexId] int IDENTITY (1, 1) NOT NULL,
				                [DeviceId] int				   
				            );";

            //Insert into the temp table
            string tempsql = "INSERT INTO #PageIndex ([DeviceId])";
            tempsql += " SELECT ";
            if (pageSize > 0) tempsql += "TOP " + PageUpperBound.ToString();
            tempsql += " [DeviceId] ";
            tempsql += " FROM [Device] AllRecords with (NOLOCK)";
            if (!string.IsNullOrEmpty(whereClause) && whereClause.Length > 0) tempsql += " WHERE " + whereClause;
            if (orderByClause.Length > 0) 
			{
				tempsql += " ORDER BY " + orderByClause;
				if( !orderByClause.Contains("DeviceId"))
					tempsql += " , (AllRecords.[DeviceId])"; 
			}
			else 
			{
				tempsql  += " ORDER BY (AllRecords.[DeviceId])"; 
			}           
            
            // Return paged results
            string pagedResultsSql =
                (string.IsNullOrEmpty(SelectClause)? GetDeviceSelectClause():(string.Format("Select {0} ",SelectClause)))+@" FROM [Device] , #PageIndex PageIndex WHERE ";
            pagedResultsSql += " PageIndex.IndexId > " + PageLowerBound.ToString(); 
            pagedResultsSql += @" AND [Device].[DeviceId] = PageIndex.[DeviceId] 
				                  ORDER BY PageIndex.IndexId;";
            pagedResultsSql += " drop table #PageIndex";
            sql = sql + tempsql + pagedResultsSql;
			sql = string.Format(sql, whereClause, pageSize, startIndex, orderByClause);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql, GetSQLParamtersBySearchColumns(searchColumns));
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<Device>(ds, DeviceFromDataRow);
			}else{ return null;}
		}

		private int GetDeviceCount(string whereClause, List<SearchColumn> searchColumns)
		{

			string sql=string.Empty;
			if(string.IsNullOrEmpty(whereClause))
				sql = "SELECT Count(*) FROM Device as AllRecords  ";
			else
				sql = "SELECT Count(*) FROM Device as AllRecords  where  " +whereClause;
			var rowCount = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, GetSQLParamtersBySearchColumns(searchColumns));
			return rowCount == DBNull.Value ? 0 :(int)rowCount;
		}

		[MOLog(AuditOperations.Create,typeof(Device))]
		public virtual Device InsertDevice(Device entity)
		{

			Device other=new Device();
			other = entity;
			if(entity.IsTransient())
			{
				string sql=@"Insert into [Device] ( [Name]
				,[DeviceKey]
				,[IsMOSDevice]
				,[IP]
				,[Port]
				,[GroupId]
				,[DeviceStatusId]
				,[DeviceTypeId]
				,[LastHeartBeatTime]
				,[LastUpdatedDate]
				,[CreationDate] )
				Values
				( @Name
				, @DeviceKey
				, @IsMOSDevice
				, @IP
				, @Port
				, @GroupId
				, @DeviceStatusId
				, @DeviceTypeId
				, @LastHeartBeatTime
				, @LastUpdatedDate
				, @CreationDate );
				Select scope_identity()";
				SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@Name",entity.Name)
					, new SqlParameter("@DeviceKey",entity.DeviceKey)
					, new SqlParameter("@IsMOSDevice",entity.IsMosDevice)
					, new SqlParameter("@IP",entity.Ip)
					, new SqlParameter("@Port",entity.Port)
					, new SqlParameter("@GroupId",entity.GroupId)
					, new SqlParameter("@DeviceStatusId",entity.DeviceStatusId)
					, new SqlParameter("@DeviceTypeId",entity.DeviceTypeId)
					, new SqlParameter("@LastHeartBeatTime",entity.LastHeartBeatTime)
					, new SqlParameter("@LastUpdatedDate",entity.LastUpdatedDate)
					, new SqlParameter("@CreationDate",entity.CreationDate)};
				var identity=SqlHelper.ExecuteScalar(this.ConnectionString,CommandType.Text,sql,parameterArray);
				if(identity==DBNull.Value) throw new DataException("Identity column was null as a result of the insert operation.");
				return GetDevice(Convert.ToInt32(identity));
			}
			return entity;
		}

		[MOLog(AuditOperations.Update,typeof(Device))]
		public virtual Device UpdateDevice(Device entity)
		{

			if (entity.IsTransient()) return entity;
			Device other = GetDevice(entity.DeviceId);
			if (entity.Equals(other)) return entity;
			string sql=@"Update [Device] set  [Name]=@Name
							, [DeviceKey]=@DeviceKey
							, [IsMOSDevice]=@IsMOSDevice
							, [IP]=@IP
							, [Port]=@Port
							, [GroupId]=@GroupId
							, [DeviceStatusId]=@DeviceStatusId
							, [DeviceTypeId]=@DeviceTypeId
							, [LastHeartBeatTime]=@LastHeartBeatTime
							, [LastUpdatedDate]=@LastUpdatedDate
							, [CreationDate]=@CreationDate 
							 where DeviceId=@DeviceId";
			SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@Name",entity.Name)
					, new SqlParameter("@DeviceKey",entity.DeviceKey)
					, new SqlParameter("@IsMOSDevice",entity.IsMosDevice)
					, new SqlParameter("@IP",entity.Ip)
					, new SqlParameter("@Port",entity.Port)
					, new SqlParameter("@GroupId",entity.GroupId)
					, new SqlParameter("@DeviceStatusId",entity.DeviceStatusId)
					, new SqlParameter("@DeviceTypeId",entity.DeviceTypeId)
					, new SqlParameter("@LastHeartBeatTime",entity.LastHeartBeatTime)
					, new SqlParameter("@LastUpdatedDate",entity.LastUpdatedDate)
					, new SqlParameter("@CreationDate",entity.CreationDate)
					, new SqlParameter("@DeviceId",entity.DeviceId)};
			SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,parameterArray);
			return GetDevice(entity.DeviceId);
		}

		public virtual bool DeleteDevice(System.Int32 DeviceId)
		{

			string sql="delete from [Device] where DeviceId=@DeviceId";
			SqlParameter parameter=new SqlParameter("@DeviceId",DeviceId);
			var identity=SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			return (Convert.ToInt32(identity))==1? true: false;
		}

		[MOLog(AuditOperations.Delete,typeof(Device))]
		public virtual Device DeleteDevice(Device entity)
		{
			this.DeleteDevice(entity.DeviceId);
			return entity;
		}


		public virtual Device DeviceFromDataRow(DataRow dr)
		{
			if(dr==null) return null;
			Device entity=new Device();
			if (dr.Table.Columns.Contains("DeviceId"))
			{
			entity.DeviceId = (System.Int32)dr["DeviceId"];
			}
			if (dr.Table.Columns.Contains("Name"))
			{
			entity.Name = dr["Name"].ToString();
			}
			if (dr.Table.Columns.Contains("DeviceKey"))
			{
			entity.DeviceKey = dr["DeviceKey"].ToString();
			}
			if (dr.Table.Columns.Contains("IsMOSDevice"))
			{
			entity.IsMosDevice = (System.Boolean)dr["IsMOSDevice"];
			}
			if (dr.Table.Columns.Contains("IP"))
			{
			entity.Ip = dr["IP"].ToString();
			}
			if (dr.Table.Columns.Contains("Port"))
			{
			entity.Port = (System.Int32)dr["Port"];
			}
			if (dr.Table.Columns.Contains("GroupId"))
			{
			entity.GroupId = (System.Int32)dr["GroupId"];
			}
			if (dr.Table.Columns.Contains("DeviceStatusId"))
			{
			entity.DeviceStatusId = (System.Int32)dr["DeviceStatusId"];
			}
			if (dr.Table.Columns.Contains("DeviceTypeId"))
			{
			entity.DeviceTypeId = (System.Int32)dr["DeviceTypeId"];
			}
			if (dr.Table.Columns.Contains("LastHeartBeatTime"))
			{
			entity.LastHeartBeatTime = (System.DateTime)dr["LastHeartBeatTime"];
			}
			if (dr.Table.Columns.Contains("LastUpdatedDate"))
			{
			entity.LastUpdatedDate = (System.DateTime)dr["LastUpdatedDate"];
			}
			if (dr.Table.Columns.Contains("CreationDate"))
			{
			entity.CreationDate = (System.DateTime)dr["CreationDate"];
			}
			return entity;
		}

	}
	
	
}
