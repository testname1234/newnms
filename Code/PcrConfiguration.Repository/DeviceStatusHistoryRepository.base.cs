﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using PcrConfiguration.Core;
using PcrConfiguration.Core.Entities;
using PcrConfiguration.Core.DataInterfaces;
using PcrConfiguration.Core.Extensions;

namespace PcrConfiguration.Repository
{
		
	public abstract partial class DeviceStatusHistoryRepositoryBase : Repository, IDeviceStatusHistoryRepositoryBase 
	{
        
        public DeviceStatusHistoryRepositoryBase()
        {   
            this.SearchColumns=new Dictionary<string, SearchColumn>();

			this.SearchColumns.Add("DeviceStatusHistoryId",new SearchColumn(){Name="DeviceStatusHistoryId",Title="DeviceStatusHistoryId",SelectClause="DeviceStatusHistoryId",WhereClause="AllRecords.DeviceStatusHistoryId",DataType="System.Int32",IsForeignColumn=false,PropertyName="DeviceStatusHistoryId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("DeviceId",new SearchColumn(){Name="DeviceId",Title="DeviceId",SelectClause="DeviceId",WhereClause="AllRecords.DeviceId",DataType="System.Int32",IsForeignColumn=false,PropertyName="DeviceId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("DeviceStatusId",new SearchColumn(){Name="DeviceStatusId",Title="DeviceStatusId",SelectClause="DeviceStatusId",WhereClause="AllRecords.DeviceStatusId",DataType="System.Int32",IsForeignColumn=false,PropertyName="DeviceStatusId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("GroupId",new SearchColumn(){Name="GroupId",Title="GroupId",SelectClause="GroupId",WhereClause="AllRecords.GroupId",DataType="System.Int32",IsForeignColumn=false,PropertyName="GroupId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("CreationDate",new SearchColumn(){Name="CreationDate",Title="CreationDate",SelectClause="CreationDate",WhereClause="AllRecords.CreationDate",DataType="System.DateTime",IsForeignColumn=false,PropertyName="CreationDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});        
        }
        
		public virtual List<SearchColumn> GetDeviceStatusHistorySearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                searchColumns.Add(keyValuePair.Value);
            }
            return searchColumns;
        }
		
		
		
        public virtual Dictionary<string, string> GetDeviceStatusHistoryBasicSearchColumns()
        {
			Dictionary<string, string> columnList = new Dictionary<string, string>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsBasicSearchColumm)
                {
					keyValuePair.Value.Value = string.Empty;
                    columnList.Add(keyValuePair.Key, keyValuePair.Value.Title);
                }
            }
            return columnList;
        }

        public virtual List<SearchColumn> GetDeviceStatusHistoryAdvanceSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsAdvanceSearchColumn)
                {
					keyValuePair.Value.Value = string.Empty;
					searchColumns.Add(keyValuePair.Value);
                }
            }
            return searchColumns;
        }
        
        
        public virtual string GetDeviceStatusHistorySelectClause()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            string selectQuery=string.Empty;
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (!keyValuePair.Value.IgnoreForDefaultSelect)
                {
					if (keyValuePair.Value.IsForeignColumn)
                	{
						if(string.IsNullOrEmpty(selectQuery))
						{
							selectQuery = "("+keyValuePair.Value.SelectClause+") as \""+keyValuePair.Key+"\"";
						}
						else
						{
							selectQuery += ",(" + keyValuePair.Value.SelectClause + ") as \"" + keyValuePair.Key + "\"";
						}
                	}
                	else
                	{
                    	if (string.IsNullOrEmpty(selectQuery))
                    	{
                        	selectQuery =  "[DeviceStatusHistory]."+keyValuePair.Key;
                    	}
                    	else
                    	{
                        	selectQuery += ",[DeviceStatusHistory]."+keyValuePair.Key;
                    	}
                	}
            	}
            }
            return "Select "+selectQuery+" ";
        }
        

		public virtual List<DeviceStatusHistory> GetDeviceStatusHistoryByDeviceId(System.Int32 DeviceId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetDeviceStatusHistorySelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [DeviceStatusHistory] with (nolock)  where DeviceId=@DeviceId  ";
			SqlParameter parameter=new SqlParameter("@DeviceId",DeviceId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<DeviceStatusHistory>(ds,DeviceStatusHistoryFromDataRow);
		}

		public virtual List<DeviceStatusHistory> GetDeviceStatusHistoryByDeviceStatusId(System.Int32 DeviceStatusId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetDeviceStatusHistorySelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [DeviceStatusHistory] with (nolock)  where DeviceStatusId=@DeviceStatusId  ";
			SqlParameter parameter=new SqlParameter("@DeviceStatusId",DeviceStatusId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<DeviceStatusHistory>(ds,DeviceStatusHistoryFromDataRow);
		}

		public virtual List<DeviceStatusHistory> GetDeviceStatusHistoryByGroupId(System.Int32 GroupId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetDeviceStatusHistorySelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [DeviceStatusHistory] with (nolock)  where GroupId=@GroupId  ";
			SqlParameter parameter=new SqlParameter("@GroupId",GroupId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<DeviceStatusHistory>(ds,DeviceStatusHistoryFromDataRow);
		}

		public virtual DeviceStatusHistory GetDeviceStatusHistory(System.Int32 DeviceStatusHistoryId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetDeviceStatusHistorySelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [DeviceStatusHistory] with (nolock)  where DeviceStatusHistoryId=@DeviceStatusHistoryId ";
			SqlParameter parameter=new SqlParameter("@DeviceStatusHistoryId",DeviceStatusHistoryId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
			return DeviceStatusHistoryFromDataRow(ds.Tables[0].Rows[0]);
		}

		public  List<DeviceStatusHistory> GetDeviceStatusHistoryByKeyValue(string Key,string Value,Operands operand,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetDeviceStatusHistorySelectClause():(string.Format("Select {0} ",SelectClause));
			sql+= string.Format("from [DeviceStatusHistory] with (nolock)  where {0} {1} {2} ",Key,operand.ToOperandString(),Value);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<DeviceStatusHistory>(ds,DeviceStatusHistoryFromDataRow);
		}

		public virtual List<DeviceStatusHistory> GetAllDeviceStatusHistory(string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetDeviceStatusHistorySelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [DeviceStatusHistory] with (nolock)  ";
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,null);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<DeviceStatusHistory>(ds, DeviceStatusHistoryFromDataRow);
		}

		public virtual List<DeviceStatusHistory> GetPagedDeviceStatusHistory(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null)
		{

			string whereClause = base.GetAdvancedWhereClauseByColumn(searchColumns, GetSearchColumns());
               if (!String.IsNullOrEmpty(orderByClause))
               {
                   KeyValuePair<string, string> parsedOrderByClause = base.ParseOrderByClause(orderByClause);
                   orderByClause = base.GetBasicSearchOrderByClauseByColumn(parsedOrderByClause.Key, parsedOrderByClause.Value, this.SearchColumns);
               }

            count=GetDeviceStatusHistoryCount(whereClause, searchColumns);
			if(count>0)
			{
			if (count < startIndex) startIndex = (count / pageSize) * pageSize;			
			
           	int PageLowerBound = startIndex;
            int PageUpperBound = PageLowerBound + pageSize;
            string sql = @"CREATE TABLE #PageIndex
				            (
				                [IndexId] int IDENTITY (1, 1) NOT NULL,
				                [DeviceStatusHistoryId] int				   
				            );";

            //Insert into the temp table
            string tempsql = "INSERT INTO #PageIndex ([DeviceStatusHistoryId])";
            tempsql += " SELECT ";
            if (pageSize > 0) tempsql += "TOP " + PageUpperBound.ToString();
            tempsql += " [DeviceStatusHistoryId] ";
            tempsql += " FROM [DeviceStatusHistory] AllRecords with (NOLOCK)";
            if (!string.IsNullOrEmpty(whereClause) && whereClause.Length > 0) tempsql += " WHERE " + whereClause;
            if (orderByClause.Length > 0) 
			{
				tempsql += " ORDER BY " + orderByClause;
				if( !orderByClause.Contains("DeviceStatusHistoryId"))
					tempsql += " , (AllRecords.[DeviceStatusHistoryId])"; 
			}
			else 
			{
				tempsql  += " ORDER BY (AllRecords.[DeviceStatusHistoryId])"; 
			}           
            
            // Return paged results
            string pagedResultsSql =
                (string.IsNullOrEmpty(SelectClause)? GetDeviceStatusHistorySelectClause():(string.Format("Select {0} ",SelectClause)))+@" FROM [DeviceStatusHistory] , #PageIndex PageIndex WHERE ";
            pagedResultsSql += " PageIndex.IndexId > " + PageLowerBound.ToString(); 
            pagedResultsSql += @" AND [DeviceStatusHistory].[DeviceStatusHistoryId] = PageIndex.[DeviceStatusHistoryId] 
				                  ORDER BY PageIndex.IndexId;";
            pagedResultsSql += " drop table #PageIndex";
            sql = sql + tempsql + pagedResultsSql;
			sql = string.Format(sql, whereClause, pageSize, startIndex, orderByClause);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql, GetSQLParamtersBySearchColumns(searchColumns));
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<DeviceStatusHistory>(ds, DeviceStatusHistoryFromDataRow);
			}else{ return null;}
		}

		private int GetDeviceStatusHistoryCount(string whereClause, List<SearchColumn> searchColumns)
		{

			string sql=string.Empty;
			if(string.IsNullOrEmpty(whereClause))
				sql = "SELECT Count(*) FROM DeviceStatusHistory as AllRecords  ";
			else
				sql = "SELECT Count(*) FROM DeviceStatusHistory as AllRecords  where  " +whereClause;
			var rowCount = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, GetSQLParamtersBySearchColumns(searchColumns));
			return rowCount == DBNull.Value ? 0 :(int)rowCount;
		}

		[MOLog(AuditOperations.Create,typeof(DeviceStatusHistory))]
		public virtual DeviceStatusHistory InsertDeviceStatusHistory(DeviceStatusHistory entity)
		{

			DeviceStatusHistory other=new DeviceStatusHistory();
			other = entity;
			if(entity.IsTransient())
			{
				string sql=@"Insert into [DeviceStatusHistory] ( [DeviceId]
				,[DeviceStatusId]
				,[GroupId]
				,[CreationDate] )
				Values
				( @DeviceId
				, @DeviceStatusId
				, @GroupId
				, @CreationDate );
				Select scope_identity()";
				SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@DeviceId",entity.DeviceId)
					, new SqlParameter("@DeviceStatusId",entity.DeviceStatusId)
					, new SqlParameter("@GroupId",entity.GroupId)
					, new SqlParameter("@CreationDate",entity.CreationDate)};
				var identity=SqlHelper.ExecuteScalar(this.ConnectionString,CommandType.Text,sql,parameterArray);
				if(identity==DBNull.Value) throw new DataException("Identity column was null as a result of the insert operation.");
				return GetDeviceStatusHistory(Convert.ToInt32(identity));
			}
			return entity;
		}

		[MOLog(AuditOperations.Update,typeof(DeviceStatusHistory))]
		public virtual DeviceStatusHistory UpdateDeviceStatusHistory(DeviceStatusHistory entity)
		{

			if (entity.IsTransient()) return entity;
			DeviceStatusHistory other = GetDeviceStatusHistory(entity.DeviceStatusHistoryId);
			if (entity.Equals(other)) return entity;
			string sql=@"Update [DeviceStatusHistory] set  [DeviceId]=@DeviceId
							, [DeviceStatusId]=@DeviceStatusId
							, [GroupId]=@GroupId
							, [CreationDate]=@CreationDate 
							 where DeviceStatusHistoryId=@DeviceStatusHistoryId";
			SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@DeviceId",entity.DeviceId)
					, new SqlParameter("@DeviceStatusId",entity.DeviceStatusId)
					, new SqlParameter("@GroupId",entity.GroupId)
					, new SqlParameter("@CreationDate",entity.CreationDate)
					, new SqlParameter("@DeviceStatusHistoryId",entity.DeviceStatusHistoryId)};
			SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,parameterArray);
			return GetDeviceStatusHistory(entity.DeviceStatusHistoryId);
		}

		public virtual bool DeleteDeviceStatusHistory(System.Int32 DeviceStatusHistoryId)
		{

			string sql="delete from [DeviceStatusHistory] with (nolock) where DeviceStatusHistoryId=@DeviceStatusHistoryId";
			SqlParameter parameter=new SqlParameter("@DeviceStatusHistoryId",DeviceStatusHistoryId);
			var identity=SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			return (Convert.ToInt32(identity))==1? true: false;
		}

		[MOLog(AuditOperations.Delete,typeof(DeviceStatusHistory))]
		public virtual DeviceStatusHistory DeleteDeviceStatusHistory(DeviceStatusHistory entity)
		{
			this.DeleteDeviceStatusHistory(entity.DeviceStatusHistoryId);
			return entity;
		}


		public virtual DeviceStatusHistory DeviceStatusHistoryFromDataRow(DataRow dr)
		{
			if(dr==null) return null;
			DeviceStatusHistory entity=new DeviceStatusHistory();
			if (dr.Table.Columns.Contains("DeviceStatusHistoryId"))
			{
			entity.DeviceStatusHistoryId = (System.Int32)dr["DeviceStatusHistoryId"];
			}
			if (dr.Table.Columns.Contains("DeviceId"))
			{
			entity.DeviceId = (System.Int32)dr["DeviceId"];
			}
			if (dr.Table.Columns.Contains("DeviceStatusId"))
			{
			entity.DeviceStatusId = (System.Int32)dr["DeviceStatusId"];
			}
			if (dr.Table.Columns.Contains("GroupId"))
			{
			entity.GroupId = (System.Int32)dr["GroupId"];
			}
			if (dr.Table.Columns.Contains("CreationDate"))
			{
			entity.CreationDate = (System.DateTime)dr["CreationDate"];
			}
			return entity;
		}

	}
	
	
}
