﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using PcrConfiguration.Core;
using PcrConfiguration.Core.Entities;
using PcrConfiguration.Core.DataInterfaces;
using PcrConfiguration.Core.Extensions;

namespace PcrConfiguration.Repository
{
		
	public abstract partial class DeviceTypeRepositoryBase : Repository, IDeviceTypeRepositoryBase 
	{
        
        public DeviceTypeRepositoryBase()
        {   
            this.SearchColumns=new Dictionary<string, SearchColumn>();

			this.SearchColumns.Add("DeviceTypeId",new SearchColumn(){Name="DeviceTypeId",Title="DeviceTypeId",SelectClause="DeviceTypeId",WhereClause="AllRecords.DeviceTypeId",DataType="System.Int32",IsForeignColumn=false,PropertyName="DeviceTypeId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Name",new SearchColumn(){Name="Name",Title="Name",SelectClause="Name",WhereClause="AllRecords.Name",DataType="System.String",IsForeignColumn=false,PropertyName="Name",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});        
        }
        
		public virtual List<SearchColumn> GetDeviceTypeSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                searchColumns.Add(keyValuePair.Value);
            }
            return searchColumns;
        }
		
		
		
        public virtual Dictionary<string, string> GetDeviceTypeBasicSearchColumns()
        {
			Dictionary<string, string> columnList = new Dictionary<string, string>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsBasicSearchColumm)
                {
					keyValuePair.Value.Value = string.Empty;
                    columnList.Add(keyValuePair.Key, keyValuePair.Value.Title);
                }
            }
            return columnList;
        }

        public virtual List<SearchColumn> GetDeviceTypeAdvanceSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsAdvanceSearchColumn)
                {
					keyValuePair.Value.Value = string.Empty;
					searchColumns.Add(keyValuePair.Value);
                }
            }
            return searchColumns;
        }
        
        
        public virtual string GetDeviceTypeSelectClause()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            string selectQuery=string.Empty;
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (!keyValuePair.Value.IgnoreForDefaultSelect)
                {
					if (keyValuePair.Value.IsForeignColumn)
                	{
						if(string.IsNullOrEmpty(selectQuery))
						{
							selectQuery = "("+keyValuePair.Value.SelectClause+") as \""+keyValuePair.Key+"\"";
						}
						else
						{
							selectQuery += ",(" + keyValuePair.Value.SelectClause + ") as \"" + keyValuePair.Key + "\"";
						}
                	}
                	else
                	{
                    	if (string.IsNullOrEmpty(selectQuery))
                    	{
                        	selectQuery =  "[DeviceType]."+keyValuePair.Key;
                    	}
                    	else
                    	{
                        	selectQuery += ",[DeviceType]."+keyValuePair.Key;
                    	}
                	}
            	}
            }
            return "Select "+selectQuery+" ";
        }
        

		public virtual DeviceType GetDeviceType(System.Int32 DeviceTypeId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetDeviceTypeSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [DeviceType] with (nolock)  where DeviceTypeId=@DeviceTypeId ";
			SqlParameter parameter=new SqlParameter("@DeviceTypeId",DeviceTypeId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
			return DeviceTypeFromDataRow(ds.Tables[0].Rows[0]);
		}

		public  List<DeviceType> GetDeviceTypeByKeyValue(string Key,string Value,Operands operand,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetDeviceTypeSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+= string.Format("from [DeviceType] with (nolock)  where {0} {1} {2} ",Key,operand.ToOperandString(),Value);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<DeviceType>(ds,DeviceTypeFromDataRow);
		}

		public virtual List<DeviceType> GetAllDeviceType(string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetDeviceTypeSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [DeviceType] with (nolock)  ";
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,null);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<DeviceType>(ds, DeviceTypeFromDataRow);
		}

		public virtual List<DeviceType> GetPagedDeviceType(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null)
		{

			string whereClause = base.GetAdvancedWhereClauseByColumn(searchColumns, GetSearchColumns());
               if (!String.IsNullOrEmpty(orderByClause))
               {
                   KeyValuePair<string, string> parsedOrderByClause = base.ParseOrderByClause(orderByClause);
                   orderByClause = base.GetBasicSearchOrderByClauseByColumn(parsedOrderByClause.Key, parsedOrderByClause.Value, this.SearchColumns);
               }

            count=GetDeviceTypeCount(whereClause, searchColumns);
			if(count>0)
			{
			if (count < startIndex) startIndex = (count / pageSize) * pageSize;			
			
           	int PageLowerBound = startIndex;
            int PageUpperBound = PageLowerBound + pageSize;
            string sql = @"CREATE TABLE #PageIndex
				            (
				                [IndexId] int IDENTITY (1, 1) NOT NULL,
				                [DeviceTypeId] int				   
				            );";

            //Insert into the temp table
            string tempsql = "INSERT INTO #PageIndex ([DeviceTypeId])";
            tempsql += " SELECT ";
            if (pageSize > 0) tempsql += "TOP " + PageUpperBound.ToString();
            tempsql += " [DeviceTypeId] ";
            tempsql += " FROM [DeviceType] AllRecords with (NOLOCK)";
            if (!string.IsNullOrEmpty(whereClause) && whereClause.Length > 0) tempsql += " WHERE " + whereClause;
            if (orderByClause.Length > 0) 
			{
				tempsql += " ORDER BY " + orderByClause;
				if( !orderByClause.Contains("DeviceTypeId"))
					tempsql += " , (AllRecords.[DeviceTypeId])"; 
			}
			else 
			{
				tempsql  += " ORDER BY (AllRecords.[DeviceTypeId])"; 
			}           
            
            // Return paged results
            string pagedResultsSql =
                (string.IsNullOrEmpty(SelectClause)? GetDeviceTypeSelectClause():(string.Format("Select {0} ",SelectClause)))+@" FROM [DeviceType] , #PageIndex PageIndex WHERE ";
            pagedResultsSql += " PageIndex.IndexId > " + PageLowerBound.ToString(); 
            pagedResultsSql += @" AND [DeviceType].[DeviceTypeId] = PageIndex.[DeviceTypeId] 
				                  ORDER BY PageIndex.IndexId;";
            pagedResultsSql += " drop table #PageIndex";
            sql = sql + tempsql + pagedResultsSql;
			sql = string.Format(sql, whereClause, pageSize, startIndex, orderByClause);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql, GetSQLParamtersBySearchColumns(searchColumns));
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<DeviceType>(ds, DeviceTypeFromDataRow);
			}else{ return null;}
		}

		private int GetDeviceTypeCount(string whereClause, List<SearchColumn> searchColumns)
		{

			string sql=string.Empty;
			if(string.IsNullOrEmpty(whereClause))
				sql = "SELECT Count(*) FROM DeviceType as AllRecords  ";
			else
				sql = "SELECT Count(*) FROM DeviceType as AllRecords  where  " +whereClause;
			var rowCount = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, GetSQLParamtersBySearchColumns(searchColumns));
			return rowCount == DBNull.Value ? 0 :(int)rowCount;
		}

		[MOLog(AuditOperations.Create,typeof(DeviceType))]
		public virtual DeviceType InsertDeviceType(DeviceType entity)
		{

			DeviceType other=new DeviceType();
			other = entity;
			if(entity.IsTransient())
			{
				string sql=@"Insert into [DeviceType] ( [Name] )
				Values
				( @Name );
				Select scope_identity()";
				SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@Name",entity.Name)};
				var identity=SqlHelper.ExecuteScalar(this.ConnectionString,CommandType.Text,sql,parameterArray);
				if(identity==DBNull.Value) throw new DataException("Identity column was null as a result of the insert operation.");
				return GetDeviceType(Convert.ToInt32(identity));
			}
			return entity;
		}

		[MOLog(AuditOperations.Update,typeof(DeviceType))]
		public virtual DeviceType UpdateDeviceType(DeviceType entity)
		{

			if (entity.IsTransient()) return entity;
			DeviceType other = GetDeviceType(entity.DeviceTypeId);
			if (entity.Equals(other)) return entity;
			string sql=@"Update [DeviceType] set  [Name]=@Name 
							 where DeviceTypeId=@DeviceTypeId";
			SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@Name",entity.Name)
					, new SqlParameter("@DeviceTypeId",entity.DeviceTypeId)};
			SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,parameterArray);
			return GetDeviceType(entity.DeviceTypeId);
		}

		public virtual bool DeleteDeviceType(System.Int32 DeviceTypeId)
		{

			string sql="delete from [DeviceType] with (nolock) where DeviceTypeId=@DeviceTypeId";
			SqlParameter parameter=new SqlParameter("@DeviceTypeId",DeviceTypeId);
			var identity=SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			return (Convert.ToInt32(identity))==1? true: false;
		}

		[MOLog(AuditOperations.Delete,typeof(DeviceType))]
		public virtual DeviceType DeleteDeviceType(DeviceType entity)
		{
			this.DeleteDeviceType(entity.DeviceTypeId);
			return entity;
		}


		public virtual DeviceType DeviceTypeFromDataRow(DataRow dr)
		{
			if(dr==null) return null;
			DeviceType entity=new DeviceType();
			if (dr.Table.Columns.Contains("DeviceTypeId"))
			{
			entity.DeviceTypeId = (System.Int32)dr["DeviceTypeId"];
			}
			if (dr.Table.Columns.Contains("Name"))
			{
			entity.Name = dr["Name"].ToString();
			}
			return entity;
		}

	}
	
	
}
