﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using PcrConfiguration.Core;
using PcrConfiguration.Core.Entities;
using PcrConfiguration.Core.DataInterfaces;
using PcrConfiguration.Core.Extensions;

namespace PcrConfiguration.Repository
{
		
	public abstract partial class GroupRepositoryBase : Repository, IGroupRepositoryBase 
	{
        
        public GroupRepositoryBase()
        {   
            this.SearchColumns=new Dictionary<string, SearchColumn>();

			this.SearchColumns.Add("GroupId",new SearchColumn(){Name="GroupId",Title="GroupId",SelectClause="GroupId",WhereClause="AllRecords.GroupId",DataType="System.Int32",IsForeignColumn=false,PropertyName="GroupId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Name",new SearchColumn(){Name="Name",Title="Name",SelectClause="Name",WhereClause="AllRecords.Name",DataType="System.String",IsForeignColumn=false,PropertyName="Name",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("GroupKey",new SearchColumn(){Name="GroupKey",Title="GroupKey",SelectClause="GroupKey",WhereClause="AllRecords.GroupKey",DataType="System.String",IsForeignColumn=false,PropertyName="GroupKey",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("ParentId",new SearchColumn(){Name="ParentId",Title="ParentId",SelectClause="ParentId",WhereClause="AllRecords.ParentId",DataType="System.Int32?",IsForeignColumn=false,PropertyName="ParentId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("CreationDate",new SearchColumn(){Name="CreationDate",Title="CreationDate",SelectClause="CreationDate",WhereClause="AllRecords.CreationDate",DataType="System.DateTime",IsForeignColumn=false,PropertyName="CreationDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});        
        }
        
		public virtual List<SearchColumn> GetGroupSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                searchColumns.Add(keyValuePair.Value);
            }
            return searchColumns;
        }
		
		
		
        public virtual Dictionary<string, string> GetGroupBasicSearchColumns()
        {
			Dictionary<string, string> columnList = new Dictionary<string, string>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsBasicSearchColumm)
                {
					keyValuePair.Value.Value = string.Empty;
                    columnList.Add(keyValuePair.Key, keyValuePair.Value.Title);
                }
            }
            return columnList;
        }

        public virtual List<SearchColumn> GetGroupAdvanceSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsAdvanceSearchColumn)
                {
					keyValuePair.Value.Value = string.Empty;
					searchColumns.Add(keyValuePair.Value);
                }
            }
            return searchColumns;
        }
        
        
        public virtual string GetGroupSelectClause()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            string selectQuery=string.Empty;
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (!keyValuePair.Value.IgnoreForDefaultSelect)
                {
					if (keyValuePair.Value.IsForeignColumn)
                	{
						if(string.IsNullOrEmpty(selectQuery))
						{
							selectQuery = "("+keyValuePair.Value.SelectClause+") as \""+keyValuePair.Key+"\"";
						}
						else
						{
							selectQuery += ",(" + keyValuePair.Value.SelectClause + ") as \"" + keyValuePair.Key + "\"";
						}
                	}
                	else
                	{
                    	if (string.IsNullOrEmpty(selectQuery))
                    	{
                        	selectQuery =  "[Group]."+keyValuePair.Key;
                    	}
                    	else
                    	{
                        	selectQuery += ",[Group]."+keyValuePair.Key;
                    	}
                	}
            	}
            }
            return "Select "+selectQuery+" ";
        }
        

		public virtual Group GetGroup(System.Int32 GroupId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetGroupSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [Group] with (nolock)  where GroupId=@GroupId ";
			SqlParameter parameter=new SqlParameter("@GroupId",GroupId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
			return GroupFromDataRow(ds.Tables[0].Rows[0]);
		}

		public  List<Group> GetGroupByKeyValue(string Key,string Value,Operands operand,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetGroupSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+= string.Format("from [Group] with (nolock)  where {0} {1} {2} ",Key,operand.ToOperandString(),Value);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<Group>(ds,GroupFromDataRow);
		}

		public virtual List<Group> GetAllGroup(string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetGroupSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [Group] with (nolock)  ";
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,null);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<Group>(ds, GroupFromDataRow);
		}

		public virtual List<Group> GetPagedGroup(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null)
		{

			string whereClause = base.GetAdvancedWhereClauseByColumn(searchColumns, GetSearchColumns());
               if (!String.IsNullOrEmpty(orderByClause))
               {
                   KeyValuePair<string, string> parsedOrderByClause = base.ParseOrderByClause(orderByClause);
                   orderByClause = base.GetBasicSearchOrderByClauseByColumn(parsedOrderByClause.Key, parsedOrderByClause.Value, this.SearchColumns);
               }

            count=GetGroupCount(whereClause, searchColumns);
			if(count>0)
			{
			if (count < startIndex) startIndex = (count / pageSize) * pageSize;			
			
           	int PageLowerBound = startIndex;
            int PageUpperBound = PageLowerBound + pageSize;
            string sql = @"CREATE TABLE #PageIndex
				            (
				                [IndexId] int IDENTITY (1, 1) NOT NULL,
				                [GroupId] int				   
				            );";

            //Insert into the temp table
            string tempsql = "INSERT INTO #PageIndex ([GroupId])";
            tempsql += " SELECT ";
            if (pageSize > 0) tempsql += "TOP " + PageUpperBound.ToString();
            tempsql += " [GroupId] ";
            tempsql += " FROM [Group] AllRecords with (NOLOCK)";
            if (!string.IsNullOrEmpty(whereClause) && whereClause.Length > 0) tempsql += " WHERE " + whereClause;
            if (orderByClause.Length > 0) 
			{
				tempsql += " ORDER BY " + orderByClause;
				if( !orderByClause.Contains("GroupId"))
					tempsql += " , (AllRecords.[GroupId])"; 
			}
			else 
			{
				tempsql  += " ORDER BY (AllRecords.[GroupId])"; 
			}           
            
            // Return paged results
            string pagedResultsSql =
                (string.IsNullOrEmpty(SelectClause)? GetGroupSelectClause():(string.Format("Select {0} ",SelectClause)))+@" FROM [Group] , #PageIndex PageIndex WHERE ";
            pagedResultsSql += " PageIndex.IndexId > " + PageLowerBound.ToString(); 
            pagedResultsSql += @" AND [Group].[GroupId] = PageIndex.[GroupId] 
				                  ORDER BY PageIndex.IndexId;";
            pagedResultsSql += " drop table #PageIndex";
            sql = sql + tempsql + pagedResultsSql;
			sql = string.Format(sql, whereClause, pageSize, startIndex, orderByClause);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql, GetSQLParamtersBySearchColumns(searchColumns));
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<Group>(ds, GroupFromDataRow);
			}else{ return null;}
		}

		private int GetGroupCount(string whereClause, List<SearchColumn> searchColumns)
		{

			string sql=string.Empty;
			if(string.IsNullOrEmpty(whereClause))
				sql = "SELECT Count(*) FROM Group as AllRecords  ";
			else
				sql = "SELECT Count(*) FROM Group as AllRecords  where  " +whereClause;
			var rowCount = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, GetSQLParamtersBySearchColumns(searchColumns));
			return rowCount == DBNull.Value ? 0 :(int)rowCount;
		}

		[MOLog(AuditOperations.Create,typeof(Group))]
		public virtual Group InsertGroup(Group entity)
		{

			Group other=new Group();
			other = entity;
			if(entity.IsTransient())
			{
				string sql=@"Insert into [Group] ( [Name]
				,[GroupKey]
				,[ParentId]
				,[CreationDate] )
				Values
				( @Name
				, @GroupKey
				, @ParentId
				, @CreationDate );
				Select scope_identity()";
				SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@Name",entity.Name)
					, new SqlParameter("@GroupKey",entity.GroupKey)
					, new SqlParameter("@ParentId",entity.ParentId ?? (object)DBNull.Value)
					, new SqlParameter("@CreationDate",entity.CreationDate)};
				var identity=SqlHelper.ExecuteScalar(this.ConnectionString,CommandType.Text,sql,parameterArray);
				if(identity==DBNull.Value) throw new DataException("Identity column was null as a result of the insert operation.");
				return GetGroup(Convert.ToInt32(identity));
			}
			return entity;
		}

		[MOLog(AuditOperations.Update,typeof(Group))]
		public virtual Group UpdateGroup(Group entity)
		{

			if (entity.IsTransient()) return entity;
			Group other = GetGroup(entity.GroupId);
			if (entity.Equals(other)) return entity;
			string sql=@"Update [Group] set  [Name]=@Name
							, [GroupKey]=@GroupKey
							, [ParentId]=@ParentId
							, [CreationDate]=@CreationDate 
							 where GroupId=@GroupId";
			SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@Name",entity.Name)
					, new SqlParameter("@GroupKey",entity.GroupKey)
					, new SqlParameter("@ParentId",entity.ParentId ?? (object)DBNull.Value)
					, new SqlParameter("@CreationDate",entity.CreationDate)
					, new SqlParameter("@GroupId",entity.GroupId)};
			SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,parameterArray);
			return GetGroup(entity.GroupId);
		}

		public virtual bool DeleteGroup(System.Int32 GroupId)
		{

			string sql="delete from [Group] where GroupId=@GroupId";
			SqlParameter parameter=new SqlParameter("@GroupId",GroupId);
			var identity=SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			return (Convert.ToInt32(identity))==1? true: false;
		}

		[MOLog(AuditOperations.Delete,typeof(Group))]
		public virtual Group DeleteGroup(Group entity)
		{
			this.DeleteGroup(entity.GroupId);
			return entity;
		}


		public virtual Group GroupFromDataRow(DataRow dr)
		{
			if(dr==null) return null;
			Group entity=new Group();
			if (dr.Table.Columns.Contains("GroupId"))
			{
			entity.GroupId = (System.Int32)dr["GroupId"];
			}
			if (dr.Table.Columns.Contains("Name"))
			{
			entity.Name = dr["Name"].ToString();
			}
			if (dr.Table.Columns.Contains("GroupKey"))
			{
			entity.GroupKey = dr["GroupKey"].ToString();
			}
			if (dr.Table.Columns.Contains("ParentId"))
			{
			entity.ParentId = dr["ParentId"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["ParentId"];
			}
			if (dr.Table.Columns.Contains("CreationDate"))
			{
			entity.CreationDate = (System.DateTime)dr["CreationDate"];
			}
			return entity;
		}

	}
	
	
}
