﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SolrNet.Attributes;

namespace SolrManager.InputEntities
{
    public class ResourceMeta
    {
        public string _id { get; set; }

        [SolrUniqueKey("id")]
        public string id
        {
            get { return _id; }
            set { _id = value; }
        }

        [SolrField("Key")]
        public string Key { get; set; }


        [SolrField("Value")]
        public string Value { get; set; }

        [SolrField("LastUpdateDate")]
        public DateTime LastUpdateDate { get; set; }
    }
}
