﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SolrNet.Attributes;


namespace SolrManager.InputEntities
{
    public class SolarNews
    {
        public string _id { get; set; }

        [SolrUniqueKey("id")]
        public string id
        {
            get { return _id; }
            set { _id = value; }
        }
        //[SolrField("title")]
        //public System.String Title { get; set; }

        //[SolrField("description")]
        //public System.String Description { get; set; }

        [SolrField("title")]
        public string title { get; set; }

        [SolrField("slug")]
        public string slug { get; set; }

        [SolrField("description")]
        public string Description { get; set; }

        [SolrField("tags")]
        public string Tags { get; set; }

        [SolrField("filters")]
        public List<int> Filters { get; set; }

        [SolrField("bunchid")]
        public string BunchId { get; set; }

        [SolrField("isarchival")]
        public bool IsArchival { get; set; }

        //[SolrField("filterid")]
        //public int FilterId { get; set; }

        [SolrField("creationdate")]
        public DateTime CreationDate { get; set; }

        [SolrField("lastupdatedate")]
        public DateTime LastUpdateDate { get; set; }

        [SolrField("publishdate")]
        public DateTime PublishDate { get; set; }

    }
}
