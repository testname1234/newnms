﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SolrNet.Attributes;

namespace SolrManager.InputEntities
{
    public class SolrCelebrity
    {
        public string _id { get; set; }

        [SolrUniqueKey("id")]
        public string id
        {
            get { return _id; }
            set { _id = value; }
        }

        [SolrField("LastUpdateDate")]
        public DateTime LastUpdateDate { get; set; }

        [SolrField("Slug")]
        public string Slug { get; set; }
    }
}
