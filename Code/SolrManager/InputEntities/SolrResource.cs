﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SolrNet.Attributes;

namespace SolrManager.InputEntities
{
    public class SolrResource
    {
        public string Guid { get; set; }

        [SolrUniqueKey("id")]
        public string id
        {
            get { return Guid; }
            set { Guid = value; }
        }

        [SolrField("LastUpdateDate")]
        public DateTime LastUpdateDate { get; set; }

        [SolrField("ResourceTypeId")]
        public int ResourceTypeId { get; set; }

        [SolrField("FilePath")]
        public string FilePath { get; set; }

        [SolrField("FileName")]
        public string FileName { get; set; }

        [SolrField("Slug", Boost=10)]
        public string Slug { get; set; }

        [SolrField("description")]
        public string Description { get; set; }

        [SolrField("Metas", Boost=20)]
        public string Metas { get; set; }

        [SolrField("Keyword", Boost = 20)]
        public string Keyword { get; set; }

        [SolrField("BucketId")]
        public int BucketId { get; set; }

        [SolrField("Barcode")]
        public int Barcode { get; set; }

        [SolrField("IsPrivate")]
        public bool IsPrivate { get; set; }

        [SolrField("isHD")]
        public System.Boolean? isHD { get; set; }

        [SolrField("IsCompleted")]
        public bool IsCompleted { get; set; }

        [SolrField("ResourceDate")]       
        public DateTime ResourceDate { get; set; }

        [SolrField("CreationDate")]
        public DateTime CreationDate { get; set; }


        [SolrField("UserIds")]
        public List<int> UserIds { get; set; }

        [SolrField("DateStamp")]
        public long TimeStamp { get; set; }

        [SolrField("DeletedDateStamp")]
        public long DeletedTimeStamp { get; set; }

    }
}
