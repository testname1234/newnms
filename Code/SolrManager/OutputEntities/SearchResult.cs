﻿using System;
using System.Collections.Generic;

namespace SolrManager.OutputEntities
{
    public class SearchResult<T>
    {
        public List<T> Results { get; set; }

        

        public int TotalCount { get; set; }

        public string DidYouMean { get; set; }

        public bool QueryError { get; set; }
    }
}
