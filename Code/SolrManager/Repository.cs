﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Data;
//using System.Threading.Tasks;
//using SolrManager.InputEntities;

//namespace SolrManager
//{
//    public class Repository
//    {

//        string ConnectionString = @"Data Source=M1-0036\SQLEXPRESS;Initial Catalog=GeneratedFromErd;Integrated Security=True";
//        protected delegate T TFromDataRow<T>(DataRow dr);


//        // Get from DB-------------------------------------------
//        public List<Course> GetAllCourses()
//        {
//            string sql = "Select * from Course";
//            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, null);
//            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
//            return CollectionFromDataSet<Course>(ds, CourseFromDataRow);
//        }

//        public List<Student> GetAllStudents()
//        {
//            string sql = "Select * from Student";
//            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, null);
//            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
//            return CollectionFromDataSet<Student>(ds, StudentFromDataRow);
//        }


//        //Entities Mapping-----------------------------------------------
//        protected static List<T> CollectionFromDataSet<T>(DataSet ds, TFromDataRow<T> action)
//        {
//            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0)
//                return null;

//            List<T> list = new List<T>(ds.Tables[0].Rows.Count);
//            foreach (DataRow dr in ds.Tables[0].Rows)
//            {
//                list.Add(action(dr));
//            }

//            return list;
//        }
//        public virtual Course CourseFromDataRow(DataRow dr)
//        {
//            if (dr == null) return null;
//            Course entity = new Course();
//            if (dr.Table.Columns.Contains("CourseId"))
//            {
//                entity.CourseId = (System.Int32)dr["CourseId"];
//            }
//            if (dr.Table.Columns.Contains("Name"))
//            {
//                entity.Name = dr["Name"].ToString();
//            }
//            return entity;
//        }
//        public virtual Student StudentFromDataRow(DataRow dr)
//        {
//            if (dr == null) return null;
//            Student entity = new Student();
//            if (dr.Table.Columns.Contains("StudentId"))
//            {
//                entity.StudentId = (System.Int32)dr["StudentId"];
//            }
//            if (dr.Table.Columns.Contains("Name"))
//            {
//                entity.Name = dr["Name"].ToString();
//            }
//            if (dr.Table.Columns.Contains("Class"))
//            {
//                entity.Class = dr["Class"].ToString();
//            }
//            return entity;
//        }
//    }
//}
