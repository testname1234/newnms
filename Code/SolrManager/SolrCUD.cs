﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Practices.ServiceLocation;
using NMS.Core;
using NMS.MongoRepository;
using NMS.Repository;
using SolrManager.Enums;
using SolrNet;
using NMS.Core.Entities;
using NMS.Core.Entities.Mongo;
using SolrManager.InputEntities;





namespace SolrManager
{
    public class SolrCUD
    {
        public void Dump<T>(string Url)
        {
            System.Type tp = typeof(T);

            var solr = ServiceLocator.Current.GetInstance<ISolrOperations<SolarNews>>();
            MNewsRepository mNewsRep = new MNewsRepository();
            List<MNews> mNews = mNewsRep.GetByUpdatedDate(DateTime.Now.AddDays(-5));
            List<SolarNews> sNews = new List<SolarNews>();
            if (mNews != null && mNews.Count > 0)
            {
                sNews.CopyFrom(mNews);
                solr.Delete("*:*");
                solr.AddRange(sNews);
                solr.Commit();
            }


        }
        //public void Delete<T>(string Url)
        //{

        //    System.Type tp = typeof(T);

        //    if (tp.Name == EntityTypeES.ProductModel.ToString())
        //    {
        //        var solr = ServiceLocator.Current.GetInstance<ISolrOperations<ProductModel>>();
        //        solr.Delete(SolrQuery.All);
        //        solr.Commit();
        //    }
        //    if (tp.Name == EntityTypeES.CustomerShortModel.ToString())
        //    {
        //        var solr = ServiceLocator.Current.GetInstance<ISolrOperations<CustomerShortModel>>();
        //        solr.Delete(SolrQuery.All);
        //        solr.Commit();
        //    }
        //}
    }
}
