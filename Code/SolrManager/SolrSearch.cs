﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Practices.ServiceLocation;
using SolrManager.Enums;
using NMS.Core;
using SolrManager.OutputEntities;
using SolrNet;
using SolrNet.Commands.Parameters;


namespace SolrManager
{

    public class SolrSearch
    {

        public SearchResult<T> searchString<T>(string ToSearch, int pageSize, int pageNumber, string customerType = null)
        {
            System.Type tp = typeof(T);

            // Query will be individual
            var queryString = string.Empty;
            //-----------------------------------------------------------


            queryString = "title:(*" + ToSearch + "*) OR description:(*" + ToSearch + "*)";


            SearchParameters parameters = new SearchParameters();
            if (pageSize > 0 && pageNumber > 0)
            {
                parameters.PageSize = pageSize;
                parameters.PageIndex = pageNumber;
            }
            parameters.FreeSearch = ToSearch.ToLower();
            var solr = ServiceLocator.Current.GetInstance<ISolrReadOnlyOperations<T>>();


            var start = (parameters.PageIndex) * parameters.PageSize;
            var solrQuery = new SolrQuery(queryString);

            var matchingResult = solr.Query(solrQuery, new QueryOptions
            {
                Rows = parameters.PageSize,
                Start = start
            });

            SearchResult<T> result = new SearchResult<T>()
            {
                Results = matchingResult,
                TotalCount = matchingResult.NumFound

            };

            return result;
            
        }

        public SolrNet.SortOrder[] GetSelectedSort(SearchParameters parameters)
        {
            return new[] { SolrNet.SortOrder.Parse(parameters.Sort) }.Where(o => o != null).ToArray();
        }



    }

}
