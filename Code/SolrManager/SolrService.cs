﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Practices.ServiceLocation;

using SolrManager.Enums;
using SolrNet;

using SolrManager.InputEntities;
using SolrManager.OutputEntities;
using SolrNet.Commands.Parameters;
using SolrNet.Impl;

namespace SolrManager
{
    public class SolrService<T>
    {

        public void Connect(string url)
        {
            Startup.Init<T>(url);
        }

        public void AddRecords(List<T> data)
        {
            var solr = ServiceLocator.Current.GetInstance<ISolrOperations<T>>();
            solr.AddRange(data);
            solr.Commit();
        }

        public void DeleteById(string Id)
        {
            var solr = ServiceLocator.Current.GetInstance<ISolrOperations<T>>();
            solr.Delete(Id);
            solr.Commit();
        }

        public void Clear()
        {
            var solr = ServiceLocator.Current.GetInstance<ISolrOperations<T>>();
            solr.Delete(SolrQuery.All);
            solr.Commit();
        }

        public SearchResult<T> QuerySolr(string term, List<string> filters, string metas, int pageSize, int pageNumber, int? resourceTypeId, int? bucketid, bool alldata, List<SolrNet.SortOrder> sortParams = null, bool isPageIndex = false)
        {

           string FinalQuery = "(";
           string bucketdata = string.Empty;
           string[] allMetas = null;
           string metaquery = string.Empty;

           if (metas != null)
               allMetas = metas.Replace(" ", ",").Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);

           for (int f = 0; f < filters.Count(); f++)
           {
               string[] query = term.Replace(" ", ",").Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);

               for (int i = 0; i < query.Length; i++)
               {
                   if (i == 0)
                   {
                       string combinedterms = "";

                       if (query.Count() > 1 && metas != null)
                           combinedterms = filters[f] + ":"+ term + "* OR  "; 

                       FinalQuery += "( " + combinedterms  + filters[f] + ":*" + query[i];
                   }
                   else
                   {
                       FinalQuery += " AND " + filters[f] + ":" + query[i];
                   }

                   if (i == query.Length - 1)
                       FinalQuery += "*)";

               }

               if (f!=filters.Count-1)
                   FinalQuery += " OR ";

               if (f == filters.Count - 1)
                   FinalQuery += ")";
           }


           if (allMetas != null)
           {
               for (int i = 0; i < allMetas.Length; i++)
               {
                     if(i==0)
                         metaquery += "Metas:*" + allMetas[i];
                   else
                         metaquery += " AND Metas:" + allMetas[i];

                     //if (i == allMetas.Length - 1)
                     //    metaquery += "*)";
               }
           }

            if (metas != null && allMetas.Count() > 1)
               FinalQuery+= " AND ("  + metaquery + "*)";




            if (bucketid != null)
                bucketdata = "BucketId:" + bucketid;

            if (alldata)
            {
                if (bucketdata != string.Empty)
                    bucketdata += " OR IsPrivate:false";
                else
                    bucketdata = "IsPrivate:false";
            }
            else
            {
                if (bucketdata == string.Empty)
                    bucketdata = "IsPrivate:true";
            }

            bucketdata = "  AND (" + bucketdata + ")";
            FinalQuery += bucketdata;

            if (resourceTypeId != null)
            {
                FinalQuery += " AND (ResourceTypeId:" + resourceTypeId.Value + ")";
            }    


            System.Type tp = typeof(T);
            SearchParameters parameters = new SearchParameters();
            if (pageSize > 0)
            {
                parameters.PageSize = pageSize;
                parameters.PageIndex = pageNumber;
            }
            var solr = ServiceLocator.Current.GetInstance<ISolrReadOnlyOperations<T>>();
            SolrQueryResults<T> matchingResult = GetMatchingResultswithoutDismax(FinalQuery, sortParams, parameters, solr, isPageIndex);
            SearchResult<T> result = new SearchResult<T>()
            {
                Results = matchingResult,
                TotalCount = matchingResult.NumFound
            };
            return result;
        }

        public SearchResult<T> QuerySolr(string query, int pageSize, int pageNumber, List<SolrNet.SortOrder> sortParams = null, bool isPageIndex = false)
        {
            System.Type tp = typeof(T);
            SearchParameters parameters = new SearchParameters();
            if (pageSize > 0)
            {
                parameters.PageSize = pageSize;
                parameters.PageIndex = pageNumber;
            }
            var solr = ServiceLocator.Current.GetInstance<ISolrReadOnlyOperations<T>>();
            SolrQueryResults<T> matchingResult = GetMatchingResultswithoutDismax(query, sortParams, parameters, solr, isPageIndex);
            SearchResult<T> result = new SearchResult<T>()
            {
                Results = matchingResult,
                TotalCount = matchingResult.NumFound
            };
            return result;

        }

        public string GenerateQuery(string term, List<string> columns)
        {
            List<string> terms = RemoveStopWords(term);
            string query = string.Empty;
            List<string> columnQuery = new List<string>();
            foreach (var t in terms)
            {
                List<string> lst =new List<string>();
                foreach (var column in columns)
                {
                    lst.Add(string.Format("{0}:{1}{2}", column, t, terms.IndexOf(t) == terms.Count - 1 ? "*" : string.Empty));
                }
                columnQuery.Add("(" + string.Join(" OR ", lst.ToArray()) + ")");
            }
            return string.Join(" AND ", columnQuery.ToArray());
        }

        public string GenerateQueryRange(string term,string term2, List<string> columns)
        {
            List<string> terms = RemoveStopWords(Convert.ToString(term));
            string query = string.Empty;
            List<string> columnQuery = new List<string>();
            foreach (var t in terms)
            {
                
                List <string> lst = new List<string>();
                foreach (var column in columns)
                {
                    lst.Add(string.Format("{0}:[{1}{2}", column, t, terms.IndexOf(t) == terms.Count - 1 ? " TO "+term2+"]" : string.Empty));
                }
                columnQuery.Add("(" + string.Join(" OR ", lst.ToArray()) + ")");
            }
            return string.Join(" AND ", columnQuery.ToArray());
        }

        
        public string GenerateQueryRangewWithText(string text, string term, string term2, List<string> columns)
        {
            List<string> terms = RemoveStopWords(Convert.ToString(term));
            List<string> texts = RemoveStopWords(Convert.ToString(text));
            string query = string.Empty;
            List<string> columnQuery = new List<string>();
            //foreach (var t in texts)
            // {

            List<string> lst = new List<string>();
            foreach (var column in columns)
            {

                lst.Add(string.Format("{0}:{1}{2}", column, text, "*" + " AND " + string.Format("{0}:[{2}", "ResourceDate", text, term + " TO " + term2 + "]")));



            }

            columnQuery.Add("(" + string.Join(" OR ", lst.ToArray()) + ")");
            //}
            return string.Join(" AND ", columnQuery.ToArray());
        }
        private static SolrQueryResults<T> GetMatchingResultswithoutDismax(string query, List<SolrNet.SortOrder> sortParams, SearchParameters parameters, ISolrReadOnlyOperations<T> solr, bool isPageIndex = false)
        {

            var start = isPageIndex ? parameters.PageIndex : (parameters.PageIndex - 1) * parameters.PageSize;
            start = start == 1 ? 0 : start;
            var solrQuery = new SolrQuery(query);

            List<SolrNet.SortOrder> lstsortOrders = null;
            SolrQueryResults<T> matchingResult = null;
            if (sortParams != null)
            {
                matchingResult = solr.Query(solrQuery, new QueryOptions
                {
                    Rows = parameters.PageSize,
                    Start = start,
                    OrderBy = sortParams,
                    SpellCheck = new SpellCheckingParameters()
                    {
                        Build = true,
                        Collate = true,
                        Query = query
                    }
                });

            }
            else
            {
                matchingResult = solr.Query(solrQuery, new QueryOptions
                {
                    Rows = parameters.PageSize,
                    Start = start,
                    SpellCheck = new SpellCheckingParameters()
                    {
                        Build = true,
                        Collate = true,
                        Query = query
                    }
                });
            }
            return matchingResult;
        }

        public SearchResult<T> Search(string query, int pageSize, int pageNumber, string keywords)
        {
            //System.Type tp = typeof(T);

            //SearchParameters parameters = new SearchParameters();
            //if (pageSize > 0 && pageNumber > 0)
            //{
            //    parameters.PageSize = pageSize;
            //    parameters.PageIndex = pageNumber;
            //}
            //parameters.FreeSearch = keywords.ToLower();
            //var solr = ServiceLocator.Current.GetInstance<ISolrReadOnlyOperations<T>>();


            //var start = (parameters.PageIndex - 1) * parameters.PageSize;
            //var solrQuery = new SolrQuery(query);

            //var matchingResult = solr.Query(solrQuery, new QueryOptions
            //{
            //    Rows = parameters.PageSize,
            //    Start = start
            //});

            //SearchResult<T> result = new SearchResult<T>()
            //{
            //    Results = matchingResult,
            //    TotalCount = matchingResult.NumFound

            //};

            return SearchEDismax(keywords, pageSize, pageNumber, keywords, "title description", false);

            //return result;

        }

        public SearchResult<T> SearchEDismax(string query, int pageSize, int pageNumber, string keywords, string searchColumn, bool isSuggestion = true, string sortParams = null, string mm = "100%")
        {

            System.Type tp = typeof(T);

            SearchParameters parameters = new SearchParameters();
            if (pageSize > 0 && pageNumber > 0)
            {
                parameters.PageSize = pageSize;
                parameters.PageIndex = pageNumber;
            }
            if (!string.IsNullOrEmpty(sortParams))
                parameters.Sort = sortParams;

            var solr = ServiceLocator.Current.GetInstance<ISolrReadOnlyOperations<T>>();
            var matchingResult = GetMatchingResult(query, keywords, searchColumn, parameters, solr, mm);

            if (isSuggestion)
            {
                if (matchingResult.Count < 1 && pageNumber == 1)
                {
                    List<KeyValuePair<string, string>> spellcheckPairs = new List<KeyValuePair<string, string>>();
                    SpellCheckResults spellcheckresults = matchingResult.SpellChecking;

                    if (spellcheckresults != null)
                    {
                        foreach (SpellCheckResult item in spellcheckresults)
                        {
                            KeyValuePair<string, string> pair = new KeyValuePair<string, string>(item.Query, item.Suggestions.FirstOrDefault());
                            spellcheckPairs.Add(pair);
                        }
                    }

                    if (spellcheckPairs != null)
                    {
                        foreach (KeyValuePair<string, string> item in spellcheckPairs)
                        {
                            query = query.Replace(item.Key, item.Value);
                        }
                    }

                    if (spellcheckresults.Count() > 0)
                    {
                        matchingResult = GetMatchingResult(query, query, searchColumn, parameters, solr);
                    }
                }
            }

            SearchResult<T> result = new SearchResult<T>()
            {
                Results = matchingResult,
                TotalCount = matchingResult.NumFound

            };

            return result;

        }

        private static SolrQueryResults<T> GetMatchingResult(string query, string keywords, string searchColumn, SearchParameters parameters, ISolrReadOnlyOperations<T> solr, string mm = "100%")
        {
            //parameters.FreeSearch = "*" + keywords.ToLower() + "*";
            var start = (parameters.PageIndex - 1) * parameters.PageSize;
            //  query = "*" + query + "*";
            var solrQuery = new SolrQuery(query);
            // SpellCheckingParameters
            var localParams = new LocalParams { { "type", "edismax" }, { "qf", searchColumn }, { "mm", mm }, { "qs", "1000" }, { "qt", "spell" }, { "spellcheck", "true" } };
            if (!string.IsNullOrEmpty(parameters.Sort))
                localParams.Add("sort", parameters.Sort);

            var matchingResult = solr.Query(localParams + solrQuery, new QueryOptions
            {
                Rows = parameters.PageSize,
                Start = start,
                SpellCheck = new SpellCheckingParameters()
                {
                    Build = true,
                    Collate = true,
                    Query = query
                }
            });

            return matchingResult;
        }

        public SearchResult<T> SearchEDismax(string query, int pageSize, int pageNumber, string keywords, string searchColumn, List<SolrNet.SortOrder> sortOrder = null)
        {
            var filterQuery = query.Replace(keywords, "");
            keywords = keywords.Trim('*');
            string searchTitle = string.Empty;
            string searchDescription = string.Empty;
            string[] titles = keywords.Replace(" ", ",").Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
            foreach (string title in titles)
            {
                if (searchTitle == string.Empty)
                {
                    searchTitle = "title:*" + title;
                    searchDescription = "description:*" + title;
                }
                else
                {
                    searchTitle += " AND title:" + title;
                    searchDescription += " AND description:" + title;
                }
            }
            searchTitle = "AND ((" + searchTitle + "*)";

            searchDescription = "(" + searchDescription + "*))";
            query = filterQuery + " " + searchTitle + " OR " + searchDescription;

            return QuerySolr(query, pageSize, pageNumber, new List<SolrNet.SortOrder> { new SolrNet.SortOrder("publishdate", SolrNet.Order.DESC), new SolrNet.SortOrder("score", SolrNet.Order.DESC) }, true);

            System.Type tp = typeof(T);

            SearchParameters parameters = new SearchParameters();
            if (pageSize > 0 && pageNumber > 0)
            {
                parameters.PageSize = pageSize;
                parameters.PageIndex = pageNumber;
            }
            parameters.FreeSearch = keywords.ToLower();
            var solr = ServiceLocator.Current.GetInstance<ISolrReadOnlyOperations<T>>();


            var start = (parameters.PageIndex - 1) * parameters.PageSize;
            var solrQuery = new SolrQuery(query);
            var localParams = new LocalParams { { "type", "edismax" }, { "qf", searchColumn }, { "mm", "100%" }, { "qs", "1000" } };

            var matchingResult = solr.Query(localParams + solrQuery, new QueryOptions
            {
                Rows = parameters.PageSize,
                Start = start,
                OrderBy = sortOrder != null ? sortOrder : new List<SolrNet.SortOrder> { new SolrNet.SortOrder("score") }
            });

            SearchResult<T> result = new SearchResult<T>()
            {
                Results = matchingResult,
                TotalCount = matchingResult.NumFound

            };

            return result;

        }

        public SearchResult<T> Search(string query, int pageSize, int pageNumber, List<SolrNet.SortOrder> sortOrder = null)
        {

            //LowerCaseFilterFactory

            System.Type tp = typeof(T);

            SearchParameters parameters = new SearchParameters();
            if (pageSize > 0 && pageNumber > 0)
            {
                parameters.PageSize = pageSize;
                parameters.PageIndex = pageNumber;

            }
            //parameters.FreeSearch = keywords.ToLower();
            var solr = ServiceLocator.Current.GetInstance<ISolrReadOnlyOperations<T>>();


            var start = (parameters.PageIndex - 1) * parameters.PageSize;
            var solrQuery = new SolrQuery(query);



            var matchingResult = solr.Query(solrQuery, new QueryOptions
            {
                Rows = parameters.PageSize,
                Start = start,
                OrderBy = sortOrder != null ? sortOrder : new List<SolrNet.SortOrder> { new SolrNet.SortOrder("score") }
            });

            SearchResult<T> result = new SearchResult<T>()
            {
                Results = matchingResult,
                TotalCount = matchingResult.NumFound

            };

            return result;

        }

        public SearchResult<T> GetAllresultsBySearch(string query)
        {

            var solr = ServiceLocator.Current.GetInstance<ISolrReadOnlyOperations<T>>();
            var solrQuery = new SolrQuery(query);

            var matchingResult = solr.Query(solrQuery);
            SearchResult<T> result = new SearchResult<T>()
            {
                Results = matchingResult,
                TotalCount = matchingResult.NumFound

            };

            return result;
        }

        public SearchResult<T> Sort(string query, string sortColumn, Order order, int pageSize)
        {
            System.Type tp = typeof(T);

            SearchParameters parameters = new SearchParameters();
            if (pageSize > 0)
            {
                parameters.PageSize = pageSize;
                parameters.PageIndex = 1;
            }
            var solr = ServiceLocator.Current.GetInstance<ISolrReadOnlyOperations<T>>();


            var start = (parameters.PageIndex - 1) * parameters.PageSize;
            SolrQuery solrQuery = new SolrQuery(query);

            SolrNet.SortOrder sOrder = new SolrNet.SortOrder(sortColumn, order);

            QueryOptions options = new QueryOptions();
            options.OrderBy.Add(sOrder);
            options.Rows = parameters.PageSize;
            options.Start = start;
            var matchingResult = solr.Query(solrQuery, options);

            SearchResult<T> result = new SearchResult<T>()
            {
                Results = matchingResult,
                TotalCount = matchingResult.NumFound
            };

            return result;

        }

        public SolrNet.SortOrder[] GetSelectedSort(SearchParameters parameters)
        {
            return new[] { SolrNet.SortOrder.Parse(parameters.Sort) }.Where(o => o != null).ToArray();
        }

        static string StopWrdstr =
             @"a
about
above
after
again
against
all
am
an
and
any
are
aren't
as
at
be
because
been
before
being
below
between
both
but
by
can't
cannot
could
couldn't
did
didn't
do
does
doesn't
doing
don't
down
during
each
few
for
from
further
had
hadn't
has
hasn't
have
haven't
having
he
he'd
he'll
he's
her
here
here's
hers
herself
him
himself
his
how
how's
i
i'd
i'll
i'm
i've
if
in
into
is
isn't
it
it's
its
itself
let's
me
more
most
mustn't
my
myself
no
nor
not
of
off
on
once
only
or
other
ought
our
ours
ourselves
out
over
own
same
shan't
she
she'd
she'll
she's
should
shouldn't
so
some
such
than
that
that's
the
their
theirs
them
themselves
then
there
there's
these
they
they'd
they'll
they're
they've
this
those
through
to
too
under
until
up
very
was
wasn't
we
we'd
we'll
we're
we've
were
weren't
what
what's
when
when's
where
where's
which
while
who
who's
whom
why
why's
with
won't
would
wouldn't
you
you'd
you'll
you're
you've
your
yours
yourself
yourselves";
        public static List<string> StopWordsList = StopWrdstr.Split(new string[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries).ToList();

        public List<string> RemoveStopWords(string SearchTerm)
        {
            List<string> FilteredList = SearchTerm.Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries).ToList();
            List<string> ReturnFilteredList = new List<string>();
            foreach (string tmpString in FilteredList)
            {
                if (tmpString.Trim().Length > 0 && !StopWordsList.Any(x => x.ToString() == tmpString))
                {
                    ReturnFilteredList.Add(tmpString);
                }
            }
            return ReturnFilteredList;
        }
    }
}
