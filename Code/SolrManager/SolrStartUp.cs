﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SolrNet;

namespace SolrManager
{
   public class SolrStartUp
    {
        public  void StartSolrNet<T>(string Url)
        {   
            Startup.Init<T>(Url);         
        }
    }
}
