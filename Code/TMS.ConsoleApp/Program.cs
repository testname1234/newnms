﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using TMS.Service;
using TMS.Repository;
using TMS.Core;
using TMS.ProcessThreads;
using ControlPanel.Core;
using TMS.Core.DataInterfaces;
using TMS.Core.Entities;

namespace TMS.ConsoleApp
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            //ImportTickerThread thread = new ImportTickerThread();
            //thread.Execute("");
            var curTime = DateTime.Now;
            var files = Directory.GetFiles(AppDomain.CurrentDomain.BaseDirectory + "\\temp\\");
            IChannelRepository channelRepository = TMS.Core.IoC.Resolve<IChannelRepository>("TMSChannelRepository");
           
            var channel = channelRepository.GetChannelsForTickerMonitoring().Where(x=>x.ChannelId==37).First();
            ExtractSecondsImages thread = new ExtractSecondsImages();
            thread.Execute("");
            //thread.ExtractTickers(channel, DateTime.UtcNow, files);
            Console.WriteLine(DateTime.Now.Subtract(curTime).TotalSeconds.ToString());

            //ExtractTickers();
            Console.ReadKey();
        }

        /*
        public static void ExtractTickers()
        {
            string strpath = @"C:\Users\waqarullahkhan\Desktop\SnapshotsGeo";
            var files = !Directory.Exists(strpath) ? new List<string>() : Directory.GetFiles(strpath, "*.png").OrderBy(x => Convert.ToInt32(x.Split(new string[] { "\\" }, StringSplitOptions.RemoveEmptyEntries).Last().Replace("snap", "").Replace(".png", ""))).ToList();
            if (files.Count() == 0)
            {
                FFMPEGLib.FFMPEG.CreateSnapshots("i:\\geo.mp4", 1.0 / 3.0, strpath);
                files = Directory.GetFiles(strpath, "*.png").OrderBy(x => Convert.ToInt32(x.Split(new string[] { "\\" }, StringSplitOptions.RemoveEmptyEntries).Last().Replace("snap", "").Replace(".png", ""))).ToList();
            }
            string output = "c:\\temp";
            string outputFinal = "c:\\temp\\final";
            if (Directory.Exists(outputFinal))
                Directory.Delete(outputFinal, true);
            Directory.CreateDirectory(outputFinal);
            int i = 0;
            List<Point> polygonPoints = new List<Point>();
            polygonPoints.Add(new Point(0, 490));
            polygonPoints.Add(new Point(580, 490));
            polygonPoints.Add(new Point(580, 550));
            polygonPoints.Add(new Point(0, 550));

            LogoMatching matching = new LogoMatching();
            foreach (var file in files)
            {
                using (Bitmap bmp = new Bitmap(file))
                {
                    Graphics Ga = Graphics.FromImage(bmp);

                    SolidBrush Brush = new SolidBrush(Color.FromArgb(1, 1, 1));

                    var graphicPath = new System.Drawing.Drawing2D.GraphicsPath();
                    graphicPath.AddPolygon(polygonPoints.ToArray());
                    Region region = new Region();
                    region.Exclude(graphicPath);
                    Ga.FillRegion(Brush, region);
                    int minX = polygonPoints.Select(x => x.X).Min();
                    int maxX = polygonPoints.Select(x => x.X).Max();
                    int minY = polygonPoints.Select(x => x.Y).Min();
                    int maxY = polygonPoints.Select(x => x.Y).Max();
                    Rectangle rectcrop = new Rectangle(minX, minY, maxX - minX, maxY - minY);
                    bmp.Clone(rectcrop, bmp.PixelFormat).Save(output + "\\" + i + ".jpg");

                    List<List<IPoint>> iPointLists = new List<List<IPoint>>();
                    iPointLists.Add(new List<IPoint>());
                    iPointLists.Add(new List<IPoint>());
                    Bitmap img2 = new Bitmap(output + "\\" + i + ".jpg");
                    img2 = matching.MakeGrayscale3(img2);
                    iPointLists[0] = (matching.GetImagePoints(img2, 0.002f));
                    bool flag = false;
                    if (iPointLists[0].Count > 10)
                    {
                        var _files = Directory.GetFiles(outputFinal, "*.jpg").OrderBy(x => Convert.ToInt32(x.Split(new string[] { "\\" }, StringSplitOptions.RemoveEmptyEntries).Last().Split('-')[0].Replace("snap", "").Replace(".jpg", ""))).ToList();

                        for (int x = _files.Count() - 1; x >= (_files.Count() - 1) - 10 && x >= 0; x--)
                        {
                            Bitmap img1 = new Bitmap(_files[x]);
                            img1 = matching.MakeGrayscale3(img1);
                            iPointLists[1] = (matching.GetImagePoints(img1, 0.002f));

                            var matches = matching.GetMatchesOptimized(iPointLists[0], iPointLists[1]);
                            double matchLength = ((float)matches[0].Count / (float)iPointLists[0].Count) * 100.0;
                            if (matchLength > 50)
                            {
                                flag = true;
                                Console.WriteLine(i + "-" + matchLength);
                                break;
                            }
                        }
                        if (!flag)
                            File.Copy(output + "\\" + i + ".jpg", outputFinal + "\\" + i + "-" + iPointLists[0].Count + ".jpg");
                    }
                    i++;
                }
            }
        }*/
    }
}