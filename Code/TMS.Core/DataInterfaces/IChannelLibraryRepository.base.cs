﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using TMS.Core.Entities;

namespace TMS.Core.DataInterfaces
{
		
	public interface IChannelLibraryRepositoryBase
	{
        
        Dictionary<string, string> GetChannelLibraryBasicSearchColumns();
        List<SearchColumn> GetChannelLibrarySearchColumns();
        List<SearchColumn> GetChannelLibraryAdvanceSearchColumns();
        

		ChannelLibrary GetChannelLibrary(System.Int32 ChannelLibraryId,string SelectClause=null);
		ChannelLibrary UpdateChannelLibrary(ChannelLibrary entity);
		bool DeleteChannelLibrary(System.Int32 ChannelLibraryId);
		ChannelLibrary DeleteChannelLibrary(ChannelLibrary entity);
		List<ChannelLibrary> GetPagedChannelLibrary(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<ChannelLibrary> GetAllChannelLibrary(string SelectClause=null);
		ChannelLibrary InsertChannelLibrary(ChannelLibrary entity);
		List<ChannelLibrary> GetChannelLibraryByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);	}
	
	
}
