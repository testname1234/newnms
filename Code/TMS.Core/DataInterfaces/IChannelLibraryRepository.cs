﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using TMS.Core.Entities;
using OpenSURFcs;

namespace TMS.Core.DataInterfaces
{
		
	public interface IChannelLibraryRepository: IChannelLibraryRepositoryBase
	{

        ChannelLibrary GetChannelLibraryByChannelLocationId(int channelId, int channelLocationId);
    }
	
	
}
