﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using TMS.Core.Entities;

namespace TMS.Core.DataInterfaces
{
		
	public interface IChannelRepositoryBase
	{
        
        Dictionary<string, string> GetChannelBasicSearchColumns();
        List<SearchColumn> GetChannelSearchColumns();
        List<SearchColumn> GetChannelAdvanceSearchColumns();
        

		Channel GetChannel(System.Int32 ChannelId,string SelectClause=null);
		Channel UpdateChannel(Channel entity);
		bool DeleteChannel(System.Int32 ChannelId);
		Channel DeleteChannel(Channel entity);
		List<Channel> GetPagedChannel(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<Channel> GetAllChannel(string SelectClause=null);
		Channel InsertChannel(Channel entity);
		List<Channel> GetChannelByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);	}
	
	
}
