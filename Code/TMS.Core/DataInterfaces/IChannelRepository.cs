﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using TMS.Core.Entities;

namespace TMS.Core.DataInterfaces
{
    public interface IChannelRepository : IChannelRepositoryBase
    {
        List<Channel> GetChannels(DateTime lastUpdateDate);
        List<Channel> GetChannelByUserId(int userId);
        DateTime GetMaxChannelDate(int channelId);
        List<Channel> GetChannelsForTickerMonitoring();
    }
}
