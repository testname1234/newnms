﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using TMS.Core.Entities;

namespace TMS.Core.DataInterfaces
{
		
	public interface IChannelTickerLocationRepositoryBase
	{
        
        Dictionary<string, string> GetChannelTickerLocationBasicSearchColumns();
        List<SearchColumn> GetChannelTickerLocationSearchColumns();
        List<SearchColumn> GetChannelTickerLocationAdvanceSearchColumns();
        

		ChannelTickerLocation GetChannelTickerLocation(System.Int32 ChannelTickerLocationId,string SelectClause=null);
		ChannelTickerLocation UpdateChannelTickerLocation(ChannelTickerLocation entity);
		bool DeleteChannelTickerLocation(System.Int32 ChannelTickerLocationId);
		ChannelTickerLocation DeleteChannelTickerLocation(ChannelTickerLocation entity);
		List<ChannelTickerLocation> GetPagedChannelTickerLocation(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<ChannelTickerLocation> GetAllChannelTickerLocation(string SelectClause=null);
		ChannelTickerLocation InsertChannelTickerLocation(ChannelTickerLocation entity);
		List<ChannelTickerLocation> GetChannelTickerLocationByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);	}
	
	
}
