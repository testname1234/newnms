﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using TMS.Core.Entities;

namespace TMS.Core.DataInterfaces
{
		
	public interface IChannelTickerLocationRepository: IChannelTickerLocationRepositoryBase
	{
        List<ChannelTickerLocation> GetChannelTickerLocationByChannelId(int channelId);
	}
	
	
}
