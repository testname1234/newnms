﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using TMS.Core.Entities;

namespace TMS.Core.DataInterfaces
{
		
	public interface IChannelUserMappingRepositoryBase
	{
        
        Dictionary<string, string> GetChannelUserMappingBasicSearchColumns();
        List<SearchColumn> GetChannelUserMappingSearchColumns();
        List<SearchColumn> GetChannelUserMappingAdvanceSearchColumns();
        

		List<ChannelUserMapping> GetChannelUserMappingByChannelId(System.Int32? ChannelId,string SelectClause=null);
		ChannelUserMapping GetChannelUserMapping(System.Int32 ChannelUserMappingId,string SelectClause=null);
		ChannelUserMapping UpdateChannelUserMapping(ChannelUserMapping entity);
		bool DeleteChannelUserMapping(System.Int32 ChannelUserMappingId);
		ChannelUserMapping DeleteChannelUserMapping(ChannelUserMapping entity);
		List<ChannelUserMapping> GetPagedChannelUserMapping(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<ChannelUserMapping> GetAllChannelUserMapping(string SelectClause=null);
		ChannelUserMapping InsertChannelUserMapping(ChannelUserMapping entity);
		List<ChannelUserMapping> GetChannelUserMappingByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);	}
	
	
}
