﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using TMS.Core.Entities;

namespace TMS.Core.DataInterfaces
{
		
	public interface IExecutionRepositoryBase
	{
        
        Dictionary<string, string> GetExecutionBasicSearchColumns();
        List<SearchColumn> GetExecutionSearchColumns();
        List<SearchColumn> GetExecutionAdvanceSearchColumns();
        

		List<Execution> GetExecutionByTickerLineId(System.Int32? TickerLineId,string SelectClause=null);
		Execution GetExecution(System.Int32 ExecutionId,string SelectClause=null);
		Execution UpdateExecution(Execution entity);
		bool DeleteExecution(System.Int32 ExecutionId);
		Execution DeleteExecution(Execution entity);
		List<Execution> GetPagedExecution(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<Execution> GetAllExecution(string SelectClause=null);
		Execution InsertExecution(Execution entity);
		List<Execution> GetExecutionByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);	}
	
	
}
