﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using TMS.Core.Entities;

namespace TMS.Core.DataInterfaces
{
		
	public interface ITickerCategoryRepositoryBase
	{
        
        Dictionary<string, string> GetTickerCategoryBasicSearchColumns();
        List<SearchColumn> GetTickerCategorySearchColumns();
        List<SearchColumn> GetTickerCategoryAdvanceSearchColumns();
        

		TickerCategory GetTickerCategory(System.Int32 TickerCategoryId,string SelectClause=null);
		TickerCategory UpdateTickerCategory(TickerCategory entity);
		bool DeleteTickerCategory(System.Int32 TickerCategoryId);
		TickerCategory DeleteTickerCategory(TickerCategory entity);
		List<TickerCategory> GetPagedTickerCategory(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<TickerCategory> GetAllTickerCategory(string SelectClause=null);
		TickerCategory InsertTickerCategory(TickerCategory entity);
		List<TickerCategory> GetTickerCategoryByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);	}
	
	
}
