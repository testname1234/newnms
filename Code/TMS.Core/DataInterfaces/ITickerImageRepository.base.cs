﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using TMS.Core.Entities;

namespace TMS.Core.DataInterfaces
{
		
	public interface ITickerImageRepositoryBase
	{
        
        Dictionary<string, string> GetTickerImageBasicSearchColumns();
        List<SearchColumn> GetTickerImageSearchColumns();
        List<SearchColumn> GetTickerImageAdvanceSearchColumns();
        

		TickerImage GetTickerImage(System.Int32 TickerImageId,string SelectClause=null);
		TickerImage UpdateTickerImage(TickerImage entity);
		bool DeleteTickerImage(System.Int32 TickerImageId);
		TickerImage DeleteTickerImage(TickerImage entity);
		List<TickerImage> GetPagedTickerImage(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<TickerImage> GetAllTickerImage(string SelectClause=null);
		TickerImage InsertTickerImage(TickerImage entity);
		List<TickerImage> GetTickerImageByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);	}
	
	
}
