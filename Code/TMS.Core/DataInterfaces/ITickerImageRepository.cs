﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using TMS.Core.Entities;
using TMS.Core.Enums;

namespace TMS.Core.DataInterfaces
{
		
	public interface ITickerImageRepository: ITickerImageRepositoryBase
	{
        List<TickerImage> GetTickerImages(int? channelId, TickerImageStatuses tickerImageStatus, int pageIndex, int pageCount);
        bool UpdateTickerImageStatus(int tickerImageId, int statusId);
	}
	
	
}
