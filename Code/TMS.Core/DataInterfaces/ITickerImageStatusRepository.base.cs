﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using TMS.Core.Entities;

namespace TMS.Core.DataInterfaces
{
		
	public interface ITickerImageStatusRepositoryBase
	{
        
        Dictionary<string, string> GetTickerImageStatusBasicSearchColumns();
        List<SearchColumn> GetTickerImageStatusSearchColumns();
        List<SearchColumn> GetTickerImageStatusAdvanceSearchColumns();
        

		TickerImageStatus GetTickerImageStatus(System.Int32 TickerImageStatusId,string SelectClause=null);
		TickerImageStatus UpdateTickerImageStatus(TickerImageStatus entity);
		bool DeleteTickerImageStatus(System.Int32 TickerImageStatusId);
		TickerImageStatus DeleteTickerImageStatus(TickerImageStatus entity);
		List<TickerImageStatus> GetPagedTickerImageStatus(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<TickerImageStatus> GetAllTickerImageStatus(string SelectClause=null);
		TickerImageStatus InsertTickerImageStatus(TickerImageStatus entity);
		List<TickerImageStatus> GetTickerImageStatusByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);	}
	
	
}
