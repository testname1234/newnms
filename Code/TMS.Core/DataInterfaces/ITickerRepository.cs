﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using TMS.Core.Entities;

namespace TMS.Core.DataInterfaces
{
		
	public interface ITickerRepository: ITickerRepositoryBase
	{
        List<Ticker> GetTickers(string keyword);
	}
	
	
}
