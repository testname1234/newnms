﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using TMS.Core.Entities;

namespace TMS.Core.DataInterfaces
{
		
	public interface IVideoRepositoryBase
	{
        
        Dictionary<string, string> GetVideoBasicSearchColumns();
        List<SearchColumn> GetVideoSearchColumns();
        List<SearchColumn> GetVideoAdvanceSearchColumns();
        

		List<Video> GetVideoByVideoStatusId(System.Int32? VideoStatusId,string SelectClause=null);
		Video GetVideo(System.Int32 VideoId,string SelectClause=null);
		Video UpdateVideo(Video entity);
		bool DeleteVideo(System.Int32 VideoId);
		Video DeleteVideo(Video entity);
		List<Video> GetPagedVideo(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<Video> GetAllVideo(string SelectClause=null);
		Video InsertVideo(Video entity);
		List<Video> GetVideoByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);	}
	
	
}
