﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using TMS.Core.Entities;
using TMS.Core.Enums;

namespace TMS.Core.DataInterfaces
{
		
	public interface IVideoRepository: IVideoRepositoryBase
	{
        List<Video> GetVideos(int channelId, VideoStatuses videoStatus, DateTime? lastUpdateDate);
        bool Exist(int channelId, DateTime dateTime1, DateTime dateTime2);
        bool UpdateVideoStatus(int videoId, VideoStatuses videoStatus);
    }
	
	
}
