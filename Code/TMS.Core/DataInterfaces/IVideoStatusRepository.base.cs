﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using TMS.Core.Entities;

namespace TMS.Core.DataInterfaces
{
		
	public interface IVideoStatusRepositoryBase
	{
        
        Dictionary<string, string> GetVideoStatusBasicSearchColumns();
        List<SearchColumn> GetVideoStatusSearchColumns();
        List<SearchColumn> GetVideoStatusAdvanceSearchColumns();
        

		VideoStatus GetVideoStatus(System.Int32 VideoStatusId,string SelectClause=null);
		VideoStatus UpdateVideoStatus(VideoStatus entity);
		bool DeleteVideoStatus(System.Int32 VideoStatusId);
		VideoStatus DeleteVideoStatus(VideoStatus entity);
		List<VideoStatus> GetPagedVideoStatus(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null);
		List<VideoStatus> GetAllVideoStatus(string SelectClause=null);
		VideoStatus InsertVideoStatus(VideoStatus entity);
		List<VideoStatus> GetVideoStatusByKeyValue(string Key,string Value,Operands operand,string SelectClause=null);	}
	
	
}
