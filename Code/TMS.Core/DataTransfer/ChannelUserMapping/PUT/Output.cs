﻿using System;
using System.Runtime.Serialization;
using Validation;

namespace TMS.Core.DataTransfer.ChannelUserMapping
{
    [DataContract]
	public class PutOutput
	{
			
		[DataMember (EmitDefaultValue=false)]
		public System.Int32 ChannelUserMappingId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32? ChannelId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32? UserId{ get; set; }

	}	
}
