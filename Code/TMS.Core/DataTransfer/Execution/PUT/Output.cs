﻿using System;
using System.Runtime.Serialization;
using Validation;

namespace TMS.Core.DataTransfer.Execution
{
    [DataContract]
	public class PutOutput
	{
			
		[DataMember (EmitDefaultValue=false)]
		public System.Int32 ExecutionId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32? TickerLineId{ get; set; }

		[IgnoreDataMember]
		public System.DateTime? ExecutionTime{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public string ExecutionTimeStr
		{
			 get {if(ExecutionTime.HasValue) return ExecutionTime.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty;}
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { ExecutionTime = date.ToUniversalTime();  }  } 
		}

	}	
}
