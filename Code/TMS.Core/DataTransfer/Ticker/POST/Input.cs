﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Validation;

namespace TMS.Core.DataTransfer.Ticker
{
    [DataContract]
    public class PostInput
    {

        [DataMember(EmitDefaultValue = false)]
        public int TickerId { get; set; }

        [FieldLength(MaxLength = -1)]
        [FieldNullable(IsNullable = true)]
        [DataMember(EmitDefaultValue = false)]
        public string GroupName { get; set; }

        [FieldTypeValidation(DataType = DataTypes.Integer)]
        [FieldNullable(IsNullable = true)]
        [DataMember(EmitDefaultValue = false)]
        public string Severity { get; set; }

        [FieldTypeValidation(DataType = DataTypes.Integer)]
        [FieldNullable(IsNullable = true)]
        [DataMember(EmitDefaultValue = false)]
        public string TickerTypeId { get; set; }

        [FieldTypeValidation(DataType = DataTypes.Integer)]
        [FieldNullable(IsNullable = true)]
        [DataMember(EmitDefaultValue = false)]
        public string CategoryId { get; set; }

        [FieldTypeValidation(DataType = DataTypes.Integer)]
        [FieldNullable(IsNullable = true)]
        [DataMember(EmitDefaultValue = false)]
        public string UserId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string CreationDate { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string LastUpdatedDate { get; set; }

        [FieldTypeValidation(DataType = DataTypes.Boolean)]
        [FieldNullable(IsNullable = true)]
        [DataMember(EmitDefaultValue = false)]
        public string IsActive { get; set; }


        [DataMember(EmitDefaultValue = false)]
        public List<Core.DataTransfer.TickerLine.PostInput> TickerLines { get; set; }
    }
}
