﻿using System;
using System.Runtime.Serialization;
using Validation;

namespace TMS.Core.DataTransfer.TickerImageStatus
{
    [DataContract]
	public class PostOutput
	{
			
		[DataMember (EmitDefaultValue=false)]
		public System.Int32 TickerImageStatusId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.String TickerImageStatusName{ get; set; }

	}	
}
