﻿using System;
using System.Runtime.Serialization;
using Validation;

namespace TMS.Core.DataTransfer.TickerType
{
    [DataContract]
	public class PutOutput
	{
			
		[DataMember (EmitDefaultValue=false)]
		public System.Int32 TickerTypeId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.String TickerTypeName{ get; set; }

	}	
}
