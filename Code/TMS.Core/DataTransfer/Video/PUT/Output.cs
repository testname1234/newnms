﻿using System;
using System.Runtime.Serialization;
using Validation;

namespace TMS.Core.DataTransfer.Video
{
    [DataContract]
	public class PutOutput
	{
			
		[DataMember (EmitDefaultValue=false)]
		public System.Int32 VideoId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.String Url{ get; set; }

		[IgnoreDataMember]
		public System.DateTime? StartTime{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public string StartTimeStr
		{
			 get {if(StartTime.HasValue) return StartTime.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty;}
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { StartTime = date.ToUniversalTime();  }  } 
		}

		[IgnoreDataMember]
		public System.DateTime? EndTime{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public string EndTimeStr
		{
			 get {if(EndTime.HasValue) return EndTime.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty;}
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { EndTime = date.ToUniversalTime();  }  } 
		}

		[IgnoreDataMember]
		public System.DateTime? CreationDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public string CreationDateStr
		{
			 get {if(CreationDate.HasValue) return CreationDate.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty;}
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime();  }  } 
		}

		[IgnoreDataMember]
		public System.DateTime? LastUpdateDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public string LastUpdateDateStr
		{
			 get {if(LastUpdateDate.HasValue) return LastUpdateDate.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty;}
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { LastUpdateDate = date.ToUniversalTime();  }  } 
		}

		[DataMember (EmitDefaultValue=false)]
		public System.Boolean? IsActive{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32? ChannelId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.Int32? VideoStatusId{ get; set; }

	}	
}
