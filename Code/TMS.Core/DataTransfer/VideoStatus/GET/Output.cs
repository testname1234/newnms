﻿using System;
using System.Runtime.Serialization;
using Validation;

namespace TMS.Core.DataTransfer.VideoStatus
{
    [DataContract]
	public class GetOutput
	{
			
		[DataMember (EmitDefaultValue=false)]
		public System.Int32 VideoStatusId{ get; set; }

		[DataMember (EmitDefaultValue=false)]
		public System.String VideoStatusName{ get; set; }

	}	
}
