﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace TMS.Core.Entities
{
    [DataContract]
	public abstract partial class ChannelBase:EntityBase, IEquatable<ChannelBase>
	{
			
		[PrimaryKey]
		[FieldNameAttribute("ChannelId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 ChannelId{ get; set; }

		[FieldNameAttribute("Name",true,false,0)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Name{ get; set; }

		[FieldNameAttribute("CreationDate",true,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime? CreationDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string CreationDateStr
		{
			 get {if(CreationDate.HasValue) return CreationDate.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty;}
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("LastUpdateDate",true,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime? LastUpdateDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string LastUpdateDateStr
		{
			 get {if(LastUpdateDate.HasValue) return LastUpdateDate.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty;}
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { LastUpdateDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("IsActive",true,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean? IsActive{ get; set; }

		[FieldNameAttribute("IsTickerMonitoringOn",true,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean? IsTickerMonitoringOn{ get; set; }

		[FieldNameAttribute("ImagesSharePath",true,false,255)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String ImagesSharePath{ get; set; }

		[FieldNameAttribute("ReelPath",true,false,200)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String ReelPath{ get; set; }

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<ChannelBase> Members

        public virtual bool Equals(ChannelBase other)
        {
			if(this.ChannelId==other.ChannelId  && this.Name==other.Name  && this.CreationDate==other.CreationDate  && this.LastUpdateDate==other.LastUpdateDate  && this.IsActive==other.IsActive  && this.IsTickerMonitoringOn==other.IsTickerMonitoringOn  && this.ImagesSharePath==other.ImagesSharePath  && this.ReelPath==other.ReelPath )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(Channel other)
        {
			if(other!=null)
			{
				this.ChannelId=other.ChannelId;
				this.Name=other.Name;
				this.CreationDate=other.CreationDate;
				this.LastUpdateDate=other.LastUpdateDate;
				this.IsActive=other.IsActive;
				this.IsTickerMonitoringOn=other.IsTickerMonitoringOn;
				this.ImagesSharePath=other.ImagesSharePath;
				this.ReelPath=other.ReelPath;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
