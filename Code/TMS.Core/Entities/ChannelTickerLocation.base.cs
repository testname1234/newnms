﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace TMS.Core.Entities
{
    [DataContract]
	public abstract partial class ChannelTickerLocationBase:EntityBase, IEquatable<ChannelTickerLocationBase>
	{
			
		[PrimaryKey]
		[FieldNameAttribute("ChannelTickerLocationId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 ChannelTickerLocationId{ get; set; }

		[FieldNameAttribute("ChannelId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 ChannelId{ get; set; }

		[FieldNameAttribute("Points",false,false,1000)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Points{ get; set; }

		[FieldNameAttribute("IsActive",false,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean IsActive{ get; set; }

		[FieldNameAttribute("CreationDate",false,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime CreationDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string CreationDateStr
		{
			 get { return CreationDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("LastUpdateDate",false,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime LastUpdateDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string LastUpdateDateStr
		{
			 get { return LastUpdateDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { LastUpdateDate = date.ToUniversalTime();  }  } 
		}

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<ChannelTickerLocationBase> Members

        public virtual bool Equals(ChannelTickerLocationBase other)
        {
			if(this.ChannelTickerLocationId==other.ChannelTickerLocationId  && this.ChannelId==other.ChannelId  && this.Points==other.Points  && this.IsActive==other.IsActive  && this.CreationDate==other.CreationDate  && this.LastUpdateDate==other.LastUpdateDate )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(ChannelTickerLocation other)
        {
			if(other!=null)
			{
				this.ChannelTickerLocationId=other.ChannelTickerLocationId;
				this.ChannelId=other.ChannelId;
				this.Points=other.Points;
				this.IsActive=other.IsActive;
				this.CreationDate=other.CreationDate;
				this.LastUpdateDate=other.LastUpdateDate;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
