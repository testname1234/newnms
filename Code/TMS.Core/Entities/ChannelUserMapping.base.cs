﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace TMS.Core.Entities
{
    [DataContract]
	public abstract partial class ChannelUserMappingBase:EntityBase, IEquatable<ChannelUserMappingBase>
	{
			
		[PrimaryKey]
		[FieldNameAttribute("ChannelUserMappingId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 ChannelUserMappingId{ get; set; }

		[FieldNameAttribute("ChannelId",true,true,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? ChannelId{ get; set; }

		[FieldNameAttribute("UserId",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? UserId{ get; set; }

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<ChannelUserMappingBase> Members

        public virtual bool Equals(ChannelUserMappingBase other)
        {
			if(this.ChannelUserMappingId==other.ChannelUserMappingId  && this.ChannelId==other.ChannelId  && this.UserId==other.UserId )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(ChannelUserMapping other)
        {
			if(other!=null)
			{
				this.ChannelUserMappingId=other.ChannelUserMappingId;
				this.ChannelId=other.ChannelId;
				this.UserId=other.UserId;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
