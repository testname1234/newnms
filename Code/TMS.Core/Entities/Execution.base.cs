﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace TMS.Core.Entities
{
    [DataContract]
	public abstract partial class ExecutionBase:EntityBase, IEquatable<ExecutionBase>
	{
			
		[PrimaryKey]
		[FieldNameAttribute("ExecutionId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 ExecutionId{ get; set; }

		[FieldNameAttribute("TickerLineId",true,true,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? TickerLineId{ get; set; }

		[FieldNameAttribute("ExecutionTime",true,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime? ExecutionTime{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string ExecutionTimeStr
		{
			 get {if(ExecutionTime.HasValue) return ExecutionTime.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty;}
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { ExecutionTime = date.ToUniversalTime();  }  } 
		}

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<ExecutionBase> Members

        public virtual bool Equals(ExecutionBase other)
        {
			if(this.ExecutionId==other.ExecutionId  && this.TickerLineId==other.TickerLineId  && this.ExecutionTime==other.ExecutionTime )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(Execution other)
        {
			if(other!=null)
			{
				this.ExecutionId=other.ExecutionId;
				this.TickerLineId=other.TickerLineId;
				this.ExecutionTime=other.ExecutionTime;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
