﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace NMS.Core.Entities
{
    [DataContract]
	public abstract partial class TickerBase:EntityBase, IEquatable<TickerBase>
	{
			
		[PrimaryKey]
		[FieldNameAttribute("TickerId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 TickerId{ get; set; }

		[FieldNameAttribute("CategoryId",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? CategoryId{ get; set; }

		[FieldNameAttribute("NewsGuid",true,false,50)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String NewsGuid{ get; set; }

		[FieldNameAttribute("UserId",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? UserId{ get; set; }

		[FieldNameAttribute("OnAiredTime",true,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime? OnAiredTime{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string OnAiredTimeStr
		{
			 get {if(OnAiredTime.HasValue) return OnAiredTime.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty;}
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { OnAiredTime = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("SequenceId",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? SequenceId{ get; set; }

		[FieldNameAttribute("CreationDate",true,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime? CreationDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string CreationDateStr
		{
			 get {if(CreationDate.HasValue) return CreationDate.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty;}
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("LastUpdatedDate",true,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime? LastUpdatedDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string LastUpdatedDateStr
		{
			 get {if(LastUpdatedDate.HasValue) return LastUpdatedDate.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty;}
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { LastUpdatedDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("IsActive",true,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean? IsActive{ get; set; }

		[FieldNameAttribute("CreatedBy",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? CreatedBy{ get; set; }

		[FieldNameAttribute("TickerGroupName",true,false,0)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String TickerGroupName{ get; set; }

		[FieldNameAttribute("LocationId",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? LocationId{ get; set; }

		[FieldNameAttribute("OnAirRefId",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? OnAirRefId{ get; set; }

		[FieldNameAttribute("NewsFileId",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? NewsFileId{ get; set; }

		[FieldNameAttribute("IsApproved",false,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean IsApproved{ get; set; }

		[FieldNameAttribute("ViewCount",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 ViewCount{ get; set; }

		[FieldNameAttribute("Text",true,false,0)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Text{ get; set; }

		[FieldNameAttribute("StatusId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 StatusId{ get; set; }

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<TickerBase> Members

        public virtual bool Equals(TickerBase other)
        {
			if(this.TickerId==other.TickerId  && this.CategoryId==other.CategoryId  && this.NewsGuid==other.NewsGuid  && this.UserId==other.UserId  && this.OnAiredTime==other.OnAiredTime  && this.SequenceId==other.SequenceId  && this.CreationDate==other.CreationDate  && this.LastUpdatedDate==other.LastUpdatedDate  && this.IsActive==other.IsActive  && this.TickerGroupName==other.TickerGroupName  && this.LocationId==other.LocationId  && this.OnAirRefId==other.OnAirRefId  && this.NewsFileId==other.NewsFileId  && this.IsApproved==other.IsApproved  && this.ViewCount==other.ViewCount  && this.Text==other.Text  && this.StatusId==other.StatusId )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(Ticker other)
        {
			if(other!=null)
			{
				this.TickerId=other.TickerId;
				this.CategoryId=other.CategoryId;
				this.NewsGuid=other.NewsGuid;
				this.UserId=other.UserId;
				this.OnAiredTime=other.OnAiredTime;
				this.SequenceId=other.SequenceId;
				this.CreationDate=other.CreationDate;
				this.LastUpdatedDate=other.LastUpdatedDate;
				this.IsActive=other.IsActive;
				this.CreatedBy=other.CreatedBy;
				this.TickerGroupName=other.TickerGroupName;
				this.LocationId=other.LocationId;
				this.OnAirRefId=other.OnAirRefId;
				this.NewsFileId=other.NewsFileId;
				this.IsApproved=other.IsApproved;
				this.ViewCount=other.ViewCount;
				this.Text=other.Text;
				this.StatusId=other.StatusId;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
