﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace TMS.Core.Entities
{
    [DataContract]
    public partial class Ticker : TickerBase
    {
        public List<TickerLine> TickerLines { get; set; }
    }
}