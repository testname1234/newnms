﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace TMS.Core.Entities
{
    [DataContract]
	public abstract partial class TickerImageBase:EntityBase, IEquatable<TickerImageBase>
	{
			
		[PrimaryKey]
		[FieldNameAttribute("TickerImageId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 TickerImageId{ get; set; }

		[FieldNameAttribute("ChannelId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 ChannelId{ get; set; }

		[FieldNameAttribute("FileDate",false,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime FileDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string FileDateStr
		{
			 get { return FileDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { FileDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("GroupId",false,false,16)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Guid GroupId{ get; set; }

		[FieldNameAttribute("ImagePath",false,false,500)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String ImagePath{ get; set; }

		[FieldNameAttribute("ChannelTickerLocationId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 ChannelTickerLocationId{ get; set; }

		[FieldNameAttribute("CreationDate",false,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime CreationDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string CreationDateStr
		{
			 get { return CreationDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("LastUpdateDate",false,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime LastUpdateDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string LastUpdateDateStr
		{
			 get { return LastUpdateDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { LastUpdateDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("TickerImageStatusId",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? TickerImageStatusId{ get; set; }

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<TickerImageBase> Members

        public virtual bool Equals(TickerImageBase other)
        {
			if(this.TickerImageId==other.TickerImageId  && this.ChannelId==other.ChannelId  && this.FileDate==other.FileDate  && this.GroupId==other.GroupId  && this.ImagePath==other.ImagePath  && this.ChannelTickerLocationId==other.ChannelTickerLocationId  && this.CreationDate==other.CreationDate  && this.LastUpdateDate==other.LastUpdateDate  && this.TickerImageStatusId==other.TickerImageStatusId )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(TickerImage other)
        {
			if(other!=null)
			{
				this.TickerImageId=other.TickerImageId;
				this.ChannelId=other.ChannelId;
				this.FileDate=other.FileDate;
				this.GroupId=other.GroupId;
				this.ImagePath=other.ImagePath;
				this.ChannelTickerLocationId=other.ChannelTickerLocationId;
				this.CreationDate=other.CreationDate;
				this.LastUpdateDate=other.LastUpdateDate;
				this.TickerImageStatusId=other.TickerImageStatusId;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
