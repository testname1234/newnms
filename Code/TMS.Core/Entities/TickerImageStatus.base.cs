﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace TMS.Core.Entities
{
    [DataContract]
	public abstract partial class TickerImageStatusBase:EntityBase, IEquatable<TickerImageStatusBase>
	{
			
		[PrimaryKey]
		[FieldNameAttribute("TickerImageStatusId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 TickerImageStatusId{ get; set; }

		[FieldNameAttribute("TickerImageStatusName",true,false,100)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String TickerImageStatusName{ get; set; }

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<TickerImageStatusBase> Members

        public virtual bool Equals(TickerImageStatusBase other)
        {
			if(this.TickerImageStatusId==other.TickerImageStatusId  && this.TickerImageStatusName==other.TickerImageStatusName )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(TickerImageStatus other)
        {
			if(other!=null)
			{
				this.TickerImageStatusId=other.TickerImageStatusId;
				this.TickerImageStatusName=other.TickerImageStatusName;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
