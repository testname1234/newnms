﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace TMS.Core.Entities
{
    [DataContract]
	public abstract partial class TickerLineBase:EntityBase, IEquatable<TickerLineBase>
	{
			
		[PrimaryKey]
		[FieldNameAttribute("TickerLineId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 TickerLineId{ get; set; }

		[FieldNameAttribute("Text",true,false,0)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Text{ get; set; }

		[FieldNameAttribute("LanguageCode",true,false,50)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String LanguageCode{ get; set; }

		[FieldNameAttribute("TickerId",true,true,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? TickerId{ get; set; }

		[FieldNameAttribute("SequenceId",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? SequenceId{ get; set; }

		[FieldNameAttribute("ChannelId",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? ChannelId{ get; set; }

		[FieldNameAttribute("CreationDate",true,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime? CreationDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string CreationDateStr
		{
			 get {if(CreationDate.HasValue) return CreationDate.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty;}
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("LastUpdatedDate",true,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime? LastUpdatedDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string LastUpdatedDateStr
		{
			 get {if(LastUpdatedDate.HasValue) return LastUpdatedDate.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty;}
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { LastUpdatedDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("IsActive",true,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean? IsActive{ get; set; }

		[FieldNameAttribute("UserId",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? UserId{ get; set; }

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<TickerLineBase> Members

        public virtual bool Equals(TickerLineBase other)
        {
			if(this.TickerLineId==other.TickerLineId  && this.Text==other.Text  && this.LanguageCode==other.LanguageCode  && this.TickerId==other.TickerId  && this.SequenceId==other.SequenceId  && this.ChannelId==other.ChannelId  && this.CreationDate==other.CreationDate  && this.LastUpdatedDate==other.LastUpdatedDate  && this.IsActive==other.IsActive  && this.UserId==other.UserId )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(TickerLine other)
        {
			if(other!=null)
			{
				this.TickerLineId=other.TickerLineId;
				this.Text=other.Text;
				this.LanguageCode=other.LanguageCode;
				this.TickerId=other.TickerId;
				this.SequenceId=other.SequenceId;
				this.ChannelId=other.ChannelId;
				this.CreationDate=other.CreationDate;
				this.LastUpdatedDate=other.LastUpdatedDate;
				this.IsActive=other.IsActive;
				this.UserId=other.UserId;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
