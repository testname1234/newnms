﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace TMS.Core.Entities
{
    [DataContract]
	public abstract partial class TickerTypeBase:EntityBase, IEquatable<TickerTypeBase>
	{
			
		[PrimaryKey]
		[FieldNameAttribute("TickerTypeId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 TickerTypeId{ get; set; }

		[FieldNameAttribute("TickerTypeName",true,false,250)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String TickerTypeName{ get; set; }

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<TickerTypeBase> Members

        public virtual bool Equals(TickerTypeBase other)
        {
			if(this.TickerTypeId==other.TickerTypeId  && this.TickerTypeName==other.TickerTypeName )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(TickerType other)
        {
			if(other!=null)
			{
				this.TickerTypeId=other.TickerTypeId;
				this.TickerTypeName=other.TickerTypeName;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
