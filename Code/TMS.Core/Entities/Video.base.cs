﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace TMS.Core.Entities
{
    [DataContract]
	public abstract partial class VideoBase:EntityBase, IEquatable<VideoBase>
	{
			
		[PrimaryKey]
		[FieldNameAttribute("VideoId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 VideoId{ get; set; }

		[FieldNameAttribute("Url",true,false,0)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String Url{ get; set; }

		[FieldNameAttribute("StartTime",true,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime? StartTime{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string StartTimeStr
		{
			 get {if(StartTime.HasValue) return StartTime.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty;}
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { StartTime = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("EndTime",true,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime? EndTime{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string EndTimeStr
		{
			 get {if(EndTime.HasValue) return EndTime.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty;}
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { EndTime = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("CreationDate",true,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime? CreationDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string CreationDateStr
		{
			 get {if(CreationDate.HasValue) return CreationDate.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty;}
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { CreationDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("LastUpdateDate",true,false,8)]
		[IgnoreDataMember]
		public virtual System.DateTime? LastUpdateDate{ get; set;}


		[DataMember (EmitDefaultValue=false)]
		public virtual string LastUpdateDateStr
		{
			 get {if(LastUpdateDate.HasValue) return LastUpdateDate.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); else return string.Empty;}
			 set  {  DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { LastUpdateDate = date.ToUniversalTime();  }  } 
		}

		[FieldNameAttribute("IsActive",true,false,1)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Boolean? IsActive{ get; set; }

		[FieldNameAttribute("ChannelId",true,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? ChannelId{ get; set; }

		[FieldNameAttribute("VideoStatusId",true,true,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32? VideoStatusId{ get; set; }

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<VideoBase> Members

        public virtual bool Equals(VideoBase other)
        {
			if(this.VideoId==other.VideoId  && this.Url==other.Url  && this.StartTime==other.StartTime  && this.EndTime==other.EndTime  && this.CreationDate==other.CreationDate  && this.LastUpdateDate==other.LastUpdateDate  && this.IsActive==other.IsActive  && this.ChannelId==other.ChannelId  && this.VideoStatusId==other.VideoStatusId )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(Video other)
        {
			if(other!=null)
			{
				this.VideoId=other.VideoId;
				this.Url=other.Url;
				this.StartTime=other.StartTime;
				this.EndTime=other.EndTime;
				this.CreationDate=other.CreationDate;
				this.LastUpdateDate=other.LastUpdateDate;
				this.IsActive=other.IsActive;
				this.ChannelId=other.ChannelId;
				this.VideoStatusId=other.VideoStatusId;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
