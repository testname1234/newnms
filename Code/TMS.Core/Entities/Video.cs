﻿
using System;
using System.ComponentModel;
using System.Collections;
using System.Runtime.Serialization;
using TMS.Core.Enums;


namespace TMS.Core.Entities
{
    [DataContract]
	public partial class Video : VideoBase 
	{
        public VideoStatuses VideoStatus { get { return (VideoStatuses)base.VideoStatusId; } set { base.VideoStatusId = (int)value; } }
	
		
	}
}
