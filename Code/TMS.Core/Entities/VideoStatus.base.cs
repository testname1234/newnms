﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace TMS.Core.Entities
{
    [DataContract]
	public abstract partial class VideoStatusBase:EntityBase, IEquatable<VideoStatusBase>
	{
			
		[PrimaryKey]
		[FieldNameAttribute("VideoStatusId",false,false,4)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.Int32 VideoStatusId{ get; set; }

		[FieldNameAttribute("VideoStatusName",true,false,100)]
		[DataMember (EmitDefaultValue=false)]
		public virtual System.String VideoStatusName{ get; set; }

		
		public virtual bool IsTransient()
        {

            return EntityHelper.IsTransient(this);
        }
		
		#region IEquatable<VideoStatusBase> Members

        public virtual bool Equals(VideoStatusBase other)
        {
			if(this.VideoStatusId==other.VideoStatusId  && this.VideoStatusName==other.VideoStatusName )
			{
				return true;
			}
			else
			{
				return false;
			}
		
		}
		
		public virtual void CopyFrom(VideoStatus other)
        {
			if(other!=null)
			{
				this.VideoStatusId=other.VideoStatusId;
				this.VideoStatusName=other.VideoStatusName;
			}
			
		
		}

        #endregion
		
		
		
	}
	
	
}
