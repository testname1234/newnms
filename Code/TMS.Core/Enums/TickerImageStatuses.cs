﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TMS.Core.Enums
{
    public enum TickerImageStatuses
    {
        Pending = 1,
        Skipped,
        Completed
    }
}
