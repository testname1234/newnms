﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TMS.Core.Enums
{
    public enum VideoStatuses
    {
        Pending = 1,
        Skipped,
        Completed
    }
}
