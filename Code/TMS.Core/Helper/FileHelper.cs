﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TMS.Core.Helper
{
    public class FileHelper
    {
        public static byte[] GetImageFromCache(string filePath)
        {
            return File.ReadAllBytes(filePath);
        }

        public static byte[] GetPartialFile(string filePath, int from, int to, out long totalLength)
        {
            byte[] data;
            using (FileStream file = new FileStream(filePath, FileMode.Open))
            {
                totalLength = file.Length;
                if (to > 0)
                    data = new byte[to - from];
                else data = new byte[totalLength - from];

                file.Seek(from, SeekOrigin.Begin);
                
                file.Read(data, 0, data.Count());
                file.Close();
            }
            return data;
        }

        public static void SaveImageFromCache(string filePath, byte[] bytes)
        {
            File.WriteAllBytes(filePath, bytes);
        }
    }
}
