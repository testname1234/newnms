﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TMS.Core.Helper
{
    public class FormatHelper
    {
        public static string FormatTimeSpan(TimeSpan ts)
        {
            return ts.Hours.ToString("00") + ":" + ts.Minutes.ToString("00") + ":" + ts.Seconds.ToString("00");
        }

        public static string ShortenText(string text, int maxLength)
        {
            int splitIndex = ((text.Length > maxLength) ? maxLength : text.Length);
            foreach (char ch in text.Skip(splitIndex))
            {
                if (ch != ' ' && ch != '.' && ch != '?' && ch != '!' && ch != ',')
                    splitIndex++;
                else break;
            }
            return text.Substring(0, splitIndex) + ((splitIndex < text.Length) ? "..." : "");
        }
    }
}
