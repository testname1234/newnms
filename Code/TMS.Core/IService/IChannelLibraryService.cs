﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using TMS.Core.Entities;
using TMS.Core.DataTransfer;
using TMS.Core.DataTransfer.ChannelLibrary;

namespace TMS.Core.IService
{
		
	public interface IChannelLibraryService
	{
        Dictionary<string, string> GetChannelLibraryBasicSearchColumns();
        
        List<SearchColumn> GetChannelLibraryAdvanceSearchColumns();

		ChannelLibrary GetChannelLibrary(System.Int32 ChannelLibraryId);
		DataTransfer<List<GetOutput>> GetAll();
		ChannelLibrary UpdateChannelLibrary(ChannelLibrary entity);
		bool DeleteChannelLibrary(System.Int32 ChannelLibraryId);
		List<ChannelLibrary> GetAllChannelLibrary();
		ChannelLibrary InsertChannelLibrary(ChannelLibrary entity);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);
	}
	
	
}
