﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using TMS.Core.Entities;
using TMS.Core.DataTransfer;
using TMS.Core.DataTransfer.Channel;

namespace TMS.Core.IService
{
		
	public interface IChannelService
	{
        Dictionary<string, string> GetChannelBasicSearchColumns();
        
        List<SearchColumn> GetChannelAdvanceSearchColumns();

		Channel GetChannel(System.Int32 ChannelId);
		DataTransfer<List<GetOutput>> GetAll();
		Channel UpdateChannel(Channel entity);
		bool DeleteChannel(System.Int32 ChannelId);
		List<Channel> GetAllChannel();
		Channel InsertChannel(Channel entity);
        List<Channel> GetChannels(DateTime lastUpdateDate);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);

        DateTime GetMaxChannelDate(int channelId);

        List<Channel> GetChannelByUserId(int userId);
    }
	
	
}
