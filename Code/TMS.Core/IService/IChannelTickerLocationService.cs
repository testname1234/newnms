﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using TMS.Core.Entities;
using TMS.Core.DataTransfer;
using TMS.Core.DataTransfer.ChannelTickerLocation;

namespace TMS.Core.IService
{
		
	public interface IChannelTickerLocationService
	{
        Dictionary<string, string> GetChannelTickerLocationBasicSearchColumns();
        
        List<SearchColumn> GetChannelTickerLocationAdvanceSearchColumns();

		ChannelTickerLocation GetChannelTickerLocation(System.Int32 ChannelTickerLocationId);
		DataTransfer<List<GetOutput>> GetAll();
		ChannelTickerLocation UpdateChannelTickerLocation(ChannelTickerLocation entity);
		bool DeleteChannelTickerLocation(System.Int32 ChannelTickerLocationId);
		List<ChannelTickerLocation> GetAllChannelTickerLocation();
		ChannelTickerLocation InsertChannelTickerLocation(ChannelTickerLocation entity);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);
	}
	
	
}
