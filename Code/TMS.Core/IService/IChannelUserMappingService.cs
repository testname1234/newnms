﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using TMS.Core.Entities;
using TMS.Core.DataTransfer;
using TMS.Core.DataTransfer.ChannelUserMapping;

namespace TMS.Core.IService
{
		
	public interface IChannelUserMappingService
	{
        Dictionary<string, string> GetChannelUserMappingBasicSearchColumns();
        
        List<SearchColumn> GetChannelUserMappingAdvanceSearchColumns();

		List<ChannelUserMapping> GetChannelUserMappingByChannelId(System.Int32? ChannelId);
		ChannelUserMapping GetChannelUserMapping(System.Int32 ChannelUserMappingId);
		DataTransfer<List<GetOutput>> GetAll();
		ChannelUserMapping UpdateChannelUserMapping(ChannelUserMapping entity);
		bool DeleteChannelUserMapping(System.Int32 ChannelUserMappingId);
		List<ChannelUserMapping> GetAllChannelUserMapping();
		ChannelUserMapping InsertChannelUserMapping(ChannelUserMapping entity);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);
	}
	
	
}
