﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using TMS.Core.Entities;
using TMS.Core.DataTransfer;
using TMS.Core.DataTransfer.Execution;

namespace TMS.Core.IService
{
		
	public interface IExecutionService
	{
        Dictionary<string, string> GetExecutionBasicSearchColumns();
        
        List<SearchColumn> GetExecutionAdvanceSearchColumns();

		Execution GetExecution(System.Int32 ExecutionId);
		DataTransfer<List<GetOutput>> GetAll();
		Execution UpdateExecution(Execution entity);
		bool DeleteExecution(System.Int32 ExecutionId);
		List<Execution> GetAllExecution();
		Execution InsertExecution(Execution entity);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);
	}
	
	
}
