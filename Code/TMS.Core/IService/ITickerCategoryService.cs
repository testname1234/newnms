﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using TMS.Core.Entities;
using TMS.Core.DataTransfer;
using TMS.Core.DataTransfer.TickerCategory;

namespace TMS.Core.IService
{
		
	public interface ITickerCategoryService
	{
        Dictionary<string, string> GetTickerCategoryBasicSearchColumns();
        
        List<SearchColumn> GetTickerCategoryAdvanceSearchColumns();

		TickerCategory GetTickerCategory(System.Int32 TickerCategoryId);
		DataTransfer<List<GetOutput>> GetAll();
		TickerCategory UpdateTickerCategory(TickerCategory entity);
		bool DeleteTickerCategory(System.Int32 TickerCategoryId);
		List<TickerCategory> GetAllTickerCategory();
		TickerCategory InsertTickerCategory(TickerCategory entity);
        List<TickerCategory> GetTickerCategories(DateTime lastUpdateDate);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);
	}
	
	
}
