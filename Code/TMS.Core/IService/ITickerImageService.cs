﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using TMS.Core.Entities;
using TMS.Core.DataTransfer;
using TMS.Core.DataTransfer.TickerImage;
using TMS.Core.Enums;

namespace TMS.Core.IService
{
		
	public interface ITickerImageService
	{
        Dictionary<string, string> GetTickerImageBasicSearchColumns();
        
        List<SearchColumn> GetTickerImageAdvanceSearchColumns();

		TickerImage GetTickerImage(System.Int32 TickerImageId);
		DataTransfer<List<GetOutput>> GetAll();
		TickerImage UpdateTickerImage(TickerImage entity);
		bool DeleteTickerImage(System.Int32 TickerImageId);
		List<TickerImage> GetAllTickerImage();
		TickerImage InsertTickerImage(TickerImage entity);
        List<TickerImage> GetTickerImages(int? channelId, TickerImageStatuses tickerImageStatus, int pageIndex, int pageCount);
        bool UpdateTickerImageStatus(int tickerImageId, int statusId);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);
	}
	
	
}
