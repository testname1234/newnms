﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using TMS.Core.Entities;
using TMS.Core.DataTransfer;
using TMS.Core.DataTransfer.TickerImageStatus;

namespace TMS.Core.IService
{
		
	public interface ITickerImageStatusService
	{
        Dictionary<string, string> GetTickerImageStatusBasicSearchColumns();
        
        List<SearchColumn> GetTickerImageStatusAdvanceSearchColumns();

		TickerImageStatus GetTickerImageStatus(System.Int32 TickerImageStatusId);
		DataTransfer<List<GetOutput>> GetAll();
		TickerImageStatus UpdateTickerImageStatus(TickerImageStatus entity);
		bool DeleteTickerImageStatus(System.Int32 TickerImageStatusId);
		List<TickerImageStatus> GetAllTickerImageStatus();
		TickerImageStatus InsertTickerImageStatus(TickerImageStatus entity);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);
	}
	
	
}
