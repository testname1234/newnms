﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using TMS.Core.Entities;
using TMS.Core.DataTransfer;
using TMS.Core.DataTransfer.TickerLine;

namespace TMS.Core.IService
{
		
	public interface ITickerLineService
	{
        Dictionary<string, string> GetTickerLineBasicSearchColumns();
        
        List<SearchColumn> GetTickerLineAdvanceSearchColumns();

		List<TickerLine> GetTickerLineByTickerId(System.Int32? TickerId);
		TickerLine GetTickerLine(System.Int32 TickerLineId);
		DataTransfer<List<GetOutput>> GetAll();
		TickerLine UpdateTickerLine(TickerLine entity);
		bool DeleteTickerLine(System.Int32 TickerLineId);
		List<TickerLine> GetAllTickerLine();
		TickerLine InsertTickerLine(TickerLine entity);
        List<TickerLine> GetTickerLines(int tickerId, string keyword);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);
	}
	
	
}
