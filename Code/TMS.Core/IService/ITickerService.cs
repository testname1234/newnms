﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using TMS.Core.Entities;
using TMS.Core.DataTransfer;
using TMS.Core.DataTransfer.Ticker;

namespace TMS.Core.IService
{
		
	public interface ITickerService
	{
        Dictionary<string, string> GetTickerBasicSearchColumns();
        
        List<SearchColumn> GetTickerAdvanceSearchColumns();

		Ticker GetTicker(System.Int32 TickerId);
		DataTransfer<List<GetOutput>> GetAll();
		Ticker UpdateTicker(Ticker entity);
		bool DeleteTicker(System.Int32 TickerId);
		List<Ticker> GetAllTicker();
		Ticker InsertTicker(Ticker entity);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);

        List<Ticker> GetTickers(string keyword);
	}
	
	
}
