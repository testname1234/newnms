﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using TMS.Core.Entities;
using TMS.Core.DataTransfer;
using TMS.Core.DataTransfer.TickerType;

namespace TMS.Core.IService
{
		
	public interface ITickerTypeService
	{
        Dictionary<string, string> GetTickerTypeBasicSearchColumns();
        
        List<SearchColumn> GetTickerTypeAdvanceSearchColumns();

		TickerType GetTickerType(System.Int32 TickerTypeId);
		DataTransfer<List<GetOutput>> GetAll();
		TickerType UpdateTickerType(TickerType entity);
		bool DeleteTickerType(System.Int32 TickerTypeId);
		List<TickerType> GetAllTickerType();
		TickerType InsertTickerType(TickerType entity);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);
	}
	
	
}
