﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using TMS.Core.Entities;
using TMS.Core.DataTransfer;
using TMS.Core.DataTransfer.Video;
using TMS.Core.Enums;

namespace TMS.Core.IService
{
		
	public interface IVideoService
	{
        Dictionary<string, string> GetVideoBasicSearchColumns();
        
        List<SearchColumn> GetVideoAdvanceSearchColumns();

		Video GetVideo(System.Int32 VideoId);
		DataTransfer<List<GetOutput>> GetAll();
		Video UpdateVideo(Video entity);
		bool DeleteVideo(System.Int32 VideoId);
		List<Video> GetAllVideo();
		Video InsertVideo(Video entity);
        List<Video> GetVideos(int channelId, VideoStatuses videoStatus, DateTime? lastUpdateDate);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);

        bool Exist(int channelId, DateTime dateTime1, DateTime dateTime2);
        bool UpdateVideoStatus(int videoId, VideoStatuses videoStatus);
    }
	
	
}
