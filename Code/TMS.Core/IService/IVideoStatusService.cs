﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using TMS.Core.Entities;
using TMS.Core.DataTransfer;
using TMS.Core.DataTransfer.VideoStatus;

namespace TMS.Core.IService
{
		
	public interface IVideoStatusService
	{
        Dictionary<string, string> GetVideoStatusBasicSearchColumns();
        
        List<SearchColumn> GetVideoStatusAdvanceSearchColumns();

		VideoStatus GetVideoStatus(System.Int32 VideoStatusId);
		DataTransfer<List<GetOutput>> GetAll();
		VideoStatus UpdateVideoStatus(VideoStatus entity);
		bool DeleteVideoStatus(System.Int32 VideoStatusId);
		List<VideoStatus> GetAllVideoStatus();
		VideoStatus InsertVideoStatus(VideoStatus entity);

        DataTransfer<GetOutput> Get(string id);
        DataTransfer<PostOutput> Insert(PostInput Input);
        DataTransfer<PutOutput> Update(PutInput Input);
        DataTransfer<string> Delete(string id);
	}
	
	
}
