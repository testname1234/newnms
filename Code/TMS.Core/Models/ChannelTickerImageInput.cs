﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace TMS.Core.Models
{
    public class ChannelTickerImageInput
    {
        [DataMember(EmitDefaultValue = false)]
        public int? UserId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int? ChannelId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int PageCount { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int PageIndex { get; set; }
    }
} 
