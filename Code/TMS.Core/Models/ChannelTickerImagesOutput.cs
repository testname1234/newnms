﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TMS.Core.Models
{
    public class ChannelTickerImagesOutput
    {
        public List<Core.DataTransfer.TickerImage.GetOutput> TickerImages { get; set; }
    } 
}
