﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TMS.Core.Models
{
    public class ChannelVideosOutput
    {
        public List<Core.DataTransfer.Video.GetOutput> Videos { get; set; }
    }
}
