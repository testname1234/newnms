﻿using System;
using System.Text;

namespace TMS.Core.Models
{
    public class Point
    {
        public int X { get; set; }
        public int Y { get; set; }
    }
}
