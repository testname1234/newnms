﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TMS.Core.Models
{
    public class TickerInsertInput
    {
        public List<Core.DataTransfer.Ticker.PostInput> Tickers { get; set; }
        public string Keyword { get; set; }
        public int? TickerId { get; set; }
    }
}
