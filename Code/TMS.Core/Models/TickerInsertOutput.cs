﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TMS.Core.Models
{
    public class TickerInsertOutput
    {
        public List<Core.DataTransfer.Ticker.GetOutput> Tickers { get; set; }

        public List<Core.DataTransfer.TickerLine.GetOutput> TickerLines { get; set; }
    }
}
