﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace TMS.Core.Models
{
    public class TickerLoadInitialDataInput
    {
        [DataMember(EmitDefaultValue = false)]
        public int UserId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int? ChannelId { get; set; }

        [IgnoreDataMember]
        public System.DateTime TickerCategoryLastUpdateDate { get; set; }

        [DataMember(EmitDefaultValue = false)]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string TickerCategoryLastUpdateDateStr
        {
            get { return TickerCategoryLastUpdateDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
            set { DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { TickerCategoryLastUpdateDate = date.ToUniversalTime(); } }
        }

        [IgnoreDataMember]
        public System.DateTime VideoLastUpdateDate { get; set; }

        [DataMember(EmitDefaultValue = false)]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string VideoLastUpdateDateStr
        {
            get { return VideoLastUpdateDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); }
            set { DateTime date = new DateTime(); if (DateTime.TryParse(value, out date)) { VideoLastUpdateDate = date.ToUniversalTime(); } }
        }
    }
}
