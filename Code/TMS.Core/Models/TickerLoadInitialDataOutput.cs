﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using TMS.Core.Entities;

namespace TMS.Core.Models
{
    public class TickerLoadInitialDataOutput
    {
        [DataMember(EmitDefaultValue = false)]
        public List<Core.DataTransfer.Channel.GetOutput> Channels { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public List<Core.DataTransfer.TickerCategory.GetOutput> TickerCategories { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public List<Core.DataTransfer.TickerImage.GetOutput> TickerImages { get; set; }
    }
}
