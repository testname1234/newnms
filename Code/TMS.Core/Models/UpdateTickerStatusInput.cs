﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TMS.Core.Models
{
    public class UpdateTickerStatusInput
    {
        public int TickerImageId { get; set; }
        public int TickerImageStatusId { get; set; }
    }
}
