﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TMS.Core.Enums;

namespace TMS.Core.Models
{
    public class UpdateVideoStatusInput
    {
        public int VideoId { get; set; }
        public int VideoStatusId { get; set; }
    }
}
