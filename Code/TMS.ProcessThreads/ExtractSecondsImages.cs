﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ControlPanel.Core;
using ControlPanel.Core.Enums;
using ControlPanel.Core.IService;
using TMS.Core.DataInterfaces;
using TMS.Core.Entities;
using System.Drawing;
using TMS.Core.Enums;
using System.Globalization;
using OpenSurf;
using OpenSURFcs;
using FFMPEGLib.Helper;

namespace TMS.ProcessThreads
{
    public class ExtractSecondsImages : ISystemProcessThread
    {
        private string _threadName;

        public string ThreadName
        {
            get
            {
                return _threadName;
            }
            set
            {
                _threadName = value;
            }
        }

        private int? _startRange;

        public int? StartRange
        {
            get
            {
                return _startRange;
            }
            set
            {
                _startRange = value;
            }
        }

        private int? _endRange;

        public int? EndRange
        {
            get
            {
                return _endRange;
            }
            set
            {
                _endRange = value;
            }
        }

        private DateTime? _lastSuccessfullyExecuted;

        public DateTime? LastSuccessfullyExecuted
        {
            get
            {
                return _lastSuccessfullyExecuted;
            }
            set
            {
                _lastSuccessfullyExecuted = value;
            }
        }

        private TimeSpan? _scheduleTime;

        public TimeSpan? ScheduledTime
        {
            get
            {
                return _scheduleTime;
            }
            set
            {
                _scheduleTime = value;
            }
        }

        public string Initialize()
        {
            return "Service Initialize " + DateTime.UtcNow.ToString("MMM dd,yyyy hh:mm:ss tt");
        }

        public string Execute(string argument)
        {
            ISystemEventLogService logService = IoC.Resolve<ISystemEventLogService>("SystemEventLogService");
            try
            {
                IChannelRepository channelRepository = IoC.Resolve<IChannelRepository>("TMSChannelRepository");
                ITickerImageRepository tickerImageRepository = IoC.Resolve<ITickerImageRepository>("TMSTickerImageRepository");
                List<Channel> channels = channelRepository.GetChannelsForTickerMonitoring();
                

                if (channels != null)
                {
                    Parallel.ForEach(channels, channel =>
                    //foreach (Channel channel in channels)
                    {
                        string extractFilesPath = channel.ImagesSharePath + @"\temp\";
                        if (Directory.Exists(extractFilesPath))
                        {
                            try
                            {
                                Directory.Delete(extractFilesPath, true);
                            }
                            catch
                            {
                            }
                        }

                        try
                        {
                            if (!string.IsNullOrEmpty(channel.ReelPath))
                            {
                                int lastSecond = 0;
                                string imagesSharePath = channel.ReelPath;
                                string[] files = Directory.GetFiles(imagesSharePath);
                                files = files.Where(x => !x.Contains(".db") && !x.Contains(".ini")).ToArray();

                                string lastFile = files.Last();

                                var duration = FFMPEGLib.FFMPEG.GetDurationInMilliSeconds(lastFile) / 1000;

                                DateTime reelStartTime = DateTime.ParseExact(lastFile.Substring(lastFile.LastIndexOf('\\') + 1).Replace(".ts", "").Split('$')[1], "yyyy-MM-dd H-mm-ss", CultureInfo.InvariantCulture);

                                Directory.CreateDirectory(extractFilesPath);

                                FFMPEGLib.FFMPEG.CreateSnapshotsAfterThisSecond(lastFile, 0.3, extractFilesPath, Convert.ToInt32(duration - 60));

                                var secondFiles = Directory.GetFiles(extractFilesPath, "*.jpg");

                                int successCount = ExtractTickers(channel, reelStartTime, secondFiles);

                                Directory.Delete(extractFilesPath, true);

                                logService.InsertSystemEventLog(string.Format("ExtractSecondsImages: {1} Ticker images generated for {0}", channel.Name, successCount), null, EventCodes.Log);
                            }
                        }
                        catch (Exception ex)
                        {
                            logService.InsertSystemEventLog(string.Format("Error in ExtractSecondsImages: {0}", ex.Message), ex.StackTrace, EventCodes.Error);
                        }
                    });
                }

                return "Success";
            }
            catch (Exception exp)
            {
                logService.InsertSystemEventLog(string.Format("Error in ExtractSecondsImages: {0}", exp.Message), exp.StackTrace, EventCodes.Error);
                return "Error";
            }
        }

        public int ExtractTickers(Channel channel,DateTime reelStartTime, string[] secondFiles)
        {
            ITickerImageRepository tickerImageRepository = IoC.Resolve<ITickerImageRepository>("TMSTickerImageRepository");
            IChannelTickerLocationRepository channelTickerLocationRepository = IoC.Resolve<IChannelTickerLocationRepository>("TMSChannelTickerLocationRepository");
              
            List<ChannelTickerLocation> tickerLocations = channelTickerLocationRepository.GetChannelTickerLocationByChannelId(channel.ChannelId);
            int count = 0;
                                
            if (tickerLocations != null)
            {
                foreach (ChannelTickerLocation tickerLocation in tickerLocations)
                {
                    List<TMS.Core.Models.Point> myPoints = JSONhelper.GetObject<List<TMS.Core.Models.Point>>(tickerLocation.Points);
                    List<Point> tickerCroppingPoints = new List<Point>();
                    foreach (TMS.Core.Models.Point myPoint in myPoints)
                    {
                        Point tickerCroppingPoint = new Point();
                        tickerCroppingPoint.X = myPoint.X;
                        tickerCroppingPoint.Y = myPoint.Y;
                        tickerCroppingPoints.Add(tickerCroppingPoint);
                    }
                    int index = 1;
                    List<List<IPoint>> previousPoints = new List<List<IPoint>>();
                    foreach (string file in secondFiles)
                    {
                        if (File.Exists(file))
                        {
                            using (Bitmap bmp = new Bitmap(file))
                            {
                                Graphics Ga = Graphics.FromImage(bmp);
                                SolidBrush Brush = new SolidBrush(Color.FromArgb(1, 1, 1));

                                var graphicPath = new System.Drawing.Drawing2D.GraphicsPath();
                                graphicPath.AddPolygon(tickerCroppingPoints.ToArray());

                                Region region = new Region();
                                region.Exclude(graphicPath);
                                Ga.FillRegion(Brush, region);

                                int minX = tickerCroppingPoints.Select(x => x.X).Min();
                                int maxX = tickerCroppingPoints.Select(x => x.X).Max();
                                int minY = tickerCroppingPoints.Select(x => x.Y).Min();
                                int maxY = tickerCroppingPoints.Select(x => x.Y).Max();
                                Rectangle rectcrop = new Rectangle(minX, minY, maxX - minX, maxY - minY);

                                string outputPath = channel.ImagesSharePath + reelStartTime.AddSeconds(index).ToString().Replace("/", "").Replace(" ", "").Replace(":", "") + ".jpg";
                                //string outputPath = AppDomain.CurrentDomain.BaseDirectory + "\\tempTickers\\" + reelStartTime.AddSeconds(index).ToString().Replace("/", "").Replace(" ", "").Replace(":", "") + ".jpg";//
                                LogoMatching matching = new LogoMatching();
                                var points = matching.GetImagePoints(bmp, 0.009f);
                                if (points.Count > 16)
                                {
                                    if (previousPoints.Count == 0)
                                    {
                                        bmp.Clone(rectcrop, System.Drawing.Imaging.PixelFormat.Undefined).Save(outputPath);
                                        previousPoints.Add(points);
                                        InsertDistinctTicker(channel, tickerImageRepository, tickerLocation, outputPath);
                                        count++;
                                    }
                                    else
                                    {
                                        double ratio = 0;
                                        for (int i = 0; i < previousPoints.Count; i++)
                                        {
                                            var pts = previousPoints[i];
                                            var matches = matching.GetMatches(points, pts);

                                            var currentRatio = (double)matches[0].Count / (double)points.Count;
                                            if (currentRatio > ratio)
                                                ratio = currentRatio;

                                        }

                                        if (ratio < 0.6)
                                        {
                                            previousPoints.Add(points);
                                            bmp.Clone(rectcrop, System.Drawing.Imaging.PixelFormat.Undefined).Save(outputPath);
                                            InsertDistinctTicker(channel, tickerImageRepository, tickerLocation, outputPath);
                                            count++;
                                        }
                                    }



                                }


                                index++;

                                bmp.Dispose();
                            }
                        }
                    }
                }
            }

            return count;
        }

        private static void InsertDistinctTicker(Channel channel, ITickerImageRepository tickerImageRepository, ChannelTickerLocation tickerLocation, string outputPath)
        {
            TickerImage entity = new TickerImage();
            entity.ChannelId = channel.ChannelId;
            entity.ChannelTickerLocationId = tickerLocation.ChannelTickerLocationId;
            entity.FileDate = DateTime.UtcNow;
            entity.FileDateStr = DateTime.UtcNow.ToString();
            entity.ImagePath = outputPath;
            entity.TickerImageStatusId = (int)TickerImageStatuses.Pending;
            entity.CreationDate = DateTime.UtcNow;
            entity.LastUpdateDate = DateTime.UtcNow;
            tickerImageRepository.InsertTickerImage(entity);
        }

        public static string GetDirectoryListingRegexForUrl(string url)
        {
            if (url.Equals("http://www.ibiblio.org/pub/"))
            {
                return "<a href=\".*\">(?<name>.*)</a>";
            }
            throw new NotSupportedException();
        }
    }
}
