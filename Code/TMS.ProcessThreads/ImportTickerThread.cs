﻿using ControlPanel.Core;
using ControlPanel.Core.Enums;
using ControlPanel.Core.IService;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using TMS.Core.DataInterfaces;
using TMS.Core.Entities;
using TMS.Core.Enums;

namespace TMS.ProcessThreads
{
    public class ImportTickerThread : ISystemProcessThread
    {
        private string _threadName;

        public string ThreadName
        {
            get
            {
                return _threadName;
            }
            set
            {
                _threadName = value;
            }
        }

        private int? _startRange;

        public int? StartRange
        {
            get
            {
                return _startRange;
            }
            set
            {
                _startRange = value;
            }
        }

        private int? _endRange;

        public int? EndRange
        {
            get
            {
                return _endRange;
            }
            set
            {
                _endRange = value;
            }
        }

        private DateTime? _lastSuccessfullyExecuted;

        public DateTime? LastSuccessfullyExecuted
        {
            get
            {
                return _lastSuccessfullyExecuted;
            }
            set
            {
                _lastSuccessfullyExecuted = value;
            }
        }

        private TimeSpan? _scheduleTime;

        public TimeSpan? ScheduledTime
        {
            get
            {
                return _scheduleTime;
            }
            set
            {
                _scheduleTime = value;
            }
        }

        public string Initialize()
        {
            return "Service Initialize " + DateTime.UtcNow.ToString("MMM dd,yyyy hh:mm:ss tt");
        }

        public string Execute(string argument)
        {
            ISystemEventLogService logService = IoC.Resolve<ISystemEventLogService>("SystemEventLogService");
            try
            {
                IChannelRepository channelRepository = IoC.Resolve<IChannelRepository>("TMSChannelRepository");
                IChannelTickerLocationRepository channelTickerLocationRepository = IoC.Resolve<IChannelTickerLocationRepository>("TMSChannelTickerLocationRepository");
                ITickerImageRepository tickerImageRepository = IoC.Resolve<ITickerImageRepository>("TMSTickerImageRepository");

                List<Channel> channels = channelRepository.GetChannelsForTickerMonitoring();

                if (channels != null)
                {
                    foreach (Channel channel in channels)
                    {
                        try
                        {
                            if (!string.IsNullOrEmpty(channel.ImagesSharePath))
                            {
                                var imageFiles = !Directory.Exists(channel.ImagesSharePath) ? new List<string>() : Directory.GetFiles(channel.ImagesSharePath, "*.jpg").OrderBy(x => Convert.ToInt32(x.Split(new string[] { "\\" }, StringSplitOptions.RemoveEmptyEntries).Last().Replace("snap", "").Replace(".jpg", ""))).ToList();
                                if (imageFiles.Count > 0)
                                {
                                    List<ChannelTickerLocation> tickerLocations = channelTickerLocationRepository.GetChannelTickerLocationByChannelId(channel.ChannelId);

                                    if (tickerLocations != null)
                                    {
                                        foreach (ChannelTickerLocation tickerLocation in tickerLocations)
                                        {
                                            List<Point> tickerCroppingPoints = JSONhelper.GetObject<List<Point>>(tickerLocation.Points);
                                            int index = 1;
                                            foreach (string file in imageFiles)
                                            {
                                                using (Bitmap bmp = new Bitmap(file))
                                                {
                                                    Graphics Ga = Graphics.FromImage(bmp);
                                                    SolidBrush Brush = new SolidBrush(Color.FromArgb(1, 1, 1));

                                                    var graphicPath = new System.Drawing.Drawing2D.GraphicsPath();
                                                    graphicPath.AddPolygon(tickerCroppingPoints.ToArray());

                                                    Region region = new Region();
                                                    region.Exclude(graphicPath);
                                                    Ga.FillRegion(Brush, region);

                                                    int minX = tickerCroppingPoints.Select(x => x.X).Min();
                                                    int maxX = tickerCroppingPoints.Select(x => x.X).Max();
                                                    int minY = tickerCroppingPoints.Select(x => x.Y).Min();
                                                    int maxY = tickerCroppingPoints.Select(x => x.Y).Max();
                                                    Rectangle rectcrop = new Rectangle(minX, minY, maxX - minX, maxY - minY);

                                                    string outputPath = "E:\\Data\\frames\\ARY News\\output\\" + index + ".jpg";
                                                    bmp.Clone(rectcrop, bmp.PixelFormat).Save(outputPath);

                                                    TickerImage entity = new TickerImage();
                                                    entity.ChannelId = channel.ChannelId;
                                                    entity.ChannelTickerLocationId = tickerLocation.ChannelTickerLocationId;
                                                    entity.FileDate = DateTime.UtcNow;
                                                    entity.FileDateStr = DateTime.UtcNow.ToString();
                                                    entity.ImagePath = outputPath;
                                                    entity.TickerImageStatusId = (int)TickerImageStatuses.Pending;
                                                    entity.CreationDate = DateTime.UtcNow;
                                                    entity.LastUpdateDate = DateTime.UtcNow;

                                                    tickerImageRepository.InsertTickerImage(entity);

                                                    index++;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                        }
                    }
                }

                return "Service Started";
            }
            catch (Exception exp)
            {
                logService.InsertSystemEventLog(string.Format("Error in IndexNewsThread: {0}", exp.Message), exp.StackTrace, EventCodes.Error);
                return "Error";
            }
        }
    }
}