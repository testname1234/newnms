﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using TMS.Core;
using TMS.Core.Entities;
using TMS.Core.DataInterfaces;
using TMS.Core.Extensions;

namespace TMS.Repository
{
		
	public abstract partial class ChannelRepositoryBase : Repository, IChannelRepositoryBase 
	{
        
        public ChannelRepositoryBase()
        {   
            this.SearchColumns=new Dictionary<string, SearchColumn>();

			this.SearchColumns.Add("ChannelId",new SearchColumn(){Name="ChannelId",Title="ChannelId",SelectClause="ChannelId",WhereClause="AllRecords.ChannelId",DataType="System.Int32",IsForeignColumn=false,PropertyName="ChannelId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Name",new SearchColumn(){Name="Name",Title="Name",SelectClause="Name",WhereClause="AllRecords.Name",DataType="System.String",IsForeignColumn=false,PropertyName="Name",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("CreationDate",new SearchColumn(){Name="CreationDate",Title="CreationDate",SelectClause="CreationDate",WhereClause="AllRecords.CreationDate",DataType="System.DateTime?",IsForeignColumn=false,PropertyName="CreationDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("LastUpdateDate",new SearchColumn(){Name="LastUpdateDate",Title="LastUpdateDate",SelectClause="LastUpdateDate",WhereClause="AllRecords.LastUpdateDate",DataType="System.DateTime?",IsForeignColumn=false,PropertyName="LastUpdateDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("IsActive",new SearchColumn(){Name="IsActive",Title="IsActive",SelectClause="IsActive",WhereClause="AllRecords.IsActive",DataType="System.Boolean?",IsForeignColumn=false,PropertyName="IsActive",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("IsTickerMonitoringOn",new SearchColumn(){Name="IsTickerMonitoringOn",Title="IsTickerMonitoringOn",SelectClause="IsTickerMonitoringOn",WhereClause="AllRecords.IsTickerMonitoringOn",DataType="System.Boolean?",IsForeignColumn=false,PropertyName="IsTickerMonitoringOn",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("ImagesSharePath",new SearchColumn(){Name="ImagesSharePath",Title="ImagesSharePath",SelectClause="ImagesSharePath",WhereClause="AllRecords.ImagesSharePath",DataType="System.String",IsForeignColumn=false,PropertyName="ImagesSharePath",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("ReelPath",new SearchColumn(){Name="ReelPath",Title="ReelPath",SelectClause="ReelPath",WhereClause="AllRecords.ReelPath",DataType="System.String",IsForeignColumn=false,PropertyName="ReelPath",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});        
        }
        
		public virtual List<SearchColumn> GetChannelSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                searchColumns.Add(keyValuePair.Value);
            }
            return searchColumns;
        }
		
		
		
        public virtual Dictionary<string, string> GetChannelBasicSearchColumns()
        {
			Dictionary<string, string> columnList = new Dictionary<string, string>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsBasicSearchColumm)
                {
					keyValuePair.Value.Value = string.Empty;
                    columnList.Add(keyValuePair.Key, keyValuePair.Value.Title);
                }
            }
            return columnList;
        }

        public virtual List<SearchColumn> GetChannelAdvanceSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsAdvanceSearchColumn)
                {
					keyValuePair.Value.Value = string.Empty;
					searchColumns.Add(keyValuePair.Value);
                }
            }
            return searchColumns;
        }
        
        
        public virtual string GetChannelSelectClause()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            string selectQuery=string.Empty;
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (!keyValuePair.Value.IgnoreForDefaultSelect)
                {
					if (keyValuePair.Value.IsForeignColumn)
                	{
						if(string.IsNullOrEmpty(selectQuery))
						{
							selectQuery = "("+keyValuePair.Value.SelectClause+") as \""+keyValuePair.Key+"\"";
						}
						else
						{
							selectQuery += ",(" + keyValuePair.Value.SelectClause + ") as \"" + keyValuePair.Key + "\"";
						}
                	}
                	else
                	{
                    	if (string.IsNullOrEmpty(selectQuery))
                    	{
                        	selectQuery =  "[Channel].["+keyValuePair.Key+"]";
                    	}
                    	else
                    	{
                        	selectQuery += ",[Channel].["+keyValuePair.Key+"]";
                    	}
                	}
            	}
            }
            return "Select "+selectQuery+" ";
        }
        

		public virtual Channel GetChannel(System.Int32 ChannelId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetChannelSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [Channel] with (nolock)  where ChannelId=@ChannelId ";
			SqlParameter parameter=new SqlParameter("@ChannelId",ChannelId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
			return ChannelFromDataRow(ds.Tables[0].Rows[0]);
		}

		public  List<Channel> GetChannelByKeyValue(string Key,string Value,Operands operand,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetChannelSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+= string.Format("from [Channel] with (nolock)  where {0} {1} '{2}' ",Key,operand.ToOperandString(),Value);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<Channel>(ds,ChannelFromDataRow);
		}

		public virtual List<Channel> GetAllChannel(string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetChannelSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [Channel] with (nolock)  ";
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,null);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<Channel>(ds, ChannelFromDataRow);
		}

		public virtual List<Channel> GetPagedChannel(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null)
		{

			string whereClause = base.GetAdvancedWhereClauseByColumn(searchColumns, GetSearchColumns());
               if (!String.IsNullOrEmpty(orderByClause))
               {
                   KeyValuePair<string, string> parsedOrderByClause = base.ParseOrderByClause(orderByClause);
                   orderByClause = base.GetBasicSearchOrderByClauseByColumn(parsedOrderByClause.Key, parsedOrderByClause.Value, this.SearchColumns);
               }

            count=GetChannelCount(whereClause, searchColumns);
			if(count>0)
			{
			if (count < startIndex) startIndex = (count / pageSize) * pageSize;			
			
           	int PageLowerBound = startIndex;
            int PageUpperBound = PageLowerBound + pageSize;
            string sql = @"CREATE TABLE #PageIndex
				            (
				                [IndexId] int IDENTITY (1, 1) NOT NULL,
				                [ChannelId] int				   
				            );";

            //Insert into the temp table
            string tempsql = "INSERT INTO #PageIndex ([ChannelId])";
            tempsql += " SELECT ";
            if (pageSize > 0) tempsql += "TOP " + PageUpperBound.ToString();
            tempsql += " [ChannelId] ";
            tempsql += " FROM [Channel] AllRecords with (NOLOCK)";
            if (!string.IsNullOrEmpty(whereClause) && whereClause.Length > 0) tempsql += " WHERE " + whereClause;
            if (orderByClause.Length > 0) 
			{
				tempsql += " ORDER BY " + orderByClause;
				if( !orderByClause.Contains("ChannelId"))
					tempsql += " , (AllRecords.[ChannelId])"; 
			}
			else 
			{
				tempsql  += " ORDER BY (AllRecords.[ChannelId])"; 
			}           
            
            // Return paged results
            string pagedResultsSql =
                (string.IsNullOrEmpty(SelectClause)? GetChannelSelectClause():(string.Format("Select {0} ",SelectClause)))+@" FROM [Channel] , #PageIndex PageIndex WHERE ";
            pagedResultsSql += " PageIndex.IndexId > " + PageLowerBound.ToString(); 
            pagedResultsSql += @" AND [Channel].[ChannelId] = PageIndex.[ChannelId] 
				                  ORDER BY PageIndex.IndexId;";
            pagedResultsSql += " drop table #PageIndex";
            sql = sql + tempsql + pagedResultsSql;
			sql = string.Format(sql, whereClause, pageSize, startIndex, orderByClause);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql, GetSQLParamtersBySearchColumns(searchColumns));
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<Channel>(ds, ChannelFromDataRow);
			}else{ return null;}
		}

		private int GetChannelCount(string whereClause, List<SearchColumn> searchColumns)
		{

			string sql=string.Empty;
			if(string.IsNullOrEmpty(whereClause))
				sql = "SELECT Count(*) FROM Channel as AllRecords  ";
			else
				sql = "SELECT Count(*) FROM Channel as AllRecords  where  " +whereClause;
			var rowCount = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, GetSQLParamtersBySearchColumns(searchColumns));
			return rowCount == DBNull.Value ? 0 :(int)rowCount;
		}

		[MOLog(AuditOperations.Create,typeof(Channel))]
		public virtual Channel InsertChannel(Channel entity)
		{

			Channel other=new Channel();
			other = entity;
			if(entity.IsTransient())
			{
				string sql=@"Insert into Channel ( [Name]
				,[CreationDate]
				,[LastUpdateDate]
				,[IsActive]
				,[IsTickerMonitoringOn]
				,[ImagesSharePath]
				,[ReelPath] )
				Values
				( @Name
				, @CreationDate
				, @LastUpdateDate
				, @IsActive
				, @IsTickerMonitoringOn
				, @ImagesSharePath
				, @ReelPath );
				Select scope_identity()";
				SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@Name",entity.Name ?? (object)DBNull.Value)
					, new SqlParameter("@CreationDate",entity.CreationDate ?? (object)DBNull.Value)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate ?? (object)DBNull.Value)
					, new SqlParameter("@IsActive",entity.IsActive ?? (object)DBNull.Value)
					, new SqlParameter("@IsTickerMonitoringOn",entity.IsTickerMonitoringOn ?? (object)DBNull.Value)
					, new SqlParameter("@ImagesSharePath",entity.ImagesSharePath ?? (object)DBNull.Value)
					, new SqlParameter("@ReelPath",entity.ReelPath ?? (object)DBNull.Value)};
				var identity=SqlHelper.ExecuteScalar(this.ConnectionString,CommandType.Text,sql,parameterArray);
				if(identity==DBNull.Value) throw new DataException("Identity column was null as a result of the insert operation.");
				return GetChannel(Convert.ToInt32(identity));
			}
			return entity;
		}

		[MOLog(AuditOperations.Update,typeof(Channel))]
		public virtual Channel UpdateChannel(Channel entity)
		{

			if (entity.IsTransient()) return entity;
			Channel other = GetChannel(entity.ChannelId);
			if (entity.Equals(other)) return entity;
			string sql=@"Update Channel set  [Name]=@Name
							, [CreationDate]=@CreationDate
							, [LastUpdateDate]=@LastUpdateDate
							, [IsActive]=@IsActive
							, [IsTickerMonitoringOn]=@IsTickerMonitoringOn
							, [ImagesSharePath]=@ImagesSharePath
							, [ReelPath]=@ReelPath 
							 where ChannelId=@ChannelId";
			SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@Name",entity.Name ?? (object)DBNull.Value)
					, new SqlParameter("@CreationDate",entity.CreationDate ?? (object)DBNull.Value)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate ?? (object)DBNull.Value)
					, new SqlParameter("@IsActive",entity.IsActive ?? (object)DBNull.Value)
					, new SqlParameter("@IsTickerMonitoringOn",entity.IsTickerMonitoringOn ?? (object)DBNull.Value)
					, new SqlParameter("@ImagesSharePath",entity.ImagesSharePath ?? (object)DBNull.Value)
					, new SqlParameter("@ReelPath",entity.ReelPath ?? (object)DBNull.Value)
					, new SqlParameter("@ChannelId",entity.ChannelId)};
			SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,parameterArray);
			return GetChannel(entity.ChannelId);
		}

		public virtual bool DeleteChannel(System.Int32 ChannelId)
		{

			string sql="delete from Channel where ChannelId=@ChannelId";
			SqlParameter parameter=new SqlParameter("@ChannelId",ChannelId);
			var identity=SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			return (Convert.ToInt32(identity))==1? true: false;
		}

		[MOLog(AuditOperations.Delete,typeof(Channel))]
		public virtual Channel DeleteChannel(Channel entity)
		{
			this.DeleteChannel(entity.ChannelId);
			return entity;
		}


		public virtual Channel ChannelFromDataRow(DataRow dr)
		{
			if(dr==null) return null;
			Channel entity=new Channel();
			if (dr.Table.Columns.Contains("ChannelId"))
			{
			entity.ChannelId = (System.Int32)dr["ChannelId"];
			}
			if (dr.Table.Columns.Contains("Name"))
			{
			entity.Name = dr["Name"].ToString();
			}
			if (dr.Table.Columns.Contains("CreationDate"))
			{
			entity.CreationDate = dr["CreationDate"]==DBNull.Value?(System.DateTime?)null:(System.DateTime?)dr["CreationDate"];
			}
			if (dr.Table.Columns.Contains("LastUpdateDate"))
			{
			entity.LastUpdateDate = dr["LastUpdateDate"]==DBNull.Value?(System.DateTime?)null:(System.DateTime?)dr["LastUpdateDate"];
			}
			if (dr.Table.Columns.Contains("IsActive"))
			{
			entity.IsActive = dr["IsActive"]==DBNull.Value?(System.Boolean?)null:(System.Boolean?)dr["IsActive"];
			}
			if (dr.Table.Columns.Contains("IsTickerMonitoringOn"))
			{
			entity.IsTickerMonitoringOn = dr["IsTickerMonitoringOn"]==DBNull.Value?(System.Boolean?)null:(System.Boolean?)dr["IsTickerMonitoringOn"];
			}
			if (dr.Table.Columns.Contains("ImagesSharePath"))
			{
			entity.ImagesSharePath = dr["ImagesSharePath"].ToString();
			}
			if (dr.Table.Columns.Contains("ReelPath"))
			{
			entity.ReelPath = dr["ReelPath"].ToString();
			}
			return entity;
		}

	}
	
	
}
