﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using TMS.Core.Entities;
using TMS.Core.DataInterfaces;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace TMS.Repository
{
		
	public partial class ChannelRepository: ChannelRepositoryBase, IChannelRepository
	{
        public override string ConnectionString
        {
            get
            {
                if (ConfigurationManager.ConnectionStrings["connTMS"] != null)
                    return ConfigurationManager.ConnectionStrings["connTMS"].ToString();
                else return base.ConnectionString;
            }
        }

        public List<Channel> GetChannels(DateTime lastUpdateDate)
        {
            string sql = GetChannelSelectClause();
            sql += " from [Channel] with (nolock)  where LastUpdateDate > @Date";

            SqlParameter[] parameters = new SqlParameter[] {
                new SqlParameter("@Date", lastUpdateDate)
            };

            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, parameters);
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<Channel>(ds, ChannelFromDataRow);
        }

        public List<Channel> GetChannelByUserId(int userId)
        {
            string sql = "select t1.* from [Channel] t1 inner join [ChannelUserMapping] t2 on t1.[ChannelId] = t2.[ChannelId] where t2.[UserId] = @UserId";

            SqlParameter[] parameters = new SqlParameter[] {
                new SqlParameter("@UserId", userId)
            };

            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, parameters);
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<Channel>(ds, ChannelFromDataRow);
        }

        public List<Channel> GetChannelsForTickerMonitoring()
        {
            string sql = "select * from [Channel] where [IsTickerMonitoringOn] = 1 and [IsActive] = 1";
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, null);
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<Channel>(ds, ChannelFromDataRow);
        }

        public DateTime GetMaxChannelDate(int channelId)
        {
            string sql = "select max(endtime) from [video] with (nolock)  where channelid = @channelId";

            SqlParameter[] parameters = new SqlParameter[] {
                new SqlParameter("@channelId", channelId)
            };

            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, parameters);
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0 || ds.Tables[0].Rows[0][0] == DBNull.Value) return DateTime.UtcNow.AddHours(-2);
            return Convert.ToDateTime(ds.Tables[0].Rows[0][0]);
        }
    }
	
	
}
