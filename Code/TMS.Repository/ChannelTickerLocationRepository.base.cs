﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using TMS.Core;
using TMS.Core.Entities;
using TMS.Core.DataInterfaces;
using TMS.Core.Extensions;

namespace TMS.Repository
{
		
	public abstract partial class ChannelTickerLocationRepositoryBase : Repository, IChannelTickerLocationRepositoryBase 
	{
        
        public ChannelTickerLocationRepositoryBase()
        {   
            this.SearchColumns=new Dictionary<string, SearchColumn>();

			this.SearchColumns.Add("ChannelTickerLocationId",new SearchColumn(){Name="ChannelTickerLocationId",Title="ChannelTickerLocationId",SelectClause="ChannelTickerLocationId",WhereClause="AllRecords.ChannelTickerLocationId",DataType="System.Int32",IsForeignColumn=false,PropertyName="ChannelTickerLocationId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("ChannelId",new SearchColumn(){Name="ChannelId",Title="ChannelId",SelectClause="ChannelId",WhereClause="AllRecords.ChannelId",DataType="System.Int32",IsForeignColumn=false,PropertyName="ChannelId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Points",new SearchColumn(){Name="Points",Title="Points",SelectClause="Points",WhereClause="AllRecords.Points",DataType="System.String",IsForeignColumn=false,PropertyName="Points",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("IsActive",new SearchColumn(){Name="IsActive",Title="IsActive",SelectClause="IsActive",WhereClause="AllRecords.IsActive",DataType="System.Boolean",IsForeignColumn=false,PropertyName="IsActive",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("CreationDate",new SearchColumn(){Name="CreationDate",Title="CreationDate",SelectClause="CreationDate",WhereClause="AllRecords.CreationDate",DataType="System.DateTime",IsForeignColumn=false,PropertyName="CreationDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("LastUpdateDate",new SearchColumn(){Name="LastUpdateDate",Title="LastUpdateDate",SelectClause="LastUpdateDate",WhereClause="AllRecords.LastUpdateDate",DataType="System.DateTime",IsForeignColumn=false,PropertyName="LastUpdateDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});        
        }
        
		public virtual List<SearchColumn> GetChannelTickerLocationSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                searchColumns.Add(keyValuePair.Value);
            }
            return searchColumns;
        }
		
		
		
        public virtual Dictionary<string, string> GetChannelTickerLocationBasicSearchColumns()
        {
			Dictionary<string, string> columnList = new Dictionary<string, string>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsBasicSearchColumm)
                {
					keyValuePair.Value.Value = string.Empty;
                    columnList.Add(keyValuePair.Key, keyValuePair.Value.Title);
                }
            }
            return columnList;
        }

        public virtual List<SearchColumn> GetChannelTickerLocationAdvanceSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsAdvanceSearchColumn)
                {
					keyValuePair.Value.Value = string.Empty;
					searchColumns.Add(keyValuePair.Value);
                }
            }
            return searchColumns;
        }
        
        
        public virtual string GetChannelTickerLocationSelectClause()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            string selectQuery=string.Empty;
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (!keyValuePair.Value.IgnoreForDefaultSelect)
                {
					if (keyValuePair.Value.IsForeignColumn)
                	{
						if(string.IsNullOrEmpty(selectQuery))
						{
							selectQuery = "("+keyValuePair.Value.SelectClause+") as \""+keyValuePair.Key+"\"";
						}
						else
						{
							selectQuery += ",(" + keyValuePair.Value.SelectClause + ") as \"" + keyValuePair.Key + "\"";
						}
                	}
                	else
                	{
                    	if (string.IsNullOrEmpty(selectQuery))
                    	{
                        	selectQuery =  "[ChannelTickerLocation].["+keyValuePair.Key+"]";
                    	}
                    	else
                    	{
                        	selectQuery += ",[ChannelTickerLocation].["+keyValuePair.Key+"]";
                    	}
                	}
            	}
            }
            return "Select "+selectQuery+" ";
        }
        

		public virtual ChannelTickerLocation GetChannelTickerLocation(System.Int32 ChannelTickerLocationId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetChannelTickerLocationSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [ChannelTickerLocation] with (nolock)  where ChannelTickerLocationId=@ChannelTickerLocationId ";
			SqlParameter parameter=new SqlParameter("@ChannelTickerLocationId",ChannelTickerLocationId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
			return ChannelTickerLocationFromDataRow(ds.Tables[0].Rows[0]);
		}

		public  List<ChannelTickerLocation> GetChannelTickerLocationByKeyValue(string Key,string Value,Operands operand,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetChannelTickerLocationSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+= string.Format("from [ChannelTickerLocation] with (nolock)  where {0} {1} '{2}' ",Key,operand.ToOperandString(),Value);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<ChannelTickerLocation>(ds,ChannelTickerLocationFromDataRow);
		}

		public virtual List<ChannelTickerLocation> GetAllChannelTickerLocation(string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetChannelTickerLocationSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [ChannelTickerLocation] with (nolock)  ";
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,null);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<ChannelTickerLocation>(ds, ChannelTickerLocationFromDataRow);
		}

		public virtual List<ChannelTickerLocation> GetPagedChannelTickerLocation(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null)
		{

			string whereClause = base.GetAdvancedWhereClauseByColumn(searchColumns, GetSearchColumns());
               if (!String.IsNullOrEmpty(orderByClause))
               {
                   KeyValuePair<string, string> parsedOrderByClause = base.ParseOrderByClause(orderByClause);
                   orderByClause = base.GetBasicSearchOrderByClauseByColumn(parsedOrderByClause.Key, parsedOrderByClause.Value, this.SearchColumns);
               }

            count=GetChannelTickerLocationCount(whereClause, searchColumns);
			if(count>0)
			{
			if (count < startIndex) startIndex = (count / pageSize) * pageSize;			
			
           	int PageLowerBound = startIndex;
            int PageUpperBound = PageLowerBound + pageSize;
            string sql = @"CREATE TABLE #PageIndex
				            (
				                [IndexId] int IDENTITY (1, 1) NOT NULL,
				                [ChannelTickerLocationId] int				   
				            );";

            //Insert into the temp table
            string tempsql = "INSERT INTO #PageIndex ([ChannelTickerLocationId])";
            tempsql += " SELECT ";
            if (pageSize > 0) tempsql += "TOP " + PageUpperBound.ToString();
            tempsql += " [ChannelTickerLocationId] ";
            tempsql += " FROM [ChannelTickerLocation] AllRecords with (NOLOCK)";
            if (!string.IsNullOrEmpty(whereClause) && whereClause.Length > 0) tempsql += " WHERE " + whereClause;
            if (orderByClause.Length > 0) 
			{
				tempsql += " ORDER BY " + orderByClause;
				if( !orderByClause.Contains("ChannelTickerLocationId"))
					tempsql += " , (AllRecords.[ChannelTickerLocationId])"; 
			}
			else 
			{
				tempsql  += " ORDER BY (AllRecords.[ChannelTickerLocationId])"; 
			}           
            
            // Return paged results
            string pagedResultsSql =
                (string.IsNullOrEmpty(SelectClause)? GetChannelTickerLocationSelectClause():(string.Format("Select {0} ",SelectClause)))+@" FROM [ChannelTickerLocation] , #PageIndex PageIndex WHERE ";
            pagedResultsSql += " PageIndex.IndexId > " + PageLowerBound.ToString(); 
            pagedResultsSql += @" AND [ChannelTickerLocation].[ChannelTickerLocationId] = PageIndex.[ChannelTickerLocationId] 
				                  ORDER BY PageIndex.IndexId;";
            pagedResultsSql += " drop table #PageIndex";
            sql = sql + tempsql + pagedResultsSql;
			sql = string.Format(sql, whereClause, pageSize, startIndex, orderByClause);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql, GetSQLParamtersBySearchColumns(searchColumns));
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<ChannelTickerLocation>(ds, ChannelTickerLocationFromDataRow);
			}else{ return null;}
		}

		private int GetChannelTickerLocationCount(string whereClause, List<SearchColumn> searchColumns)
		{

			string sql=string.Empty;
			if(string.IsNullOrEmpty(whereClause))
				sql = "SELECT Count(*) FROM ChannelTickerLocation as AllRecords  ";
			else
				sql = "SELECT Count(*) FROM ChannelTickerLocation as AllRecords  where  " +whereClause;
			var rowCount = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, GetSQLParamtersBySearchColumns(searchColumns));
			return rowCount == DBNull.Value ? 0 :(int)rowCount;
		}

		[MOLog(AuditOperations.Create,typeof(ChannelTickerLocation))]
		public virtual ChannelTickerLocation InsertChannelTickerLocation(ChannelTickerLocation entity)
		{

			ChannelTickerLocation other=new ChannelTickerLocation();
			other = entity;
			if(entity.IsTransient())
			{
				string sql=@"Insert into ChannelTickerLocation ( [ChannelId]
				,[Points]
				,[IsActive]
				,[CreationDate]
				,[LastUpdateDate] )
				Values
				( @ChannelId
				, @Points
				, @IsActive
				, @CreationDate
				, @LastUpdateDate );
				Select scope_identity()";
				SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@ChannelId",entity.ChannelId)
					, new SqlParameter("@Points",entity.Points)
					, new SqlParameter("@IsActive",entity.IsActive)
					, new SqlParameter("@CreationDate",entity.CreationDate)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate)};
				var identity=SqlHelper.ExecuteScalar(this.ConnectionString,CommandType.Text,sql,parameterArray);
				if(identity==DBNull.Value) throw new DataException("Identity column was null as a result of the insert operation.");
				return GetChannelTickerLocation(Convert.ToInt32(identity));
			}
			return entity;
		}

		[MOLog(AuditOperations.Update,typeof(ChannelTickerLocation))]
		public virtual ChannelTickerLocation UpdateChannelTickerLocation(ChannelTickerLocation entity)
		{

			if (entity.IsTransient()) return entity;
			ChannelTickerLocation other = GetChannelTickerLocation(entity.ChannelTickerLocationId);
			if (entity.Equals(other)) return entity;
			string sql=@"Update ChannelTickerLocation set  [ChannelId]=@ChannelId
							, [Points]=@Points
							, [IsActive]=@IsActive
							, [CreationDate]=@CreationDate
							, [LastUpdateDate]=@LastUpdateDate 
							 where ChannelTickerLocationId=@ChannelTickerLocationId";
			SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@ChannelId",entity.ChannelId)
					, new SqlParameter("@Points",entity.Points)
					, new SqlParameter("@IsActive",entity.IsActive)
					, new SqlParameter("@CreationDate",entity.CreationDate)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate)
					, new SqlParameter("@ChannelTickerLocationId",entity.ChannelTickerLocationId)};
			SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,parameterArray);
			return GetChannelTickerLocation(entity.ChannelTickerLocationId);
		}

		public virtual bool DeleteChannelTickerLocation(System.Int32 ChannelTickerLocationId)
		{

			string sql="delete from ChannelTickerLocation where ChannelTickerLocationId=@ChannelTickerLocationId";
			SqlParameter parameter=new SqlParameter("@ChannelTickerLocationId",ChannelTickerLocationId);
			var identity=SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			return (Convert.ToInt32(identity))==1? true: false;
		}

		[MOLog(AuditOperations.Delete,typeof(ChannelTickerLocation))]
		public virtual ChannelTickerLocation DeleteChannelTickerLocation(ChannelTickerLocation entity)
		{
			this.DeleteChannelTickerLocation(entity.ChannelTickerLocationId);
			return entity;
		}


		public virtual ChannelTickerLocation ChannelTickerLocationFromDataRow(DataRow dr)
		{
			if(dr==null) return null;
			ChannelTickerLocation entity=new ChannelTickerLocation();
			if (dr.Table.Columns.Contains("ChannelTickerLocationId"))
			{
			entity.ChannelTickerLocationId = (System.Int32)dr["ChannelTickerLocationId"];
			}
			if (dr.Table.Columns.Contains("ChannelId"))
			{
			entity.ChannelId = (System.Int32)dr["ChannelId"];
			}
			if (dr.Table.Columns.Contains("Points"))
			{
			entity.Points = dr["Points"].ToString();
			}
			if (dr.Table.Columns.Contains("IsActive"))
			{
			entity.IsActive = (System.Boolean)dr["IsActive"];
			}
			if (dr.Table.Columns.Contains("CreationDate"))
			{
			entity.CreationDate = (System.DateTime)dr["CreationDate"];
			}
			if (dr.Table.Columns.Contains("LastUpdateDate"))
			{
			entity.LastUpdateDate = (System.DateTime)dr["LastUpdateDate"];
			}
			return entity;
		}

	}
	
	
}
