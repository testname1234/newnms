﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using TMS.Core;
using TMS.Core.Entities;
using TMS.Core.DataInterfaces;
using TMS.Core.Extensions;

namespace TMS.Repository
{
		
	public abstract partial class ChannelUserMappingRepositoryBase : Repository, IChannelUserMappingRepositoryBase 
	{
        
        public ChannelUserMappingRepositoryBase()
        {   
            this.SearchColumns=new Dictionary<string, SearchColumn>();

			this.SearchColumns.Add("ChannelUserMappingId",new SearchColumn(){Name="ChannelUserMappingId",Title="ChannelUserMappingId",SelectClause="ChannelUserMappingId",WhereClause="AllRecords.ChannelUserMappingId",DataType="System.Int32",IsForeignColumn=false,PropertyName="ChannelUserMappingId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("ChannelId",new SearchColumn(){Name="ChannelId",Title="ChannelId",SelectClause="ChannelId",WhereClause="AllRecords.ChannelId",DataType="System.Int32?",IsForeignColumn=false,PropertyName="ChannelId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("UserId",new SearchColumn(){Name="UserId",Title="UserId",SelectClause="UserId",WhereClause="AllRecords.UserId",DataType="System.Int32?",IsForeignColumn=false,PropertyName="UserId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});        
        }
        
		public virtual List<SearchColumn> GetChannelUserMappingSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                searchColumns.Add(keyValuePair.Value);
            }
            return searchColumns;
        }
		
		
		
        public virtual Dictionary<string, string> GetChannelUserMappingBasicSearchColumns()
        {
			Dictionary<string, string> columnList = new Dictionary<string, string>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsBasicSearchColumm)
                {
					keyValuePair.Value.Value = string.Empty;
                    columnList.Add(keyValuePair.Key, keyValuePair.Value.Title);
                }
            }
            return columnList;
        }

        public virtual List<SearchColumn> GetChannelUserMappingAdvanceSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsAdvanceSearchColumn)
                {
					keyValuePair.Value.Value = string.Empty;
					searchColumns.Add(keyValuePair.Value);
                }
            }
            return searchColumns;
        }
        
        
        public virtual string GetChannelUserMappingSelectClause()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            string selectQuery=string.Empty;
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (!keyValuePair.Value.IgnoreForDefaultSelect)
                {
					if (keyValuePair.Value.IsForeignColumn)
                	{
						if(string.IsNullOrEmpty(selectQuery))
						{
							selectQuery = "("+keyValuePair.Value.SelectClause+") as \""+keyValuePair.Key+"\"";
						}
						else
						{
							selectQuery += ",(" + keyValuePair.Value.SelectClause + ") as \"" + keyValuePair.Key + "\"";
						}
                	}
                	else
                	{
                    	if (string.IsNullOrEmpty(selectQuery))
                    	{
                        	selectQuery =  "[ChannelUserMapping].["+keyValuePair.Key+"]";
                    	}
                    	else
                    	{
                        	selectQuery += ",[ChannelUserMapping].["+keyValuePair.Key+"]";
                    	}
                	}
            	}
            }
            return "Select "+selectQuery+" ";
        }
        

		public virtual List<ChannelUserMapping> GetChannelUserMappingByChannelId(System.Int32? ChannelId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetChannelUserMappingSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [ChannelUserMapping] with (nolock)  where ChannelId=@ChannelId  ";
			SqlParameter parameter=new SqlParameter("@ChannelId",ChannelId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<ChannelUserMapping>(ds,ChannelUserMappingFromDataRow);
		}

		public virtual ChannelUserMapping GetChannelUserMapping(System.Int32 ChannelUserMappingId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetChannelUserMappingSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [ChannelUserMapping] with (nolock)  where ChannelUserMappingId=@ChannelUserMappingId ";
			SqlParameter parameter=new SqlParameter("@ChannelUserMappingId",ChannelUserMappingId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
			return ChannelUserMappingFromDataRow(ds.Tables[0].Rows[0]);
		}

		public  List<ChannelUserMapping> GetChannelUserMappingByKeyValue(string Key,string Value,Operands operand,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetChannelUserMappingSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+= string.Format("from [ChannelUserMapping] with (nolock)  where {0} {1} '{2}' ",Key,operand.ToOperandString(),Value);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<ChannelUserMapping>(ds,ChannelUserMappingFromDataRow);
		}

		public virtual List<ChannelUserMapping> GetAllChannelUserMapping(string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetChannelUserMappingSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [ChannelUserMapping] with (nolock)  ";
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,null);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<ChannelUserMapping>(ds, ChannelUserMappingFromDataRow);
		}

		public virtual List<ChannelUserMapping> GetPagedChannelUserMapping(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null)
		{

			string whereClause = base.GetAdvancedWhereClauseByColumn(searchColumns, GetSearchColumns());
               if (!String.IsNullOrEmpty(orderByClause))
               {
                   KeyValuePair<string, string> parsedOrderByClause = base.ParseOrderByClause(orderByClause);
                   orderByClause = base.GetBasicSearchOrderByClauseByColumn(parsedOrderByClause.Key, parsedOrderByClause.Value, this.SearchColumns);
               }

            count=GetChannelUserMappingCount(whereClause, searchColumns);
			if(count>0)
			{
			if (count < startIndex) startIndex = (count / pageSize) * pageSize;			
			
           	int PageLowerBound = startIndex;
            int PageUpperBound = PageLowerBound + pageSize;
            string sql = @"CREATE TABLE #PageIndex
				            (
				                [IndexId] int IDENTITY (1, 1) NOT NULL,
				                [ChannelUserMappingId] int				   
				            );";

            //Insert into the temp table
            string tempsql = "INSERT INTO #PageIndex ([ChannelUserMappingId])";
            tempsql += " SELECT ";
            if (pageSize > 0) tempsql += "TOP " + PageUpperBound.ToString();
            tempsql += " [ChannelUserMappingId] ";
            tempsql += " FROM [ChannelUserMapping] AllRecords with (NOLOCK)";
            if (!string.IsNullOrEmpty(whereClause) && whereClause.Length > 0) tempsql += " WHERE " + whereClause;
            if (orderByClause.Length > 0) 
			{
				tempsql += " ORDER BY " + orderByClause;
				if( !orderByClause.Contains("ChannelUserMappingId"))
					tempsql += " , (AllRecords.[ChannelUserMappingId])"; 
			}
			else 
			{
				tempsql  += " ORDER BY (AllRecords.[ChannelUserMappingId])"; 
			}           
            
            // Return paged results
            string pagedResultsSql =
                (string.IsNullOrEmpty(SelectClause)? GetChannelUserMappingSelectClause():(string.Format("Select {0} ",SelectClause)))+@" FROM [ChannelUserMapping] , #PageIndex PageIndex WHERE ";
            pagedResultsSql += " PageIndex.IndexId > " + PageLowerBound.ToString(); 
            pagedResultsSql += @" AND [ChannelUserMapping].[ChannelUserMappingId] = PageIndex.[ChannelUserMappingId] 
				                  ORDER BY PageIndex.IndexId;";
            pagedResultsSql += " drop table #PageIndex";
            sql = sql + tempsql + pagedResultsSql;
			sql = string.Format(sql, whereClause, pageSize, startIndex, orderByClause);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql, GetSQLParamtersBySearchColumns(searchColumns));
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<ChannelUserMapping>(ds, ChannelUserMappingFromDataRow);
			}else{ return null;}
		}

		private int GetChannelUserMappingCount(string whereClause, List<SearchColumn> searchColumns)
		{

			string sql=string.Empty;
			if(string.IsNullOrEmpty(whereClause))
				sql = "SELECT Count(*) FROM ChannelUserMapping as AllRecords  ";
			else
				sql = "SELECT Count(*) FROM ChannelUserMapping as AllRecords  where  " +whereClause;
			var rowCount = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, GetSQLParamtersBySearchColumns(searchColumns));
			return rowCount == DBNull.Value ? 0 :(int)rowCount;
		}

		[MOLog(AuditOperations.Create,typeof(ChannelUserMapping))]
		public virtual ChannelUserMapping InsertChannelUserMapping(ChannelUserMapping entity)
		{

			ChannelUserMapping other=new ChannelUserMapping();
			other = entity;
			if(entity.IsTransient())
			{
				string sql=@"Insert into ChannelUserMapping ( [ChannelId]
				,[UserId] )
				Values
				( @ChannelId
				, @UserId );
				Select scope_identity()";
				SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@ChannelId",entity.ChannelId ?? (object)DBNull.Value)
					, new SqlParameter("@UserId",entity.UserId ?? (object)DBNull.Value)};
				var identity=SqlHelper.ExecuteScalar(this.ConnectionString,CommandType.Text,sql,parameterArray);
				if(identity==DBNull.Value) throw new DataException("Identity column was null as a result of the insert operation.");
				return GetChannelUserMapping(Convert.ToInt32(identity));
			}
			return entity;
		}

		[MOLog(AuditOperations.Update,typeof(ChannelUserMapping))]
		public virtual ChannelUserMapping UpdateChannelUserMapping(ChannelUserMapping entity)
		{

			if (entity.IsTransient()) return entity;
			ChannelUserMapping other = GetChannelUserMapping(entity.ChannelUserMappingId);
			if (entity.Equals(other)) return entity;
			string sql=@"Update ChannelUserMapping set  [ChannelId]=@ChannelId
							, [UserId]=@UserId 
							 where ChannelUserMappingId=@ChannelUserMappingId";
			SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@ChannelId",entity.ChannelId ?? (object)DBNull.Value)
					, new SqlParameter("@UserId",entity.UserId ?? (object)DBNull.Value)
					, new SqlParameter("@ChannelUserMappingId",entity.ChannelUserMappingId)};
			SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,parameterArray);
			return GetChannelUserMapping(entity.ChannelUserMappingId);
		}

		public virtual bool DeleteChannelUserMapping(System.Int32 ChannelUserMappingId)
		{

			string sql="delete from ChannelUserMapping where ChannelUserMappingId=@ChannelUserMappingId";
			SqlParameter parameter=new SqlParameter("@ChannelUserMappingId",ChannelUserMappingId);
			var identity=SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			return (Convert.ToInt32(identity))==1? true: false;
		}

		[MOLog(AuditOperations.Delete,typeof(ChannelUserMapping))]
		public virtual ChannelUserMapping DeleteChannelUserMapping(ChannelUserMapping entity)
		{
			this.DeleteChannelUserMapping(entity.ChannelUserMappingId);
			return entity;
		}


		public virtual ChannelUserMapping ChannelUserMappingFromDataRow(DataRow dr)
		{
			if(dr==null) return null;
			ChannelUserMapping entity=new ChannelUserMapping();
			if (dr.Table.Columns.Contains("ChannelUserMappingId"))
			{
			entity.ChannelUserMappingId = (System.Int32)dr["ChannelUserMappingId"];
			}
			if (dr.Table.Columns.Contains("ChannelId"))
			{
			entity.ChannelId = dr["ChannelId"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["ChannelId"];
			}
			if (dr.Table.Columns.Contains("UserId"))
			{
			entity.UserId = dr["UserId"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["UserId"];
			}
			return entity;
		}

	}
	
	
}
