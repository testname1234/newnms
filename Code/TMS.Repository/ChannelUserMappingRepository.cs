﻿using System.Configuration;
using TMS.Core.DataInterfaces;

namespace TMS.Repository
{
    public partial class ChannelUserMappingRepository : ChannelUserMappingRepositoryBase, IChannelUserMappingRepository
    {
        public override string ConnectionString
        {
            get
            {
                if (ConfigurationManager.ConnectionStrings["connTMS"] != null)
                    return ConfigurationManager.ConnectionStrings["connTMS"].ToString();
                else return base.ConnectionString;
            }
        }
    }
}