﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TMS.Core.DataInterfaces;

namespace TMS.Repository
{
    public class ExceptionRepository : IExceptionRepository
    {
        public void InsertException(Exception exp)
        {
            string sql = @"Insert into ELMAH_Error ( [Application],[Host],[Type],[Source],[Message],[User],[StatusCode],[TimeUtc],[AllXml])
				Values
				( @Application, @Host, @Type, @Source, @Message, @User, 0, @TimeUtc, @AllXml);
				Select scope_identity()";
            SqlParameter[] parameterArray = new SqlParameter[]{
                     new SqlParameter("@Application","/LM/W3SVC/12/ROOT")
                    , new SqlParameter("@Host","Thread logging")
                , new SqlParameter("@Type",exp.GetType().ToString())
                , new SqlParameter("@Source",exp.Source)
                , new SqlParameter("@Message",exp.Message)
                , new SqlParameter("@User","Thread logging")
                , new SqlParameter("@TimeUtc",DateTime.UtcNow)
                , new SqlParameter("@AllXml",exp.StackTrace)
                };
            SqlHelper.ExecuteScalar(ConfigurationManager.ConnectionStrings["connTMS"].ConnectionString, CommandType.Text, sql, parameterArray);
        }


        public bool JSErrorLogging(string msg, string url, string linenumber, string user, string type,string host)
        {
            string sql = @"Insert into ELMAH_Error ([Application], [Host] ,[type],[source],[Message],[USER],StatusCode,TimeUTC,[AllXml])
				Values
				( @Application,@Host ,@Type,@Source,@Message,@User,@StatusCode,@TimeUTC,@AllXml);
				Select scope_identity()";
            SqlParameter[] parameterArray = new SqlParameter[]{
                
                 new SqlParameter("@Application","/LM/W3SVC/12/ROOT")
                , new SqlParameter("@Host",host)
                , new SqlParameter("@StatusCode",9898)
                , new SqlParameter("@Type",msg)
                , new SqlParameter("@Source","WebClientSide:Javascript")
                , new SqlParameter("@Message",string.Format("{0}{1}{2}",type, " --- Line Number " + linenumber, "--- Source File :" + url))
                , new SqlParameter("@User",user)
                , new SqlParameter("@TimeUtc",DateTime.UtcNow)
                , new SqlParameter("@AllXml",' '),
                
                };
            return SqlHelper.ExecuteNonQuery(ConfigurationManager.ConnectionStrings["connTMS"].ConnectionString, CommandType.Text, sql, parameterArray) > 0;
        }
    }
}
