﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using TMS.Core;
using TMS.Core.Entities;
using TMS.Core.DataInterfaces;
using TMS.Core.Extensions;

namespace TMS.Repository
{
		
	public abstract partial class ExecutionRepositoryBase : Repository, IExecutionRepositoryBase 
	{
        
        public ExecutionRepositoryBase()
        {   
            this.SearchColumns=new Dictionary<string, SearchColumn>();

			this.SearchColumns.Add("ExecutionId",new SearchColumn(){Name="ExecutionId",Title="ExecutionId",SelectClause="ExecutionId",WhereClause="AllRecords.ExecutionId",DataType="System.Int32",IsForeignColumn=false,PropertyName="ExecutionId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("TickerLineId",new SearchColumn(){Name="TickerLineId",Title="TickerLineId",SelectClause="TickerLineId",WhereClause="AllRecords.TickerLineId",DataType="System.Int32?",IsForeignColumn=false,PropertyName="TickerLineId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("ExecutionTime",new SearchColumn(){Name="ExecutionTime",Title="ExecutionTime",SelectClause="ExecutionTime",WhereClause="AllRecords.ExecutionTime",DataType="System.DateTime?",IsForeignColumn=false,PropertyName="ExecutionTime",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});        
        }
        
		public virtual List<SearchColumn> GetExecutionSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                searchColumns.Add(keyValuePair.Value);
            }
            return searchColumns;
        }
		
		
		
        public virtual Dictionary<string, string> GetExecutionBasicSearchColumns()
        {
			Dictionary<string, string> columnList = new Dictionary<string, string>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsBasicSearchColumm)
                {
					keyValuePair.Value.Value = string.Empty;
                    columnList.Add(keyValuePair.Key, keyValuePair.Value.Title);
                }
            }
            return columnList;
        }

        public virtual List<SearchColumn> GetExecutionAdvanceSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsAdvanceSearchColumn)
                {
					keyValuePair.Value.Value = string.Empty;
					searchColumns.Add(keyValuePair.Value);
                }
            }
            return searchColumns;
        }
        
        
        public virtual string GetExecutionSelectClause()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            string selectQuery=string.Empty;
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (!keyValuePair.Value.IgnoreForDefaultSelect)
                {
					if (keyValuePair.Value.IsForeignColumn)
                	{
						if(string.IsNullOrEmpty(selectQuery))
						{
							selectQuery = "("+keyValuePair.Value.SelectClause+") as \""+keyValuePair.Key+"\"";
						}
						else
						{
							selectQuery += ",(" + keyValuePair.Value.SelectClause + ") as \"" + keyValuePair.Key + "\"";
						}
                	}
                	else
                	{
                    	if (string.IsNullOrEmpty(selectQuery))
                    	{
                        	selectQuery =  "[Execution].["+keyValuePair.Key+"]";
                    	}
                    	else
                    	{
                        	selectQuery += ",[Execution].["+keyValuePair.Key+"]";
                    	}
                	}
            	}
            }
            return "Select "+selectQuery+" ";
        }
        

		public virtual List<Execution> GetExecutionByTickerLineId(System.Int32? TickerLineId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetExecutionSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [Execution] with (nolock)  where TickerLineId=@TickerLineId  ";
			SqlParameter parameter=new SqlParameter("@TickerLineId",TickerLineId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<Execution>(ds,ExecutionFromDataRow);
		}

		public virtual Execution GetExecution(System.Int32 ExecutionId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetExecutionSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [Execution] with (nolock)  where ExecutionId=@ExecutionId ";
			SqlParameter parameter=new SqlParameter("@ExecutionId",ExecutionId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
			return ExecutionFromDataRow(ds.Tables[0].Rows[0]);
		}

		public  List<Execution> GetExecutionByKeyValue(string Key,string Value,Operands operand,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetExecutionSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+= string.Format("from [Execution] with (nolock)  where {0} {1} '{2}' ",Key,operand.ToOperandString(),Value);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<Execution>(ds,ExecutionFromDataRow);
		}

		public virtual List<Execution> GetAllExecution(string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetExecutionSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [Execution] with (nolock)  ";
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,null);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<Execution>(ds, ExecutionFromDataRow);
		}

		public virtual List<Execution> GetPagedExecution(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null)
		{

			string whereClause = base.GetAdvancedWhereClauseByColumn(searchColumns, GetSearchColumns());
               if (!String.IsNullOrEmpty(orderByClause))
               {
                   KeyValuePair<string, string> parsedOrderByClause = base.ParseOrderByClause(orderByClause);
                   orderByClause = base.GetBasicSearchOrderByClauseByColumn(parsedOrderByClause.Key, parsedOrderByClause.Value, this.SearchColumns);
               }

            count=GetExecutionCount(whereClause, searchColumns);
			if(count>0)
			{
			if (count < startIndex) startIndex = (count / pageSize) * pageSize;			
			
           	int PageLowerBound = startIndex;
            int PageUpperBound = PageLowerBound + pageSize;
            string sql = @"CREATE TABLE #PageIndex
				            (
				                [IndexId] int IDENTITY (1, 1) NOT NULL,
				                [ExecutionId] int				   
				            );";

            //Insert into the temp table
            string tempsql = "INSERT INTO #PageIndex ([ExecutionId])";
            tempsql += " SELECT ";
            if (pageSize > 0) tempsql += "TOP " + PageUpperBound.ToString();
            tempsql += " [ExecutionId] ";
            tempsql += " FROM [Execution] AllRecords with (NOLOCK)";
            if (!string.IsNullOrEmpty(whereClause) && whereClause.Length > 0) tempsql += " WHERE " + whereClause;
            if (orderByClause.Length > 0) 
			{
				tempsql += " ORDER BY " + orderByClause;
				if( !orderByClause.Contains("ExecutionId"))
					tempsql += " , (AllRecords.[ExecutionId])"; 
			}
			else 
			{
				tempsql  += " ORDER BY (AllRecords.[ExecutionId])"; 
			}           
            
            // Return paged results
            string pagedResultsSql =
                (string.IsNullOrEmpty(SelectClause)? GetExecutionSelectClause():(string.Format("Select {0} ",SelectClause)))+@" FROM [Execution] , #PageIndex PageIndex WHERE ";
            pagedResultsSql += " PageIndex.IndexId > " + PageLowerBound.ToString(); 
            pagedResultsSql += @" AND [Execution].[ExecutionId] = PageIndex.[ExecutionId] 
				                  ORDER BY PageIndex.IndexId;";
            pagedResultsSql += " drop table #PageIndex";
            sql = sql + tempsql + pagedResultsSql;
			sql = string.Format(sql, whereClause, pageSize, startIndex, orderByClause);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql, GetSQLParamtersBySearchColumns(searchColumns));
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<Execution>(ds, ExecutionFromDataRow);
			}else{ return null;}
		}

		private int GetExecutionCount(string whereClause, List<SearchColumn> searchColumns)
		{

			string sql=string.Empty;
			if(string.IsNullOrEmpty(whereClause))
				sql = "SELECT Count(*) FROM Execution as AllRecords  ";
			else
				sql = "SELECT Count(*) FROM Execution as AllRecords  where  " +whereClause;
			var rowCount = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, GetSQLParamtersBySearchColumns(searchColumns));
			return rowCount == DBNull.Value ? 0 :(int)rowCount;
		}

		[MOLog(AuditOperations.Create,typeof(Execution))]
		public virtual Execution InsertExecution(Execution entity)
		{

			Execution other=new Execution();
			other = entity;
			if(entity.IsTransient())
			{
				string sql=@"Insert into Execution ( [TickerLineId]
				,[ExecutionTime] )
				Values
				( @TickerLineId
				, @ExecutionTime );
				Select scope_identity()";
				SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@TickerLineId",entity.TickerLineId ?? (object)DBNull.Value)
					, new SqlParameter("@ExecutionTime",entity.ExecutionTime ?? (object)DBNull.Value)};
				var identity=SqlHelper.ExecuteScalar(this.ConnectionString,CommandType.Text,sql,parameterArray);
				if(identity==DBNull.Value) throw new DataException("Identity column was null as a result of the insert operation.");
				return GetExecution(Convert.ToInt32(identity));
			}
			return entity;
		}

		[MOLog(AuditOperations.Update,typeof(Execution))]
		public virtual Execution UpdateExecution(Execution entity)
		{

			if (entity.IsTransient()) return entity;
			Execution other = GetExecution(entity.ExecutionId);
			if (entity.Equals(other)) return entity;
			string sql=@"Update Execution set  [TickerLineId]=@TickerLineId
							, [ExecutionTime]=@ExecutionTime 
							 where ExecutionId=@ExecutionId";
			SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@TickerLineId",entity.TickerLineId ?? (object)DBNull.Value)
					, new SqlParameter("@ExecutionTime",entity.ExecutionTime ?? (object)DBNull.Value)
					, new SqlParameter("@ExecutionId",entity.ExecutionId)};
			SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,parameterArray);
			return GetExecution(entity.ExecutionId);
		}

		public virtual bool DeleteExecution(System.Int32 ExecutionId)
		{

			string sql="delete from Execution where ExecutionId=@ExecutionId";
			SqlParameter parameter=new SqlParameter("@ExecutionId",ExecutionId);
			var identity=SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			return (Convert.ToInt32(identity))==1? true: false;
		}

		[MOLog(AuditOperations.Delete,typeof(Execution))]
		public virtual Execution DeleteExecution(Execution entity)
		{
			this.DeleteExecution(entity.ExecutionId);
			return entity;
		}


		public virtual Execution ExecutionFromDataRow(DataRow dr)
		{
			if(dr==null) return null;
			Execution entity=new Execution();
			if (dr.Table.Columns.Contains("ExecutionId"))
			{
			entity.ExecutionId = (System.Int32)dr["ExecutionId"];
			}
			if (dr.Table.Columns.Contains("TickerLineId"))
			{
			entity.TickerLineId = dr["TickerLineId"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["TickerLineId"];
			}
			if (dr.Table.Columns.Contains("ExecutionTime"))
			{
			entity.ExecutionTime = dr["ExecutionTime"]==DBNull.Value?(System.DateTime?)null:(System.DateTime?)dr["ExecutionTime"];
			}
			return entity;
		}

	}
	
	
}
