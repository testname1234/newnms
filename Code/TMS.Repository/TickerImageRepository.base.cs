﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using TMS.Core;
using TMS.Core.Entities;
using TMS.Core.DataInterfaces;
using TMS.Core.Extensions;

namespace TMS.Repository
{
		
	public abstract partial class TickerImageRepositoryBase : Repository, ITickerImageRepositoryBase 
	{
        
        public TickerImageRepositoryBase()
        {   
            this.SearchColumns=new Dictionary<string, SearchColumn>();

			this.SearchColumns.Add("TickerImageId",new SearchColumn(){Name="TickerImageId",Title="TickerImageId",SelectClause="TickerImageId",WhereClause="AllRecords.TickerImageId",DataType="System.Int32",IsForeignColumn=false,PropertyName="TickerImageId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("ChannelId",new SearchColumn(){Name="ChannelId",Title="ChannelId",SelectClause="ChannelId",WhereClause="AllRecords.ChannelId",DataType="System.Int32",IsForeignColumn=false,PropertyName="ChannelId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("FileDate",new SearchColumn(){Name="FileDate",Title="FileDate",SelectClause="FileDate",WhereClause="AllRecords.FileDate",DataType="System.DateTime",IsForeignColumn=false,PropertyName="FileDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("GroupId",new SearchColumn(){Name="GroupId",Title="GroupId",SelectClause="GroupId",WhereClause="AllRecords.GroupId",DataType="System.Guid",IsForeignColumn=false,PropertyName="GroupId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("ImagePath",new SearchColumn(){Name="ImagePath",Title="ImagePath",SelectClause="ImagePath",WhereClause="AllRecords.ImagePath",DataType="System.String",IsForeignColumn=false,PropertyName="ImagePath",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("ChannelTickerLocationId",new SearchColumn(){Name="ChannelTickerLocationId",Title="ChannelTickerLocationId",SelectClause="ChannelTickerLocationId",WhereClause="AllRecords.ChannelTickerLocationId",DataType="System.Int32",IsForeignColumn=false,PropertyName="ChannelTickerLocationId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("CreationDate",new SearchColumn(){Name="CreationDate",Title="CreationDate",SelectClause="CreationDate",WhereClause="AllRecords.CreationDate",DataType="System.DateTime",IsForeignColumn=false,PropertyName="CreationDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("LastUpdateDate",new SearchColumn(){Name="LastUpdateDate",Title="LastUpdateDate",SelectClause="LastUpdateDate",WhereClause="AllRecords.LastUpdateDate",DataType="System.DateTime",IsForeignColumn=false,PropertyName="LastUpdateDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("TickerImageStatusId",new SearchColumn(){Name="TickerImageStatusId",Title="TickerImageStatusId",SelectClause="TickerImageStatusId",WhereClause="AllRecords.TickerImageStatusId",DataType="System.Int32?",IsForeignColumn=false,PropertyName="TickerImageStatusId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});        
        }
        
		public virtual List<SearchColumn> GetTickerImageSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                searchColumns.Add(keyValuePair.Value);
            }
            return searchColumns;
        }
		
		
		
        public virtual Dictionary<string, string> GetTickerImageBasicSearchColumns()
        {
			Dictionary<string, string> columnList = new Dictionary<string, string>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsBasicSearchColumm)
                {
					keyValuePair.Value.Value = string.Empty;
                    columnList.Add(keyValuePair.Key, keyValuePair.Value.Title);
                }
            }
            return columnList;
        }

        public virtual List<SearchColumn> GetTickerImageAdvanceSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsAdvanceSearchColumn)
                {
					keyValuePair.Value.Value = string.Empty;
					searchColumns.Add(keyValuePair.Value);
                }
            }
            return searchColumns;
        }
        
        
        public virtual string GetTickerImageSelectClause()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            string selectQuery=string.Empty;
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (!keyValuePair.Value.IgnoreForDefaultSelect)
                {
					if (keyValuePair.Value.IsForeignColumn)
                	{
						if(string.IsNullOrEmpty(selectQuery))
						{
							selectQuery = "("+keyValuePair.Value.SelectClause+") as \""+keyValuePair.Key+"\"";
						}
						else
						{
							selectQuery += ",(" + keyValuePair.Value.SelectClause + ") as \"" + keyValuePair.Key + "\"";
						}
                	}
                	else
                	{
                    	if (string.IsNullOrEmpty(selectQuery))
                    	{
                        	selectQuery =  "[TickerImage].["+keyValuePair.Key+"]";
                    	}
                    	else
                    	{
                        	selectQuery += ",[TickerImage].["+keyValuePair.Key+"]";
                    	}
                	}
            	}
            }
            return "Select "+selectQuery+" ";
        }
        

		public virtual TickerImage GetTickerImage(System.Int32 TickerImageId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetTickerImageSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [TickerImage] with (nolock)  where TickerImageId=@TickerImageId ";
			SqlParameter parameter=new SqlParameter("@TickerImageId",TickerImageId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
			return TickerImageFromDataRow(ds.Tables[0].Rows[0]);
		}

		public  List<TickerImage> GetTickerImageByKeyValue(string Key,string Value,Operands operand,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetTickerImageSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+= string.Format("from [TickerImage] with (nolock)  where {0} {1} '{2}' ",Key,operand.ToOperandString(),Value);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<TickerImage>(ds,TickerImageFromDataRow);
		}

		public virtual List<TickerImage> GetAllTickerImage(string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetTickerImageSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [TickerImage] with (nolock)  ";
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,null);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<TickerImage>(ds, TickerImageFromDataRow);
		}

		public virtual List<TickerImage> GetPagedTickerImage(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null)
		{

			string whereClause = base.GetAdvancedWhereClauseByColumn(searchColumns, GetSearchColumns());
               if (!String.IsNullOrEmpty(orderByClause))
               {
                   KeyValuePair<string, string> parsedOrderByClause = base.ParseOrderByClause(orderByClause);
                   orderByClause = base.GetBasicSearchOrderByClauseByColumn(parsedOrderByClause.Key, parsedOrderByClause.Value, this.SearchColumns);
               }

            count=GetTickerImageCount(whereClause, searchColumns);
			if(count>0)
			{
			if (count < startIndex) startIndex = (count / pageSize) * pageSize;			
			
           	int PageLowerBound = startIndex;
            int PageUpperBound = PageLowerBound + pageSize;
            string sql = @"CREATE TABLE #PageIndex
				            (
				                [IndexId] int IDENTITY (1, 1) NOT NULL,
				                [TickerImageId] int				   
				            );";

            //Insert into the temp table
            string tempsql = "INSERT INTO #PageIndex ([TickerImageId])";
            tempsql += " SELECT ";
            if (pageSize > 0) tempsql += "TOP " + PageUpperBound.ToString();
            tempsql += " [TickerImageId] ";
            tempsql += " FROM [TickerImage] AllRecords with (NOLOCK)";
            if (!string.IsNullOrEmpty(whereClause) && whereClause.Length > 0) tempsql += " WHERE " + whereClause;
            if (orderByClause.Length > 0) 
			{
				tempsql += " ORDER BY " + orderByClause;
				if( !orderByClause.Contains("TickerImageId"))
					tempsql += " , (AllRecords.[TickerImageId])"; 
			}
			else 
			{
				tempsql  += " ORDER BY (AllRecords.[TickerImageId])"; 
			}           
            
            // Return paged results
            string pagedResultsSql =
                (string.IsNullOrEmpty(SelectClause)? GetTickerImageSelectClause():(string.Format("Select {0} ",SelectClause)))+@" FROM [TickerImage] , #PageIndex PageIndex WHERE ";
            pagedResultsSql += " PageIndex.IndexId > " + PageLowerBound.ToString(); 
            pagedResultsSql += @" AND [TickerImage].[TickerImageId] = PageIndex.[TickerImageId] 
				                  ORDER BY PageIndex.IndexId;";
            pagedResultsSql += " drop table #PageIndex";
            sql = sql + tempsql + pagedResultsSql;
			sql = string.Format(sql, whereClause, pageSize, startIndex, orderByClause);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql, GetSQLParamtersBySearchColumns(searchColumns));
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<TickerImage>(ds, TickerImageFromDataRow);
			}else{ return null;}
		}

		private int GetTickerImageCount(string whereClause, List<SearchColumn> searchColumns)
		{

			string sql=string.Empty;
			if(string.IsNullOrEmpty(whereClause))
				sql = "SELECT Count(*) FROM TickerImage as AllRecords  ";
			else
				sql = "SELECT Count(*) FROM TickerImage as AllRecords  where  " +whereClause;
			var rowCount = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, GetSQLParamtersBySearchColumns(searchColumns));
			return rowCount == DBNull.Value ? 0 :(int)rowCount;
		}

		[MOLog(AuditOperations.Create,typeof(TickerImage))]
		public virtual TickerImage InsertTickerImage(TickerImage entity)
		{

			TickerImage other=new TickerImage();
			other = entity;
			if(entity.IsTransient())
			{
				string sql=@"Insert into TickerImage ( [ChannelId]
				,[FileDate]
				,[GroupId]
				,[ImagePath]
				,[ChannelTickerLocationId]
				,[CreationDate]
				,[LastUpdateDate]
				,[TickerImageStatusId] )
				Values
				( @ChannelId
				, @FileDate
				, @GroupId
				, @ImagePath
				, @ChannelTickerLocationId
				, @CreationDate
				, @LastUpdateDate
				, @TickerImageStatusId );
				Select scope_identity()";
				SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@ChannelId",entity.ChannelId)
					, new SqlParameter("@FileDate",entity.FileDate)
					, new SqlParameter("@GroupId",entity.GroupId)
					, new SqlParameter("@ImagePath",entity.ImagePath)
					, new SqlParameter("@ChannelTickerLocationId",entity.ChannelTickerLocationId)
					, new SqlParameter("@CreationDate",entity.CreationDate)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate)
					, new SqlParameter("@TickerImageStatusId",entity.TickerImageStatusId ?? (object)DBNull.Value)};
				var identity=SqlHelper.ExecuteScalar(this.ConnectionString,CommandType.Text,sql,parameterArray);
				if(identity==DBNull.Value) throw new DataException("Identity column was null as a result of the insert operation.");
				return GetTickerImage(Convert.ToInt32(identity));
			}
			return entity;
		}

		[MOLog(AuditOperations.Update,typeof(TickerImage))]
		public virtual TickerImage UpdateTickerImage(TickerImage entity)
		{

			if (entity.IsTransient()) return entity;
			TickerImage other = GetTickerImage(entity.TickerImageId);
			if (entity.Equals(other)) return entity;
			string sql=@"Update TickerImage set  [ChannelId]=@ChannelId
							, [FileDate]=@FileDate
							, [GroupId]=@GroupId
							, [ImagePath]=@ImagePath
							, [ChannelTickerLocationId]=@ChannelTickerLocationId
							, [CreationDate]=@CreationDate
							, [LastUpdateDate]=@LastUpdateDate
							, [TickerImageStatusId]=@TickerImageStatusId 
							 where TickerImageId=@TickerImageId";
			SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@ChannelId",entity.ChannelId)
					, new SqlParameter("@FileDate",entity.FileDate)
					, new SqlParameter("@GroupId",entity.GroupId)
					, new SqlParameter("@ImagePath",entity.ImagePath)
					, new SqlParameter("@ChannelTickerLocationId",entity.ChannelTickerLocationId)
					, new SqlParameter("@CreationDate",entity.CreationDate)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate)
					, new SqlParameter("@TickerImageStatusId",entity.TickerImageStatusId ?? (object)DBNull.Value)
					, new SqlParameter("@TickerImageId",entity.TickerImageId)};
			SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,parameterArray);
			return GetTickerImage(entity.TickerImageId);
		}

		public virtual bool DeleteTickerImage(System.Int32 TickerImageId)
		{

			string sql="delete from TickerImage where TickerImageId=@TickerImageId";
			SqlParameter parameter=new SqlParameter("@TickerImageId",TickerImageId);
			var identity=SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			return (Convert.ToInt32(identity))==1? true: false;
		}

		[MOLog(AuditOperations.Delete,typeof(TickerImage))]
		public virtual TickerImage DeleteTickerImage(TickerImage entity)
		{
			this.DeleteTickerImage(entity.TickerImageId);
			return entity;
		}


		public virtual TickerImage TickerImageFromDataRow(DataRow dr)
		{
			if(dr==null) return null;
			TickerImage entity=new TickerImage();
			if (dr.Table.Columns.Contains("TickerImageId"))
			{
			entity.TickerImageId = (System.Int32)dr["TickerImageId"];
			}
			if (dr.Table.Columns.Contains("ChannelId"))
			{
			entity.ChannelId = (System.Int32)dr["ChannelId"];
			}
			if (dr.Table.Columns.Contains("FileDate"))
			{
			entity.FileDate = (System.DateTime)dr["FileDate"];
			}
			if (dr.Table.Columns.Contains("GroupId"))
			{
			entity.GroupId = (System.Guid)dr["GroupId"];
			}
			if (dr.Table.Columns.Contains("ImagePath"))
			{
			entity.ImagePath = dr["ImagePath"].ToString();
			}
			if (dr.Table.Columns.Contains("ChannelTickerLocationId"))
			{
			entity.ChannelTickerLocationId = (System.Int32)dr["ChannelTickerLocationId"];
			}
			if (dr.Table.Columns.Contains("CreationDate"))
			{
			entity.CreationDate = (System.DateTime)dr["CreationDate"];
			}
			if (dr.Table.Columns.Contains("LastUpdateDate"))
			{
			entity.LastUpdateDate = (System.DateTime)dr["LastUpdateDate"];
			}
			if (dr.Table.Columns.Contains("TickerImageStatusId"))
			{
			entity.TickerImageStatusId = dr["TickerImageStatusId"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["TickerImageStatusId"];
			}
			return entity;
		}

	}
	
	
}
