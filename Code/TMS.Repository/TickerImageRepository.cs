﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using TMS.Core.Entities;
using TMS.Core.DataInterfaces;
using System.Configuration;
using TMS.Core.Enums;
using System.Data;
using System.Data.SqlClient;

namespace TMS.Repository
{
		
	public partial class TickerImageRepository: TickerImageRepositoryBase, ITickerImageRepository
	{
        public override string ConnectionString
        {
            get
            {
                if (ConfigurationManager.ConnectionStrings["connTMS"] != null)
                    return ConfigurationManager.ConnectionStrings["connTMS"].ToString();
                else return base.ConnectionString;
            }
        }

        public List<TickerImage> GetTickerImages(int? channelId, TickerImageStatuses tickerImageStatus, int pageIndex, int pageCount)
        {
            string sql = string.Format(@"select * 
                           from 
                                [TickerImage] with (nolock)  
                           where 
                                [ChannelId] = ISNULL(@ChannelId, [ChannelId]) and 
                                [TickerImageStatusId] = @TickerImageStatus
                           order by [TickerImageId] asc OFFSET {0} ROWS FETCH NEXT {1} ROWS ONLY", pageIndex.ToString(), pageCount.ToString());

            SqlParameter[] parameters = new SqlParameter[] {
                new SqlParameter("@TickerImageStatus", (int)tickerImageStatus),
                new SqlParameter("@ChannelId", channelId.HasValue ? channelId.Value : (object)DBNull.Value)
            };

            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, parameters);
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<TickerImage>(ds, TickerImageFromDataRow);
        }

        public bool UpdateTickerImageStatus(int tickerImageId, int statusId)
        {
            string sql = @"Update TickerImage set
							    [LastUpdateDate]=@LastUpdateDate
							    ,[TickerImageStatusId]=@TickerImageStatusId 
                           where 
                                TickerImageId=@TickerImageId";
            SqlParameter[] parameterArray = new SqlParameter[]{
			    new SqlParameter("@TickerImageStatusId", statusId),
                new SqlParameter("@TickerImageId", tickerImageId),
                new SqlParameter("@LastUpdateDate", DateTime.UtcNow)
            };
            return SqlHelper.ExecuteNonQuery(this.ConnectionString, CommandType.Text, sql, parameterArray) > 0;
        }
	}
	
	
}
