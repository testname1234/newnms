﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using TMS.Core;
using TMS.Core.Entities;
using TMS.Core.DataInterfaces;
using TMS.Core.Extensions;

namespace TMS.Repository
{
		
	public abstract partial class TickerImageStatusRepositoryBase : Repository, ITickerImageStatusRepositoryBase 
	{
        
        public TickerImageStatusRepositoryBase()
        {   
            this.SearchColumns=new Dictionary<string, SearchColumn>();

			this.SearchColumns.Add("TickerImageStatusId",new SearchColumn(){Name="TickerImageStatusId",Title="TickerImageStatusId",SelectClause="TickerImageStatusId",WhereClause="AllRecords.TickerImageStatusId",DataType="System.Int32",IsForeignColumn=false,PropertyName="TickerImageStatusId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("TickerImageStatusName",new SearchColumn(){Name="TickerImageStatusName",Title="TickerImageStatusName",SelectClause="TickerImageStatusName",WhereClause="AllRecords.TickerImageStatusName",DataType="System.String",IsForeignColumn=false,PropertyName="TickerImageStatusName",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});        
        }
        
		public virtual List<SearchColumn> GetTickerImageStatusSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                searchColumns.Add(keyValuePair.Value);
            }
            return searchColumns;
        }
		
		
		
        public virtual Dictionary<string, string> GetTickerImageStatusBasicSearchColumns()
        {
			Dictionary<string, string> columnList = new Dictionary<string, string>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsBasicSearchColumm)
                {
					keyValuePair.Value.Value = string.Empty;
                    columnList.Add(keyValuePair.Key, keyValuePair.Value.Title);
                }
            }
            return columnList;
        }

        public virtual List<SearchColumn> GetTickerImageStatusAdvanceSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsAdvanceSearchColumn)
                {
					keyValuePair.Value.Value = string.Empty;
					searchColumns.Add(keyValuePair.Value);
                }
            }
            return searchColumns;
        }
        
        
        public virtual string GetTickerImageStatusSelectClause()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            string selectQuery=string.Empty;
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (!keyValuePair.Value.IgnoreForDefaultSelect)
                {
					if (keyValuePair.Value.IsForeignColumn)
                	{
						if(string.IsNullOrEmpty(selectQuery))
						{
							selectQuery = "("+keyValuePair.Value.SelectClause+") as \""+keyValuePair.Key+"\"";
						}
						else
						{
							selectQuery += ",(" + keyValuePair.Value.SelectClause + ") as \"" + keyValuePair.Key + "\"";
						}
                	}
                	else
                	{
                    	if (string.IsNullOrEmpty(selectQuery))
                    	{
                        	selectQuery =  "[TickerImageStatus].["+keyValuePair.Key+"]";
                    	}
                    	else
                    	{
                        	selectQuery += ",[TickerImageStatus].["+keyValuePair.Key+"]";
                    	}
                	}
            	}
            }
            return "Select "+selectQuery+" ";
        }
        

		public virtual TickerImageStatus GetTickerImageStatus(System.Int32 TickerImageStatusId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetTickerImageStatusSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [TickerImageStatus] with (nolock)  where TickerImageStatusId=@TickerImageStatusId ";
			SqlParameter parameter=new SqlParameter("@TickerImageStatusId",TickerImageStatusId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
			return TickerImageStatusFromDataRow(ds.Tables[0].Rows[0]);
		}

		public  List<TickerImageStatus> GetTickerImageStatusByKeyValue(string Key,string Value,Operands operand,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetTickerImageStatusSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+= string.Format("from [TickerImageStatus] with (nolock)  where {0} {1} '{2}' ",Key,operand.ToOperandString(),Value);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<TickerImageStatus>(ds,TickerImageStatusFromDataRow);
		}

		public virtual List<TickerImageStatus> GetAllTickerImageStatus(string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetTickerImageStatusSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [TickerImageStatus] with (nolock)  ";
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,null);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<TickerImageStatus>(ds, TickerImageStatusFromDataRow);
		}

		public virtual List<TickerImageStatus> GetPagedTickerImageStatus(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null)
		{

			string whereClause = base.GetAdvancedWhereClauseByColumn(searchColumns, GetSearchColumns());
               if (!String.IsNullOrEmpty(orderByClause))
               {
                   KeyValuePair<string, string> parsedOrderByClause = base.ParseOrderByClause(orderByClause);
                   orderByClause = base.GetBasicSearchOrderByClauseByColumn(parsedOrderByClause.Key, parsedOrderByClause.Value, this.SearchColumns);
               }

            count=GetTickerImageStatusCount(whereClause, searchColumns);
			if(count>0)
			{
			if (count < startIndex) startIndex = (count / pageSize) * pageSize;			
			
           	int PageLowerBound = startIndex;
            int PageUpperBound = PageLowerBound + pageSize;
            string sql = @"CREATE TABLE #PageIndex
				            (
				                [IndexId] int IDENTITY (1, 1) NOT NULL,
				                [TickerImageStatusId] int				   
				            );";

            //Insert into the temp table
            string tempsql = "INSERT INTO #PageIndex ([TickerImageStatusId])";
            tempsql += " SELECT ";
            if (pageSize > 0) tempsql += "TOP " + PageUpperBound.ToString();
            tempsql += " [TickerImageStatusId] ";
            tempsql += " FROM [TickerImageStatus] AllRecords with (NOLOCK)";
            if (!string.IsNullOrEmpty(whereClause) && whereClause.Length > 0) tempsql += " WHERE " + whereClause;
            if (orderByClause.Length > 0) 
			{
				tempsql += " ORDER BY " + orderByClause;
				if( !orderByClause.Contains("TickerImageStatusId"))
					tempsql += " , (AllRecords.[TickerImageStatusId])"; 
			}
			else 
			{
				tempsql  += " ORDER BY (AllRecords.[TickerImageStatusId])"; 
			}           
            
            // Return paged results
            string pagedResultsSql =
                (string.IsNullOrEmpty(SelectClause)? GetTickerImageStatusSelectClause():(string.Format("Select {0} ",SelectClause)))+@" FROM [TickerImageStatus] , #PageIndex PageIndex WHERE ";
            pagedResultsSql += " PageIndex.IndexId > " + PageLowerBound.ToString(); 
            pagedResultsSql += @" AND [TickerImageStatus].[TickerImageStatusId] = PageIndex.[TickerImageStatusId] 
				                  ORDER BY PageIndex.IndexId;";
            pagedResultsSql += " drop table #PageIndex";
            sql = sql + tempsql + pagedResultsSql;
			sql = string.Format(sql, whereClause, pageSize, startIndex, orderByClause);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql, GetSQLParamtersBySearchColumns(searchColumns));
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<TickerImageStatus>(ds, TickerImageStatusFromDataRow);
			}else{ return null;}
		}

		private int GetTickerImageStatusCount(string whereClause, List<SearchColumn> searchColumns)
		{

			string sql=string.Empty;
			if(string.IsNullOrEmpty(whereClause))
				sql = "SELECT Count(*) FROM TickerImageStatus as AllRecords  ";
			else
				sql = "SELECT Count(*) FROM TickerImageStatus as AllRecords  where  " +whereClause;
			var rowCount = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, GetSQLParamtersBySearchColumns(searchColumns));
			return rowCount == DBNull.Value ? 0 :(int)rowCount;
		}

		[MOLog(AuditOperations.Create,typeof(TickerImageStatus))]
		public virtual TickerImageStatus InsertTickerImageStatus(TickerImageStatus entity)
		{

			TickerImageStatus other=new TickerImageStatus();
			other = entity;
			if(entity.IsTransient())
			{
				string sql=@"Insert into TickerImageStatus ( [TickerImageStatusName] )
				Values
				( @TickerImageStatusName );
				Select scope_identity()";
				SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@TickerImageStatusName",entity.TickerImageStatusName ?? (object)DBNull.Value)};
				var identity=SqlHelper.ExecuteScalar(this.ConnectionString,CommandType.Text,sql,parameterArray);
				if(identity==DBNull.Value) throw new DataException("Identity column was null as a result of the insert operation.");
				return GetTickerImageStatus(Convert.ToInt32(identity));
			}
			return entity;
		}

		[MOLog(AuditOperations.Update,typeof(TickerImageStatus))]
		public virtual TickerImageStatus UpdateTickerImageStatus(TickerImageStatus entity)
		{

			if (entity.IsTransient()) return entity;
			TickerImageStatus other = GetTickerImageStatus(entity.TickerImageStatusId);
			if (entity.Equals(other)) return entity;
			string sql=@"Update TickerImageStatus set  [TickerImageStatusName]=@TickerImageStatusName 
							 where TickerImageStatusId=@TickerImageStatusId";
			SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@TickerImageStatusName",entity.TickerImageStatusName ?? (object)DBNull.Value)
					, new SqlParameter("@TickerImageStatusId",entity.TickerImageStatusId)};
			SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,parameterArray);
			return GetTickerImageStatus(entity.TickerImageStatusId);
		}

		public virtual bool DeleteTickerImageStatus(System.Int32 TickerImageStatusId)
		{

			string sql="delete from TickerImageStatus where TickerImageStatusId=@TickerImageStatusId";
			SqlParameter parameter=new SqlParameter("@TickerImageStatusId",TickerImageStatusId);
			var identity=SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			return (Convert.ToInt32(identity))==1? true: false;
		}

		[MOLog(AuditOperations.Delete,typeof(TickerImageStatus))]
		public virtual TickerImageStatus DeleteTickerImageStatus(TickerImageStatus entity)
		{
			this.DeleteTickerImageStatus(entity.TickerImageStatusId);
			return entity;
		}


		public virtual TickerImageStatus TickerImageStatusFromDataRow(DataRow dr)
		{
			if(dr==null) return null;
			TickerImageStatus entity=new TickerImageStatus();
			if (dr.Table.Columns.Contains("TickerImageStatusId"))
			{
			entity.TickerImageStatusId = (System.Int32)dr["TickerImageStatusId"];
			}
			if (dr.Table.Columns.Contains("TickerImageStatusName"))
			{
			entity.TickerImageStatusName = dr["TickerImageStatusName"].ToString();
			}
			return entity;
		}

	}
	
	
}
