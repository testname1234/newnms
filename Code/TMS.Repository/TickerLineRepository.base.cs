﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using TMS.Core;
using TMS.Core.Entities;
using TMS.Core.DataInterfaces;
using TMS.Core.Extensions;

namespace TMS.Repository
{
		
	public abstract partial class TickerLineRepositoryBase : Repository, ITickerLineRepositoryBase 
	{
        
        public TickerLineRepositoryBase()
        {   
            this.SearchColumns=new Dictionary<string, SearchColumn>();

			this.SearchColumns.Add("TickerLineId",new SearchColumn(){Name="TickerLineId",Title="TickerLineId",SelectClause="TickerLineId",WhereClause="AllRecords.TickerLineId",DataType="System.Int32",IsForeignColumn=false,PropertyName="TickerLineId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Text",new SearchColumn(){Name="Text",Title="Text",SelectClause="Text",WhereClause="AllRecords.Text",DataType="System.String",IsForeignColumn=false,PropertyName="Text",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("LanguageCode",new SearchColumn(){Name="LanguageCode",Title="LanguageCode",SelectClause="LanguageCode",WhereClause="AllRecords.LanguageCode",DataType="System.String",IsForeignColumn=false,PropertyName="LanguageCode",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("TickerId",new SearchColumn(){Name="TickerId",Title="TickerId",SelectClause="TickerId",WhereClause="AllRecords.TickerId",DataType="System.Int32?",IsForeignColumn=false,PropertyName="TickerId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("SequenceId",new SearchColumn(){Name="SequenceId",Title="SequenceId",SelectClause="SequenceId",WhereClause="AllRecords.SequenceId",DataType="System.Int32?",IsForeignColumn=false,PropertyName="SequenceId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("ChannelId",new SearchColumn(){Name="ChannelId",Title="ChannelId",SelectClause="ChannelId",WhereClause="AllRecords.ChannelId",DataType="System.Int32?",IsForeignColumn=false,PropertyName="ChannelId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("CreationDate",new SearchColumn(){Name="CreationDate",Title="CreationDate",SelectClause="CreationDate",WhereClause="AllRecords.CreationDate",DataType="System.DateTime?",IsForeignColumn=false,PropertyName="CreationDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("LastUpdatedDate",new SearchColumn(){Name="LastUpdatedDate",Title="LastUpdatedDate",SelectClause="LastUpdatedDate",WhereClause="AllRecords.LastUpdatedDate",DataType="System.DateTime?",IsForeignColumn=false,PropertyName="LastUpdatedDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("IsActive",new SearchColumn(){Name="IsActive",Title="IsActive",SelectClause="IsActive",WhereClause="AllRecords.IsActive",DataType="System.Boolean?",IsForeignColumn=false,PropertyName="IsActive",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("UserId",new SearchColumn(){Name="UserId",Title="UserId",SelectClause="UserId",WhereClause="AllRecords.UserId",DataType="System.Int32?",IsForeignColumn=false,PropertyName="UserId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});        
        }
        
		public virtual List<SearchColumn> GetTickerLineSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                searchColumns.Add(keyValuePair.Value);
            }
            return searchColumns;
        }
		
		
		
        public virtual Dictionary<string, string> GetTickerLineBasicSearchColumns()
        {
			Dictionary<string, string> columnList = new Dictionary<string, string>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsBasicSearchColumm)
                {
					keyValuePair.Value.Value = string.Empty;
                    columnList.Add(keyValuePair.Key, keyValuePair.Value.Title);
                }
            }
            return columnList;
        }

        public virtual List<SearchColumn> GetTickerLineAdvanceSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsAdvanceSearchColumn)
                {
					keyValuePair.Value.Value = string.Empty;
					searchColumns.Add(keyValuePair.Value);
                }
            }
            return searchColumns;
        }
        
        
        public virtual string GetTickerLineSelectClause()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            string selectQuery=string.Empty;
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (!keyValuePair.Value.IgnoreForDefaultSelect)
                {
					if (keyValuePair.Value.IsForeignColumn)
                	{
						if(string.IsNullOrEmpty(selectQuery))
						{
							selectQuery = "("+keyValuePair.Value.SelectClause+") as \""+keyValuePair.Key+"\"";
						}
						else
						{
							selectQuery += ",(" + keyValuePair.Value.SelectClause + ") as \"" + keyValuePair.Key + "\"";
						}
                	}
                	else
                	{
                    	if (string.IsNullOrEmpty(selectQuery))
                    	{
                        	selectQuery =  "[TickerLine].["+keyValuePair.Key+"]";
                    	}
                    	else
                    	{
                        	selectQuery += ",[TickerLine].["+keyValuePair.Key+"]";
                    	}
                	}
            	}
            }
            return "Select "+selectQuery+" ";
        }
        

		public virtual List<TickerLine> GetTickerLineByTickerId(System.Int32? TickerId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetTickerLineSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [TickerLine] with (nolock)  where TickerId=@TickerId  ";
			SqlParameter parameter=new SqlParameter("@TickerId",TickerId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<TickerLine>(ds,TickerLineFromDataRow);
		}

		public virtual TickerLine GetTickerLine(System.Int32 TickerLineId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetTickerLineSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [TickerLine] with (nolock)  where TickerLineId=@TickerLineId ";
			SqlParameter parameter=new SqlParameter("@TickerLineId",TickerLineId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
			return TickerLineFromDataRow(ds.Tables[0].Rows[0]);
		}

		public  List<TickerLine> GetTickerLineByKeyValue(string Key,string Value,Operands operand,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetTickerLineSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+= string.Format("from [TickerLine] with (nolock)  where {0} {1} '{2}' ",Key,operand.ToOperandString(),Value);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<TickerLine>(ds,TickerLineFromDataRow);
		}

		public virtual List<TickerLine> GetAllTickerLine(string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetTickerLineSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [TickerLine] with (nolock)  ";
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,null);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<TickerLine>(ds, TickerLineFromDataRow);
		}

		public virtual List<TickerLine> GetPagedTickerLine(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null)
		{

			string whereClause = base.GetAdvancedWhereClauseByColumn(searchColumns, GetSearchColumns());
               if (!String.IsNullOrEmpty(orderByClause))
               {
                   KeyValuePair<string, string> parsedOrderByClause = base.ParseOrderByClause(orderByClause);
                   orderByClause = base.GetBasicSearchOrderByClauseByColumn(parsedOrderByClause.Key, parsedOrderByClause.Value, this.SearchColumns);
               }

            count=GetTickerLineCount(whereClause, searchColumns);
			if(count>0)
			{
			if (count < startIndex) startIndex = (count / pageSize) * pageSize;			
			
           	int PageLowerBound = startIndex;
            int PageUpperBound = PageLowerBound + pageSize;
            string sql = @"CREATE TABLE #PageIndex
				            (
				                [IndexId] int IDENTITY (1, 1) NOT NULL,
				                [TickerLineId] int				   
				            );";

            //Insert into the temp table
            string tempsql = "INSERT INTO #PageIndex ([TickerLineId])";
            tempsql += " SELECT ";
            if (pageSize > 0) tempsql += "TOP " + PageUpperBound.ToString();
            tempsql += " [TickerLineId] ";
            tempsql += " FROM [TickerLine] AllRecords with (NOLOCK)";
            if (!string.IsNullOrEmpty(whereClause) && whereClause.Length > 0) tempsql += " WHERE " + whereClause;
            if (orderByClause.Length > 0) 
			{
				tempsql += " ORDER BY " + orderByClause;
				if( !orderByClause.Contains("TickerLineId"))
					tempsql += " , (AllRecords.[TickerLineId])"; 
			}
			else 
			{
				tempsql  += " ORDER BY (AllRecords.[TickerLineId])"; 
			}           
            
            // Return paged results
            string pagedResultsSql =
                (string.IsNullOrEmpty(SelectClause)? GetTickerLineSelectClause():(string.Format("Select {0} ",SelectClause)))+@" FROM [TickerLine] , #PageIndex PageIndex WHERE ";
            pagedResultsSql += " PageIndex.IndexId > " + PageLowerBound.ToString(); 
            pagedResultsSql += @" AND [TickerLine].[TickerLineId] = PageIndex.[TickerLineId] 
				                  ORDER BY PageIndex.IndexId;";
            pagedResultsSql += " drop table #PageIndex";
            sql = sql + tempsql + pagedResultsSql;
			sql = string.Format(sql, whereClause, pageSize, startIndex, orderByClause);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql, GetSQLParamtersBySearchColumns(searchColumns));
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<TickerLine>(ds, TickerLineFromDataRow);
			}else{ return null;}
		}

		private int GetTickerLineCount(string whereClause, List<SearchColumn> searchColumns)
		{

			string sql=string.Empty;
			if(string.IsNullOrEmpty(whereClause))
				sql = "SELECT Count(*) FROM TickerLine as AllRecords  ";
			else
				sql = "SELECT Count(*) FROM TickerLine as AllRecords  where  " +whereClause;
			var rowCount = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, GetSQLParamtersBySearchColumns(searchColumns));
			return rowCount == DBNull.Value ? 0 :(int)rowCount;
		}

		[MOLog(AuditOperations.Create,typeof(TickerLine))]
		public virtual TickerLine InsertTickerLine(TickerLine entity)
		{

			TickerLine other=new TickerLine();
			other = entity;
			if(entity.IsTransient())
			{
				string sql=@"Insert into TickerLine ( [Text]
				,[LanguageCode]
				,[TickerId]
				,[SequenceId]
				,[ChannelId]
				,[CreationDate]
				,[LastUpdatedDate]
				,[IsActive]
				,[UserId] )
				Values
				( @Text
				, @LanguageCode
				, @TickerId
				, @SequenceId
				, @ChannelId
				, @CreationDate
				, @LastUpdatedDate
				, @IsActive
				, @UserId );
				Select scope_identity()";
				SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@Text",entity.Text ?? (object)DBNull.Value)
					, new SqlParameter("@LanguageCode",entity.LanguageCode ?? (object)DBNull.Value)
					, new SqlParameter("@TickerId",entity.TickerId ?? (object)DBNull.Value)
					, new SqlParameter("@SequenceId",entity.SequenceId ?? (object)DBNull.Value)
					, new SqlParameter("@ChannelId",entity.ChannelId ?? (object)DBNull.Value)
					, new SqlParameter("@CreationDate",entity.CreationDate ?? (object)DBNull.Value)
					, new SqlParameter("@LastUpdatedDate",entity.LastUpdatedDate ?? (object)DBNull.Value)
					, new SqlParameter("@IsActive",entity.IsActive ?? (object)DBNull.Value)
					, new SqlParameter("@UserId",entity.UserId ?? (object)DBNull.Value)};
				var identity=SqlHelper.ExecuteScalar(this.ConnectionString,CommandType.Text,sql,parameterArray);
				if(identity==DBNull.Value) throw new DataException("Identity column was null as a result of the insert operation.");
				return GetTickerLine(Convert.ToInt32(identity));
			}
			return entity;
		}

		[MOLog(AuditOperations.Update,typeof(TickerLine))]
		public virtual TickerLine UpdateTickerLine(TickerLine entity)
		{

			if (entity.IsTransient()) return entity;
			TickerLine other = GetTickerLine(entity.TickerLineId);
			if (entity.Equals(other)) return entity;
			string sql=@"Update TickerLine set  [Text]=@Text
							, [LanguageCode]=@LanguageCode
							, [TickerId]=@TickerId
							, [SequenceId]=@SequenceId
							, [ChannelId]=@ChannelId
							, [CreationDate]=@CreationDate
							, [LastUpdatedDate]=@LastUpdatedDate
							, [IsActive]=@IsActive
							, [UserId]=@UserId 
							 where TickerLineId=@TickerLineId";
			SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@Text",entity.Text ?? (object)DBNull.Value)
					, new SqlParameter("@LanguageCode",entity.LanguageCode ?? (object)DBNull.Value)
					, new SqlParameter("@TickerId",entity.TickerId ?? (object)DBNull.Value)
					, new SqlParameter("@SequenceId",entity.SequenceId ?? (object)DBNull.Value)
					, new SqlParameter("@ChannelId",entity.ChannelId ?? (object)DBNull.Value)
					, new SqlParameter("@CreationDate",entity.CreationDate ?? (object)DBNull.Value)
					, new SqlParameter("@LastUpdatedDate",entity.LastUpdatedDate ?? (object)DBNull.Value)
					, new SqlParameter("@IsActive",entity.IsActive ?? (object)DBNull.Value)
					, new SqlParameter("@UserId",entity.UserId ?? (object)DBNull.Value)
					, new SqlParameter("@TickerLineId",entity.TickerLineId)};
			SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,parameterArray);
			return GetTickerLine(entity.TickerLineId);
		}

		public virtual bool DeleteTickerLine(System.Int32 TickerLineId)
		{

			string sql="delete from TickerLine where TickerLineId=@TickerLineId";
			SqlParameter parameter=new SqlParameter("@TickerLineId",TickerLineId);
			var identity=SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			return (Convert.ToInt32(identity))==1? true: false;
		}

		[MOLog(AuditOperations.Delete,typeof(TickerLine))]
		public virtual TickerLine DeleteTickerLine(TickerLine entity)
		{
			this.DeleteTickerLine(entity.TickerLineId);
			return entity;
		}


		public virtual TickerLine TickerLineFromDataRow(DataRow dr)
		{
			if(dr==null) return null;
			TickerLine entity=new TickerLine();
			if (dr.Table.Columns.Contains("TickerLineId"))
			{
			entity.TickerLineId = (System.Int32)dr["TickerLineId"];
			}
			if (dr.Table.Columns.Contains("Text"))
			{
			entity.Text = dr["Text"].ToString();
			}
			if (dr.Table.Columns.Contains("LanguageCode"))
			{
			entity.LanguageCode = dr["LanguageCode"].ToString();
			}
			if (dr.Table.Columns.Contains("TickerId"))
			{
			entity.TickerId = dr["TickerId"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["TickerId"];
			}
			if (dr.Table.Columns.Contains("SequenceId"))
			{
			entity.SequenceId = dr["SequenceId"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["SequenceId"];
			}
			if (dr.Table.Columns.Contains("ChannelId"))
			{
			entity.ChannelId = dr["ChannelId"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["ChannelId"];
			}
			if (dr.Table.Columns.Contains("CreationDate"))
			{
			entity.CreationDate = dr["CreationDate"]==DBNull.Value?(System.DateTime?)null:(System.DateTime?)dr["CreationDate"];
			}
			if (dr.Table.Columns.Contains("LastUpdatedDate"))
			{
			entity.LastUpdatedDate = dr["LastUpdatedDate"]==DBNull.Value?(System.DateTime?)null:(System.DateTime?)dr["LastUpdatedDate"];
			}
			if (dr.Table.Columns.Contains("IsActive"))
			{
			entity.IsActive = dr["IsActive"]==DBNull.Value?(System.Boolean?)null:(System.Boolean?)dr["IsActive"];
			}
			if (dr.Table.Columns.Contains("UserId"))
			{
			entity.UserId = dr["UserId"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["UserId"];
			}
			return entity;
		}

	}
	
	
}
