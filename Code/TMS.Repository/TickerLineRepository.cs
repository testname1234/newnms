﻿using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using TMS.Core.DataInterfaces;
using TMS.Core.Entities;

namespace TMS.Repository
{
    public partial class TickerLineRepository : TickerLineRepositoryBase, ITickerLineRepository
    {
        public override string ConnectionString
        {
            get
            {
                if (ConfigurationManager.ConnectionStrings["connTMS"] != null)
                    return ConfigurationManager.ConnectionStrings["connTMS"].ToString();
                else return base.ConnectionString;
            }
        }

        public List<TickerLine> GetTickerLines(int tickerId, string keyword)
        {
            string sql = string.Format("select top 10 * from [TickerLine] with (nolock)  where [TickerId] = @TickerId and [Text] like '%{0}%'", keyword);

            SqlParameter[] parameters = new SqlParameter[] {
                new SqlParameter("@TickerId", tickerId),
            };

            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, parameters);
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<TickerLine>(ds, TickerLineFromDataRow);
        }
    }
}