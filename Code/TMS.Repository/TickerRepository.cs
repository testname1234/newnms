﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using TMS.Core.Entities;
using TMS.Core.DataInterfaces;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;

namespace TMS.Repository
{
		
	public partial class TickerRepository: TickerRepositoryBase, ITickerRepository
	{
        public override string ConnectionString
        {
            get
            {
                if (ConfigurationManager.ConnectionStrings["connTMS"] != null)
                    return ConfigurationManager.ConnectionStrings["connTMS"].ToString();
                else return base.ConnectionString;
            }
        }

        public List<Ticker> GetTickers(string keyword)
        {
            string sql = string.Format("select top 10 * from [Ticker] with (nolock)  where [GroupName] like N'%{0}%' and [CreationDate] between @StartTime and @EndTime", keyword);

            SqlParameter[] parameters = new SqlParameter[] { 
                new SqlParameter("@StartTime", DateTime.UtcNow.AddDays(-1).ToUniversalTime()),
                new SqlParameter("@EndTime", DateTime.UtcNow)
            };

            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, parameters);
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<Ticker>(ds, TickerFromDataRow);
        }
	}
	
	
}
