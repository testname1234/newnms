﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using TMS.Core;
using TMS.Core.Entities;
using TMS.Core.DataInterfaces;
using TMS.Core.Extensions;

namespace TMS.Repository
{
		
	public abstract partial class TickerTypeRepositoryBase : Repository, ITickerTypeRepositoryBase 
	{
        
        public TickerTypeRepositoryBase()
        {   
            this.SearchColumns=new Dictionary<string, SearchColumn>();

			this.SearchColumns.Add("TickerTypeId",new SearchColumn(){Name="TickerTypeId",Title="TickerTypeId",SelectClause="TickerTypeId",WhereClause="AllRecords.TickerTypeId",DataType="System.Int32",IsForeignColumn=false,PropertyName="TickerTypeId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("TickerTypeName",new SearchColumn(){Name="TickerTypeName",Title="TickerTypeName",SelectClause="TickerTypeName",WhereClause="AllRecords.TickerTypeName",DataType="System.String",IsForeignColumn=false,PropertyName="TickerTypeName",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});        
        }
        
		public virtual List<SearchColumn> GetTickerTypeSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                searchColumns.Add(keyValuePair.Value);
            }
            return searchColumns;
        }
		
		
		
        public virtual Dictionary<string, string> GetTickerTypeBasicSearchColumns()
        {
			Dictionary<string, string> columnList = new Dictionary<string, string>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsBasicSearchColumm)
                {
					keyValuePair.Value.Value = string.Empty;
                    columnList.Add(keyValuePair.Key, keyValuePair.Value.Title);
                }
            }
            return columnList;
        }

        public virtual List<SearchColumn> GetTickerTypeAdvanceSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsAdvanceSearchColumn)
                {
					keyValuePair.Value.Value = string.Empty;
					searchColumns.Add(keyValuePair.Value);
                }
            }
            return searchColumns;
        }
        
        
        public virtual string GetTickerTypeSelectClause()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            string selectQuery=string.Empty;
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (!keyValuePair.Value.IgnoreForDefaultSelect)
                {
					if (keyValuePair.Value.IsForeignColumn)
                	{
						if(string.IsNullOrEmpty(selectQuery))
						{
							selectQuery = "("+keyValuePair.Value.SelectClause+") as \""+keyValuePair.Key+"\"";
						}
						else
						{
							selectQuery += ",(" + keyValuePair.Value.SelectClause + ") as \"" + keyValuePair.Key + "\"";
						}
                	}
                	else
                	{
                    	if (string.IsNullOrEmpty(selectQuery))
                    	{
                        	selectQuery =  "[TickerType].["+keyValuePair.Key+"]";
                    	}
                    	else
                    	{
                        	selectQuery += ",[TickerType].["+keyValuePair.Key+"]";
                    	}
                	}
            	}
            }
            return "Select "+selectQuery+" ";
        }
        

		public virtual TickerType GetTickerType(System.Int32 TickerTypeId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetTickerTypeSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [TickerType] with (nolock)  where TickerTypeId=@TickerTypeId ";
			SqlParameter parameter=new SqlParameter("@TickerTypeId",TickerTypeId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
			return TickerTypeFromDataRow(ds.Tables[0].Rows[0]);
		}

		public  List<TickerType> GetTickerTypeByKeyValue(string Key,string Value,Operands operand,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetTickerTypeSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+= string.Format("from [TickerType] with (nolock)  where {0} {1} '{2}' ",Key,operand.ToOperandString(),Value);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<TickerType>(ds,TickerTypeFromDataRow);
		}

		public virtual List<TickerType> GetAllTickerType(string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetTickerTypeSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [TickerType] with (nolock)  ";
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,null);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<TickerType>(ds, TickerTypeFromDataRow);
		}

		public virtual List<TickerType> GetPagedTickerType(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null)
		{

			string whereClause = base.GetAdvancedWhereClauseByColumn(searchColumns, GetSearchColumns());
               if (!String.IsNullOrEmpty(orderByClause))
               {
                   KeyValuePair<string, string> parsedOrderByClause = base.ParseOrderByClause(orderByClause);
                   orderByClause = base.GetBasicSearchOrderByClauseByColumn(parsedOrderByClause.Key, parsedOrderByClause.Value, this.SearchColumns);
               }

            count=GetTickerTypeCount(whereClause, searchColumns);
			if(count>0)
			{
			if (count < startIndex) startIndex = (count / pageSize) * pageSize;			
			
           	int PageLowerBound = startIndex;
            int PageUpperBound = PageLowerBound + pageSize;
            string sql = @"CREATE TABLE #PageIndex
				            (
				                [IndexId] int IDENTITY (1, 1) NOT NULL,
				                [TickerTypeId] int				   
				            );";

            //Insert into the temp table
            string tempsql = "INSERT INTO #PageIndex ([TickerTypeId])";
            tempsql += " SELECT ";
            if (pageSize > 0) tempsql += "TOP " + PageUpperBound.ToString();
            tempsql += " [TickerTypeId] ";
            tempsql += " FROM [TickerType] AllRecords with (NOLOCK)";
            if (!string.IsNullOrEmpty(whereClause) && whereClause.Length > 0) tempsql += " WHERE " + whereClause;
            if (orderByClause.Length > 0) 
			{
				tempsql += " ORDER BY " + orderByClause;
				if( !orderByClause.Contains("TickerTypeId"))
					tempsql += " , (AllRecords.[TickerTypeId])"; 
			}
			else 
			{
				tempsql  += " ORDER BY (AllRecords.[TickerTypeId])"; 
			}           
            
            // Return paged results
            string pagedResultsSql =
                (string.IsNullOrEmpty(SelectClause)? GetTickerTypeSelectClause():(string.Format("Select {0} ",SelectClause)))+@" FROM [TickerType] , #PageIndex PageIndex WHERE ";
            pagedResultsSql += " PageIndex.IndexId > " + PageLowerBound.ToString(); 
            pagedResultsSql += @" AND [TickerType].[TickerTypeId] = PageIndex.[TickerTypeId] 
				                  ORDER BY PageIndex.IndexId;";
            pagedResultsSql += " drop table #PageIndex";
            sql = sql + tempsql + pagedResultsSql;
			sql = string.Format(sql, whereClause, pageSize, startIndex, orderByClause);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql, GetSQLParamtersBySearchColumns(searchColumns));
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<TickerType>(ds, TickerTypeFromDataRow);
			}else{ return null;}
		}

		private int GetTickerTypeCount(string whereClause, List<SearchColumn> searchColumns)
		{

			string sql=string.Empty;
			if(string.IsNullOrEmpty(whereClause))
				sql = "SELECT Count(*) FROM TickerType as AllRecords  ";
			else
				sql = "SELECT Count(*) FROM TickerType as AllRecords  where  " +whereClause;
			var rowCount = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, GetSQLParamtersBySearchColumns(searchColumns));
			return rowCount == DBNull.Value ? 0 :(int)rowCount;
		}

		[MOLog(AuditOperations.Create,typeof(TickerType))]
		public virtual TickerType InsertTickerType(TickerType entity)
		{

			TickerType other=new TickerType();
			other = entity;
			if(entity.IsTransient())
			{
				string sql=@"Insert into TickerType ( [TickerTypeName] )
				Values
				( @TickerTypeName );
				Select scope_identity()";
				SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@TickerTypeName",entity.TickerTypeName ?? (object)DBNull.Value)};
				var identity=SqlHelper.ExecuteScalar(this.ConnectionString,CommandType.Text,sql,parameterArray);
				if(identity==DBNull.Value) throw new DataException("Identity column was null as a result of the insert operation.");
				return GetTickerType(Convert.ToInt32(identity));
			}
			return entity;
		}

		[MOLog(AuditOperations.Update,typeof(TickerType))]
		public virtual TickerType UpdateTickerType(TickerType entity)
		{

			if (entity.IsTransient()) return entity;
			TickerType other = GetTickerType(entity.TickerTypeId);
			if (entity.Equals(other)) return entity;
			string sql=@"Update TickerType set  [TickerTypeName]=@TickerTypeName 
							 where TickerTypeId=@TickerTypeId";
			SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@TickerTypeName",entity.TickerTypeName ?? (object)DBNull.Value)
					, new SqlParameter("@TickerTypeId",entity.TickerTypeId)};
			SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,parameterArray);
			return GetTickerType(entity.TickerTypeId);
		}

		public virtual bool DeleteTickerType(System.Int32 TickerTypeId)
		{

			string sql="delete from TickerType where TickerTypeId=@TickerTypeId";
			SqlParameter parameter=new SqlParameter("@TickerTypeId",TickerTypeId);
			var identity=SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			return (Convert.ToInt32(identity))==1? true: false;
		}

		[MOLog(AuditOperations.Delete,typeof(TickerType))]
		public virtual TickerType DeleteTickerType(TickerType entity)
		{
			this.DeleteTickerType(entity.TickerTypeId);
			return entity;
		}


		public virtual TickerType TickerTypeFromDataRow(DataRow dr)
		{
			if(dr==null) return null;
			TickerType entity=new TickerType();
			if (dr.Table.Columns.Contains("TickerTypeId"))
			{
			entity.TickerTypeId = (System.Int32)dr["TickerTypeId"];
			}
			if (dr.Table.Columns.Contains("TickerTypeName"))
			{
			entity.TickerTypeName = dr["TickerTypeName"].ToString();
			}
			return entity;
		}

	}
	
	
}
