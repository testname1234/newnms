﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using TMS.Core;
using TMS.Core.Entities;
using TMS.Core.DataInterfaces;
using TMS.Core.Extensions;

namespace TMS.Repository
{
		
	public abstract partial class VideoRepositoryBase : Repository, IVideoRepositoryBase 
	{
        
        public VideoRepositoryBase()
        {   
            this.SearchColumns=new Dictionary<string, SearchColumn>();

			this.SearchColumns.Add("VideoId",new SearchColumn(){Name="VideoId",Title="VideoId",SelectClause="VideoId",WhereClause="AllRecords.VideoId",DataType="System.Int32",IsForeignColumn=false,PropertyName="VideoId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("Url",new SearchColumn(){Name="Url",Title="Url",SelectClause="Url",WhereClause="AllRecords.Url",DataType="System.String",IsForeignColumn=false,PropertyName="Url",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("StartTime",new SearchColumn(){Name="StartTime",Title="StartTime",SelectClause="StartTime",WhereClause="AllRecords.StartTime",DataType="System.DateTime?",IsForeignColumn=false,PropertyName="StartTime",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("EndTime",new SearchColumn(){Name="EndTime",Title="EndTime",SelectClause="EndTime",WhereClause="AllRecords.EndTime",DataType="System.DateTime?",IsForeignColumn=false,PropertyName="EndTime",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("CreationDate",new SearchColumn(){Name="CreationDate",Title="CreationDate",SelectClause="CreationDate",WhereClause="AllRecords.CreationDate",DataType="System.DateTime?",IsForeignColumn=false,PropertyName="CreationDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("LastUpdateDate",new SearchColumn(){Name="LastUpdateDate",Title="LastUpdateDate",SelectClause="LastUpdateDate",WhereClause="AllRecords.LastUpdateDate",DataType="System.DateTime?",IsForeignColumn=false,PropertyName="LastUpdateDate",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("IsActive",new SearchColumn(){Name="IsActive",Title="IsActive",SelectClause="IsActive",WhereClause="AllRecords.IsActive",DataType="System.Boolean?",IsForeignColumn=false,PropertyName="IsActive",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("ChannelId",new SearchColumn(){Name="ChannelId",Title="ChannelId",SelectClause="ChannelId",WhereClause="AllRecords.ChannelId",DataType="System.Int32?",IsForeignColumn=false,PropertyName="ChannelId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("VideoStatusId",new SearchColumn(){Name="VideoStatusId",Title="VideoStatusId",SelectClause="VideoStatusId",WhereClause="AllRecords.VideoStatusId",DataType="System.Int32?",IsForeignColumn=false,PropertyName="VideoStatusId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});        
        }
        
		public virtual List<SearchColumn> GetVideoSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                searchColumns.Add(keyValuePair.Value);
            }
            return searchColumns;
        }
		
		
		
        public virtual Dictionary<string, string> GetVideoBasicSearchColumns()
        {
			Dictionary<string, string> columnList = new Dictionary<string, string>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsBasicSearchColumm)
                {
					keyValuePair.Value.Value = string.Empty;
                    columnList.Add(keyValuePair.Key, keyValuePair.Value.Title);
                }
            }
            return columnList;
        }

        public virtual List<SearchColumn> GetVideoAdvanceSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsAdvanceSearchColumn)
                {
					keyValuePair.Value.Value = string.Empty;
					searchColumns.Add(keyValuePair.Value);
                }
            }
            return searchColumns;
        }
        
        
        public virtual string GetVideoSelectClause()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            string selectQuery=string.Empty;
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (!keyValuePair.Value.IgnoreForDefaultSelect)
                {
					if (keyValuePair.Value.IsForeignColumn)
                	{
						if(string.IsNullOrEmpty(selectQuery))
						{
							selectQuery = "("+keyValuePair.Value.SelectClause+") as \""+keyValuePair.Key+"\"";
						}
						else
						{
							selectQuery += ",(" + keyValuePair.Value.SelectClause + ") as \"" + keyValuePair.Key + "\"";
						}
                	}
                	else
                	{
                    	if (string.IsNullOrEmpty(selectQuery))
                    	{
                        	selectQuery =  "[Video].["+keyValuePair.Key+"]";
                    	}
                    	else
                    	{
                        	selectQuery += ",[Video].["+keyValuePair.Key+"]";
                    	}
                	}
            	}
            }
            return "Select "+selectQuery+" ";
        }
        

		public virtual List<Video> GetVideoByVideoStatusId(System.Int32? VideoStatusId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetVideoSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [Video] with (nolock)  where VideoStatusId=@VideoStatusId  ";
			SqlParameter parameter=new SqlParameter("@VideoStatusId",VideoStatusId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<Video>(ds,VideoFromDataRow);
		}

		public virtual Video GetVideo(System.Int32 VideoId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetVideoSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [Video] with (nolock)  where VideoId=@VideoId ";
			SqlParameter parameter=new SqlParameter("@VideoId",VideoId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
			return VideoFromDataRow(ds.Tables[0].Rows[0]);
		}

		public  List<Video> GetVideoByKeyValue(string Key,string Value,Operands operand,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetVideoSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+= string.Format("from [Video] with (nolock)  where {0} {1} '{2}' ",Key,operand.ToOperandString(),Value);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<Video>(ds,VideoFromDataRow);
		}

		public virtual List<Video> GetAllVideo(string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetVideoSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [Video] with (nolock)  ";
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,null);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<Video>(ds, VideoFromDataRow);
		}

		public virtual List<Video> GetPagedVideo(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null)
		{

			string whereClause = base.GetAdvancedWhereClauseByColumn(searchColumns, GetSearchColumns());
               if (!String.IsNullOrEmpty(orderByClause))
               {
                   KeyValuePair<string, string> parsedOrderByClause = base.ParseOrderByClause(orderByClause);
                   orderByClause = base.GetBasicSearchOrderByClauseByColumn(parsedOrderByClause.Key, parsedOrderByClause.Value, this.SearchColumns);
               }

            count=GetVideoCount(whereClause, searchColumns);
			if(count>0)
			{
			if (count < startIndex) startIndex = (count / pageSize) * pageSize;			
			
           	int PageLowerBound = startIndex;
            int PageUpperBound = PageLowerBound + pageSize;
            string sql = @"CREATE TABLE #PageIndex
				            (
				                [IndexId] int IDENTITY (1, 1) NOT NULL,
				                [VideoId] int				   
				            );";

            //Insert into the temp table
            string tempsql = "INSERT INTO #PageIndex ([VideoId])";
            tempsql += " SELECT ";
            if (pageSize > 0) tempsql += "TOP " + PageUpperBound.ToString();
            tempsql += " [VideoId] ";
            tempsql += " FROM [Video] AllRecords with (NOLOCK)";
            if (!string.IsNullOrEmpty(whereClause) && whereClause.Length > 0) tempsql += " WHERE " + whereClause;
            if (orderByClause.Length > 0) 
			{
				tempsql += " ORDER BY " + orderByClause;
				if( !orderByClause.Contains("VideoId"))
					tempsql += " , (AllRecords.[VideoId])"; 
			}
			else 
			{
				tempsql  += " ORDER BY (AllRecords.[VideoId])"; 
			}           
            
            // Return paged results
            string pagedResultsSql =
                (string.IsNullOrEmpty(SelectClause)? GetVideoSelectClause():(string.Format("Select {0} ",SelectClause)))+@" FROM [Video] , #PageIndex PageIndex WHERE ";
            pagedResultsSql += " PageIndex.IndexId > " + PageLowerBound.ToString(); 
            pagedResultsSql += @" AND [Video].[VideoId] = PageIndex.[VideoId] 
				                  ORDER BY PageIndex.IndexId;";
            pagedResultsSql += " drop table #PageIndex";
            sql = sql + tempsql + pagedResultsSql;
			sql = string.Format(sql, whereClause, pageSize, startIndex, orderByClause);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql, GetSQLParamtersBySearchColumns(searchColumns));
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<Video>(ds, VideoFromDataRow);
			}else{ return null;}
		}

		private int GetVideoCount(string whereClause, List<SearchColumn> searchColumns)
		{

			string sql=string.Empty;
			if(string.IsNullOrEmpty(whereClause))
				sql = "SELECT Count(*) FROM Video as AllRecords  ";
			else
				sql = "SELECT Count(*) FROM Video as AllRecords  where  " +whereClause;
			var rowCount = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, GetSQLParamtersBySearchColumns(searchColumns));
			return rowCount == DBNull.Value ? 0 :(int)rowCount;
		}

		[MOLog(AuditOperations.Create,typeof(Video))]
		public virtual Video InsertVideo(Video entity)
		{

			Video other=new Video();
			other = entity;
			if(entity.IsTransient())
			{
				string sql=@"Insert into Video ( [Url]
				,[StartTime]
				,[EndTime]
				,[CreationDate]
				,[LastUpdateDate]
				,[IsActive]
				,[ChannelId]
				,[VideoStatusId] )
				Values
				( @Url
				, @StartTime
				, @EndTime
				, @CreationDate
				, @LastUpdateDate
				, @IsActive
				, @ChannelId
				, @VideoStatusId );
				Select scope_identity()";
				SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@Url",entity.Url ?? (object)DBNull.Value)
					, new SqlParameter("@StartTime",entity.StartTime ?? (object)DBNull.Value)
					, new SqlParameter("@EndTime",entity.EndTime ?? (object)DBNull.Value)
					, new SqlParameter("@CreationDate",entity.CreationDate ?? (object)DBNull.Value)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate ?? (object)DBNull.Value)
					, new SqlParameter("@IsActive",entity.IsActive ?? (object)DBNull.Value)
					, new SqlParameter("@ChannelId",entity.ChannelId ?? (object)DBNull.Value)
					, new SqlParameter("@VideoStatusId",entity.VideoStatusId ?? (object)DBNull.Value)};
				var identity=SqlHelper.ExecuteScalar(this.ConnectionString,CommandType.Text,sql,parameterArray);
				if(identity==DBNull.Value) throw new DataException("Identity column was null as a result of the insert operation.");
				return GetVideo(Convert.ToInt32(identity));
			}
			return entity;
		}

		[MOLog(AuditOperations.Update,typeof(Video))]
		public virtual Video UpdateVideo(Video entity)
		{

			if (entity.IsTransient()) return entity;
			Video other = GetVideo(entity.VideoId);
			if (entity.Equals(other)) return entity;
			string sql=@"Update Video set  [Url]=@Url
							, [StartTime]=@StartTime
							, [EndTime]=@EndTime
							, [CreationDate]=@CreationDate
							, [LastUpdateDate]=@LastUpdateDate
							, [IsActive]=@IsActive
							, [ChannelId]=@ChannelId
							, [VideoStatusId]=@VideoStatusId 
							 where VideoId=@VideoId";
			SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@Url",entity.Url ?? (object)DBNull.Value)
					, new SqlParameter("@StartTime",entity.StartTime ?? (object)DBNull.Value)
					, new SqlParameter("@EndTime",entity.EndTime ?? (object)DBNull.Value)
					, new SqlParameter("@CreationDate",entity.CreationDate ?? (object)DBNull.Value)
					, new SqlParameter("@LastUpdateDate",entity.LastUpdateDate ?? (object)DBNull.Value)
					, new SqlParameter("@IsActive",entity.IsActive ?? (object)DBNull.Value)
					, new SqlParameter("@ChannelId",entity.ChannelId ?? (object)DBNull.Value)
					, new SqlParameter("@VideoStatusId",entity.VideoStatusId ?? (object)DBNull.Value)
					, new SqlParameter("@VideoId",entity.VideoId)};
			SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,parameterArray);
			return GetVideo(entity.VideoId);
		}

		public virtual bool DeleteVideo(System.Int32 VideoId)
		{

			string sql="delete from Video where VideoId=@VideoId";
			SqlParameter parameter=new SqlParameter("@VideoId",VideoId);
			var identity=SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			return (Convert.ToInt32(identity))==1? true: false;
		}

		[MOLog(AuditOperations.Delete,typeof(Video))]
		public virtual Video DeleteVideo(Video entity)
		{
			this.DeleteVideo(entity.VideoId);
			return entity;
		}


		public virtual Video VideoFromDataRow(DataRow dr)
		{
			if(dr==null) return null;
			Video entity=new Video();
			if (dr.Table.Columns.Contains("VideoId"))
			{
			entity.VideoId = (System.Int32)dr["VideoId"];
			}
			if (dr.Table.Columns.Contains("Url"))
			{
			entity.Url = dr["Url"].ToString();
			}
			if (dr.Table.Columns.Contains("StartTime"))
			{
			entity.StartTime = dr["StartTime"]==DBNull.Value?(System.DateTime?)null:(System.DateTime?)dr["StartTime"];
			}
			if (dr.Table.Columns.Contains("EndTime"))
			{
			entity.EndTime = dr["EndTime"]==DBNull.Value?(System.DateTime?)null:(System.DateTime?)dr["EndTime"];
			}
			if (dr.Table.Columns.Contains("CreationDate"))
			{
			entity.CreationDate = dr["CreationDate"]==DBNull.Value?(System.DateTime?)null:(System.DateTime?)dr["CreationDate"];
			}
			if (dr.Table.Columns.Contains("LastUpdateDate"))
			{
			entity.LastUpdateDate = dr["LastUpdateDate"]==DBNull.Value?(System.DateTime?)null:(System.DateTime?)dr["LastUpdateDate"];
			}
			if (dr.Table.Columns.Contains("IsActive"))
			{
			entity.IsActive = dr["IsActive"]==DBNull.Value?(System.Boolean?)null:(System.Boolean?)dr["IsActive"];
			}
			if (dr.Table.Columns.Contains("ChannelId"))
			{
			entity.ChannelId = dr["ChannelId"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["ChannelId"];
			}
			if (dr.Table.Columns.Contains("VideoStatusId"))
			{
			entity.VideoStatusId = dr["VideoStatusId"]==DBNull.Value?(System.Int32?)null:(System.Int32?)dr["VideoStatusId"];
			}
			return entity;
		}

	}
	
	
}
