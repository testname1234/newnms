﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using TMS.Core.Entities;
using TMS.Core.DataInterfaces;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using TMS.Core.Enums;

namespace TMS.Repository
{

    public partial class VideoRepository : VideoRepositoryBase, IVideoRepository
    {
        public override string ConnectionString
        {
            get
            {
                if (ConfigurationManager.ConnectionStrings["connTMS"] != null)
                    return ConfigurationManager.ConnectionStrings["connTMS"].ToString();
                else return base.ConnectionString;
            }
        }

        public bool Exist(int channelId, DateTime dateTime1, DateTime dateTime2)
        {
            string sql = "select count(*) from [Video] with (nolock)  where channelId=@ChannelId and starttime=@StartTime and endtime=@EndTime ";

            SqlParameter[] parameters = new SqlParameter[] { 
                new SqlParameter("@ChannelId", channelId),
                new SqlParameter("@StartTime", dateTime1),
                new SqlParameter("@EndTime", dateTime2)
            };

            return Convert.ToInt32(SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, parameters)) > 0;
        }

        public List<Video> GetVideos(int channelId, VideoStatuses videoStatus, DateTime? lastUpdateDate)
        {
            string sql = GetVideoSelectClause();
            sql += " from [Video] with (nolock)  where ChannelId = @ChannelId and VideoStatusId = @VideoStatusId and LastUpdateDate > ISNULL(@Date, LastUpdateDate) ";
            SqlParameter[] parameters = new SqlParameter[] { 
                new SqlParameter("@ChannelId", channelId),
                new SqlParameter("@VideoStatusId", (int)videoStatus),
                new SqlParameter("@Date", lastUpdateDate.HasValue ? lastUpdateDate.Value : (object)DBNull.Value)
            };
            DataSet ds = SqlHelper.ExecuteDataset(this.ConnectionString, CommandType.Text, sql, parameters);
            if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
            return CollectionFromDataSet<Video>(ds, VideoFromDataRow);
        }

        public bool UpdateVideoStatus(int videoId, VideoStatuses videoStatus)
        {
            string sql = @"update [Video] set VideoStatusId = @VideoStatusId where [VideoId] = @VideoId";
            SqlParameter[] parameters = new SqlParameter[]{
		        new SqlParameter("@VideoId", videoId),
		        new SqlParameter("@VideoStatusId", (int)videoStatus)
            };

            return SqlHelper.ExecuteNonQuery(this.ConnectionString, CommandType.Text, sql, parameters) > 0;
        }
    }


}
