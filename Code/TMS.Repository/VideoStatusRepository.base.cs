﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using TMS.Core;
using TMS.Core.Entities;
using TMS.Core.DataInterfaces;
using TMS.Core.Extensions;

namespace TMS.Repository
{
		
	public abstract partial class VideoStatusRepositoryBase : Repository, IVideoStatusRepositoryBase 
	{
        
        public VideoStatusRepositoryBase()
        {   
            this.SearchColumns=new Dictionary<string, SearchColumn>();

			this.SearchColumns.Add("VideoStatusId",new SearchColumn(){Name="VideoStatusId",Title="VideoStatusId",SelectClause="VideoStatusId",WhereClause="AllRecords.VideoStatusId",DataType="System.Int32",IsForeignColumn=false,PropertyName="VideoStatusId",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});
			this.SearchColumns.Add("VideoStatusName",new SearchColumn(){Name="VideoStatusName",Title="VideoStatusName",SelectClause="VideoStatusName",WhereClause="AllRecords.VideoStatusName",DataType="System.String",IsForeignColumn=false,PropertyName="VideoStatusName",IsAdvanceSearchColumn = false,IsBasicSearchColumm = false});        
        }
        
		public virtual List<SearchColumn> GetVideoStatusSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                searchColumns.Add(keyValuePair.Value);
            }
            return searchColumns;
        }
		
		
		
        public virtual Dictionary<string, string> GetVideoStatusBasicSearchColumns()
        {
			Dictionary<string, string> columnList = new Dictionary<string, string>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsBasicSearchColumm)
                {
					keyValuePair.Value.Value = string.Empty;
                    columnList.Add(keyValuePair.Key, keyValuePair.Value.Title);
                }
            }
            return columnList;
        }

        public virtual List<SearchColumn> GetVideoStatusAdvanceSearchColumns()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (keyValuePair.Value.IsAdvanceSearchColumn)
                {
					keyValuePair.Value.Value = string.Empty;
					searchColumns.Add(keyValuePair.Value);
                }
            }
            return searchColumns;
        }
        
        
        public virtual string GetVideoStatusSelectClause()
        {
            List<SearchColumn> searchColumns = new List<SearchColumn>();
            string selectQuery=string.Empty;
            foreach (KeyValuePair<string, SearchColumn> keyValuePair in this.SearchColumns)
            {
                if (!keyValuePair.Value.IgnoreForDefaultSelect)
                {
					if (keyValuePair.Value.IsForeignColumn)
                	{
						if(string.IsNullOrEmpty(selectQuery))
						{
							selectQuery = "("+keyValuePair.Value.SelectClause+") as \""+keyValuePair.Key+"\"";
						}
						else
						{
							selectQuery += ",(" + keyValuePair.Value.SelectClause + ") as \"" + keyValuePair.Key + "\"";
						}
                	}
                	else
                	{
                    	if (string.IsNullOrEmpty(selectQuery))
                    	{
                        	selectQuery =  "[VideoStatus].["+keyValuePair.Key+"]";
                    	}
                    	else
                    	{
                        	selectQuery += ",[VideoStatus].["+keyValuePair.Key+"]";
                    	}
                	}
            	}
            }
            return "Select "+selectQuery+" ";
        }
        

		public virtual VideoStatus GetVideoStatus(System.Int32 VideoStatusId,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetVideoStatusSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [VideoStatus] with (nolock)  where VideoStatusId=@VideoStatusId ";
			SqlParameter parameter=new SqlParameter("@VideoStatusId",VideoStatusId);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count != 1) return null;
			return VideoStatusFromDataRow(ds.Tables[0].Rows[0]);
		}

		public  List<VideoStatus> GetVideoStatusByKeyValue(string Key,string Value,Operands operand,string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetVideoStatusSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+= string.Format("from [VideoStatus] with (nolock)  where {0} {1} '{2}' ",Key,operand.ToOperandString(),Value);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<VideoStatus>(ds,VideoStatusFromDataRow);
		}

		public virtual List<VideoStatus> GetAllVideoStatus(string SelectClause=null)
		{

			string sql=string.IsNullOrEmpty(SelectClause)?GetVideoStatusSelectClause():(string.Format("Select {0} ",SelectClause));
			sql+="from [VideoStatus] with (nolock)  ";
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql,null);
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<VideoStatus>(ds, VideoStatusFromDataRow);
		}

		public virtual List<VideoStatus> GetPagedVideoStatus(string orderByClause, int pageSize, int startIndex,out int count, List<SearchColumn> searchColumns,string SelectClause=null)
		{

			string whereClause = base.GetAdvancedWhereClauseByColumn(searchColumns, GetSearchColumns());
               if (!String.IsNullOrEmpty(orderByClause))
               {
                   KeyValuePair<string, string> parsedOrderByClause = base.ParseOrderByClause(orderByClause);
                   orderByClause = base.GetBasicSearchOrderByClauseByColumn(parsedOrderByClause.Key, parsedOrderByClause.Value, this.SearchColumns);
               }

            count=GetVideoStatusCount(whereClause, searchColumns);
			if(count>0)
			{
			if (count < startIndex) startIndex = (count / pageSize) * pageSize;			
			
           	int PageLowerBound = startIndex;
            int PageUpperBound = PageLowerBound + pageSize;
            string sql = @"CREATE TABLE #PageIndex
				            (
				                [IndexId] int IDENTITY (1, 1) NOT NULL,
				                [VideoStatusId] int				   
				            );";

            //Insert into the temp table
            string tempsql = "INSERT INTO #PageIndex ([VideoStatusId])";
            tempsql += " SELECT ";
            if (pageSize > 0) tempsql += "TOP " + PageUpperBound.ToString();
            tempsql += " [VideoStatusId] ";
            tempsql += " FROM [VideoStatus] AllRecords with (NOLOCK)";
            if (!string.IsNullOrEmpty(whereClause) && whereClause.Length > 0) tempsql += " WHERE " + whereClause;
            if (orderByClause.Length > 0) 
			{
				tempsql += " ORDER BY " + orderByClause;
				if( !orderByClause.Contains("VideoStatusId"))
					tempsql += " , (AllRecords.[VideoStatusId])"; 
			}
			else 
			{
				tempsql  += " ORDER BY (AllRecords.[VideoStatusId])"; 
			}           
            
            // Return paged results
            string pagedResultsSql =
                (string.IsNullOrEmpty(SelectClause)? GetVideoStatusSelectClause():(string.Format("Select {0} ",SelectClause)))+@" FROM [VideoStatus] , #PageIndex PageIndex WHERE ";
            pagedResultsSql += " PageIndex.IndexId > " + PageLowerBound.ToString(); 
            pagedResultsSql += @" AND [VideoStatus].[VideoStatusId] = PageIndex.[VideoStatusId] 
				                  ORDER BY PageIndex.IndexId;";
            pagedResultsSql += " drop table #PageIndex";
            sql = sql + tempsql + pagedResultsSql;
			sql = string.Format(sql, whereClause, pageSize, startIndex, orderByClause);
			DataSet ds=SqlHelper.ExecuteDataset(this.ConnectionString,CommandType.Text,sql, GetSQLParamtersBySearchColumns(searchColumns));
			if (ds == null || ds.Tables.Count != 1 || ds.Tables[0].Rows.Count == 0) return null;
			return CollectionFromDataSet<VideoStatus>(ds, VideoStatusFromDataRow);
			}else{ return null;}
		}

		private int GetVideoStatusCount(string whereClause, List<SearchColumn> searchColumns)
		{

			string sql=string.Empty;
			if(string.IsNullOrEmpty(whereClause))
				sql = "SELECT Count(*) FROM VideoStatus as AllRecords  ";
			else
				sql = "SELECT Count(*) FROM VideoStatus as AllRecords  where  " +whereClause;
			var rowCount = SqlHelper.ExecuteScalar(this.ConnectionString, CommandType.Text, sql, GetSQLParamtersBySearchColumns(searchColumns));
			return rowCount == DBNull.Value ? 0 :(int)rowCount;
		}

		[MOLog(AuditOperations.Create,typeof(VideoStatus))]
		public virtual VideoStatus InsertVideoStatus(VideoStatus entity)
		{

			VideoStatus other=new VideoStatus();
			other = entity;
			if(entity.IsTransient())
			{
				string sql=@"Insert into VideoStatus ( [VideoStatusName] )
				Values
				( @VideoStatusName );
				Select scope_identity()";
				SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@VideoStatusName",entity.VideoStatusName ?? (object)DBNull.Value)};
				var identity=SqlHelper.ExecuteScalar(this.ConnectionString,CommandType.Text,sql,parameterArray);
				if(identity==DBNull.Value) throw new DataException("Identity column was null as a result of the insert operation.");
				return GetVideoStatus(Convert.ToInt32(identity));
			}
			return entity;
		}

		[MOLog(AuditOperations.Update,typeof(VideoStatus))]
		public virtual VideoStatus UpdateVideoStatus(VideoStatus entity)
		{

			if (entity.IsTransient()) return entity;
			VideoStatus other = GetVideoStatus(entity.VideoStatusId);
			if (entity.Equals(other)) return entity;
			string sql=@"Update VideoStatus set  [VideoStatusName]=@VideoStatusName 
							 where VideoStatusId=@VideoStatusId";
			SqlParameter[] parameterArray=new SqlParameter[]{
					 new SqlParameter("@VideoStatusName",entity.VideoStatusName ?? (object)DBNull.Value)
					, new SqlParameter("@VideoStatusId",entity.VideoStatusId)};
			SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,parameterArray);
			return GetVideoStatus(entity.VideoStatusId);
		}

		public virtual bool DeleteVideoStatus(System.Int32 VideoStatusId)
		{

			string sql="delete from VideoStatus where VideoStatusId=@VideoStatusId";
			SqlParameter parameter=new SqlParameter("@VideoStatusId",VideoStatusId);
			var identity=SqlHelper.ExecuteNonQuery(this.ConnectionString,CommandType.Text,sql,new SqlParameter[] { parameter });
			return (Convert.ToInt32(identity))==1? true: false;
		}

		[MOLog(AuditOperations.Delete,typeof(VideoStatus))]
		public virtual VideoStatus DeleteVideoStatus(VideoStatus entity)
		{
			this.DeleteVideoStatus(entity.VideoStatusId);
			return entity;
		}


		public virtual VideoStatus VideoStatusFromDataRow(DataRow dr)
		{
			if(dr==null) return null;
			VideoStatus entity=new VideoStatus();
			if (dr.Table.Columns.Contains("VideoStatusId"))
			{
			entity.VideoStatusId = (System.Int32)dr["VideoStatusId"];
			}
			if (dr.Table.Columns.Contains("VideoStatusName"))
			{
			entity.VideoStatusName = dr["VideoStatusName"].ToString();
			}
			return entity;
		}

	}
	
	
}
