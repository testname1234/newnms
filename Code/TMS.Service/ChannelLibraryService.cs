﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using TMS.Core.Entities;
using TMS.Core.DataInterfaces;
using TMS.Core.IService;
using TMS.Core.DataTransfer;
using TMS.Core.DataTransfer.ChannelLibrary;
using Validation;
using System.Linq;
using TMS.Core;
using System.Drawing;
using System.Configuration;
using MMS.LogoMatching;
using OpenSURFcs;

namespace TMS.Service
{
		
	public class ChannelLibraryService : IChannelLibraryService 
	{
		private IChannelLibraryRepository _iChannelLibraryRepository;
        
		public ChannelLibraryService(IChannelLibraryRepository iChannelLibraryRepository)
		{
			this._iChannelLibraryRepository = iChannelLibraryRepository;
		}
        
        public Dictionary<string, string> GetChannelLibraryBasicSearchColumns()
        {
            
            return this._iChannelLibraryRepository.GetChannelLibraryBasicSearchColumns();
           
        }
        
        public List<SearchColumn> GetChannelLibraryAdvanceSearchColumns()
        {
            
            return this._iChannelLibraryRepository.GetChannelLibraryAdvanceSearchColumns();
           
        }
        

		public ChannelLibrary GetChannelLibrary(System.Int32 ChannelLibraryId)
		{
			return _iChannelLibraryRepository.GetChannelLibrary(ChannelLibraryId);
		}

		public ChannelLibrary UpdateChannelLibrary(ChannelLibrary entity)
		{
			return _iChannelLibraryRepository.UpdateChannelLibrary(entity);
		}

		public bool DeleteChannelLibrary(System.Int32 ChannelLibraryId)
		{
			return _iChannelLibraryRepository.DeleteChannelLibrary(ChannelLibraryId);
		}

		public List<ChannelLibrary> GetAllChannelLibrary()
		{
			return _iChannelLibraryRepository.GetAllChannelLibrary();
		}

		public ChannelLibrary InsertChannelLibrary(ChannelLibrary entity)
		{
			 return _iChannelLibraryRepository.InsertChannelLibrary(entity);
		}


        public DataTransfer<GetOutput> Get(string _id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 channellibraryid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out channellibraryid))
            {
				ChannelLibrary channellibrary = _iChannelLibraryRepository.GetChannelLibrary(channellibraryid);
                if(channellibrary!=null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(channellibrary);
                    tranfer.Data=output ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }   
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<ChannelLibrary> channellibrarylist = _iChannelLibraryRepository.GetAllChannelLibrary();
            if (channellibrarylist != null && channellibrarylist.Count>0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(channellibrarylist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
           DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if(errors.Count==0)
            {
                ChannelLibrary channellibrary = new ChannelLibrary();
                PostOutput output = new PostOutput();
                channellibrary.CopyFrom(Input);
                channellibrary = _iChannelLibraryRepository.InsertChannelLibrary(channellibrary);
                output.CopyFrom(channellibrary);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                ChannelLibrary channellibraryinput = new ChannelLibrary();
                ChannelLibrary channellibraryoutput = new ChannelLibrary();
                PutOutput output = new PutOutput();
                channellibraryinput.CopyFrom(Input);
                ChannelLibrary channellibrary = _iChannelLibraryRepository.GetChannelLibrary(channellibraryinput.ChannelLibraryId);
                if (channellibrary!=null)
                {
                    channellibraryoutput = _iChannelLibraryRepository.UpdateChannelLibrary(channellibraryinput);
                    if(channellibraryoutput!=null)
                    {
                        output.CopyFrom(channellibraryoutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    } 
                }
                else                
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

         public DataTransfer<string> Delete(string _id)
         {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 channellibraryid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out channellibraryid))
            {
				 bool IsDeleted = _iChannelLibraryRepository.DeleteChannelLibrary(channellibraryid);
                if(IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data=IsDeleted.ToString().ToLower() ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }

         public void AddToChannelLibrary(string imageUrl, int channelId,int channelLocationId)
         {
             LogoMatching logoMatching = new LogoMatching();


             //using (Bitmap bmp = new Bitmap(imageUrl))
             //{
             //    bmp.Save(ConfigurationManager.AppSettings["videoImagesPath"] + vImage.Guid + ".jpg");

             //    var points = logoMatching.GetImagePoints(bmp, 0.002f);
             //    var channelLibrary = _iChannelLibraryRepository.GetChannelLibraryByChannelLocationId(channelId, channelLocationId);
             //    var interestPoints = channelLibrary.Points;
             //    if (interestPoints != null)
             //    {
             //        var alreadyExisting = logoMatching.GetMatchesOptimized(points, interestPoints);
             //        points.RemoveAll(x => alreadyExisting[0].Contains(x));
             //        if (alreadyExisting[1].Count > AppConstants.MinimumRemovePointsCount && interestPoints.Count > AppConstants.MinimumRemovePointsCount)
             //        {
             //            List<IPoint> removedList = new List<IPoint>();
             //            for (var i = 0; i < alreadyExisting[0].Count; i++)
             //            {
             //                var point = alreadyExisting[0][i];
             //                var point2 = alreadyExisting[1][i];

             //                if (!removedList.Contains(point2))
             //                {
             //                    points.Remove(point);
             //                    removedList.Add(point2);
             //                }
             //                else
             //                {

             //                }
             //            }
             //        }
             //        interestPoints.AddRange(points);
             //        if (!videoLib.InterestPointsCount.HasValue || videoLib.InterestPointsCount == 0)
             //            videoLib.InterestPointsCount = interestPoints.Count;
             //    }
             //    else
             //    {
             //        // if (videoLib.SlotTypeId != (int)SlotTypes.Advertisement && videoLib.SlotTypeId != (int)SlotTypes.Unknown)
             //        //     CleanKeyFrame(videoLib.VideoLibraryId, points, videoLib.ChannelId);
             //        videoLib.InterestPointsCount = interestPoints.Count;
             //    }
             //    // if ((videoLib.SlotTypeId == (int)SlotTypes.Advertisement || videoLib.SlotTypeId == (int)SlotTypes.Unknown) && interestPoints.Count <= 50)
             //    //     videoLib.IsActive = false;
             //    // else videoLib.IsActive = true;
             //    videoLib.IsActive = true;
             //    videoLib.ImageInterestPoints = JSONHelper.GetString(interestPoints);
             //}
         }
	}
	
	
}
