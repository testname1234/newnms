﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMS.Core;
using TMS.Core.DataInterfaces;
using TMS.Core.DataTransfer;
using TMS.Core.DataTransfer.Channel;
using TMS.Core.Entities;
using TMS.Core.IService;
using Validation;

namespace TMS.Service
{
    public class ChannelService : IChannelService
    {
        private IChannelRepository _iChannelRepository;

        public ChannelService(IChannelRepository iChannelRepository)
        {
            this._iChannelRepository = iChannelRepository;
        }

        public Dictionary<string, string> GetChannelBasicSearchColumns()
        {
            return this._iChannelRepository.GetChannelBasicSearchColumns();
        }

        public List<SearchColumn> GetChannelAdvanceSearchColumns()
        {
            return this._iChannelRepository.GetChannelAdvanceSearchColumns();
        }

        public Channel GetChannel(System.Int32 ChannelId)
        {
            return _iChannelRepository.GetChannel(ChannelId);
        }

        public Channel UpdateChannel(Channel entity)
        {
            return _iChannelRepository.UpdateChannel(entity);
        }

        public bool DeleteChannel(System.Int32 ChannelId)
        {
            return _iChannelRepository.DeleteChannel(ChannelId);
        }

        public List<Channel> GetAllChannel()
        {
            return _iChannelRepository.GetAllChannel();
        }

        public Channel InsertChannel(Channel entity)
        {
            return _iChannelRepository.InsertChannel(entity);
        }

        public DataTransfer<GetOutput> Get(string _id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 channelid = 0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id, out channelid))
            {
                Channel channel = _iChannelRepository.GetChannel(channelid);
                if (channel != null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(channel);
                    tranfer.Data = output;
                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }

        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<Channel> channellist = _iChannelRepository.GetAllChannel();
            if (channellist != null && channellist.Count > 0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(channellist);
                tranfer.Data = outputlist;
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }

        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
            DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                Channel channel = new Channel();
                PostOutput output = new PostOutput();
                channel.CopyFrom(Input);
                channel = _iChannelRepository.InsertChannel(channel);
                output.CopyFrom(channel);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                Channel channelinput = new Channel();
                Channel channeloutput = new Channel();
                PutOutput output = new PutOutput();
                channelinput.CopyFrom(Input);
                Channel channel = _iChannelRepository.GetChannel(channelinput.ChannelId);
                if (channel != null)
                {
                    channeloutput = _iChannelRepository.UpdateChannel(channelinput);
                    if (channeloutput != null)
                    {
                        output.CopyFrom(channeloutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    }
                }
                else
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<string> Delete(string _id)
        {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 channelid = 0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id, out channelid))
            {
                bool IsDeleted = _iChannelRepository.DeleteChannel(channelid);
                if (IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data = IsDeleted.ToString().ToLower();
                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }

        public List<Channel> GetChannels(System.DateTime lastUpdateDate)
        {
            return _iChannelRepository.GetChannels(lastUpdateDate);
        }

        public System.DateTime GetMaxChannelDate(int channelId)
        {
            return _iChannelRepository.GetMaxChannelDate(channelId);
        }

        public List<Channel> GetChannelByUserId(int userId)
        {
            return _iChannelRepository.GetChannelByUserId(userId);
        }
    }
}