﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using TMS.Core.Entities;
using TMS.Core.DataInterfaces;
using TMS.Core.IService;
using TMS.Core.DataTransfer;
using TMS.Core.DataTransfer.ChannelTickerLocation;
using Validation;
using System.Linq;
using TMS.Core;

namespace TMS.Service
{
		
	public class ChannelTickerLocationService : IChannelTickerLocationService 
	{
		private IChannelTickerLocationRepository _iChannelTickerLocationRepository;
        
		public ChannelTickerLocationService(IChannelTickerLocationRepository iChannelTickerLocationRepository)
		{
			this._iChannelTickerLocationRepository = iChannelTickerLocationRepository;
		}
        
        public Dictionary<string, string> GetChannelTickerLocationBasicSearchColumns()
        {
            
            return this._iChannelTickerLocationRepository.GetChannelTickerLocationBasicSearchColumns();
           
        }
        
        public List<SearchColumn> GetChannelTickerLocationAdvanceSearchColumns()
        {
            
            return this._iChannelTickerLocationRepository.GetChannelTickerLocationAdvanceSearchColumns();
           
        }
        

		public ChannelTickerLocation GetChannelTickerLocation(System.Int32 ChannelTickerLocationId)
		{
			return _iChannelTickerLocationRepository.GetChannelTickerLocation(ChannelTickerLocationId);
		}

		public ChannelTickerLocation UpdateChannelTickerLocation(ChannelTickerLocation entity)
		{
			return _iChannelTickerLocationRepository.UpdateChannelTickerLocation(entity);
		}

		public bool DeleteChannelTickerLocation(System.Int32 ChannelTickerLocationId)
		{
			return _iChannelTickerLocationRepository.DeleteChannelTickerLocation(ChannelTickerLocationId);
		}

		public List<ChannelTickerLocation> GetAllChannelTickerLocation()
		{
			return _iChannelTickerLocationRepository.GetAllChannelTickerLocation();
		}

		public ChannelTickerLocation InsertChannelTickerLocation(ChannelTickerLocation entity)
		{
			 return _iChannelTickerLocationRepository.InsertChannelTickerLocation(entity);
		}


        public DataTransfer<GetOutput> Get(string _id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 channeltickerlocationid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out channeltickerlocationid))
            {
				ChannelTickerLocation channeltickerlocation = _iChannelTickerLocationRepository.GetChannelTickerLocation(channeltickerlocationid);
                if(channeltickerlocation!=null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(channeltickerlocation);
                    tranfer.Data=output ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }   
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<ChannelTickerLocation> channeltickerlocationlist = _iChannelTickerLocationRepository.GetAllChannelTickerLocation();
            if (channeltickerlocationlist != null && channeltickerlocationlist.Count>0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(channeltickerlocationlist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
           DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if(errors.Count==0)
            {
                ChannelTickerLocation channeltickerlocation = new ChannelTickerLocation();
                PostOutput output = new PostOutput();
                channeltickerlocation.CopyFrom(Input);
                channeltickerlocation = _iChannelTickerLocationRepository.InsertChannelTickerLocation(channeltickerlocation);
                output.CopyFrom(channeltickerlocation);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                ChannelTickerLocation channeltickerlocationinput = new ChannelTickerLocation();
                ChannelTickerLocation channeltickerlocationoutput = new ChannelTickerLocation();
                PutOutput output = new PutOutput();
                channeltickerlocationinput.CopyFrom(Input);
                ChannelTickerLocation channeltickerlocation = _iChannelTickerLocationRepository.GetChannelTickerLocation(channeltickerlocationinput.ChannelTickerLocationId);
                if (channeltickerlocation!=null)
                {
                    channeltickerlocationoutput = _iChannelTickerLocationRepository.UpdateChannelTickerLocation(channeltickerlocationinput);
                    if(channeltickerlocationoutput!=null)
                    {
                        output.CopyFrom(channeltickerlocationoutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    } 
                }
                else                
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

         public DataTransfer<string> Delete(string _id)
         {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 channeltickerlocationid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out channeltickerlocationid))
            {
				 bool IsDeleted = _iChannelTickerLocationRepository.DeleteChannelTickerLocation(channeltickerlocationid);
                if(IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data=IsDeleted.ToString().ToLower() ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }
	}
	
	
}
