﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using TMS.Core.Entities;
using TMS.Core.DataInterfaces;
using TMS.Core.IService;
using TMS.Core.DataTransfer;
using TMS.Core.DataTransfer.ChannelUserMapping;
using Validation;
using System.Linq;

namespace TMS.Core.Service
{
		
	public class ChannelUserMappingService : IChannelUserMappingService 
	{
		private IChannelUserMappingRepository _iChannelUserMappingRepository;
        
		public ChannelUserMappingService(IChannelUserMappingRepository iChannelUserMappingRepository)
		{
			this._iChannelUserMappingRepository = iChannelUserMappingRepository;
		}
        
        public Dictionary<string, string> GetChannelUserMappingBasicSearchColumns()
        {
            
            return this._iChannelUserMappingRepository.GetChannelUserMappingBasicSearchColumns();
           
        }
        
        public List<SearchColumn> GetChannelUserMappingAdvanceSearchColumns()
        {
            
            return this._iChannelUserMappingRepository.GetChannelUserMappingAdvanceSearchColumns();
           
        }
        

		public virtual List<ChannelUserMapping> GetChannelUserMappingByChannelId(System.Int32? ChannelId)
		{
			return _iChannelUserMappingRepository.GetChannelUserMappingByChannelId(ChannelId);
		}

		public ChannelUserMapping GetChannelUserMapping(System.Int32 ChannelUserMappingId)
		{
			return _iChannelUserMappingRepository.GetChannelUserMapping(ChannelUserMappingId);
		}

		public ChannelUserMapping UpdateChannelUserMapping(ChannelUserMapping entity)
		{
			return _iChannelUserMappingRepository.UpdateChannelUserMapping(entity);
		}

		public bool DeleteChannelUserMapping(System.Int32 ChannelUserMappingId)
		{
			return _iChannelUserMappingRepository.DeleteChannelUserMapping(ChannelUserMappingId);
		}

		public List<ChannelUserMapping> GetAllChannelUserMapping()
		{
			return _iChannelUserMappingRepository.GetAllChannelUserMapping();
		}

		public ChannelUserMapping InsertChannelUserMapping(ChannelUserMapping entity)
		{
			 return _iChannelUserMappingRepository.InsertChannelUserMapping(entity);
		}


        public DataTransfer<GetOutput> Get(string _id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 channelusermappingid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out channelusermappingid))
            {
				ChannelUserMapping channelusermapping = _iChannelUserMappingRepository.GetChannelUserMapping(channelusermappingid);
                if(channelusermapping!=null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(channelusermapping);
                    tranfer.Data=output ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }   
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<ChannelUserMapping> channelusermappinglist = _iChannelUserMappingRepository.GetAllChannelUserMapping();
            if (channelusermappinglist != null && channelusermappinglist.Count>0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(channelusermappinglist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
           DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if(errors.Count==0)
            {
                ChannelUserMapping channelusermapping = new ChannelUserMapping();
                PostOutput output = new PostOutput();
                channelusermapping.CopyFrom(Input);
                channelusermapping = _iChannelUserMappingRepository.InsertChannelUserMapping(channelusermapping);
                output.CopyFrom(channelusermapping);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                ChannelUserMapping channelusermappinginput = new ChannelUserMapping();
                ChannelUserMapping channelusermappingoutput = new ChannelUserMapping();
                PutOutput output = new PutOutput();
                channelusermappinginput.CopyFrom(Input);
                ChannelUserMapping channelusermapping = _iChannelUserMappingRepository.GetChannelUserMapping(channelusermappinginput.ChannelUserMappingId);
                if (channelusermapping!=null)
                {
                    channelusermappingoutput = _iChannelUserMappingRepository.UpdateChannelUserMapping(channelusermappinginput);
                    if(channelusermappingoutput!=null)
                    {
                        output.CopyFrom(channelusermappingoutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    } 
                }
                else                
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

         public DataTransfer<string> Delete(string _id)
         {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 channelusermappingid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out channelusermappingid))
            {
				 bool IsDeleted = _iChannelUserMappingRepository.DeleteChannelUserMapping(channelusermappingid);
                if(IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data=IsDeleted.ToString().ToLower() ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }
	}
	
	
}
