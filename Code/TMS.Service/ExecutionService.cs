﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using TMS.Core.Entities;
using TMS.Core.DataInterfaces;
using TMS.Core.IService;
using TMS.Core.DataTransfer;
using TMS.Core.DataTransfer.Execution;
using TMS.Core;
using Validation;
using System.Linq;

namespace TMS.Service
{
		
	public class ExecutionService : IExecutionService 
	{
		private IExecutionRepository _iExecutionRepository;
        
		public ExecutionService(IExecutionRepository iExecutionRepository)
		{
			this._iExecutionRepository = iExecutionRepository;
		}
        
        public Dictionary<string, string> GetExecutionBasicSearchColumns()
        {
            
            return this._iExecutionRepository.GetExecutionBasicSearchColumns();
           
        }
        
        public List<SearchColumn> GetExecutionAdvanceSearchColumns()
        {
            
            return this._iExecutionRepository.GetExecutionAdvanceSearchColumns();
           
        }
        

		public Execution GetExecution(System.Int32 ExecutionId)
		{
			return _iExecutionRepository.GetExecution(ExecutionId);
		}

		public Execution UpdateExecution(Execution entity)
		{
			return _iExecutionRepository.UpdateExecution(entity);
		}

		public bool DeleteExecution(System.Int32 ExecutionId)
		{
			return _iExecutionRepository.DeleteExecution(ExecutionId);
		}

		public List<Execution> GetAllExecution()
		{
			return _iExecutionRepository.GetAllExecution();
		}

		public Execution InsertExecution(Execution entity)
		{
			 return _iExecutionRepository.InsertExecution(entity);
		}


        public DataTransfer<GetOutput> Get(string _id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 executionid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out executionid))
            {
				Execution execution = _iExecutionRepository.GetExecution(executionid);
                if(execution!=null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(execution);
                    tranfer.Data=output ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }   
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<Execution> executionlist = _iExecutionRepository.GetAllExecution();
            if (executionlist != null && executionlist.Count>0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(executionlist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
           DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if(errors.Count==0)
            {
                Execution execution = new Execution();
                PostOutput output = new PostOutput();
                execution.CopyFrom(Input);
                execution = _iExecutionRepository.InsertExecution(execution);
                output.CopyFrom(execution);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                Execution executioninput = new Execution();
                Execution executionoutput = new Execution();
                PutOutput output = new PutOutput();
                executioninput.CopyFrom(Input);
                Execution execution = _iExecutionRepository.GetExecution(executioninput.ExecutionId);
                if (execution!=null)
                {
                    executionoutput = _iExecutionRepository.UpdateExecution(executioninput);
                    if(executionoutput!=null)
                    {
                        output.CopyFrom(executionoutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    } 
                }
                else                
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

         public DataTransfer<string> Delete(string _id)
         {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 executionid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out executionid))
            {
				 bool IsDeleted = _iExecutionRepository.DeleteExecution(executionid);
                if(IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data=IsDeleted.ToString().ToLower() ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }
	}
	
	
}
