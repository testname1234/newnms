﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using TMS.Core.Entities;
using TMS.Core.DataInterfaces;
using TMS.Core.IService;
using TMS.Core.DataTransfer;
using TMS.Core.DataTransfer.TickerCategory;
using Validation;
using System.Linq;
using TMS.Core;

namespace TMS.Service
{

    public class TickerCategoryService : ITickerCategoryService
    {
        private ITickerCategoryRepository _iTickerCategoryRepository;

        public TickerCategoryService(ITickerCategoryRepository iTickerCategoryRepository)
        {
            this._iTickerCategoryRepository = iTickerCategoryRepository;
        }

        public Dictionary<string, string> GetTickerCategoryBasicSearchColumns()
        {

            return this._iTickerCategoryRepository.GetTickerCategoryBasicSearchColumns();

        }

        public List<SearchColumn> GetTickerCategoryAdvanceSearchColumns()
        {

            return this._iTickerCategoryRepository.GetTickerCategoryAdvanceSearchColumns();

        }


        public TickerCategory GetTickerCategory(System.Int32 TickerCategoryId)
        {
            return _iTickerCategoryRepository.GetTickerCategory(TickerCategoryId);
        }

        public TickerCategory UpdateTickerCategory(TickerCategory entity)
        {
            return _iTickerCategoryRepository.UpdateTickerCategory(entity);
        }

        public bool DeleteTickerCategory(System.Int32 TickerCategoryId)
        {
            return _iTickerCategoryRepository.DeleteTickerCategory(TickerCategoryId);
        }

        public List<TickerCategory> GetAllTickerCategory()
        {
            return _iTickerCategoryRepository.GetAllTickerCategory();
        }

        public TickerCategory InsertTickerCategory(TickerCategory entity)
        {
            return _iTickerCategoryRepository.InsertTickerCategory(entity);
        }


        public DataTransfer<GetOutput> Get(string _id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 tickercategoryid = 0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id, out tickercategoryid))
            {
                TickerCategory tickercategory = _iTickerCategoryRepository.GetTickerCategory(tickercategoryid);
                if (tickercategory != null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(tickercategory);
                    tranfer.Data = output;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<TickerCategory> tickercategorylist = _iTickerCategoryRepository.GetAllTickerCategory();
            if (tickercategorylist != null && tickercategorylist.Count > 0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(tickercategorylist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
            DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                TickerCategory tickercategory = new TickerCategory();
                PostOutput output = new PostOutput();
                tickercategory.CopyFrom(Input);
                tickercategory = _iTickerCategoryRepository.InsertTickerCategory(tickercategory);
                output.CopyFrom(tickercategory);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                TickerCategory tickercategoryinput = new TickerCategory();
                TickerCategory tickercategoryoutput = new TickerCategory();
                PutOutput output = new PutOutput();
                tickercategoryinput.CopyFrom(Input);
                TickerCategory tickercategory = _iTickerCategoryRepository.GetTickerCategory(tickercategoryinput.TickerCategoryId);
                if (tickercategory != null)
                {
                    tickercategoryoutput = _iTickerCategoryRepository.UpdateTickerCategory(tickercategoryinput);
                    if (tickercategoryoutput != null)
                    {
                        output.CopyFrom(tickercategoryoutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    }
                }
                else
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<string> Delete(string _id)
        {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 tickercategoryid = 0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id, out tickercategoryid))
            {
                bool IsDeleted = _iTickerCategoryRepository.DeleteTickerCategory(tickercategoryid);
                if (IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data = IsDeleted.ToString().ToLower();

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }

        public List<TickerCategory> GetTickerCategories(DateTime lastUpdateDate)
        {
            return _iTickerCategoryRepository.GetTickerCategories(lastUpdateDate);
        }
    }
}
