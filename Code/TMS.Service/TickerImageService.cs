﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using TMS.Core.Entities;
using TMS.Core.DataInterfaces;
using TMS.Core.IService;
using TMS.Core.DataTransfer;
using TMS.Core.DataTransfer.TickerImage;
using Validation;
using System.Linq;
using TMS.Core;
using TMS.Core.Enums;

namespace TMS.Service
{
		
	public class TickerImageService : ITickerImageService 
	{
		private ITickerImageRepository _iTickerImageRepository;
        
		public TickerImageService(ITickerImageRepository iTickerImageRepository)
		{
			this._iTickerImageRepository = iTickerImageRepository;
		}
        
        public Dictionary<string, string> GetTickerImageBasicSearchColumns()
        {
            
            return this._iTickerImageRepository.GetTickerImageBasicSearchColumns();
           
        }
        
        public List<SearchColumn> GetTickerImageAdvanceSearchColumns()
        {
            
            return this._iTickerImageRepository.GetTickerImageAdvanceSearchColumns();
           
        }
        

		public TickerImage GetTickerImage(System.Int32 TickerImageId)
		{
			return _iTickerImageRepository.GetTickerImage(TickerImageId);
		}

		public TickerImage UpdateTickerImage(TickerImage entity)
		{
			return _iTickerImageRepository.UpdateTickerImage(entity);
		}

		public bool DeleteTickerImage(System.Int32 TickerImageId)
		{
			return _iTickerImageRepository.DeleteTickerImage(TickerImageId);
		}

		public List<TickerImage> GetAllTickerImage()
		{
			return _iTickerImageRepository.GetAllTickerImage();
		}

        public List<TickerImage> GetTickerImages(int? channelId, TickerImageStatuses tickerImageStatus, int pageIndex, int pageCount)
        {
            return _iTickerImageRepository.GetTickerImages(channelId, tickerImageStatus, pageIndex, pageCount);
        }

		public TickerImage InsertTickerImage(TickerImage entity)
		{
			 return _iTickerImageRepository.InsertTickerImage(entity);
		}

        public bool UpdateTickerImageStatus(int tickerImageId, int statusId)
        {
            return _iTickerImageRepository.UpdateTickerImageStatus(tickerImageId, statusId);
        }


        public DataTransfer<GetOutput> Get(string _id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 tickerimageid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out tickerimageid))
            {
				TickerImage tickerimage = _iTickerImageRepository.GetTickerImage(tickerimageid);
                if(tickerimage!=null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(tickerimage);
                    tranfer.Data=output ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }   
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<TickerImage> tickerimagelist = _iTickerImageRepository.GetAllTickerImage();
            if (tickerimagelist != null && tickerimagelist.Count>0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(tickerimagelist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
           DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if(errors.Count==0)
            {
                TickerImage tickerimage = new TickerImage();
                PostOutput output = new PostOutput();
                tickerimage.CopyFrom(Input);
                tickerimage = _iTickerImageRepository.InsertTickerImage(tickerimage);
                output.CopyFrom(tickerimage);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                TickerImage tickerimageinput = new TickerImage();
                TickerImage tickerimageoutput = new TickerImage();
                PutOutput output = new PutOutput();
                tickerimageinput.CopyFrom(Input);
                TickerImage tickerimage = _iTickerImageRepository.GetTickerImage(tickerimageinput.TickerImageId);
                if (tickerimage!=null)
                {
                    tickerimageoutput = _iTickerImageRepository.UpdateTickerImage(tickerimageinput);
                    if(tickerimageoutput!=null)
                    {
                        output.CopyFrom(tickerimageoutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    } 
                }
                else                
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

         public DataTransfer<string> Delete(string _id)
         {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 tickerimageid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out tickerimageid))
            {
				 bool IsDeleted = _iTickerImageRepository.DeleteTickerImage(tickerimageid);
                if(IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data=IsDeleted.ToString().ToLower() ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }
	}
	
	
}
