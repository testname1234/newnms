﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMS.Core;
using TMS.Core.DataInterfaces;
using TMS.Core.DataTransfer;
using TMS.Core.DataTransfer.TickerLine;
using TMS.Core.Entities;
using TMS.Core.IService;
using Validation;

namespace TMS.Service
{
    public class TickerLineService : ITickerLineService
    {
        private ITickerLineRepository _iTickerLineRepository;

        public TickerLineService(ITickerLineRepository iTickerLineRepository)
        {
            this._iTickerLineRepository = iTickerLineRepository;
        }

        public Dictionary<string, string> GetTickerLineBasicSearchColumns()
        {
            return this._iTickerLineRepository.GetTickerLineBasicSearchColumns();
        }

        public List<SearchColumn> GetTickerLineAdvanceSearchColumns()
        {
            return this._iTickerLineRepository.GetTickerLineAdvanceSearchColumns();
        }

        public virtual List<TickerLine> GetTickerLineByTickerId(System.Int32? TickerId)
        {
            return _iTickerLineRepository.GetTickerLineByTickerId(TickerId);
        }

        public TickerLine GetTickerLine(System.Int32 TickerLineId)
        {
            return _iTickerLineRepository.GetTickerLine(TickerLineId);
        }

        public TickerLine UpdateTickerLine(TickerLine entity)
        {
            return _iTickerLineRepository.UpdateTickerLine(entity);
        }

        public bool DeleteTickerLine(System.Int32 TickerLineId)
        {
            return _iTickerLineRepository.DeleteTickerLine(TickerLineId);
        }

        public List<TickerLine> GetAllTickerLine()
        {
            return _iTickerLineRepository.GetAllTickerLine();
        }

        public TickerLine InsertTickerLine(TickerLine entity)
        {
            return _iTickerLineRepository.InsertTickerLine(entity);
        }

        public DataTransfer<GetOutput> Get(string _id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 tickerlineid = 0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id, out tickerlineid))
            {
                TickerLine tickerline = _iTickerLineRepository.GetTickerLine(tickerlineid);
                if (tickerline != null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(tickerline);
                    tranfer.Data = output;
                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }

        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<TickerLine> tickerlinelist = _iTickerLineRepository.GetAllTickerLine();
            if (tickerlinelist != null && tickerlinelist.Count > 0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(tickerlinelist);
                tranfer.Data = outputlist;
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }

        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
            DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                TickerLine tickerline = new TickerLine();
                PostOutput output = new PostOutput();
                tickerline.CopyFrom(Input);
                tickerline = _iTickerLineRepository.InsertTickerLine(tickerline);
                output.CopyFrom(tickerline);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                TickerLine tickerlineinput = new TickerLine();
                TickerLine tickerlineoutput = new TickerLine();
                PutOutput output = new PutOutput();
                tickerlineinput.CopyFrom(Input);
                TickerLine tickerline = _iTickerLineRepository.GetTickerLine(tickerlineinput.TickerLineId);
                if (tickerline != null)
                {
                    tickerlineoutput = _iTickerLineRepository.UpdateTickerLine(tickerlineinput);
                    if (tickerlineoutput != null)
                    {
                        output.CopyFrom(tickerlineoutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    }
                }
                else
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<string> Delete(string _id)
        {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 tickerlineid = 0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id, out tickerlineid))
            {
                bool IsDeleted = _iTickerLineRepository.DeleteTickerLine(tickerlineid);
                if (IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data = IsDeleted.ToString().ToLower();
                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }

        public List<TickerLine> GetTickerLines(int tickerId, string keyword)
        {
            return _iTickerLineRepository.GetTickerLines(tickerId, keyword);
        }
    }
}