﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMS.Core;
using TMS.Core.DataInterfaces;
using TMS.Core.DataTransfer;
using TMS.Core.DataTransfer.Ticker;
using TMS.Core.Entities;
using TMS.Core.IService;
using Validation;

namespace TMS.Service
{
    public class TickerService : ITickerService
    {
        private ITickerRepository _iTickerRepository;

        public TickerService(ITickerRepository iTickerRepository)
        {
            this._iTickerRepository = iTickerRepository;
        }

        public Dictionary<string, string> GetTickerBasicSearchColumns()
        {
            return this._iTickerRepository.GetTickerBasicSearchColumns();
        }

        public List<SearchColumn> GetTickerAdvanceSearchColumns()
        {
            return this._iTickerRepository.GetTickerAdvanceSearchColumns();
        }

        public Ticker GetTicker(System.Int32 TickerId)
        {
            return _iTickerRepository.GetTicker(TickerId);
        }

        public Ticker UpdateTicker(Ticker entity)
        {
            return _iTickerRepository.UpdateTicker(entity);
        }

        public bool DeleteTicker(System.Int32 TickerId)
        {
            return _iTickerRepository.DeleteTicker(TickerId);
        }

        public List<Ticker> GetAllTicker()
        {
            return _iTickerRepository.GetAllTicker();
        }

        public Ticker InsertTicker(Ticker entity)
        {
            return _iTickerRepository.InsertTicker(entity);
        }

        public DataTransfer<GetOutput> Get(string _id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 tickerid = 0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id, out tickerid))
            {
                Ticker ticker = _iTickerRepository.GetTicker(tickerid);
                if (ticker != null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(ticker);
                    tranfer.Data = output;
                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }

        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<Ticker> tickerlist = _iTickerRepository.GetAllTicker();
            if (tickerlist != null && tickerlist.Count > 0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(tickerlist);
                tranfer.Data = outputlist;
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }

        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
            DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                Ticker ticker = new Ticker();
                PostOutput output = new PostOutput();
                ticker.CopyFrom(Input);
                ticker = _iTickerRepository.InsertTicker(ticker);
                output.CopyFrom(ticker);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                Ticker tickerinput = new Ticker();
                Ticker tickeroutput = new Ticker();
                PutOutput output = new PutOutput();
                tickerinput.CopyFrom(Input);
                Ticker ticker = _iTickerRepository.GetTicker(tickerinput.TickerId);
                if (ticker != null)
                {
                    tickeroutput = _iTickerRepository.UpdateTicker(tickerinput);
                    if (tickeroutput != null)
                    {
                        output.CopyFrom(tickeroutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    }
                }
                else
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<string> Delete(string _id)
        {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 tickerid = 0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id, out tickerid))
            {
                bool IsDeleted = _iTickerRepository.DeleteTicker(tickerid);
                if (IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data = IsDeleted.ToString().ToLower();
                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }

        public List<Ticker> GetTickers(string keyword)
        {
            return _iTickerRepository.GetTickers(keyword);
        }
    }
}