﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using TMS.Core.Entities;
using TMS.Core.DataInterfaces;
using TMS.Core.IService;
using TMS.Core.DataTransfer;
using TMS.Core.DataTransfer.TickerType;
using Validation;
using System.Linq;
using TMS.Core;

namespace TMS.Service
{
		
	public class TickerTypeService : ITickerTypeService 
	{
		private ITickerTypeRepository _iTickerTypeRepository;
        
		public TickerTypeService(ITickerTypeRepository iTickerTypeRepository)
		{
			this._iTickerTypeRepository = iTickerTypeRepository;
		}
        
        public Dictionary<string, string> GetTickerTypeBasicSearchColumns()
        {
            
            return this._iTickerTypeRepository.GetTickerTypeBasicSearchColumns();
           
        }
        
        public List<SearchColumn> GetTickerTypeAdvanceSearchColumns()
        {
            
            return this._iTickerTypeRepository.GetTickerTypeAdvanceSearchColumns();
           
        }
        

		public TickerType GetTickerType(System.Int32 TickerTypeId)
		{
			return _iTickerTypeRepository.GetTickerType(TickerTypeId);
		}

		public TickerType UpdateTickerType(TickerType entity)
		{
			return _iTickerTypeRepository.UpdateTickerType(entity);
		}

		public bool DeleteTickerType(System.Int32 TickerTypeId)
		{
			return _iTickerTypeRepository.DeleteTickerType(TickerTypeId);
		}

		public List<TickerType> GetAllTickerType()
		{
			return _iTickerTypeRepository.GetAllTickerType();
		}

		public TickerType InsertTickerType(TickerType entity)
		{
			 return _iTickerTypeRepository.InsertTickerType(entity);
		}


        public DataTransfer<GetOutput> Get(string _id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 tickertypeid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out tickertypeid))
            {
				TickerType tickertype = _iTickerTypeRepository.GetTickerType(tickertypeid);
                if(tickertype!=null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(tickertype);
                    tranfer.Data=output ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }   
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<TickerType> tickertypelist = _iTickerTypeRepository.GetAllTickerType();
            if (tickertypelist != null && tickertypelist.Count>0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(tickertypelist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
           DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if(errors.Count==0)
            {
                TickerType tickertype = new TickerType();
                PostOutput output = new PostOutput();
                tickertype.CopyFrom(Input);
                tickertype = _iTickerTypeRepository.InsertTickerType(tickertype);
                output.CopyFrom(tickertype);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                TickerType tickertypeinput = new TickerType();
                TickerType tickertypeoutput = new TickerType();
                PutOutput output = new PutOutput();
                tickertypeinput.CopyFrom(Input);
                TickerType tickertype = _iTickerTypeRepository.GetTickerType(tickertypeinput.TickerTypeId);
                if (tickertype!=null)
                {
                    tickertypeoutput = _iTickerTypeRepository.UpdateTickerType(tickertypeinput);
                    if(tickertypeoutput!=null)
                    {
                        output.CopyFrom(tickertypeoutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    } 
                }
                else                
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

         public DataTransfer<string> Delete(string _id)
         {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 tickertypeid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out tickertypeid))
            {
				 bool IsDeleted = _iTickerTypeRepository.DeleteTickerType(tickertypeid);
                if(IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data=IsDeleted.ToString().ToLower() ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }
	}
	
	
}
