﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using TMS.Core.Entities;
using TMS.Core.DataInterfaces;
using TMS.Core.IService;
using TMS.Core.DataTransfer;
using TMS.Core.DataTransfer.Video;
using Validation;
using System.Linq;
using TMS.Core;
using TMS.Core.Enums;

namespace TMS.Service
{
		
	public class VideoService : IVideoService 
	{
		private IVideoRepository _iVideoRepository;
        
		public VideoService(IVideoRepository iVideoRepository)
		{
			this._iVideoRepository = iVideoRepository;
		}
        
        public Dictionary<string, string> GetVideoBasicSearchColumns()
        {
            
            return this._iVideoRepository.GetVideoBasicSearchColumns();
           
        }
        
        public List<SearchColumn> GetVideoAdvanceSearchColumns()
        {
            
            return this._iVideoRepository.GetVideoAdvanceSearchColumns();
           
        }
        

		public Video GetVideo(System.Int32 VideoId)
		{
			return _iVideoRepository.GetVideo(VideoId);
		}

		public Video UpdateVideo(Video entity)
		{
			return _iVideoRepository.UpdateVideo(entity);
		}

		public bool DeleteVideo(System.Int32 VideoId)
		{
			return _iVideoRepository.DeleteVideo(VideoId);
		}

		public List<Video> GetAllVideo()
		{
			return _iVideoRepository.GetAllVideo();
		}

		public Video InsertVideo(Video entity)
		{
			 return _iVideoRepository.InsertVideo(entity);
		}

        public List<Video> GetVideos(int channelId, VideoStatuses videoStatus, DateTime? lastUpdateDate)
        {
            return _iVideoRepository.GetVideos(channelId, videoStatus, lastUpdateDate);
        }

        public bool UpdateVideoStatus(int videoId, VideoStatuses videoStatus)
        {
            return _iVideoRepository.UpdateVideoStatus(videoId, videoStatus);
        }


        public DataTransfer<GetOutput> Get(string _id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 videoid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out videoid))
            {
				Video video = _iVideoRepository.GetVideo(videoid);
                if(video!=null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(video);
                    tranfer.Data=output ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }   
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<Video> videolist = _iVideoRepository.GetAllVideo();
            if (videolist != null && videolist.Count>0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(videolist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
           DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if(errors.Count==0)
            {
                Video video = new Video();
                PostOutput output = new PostOutput();
                video.CopyFrom(Input);
                video = _iVideoRepository.InsertVideo(video);
                output.CopyFrom(video);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                Video videoinput = new Video();
                Video videooutput = new Video();
                PutOutput output = new PutOutput();
                videoinput.CopyFrom(Input);
                Video video = _iVideoRepository.GetVideo(videoinput.VideoId);
                if (video!=null)
                {
                    videooutput = _iVideoRepository.UpdateVideo(videoinput);
                    if(videooutput!=null)
                    {
                        output.CopyFrom(videooutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    } 
                }
                else                
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

         public DataTransfer<string> Delete(string _id)
         {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 videoid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out videoid))
            {
				 bool IsDeleted = _iVideoRepository.DeleteVideo(videoid);
                if(IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data=IsDeleted.ToString().ToLower() ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }


         public bool Exist(int channelId, DateTime dateTime1, DateTime dateTime2)
         {
             return _iVideoRepository.Exist(channelId, dateTime1, dateTime2);
         }
    }
	
	
}
