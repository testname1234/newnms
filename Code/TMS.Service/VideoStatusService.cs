﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using TMS.Core.Entities;
using TMS.Core.DataInterfaces;
using TMS.Core.IService;
using TMS.Core.DataTransfer;
using TMS.Core.DataTransfer.VideoStatus;
using Validation;
using System.Linq;

namespace TMS.Core.Service
{
		
	public class VideoStatusService : IVideoStatusService 
	{
		private IVideoStatusRepository _iVideoStatusRepository;
        
		public VideoStatusService(IVideoStatusRepository iVideoStatusRepository)
		{
			this._iVideoStatusRepository = iVideoStatusRepository;
		}
        
        public Dictionary<string, string> GetVideoStatusBasicSearchColumns()
        {
            
            return this._iVideoStatusRepository.GetVideoStatusBasicSearchColumns();
           
        }
        
        public List<SearchColumn> GetVideoStatusAdvanceSearchColumns()
        {
            
            return this._iVideoStatusRepository.GetVideoStatusAdvanceSearchColumns();
           
        }
        

		public VideoStatus GetVideoStatus(System.Int32 VideoStatusId)
		{
			return _iVideoStatusRepository.GetVideoStatus(VideoStatusId);
		}

		public VideoStatus UpdateVideoStatus(VideoStatus entity)
		{
			return _iVideoStatusRepository.UpdateVideoStatus(entity);
		}

		public bool DeleteVideoStatus(System.Int32 VideoStatusId)
		{
			return _iVideoStatusRepository.DeleteVideoStatus(VideoStatusId);
		}

		public List<VideoStatus> GetAllVideoStatus()
		{
			return _iVideoStatusRepository.GetAllVideoStatus();
		}

		public VideoStatus InsertVideoStatus(VideoStatus entity)
		{
			 return _iVideoStatusRepository.InsertVideoStatus(entity);
		}


        public DataTransfer<GetOutput> Get(string _id)
        {
            DataTransfer<GetOutput> tranfer = new DataTransfer<GetOutput>();
            System.Int32 videostatusid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out videostatusid))
            {
				VideoStatus videostatus = _iVideoStatusRepository.GetVideoStatus(videostatusid);
                if(videostatus!=null)
                {
                    tranfer.IsSuccess = true;
                    GetOutput output = new GetOutput();
                    output.CopyFrom(videostatus);
                    tranfer.Data=output ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }   
        public DataTransfer<List<GetOutput>> GetAll()
        {
            DataTransfer<List<GetOutput>> tranfer = new DataTransfer<List<GetOutput>>();
            List<VideoStatus> videostatuslist = _iVideoStatusRepository.GetAllVideoStatus();
            if (videostatuslist != null && videostatuslist.Count>0)
            {
                tranfer.IsSuccess = true;
                List<GetOutput> outputlist = new List<GetOutput>();
                outputlist.CopyFrom(videostatuslist);
                tranfer.Data = outputlist;

            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: No record found.";
            }
            return tranfer;
        }
        public DataTransfer<PostOutput> Insert(PostInput Input)
        {
           DataTransfer<PostOutput> transer = new DataTransfer<PostOutput>();
            IList<string> errors = Validator.Validate(Input);
            if(errors.Count==0)
            {
                VideoStatus videostatus = new VideoStatus();
                PostOutput output = new PostOutput();
                videostatus.CopyFrom(Input);
                videostatus = _iVideoStatusRepository.InsertVideoStatus(videostatus);
                output.CopyFrom(videostatus);
                transer.IsSuccess = true;
                transer.Data = output;
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

        public DataTransfer<PutOutput> Update(PutInput Input)
        {
            DataTransfer<PutOutput> transer = new DataTransfer<PutOutput>();
            IList<string> errors = Validator.Validate(Input);
            if (errors.Count == 0)
            {
                VideoStatus videostatusinput = new VideoStatus();
                VideoStatus videostatusoutput = new VideoStatus();
                PutOutput output = new PutOutput();
                videostatusinput.CopyFrom(Input);
                VideoStatus videostatus = _iVideoStatusRepository.GetVideoStatus(videostatusinput.VideoStatusId);
                if (videostatus!=null)
                {
                    videostatusoutput = _iVideoStatusRepository.UpdateVideoStatus(videostatusinput);
                    if(videostatusoutput!=null)
                    {
                        output.CopyFrom(videostatusoutput);
                        transer.IsSuccess = true;
                        transer.Data = output;
                    }
                    else
                    {
                        transer.IsSuccess = false;
                        transer.Errors = new string[1];
                        transer.Errors[0] = "Error: Could not update.";
                    } 
                }
                else                
                {
                    transer.IsSuccess = false;
                    transer.Errors = new string[1];
                    transer.Errors[0] = "Error: Record not found.";
                }
            }
            else
            {
                transer.IsSuccess = false;
                transer.Errors = errors.ToArray<string>();
            }
            return transer;
        }

         public DataTransfer<string> Delete(string _id)
         {
            DataTransfer<string> tranfer = new DataTransfer<string>();
            System.Int32 videostatusid=0;
            if (!string.IsNullOrEmpty(_id) && System.Int32.TryParse(_id,out videostatusid))
            {
				 bool IsDeleted = _iVideoStatusRepository.DeleteVideoStatus(videostatusid);
                if(IsDeleted)
                {
                    tranfer.IsSuccess = true;
                    tranfer.Data=IsDeleted.ToString().ToLower() ;

                }
                else
                {
                    tranfer.IsSuccess = false;
                    tranfer.Errors = new string[1];
                    tranfer.Errors[0] = "Error: No record found.";
                }             
                
            }
            else
            {
                tranfer.IsSuccess = false;
                tranfer.Errors = new string[1];
                tranfer.Errors[0] = "Error: Invalid request.";
            }
            return tranfer;
        }
	}
	
	
}
