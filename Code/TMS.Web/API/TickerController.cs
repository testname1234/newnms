﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TMS.Core;
using TMS.Core.IService;
using TMS.Core.DataTransfer;
using TMS.Core.Entities;
using TMS.Core.Models;
using TMS.Core.Enums;

namespace TMS.Web.API
{
    public class TickerController : ApiController
    {
        private ITickerService tickerService = IoC.Resolve<ITickerService>("TMSTickerService");
        private ITickerLineService tickerLineService = IoC.Resolve<ITickerLineService>("TMSTickerLineService");
        private IChannelService channelService = IoC.Resolve<IChannelService>("TMSChannelService");
        private ITickerCategoryService tickerCategoryService = IoC.Resolve<ITickerCategoryService>("TMSTickerCategoryService");
        private ITickerImageService tickerImageService = IoC.Resolve<ITickerImageService>("TMSTickerImageService");
        private IVideoService videoService = IoC.Resolve<IVideoService>("TMSVideoService");
        private IExecutionService executionService = IoC.Resolve<IExecutionService>("TMSExecutionService");

        [HttpPost]
        [ActionName("LoadInitialData")]
        public DataTransfer<TickerLoadInitialDataOutput> LoadInitialData(TickerLoadInitialDataInput input)
        {
            DataTransfer<TickerLoadInitialDataOutput> transfer = new DataTransfer<TickerLoadInitialDataOutput>();

            try
            {
                if (input != null)
                {
                    transfer.Data = new TickerLoadInitialDataOutput();

                    List<Channel> channels = channelService.GetChannelByUserId(input.UserId);
                    if (channels != null)
                    {
                        transfer.Data.Channels = new List<Core.DataTransfer.Channel.GetOutput>();
                        transfer.Data.Channels.CopyFrom(channels);
                    }

                    List<TickerCategory> tickerCategories = tickerCategoryService.GetTickerCategories(input.TickerCategoryLastUpdateDate);
                    if (tickerCategories != null)
                    {
                        transfer.Data.TickerCategories = new List<Core.DataTransfer.TickerCategory.GetOutput>();
                        transfer.Data.TickerCategories.CopyFrom(tickerCategories);
                    }

                    if (input.ChannelId.HasValue && channels.Any(x => x.ChannelId == input.ChannelId.Value))
                    {
                        //List<TickerImage> tickerImages = tickerImageService.GetTickerImages(37, TickerImageStatuses.Pending);
                        //if (tickerImages != null && tickerImages.Count > 0)
                        //{
                        //    transfer.Data.TickerImages = new List<Core.DataTransfer.TickerImage.GetOutput>();
                        //    transfer.Data.TickerImages.CopyFrom(tickerImages);
                        //}
                    }
                }
            }
            catch (Exception ex)
            {
                transfer.IsSuccess = false;
                transfer.Errors = new string[1];
                transfer.Errors[0] = ex.Message;
                ExceptionLogger.Log(ex);
            }

            return transfer;
        }

        [HttpPost]
        [ActionName("InsertTickers")]
        public DataTransfer<TickerInsertOutput> InsertTickers(TickerInsertInput input)
        {
            DataTransfer<TickerInsertOutput> transfer = new DataTransfer<TickerInsertOutput>();

            try
            {
                if (input != null && input.Tickers != null)
                {
                    List<Ticker> listTickers = new List<Ticker>();

                    foreach (TMS.Core.DataTransfer.Ticker.PostInput ticker in input.Tickers)
                    {
                        Ticker entity = new Ticker();

                        if (ticker.TickerId > 0)
                        {
                            entity = tickerService.GetTicker(ticker.TickerId);
                        }
                        else
                        {
                            entity.CopyFrom(ticker);

                            entity.LastUpdatedDate = DateTime.UtcNow;
                            entity.CreationDate = DateTime.UtcNow;
                            entity.IsActive = true;

                            entity = tickerService.InsertTicker(entity);
                        }

                        if (entity != null && ticker.TickerLines != null)
                        {
                            entity.TickerLines = new List<TickerLine>();

                            foreach (TMS.Core.DataTransfer.TickerLine.PostInput tickerLine in ticker.TickerLines)
                            {
                                TickerLine tickerLineEntity = new TickerLine();

                                if (tickerLine.TickerLineId > 0)
                                {
                                    tickerLineEntity = tickerLineService.GetTickerLine(tickerLine.TickerLineId);
                                }
                                else
                                {
                                    tickerLineEntity.CopyFrom(tickerLine);

                                    tickerLineEntity.TickerId = entity.TickerId;
                                    tickerLineEntity.LastUpdatedDate = DateTime.UtcNow;
                                    tickerLineEntity.CreationDate = DateTime.UtcNow;
                                    tickerLineEntity.IsActive = true;

                                    tickerLineEntity = tickerLineService.InsertTickerLine(tickerLineEntity);
                                }

                                //if (tickerLineEntity != null)
                                //{
                                //    Execution executionEntity = new Execution();
                                //    executionEntity.TickerLineId = tickerLineEntity.TickerLineId;
                                //    executionEntity.ExecutionTime = tickerLine.OnAirTime;
                                //
                                //    executionService.InsertExecution(executionEntity);
                                //}

                                entity.TickerLines.Add(tickerLineEntity);
                            }

                            listTickers.Add(entity);
                        }
                    }

                    transfer.Data = new TickerInsertOutput();
                    transfer.Data.Tickers = new List<Core.DataTransfer.Ticker.GetOutput>();
                    transfer.Data.Tickers.CopyFrom(listTickers);
                }
            }
            catch (Exception ex)
            {
                transfer.IsSuccess = false;
                transfer.Errors = new string[1];
                transfer.Errors[0] = ex.Message;
                ExceptionLogger.Log(ex);
            }

            return transfer;
        }

        [HttpPost]
        [ActionName("UpdateVideoStatus")]
        public DataTransfer<bool> UpdateVideoStatus(UpdateVideoStatusInput input)
        {
            DataTransfer<bool> transfer = new DataTransfer<bool>();

            try
            {
                if (input != null)
                {
                    transfer.Data = videoService.UpdateVideoStatus(input.VideoId, (VideoStatuses)input.VideoStatusId);
                }
            }
            catch (Exception ex)
            {
                transfer.IsSuccess = false;
                transfer.Errors = new string[1];
                transfer.Errors[0] = ex.Message;
                ExceptionLogger.Log(ex);
            }

            return transfer;
        }

        [HttpPost]
        [ActionName("UpdateTickerImageStatus")]
        public DataTransfer<bool> UpdateTickerImageStatus(UpdateTickerStatusInput input)
        {
            DataTransfer<bool> transfer = new DataTransfer<bool>();

            try
            {
                if (input != null)
                {
                    transfer.Data = tickerImageService.UpdateTickerImageStatus(input.TickerImageId, input.TickerImageStatusId);
                }
            }
            catch (Exception ex)
            {
                transfer.IsSuccess = false;
                transfer.Errors = new string[1];
                transfer.Errors[0] = ex.Message;
                ExceptionLogger.Log(ex);
            }

            return transfer;
        }

        [HttpGet]
        [ActionName("GetChannelVideos")]
        public DataTransfer<ChannelVideosOutput> GetChannelVideos(int id)
        {
            DataTransfer<ChannelVideosOutput> transfer = new DataTransfer<ChannelVideosOutput>();
            transfer.Data = new ChannelVideosOutput();

            try
            {
                if (id > 0)
                {
                    List<Video> videos = videoService.GetVideos(id, VideoStatuses.Pending, null);
                    if (videos != null)
                    {
                        transfer.Data.Videos = new List<Core.DataTransfer.Video.GetOutput>();
                        transfer.Data.Videos.CopyFrom(videos);
                    }
                }
            }
            catch (Exception ex)
            {
                transfer.IsSuccess = false;
                transfer.Errors = new string[1];
                transfer.Errors[0] = ex.Message;
                ExceptionLogger.Log(ex);
            }

            return transfer;
        }

        [HttpPost]
        [ActionName("GetMoreTickerImages")]
        public DataTransfer<ChannelTickerImagesOutput> GetMoreTickerImages(ChannelTickerImageInput input)
        {
            DataTransfer<ChannelTickerImagesOutput> transfer = new DataTransfer<ChannelTickerImagesOutput>();
            transfer.Data = new ChannelTickerImagesOutput();

            try
            {
                if (input != null)
                {
                    List<TickerImage> tickerImages = tickerImageService.GetTickerImages(input.ChannelId, TickerImageStatuses.Pending, input.PageIndex, input.PageCount);
                    if (tickerImages != null && tickerImages.Count > 0)
                    {
                        transfer.Data.TickerImages = new List<Core.DataTransfer.TickerImage.GetOutput>();
                        transfer.Data.TickerImages.CopyFrom(tickerImages);
                    }
                }
            }
            catch (Exception ex)
            {
                transfer.IsSuccess = false;
                transfer.Errors = new string[1];
                transfer.Errors[0] = ex.Message;
                ExceptionLogger.Log(ex);
            }

            return transfer;
        }

        [HttpPost]
        [ActionName("GetTickerSuggestions")]
        public DataTransfer<TickerInsertOutput> GetTickerSuggestions(TickerInsertInput input)
        {
            DataTransfer<TickerInsertOutput> transfer = new DataTransfer<TickerInsertOutput>();
            try
            {
                if (!string.IsNullOrEmpty(input.Keyword))
                {
                    transfer.Data = new TickerInsertOutput();

                    List<Ticker> tickers = tickerService.GetTickers(input.Keyword.Trim());
                    if (tickers != null)
                    {
                        transfer.Data.Tickers = new List<Core.DataTransfer.Ticker.GetOutput>();
                        transfer.Data.Tickers.CopyFrom(tickers);
                    }
                }
            }
            catch (Exception ex)
            {
                transfer.IsSuccess = false;
                transfer.Errors = new string[1];
                transfer.Errors[0] = ex.Message;
                ExceptionLogger.Log(ex);
            }

            return transfer;
        }

        [HttpGet]
        [ActionName("GetTickerLineSuggestions")]
        public DataTransfer<TickerInsertOutput> GetTickerLineSuggestions(TickerInsertInput input)
        {
            DataTransfer<TickerInsertOutput> transfer = new DataTransfer<TickerInsertOutput>();
            transfer.Data = new TickerInsertOutput();

            try
            {
                if (input != null && input.TickerId.HasValue && !string.IsNullOrEmpty(input.Keyword))
                {
                    List<TickerLine> tickerLines = tickerLineService.GetTickerLines(input.TickerId.Value, input.Keyword);
                    if (tickerLines != null)
                    {
                        transfer.Data.TickerLines = new List<Core.DataTransfer.TickerLine.GetOutput>();
                        transfer.Data.TickerLines.CopyFrom(tickerLines);
                    }
                }
            }
            catch (Exception ex)
            {
                transfer.IsSuccess = false;
                transfer.Errors = new string[1];
                transfer.Errors[0] = ex.Message;
                ExceptionLogger.Log(ex);
            }

            return transfer;
        }
    }
}