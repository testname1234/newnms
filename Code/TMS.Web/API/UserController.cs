﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using NMS.Core;


using System.Web;


namespace TMS.Web.API
{
    public class UserController : ApiController, NMS.Core.IController.IUserController
    {
        NMS.Core.IService.IUserManagementService userManagementService = NMS.Core.IoC.Resolve<NMS.Core.IService.IUserManagementService>("UserManagementService");
        NMS.Core.IService.ITeamService teamService = NMS.Core.IoC.Resolve<NMS.Core.IService.ITeamService>("TeamService");
        NMS.Core.IService.ITeamRoleService teamRoleService = NMS.Core.IoC.Resolve<NMS.Core.IService.ITeamRoleService>("TeamRoleService");
      

        [HttpPost]
        [Obsolete("This method is deprecated.")]
        [ActionName("Login")]
        public NMS.Core.DataTransfer.DataTransfer<NMS.Core.DataTransfer.User.PostOutput> Login(NMS.Core.DataTransfer.User.PostInput input)
        {
            NMS.Core.DataTransfer.DataTransfer<NMS.Core.DataTransfer.User.PostOutput> transfer = new NMS.Core.DataTransfer.DataTransfer<NMS.Core.DataTransfer.User.PostOutput>();
            try
            {
                if (input != null)
                {
                    NMS.Core.DataTransfer.User.PostOutput user = new NMS.Core.DataTransfer.User.PostOutput();
                    if (user != null)
                        transfer.Data = user;
                    else
                    {
                        transfer.IsSuccess = false;
                        transfer.Errors = new string[] { "Login failed" };
                    }
                }
                else
                {
                    transfer.IsSuccess = false;
                    transfer.Errors = new string[] { "Invalid Input provided" };
                }
            }
            catch (Exception exp)
            {
                NMS.Core.ExceptionLogger.Log(exp);
                transfer.IsSuccess = false;
                transfer.Errors = new string[] { "An error occured" };
            }
            return transfer;
        }

        [HttpPost]
        [ActionName("LoginMMS")]
        public NMS.Core.DataTransfer.DataTransfer<NMS.Core.Models.MMSLoadInitialDataLoginOutput> LoginMMS(NMS.Core.DataTransfer.User.PostInput input)
        {
            string MMSUsermanagementUrl = NMS.Core.AppSettings.MMSUsermanagementUrl;

            NMS.Core.DataTransfer.DataTransfer<NMS.Core.Models.MMSLoadInitialDataLoginOutput> transfer = new NMS.Core.DataTransfer.DataTransfer<NMS.Core.Models.MMSLoadInitialDataLoginOutput>();

            try
            {
                if (input != null)
                {

                    NMS.Core.Helper.HttpWebRequestHelper helper = new NMS.Core.Helper.HttpWebRequestHelper();

                    NMS.Core.Models.MMSLoadInitialDataLoginInput mmsInput = new NMS.Core.Models.MMSLoadInitialDataLoginInput();
                    mmsInput.LoginId = input.Login;
                    mmsInput.Password = input.Password;

                    NMS.Core.DataTransfer.DataTransfer<NMS.Core.Models.MMSLoadInitialDataLoginOutput> output = helper.PostRequest<NMS.Core.DataTransfer.DataTransfer<NMS.Core.Models.MMSLoadInitialDataLoginOutput>>(MMSUsermanagementUrl + "api/admin/Authenticate", mmsInput, null);


                    if (output != null && output.IsSuccess == true)
                    {
                        transfer.Data = output.Data;

                        //Session["User"] = "";
                    }
                    else
                    {
                        transfer.IsSuccess = false;
                        transfer.Errors = new string[] { output.Errors[0].ToString() + "" };
                    }
                }
                else
                {
                    transfer.IsSuccess = false;
                    transfer.Errors = new string[] { "Invalid Input provided" };
                }
            }
            catch (Exception exp)
            {
                NMS.Core.ExceptionLogger.Log(exp);
                transfer.IsSuccess = false;
                transfer.Errors = new string[] { "An error occured" };
            }
            return transfer;
        }

        [HttpGet]
        [ActionName("GetUsers")]
        public MMS.Integration.DataTransfer<List<MMS.Integration.UserManagement.DataTransfer.UserOutput>> GetUsers()
        {
            MMS.Integration.DataTransfer<List<MMS.Integration.UserManagement.DataTransfer.UserOutput>> transfer = new MMS.Integration.DataTransfer<List<MMS.Integration.UserManagement.DataTransfer.UserOutput>>();
            MMS.Integration.UserManagement.UserManagement userManagement = new MMS.Integration.UserManagement.UserManagement();
            try
            {
                transfer = userManagement.UsersWithWorkRole(1);
            }
            catch (Exception exp)
            {
                NMS.Core.ExceptionLogger.Log(exp);
                transfer.IsSuccess = false;
                transfer.Errors = new string[] { exp.Message };
            }

            return transfer;
        }

        [HttpGet]
        [ActionName("GetUsersByProgram")]
        public NMS.Core.DataTransfer.DataTransfer<List<NMS.Core.DataTransfer.Team.GetOutput>> GetUsersByProgram(int programId)
        {
            NMS.Core.DataTransfer.DataTransfer<List<NMS.Core.DataTransfer.Team.GetOutput>> transfer = new NMS.Core.DataTransfer.DataTransfer<List<NMS.Core.DataTransfer.Team.GetOutput>>();

            try
            {
                List<NMS.Core.Entities.Team> teams = teamService.GetTeamByProgramId(programId);
                transfer.Data = new List<NMS.Core.DataTransfer.Team.GetOutput>();
                if (teams != null)
                    foreach (NMS.Core.Entities.Team t in teams)
                    {
                        NMS.Core.DataTransfer.Team.GetOutput entity = new NMS.Core.DataTransfer.Team.GetOutput();
                        entity.CopyFrom(t);
                        transfer.Data.Add(entity);
                    }
            }
            catch (Exception exp)
            {
                NMS.Core.ExceptionLogger.Log(exp);
                transfer.IsSuccess = false;
                transfer.Errors = new string[] { exp.Message };
            }

            return transfer;
        }

        [HttpPost]
        [ActionName("DeleteFromTeam")]
        public NMS.Core.DataTransfer.DataTransfer<bool> DeleteFromTeam(NMS.Core.DataTransfer.Team.PostInput input)
        {
            NMS.Core.DataTransfer.DataTransfer<bool> transfer = new NMS.Core.DataTransfer.DataTransfer<bool>();
            try
            {
                transfer.Data = teamService.DeleteFromTeam(Convert.ToInt32(input.UserId), Convert.ToInt32(input.WorkRoleId), Convert.ToInt32(input.ProgramId));
            }
            catch (Exception exp)
            {
                NMS.Core.ExceptionLogger.Log(exp);
                transfer.IsSuccess = false;
                transfer.Errors = new string[] { exp.Message };
            }
            return transfer;
        }

        [HttpPost]
        [ActionName("AddUserToTeam")]
        public NMS.Core.DataTransfer.DataTransfer<List<NMS.Core.DataTransfer.Team.GetOutput>> AddUserToTeam(NMS.Core.DataTransfer.Team.PostInput input)
        {
            NMS.Core.DataTransfer.DataTransfer<List<NMS.Core.DataTransfer.Team.GetOutput>> transfer = new NMS.Core.DataTransfer.DataTransfer<List<NMS.Core.DataTransfer.Team.GetOutput>>();

            try
            {
                string message = teamService.InsertTeam(input);
                transfer.Data = new List<NMS.Core.DataTransfer.Team.GetOutput>();
                List<NMS.Core.Entities.Team> teams = teamService.GetTeamByProgramId(Convert.ToInt32(input.ProgramId));
                foreach (NMS.Core.Entities.Team t in teams)
                {
                    NMS.Core.DataTransfer.Team.GetOutput entity = new NMS.Core.DataTransfer.Team.GetOutput();
                    entity.CopyFrom(t);
                    transfer.Data.Add(entity);
                }
            }
            catch (Exception exp)
            {
                NMS.Core.ExceptionLogger.Log(exp);
                transfer.IsSuccess = false;
                transfer.Errors = new string[] { exp.Message };
            }

            return transfer;
        }


        [HttpGet]
        [ActionName("GetTeamRole")]
        public NMS.Core.DataTransfer.DataTransfer<List<NMS.Core.DataTransfer.TeamRole.GetOutput>> GetTeamRole()
        {
            NMS.Core.DataTransfer.DataTransfer<List<NMS.Core.DataTransfer.TeamRole.GetOutput>> transfer = new NMS.Core.DataTransfer.DataTransfer<List<NMS.Core.DataTransfer.TeamRole.GetOutput>>();
            try
            {
                transfer = teamRoleService.GetAll();
            }
            catch (Exception exp)
            {
                NMS.Core.ExceptionLogger.Log(exp);
                transfer.IsSuccess = false;
                transfer.Errors = new string[] { exp.Message };
            }

            return transfer;
        }
    }
}
