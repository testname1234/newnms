﻿using System.Web.Optimization;

namespace TMS.Web
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/models/")
            .IncludeDirectory("~/Scripts/app/models/", "*.js", searchSubdirectories: false));

            bundles.Add(new ScriptBundle("~/bundles/managers/")
            .IncludeDirectory("~/Scripts/app/managers/", "*.js", searchSubdirectories: false));

            bundles.Add(new ScriptBundle("~/bundles/common/")
            .IncludeDirectory("~/Scripts/app/common/", "*.js", searchSubdirectories: false));

            bundles.Add(new ScriptBundle("~/bundles/ticker/")
            .IncludeDirectory("~/Scripts/app/Ticker", "*.js", searchSubdirectories: false));

            bundles.Add(new ScriptBundle("~/bundles/mock/")
            .IncludeDirectory("~/Scripts/app/mock", "*.js", searchSubdirectories: false));

            bundles.Add(new ScriptBundle("~/bundles/usermanagement/")
           .IncludeDirectory("~/Scripts/app/user", "*.js", searchSubdirectories: false));
        }
    }
}