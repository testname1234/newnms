﻿define('binder',
    [
        'jquery',
        'ko',
        'config',
        'enum',
        'presenter',
        'vm',
        'appdata',
        'manager',
        'datacontext',
        'model',
        'dataservice',
        'vm.tickerreporter'
    ],
    function ($, ko, config, e, presenter, vm, appdata, manager, dc, model, dataservice, tickerReporterVM) {
        var logger = config.logger;

        var
            ids = config.viewIds.tickerManagement,

            bindPreLoginViews = function () {
                ko.applyBindings(vm.shell, getView(ids.shellTopNavView));
                ko.applyBindings(vm.tickerreporter, getView(ids.tickerReporter));
            },

            bindPostLoginViews = function () {
            },

            deleteExtraViews = function () {
            },

            bindStartUpEvents = function () {
                $(document).keydown(function (arg) {
                    switch (arg.which) {
                        case e.Keys.LEFT:
                            tickerReporterVM.navigateTickerImage(true);
                            break;
                        case e.Keys.RIGHT:
                            tickerReporterVM.navigateTickerImage(false);
                            break;
                        default:
                            return;
                    }
                    arg.preventDefault();
                });
            },

            getView = function (viewName) {
                return $(viewName).get(0);
            };

        return {
            bindPreLoginViews: bindPreLoginViews,
            bindPostLoginViews: bindPostLoginViews,
            bindStartUpEvents: bindStartUpEvents,
            deleteExtraViews: deleteExtraViews
        }
    });