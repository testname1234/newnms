﻿define('bootstrapper',
    [
        'enum',
        'config',
        'binder',
        'route-config',
        'manager',
        'vm',
        'presenter',
        'appdata',
        'helper',
        'model.user',
        'ko'
    ],
    function (e, config, binder, routeConfig, manager, vm, presenter, appdata, helper, usermodel, ko) {

        if ($.trim(helper.getCookie('user-id')) == '') {
            window.location.href = '/Login#/index';
        }
        var
            run = function () {
                
                config.dataserviceInit();
                appdata.extractUserInformation();
                binder.bindPreLoginViews();
                binder.bindStartUpEvents();
                routeConfig.register();
                presenter.toggleActivity(true);

                if ($.trim(helper.getCookie('user-id')) == '') {
                    window.location.href = '/Login#/index';
                }
                $.when(manager.ticker.loadInitialData())
                   .done(function (responseData) {
                       binder.bindPostLoginViews();
                       vm.shell.init();
                       vm.tickerreporter.init();
                     
                       setTimeout(function () {
                           presenter.toggleActivity(false);
                       }, 100);
                   })
                   .fail(function (responseData) {
                   });
                presenter.toggleActivity(false);
            };

        return {
            run: run
        }
    });