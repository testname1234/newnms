﻿define('route-config',
    ['config', 'router', 'vm', 'appdata', 'enum'],
    function (config, router, vm, appdata, e) {
        var
            logger = config.logger,

            register = function () {
                var routeData;
                routeData = [
                   // Pending Stories routes
                   {
                       view: config.viewIds.tickerManagement.tickerReporter,
                       routes: [
                           {
                               isDefault: true,
                               route: config.hashes.tickerManagement.tickerReporter,
                               title: 'Axact Media Solution',
                               callback: vm.tickerreporter.activate,
                               group: '.route-top'
                           }
                       ]
                   },
                ];

                // Invalid routes
                routeData.push({
                    view: '',
                    route: /.*/,
                    title: '',
                    callback: function () {
                        logger.error(config.toasts.invalidRoute);
                    }
                });
                
                for (var i = 0; i < routeData.length; i++) {
                    router.register(routeData[i]);
                }

                // Crank up the router
                router.run();
            };

        return {
            register: register
        };
    });