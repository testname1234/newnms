﻿define('vm',
    [
        'vm.shell',
        'vm.tickerreporter'
    ],

    function (shell,tickerreporter) {
        return {
            shell: shell,
            tickerreporter: tickerreporter
        };
    });