﻿define('vm.shell',
     [
         'ko',
         'config',
         'router',
         'enum',
         'datacontext',
         'model',
         'manager',
         'appdata',
         'presenter',
         'utils',
         'model.user',
         'helper'
       
     ],
    function (ko, config, router, e, dc, model, manager, appdata, presenter, utils, usermodel, helper) {
        var logger = config.logger;

        var
            // Properties
            //-------------------

            menuHashes = config.hashes,
            templates = config.templateNames,
            currentHash = ko.computed({
                read: function () {
                    return router.currentHash();
                },
                deferEvaluation: true
            }),
            activeHash = ko.computed({
                read: function () {
                    if (currentHash() == menuHashes.tickerManagement.tickerReporter) {
                        return menuHashes.tickerManagement.tickerReporter;
                    }
                },
                deferEvaluation: true
            }),


            // Methods
            //-------------------
            displayHelp = function () {
                router.navigateTo(config.hashes.production.report);
            },
            logOut = function () {
                window.location.href = '/login#/index';
                document.cookie = config.sessionKeyName + '=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';
                document.cookie = config.roleid + '=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';
                document.cookie = config.userid + '=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';
            },

            activate = function (routeData) {
            },
            init = function () {
              
            };

        return {
            activate: activate,
            menuHashes: menuHashes,
            templates:templates,
            init: init,
            currentHash: currentHash,
            activeHash: activeHash,
            appdata: appdata,
            logOut: logOut
        };
    });