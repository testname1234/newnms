﻿define('vm.tickerreporter',
     ['config', 'messenger', 'model', 'datacontext', 'model.mapper', 'moment', 'appdata', 'enum', 'presenter', 'manager', 'underscore', 'utils', 'router'],
     function (config, messenger, model, dc, mapper, moment, appdata, e, presenter, manager, _, utils, router) {
         var
             // Properties
             // ------------------------

             templates = config.templateNames,
             trc = model.TickerReporterControl(),
             currentChannel = ko.observable(),
             tickerTypes = ko.observableArray([]),
             isPreviousVisible = ko.observable(false),
             currentSelectedGroupId = ko.observable(),
             isNextVisible = ko.observable(false),
             isSkipVisible = ko.observable(false),
             currentTickerImageIndex = ko.observable(0),

             // Computed Properties
             // ------------------------

             channels = ko.computed({
                 read: function () {
                     return dc.channels.getObservableList();
                 },
                 deferEvaluation: true
             }),
             categories = ko.computed({
                 read: function () {
                     return dc.tickerCategories.getObservableList();
                 },
                 deferEvaluation: true
             }),
             currentTickerImage = ko.computed({
                 read: function () {
                     
                     var arr = dc.tickerImages.getObservableList();
                     if (arr.length > 0 && currentTickerImageIndex() < arr.length) {
                         return arr[currentTickerImageIndex()];
                     } else {
                         return new model.TickerImage();
                     }
                 },
                 deferEvaluation: true
             }),
             currentTickerImageNext = ko.computed({
                 read: function () {

                     var arr = dc.tickerImages.getObservableList();
                     if (arr.length > 0 && currentTickerImageIndex() < arr.length  && arr[currentTickerImageIndex() + 1]) {
                         return arr[currentTickerImageIndex() + 1];
                     } else {
                         return new model.TickerImage();
                     }
                 },
                 deferEvaluation: true
             }),
             currentTickerImagePrev = ko.computed({
                  read: function () {

                      var arr = dc.tickerImages.getObservableList();
                      if (arr.length > 0 && currentTickerImageIndex() < arr.length && arr[currentTickerImageIndex() - 1]) {
                          return arr[currentTickerImageIndex() - 1];
                      } else {
                          return new model.TickerImage();
                      }
                  },
                  deferEvaluation: true
              }),
             // Methods
             // ------------------------

             navigateTickerImage = function (direction) {
                
                 manager.ticker.updateTickerImageStatus(currentTickerImage(), e.TickerImageStatus.Skipped);

                 var arr = dc.tickerImages.getObservableList();
                 if (direction) {
                     if (currentTickerImageIndex() - 1 > 0)
                         currentTickerImageIndex(currentTickerImageIndex() - 1);
                     else
                         currentTickerImageIndex(0);
                 } else {
                     if (currentTickerImageIndex() + 1 < arr.length)
                         currentTickerImageIndex(currentTickerImageIndex() + 1);
                     else
                         currentTickerImageIndex(arr.length - 1);
                 }
                 if (arr.length - currentTickerImageIndex() < 20)
                 {
                     
                     manager.ticker.getMoreTickerImages();

                 }
             
                 trc.setCurrentTickerImage(currentTickerImage());
              
             },
             insertTicker = function (data) {
                 if (!currentChannel()) {
                     config.logger.error("Please select a channel!");
                     return;
                 }
                 if (!trc.currentTicker().tickerTypeId()) {
                     config.logger.error("Please select a ticker type!");
                     return;
                 }
                 if (!trc.currentTicker().categoryId()) {
                     config.logger.error("Please select a category!");
                     return;
                 }
                 if (!trc.currentTicker().groupName()) {
                     config.logger.error("Please enter a group name!");
                     return;
                 }
                 if (!trc.tickerLineList().length > 0) {
                     config.logger.error("Please enter atleast one ticker!");
                     return;
                 }

                 manager.ticker.insertTicker(trc.currentTicker(), trc.tickerLineList(), currentChannel());
                

                 $.when(manager.ticker.insertTicker(trc.currentTicker(), trc.tickerLineList(), currentChannel()) ).done(function (x) {
                     navigateTickerImage(false);
                 });


                 trc.isReset(true);
                 trc.resetControl();
             },
             getTickerSuggestions = function (value) {
                 
                 if (value && value.length > 0)
                     manager.ticker.getTickerSuggestions(value, trc.groupSuggestionList);
                 else
                     trc.groupSuggestionList([]);
             },
             setSuggestionText = function (data) {
                 trc.groupSuggestionList([]);
                 currentSelectedGroupId(undefined);
                
                 currentSelectedGroupId(data.id);
                 trc.currentTicker().id = currentSelectedGroupId();
                 trc.currentTicker().groupName(data.groupName());

                 $("#group_name").val(data.groupName());
             },

             subscribeEvent = function () {
                 currentChannel.subscribe(function (value) {
                     trc.isReset(true);
                     trc.resetControl();

                     if (value) {
                         var channel = dc.channels.getLocalById(value);
                         if (channel) {
                             appdata.currentChannel(channel);
                             manager.ticker.getMoreTickerImages();
                         }
                     }
                 });
             },
             activate = function (routeData, callback) {
                 messenger.publish.viewModelActivated({ canleaveCallback: canLeave });
                 $('body').addClass('tickerspage');
                 $('html').removeClass('scrolstart scrolstart1');
             },
             canLeave = function () {
                 $('body').removeClass('tickerspage');
                 return true;
             },
             init = function () {
                 subscribeEvent();

                 tickerTypes.push({ id: 1, name: 'News Ticker' });
                 tickerTypes.push({ id: 2, name: 'Program Ticker' });
                 currentTickerImageIndex(0);
             };

         return {
             activate: activate,
             canLeave: canLeave,
             init: init,
             trc: trc,
             templates: templates,
             channels: channels,
             currentChannel: currentChannel,
             insertTicker: insertTicker,
             categories: categories,
             tickerTypes: tickerTypes,
             isPreviousVisible: isPreviousVisible,
             isSkipVisible: isSkipVisible,
             isNextVisible: isNextVisible,
             e: e,
             getTickerSuggestions: getTickerSuggestions,
             setSuggestionText: setSuggestionText,
             currentTickerImage: currentTickerImage,
             currentTickerImagePrev: currentTickerImagePrev,
             currentTickerImageNext: currentTickerImageNext,
             navigateTickerImage: navigateTickerImage
         };
     });