﻿define('vm.login',
    ['ko', 'jquery', 'manager', 'datacontext', 'config'],
    function (ko, $, manager, dc, config) {
        var
            username = ko.observable(),
            password = ko.observable(),
            modules = ko.observableArray([]),
            errorMessage = ko.observable(false),

            login = function () {
                var obj = {};
                obj.Login = $.trim(username());
                obj.Password = $.trim(password());

                $.when(manager.usermanagement.userlogin(obj))
                .done(function (data) {
                    if (data.IsSuccess && data.Data) {
                        errorMessage(false);

                        var id = data.Data.Modules[0].UserId;

                        modules(dc.userRoles.getModuleUrlBymoduleId(id)());

                        if (modules().length <= 1) {
                            url = modules()[0].moduleUrl
                            document.cookie = config.userid + "=" + modules()[0].userId;
                            document.cookie = config.sessionKeyName + "=" + modules()[0].sessionkey;
                            document.cookie = "UserName" + "=" + modules()[0].fullName;
                            try {
                                if (app) {
                                    document.cookie = "UserName" + "=" + modules()[0].workRoleName;
                                }
                            }
                            catch (e) {
                            }
                            document.cookie = config.roleid + "=" + modules()[0].workRoleId;
                            window.location.href = url;
                        }
                    }

                    else {
                    }
                })
                .fail(function (ex) {
                    errorMessage(true);
                });
            },
            naviageroute = function (d) {
                url = d.moduleUrl
                document.cookie = config.userid + "=" + d.userId;
                document.cookie = config.sessionKeyName + "=" + d.sessionkey;
                document.cookie = config.roleid + "=" + d.workRoleId;
                window.location.href = url;
            };

        $("input").keyup(function (event) {
            if (event.keyCode == 13) {
                login();
            }
        });

        activate = function (routeData, callback) {
        },
        canLeave = function () {
            return true;
        };

        return {
            activate: activate,
            canLeave: canLeave,
            username: username,
            password: password,
            login: login,
            modules: modules,
            errorMessage: errorMessage,
            naviageroute: naviageroute
        };
    });