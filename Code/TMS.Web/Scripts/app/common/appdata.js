﻿define('appdata',
    ['ko', 'utils', 'moment', 'model.user', 'model.channel', 'helper', 'enum'],
    function (ko, utils, moment, User, Channel, helper, e) {
        // Properties
        // ------------------------------

        var
            sessionKey = helper.getCookie('SessionKey'),
            currentUser = ko.observable(new User()),
            currentTime = ko.observable(),
            currentChannel = ko.observable(new Channel()),
            channelLastUpdateDate = utils.getDefaultUTCDate(),
            tickerCategoryLastUpdateDate = utils.getDefaultUTCDate(),
            tickerImageLastUpdateDate = utils.getDefaultUTCDate(),

            // Methods
            // ------------------------------

            extractUserInformation = function () {
                var tempUser = new User();

                tempUser.id = parseInt($.trim(helper.getCookie('user-id')));
                tempUser.name = $.trim(helper.getCookie('UserName').toUpperCase());
                tempUser.displayName = $.trim(helper.getCookie('UserName').toUpperCase());
                tempUser.userType = parseInt($.trim(helper.getCookie('role-id')));

                currentUser(tempUser);
            },

            init = function () {
                setInterval(function () {
                    currentTime(new Date());
                }, 1000);
            };

        init();

        return {
            currentUser: currentUser,
            extractUserInformation: extractUserInformation,
            sessionKey: sessionKey,
            channelLastUpdateDate: channelLastUpdateDate,
            tickerCategoryLastUpdateDate: tickerCategoryLastUpdateDate,
            tickerImageLastUpdateDate: tickerImageLastUpdateDate,
            currentChannel: currentChannel
        };
    });