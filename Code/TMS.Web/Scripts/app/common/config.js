﻿define('config',
    ['mock/mock', 'toastr'],
    function (mock, toastr) {
        var
            // Properties
            //------------
            ApiBaseUrl = '/api/',

            UserConsoleApiUrl = 'http://localhost:9000/api/',

            ServerDateFormat = 'YYYY-MM-DD',

            sessionKeyName = "SessionKey",

            roleid = "role-id",

            userid = "user-id",

            ShortDateFormat = 'Do MMM YY-HH:MM',

            DateFormat = 'MMMM D, YYYY',

            DateTimeFormat = 'MMMM D, YYYY HH:MM:SS',

            ResourceUploadUrl = '/api/ResourceUploader/',

            MediaServerUrl = 'http://10.3.12.119/api/Resource/',

            verificationscreenflag = '',

            bucketId = 1,
            apiKey = "H@);KDJ7Xd",

            storeExpirationMs = (1000 * 60 * 60 * 24),
            logger = toastr,

            hashes = {
                  tickerManagement: {
                    tickerReporter : '#/home' 
                },
            },

            viewIds = {
                tickerManagement: {
                    tickerReporter: '#reporter-home',
                    shellTopNavView: '#shell-top-nav-view'
                }
            },

         
            templateNames = {
                tickerReporter: 'usercontrol.tickerreporter'
            },
            views = {
                usermanagement: {
                    login: { title: 'Login', url: '#/index', isInTopMenu: false, view: "#login-view" }
                }
            }
            toasts = {
                changesPending: 'Please save or cancel your changes before leaving the page.',
                errorSavingData: 'Data could not be saved. Please check the logs.',
                errorGettingData: 'Could not retrieve data.  Please check the logs.',
                invalidRoute: 'Cannot navigate. Invalid route',
                retreivedData: 'Data retrieved successfully',
                savedData: 'Data saved successfully',
                cantProceed: 'Please select stories to proceed',
                Proceedsuccess: 'Stories saved successfully!'
            },

            messages = {
                viewModelActivated: 'viewmodel-activation'
            },

            eventIds = {
                onLogIn: "logged-in"
            },

            modalDialogIds = {
            },

            storageType = {
                localStorage: 0,
                sessionStorage: 1
            },

            localStorageKeys = {
                credentials: "credentials"
            },

            stateKeys = {
                lastView: 'state.active-hash'
            },

            throttle = 500,
            pageTransitionThrottle = 500,
            maxItemsPerPage = 10,
            pollingInterval = 5,
            displayPollingInterval = 1,
            reporterPollingInterval = 5,
            publicReporterPollingInterval = 5,
            _useMocks = false, // Set this to toggle mocks
            _useWebAPI = true,
            noimageUrl = '/content/images/producer/noimage.jpg',
            imgurl = '../../Content/images/reporter/article-image.jpg',
            pleaseWait = '../../Content/images/producer/pleaseWait3.png',
            sampleDataFile = '/App_data/sampledata.txt',

            dataserviceInit = function () {
            },
            useMocks = function (val) {
                if (val) {
                    _useMocks = val;
                    init();
                }
                return _useMocks;
            },
            configureExternalTemplates = function () {
                infuser.defaults.templatePrefix = "_";
                infuser.defaults.templateSuffix = ".tmpl.html?v=2.1";
                infuser.defaults.templateUrl = "/Tmpl";
               //infuser.defaults.ajax.cache = false;
            },
             CutterpresentScreen = function (obj) {

                 if (obj == 'canvasName')
                 { return 'myCanvas'; }
                 if (obj == 'canvasButton')
                 { return '#lasso'; } //#lasso1 , #lasso2 , #lasso3
                 if (obj == 'pointDiffer')
                 { return 120; }
                 if (obj == 'Screen')
                 { return '#content-viewer'; }

             },
            init = function () {
                if (_useMocks) {
                    dataserviceInit = mock.dataserviceInit;
                }
                if (!_useWebAPI) {
                    ApiBaseUrl = 'http://localhost:9000/api/';
                }
                dataserviceInit();
                configureExternalTemplates();
            };

        init();

        return {
            logger: logger,
            ApiBaseUrl: ApiBaseUrl,
            ResourceUploadUrl: ResourceUploadUrl,
            MediaServerUrl: MediaServerUrl,
            hashes: hashes,
            viewIds: viewIds,
            eventIds: eventIds,
            storageType: storageType,
            localStorageKeys: localStorageKeys,
            modalDialogIds: modalDialogIds,
            useMocks: useMocks,
            stateKeys: stateKeys,
            dataserviceInit: dataserviceInit,
            throttle: throttle,
            maxItemsPerPage: maxItemsPerPage,
            pageTransitionThrottle: pageTransitionThrottle,
            pollingInterval: pollingInterval,
            displayPollingInterval: displayPollingInterval,
            window: window,
            toasts: toasts,
            templateNames: templateNames,
            noimageUrl: noimageUrl,
            reporterPollingInterval: reporterPollingInterval,
            publicReporterPollingInterval: publicReporterPollingInterval,
            sampleDataFile: sampleDataFile,
            ShortDateFormat: ShortDateFormat,
            DateFormat: DateFormat,
            DateTimeFormat: DateTimeFormat,
            ServerDateFormat: ServerDateFormat,
            messages: messages,
            sessionKeyName: sessionKeyName,
            roleid: roleid,
            verificationscreenflag: verificationscreenflag,
            userid: userid,
            CutterpresentScreen: CutterpresentScreen,
            pleaseWait: pleaseWait,
            bucketId: bucketId,
            apiKey: apiKey,
            views: views
        }
    });