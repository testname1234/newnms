﻿define('datacontext',
	['jquery', 'underscore', 'ko', 'model.mapper', 'model', 'config', 'utils'],
	function ($, _, ko, modelmapper, model, config, utils) {
	    var logger = config.logger,

			getCurrentUserId = function () {
			    return config.currentUser().id();
			},

			itemsToArray = function (items, observableArray, filter, sortFunction) {
			    // Maps the memo to an observableArray,
			    // then returns the observableArray
			    if (!observableArray) return;

			    // Create an array from the memo object
			    var underlyingArray = utils.mapMemoToArray(items);

			    if (filter) {
			        underlyingArray = _.filter(underlyingArray, function (o) {
			            var match = filter.predicate(filter, o);
			            return match;
			        });
			    }
			    if (sortFunction) {
			        underlyingArray.sort(sortFunction);
			    }
			    observableArray(underlyingArray);
			},
			mapToContext = function (dtoList, items, results, mapper, filter, sortFunction) {
			    // Loop through the raw dto list and populate a dictionary of the items
			    items = _.reduce(dtoList, function (memo, dto) {
			        var id = mapper.getDtoId(dto);
			        var existingItem = items[id];
			        memo[id] = mapper.fromDto(dto, existingItem);
			        return memo;
			    }, {});
			    itemsToArray(items, results, filter, sortFunction);
			    return items; // must return these
			},
			EntitySet = function (getFunction, mapper, nullo, updateFunction) {
			    var
					items = {},
					observableList = ko.observableArray([]),
					channelsDictionary = {},
                    tickerDictionary = {},
					resourceDictionary = {},
					UserRoleDictionary = {},

					mapDtoToContext = function (dto) {
					    var id = mapper.getDtoId(dto);
					    var existingItem = items[id];
					    items[id] = mapper.fromDto(dto, existingItem);
					    return items[id];
					},

					add = function (dto, options) {
					    var newItem = mapDtoToContext(dto);

					    if (options) {
					        if (options.addToObservable) {
					            observableList.push(newItem);
					        }
					        if (options.groupByEpisodeId) {
					            if (episodeDictionary[newItem.episodeId]) {
					                if (episodeDictionary[newItem.episodeId]().length === 0) {
					                    episodeDictionary[newItem.episodeId]([newItem]);
					                }
					                else {
					                    var simpleArray = episodeDictionary[newItem.episodeId]();
					                    simpleArray.push(newItem);
					                    episodeDictionary[newItem.episodeId](simpleArray);
					                }
					            }
					            else {
					                episodeDictionary[newItem.episodeId] = ko.observableArray([newItem]);
					            }
					        }
					        if (options.groupByProgramElementId) {
					            if (programElementDictionary[newItem.programElementId]) {
					                if (programElementDictionary[newItem.programElementId]().length === 0) {
					                    programElementDictionary[newItem.programElementId]([newItem]);
					                }
					                else {
					                    var simpleArray = programElementDictionary[newItem.programElementId]();
					                    simpleArray.push(newItem);
					                    programElementDictionary[newItem.programElementId](simpleArray);
					                }
					            }
					            else {
					                programElementDictionary[newItem.programElementId] = ko.observableArray([newItem]);
					            }
					        }
					    }

					    return newItem;
					},

					addToObservableList = function (itemsArray) {
					    if (observableList().length > 0) {
					        var simpleArray = observableList();
					        for (var j = 0; j < itemsArray.length; j++) {
					            simpleArray.unshift(itemsArray[j])
					            items[itemsArray[j].id] = itemsArray[j];
					        }
					        observableList(simpleArray);
					    }
					    else {
					        observableList(itemsArray);
					    }
					},

                    addToDictionary = function (obj, refId, dictionaryId) {
                        var dictionary = {};

                        switch (dictionaryId) {
                            case 'episode':
                                dictionary = episodeDictionary;
                                break;
                            case 'program-element':
                                dictionary = programElementDictionary;
                                break;
                            default:
                                return;
                                break;
                        }

                        if (obj && refId) {
                            var tempArray = dictionary[refId]()
                            if (tempArray) {
                                tempArray.push(obj);
                                dictionary[refId](tempArray);
                            }
                        }
                    },

					removeById = function (id, removeFromObservableList) {
					    delete items[id];
					    if (removeFromObservableList) {
					        for (var i = 0; i < observableList().length; i++) {
					            if (observableList()[i].id === id) {
					                return observableList.splice(i, 1);
					            }
					        };
					    }
					},

					removeFromDictionary = function (id, refId, dictionaryId) {
					    var dictionary = {};

					    switch (dictionaryId) {
					        case 'episode':
					            dictionary = episodeDictionary;
					            break;
					        case 'program-element':
					            dictionary = programElementDictionary;
					            break;
					        default:
					            return;
					            break;
					    }

					    if (id && refId) {
					        var tempArray = dictionary[refId]()
					        if (tempArray) {
					            for (var i = 0; i < tempArray.length; i++) {
					                if (tempArray[i].id === id) {
					                    return tempArray.splice(i, 1);
					                }
					            }
					        }
					    }
					},

					getLocalById = function (id) {
					    // This is the only place we set to NULLO
					    return !!id && !!items[id] ? items[id] : nullo;
					},

                    getByTickerId = function (id) {
                        var array = !!id && !!tickerDictionary[id] ? tickerDictionary[id] : ko.observableArray([]);
                        if (!!tickerDictionary[id] === false)
                            tickerDictionary[id] = array;
                        return array;
                    },

                    getByTickerTypeId = function (id) {
                        var array = !!id && !!tickerDictionary[id] ? tickerDictionary[id] : ko.observableArray([]);
                        if (!!tickerDictionary[id] === false)
                            tickerDictionary[id] = array;
                        return array;
                    },

					getByChannelId = function (id) {
					    var array = !!id && !!channelsDictionary[id] ? channelsDictionary[id] : ko.observableArray([]);
					    if (!!channelsDictionary[id] === false)
					        channelsDictionary[id] = array;
					    return array;
					},

                    getModuleUrlBymoduleId = function (id) {
                        var array = !!id && !!UserRoleDictionary[id] ? UserRoleDictionary[id] : ko.observableArray([]);
                        if (!!UserRoleDictionary[id] === false)
                            UserRoleDictionary[id] = array;
                        return array;
                    },

					getAllLocal = function () {
					    return utils.mapMemoToArray(items);
					},

					getAllLocalByIds = function (listIds) {
					    var itemsArray = [];
					    for (var i = 0; i < listIds.length; i++) {
					        var tempObj = getLocalById(listIds[i]);
					        if (tempObj) {
					            itemsArray.push(tempObj);
					        }
					    }
					    return itemsArray;
					},

					getObservableList = function (id) {
					    if (id) {
					        var array = !!id && !!observableList()[id] ? observableList()[id] : ko.observableArray([]);
					        if (!!observableList()[id] === false)
					            observableList()[id] = array;
					        return array;
					    }
					    else {
					        return observableList();
					    }
					},

					getData = function (options) {
					    return $.Deferred(function (def) {
					        var results = options && options.results,
								sortFunction = options && options.sortFunction,
								filter = options && options.filter,
								forceRefresh = options && options.forceRefresh,
								param = options && options.param,
								getFunctionOverride = options && options.getFunctionOverride;

					        getFunction = getFunctionOverride || getFunction;

					        // If the internal items object doesnt exist,
					        // or it exists but has no properties,
					        // or we force a refresh
					        if (forceRefresh || !items || !utils.hasProperties(items)) {
					            getFunction({
					                success: function (dtoList) {
					                    items = mapToContext(dtoList, items, results, mapper, filter, sortFunction);
					                    def.resolve(results);
					                },
					                error: function (response) {
					                    logger.error(config.toasts.errorGettingData);
					                    def.reject();
					                }
					            }, param);
					        } else {
					            itemsToArray(items, results, filter, sortFunction);
					            def.resolve(results);
					        }
					    }).promise();
					},

					fillData = function (list, options) {
					    if (list && list.length > 0) {
					        if (options && options.sort) {
					            list.sort(function (entity1, entity2) {
					                return mapper.getSortedValue(entity1) > mapper.getSortedValue(entity2) ? 1 : -1;
					            });
					        }

					        var temp = [];

					        temp = mapToContext(list, items, null, mapper, null, null);

					        var itemsArray = getItemsArray(temp);
					        if (options) {
					            var channelIds = [];
					            var channelArray = [];
					            var tickerIds = [];
					            var tickerArray = [];
					            var modulesId = [];
					            var moduleurlarray = [];

					            if (options.groupByChannelId) {
					                for (var i = 0; i < itemsArray.length; i++) {
					                    var item = itemsArray[i];
					                    if (channelArray[item.channelId] == null) {
					                        channelArray[item.channelId] = [];
					                        channelIds.push(item.channelId);
					                    }

					                    channelArray[item.channelId].push(itemsArray[i]);
					                }
					            }
					            if (options.groupByTickerId) {
					                for (var i = 0; i < itemsArray.length; i++) {
					                    var item = itemsArray[i];
					                    if (tickerArray[item.tickerId] == null) {
					                        tickerArray[item.tickerId] = [];
					                        tickerIds.push(item.tickerId);
					                    }

					                    tickerArray[item.tickerId].push(itemsArray[i]);
					                }
					            }
					            if (options.groupModuleUrlByModuleId) {
					                for (var i = 0; i < itemsArray.length; i++) {
					                    var item = itemsArray[i];

					                    if (moduleurlarray[item.userId] == null) {
					                        moduleurlarray[item.userId] = [];
					                        modulesId.push(item.userId);
					                    }

					                    moduleurlarray[item.userId].push(itemsArray[i]);
					                }
					            }

					            for (var i = 0; i < tickerIds.length; i++) {
					                var id = tickerIds[i];
					                if (tickerDictionary[id]) {
					                    if (tickerDictionary[id]().length === 0) {
					                        tickerDictionary[id](tickerArray[id]);
					                    }
					                    else {
					                        var simpleArray = tickerDictionary[id]();

					                        for (var j = 0; j < tickerArray[id].length; j++) {
					                            simpleArray.push(tickerArray[id][j])
					                        }
					                        tickerDictionary[id](simpleArray);
					                    }
					                }
					                else {
					                    tickerDictionary[tickerIds[i]] = ko.observableArray(tickerArray[id]);
					                }
					            }

					            for (var i = 0; i < modulesId.length; i++) {
					                var id = modulesId[i];
					                if (UserRoleDictionary[id]) {
					                    if (UserRoleDictionary[id]().length === 0) {
					                        UserRoleDictionary[id](moduleurlarray[id]);
					                    }
					                    else {
					                        var simpleArray = UserRoleDictionary[id]();
					                    }
					                    for (var j = 0; j < moduleurlarray[id].length; j++) {
					                        simpleArray.push(moduleurlarray[id][j])
					                    }
					                    UserRoleDictionary[id](simpleArray);
					                }

					                else {
					                    UserRoleDictionary[modulesId[i]] = ko.observableArray(moduleurlarray[id]);
					                }
					            }
					        }

					        if (observableList().length > 0) {
					            var simpleArray = observableList();
					            for (var j = 0; j < itemsArray.length; j++) {
					                simpleArray.push(itemsArray[j])
					                items[itemsArray[j].id] = itemsArray[j];
					            }
					            observableList(simpleArray);
					        } else {
					            observableList(itemsArray);
					            items = temp;
					        }

					        if (options && options.returnList)
					            return itemsArray;
					        if (options && options.sort)
					            return mapper.getSortedValue(list[list.length - 1]);
					    }
					},

					getItemsArray = function (items) {
					    if (!items) return;

					    var underlyingArray = [];
					    for (var prop in items) {
					        if (items.hasOwnProperty(prop)) {
					            if (!getLocalById(items[prop].id))
					                underlyingArray.push(items[prop]);
					        }
					    }
					    return underlyingArray;
					},

					updateData = function (entity, updateObservableList) {
					    items[entity.id] = entity;

					    if (updateObservableList) {
					        for (var i = 0; i < observableList().length; i++) {
					            if (observableList()[i].id === entity.id) {
					                observableList()[i] = entity;
					                break;
					            }
					        };
					    }

					    return items[entity.id];
					};

			    return {
			        observableList: observableList,
			        mapDtoToContext: mapDtoToContext,
			        add: add,
			        addToDictionary: addToDictionary,
			        getAllLocal: getAllLocal,
			        getLocalById: getLocalById,
			        getAllLocalByIds: getAllLocalByIds,
			        getData: getData,
			        getObservableList: getObservableList,
			        removeById: removeById,
			        removeFromDictionary: removeFromDictionary,
			        fillData: fillData,
			        updateData: updateData,
			        addToObservableList: addToObservableList,
			        getByTickerId: getByTickerId,
			        getByChannelId: getByChannelId,
			        getModuleUrlBymoduleId: getModuleUrlBymoduleId
			    };
			},

			//----------------------------------
			// Repositories
			//----------------------------------

			users = new EntitySet(null, modelmapper.user, null),
		    userRoles = new EntitySet(null, modelmapper.UserRole, null),
            tickers = new EntitySet(null, modelmapper.ticker, null),
            tickerLines = new EntitySet(null, modelmapper.tickerLine, null),
            tickerCategories = new EntitySet(null, modelmapper.tickerCategory, null),
            tickerImages = new EntitySet(null, modelmapper.tickerImage, null),
	        channels = new EntitySet(null, modelmapper.channel, null);

	    var datacontext = {
	        
	        users: users,
	        userRoles: userRoles,
	        tickers: tickers,
	        channels: channels,
	        tickerCategories: tickerCategories,
	        tickerImages: tickerImages
	    };

	    model.setDataContext(datacontext);

	    return datacontext;
	});