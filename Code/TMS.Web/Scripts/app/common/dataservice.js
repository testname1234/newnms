﻿define('dataservice',
    [
        'config',
        'appdata'
    ],
    function (config, appdata) {
        var
            init = function () {
                //#region ticker

                sessionkey = config.sessionKeyName,
                sessionkeyvalue = appdata.sessionKey,

                $.ajaxSetup({
                    headers: { sessionkey: sessionkeyvalue }
                });

                amplify.request.define('login', 'ajax', {
                    url: config.ApiBaseUrl + 'Login',
                    dataType: 'json',
                    type: 'POST'
                });

                amplify.request.define('user-management-login', 'ajax', {
                    url: config.ApiBaseUrl + 'User/LoginMMS/',
                    dataType: 'json',
                    type: 'POST'
                });

                amplify.request.define('load-initial-data', 'ajax', {
                    url: config.ApiBaseUrl + 'Ticker/LoadInitialData',

                    dataType: 'json',
                    type: 'POST'
                });

                amplify.request.define('ticker-polling', 'ajax', {
                    url: config.ApiBaseUrl + 'Ticker/TickerPolling',
                    dataType: 'json',
                    type: 'POST'
                });

                amplify.request.define('insert-TMS-ticker', 'ajax', {
                    url: config.ApiBaseUrl + 'Ticker/InsertTickers',
                    dataType: 'json',
                    type: 'POST'
                });
                amplify.request.define('Update-TMS-VideoStatus', 'ajax', {
                    url: config.ApiBaseUrl + 'Ticker/UpdateVideoStatus',
                    dataType: 'json',
                    type: 'POST'
                });
                amplify.request.define('Get-TMS-TickerSuggestions', 'ajax', {
                    url: config.ApiBaseUrl + 'Ticker/GetTickerSuggestions/',
                    dataType: 'json',
                    type: 'POST'
                });
                amplify.request.define('update-ticker-image-status', 'ajax', {
                    url: config.ApiBaseUrl + 'Ticker/UpdateTickerImageStatus/',
                    dataType: 'json',
                    type: 'POST'
                });
                amplify.request.define('get-more-ticker-images', 'ajax', {
                    url: config.ApiBaseUrl + 'Ticker/GetMoreTickerImages/',
                    dataType: 'json',
                    type: 'POST'
                });
                //#endregion
            },

            login = function (data) {
                return $.Deferred(function (deferred) {
                    amplify.request({
                        resourceId: 'login',
                        data: data,
                        success: deferred.resolve,
                        error: deferred.reject
                    })
                }).promise();
            },
            loadInitialData = function (data) {
                return $.Deferred(function (deferred) {
                    amplify.request({
                        resourceId: 'load-initial-data',
                        data: data,
                        success: deferred.resolve,
                        error: deferred.reject
                    })
                }).promise();
            },
            tickerPolling = function (data) {
                return $.Deferred(function (deferred) {
                    amplify.request({
                        resourceId: 'ticker-polling',
                        data: data,
                        success: deferred.resolve,
                        error: deferred.reject
                    })
                }).promise();
            },
            insertTicker = function (data) {
                return $.Deferred(function (deferred) {
                    amplify.request({
                        resourceId: 'insert-TMS-ticker',
                        data: data,
                        success: deferred.resolve,
                        error: deferred.reject
                    })
                }).promise();
            },
            getMoreTickerImages = function (data) {
                return $.Deferred(function (deferred) {
                    amplify.request({
                        resourceId: 'get-more-ticker-images',
                        data: data,
                        success: deferred.resolve,
                        error: deferred.reject
                    })
                }).promise();
            },
            updateVideoStatus = function (data) {
                return $.Deferred(function (deferred) {
                    amplify.request({
                        resourceId: 'Update-TMS-VideoStatus',
                        data: data,
                        success: deferred.resolve,
                        error: deferred.reject
                    })
                }).promise();
            },
            updateTickerImageStatus = function (data) {
                return $.Deferred(function (deferred) {
                    amplify.request({
                        resourceId: 'update-ticker-image-status',
                        data: data,
                        success: deferred.resolve,
                        error: deferred.reject
                    })
                }).promise();
            },
            getTickerSuggestions = function (data) {
                return $.Deferred(function (deferred) {
                    amplify.request({
                        resourceId: 'Get-TMS-TickerSuggestions',
                        data: data,
                        success: deferred.resolve,
                        error: deferred.reject
                    })
                }).promise();
            },
            usermanagementlogin = function (data) {
                return $.Deferred(function (deferred) {
                    amplify.request({
                        resourceId: 'user-management-login',
                        data: data,
                        success: deferred.resolve,
                        error: deferred.reject
                    });
                }).promise();
            };

        //#endregion

        init();

        return {
            login: login,
            loadInitialData: loadInitialData,
            tickerPolling: tickerPolling,
            usermanagementlogin: usermanagementlogin,
            insertTicker: insertTicker,
            getMoreTickerImages: getMoreTickerImages,
            getTickerSuggestions: getTickerSuggestions,
            updateTickerImageStatus: updateTickerImageStatus
        }
    });