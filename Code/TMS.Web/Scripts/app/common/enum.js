﻿define('enum',
    [],
    function () {
        var
            ProgramDesignerStep = {
                TemplateSelection: 1,
                ContentSelection: 2,
                Rundown: 3,
                Finished: 4,
            },
            UserType = {
                Producer: 5,
                StoryWriter: 21,
                NLE: 20,
                TickerWriter: 49,
                TickerManager: 50,
                TickerProducer: 51,
                HeadlineProducer: 52,
                TickerReporter: 1047
            },
            NewsVerificationStatus = {
                Verified: 79,
                NotVerified: 80,
                Rejected: 81
            },
            TickerStatus = {
                Pending: 5,
                OnHold: 2,
                Rejected: 3,
                Approved: 1,
                OnAired: 6,
                Deleted: 4,
                OnAir: 7,
                Freezed: 8
            },
            TickerType = {
                Breaking: { Value: 1, Text: 'Breaking' },
                Latest: { Value: 2, Text: 'Latest' },
                Category: { Value: 3, Text: 'Category' },
            },
            TickerImageStatus = {
                Pending: 1,
                Skipped: 2,
                Completed: 3

            },
            Severity = {
                Low: 1,
                Medium: 2,
                High: 3
            },
            Frequency = {
                Low: 1,
                Medium: 2,
                High: 3
            },
            Keys = {
                UP: 38,
                LEFT: 37,
                DOWN: 40,
                RIGHT: 39
            };

        return {



            ProgramDesignerStep: ProgramDesignerStep,
            UserType: UserType,
            NewsVerificationStatus: NewsVerificationStatus,
            TickerStatus: TickerStatus,
            TickerType: TickerType,
            Severity: Severity,
            Frequency: Frequency,
            Keys: Keys,
            TickerImageStatus: TickerImageStatus


        }
    });