﻿define('extensions',
    [],
    function () {
        Array.prototype.remove = function (item) {
            var index = this.indexOf(item);
            if (index > -1) {
                this.splice(index, 1);
            }
        }

        Array.prototype.clone = function () {
            var obj = this;
            if (null == obj || "object" != typeof obj) return obj;
            var copy = obj.constructor();
            for (var attr in obj) {
                if (obj.hasOwnProperty(attr)) copy[attr] = obj[attr];
            }
            return copy;
        };
    });