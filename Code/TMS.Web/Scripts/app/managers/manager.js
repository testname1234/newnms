﻿define('manager',
    [
      
        'manager.usermanagement',
        'manager.ticker'
    ],
    function (usermanagement,ticker) {

        return {
            usermanagement: usermanagement,
            ticker: ticker
    }
});