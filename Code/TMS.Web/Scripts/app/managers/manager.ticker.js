﻿define('manager.ticker',
    [
        'jquery',
        'enum',
        'dataservice',
        'datacontext',
        'underscore',
        'utils',
        'appdata',
        'config',
        'model',
        'model.mapper',
        'presenter',
        'router',
        'moment',
        'store'
    ],
    function ($, e, dataservice, dc, _, utils, appdata, config, model, mapper, presenter, router, moment, localStorage) {
        var

            //#region Local Data Processing

            saveDataToLocalStorage = function () { 
                localStorage.save("tickerCategoryLastUpdateDate", appdata.tickerCategoryLastUpdateDate);
                localStorage.save("tickerImageLastUpdateDate", appdata.tickerImageLastUpdateDate);
            },
            saveChannel = function (data) {
                if (data && data.length > 0) {
                    appdata.channelLastUpdateDate = dc.channels.fillData(data, { sort: true });
                }
            },
            saveTickerCategory = function (data) {
                if (data && data.length > 0) {
                    appdata.tickerCategoryLastUpdateDate = dc.tickerCategories.fillData(data, { sort: true });
                }
            },
            saveTickerImages = function (data) {
                if (data && data.length > 0) {
                    appdata.tickerImageLastUpdateDate = dc.tickerImages.fillData(data, { sort: true, groupByChannelId: true });
                }
            },
            getTickerObject = function (ticker, tickerLines, channelId) {
                for (var i = 0, len = tickerLines.length; i < len; i++) {
                    tickerLines[i].ChannelId = channelId;
                }
                var reqObj = {
                    CategoryId: ticker.categoryId(),
                    TickerId: ticker.id,
                    UserId: appdata.currentUser().id,
                    Severity: 1,
                    TickerTypeId: ticker.tickerTypeId(),
                    GroupName: ticker.groupName(),
                    TickerLines: tickerLines
                }
                return reqObj;
            },

            //#endregion

            //#region Server Calls

            loadInitialData = function () {
                var requestObj = {
                    ChannelLastUpdateDateStr: appdata.channelLastUpdateDate,
                    TickerCategoryLastUpdateDateStr: appdata.tickerCategoryLastUpdateDate,
                    TickerImageLastUpdateDateStr: appdata.tickerImageLastUpdateDate,
                    UserId: appdata.currentUser().id
                }

                return $.Deferred(function (d) {
                    $.when(dataservice.loadInitialData(requestObj))
                        .done(function (responseData) {
                            if (responseData && responseData.IsSuccess && responseData.Data) {
                                saveChannel(responseData.Data.Channels);
                                saveTickerCategory(responseData.Data.TickerCategories);
                                saveTickerImages(responseData.Data.TickerImages);

                                saveDataToLocalStorage();

                                d.resolve();
                            } else {
                                if (responseData.Errors && responseData.Errors.length > 0) {
                                    switch (responseData.Errors[0]) {
                                        case 'InvalidSessionKey':
                                            window.location.href = "/login#/index";
                                            break;
                                        default:
                                            break;
                                    }
                                }
                                d.reject();
                            }
                        })
                        .fail(function (data) {
                            d.reject();
                        })
                }).promise();
            },
            insertTicker = function (ticker, tickerLines, channelId) {
                var tickerObj = getTickerObject(ticker, tickerLines, channelId);
                var requestObj = {
                    Tickers: [tickerObj],
                }
                return $.Deferred(function (d) {
                    $.when(dataservice.insertTicker(requestObj))
                        .done(function (responseData) {
                            if (responseData && responseData.IsSuccess && responseData.Data) {
                                d.resolve();
                            }
                        })
                        .fail(function (data) {
                            d.reject();
                        })
                }).promise();
            },
            getMoreTickerImages = function () {
                var requestObj = {
                    UserId: appdata.currentUser().id,
                    ChannelId: appdata.currentChannel().id,
                    PageIndex: dc.tickerImages.getAllLocal().length,
                    PageCount: 30
                };
                return $.Deferred(function (d) {
                    $.when(dataservice.getMoreTickerImages(requestObj))
                        .done(function (responseData) {
                            if (responseData && responseData.IsSuccess && responseData.Data) {
                                saveTickerImages(responseData.Data.TickerImages);

                                d.resolve();
                            }
                        })
                        .fail(function (data) {
                            d.reject();
                        })
                }).promise();
            },
            getTickerSuggestions = function (value, groupSuggestionList) {
                var requestObj = {
                    Keyword: value
                }
                return $.Deferred(function (d) {
                    $.when(dataservice.getTickerSuggestions(requestObj))
                           .done(function (responseData) {

                               if (responseData && responseData.IsSuccess && responseData.Data) {
                                   if (responseData.Data.Tickers) {
                                       var temp = [];
                                       for (var i = 0; i < responseData.Data.Tickers.length; i++) {
                                           var tempObj = new model.Ticker();
                                           tempObj.id = responseData.Data.Tickers[i].TickerId;
                                           tempObj.groupName(responseData.Data.Tickers[i].GroupName);
                                           temp.push(tempObj);
                                       }
                                       groupSuggestionList(temp);
                                   }
                                   d.resolve();
                               }
                           })
                           .fail(function (data) {
                               d.reject();
                           })
                }).promise();
            },
            updateTickerImageStatus = function (tickerImage, statusId) {
                var requestObj = {
                    TickerImageId: tickerImage.id,
                    TickerImageStatusId: statusId
                };
                return $.Deferred(function (d) {
                    $.when(dataservice.updateTickerImageStatus(requestObj))
                     .done(function (responseData) {
                         if (responseData && responseData.IsSuccess && responseData.Data) {
                             d.resolve();
                         }
                     })
                     .fail(function (data) {
                         d.reject();
                     })
                }).promise();
            };

        //#endregion

        return {
            loadInitialData: loadInitialData,
            insertTicker: insertTicker,
            getMoreTickerImages: getMoreTickerImages,
            getTickerSuggestions: getTickerSuggestions,
            updateTickerImageStatus: updateTickerImageStatus
        };
    });