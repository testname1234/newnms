﻿define('mock/mock.dataservice',
    ['amplify'],
    function (amplify) {
        var
            defineApi = function (model) {

            amplify.request.define('login', function (settings) {
                settings.success(model.generateLoginData().loginData);
            });

            amplify.request.define('load-initial-data', function (settings) {
                settings.success(model.generateInitialData().initialData);
            });

            amplify.request.define('refresh-data', function (settings) {
                settings.success(model.refreshDataMock());
            });

            amplify.request.define('mark-attendance', function (settings) {
                settings.success(model.markAttendanceMock());
            });
        };
        
        return {
            defineApi: defineApi
        };
    });
