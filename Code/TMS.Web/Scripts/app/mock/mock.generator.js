﻿define('mock/mock.generator',
    ['jquery', 'moment'],
    function ($, moment) {
        var
            init = function () {
                //$.mockJSON.random = true;
                //$.mockJSON.log = false;
                //$.mockJSON.data.USER_FIRST_NAME = ['John', 'Dan', 'Scott', 'Hans', 'Ward', 'Jim', 'Ryan', 'Steve', 'Ella', 'Landon', 'Haley', 'Madelyn'];
                //$.mockJSON.data.USER_LAST_NAME = ['Papa', 'Wahlin', 'Guthrie', 'Fjällemark', 'Bell', 'Cowart', 'Niemeyer', 'Sanderson'];
                //$.mockJSON.data.IMAGE_SOURCE = ['john_papa.jpg', 'dan_wahlin.jpg', 'scott_guthrie.jpg', 'hans_fjallemark.jpg', 'ward_bell.jpg', 'jim_cowart.jpg', 'ryan_niemeyer.jpg', 'steve_sanderson.jpg'];
                //$.mockJSON.data.DATE_TODAY = [moment().format('MMMM DD YYYY')];
                //$.mockJSON.data.DATE_FULL = [new Date()];
                //$.mockJSON.data.TAG = ['JavaScript', 'Knockout', 'MVVM', 'HTML5', 'Keynote', 'SQL', 'CSS', 'Metro', 'UX'];
                //$.mockJSON.data.TRACK = ['Windows 8', 'JavaScript', 'ASP.NET', '.NET', 'Data', 'Mobile', 'Cloud', 'Practices', 'Design'];
                //$.mockJSON.data.TITLE = [
                //    'Building HTML/JavaScript Apps with Knockout and MVVM',
                //    'JsRender Fundamentals',
                //    'Introduction to Building Windows 8 Metro Applications',
                //    'Building ASP.NET MVC Apps with EF Code First, HTML5, and jQuery',
                //    'jQuery Fundamentals',
                //    'jQuery Tips and Tricks',
                //    'JavaScript for .NET Developers',
                //    'jQuery Mobile',
                //    'Bootstrap',
                //    'Responsive Web Design',
                //    'Structuring JavaScript Code',
                //    'Keynote'
                //];
                //$.mockJSON.data.LEVEL = ["Beginner", "Intermediate", "Advanced"];
                //$.mockJSON.data.TWITTER = ['john_papa', 'danwahlin', 'ifthenelse', 'scottgu', 'wardbell'];
                //$.mockJSON.data.URL = ['http://www.johnpapa.net', 'http://www.pluralsight.com'];
                //$.mockJSON.data.GENDER = ['F', 'M'];
                //$.mockJSON.data.RATING = [1, 2, 3, 4, 5];
            },

            generateUsers = function () {
                var data = $.mockJSON.generateFromTemplate({
                    'Users|100-120': [{
                        'id|+1': 1,
                        name: '@USER_FIRST_NAME' + ' @USER_LAST_NAME',
                        designationId: 1,
                        category: 1,
                        'seatingZoneId|1-10': 1,
                        'diningZoneId|1-15': 1,
                        'companions|0-2': 0,
                        attendanceStatus: 1,
                        'couponNumber|1-100': 1,
                        roleId: 1,
                        username: 'test',
                        password: 'test'
                    }]
                });
                // Hard code first one to SPA
                data.user[0].id = 1;
                data.user[0].name = 'Asim Khan';
                data.user[0].designationId = 1,
                data.user[0].category = 'guest',
                data.user[0].seatingZoneId = 1,
                data.user[0].diningZoneId,
                data.user[0].accompanies,
                data.user[0].attendanceStatus,
                data.user[0].couponNumber,
                data.user[0].roleId,
                data.user[0].username,
                data.user[0].password

                return data;
            },

            generateLoginData = function () {
                var data = {};
                return data;
            },

            generateInitialData = function () {
                var data = {};
                return data;
            },

            refreshDataMock = function () {
                var data = {};
                return data;
            },

            markAttendanceMock = function () {
                var data = {};
                data.IsSuccess = true;
                return data;
            }

        init();

        return {
            model: {
                generateUsers: generateUsers,
                generateLoginData: generateLoginData,
                generateInitialData: generateInitialData,
                refreshDataMock: refreshDataMock,
                markAttendanceMock: markAttendanceMock
            }
        };
    });
