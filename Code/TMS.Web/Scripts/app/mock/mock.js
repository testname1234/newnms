﻿define('mock/mock',
    [
    'mock/mock.generator',
    'mock/mock.dataservice'
    ],
    function (generator, dataservice) {
        var
            model = generator.model,
            
            dataserviceInit = function () {
                dataservice.defineApi(model);
            };

    return {
        dataserviceInit: dataserviceInit    
    };
});