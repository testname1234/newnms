﻿define('model.channel',
    ['ko', 'utils', 'enum', 'moment', 'appdata'],
    function (ko, utils, e, moment, appdata) {

        var
            _dc = this,
      
            Channel = function () {

                var self = this;

                self.id,
                self.name,
                self.creationDate = ko.observable(),
                self.lastUpdateDate = ko.observable(),

                self.isNullo = false;

                return self;
            }

        Channel.Nullo = new Channel();

        Channel.Nullo.isNullo = true;

        Channel.datacontext = function (dc) {
            if (dc) { _dc = dc; }
            return _dc;
        };

        return Channel;
    });