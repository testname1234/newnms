﻿define('model',
    [

        'model.user',
        'model.userrole',
        'model.ticker',
        'model.tickerline',
        'model.tickerreportercontrol',
        'model.channel',
        'model.tickercategory',
        'model.tickervideo',
        'model.tickerimage'
    ],
    function (
        User,
        UserRole,
        Ticker,
        TickerLine,
        TickerReporterControl,
        Channel,
        TickerCategory,
        TickerVideo,
        TickerImage) {
        var
            model = {
                User: User,
                UserRole: UserRole,
                Ticker: Ticker,
                TickerLine: TickerLine,
                TickerReporterControl: TickerReporterControl,
                Channel: Channel,
                TickerCategory: TickerCategory,
                TickerVideo: TickerVideo,
                TickerImage: TickerImage
            };

        model.setDataContext = function (dc) {
            model.User.datacontext(dc);
            model.UserRole.datacontext(dc);
            model.Ticker.datacontext(dc);
            model.TickerLine.datacontext(dc);
            model.TickerReporterControl.datacontext(dc);
            model.Channel.datacontext(dc);
            model.TickerCategory.datacontext(dc);
            model.TickerVideo.datacontext(dc);
            model.TickerImage.datacontext(dc);
        };

        return model;
    });