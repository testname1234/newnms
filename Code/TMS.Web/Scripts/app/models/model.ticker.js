﻿define('model.ticker',
    ['ko', 'utils', 'enum', 'moment', 'appdata'],
    function (ko, utils, e, moment, appdata) {

        var
            _dc = this,

            Ticker = function () {

                var self = this;

                self.id=0,
                self.groupName = ko.observable(),
                self.severity = ko.observable(1),
                self.tickerTypeId = ko.observable(),
                self.categoryId = ko.observable(),
                self.userId = ko.observable(appdata.currentUser().id),
                self.lastUpdateDate = ko.observable(),
                self.creationDate = ko.observable(),

                self.creationDateDisplay = ko.computed({
                    read: function () {
                        return moment(self.creationDate()).fromNow();
                    },
                    deferEvaluation: true
                }),
                self.lastUpdateDateDisplay = ko.computed({
                    read: function () {

                        return moment(self.lastUpdateDate()).fromNow();
                    },
                    deferEvaluation: true
                }),
                self.tickerLines = ko.computed({
                    read: function () {
                        var arr = [];
                        appdata.arrangeTickerFlag();

                        arr = _.filter(_dc.tickerLines.getObservableList(), function (ticker) {
                            if (ticker.tickerId == self.id) {
                                return ticker;
                            }
                        });
                        return arr;
                    },
                    write: function (value) {

                    },
                    deferEvaluation: true
                }),

                self.userName = ko.computed({
                    read: function () {
                        return appdata.currentUser().name;
                    },
                    deferEvaluation: true
                }),

                self.categoryName = ko.computed({
                    read: function () {
                        var category = _dc.categories.getLocalById(self.categoryId);
                        var tempName;
                        if (category) {
                            tempName = category.name;
                        }
                        if (!tempName) {
                            var filters = _dc.filters.getObservableList();
                            for (var i = 0, len = filters.length; i < len; i++) {
                                if (filters[i].filterTypeId === e.NewsFilterType.Category && filters[i].value === self.categoryId) {
                                    tempName = filters[i].name;
                                    break;
                                }
                            }
                        }
                        return tempName;
                    },
                    deferEvaluation: true
                });
                
                self.isNullo = false;

                return self;
            }

        Ticker.Nullo = new Ticker();

        Ticker.Nullo.isNullo = true;

        Ticker.datacontext = function (dc) {
            if (dc) { _dc = dc; }
            return _dc;
        };

        return Ticker;
    });