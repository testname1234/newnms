﻿define('model.tickercategory',
    ['ko', 'utils', 'enum', 'moment', 'appdata'],
    function (ko, utils, e, moment, appdata) {

        var
            _dc = this,
      
            TickerCategory = function () {

                var self = this;
                self.id,
                self.categoryId,
                self.name,
                self.creationDate = ko.observable(),
                self.lastUpdateDate = ko.observable(),

                self.isNullo = false;

                return self;
            }

        TickerCategory.Nullo = new TickerCategory();

        TickerCategory.Nullo.isNullo = true;

        TickerCategory.datacontext = function (dc) {
            if (dc) { _dc = dc; }
            return _dc;
        };

        return TickerCategory;
    });