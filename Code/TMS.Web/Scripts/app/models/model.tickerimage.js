﻿define('model.tickerimage',
    ['ko'],
    function (ko) {

        var
            _dc = this,

            TickerImage = function () {

                var self = this;

                self.id,
                self.channelId,
                self.groupId,
                self.tickerImageUrl,
                self.statusId = ko.observable(),

                self.isNullo = false;

                return self;
            }

        TickerImage.Nullo = new TickerImage();

        TickerImage.Nullo.isNullo = true;

        TickerImage.datacontext = function (dc) {
            if (dc) { _dc = dc; }
            return _dc;
        };

        return TickerImage;
    });