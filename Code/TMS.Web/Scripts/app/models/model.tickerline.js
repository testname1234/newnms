﻿define('model.tickerline',
    ['ko', 'enum'],
    function (ko, e) {

        var
            _dc = this,

            TickerLine = function () {

                var self = this;

                self.id,
                self.title = ko.observable(),
                self.languageCode = ko.observable(),
                self.tickerId,
                self.sequenceId,
                self.channelId,
                self.creationDate = ko.observable(),
                self.lastUpdateDate = ko.observable(),
                //self.onAirTimeStr = ko.observable(),

                self.isNullo = false;

                return self;
            }

        TickerLine.Nullo = new TickerLine();

        TickerLine.Nullo.isNullo = true;

        TickerLine.datacontext = function (dc) {
            if (dc) { _dc = dc; }
            return _dc;
        };

        return TickerLine;
    });