﻿define('model.tickerreportercontrol',
    ['ko', 'model.ticker', 'model.tickerline', 'model.tickerimage', 'appdata', 'enum', 'string'],
    function (ko, Ticker, TickerLine, TickerImage, appdata, e, S) {

        var
            _dc = this,

            TickerReporterControl = function () {

                var self = this;

                self.tickersCount = ko.observable(0),
                self.currentTicker = ko.observable(new Ticker()),
                self.currentTickerImage = ko.observable(new TickerImage()),
                self.currentTickerImageNext = ko.observable(new TickerImage()),
                self.currentTickerImagePrev = ko.observable(new TickerImage()),
                self.isReset = ko.observable(false),
                self.tickerLineList = ko.observableArray([]),
                self.groupSuggestionList = ko.observableArray([]),

                self.addOrDeleteFromTickerList = function (data, isDeleted) {

                    if (isDeleted) {
                        for (var i = 0; i < self.tickerLineList().length; i++) {
                            if (data.children()[1].value === self.tickerLineList()[i].Text) {
                                self.tickerLineList.remove(self.tickerLineList()[i]);
                            }
                        }
                    }
                    else {
                        if (data.children()[1].value.trim()) {

                            //var tempTime = moment(currentVideo.startTime).add(currentVideoObject.currentTime, 'seconds');
                            //tempTime = tempTime.toISOString();
                            self.tickerLineList.push({ Text: data.children()[1].value, LanguageCode: "ur", SequenceId: self.tickerLineList().length == 0 ? 1 : self.tickerLineList().length + 1 });
                        }
                    }
                },
                self.setGroupName = function (data) {
                    self.currentTicker().groupName(data);
                },
                self.setCurrentTickerImage = function (current,next,prev) {
                    self.currentTickerImage(current);
                    self.currentTickerImageNext(next);
                    self.currentTickerImagePrev(prev);
                },

                self.resetControl = function () {
                    self.tickersCount(0);
                    self.currentTicker(new Ticker());
                    self.tickerLineList([]);
                    self.isReset(false);

                };

                self.isNullo = false;

                return self;
            }

        TickerReporterControl.Nullo = new TickerReporterControl();

        TickerReporterControl.Nullo.isNullo = true;

        TickerReporterControl.datacontext = function (dc) {
            if (dc) { _dc = dc; }
            return _dc;
        };

        return TickerReporterControl;
    });