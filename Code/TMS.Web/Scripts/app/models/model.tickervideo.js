﻿define('model.tickervideo',
    ['ko', 'utils', 'enum', 'moment', 'appdata'],
    function (ko, utils, e, moment, appdata) {

        var
            _dc = this,
      
            TickerVideo = function () {

                var self = this;
                self.id,
                self.url,
                self.startTime,
                self.endTime,
                self.channelId = ko.observable(),
                self.videoStatusId = ko.observable(),
                self.creationDate = ko.observable(),
                self.lastUpdateDate = ko.observable(),

                self.isNullo = false;

                return self;
            }

        TickerVideo.Nullo = new TickerVideo();

        TickerVideo.Nullo.isNullo = true;

        TickerVideo.datacontext = function (dc) {
            if (dc) { _dc = dc; }
            return _dc;
        };

        return TickerVideo;
    });