﻿define('model.userrole',
    ['ko', 'jquery', 'enum', 'moment', 'extensions', 'underscore', 'config'],
    function (ko, $, e, moment, ext, _, config) {

        var
            _dc = this,

            UserRole = function () {

                var self = this;

                self.moduleId,
                self.moduleUrl,
                self.workRoleId,
                self.userId,
                self.sessionkey,
                self.uniqueId,
                self.moduleAbbriviation,
                self.workRoleName,
                self.urlName,
                self.fullName
               
                self.isNullo = false;

                return self;
            }

        UserRole.Nullo = new UserRole();

        UserRole.Nullo.isNullo = true;

        UserRole.datacontext = function (dc) {
            if (dc) { _dc = dc; }
            return _dc;
        };

        return UserRole;
    });