﻿(function () {
    var root = this;

    define3rdPartyModules();
    loadPluginsAndBoot();

    function define3rdPartyModules() {
        define('jquery', [], function () { return root.jQuery; });
        define('ko', [], function () { return root.ko; });
        define('amplify', [], function () { return root.amplify; });
        define('infuser', [], function () { return root.infuser; });
        define('moment', [], function () { return root.moment; });
        define('sammy', [], function () { return root.Sammy; });
        define('toastr', [], function () { return root.toastr; });
        define('underscore', [], function () { return root._; });
        define('string', [], function () { return root.S; });
    }

    function loadPluginsAndBoot() {
        requirejs([
				'ko.bindingHandlers',
				'ko.debug.helpers'
        ], boot);
    }

    function boot() {
        require(['bootstrapper'], function (bs) { bs.run(); });
    }
})();
/*----------------------*/
String.format = function () {
    var s = arguments[0];
    for (var i = 0; i < arguments.length - 1; i++) {
        var reg = new RegExp("\\{" + i + "\\}", "gm");
        s = s.replace(reg, arguments[i + 1]);
    }

    return s;
}

$(function () {
    if (window.location.hash === "#/home") {
        $('body').addClass('scrollpadding');
    }
    $(".tabSec").each(function () {
        $(this).find('[data-src]').parent().first().addClass("newsActive");
        var frstTab = $(this).find('[data-src]').data("src");
        $(frstTab).addClass("newsActive").siblings().hide().removeClass("newsActive");
        $('body').delegate('[data-src]', 'click', function (e) {
            var slctd = $(this).parent(), tgt = $(this).data("src");
            slctd.addClass("newsActive").siblings().removeClass("newsActive");
            //$(tgt).siblings().hide().removeClass("newsActive");
            //$(tgt).fadeIn(300).addClass("newsActive");
            e.preventDefault();
        });
    });
  
    $('.help').live('click', function () {
        $('.viewHelp').click();
    });
    $('.news-grid').hide();
    $('.icon-gridBox').live('click', function () {

        $('.icon-refreshed').removeClass('active');
        $(this).addClass('active');
        $(this).parents('.contentSection').find('.latestNews').hide();
        $(this).parents('.contentSection').find('.news-grid').slideDown('fast');
    });

    $('.icon-refreshed').live('click', function () {
        $('.icon-gridBox').removeClass('active');
        $(this).addClass('active');
        $(this).parents('.contentSection').find('.news-grid').hide();
        $(this).parents('.contentSection').find('.latestNews').slideDown('fast');
    });

    $('.news-grid ul li .newsStories').hide();
    $('.news-grid ul li').live('mouseover', function (e) {
        if ($(e.target).parent().is('figure')) {
            $(this).find('.news-caption').hide();
            $(this).find('.newsStories').show();
        }
    });
    $('.news-grid ul li .newsStories').live('mouseleave', function () {
        $(this).hide();
        $(this).siblings('.news-caption').show();
    });

    $('.leftnav .controller').live('click', function () {
        $(this).siblings().find('.nav-step1').toggleClass('active', 500, 'linear');
    });

    $('html').live('click', function () {
        $('header .topnav > li').siblings().children('ul').hide();
        $('header .topnav > li').siblings().children('div').hide();
        $(".slideDown li").find('ul').stop(true, true).slideUp('fast');
        //$('.storiesColume').removeClass('active');
        //$('.storiesIcons').removeClass('active');
        //$('.storiesForm').removeClass('active');

        //$('.leftnav .controller').siblings().find('.nav-step1').removeClass('active');
        resetCount = 0;
    });

    $('header .topnav > li').live('click', function (event) {
        event.stopPropagation();

        $('header .topnav > li').siblings().children('ul, div').hide();
        $(this).children('ul').show();
        if ($(this).hasClass('alerts')) {
            $(this).parents().siblings().children('> ul').hide();
            $(this).children('div').show();
            $('header .topnav > li.alerts > div > ul').scrollTop(1);

            if ($('.topnav').children('.alerts').children('.dropdown').hasClass('showSlide')) {
                $('.topnav').children('.alerts').children('.dropdown').removeClass('showSlide')
                $('.topnav').children('.alerts').children('.dropdown').css({
                    'display': 'none'
                });
            }
            else {
                $('.topnav').children('.alerts').children('.dropdown').addClass('showSlide')
                $('.topnav').children('.alerts').children('.dropdown').css({
                    'display': 'block'
                });
            }
        }
    });

    $('.ratColume').contents().filter(function () {
        return this.nodeType === 3;
    }).remove();

    $('.audio, .video').append('<span></span>');

    var resetvar2 = 0;
    $(".viewAll").live('click', function () {
        if (resetvar2 == 0) {
            $(this).parents('.galleryView').addClass('active').stop(true, true).animate({ left: '0px' }, 200, 'linear');
            $(this).parents('.controlerView').removeClass('overlay');
            resetvar2 = 1;
        } else {
            $(this).parents('.galleryView').removeClass('active').stop(true, true).animate({ left: '-142px' }, 200, 'linear');
            $(this).parents('.controlerView').addClass('overlay');
            resetvar2 = 0;
        }
    });


    $(".galleryView a.addMore").live('click', function () {
        var lnth = parseInt($('.galleryView ul.scrollBar > li:first-child img').attr('alt')) - 1,
			addhtml = $('.galleryView ul.scrollBar > li:first-child').html();
        console.log(lnth, addhtml);
        if (lnth >= 1) {
            addhtml = addhtml.replace('newsImages' + (lnth + 1) + '', 'newsImages' + lnth);
            addhtml = addhtml.replace('alt="' + (lnth + 1) + '"', 'alt="' + (lnth) + '"');
            $(this).parent().find('.scrollBar').prepend('<li>' + addhtml + '</li>');
        }
    });

    $(".meidaFilesDtls").live('click', function () {
        if ($(this).parents('li').hasClass("active")) {
            $(".meidaFilesDtls").parents('li').removeClass('active');
            $(this).parents(".mediaFiles").find(".mediaDtls").slideUp(300);
        }
        else {
            $(this).parents('li').addClass('active');
            $(this).parents(".mediaFiles").find(".mediaDtls").slideDown(300);
        }
    });

    $(".donwloads").live('click', function () {
        $(".meidaFilesDtls").click();
    });

    // RATED MENU SCRIPT
    $("ul.ratedMenu").hide();

    $('.slideDown li').live('mouseover', function () {
        $(this).find('ul').stop(0, 0).slideDown('fast');
        //$(this).find('ul').stop(0,0).slideDown('fast');
    });
    $('.slideDown li').live('mouseout', function () {
        $(this).find('ul').stop(0, 0).slideUp('fast');
    });

    // LEFT MENU SCRIPT
    $("ul.nav-step1 li").live('mouseover', function () {
        $(this).find('ul').parent('li').addClass("active");
        setTimeout(function () {
            $(this).find('ul').stop(true, true).animate({ left: '50px' }, 200, 'linear');
        }, 1000);
    });
    $("ul.nav-step1 li").live('mouseleave', function () {
        $(this).find('ul').parent('li').removeClass("active");
        $(this).find('ul').stop(true, true).animate({ left: '-185px' }, 200, 'linear');
    });

    // INNER STORIES MENU SCRIPT
    //$(".allStories .icon-allStories").live('click', function () {
    //    var $this = $(this);
    //    $this.parents('.storiesColume').stop(true, true).animate({ width: '300px' }, 200, 'linear');
    //    $this.parents('.allStories').find('.storiesIcons').stop(true, true).animate({ 'right': 0, }, 300, 'linear');
    //});
    //$(".allStories .sep").live('click', function () {
    //    $(this).parents('.storiesColume').stop(true, true).animate({ width: '30px' }, 200, 'linear');
    //    $(this).parents('.allStories').find('.storiesIcons').stop(true, true).animate({ 'right': -195, }, 300, 'linear');
    //});
    //$('.storiesColume').live('click', function () {
    //    $(this).toggleClass('active');
    //    $(this).children('.storiesIcons').toggleClass('active');
    //    $(this).parent().children('.storiesForm').toggleClass('active');
    //});
    $('.allStories').live('click', function () {
        $(this).parent().children('.storiesIcons').toggleClass('active');
        $(this).parent().find('.storiesForm').toggleClass('active');
    });
    //$(".allStories").live('mouseover', function () {
    //    $(this).find('.storiesForm').stop(true, true).fadeIn();
    //});
    //$(".allStories").live('mouseleave', function () {
    //    $(".allStories .storiesIcons").stop(true, true).animate({ 'right': -195, }, 300, 'linear');
    //    $(this).find('.storiesForm').stop(true, true).fadeOut();
    //});

    //$(".icon, .icon-correct").live('click',function () {
    //	$(this).toggleClass('active');
    //});
    //$('.news-alert .cross-icon').click(function () {
    //    $(this).parent('.news-alert').animate({ right: '-385px' }, 200, 'linear');
    //});

    //setTimeout(function () {
    //    var asideht = $('.asideExplosives').outerHeight();
    //    $('#topic-selection-view .reportedSec').height(asideht);
    //}, 3000);

    setTimeout(function () {
        $('.topicSection2 > .newsColume:last-child > li').slice(-5).addClass('bottom');
    }, 4000);

    $('.rightSec ul.timeSec > li').click(function () {

        $(this).siblings().find('.innerpop').hide();
        if ($(this).hasClass('dropdownlist')) {

            if ($(this).find('ul').is(':visible')) {
                $(this).find('ul').hide();
            }
            else {
                $(this).find('ul').show();
            }
        } else if ($(this).hasClass('datepicshow')) {

            if ($(this).find('span#datepickerhome').is(':visible')) {
                $(this).find('span#datepickerhome').hide();
            }
            else {
                $('.episodeDDL').hide();
                $(this).find('span#datepickerhome').show();
            }

        }
    });
    $('.datepicshow').live('click', function () {
        if ($(this).find('ul').is(':visible')) {
            $(this).find('ul').hide();
            $(this).find('.datepickerBar').slideUp('slow');
        }
        else {
            $(this).find('ul').show();
            $(this).find('.datepickerBar').slideDown('slow');
        }
    });

    $('#templateMap').live('mouseover', function () {
        $('.overlay').show();
    });
    $('#templateMap').live('mouseout', function () {
        $('.overlay').hide();
    });


    $("#trash").sortable({
        connectWith: "#sequence-news-list"
    });


    $(window).load(function () {
        // Custom Scrollbar

        $(".newsNav > ul > li.showpics > ul").mCustomScrollbar({
            autoDraggerLength: true,
            autoHideScrollbar: false,
            scrollInertia: 800,
            advanced: {
                updateOnBrowserResize: true,
                updateOnContentResize: true
            }
        });

        $(".latestCol").mCustomScrollbar({
            autoDraggerLength: true,
            autoHideScrollbar: true,
            scrollInertia: 800,
            advanced: {
                updateOnBrowserResize: true,
                updateOnContentResize: true
            }
        });

    });


    function succeed(heading) {
        $('body').append("<div class='topbarSuccess'>" + heading + " <strong>Success Fully Added</strong></div>");
        $('.topbarSuccess').fadeIn().delay(2000).fadeOut();
    }

    $(window).resize(function () {
        resolutionfix();
    });
    setTimeout(function myfunction() {
        resolutionfix();
    }, 1000);
    function resolutionfix() {
        var winht = $(window).height();
        var navmenuht = $('.singlebar-menu').outerHeight();
        var rsSec = $('.rightSec ul.timeSec').outerHeight();

        var ht = winht - navmenuht - 30;

        $('.main-content').height(ht);
        $('.main-content.tickerspage').height(ht);

        if ($('body').hasClass('fullslidepage')) {
            $('.latestCol').height(winht - navmenuht - $('.newsNav').outerHeight() - rsSec - 20);
            $('.latestCol ul').height(winht - navmenuht - $('.newsNav').outerHeight() - rsSec - 50);
        } else {

            $('.leftnav').height(winht - navmenuht - 25);
            $('.leftnav .leftnavscroll').css("max-height", $('.leftnav').height() - 20);
            $('.latestCol').height(winht - navmenuht - 25 - rsSec);
            $('.latestCol ul').height($('.latestCol').height() - 70);
        }
    }

});
$(document).ready(function () {
    $(".showToolTip li").tooltip();
});