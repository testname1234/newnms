﻿using NMS.Core;
using NMS.Core.IService;
using NMS.ProcessThreads;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                INewsService newsService = IoC.Resolve<INewsService>("NewsService");


                if (ConfigurationManager.AppSettings["IsImport"] == "0")
                {
                    #region Scrap News Thread

                    string JsonArgument = "{ \"InputFile\":\"Templates/dawntemplate.xml\",\"OutputFile\":\"DawnNews\"}";
                    ScrapNewsThread scrapNewsThread = new ScrapNewsThread();
                    scrapNewsThread.Execute(JsonArgument);

                    #endregion
                }
                else
                {

                    #region Import News Thread

                    string JsonArgmentImport = "";
                    ImportNewsGeneralThread importGeneralNewsThread = new ImportNewsGeneralThread();
                    importGeneralNewsThread.Execute(JsonArgmentImport);

                    #endregion
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine(ex.StackTrace);
            }

        }
    }
}