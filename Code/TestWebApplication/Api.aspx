﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Api.aspx.cs" Inherits="TestWebApplication.Api" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table class="auto-style1">
            <tr>
                <td></td>
                <td>Api Key <asp:TextBox id="txtApiKey" runat="server"></asp:TextBox> </td>
                <td>Bucket Id <asp:TextBox id="txtBucketId" runat="server"></asp:TextBox> </td>
            </tr>
              <tr>
                <td></td>
                <td>&nbsp;</td>
            </tr>
             <tr>
                <td>Get Resource BY Date</td>
                <td><asp:button  ID="btnGetResource" runat="server" Text="Get Resource BY Date" OnClick="btnGetResource_Click"/></td>
            </tr>
            <tr>
                <td>Create SubBucket</td>
                <td>Name : <asp:TextBox id="txtBucketName" runat="server"></asp:TextBox> <asp:button  ID="btnBucket" runat="server" Text="Create SubBucket" OnClick="btnBucket_Click"/></td>
            </tr>
            <tr>
                <td></td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>DeleteSubBucket</td>
                <td>Name : <asp:TextBox id="txtBucketNameforDelete" runat="server"></asp:TextBox> <asp:button  ID="btnDeleteBucket" runat="server" Text="Delete SubBucket" OnClick="btnDeleteBucket_Click"/></td>
            </tr>
              <tr>
                <td></td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>GetAllSubBucket</td>
                <td> <asp:button  ID="btnGetAllSubBucket" runat="server" Text="GetAll SubBucket" OnClick="btnGetAllSubBucket_Click"/></td>
            </tr>
              <tr>
                <td></td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>GetSubBucket</td>
                 <td>Name : <asp:TextBox id="txtGetSubBucket" runat="server"></asp:TextBox> <asp:button  ID="btnGetSubBucket" runat="server" Text="Get SubBucket" OnClick="btnGetSubBucket_Click"/></td>
            </tr>
              <tr>
                <td></td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>GetAllFiles</td>
               <td>Name : <asp:TextBox id="txtGetAllFiles" runat="server"></asp:TextBox> <asp:button  ID="btntxtGetAllFiles" runat="server" Text="Get All Files" OnClick="btntxtGetAllFiles_Click"/></td>
            </tr>
              <tr>
                <td></td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>GetResourcesByResourceTypeId</td>
                  <td>Name : <asp:TextBox id="txtGetResourcebyTypeId" runat="server"></asp:TextBox> <asp:button  ID="btnGetResourcebyType" runat="server" Text="Get Resource By Type" OnClick="btnGetResourcebyType_Click"/></td>
            </tr>
              <tr>
                <td></td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>GetResource</td>
                 <td> <img id="imgGuid" runat="server" />  Guid : <asp:TextBox id="txtGetResourceByGuid" runat="server"></asp:TextBox>    <asp:button  ID="btnGetResourceByGuid" runat="server" Text="Get Resource By Guid" OnClick="btnGetResourceByGuid_Click"/>  
                  <img id="img" runat="server" />   File Path : <asp:TextBox id="txtGerResourceByFilePath" runat="server"></asp:TextBox>  <asp:button  ID="btnGetResourceByFilePath" runat="server" Text="Get Resource By File Path" OnClick="btnGetResourceByFilePath_Click"/>
                 </td>
            </tr>
              <tr>
                <td></td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>PostMedia</td>
                <td>    
                         Path :  <asp:TextBox ID="txtPath" runat="server"></asp:TextBox>
                         <asp:FileUpload  ID="fileupload" runat="server"/>
                         <asp:Button  ID="btmSubmit" runat="server" Text="upload" OnClick="btmSubmit_Click" /></td>
            </tr>
              <tr>
                <td>Post Resource</td>
                <td> Source :  <asp:TextBox ID="txtSource" runat="server"></asp:TextBox>
                         <asp:FileUpload  ID="fileupload1" runat="server"/>
                    <asp:Button  ID="btnPostResource" runat="server" Text="post resource" OnClick="btnPostResource_Click" /></td>
                </td>
            </tr>
            <tr>
                <td>PostMedia GUID</td>
                <td>GUID :  <asp:TextBox ID="txtGuid" runat="server"></asp:TextBox>
                    <asp:Button  ID="btnPostResourceGuid" runat="server" Text="postbyGuid" OnClick="btnPostResourceGuid_Click" /></td>
                </td>
            </tr>
            <tr>
                <td>Result</td>
                <td>&nbsp;</td>
            </tr>
              <tr>
                <td></td>
                <td><asp:Label ID="lbError" runat="server"></asp:Label></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
              <tr>
                <td></td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
