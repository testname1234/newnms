﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;
using MS.Core.Entites;
using MS.Core.Entities;
using MS.MediaIntegration;
using MS.MediaIntegration.API;

namespace TestWebApplication
{
    public partial class Api : System.Web.UI.Page
    {
        MSApi api = new MSApi();
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnBucket_Click(object sender, EventArgs e)
        {
           
            DataTransfer<bool> createsubbucket =  api.CreateSubBucket(Convert.ToInt32(txtBucketId.Text.Trim()), txtApiKey.Text.Trim(), txtBucketName.Text.Trim());
            JavaScriptSerializer serializer  = new JavaScriptSerializer();

            lbError.Text = serializer.Serialize(createsubbucket).ToString();

        }

        protected void btnDeleteBucket_Click(object sender, EventArgs e)
        {
           
            DataTransfer<bool> deletesubbucket = api.DeleteSubBucket(Convert.ToInt32(txtBucketId.Text.Trim()), txtApiKey.Text.Trim(), txtBucketNameforDelete.Text.Trim());
            JavaScriptSerializer serializer = new JavaScriptSerializer();

            lbError.Text = serializer.Serialize(deletesubbucket).ToString();
        }

        protected void btnGetAllSubBucket_Click(object sender, EventArgs e)
        {
          
            DataTransfer<List<Bucket>> allsubbucket = api.GetAllSubBucket(Convert.ToInt32(txtBucketId.Text.Trim()), txtApiKey.Text.Trim());
            JavaScriptSerializer serializer = new JavaScriptSerializer();

            lbError.Text = serializer.Serialize(allsubbucket).ToString();
        }

        protected void btnGetSubBucket_Click(object sender, EventArgs e)
        {

           
            DataTransfer<bool> getsubbucket = api.GetSubBucket(Convert.ToInt32(txtBucketId.Text.Trim()), txtApiKey.Text.Trim(), txtGetSubBucket.Text.Trim());
            JavaScriptSerializer serializer = new JavaScriptSerializer();

            lbError.Text = serializer.Serialize(getsubbucket).ToString();
        }

        protected void btntxtGetAllFiles_Click(object sender, EventArgs e)
        {

            DataTransfer<List<MediaFile>> getAllFiles = api.GetAllFiles(Convert.ToInt32(txtBucketId.Text.Trim()), txtGetAllFiles.Text.Trim());
            JavaScriptSerializer serializer = new JavaScriptSerializer();

            lbError.Text = serializer.Serialize(getAllFiles).ToString();
        }

        protected void btnGetResourcebyType_Click(object sender, EventArgs e)
        {
            DataTransfer<List<Resource>> getAllFiles = api.GetResourcesByResourceTypeId(Convert.ToInt32(txtGetResourcebyTypeId.Text.Trim()));
            JavaScriptSerializer serializer = new JavaScriptSerializer();

            lbError.Text = serializer.Serialize(getAllFiles).ToString();
        }

        protected void btnGetResourceByGuid_Click(object sender, EventArgs e)
        {
            imgGuid.Src = "http://localhost:21642/Api/Bucket/GetResource?id=" + txtGetResourceByGuid.Text;
        }

        protected void btnGetResourceByFilePath_Click(object sender, EventArgs e)
        {
           // HttpResponseMessage getAllFiles = api.GetResource(txtApiKey.Text.Trim(), Convert.ToInt32(txtBucketId.Text.Trim()), txtGerResourceByFilePath.Text);
            //JavaScriptSerializer serializer = new JavaScriptSerializer();
    
            img.Src = "http://localhost:21642/Api/Bucket/GetResource" + string.Format("?ApiKey={0}&BucketId={1}&FilePath={2}", txtApiKey.Text.Trim(), txtBucketId.Text.Trim(), txtGerResourceByFilePath.Text);
            //lbError.Text = serializer.Serialize(getAllFiles).ToString();
        }

        protected void btmSubmit_Click(object sender, EventArgs e)
        {
            if (fileupload.HasFile)
            {
                //HttpStatusCode code = api.PostMedia("kG:*:aJcbv", 4,@""+txtPath.Text + fileupload.FileName, fileupload.FileName, fileupload.FileBytes, fileupload.PostedFile.ContentType);
                HttpStatusCode code = api.PostMedia(txtApiKey.Text, Convert.ToInt32(txtBucketId.Text), @""+txtPath.Text+fileupload.FileName, fileupload.FileName, fileupload.FileBytes, fileupload.PostedFile.ContentType);
            }
        }

        protected void btnPostResource_Click(object sender, EventArgs e)
        {
            if (fileupload1.HasFile)
            {
                MS.Core.DataTransfer.Resource.PostInput input = new MS.Core.DataTransfer.Resource.PostInput();
                input.BucketId = Convert.ToInt32(txtBucketId.Text);
                input.ApiKey = txtApiKey.Text;
                input.Source = txtSource.Text;
                input.FileName = fileupload1.FileName;
                input.FilePath = txtPath.Text;

                MS.Core.DataTransfer.Resource.PostOutput output = api.PostResource(input);
            }
        }

        protected void btnPostResourceGuid_Click(object sender, EventArgs e)
        {
            api.PostMedia(txtGuid.Text, true, fileupload.FileName, fileupload.FileBytes, "file1", fileupload.PostedFile.ContentType, new System.Collections.Specialized.NameValueCollection());
        }

        protected void btnGetResource_Click(object sender, EventArgs e)
        {
           api.GetResourceByDate(DateTime.Now.AddDays(-1),20,1);
        }
    }
}