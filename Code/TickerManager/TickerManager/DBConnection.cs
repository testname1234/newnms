﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace TickerManager
{
    public partial class DBConnection : Form
    {
        public static string ConnectionString 
        {
            get { return RegistryOpps.GetConnection(); }
            set { RegistryOpps.UpdateConnection(value); }
        }
        public DBConnection()
        {
            InitializeComponent();
            string oldconnection = ConnectionString;
            tbxServer.Text = oldconnection.Split(';')[0].Replace("Data Source=", "");
            tbxDatabase.Text = oldconnection.Split(';')[1].Replace("Initial Catalog=", "");
            tbxUser.Text = oldconnection.Split(';')[2].Replace("User ID=", "");
            tbxPassword.Text = oldconnection.Split(';')[3].Replace("password=", "");
        }

        private void btnTestConnection_Click(object sender, EventArgs e)
        {
           if(TestConnection())
               MessageBox.Show("Successfull");
        }

        private bool TestConnection()
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(ConstructConnectionString()))
                {
                    connection.Open();
                }
                return true;
            }
            catch (Exception exp)
            {
                MessageBox.Show(exp.Message);
                return false;
            }
        }

        private void btnChange_Click(object sender, EventArgs e)
        {
            if (TestConnection())
                ConnectionString = ConstructConnectionString();
            this.Close();
        }

        private string ConstructConnectionString()
        {
            return "Data Source=" + tbxServer.Text + ";Initial Catalog=" + tbxDatabase.Text + ";User ID=" + tbxUser.Text + ";password=" + tbxPassword.Text;
        }
    }
}
