﻿namespace TickerManager
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tpCategory = new System.Windows.Forms.TabPage();
            this.mCRCategoryTickerRundownDataGridView = new System.Windows.Forms.DataGridView();
            this.tpLatest = new System.Windows.Forms.TabPage();
            this.mCRCategoryTickerRundownBindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.bindingNavigatorAddNewItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.bindingNavigatorDeleteItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveFirstItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMovePreviousItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.bindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorMoveNextItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveLastItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.mCRCategoryTickerRundownBindingNavigatorSaveItem = new System.Windows.Forms.ToolStripButton();
            this.tpBreaking = new System.Windows.Forms.TabPage();
            this.mCRCategoryTickerRundownBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.categoryTickerRundown = new TickerManager.CategoryTickerRundown();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.mCRCategoryTickerRundownTableAdapter = new TickerManager.CategoryTickerRundownTableAdapters.MCRCategoryTickerRundownTableAdapter();
            this.tableAdapterManager = new TickerManager.CategoryTickerRundownTableAdapters.TableAdapterManager();
            this.mCRBreakingTickerRundownBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.mCRBreakingTickerRundownTableAdapter = new TickerManager.CategoryTickerRundownTableAdapters.MCRBreakingTickerRundownTableAdapter();
            this.mCRBreakingTickerRundownDataGridView = new System.Windows.Forms.DataGridView();
            this.mCRBreakingTickerRundownBindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.toolStripLabel1 = new System.Windows.Forms.ToolStripLabel();
            this.toolStripButton2 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton3 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton4 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripTextBox1 = new System.Windows.Forms.ToolStripTextBox();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton5 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton6 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton7 = new System.Windows.Forms.ToolStripButton();
            this.mCRLatestTickerRundownBindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.toolStripButton8 = new System.Windows.Forms.ToolStripButton();
            this.toolStripLabel2 = new System.Windows.Forms.ToolStripLabel();
            this.toolStripButton9 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton10 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton11 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripTextBox2 = new System.Windows.Forms.ToolStripTextBox();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton12 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton13 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton14 = new System.Windows.Forms.ToolStripButton();
            this.mCRLatestTickerRundownBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.mCRLatestTickerRundownTableAdapter = new TickerManager.CategoryTickerRundownTableAdapters.MCRLatestTickerRundownTableAdapter();
            this.mCRLatestTickerRundownDataGridView = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn14 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnChangeConnection = new System.Windows.Forms.Button();
            this.tabControl1.SuspendLayout();
            this.tpCategory.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mCRCategoryTickerRundownDataGridView)).BeginInit();
            this.tpLatest.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mCRCategoryTickerRundownBindingNavigator)).BeginInit();
            this.mCRCategoryTickerRundownBindingNavigator.SuspendLayout();
            this.tpBreaking.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mCRCategoryTickerRundownBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.categoryTickerRundown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mCRBreakingTickerRundownBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mCRBreakingTickerRundownDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mCRBreakingTickerRundownBindingNavigator)).BeginInit();
            this.mCRBreakingTickerRundownBindingNavigator.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mCRLatestTickerRundownBindingNavigator)).BeginInit();
            this.mCRLatestTickerRundownBindingNavigator.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mCRLatestTickerRundownBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mCRLatestTickerRundownDataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tpBreaking);
            this.tabControl1.Controls.Add(this.tpLatest);
            this.tabControl1.Controls.Add(this.tpCategory);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(619, 401);
            this.tabControl1.TabIndex = 0;
            // 
            // tpCategory
            // 
            this.tpCategory.AutoScroll = true;
            this.tpCategory.Controls.Add(this.mCRCategoryTickerRundownBindingNavigator);
            this.tpCategory.Controls.Add(this.mCRCategoryTickerRundownDataGridView);
            this.tpCategory.Location = new System.Drawing.Point(4, 22);
            this.tpCategory.Name = "tpCategory";
            this.tpCategory.Padding = new System.Windows.Forms.Padding(3);
            this.tpCategory.Size = new System.Drawing.Size(611, 375);
            this.tpCategory.TabIndex = 0;
            this.tpCategory.Text = "Category";
            this.tpCategory.UseVisualStyleBackColor = true;
            // 
            // mCRCategoryTickerRundownDataGridView
            // 
            this.mCRCategoryTickerRundownDataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.mCRCategoryTickerRundownDataGridView.AutoGenerateColumns = false;
            this.mCRCategoryTickerRundownDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.mCRCategoryTickerRundownDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewTextBoxColumn7});
            this.mCRCategoryTickerRundownDataGridView.DataSource = this.mCRCategoryTickerRundownBindingSource;
            this.mCRCategoryTickerRundownDataGridView.Location = new System.Drawing.Point(4, 33);
            this.mCRCategoryTickerRundownDataGridView.Name = "mCRCategoryTickerRundownDataGridView";
            this.mCRCategoryTickerRundownDataGridView.Size = new System.Drawing.Size(604, 339);
            this.mCRCategoryTickerRundownDataGridView.TabIndex = 0;
            // 
            // tpLatest
            // 
            this.tpLatest.AutoScroll = true;
            this.tpLatest.Controls.Add(this.mCRLatestTickerRundownDataGridView);
            this.tpLatest.Controls.Add(this.mCRLatestTickerRundownBindingNavigator);
            this.tpLatest.Location = new System.Drawing.Point(4, 22);
            this.tpLatest.Name = "tpLatest";
            this.tpLatest.Padding = new System.Windows.Forms.Padding(3);
            this.tpLatest.Size = new System.Drawing.Size(611, 375);
            this.tpLatest.TabIndex = 1;
            this.tpLatest.Text = "Latest";
            this.tpLatest.UseVisualStyleBackColor = true;
            // 
            // mCRCategoryTickerRundownBindingNavigator
            // 
            this.mCRCategoryTickerRundownBindingNavigator.AddNewItem = this.bindingNavigatorAddNewItem;
            this.mCRCategoryTickerRundownBindingNavigator.BindingSource = this.mCRCategoryTickerRundownBindingSource;
            this.mCRCategoryTickerRundownBindingNavigator.CountItem = this.bindingNavigatorCountItem;
            this.mCRCategoryTickerRundownBindingNavigator.DeleteItem = this.bindingNavigatorDeleteItem;
            this.mCRCategoryTickerRundownBindingNavigator.Dock = System.Windows.Forms.DockStyle.None;
            this.mCRCategoryTickerRundownBindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bindingNavigatorMoveFirstItem,
            this.bindingNavigatorMovePreviousItem,
            this.bindingNavigatorSeparator,
            this.bindingNavigatorPositionItem,
            this.bindingNavigatorCountItem,
            this.bindingNavigatorSeparator1,
            this.bindingNavigatorMoveNextItem,
            this.bindingNavigatorMoveLastItem,
            this.bindingNavigatorSeparator2,
            this.bindingNavigatorAddNewItem,
            this.bindingNavigatorDeleteItem,
            this.mCRCategoryTickerRundownBindingNavigatorSaveItem});
            this.mCRCategoryTickerRundownBindingNavigator.Location = new System.Drawing.Point(5, 5);
            this.mCRCategoryTickerRundownBindingNavigator.MoveFirstItem = this.bindingNavigatorMoveFirstItem;
            this.mCRCategoryTickerRundownBindingNavigator.MoveLastItem = this.bindingNavigatorMoveLastItem;
            this.mCRCategoryTickerRundownBindingNavigator.MoveNextItem = this.bindingNavigatorMoveNextItem;
            this.mCRCategoryTickerRundownBindingNavigator.MovePreviousItem = this.bindingNavigatorMovePreviousItem;
            this.mCRCategoryTickerRundownBindingNavigator.Name = "mCRCategoryTickerRundownBindingNavigator";
            this.mCRCategoryTickerRundownBindingNavigator.PositionItem = this.bindingNavigatorPositionItem;
            this.mCRCategoryTickerRundownBindingNavigator.Size = new System.Drawing.Size(278, 25);
            this.mCRCategoryTickerRundownBindingNavigator.TabIndex = 1;
            this.mCRCategoryTickerRundownBindingNavigator.Text = "bindingNavigator1";
            // 
            // bindingNavigatorAddNewItem
            // 
            this.bindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorAddNewItem.Image")));
            this.bindingNavigatorAddNewItem.Name = "bindingNavigatorAddNewItem";
            this.bindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorAddNewItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorAddNewItem.Text = "Add new";
            // 
            // bindingNavigatorCountItem
            // 
            this.bindingNavigatorCountItem.Name = "bindingNavigatorCountItem";
            this.bindingNavigatorCountItem.Size = new System.Drawing.Size(35, 22);
            this.bindingNavigatorCountItem.Text = "of {0}";
            this.bindingNavigatorCountItem.ToolTipText = "Total number of items";
            // 
            // bindingNavigatorDeleteItem
            // 
            this.bindingNavigatorDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorDeleteItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorDeleteItem.Image")));
            this.bindingNavigatorDeleteItem.Name = "bindingNavigatorDeleteItem";
            this.bindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorDeleteItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorDeleteItem.Text = "Delete";
            // 
            // bindingNavigatorMoveFirstItem
            // 
            this.bindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveFirstItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveFirstItem.Image")));
            this.bindingNavigatorMoveFirstItem.Name = "bindingNavigatorMoveFirstItem";
            this.bindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveFirstItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveFirstItem.Text = "Move first";
            // 
            // bindingNavigatorMovePreviousItem
            // 
            this.bindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMovePreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMovePreviousItem.Image")));
            this.bindingNavigatorMovePreviousItem.Name = "bindingNavigatorMovePreviousItem";
            this.bindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMovePreviousItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMovePreviousItem.Text = "Move previous";
            // 
            // bindingNavigatorSeparator
            // 
            this.bindingNavigatorSeparator.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorPositionItem
            // 
            this.bindingNavigatorPositionItem.AccessibleName = "Position";
            this.bindingNavigatorPositionItem.AutoSize = false;
            this.bindingNavigatorPositionItem.Name = "bindingNavigatorPositionItem";
            this.bindingNavigatorPositionItem.Size = new System.Drawing.Size(50, 23);
            this.bindingNavigatorPositionItem.Text = "0";
            this.bindingNavigatorPositionItem.ToolTipText = "Current position";
            // 
            // bindingNavigatorSeparator1
            // 
            this.bindingNavigatorSeparator1.Name = "bindingNavigatorSeparator1";
            this.bindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorMoveNextItem
            // 
            this.bindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveNextItem.Image")));
            this.bindingNavigatorMoveNextItem.Name = "bindingNavigatorMoveNextItem";
            this.bindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveNextItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveNextItem.Text = "Move next";
            // 
            // bindingNavigatorMoveLastItem
            // 
            this.bindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveLastItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveLastItem.Image")));
            this.bindingNavigatorMoveLastItem.Name = "bindingNavigatorMoveLastItem";
            this.bindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveLastItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveLastItem.Text = "Move last";
            // 
            // bindingNavigatorSeparator2
            // 
            this.bindingNavigatorSeparator2.Name = "bindingNavigatorSeparator2";
            this.bindingNavigatorSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // mCRCategoryTickerRundownBindingNavigatorSaveItem
            // 
            this.mCRCategoryTickerRundownBindingNavigatorSaveItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.mCRCategoryTickerRundownBindingNavigatorSaveItem.Image = ((System.Drawing.Image)(resources.GetObject("mCRCategoryTickerRundownBindingNavigatorSaveItem.Image")));
            this.mCRCategoryTickerRundownBindingNavigatorSaveItem.Name = "mCRCategoryTickerRundownBindingNavigatorSaveItem";
            this.mCRCategoryTickerRundownBindingNavigatorSaveItem.Size = new System.Drawing.Size(23, 22);
            this.mCRCategoryTickerRundownBindingNavigatorSaveItem.Text = "Save Data";
            this.mCRCategoryTickerRundownBindingNavigatorSaveItem.Click += new System.EventHandler(this.mCRCategoryTickerRundownBindingNavigatorSaveItem_Click_3);
            // 
            // tpBreaking
            // 
            this.tpBreaking.Controls.Add(this.mCRBreakingTickerRundownBindingNavigator);
            this.tpBreaking.Controls.Add(this.mCRBreakingTickerRundownDataGridView);
            this.tpBreaking.Location = new System.Drawing.Point(4, 22);
            this.tpBreaking.Name = "tpBreaking";
            this.tpBreaking.Size = new System.Drawing.Size(611, 375);
            this.tpBreaking.TabIndex = 2;
            this.tpBreaking.Text = "Breaking";
            this.tpBreaking.UseVisualStyleBackColor = true;
            // 
            // mCRCategoryTickerRundownBindingSource
            // 
            this.mCRCategoryTickerRundownBindingSource.DataMember = "MCRCategoryTickerRundown";
            this.mCRCategoryTickerRundownBindingSource.DataSource = this.categoryTickerRundown;
            this.mCRCategoryTickerRundownBindingSource.AddingNew += new System.ComponentModel.AddingNewEventHandler(this.mCRCategoryTickerRundownBindingSource_AddingNew);
            // 
            // categoryTickerRundown
            // 
            this.categoryTickerRundown.DataSetName = "CategoryTickerRundown";
            this.categoryTickerRundown.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "Text";
            this.dataGridViewTextBoxColumn3.HeaderText = "Text";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "CategoryName";
            this.dataGridViewTextBoxColumn4.HeaderText = "CategoryName";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.DataPropertyName = "LanguageCode";
            this.dataGridViewTextBoxColumn7.HeaderText = "LanguageCode";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            // 
            // mCRCategoryTickerRundownTableAdapter
            // 
            this.mCRCategoryTickerRundownTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.MCRBreakingTickerRundownTableAdapter = this.mCRBreakingTickerRundownTableAdapter;
            this.tableAdapterManager.MCRCategoryTickerRundownTableAdapter = this.mCRCategoryTickerRundownTableAdapter;
            this.tableAdapterManager.MCRLatestTickerRundownTableAdapter = this.mCRLatestTickerRundownTableAdapter;
            this.tableAdapterManager.UpdateOrder = TickerManager.CategoryTickerRundownTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // mCRBreakingTickerRundownBindingSource
            // 
            this.mCRBreakingTickerRundownBindingSource.DataMember = "MCRBreakingTickerRundown";
            this.mCRBreakingTickerRundownBindingSource.DataSource = this.categoryTickerRundown;
            this.mCRBreakingTickerRundownBindingSource.AddingNew += new System.ComponentModel.AddingNewEventHandler(this.mCRBreakingTickerRundownBindingSource_AddingNew);
            // 
            // mCRBreakingTickerRundownTableAdapter
            // 
            this.mCRBreakingTickerRundownTableAdapter.ClearBeforeFill = true;
            // 
            // mCRBreakingTickerRundownDataGridView
            // 
            this.mCRBreakingTickerRundownDataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.mCRBreakingTickerRundownDataGridView.AutoGenerateColumns = false;
            this.mCRBreakingTickerRundownDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.mCRBreakingTickerRundownDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn5,
            this.dataGridViewTextBoxColumn8});
            this.mCRBreakingTickerRundownDataGridView.DataSource = this.mCRBreakingTickerRundownBindingSource;
            this.mCRBreakingTickerRundownDataGridView.Location = new System.Drawing.Point(3, 30);
            this.mCRBreakingTickerRundownDataGridView.Name = "mCRBreakingTickerRundownDataGridView";
            this.mCRBreakingTickerRundownDataGridView.Size = new System.Drawing.Size(608, 345);
            this.mCRBreakingTickerRundownDataGridView.TabIndex = 0;
            // 
            // mCRBreakingTickerRundownBindingNavigator
            // 
            this.mCRBreakingTickerRundownBindingNavigator.AddNewItem = this.toolStripButton1;
            this.mCRBreakingTickerRundownBindingNavigator.BindingSource = this.mCRBreakingTickerRundownBindingSource;
            this.mCRBreakingTickerRundownBindingNavigator.CountItem = this.toolStripLabel1;
            this.mCRBreakingTickerRundownBindingNavigator.DeleteItem = this.toolStripButton2;
            this.mCRBreakingTickerRundownBindingNavigator.Dock = System.Windows.Forms.DockStyle.None;
            this.mCRBreakingTickerRundownBindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButton3,
            this.toolStripButton4,
            this.toolStripSeparator1,
            this.toolStripTextBox1,
            this.toolStripLabel1,
            this.toolStripSeparator2,
            this.toolStripButton5,
            this.toolStripButton6,
            this.toolStripSeparator3,
            this.toolStripButton1,
            this.toolStripButton2,
            this.toolStripButton7});
            this.mCRBreakingTickerRundownBindingNavigator.Location = new System.Drawing.Point(5, 2);
            this.mCRBreakingTickerRundownBindingNavigator.MoveFirstItem = this.toolStripButton3;
            this.mCRBreakingTickerRundownBindingNavigator.MoveLastItem = this.toolStripButton6;
            this.mCRBreakingTickerRundownBindingNavigator.MoveNextItem = this.toolStripButton5;
            this.mCRBreakingTickerRundownBindingNavigator.MovePreviousItem = this.toolStripButton4;
            this.mCRBreakingTickerRundownBindingNavigator.Name = "mCRBreakingTickerRundownBindingNavigator";
            this.mCRBreakingTickerRundownBindingNavigator.PositionItem = this.toolStripTextBox1;
            this.mCRBreakingTickerRundownBindingNavigator.Size = new System.Drawing.Size(278, 25);
            this.mCRBreakingTickerRundownBindingNavigator.TabIndex = 2;
            this.mCRBreakingTickerRundownBindingNavigator.Text = "bindingNavigator1";
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton1.Image")));
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.RightToLeftAutoMirrorImage = true;
            this.toolStripButton1.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton1.Text = "Add new";
            // 
            // toolStripLabel1
            // 
            this.toolStripLabel1.Name = "toolStripLabel1";
            this.toolStripLabel1.Size = new System.Drawing.Size(35, 22);
            this.toolStripLabel1.Text = "of {0}";
            this.toolStripLabel1.ToolTipText = "Total number of items";
            // 
            // toolStripButton2
            // 
            this.toolStripButton2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton2.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton2.Image")));
            this.toolStripButton2.Name = "toolStripButton2";
            this.toolStripButton2.RightToLeftAutoMirrorImage = true;
            this.toolStripButton2.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton2.Text = "Delete";
            // 
            // toolStripButton3
            // 
            this.toolStripButton3.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton3.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton3.Image")));
            this.toolStripButton3.Name = "toolStripButton3";
            this.toolStripButton3.RightToLeftAutoMirrorImage = true;
            this.toolStripButton3.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton3.Text = "Move first";
            // 
            // toolStripButton4
            // 
            this.toolStripButton4.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton4.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton4.Image")));
            this.toolStripButton4.Name = "toolStripButton4";
            this.toolStripButton4.RightToLeftAutoMirrorImage = true;
            this.toolStripButton4.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton4.Text = "Move previous";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripTextBox1
            // 
            this.toolStripTextBox1.AccessibleName = "Position";
            this.toolStripTextBox1.AutoSize = false;
            this.toolStripTextBox1.Name = "toolStripTextBox1";
            this.toolStripTextBox1.Size = new System.Drawing.Size(50, 23);
            this.toolStripTextBox1.Text = "0";
            this.toolStripTextBox1.ToolTipText = "Current position";
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripButton5
            // 
            this.toolStripButton5.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton5.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton5.Image")));
            this.toolStripButton5.Name = "toolStripButton5";
            this.toolStripButton5.RightToLeftAutoMirrorImage = true;
            this.toolStripButton5.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton5.Text = "Move next";
            // 
            // toolStripButton6
            // 
            this.toolStripButton6.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton6.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton6.Image")));
            this.toolStripButton6.Name = "toolStripButton6";
            this.toolStripButton6.RightToLeftAutoMirrorImage = true;
            this.toolStripButton6.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton6.Text = "Move last";
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripButton7
            // 
            this.toolStripButton7.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton7.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton7.Image")));
            this.toolStripButton7.Name = "toolStripButton7";
            this.toolStripButton7.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton7.Text = "Save Data";
            this.toolStripButton7.Click += new System.EventHandler(this.toolStripButton7_Click);
            // 
            // mCRLatestTickerRundownBindingNavigator
            // 
            this.mCRLatestTickerRundownBindingNavigator.AddNewItem = this.toolStripButton8;
            this.mCRLatestTickerRundownBindingNavigator.BindingSource = this.mCRLatestTickerRundownBindingSource;
            this.mCRLatestTickerRundownBindingNavigator.CountItem = this.toolStripLabel2;
            this.mCRLatestTickerRundownBindingNavigator.DeleteItem = this.toolStripButton9;
            this.mCRLatestTickerRundownBindingNavigator.Dock = System.Windows.Forms.DockStyle.None;
            this.mCRLatestTickerRundownBindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButton10,
            this.toolStripButton11,
            this.toolStripSeparator4,
            this.toolStripTextBox2,
            this.toolStripLabel2,
            this.toolStripSeparator5,
            this.toolStripButton12,
            this.toolStripButton13,
            this.toolStripSeparator6,
            this.toolStripButton8,
            this.toolStripButton9,
            this.toolStripButton14});
            this.mCRLatestTickerRundownBindingNavigator.Location = new System.Drawing.Point(5, 3);
            this.mCRLatestTickerRundownBindingNavigator.MoveFirstItem = this.toolStripButton10;
            this.mCRLatestTickerRundownBindingNavigator.MoveLastItem = this.toolStripButton13;
            this.mCRLatestTickerRundownBindingNavigator.MoveNextItem = this.toolStripButton12;
            this.mCRLatestTickerRundownBindingNavigator.MovePreviousItem = this.toolStripButton11;
            this.mCRLatestTickerRundownBindingNavigator.Name = "mCRLatestTickerRundownBindingNavigator";
            this.mCRLatestTickerRundownBindingNavigator.PositionItem = this.toolStripTextBox2;
            this.mCRLatestTickerRundownBindingNavigator.Size = new System.Drawing.Size(278, 25);
            this.mCRLatestTickerRundownBindingNavigator.TabIndex = 2;
            this.mCRLatestTickerRundownBindingNavigator.Text = "bindingNavigator1";
            // 
            // toolStripButton8
            // 
            this.toolStripButton8.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton8.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton8.Image")));
            this.toolStripButton8.Name = "toolStripButton8";
            this.toolStripButton8.RightToLeftAutoMirrorImage = true;
            this.toolStripButton8.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton8.Text = "Add new";
            // 
            // toolStripLabel2
            // 
            this.toolStripLabel2.Name = "toolStripLabel2";
            this.toolStripLabel2.Size = new System.Drawing.Size(35, 22);
            this.toolStripLabel2.Text = "of {0}";
            this.toolStripLabel2.ToolTipText = "Total number of items";
            // 
            // toolStripButton9
            // 
            this.toolStripButton9.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton9.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton9.Image")));
            this.toolStripButton9.Name = "toolStripButton9";
            this.toolStripButton9.RightToLeftAutoMirrorImage = true;
            this.toolStripButton9.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton9.Text = "Delete";
            // 
            // toolStripButton10
            // 
            this.toolStripButton10.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton10.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton10.Image")));
            this.toolStripButton10.Name = "toolStripButton10";
            this.toolStripButton10.RightToLeftAutoMirrorImage = true;
            this.toolStripButton10.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton10.Text = "Move first";
            // 
            // toolStripButton11
            // 
            this.toolStripButton11.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton11.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton11.Image")));
            this.toolStripButton11.Name = "toolStripButton11";
            this.toolStripButton11.RightToLeftAutoMirrorImage = true;
            this.toolStripButton11.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton11.Text = "Move previous";
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripTextBox2
            // 
            this.toolStripTextBox2.AccessibleName = "Position";
            this.toolStripTextBox2.AutoSize = false;
            this.toolStripTextBox2.Name = "toolStripTextBox2";
            this.toolStripTextBox2.Size = new System.Drawing.Size(50, 23);
            this.toolStripTextBox2.Text = "0";
            this.toolStripTextBox2.ToolTipText = "Current position";
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripButton12
            // 
            this.toolStripButton12.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton12.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton12.Image")));
            this.toolStripButton12.Name = "toolStripButton12";
            this.toolStripButton12.RightToLeftAutoMirrorImage = true;
            this.toolStripButton12.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton12.Text = "Move next";
            // 
            // toolStripButton13
            // 
            this.toolStripButton13.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton13.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton13.Image")));
            this.toolStripButton13.Name = "toolStripButton13";
            this.toolStripButton13.RightToLeftAutoMirrorImage = true;
            this.toolStripButton13.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton13.Text = "Move last";
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripButton14
            // 
            this.toolStripButton14.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton14.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton14.Image")));
            this.toolStripButton14.Name = "toolStripButton14";
            this.toolStripButton14.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton14.Text = "Save Data";
            this.toolStripButton14.Click += new System.EventHandler(this.toolStripButton14_Click);
            // 
            // mCRLatestTickerRundownBindingSource
            // 
            this.mCRLatestTickerRundownBindingSource.DataMember = "MCRLatestTickerRundown";
            this.mCRLatestTickerRundownBindingSource.DataSource = this.categoryTickerRundown;
            this.mCRLatestTickerRundownBindingSource.AddingNew += new System.ComponentModel.AddingNewEventHandler(this.mCRLatestTickerRundownBindingSource_AddingNew);
            // 
            // mCRLatestTickerRundownTableAdapter
            // 
            this.mCRLatestTickerRundownTableAdapter.ClearBeforeFill = true;
            // 
            // mCRLatestTickerRundownDataGridView
            // 
            this.mCRLatestTickerRundownDataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.mCRLatestTickerRundownDataGridView.AutoGenerateColumns = false;
            this.mCRLatestTickerRundownDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.mCRLatestTickerRundownDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn12,
            this.dataGridViewTextBoxColumn14});
            this.mCRLatestTickerRundownDataGridView.DataSource = this.mCRLatestTickerRundownBindingSource;
            this.mCRLatestTickerRundownDataGridView.Location = new System.Drawing.Point(5, 31);
            this.mCRLatestTickerRundownDataGridView.Name = "mCRLatestTickerRundownDataGridView";
            this.mCRLatestTickerRundownDataGridView.Size = new System.Drawing.Size(600, 338);
            this.mCRLatestTickerRundownDataGridView.TabIndex = 2;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.DataPropertyName = "Text";
            this.dataGridViewTextBoxColumn5.HeaderText = "Text";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.DataPropertyName = "LanguageCode";
            this.dataGridViewTextBoxColumn8.HeaderText = "LanguageCode";
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            // 
            // dataGridViewTextBoxColumn12
            // 
            this.dataGridViewTextBoxColumn12.DataPropertyName = "Text";
            this.dataGridViewTextBoxColumn12.HeaderText = "Text";
            this.dataGridViewTextBoxColumn12.Name = "dataGridViewTextBoxColumn12";
            // 
            // dataGridViewTextBoxColumn14
            // 
            this.dataGridViewTextBoxColumn14.DataPropertyName = "LanguageCode";
            this.dataGridViewTextBoxColumn14.HeaderText = "LanguageCode";
            this.dataGridViewTextBoxColumn14.Name = "dataGridViewTextBoxColumn14";
            // 
            // btnChangeConnection
            // 
            this.btnChangeConnection.Location = new System.Drawing.Point(502, -1);
            this.btnChangeConnection.Name = "btnChangeConnection";
            this.btnChangeConnection.Size = new System.Drawing.Size(113, 23);
            this.btnChangeConnection.TabIndex = 1;
            this.btnChangeConnection.Text = "Change Database";
            this.btnChangeConnection.UseVisualStyleBackColor = true;
            this.btnChangeConnection.Click += new System.EventHandler(this.btnChangeConnection_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(619, 401);
            this.Controls.Add(this.btnChangeConnection);
            this.Controls.Add(this.tabControl1);
            this.Name = "Form1";
            this.Text = "Ticker Manager";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.tabControl1.ResumeLayout(false);
            this.tpCategory.ResumeLayout(false);
            this.tpCategory.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mCRCategoryTickerRundownDataGridView)).EndInit();
            this.tpLatest.ResumeLayout(false);
            this.tpLatest.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mCRCategoryTickerRundownBindingNavigator)).EndInit();
            this.mCRCategoryTickerRundownBindingNavigator.ResumeLayout(false);
            this.mCRCategoryTickerRundownBindingNavigator.PerformLayout();
            this.tpBreaking.ResumeLayout(false);
            this.tpBreaking.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mCRCategoryTickerRundownBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.categoryTickerRundown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mCRBreakingTickerRundownBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mCRBreakingTickerRundownDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mCRBreakingTickerRundownBindingNavigator)).EndInit();
            this.mCRBreakingTickerRundownBindingNavigator.ResumeLayout(false);
            this.mCRBreakingTickerRundownBindingNavigator.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mCRLatestTickerRundownBindingNavigator)).EndInit();
            this.mCRLatestTickerRundownBindingNavigator.ResumeLayout(false);
            this.mCRLatestTickerRundownBindingNavigator.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mCRLatestTickerRundownBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mCRLatestTickerRundownDataGridView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tpCategory;
        private System.Windows.Forms.TabPage tpLatest;
        private CategoryTickerRundown categoryTickerRundown;
        private System.Windows.Forms.BindingSource mCRCategoryTickerRundownBindingSource;
        private CategoryTickerRundownTableAdapters.MCRCategoryTickerRundownTableAdapter mCRCategoryTickerRundownTableAdapter;
        private CategoryTickerRundownTableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.BindingNavigator mCRCategoryTickerRundownBindingNavigator;
        private System.Windows.Forms.ToolStripButton bindingNavigatorAddNewItem;
        private System.Windows.Forms.ToolStripLabel bindingNavigatorCountItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorDeleteItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveFirstItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMovePreviousItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator;
        private System.Windows.Forms.ToolStripTextBox bindingNavigatorPositionItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator1;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveNextItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveLastItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator2;
        private System.Windows.Forms.ToolStripButton mCRCategoryTickerRundownBindingNavigatorSaveItem;
        private System.Windows.Forms.DataGridView mCRCategoryTickerRundownDataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.TabPage tpBreaking;
        private System.Windows.Forms.BindingSource mCRBreakingTickerRundownBindingSource;
        private CategoryTickerRundownTableAdapters.MCRBreakingTickerRundownTableAdapter mCRBreakingTickerRundownTableAdapter;
        private System.Windows.Forms.BindingNavigator mCRBreakingTickerRundownBindingNavigator;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private System.Windows.Forms.ToolStripLabel toolStripLabel1;
        private System.Windows.Forms.ToolStripButton toolStripButton2;
        private System.Windows.Forms.ToolStripButton toolStripButton3;
        private System.Windows.Forms.ToolStripButton toolStripButton4;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripTextBox toolStripTextBox1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripButton toolStripButton5;
        private System.Windows.Forms.ToolStripButton toolStripButton6;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripButton toolStripButton7;
        private System.Windows.Forms.DataGridView mCRBreakingTickerRundownDataGridView;
        private System.Windows.Forms.BindingNavigator mCRLatestTickerRundownBindingNavigator;
        private System.Windows.Forms.ToolStripButton toolStripButton8;
        private System.Windows.Forms.ToolStripLabel toolStripLabel2;
        private System.Windows.Forms.ToolStripButton toolStripButton9;
        private System.Windows.Forms.ToolStripButton toolStripButton10;
        private System.Windows.Forms.ToolStripButton toolStripButton11;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripTextBox toolStripTextBox2;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripButton toolStripButton12;
        private System.Windows.Forms.ToolStripButton toolStripButton13;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        private System.Windows.Forms.ToolStripButton toolStripButton14;
        private System.Windows.Forms.BindingSource mCRLatestTickerRundownBindingSource;
        private CategoryTickerRundownTableAdapters.MCRLatestTickerRundownTableAdapter mCRLatestTickerRundownTableAdapter;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private System.Windows.Forms.DataGridView mCRLatestTickerRundownDataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn12;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn14;
        private System.Windows.Forms.Button btnChangeConnection;


    }
}

