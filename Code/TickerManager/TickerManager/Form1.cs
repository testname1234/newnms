﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Reflection;
using System.Text;
using System.Windows.Forms;

namespace TickerManager
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            try
            {
                UpdateConnectionString();
                // TODO: This line of code loads data into the 'categoryTickerRundown.MCRLatestTickerRundown' table. You can move, or remove it, as needed.
                this.mCRLatestTickerRundownTableAdapter.Fill(this.categoryTickerRundown.MCRLatestTickerRundown);
                // TODO: This line of code loads data into the 'categoryTickerRundown.MCRBreakingTickerRundown' table. You can move, or remove it, as needed.
                this.mCRBreakingTickerRundownTableAdapter.Fill(this.categoryTickerRundown.MCRBreakingTickerRundown);
                // TODO: This line of code loads data into the 'categoryTickerRundown.MCRCategoryTickerRundown' table. You can move, or remove it, as needed.
                this.mCRCategoryTickerRundownTableAdapter.Fill(this.categoryTickerRundown.MCRCategoryTickerRundown);
            }
            catch (Exception exp)
            {
                MessageBox.Show(exp.Message);
            }
        }

        private void UpdateConnectionString()
        {
            this.mCRCategoryTickerRundownTableAdapter.Connection = new System.Data.SqlClient.SqlConnection(DBConnection.ConnectionString);
            this.mCRBreakingTickerRundownTableAdapter.Connection = new System.Data.SqlClient.SqlConnection(DBConnection.ConnectionString);
            this.mCRLatestTickerRundownTableAdapter.Connection = new System.Data.SqlClient.SqlConnection(DBConnection.ConnectionString);
        }

        private void mCRCategoryTickerRundownBindingNavigatorSaveItem_Click_3(object sender, EventArgs e)
        {
            this.Validate();
            this.mCRCategoryTickerRundownBindingSource.EndEdit();
            this.tableAdapterManager.MCRCategoryTickerRundownTableAdapter.Update(this.categoryTickerRundown);
        }

        private void toolStripButton14_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.mCRLatestTickerRundownBindingSource.EndEdit();
            this.tableAdapterManager.MCRLatestTickerRundownTableAdapter.Update(this.categoryTickerRundown);
        }

        private void toolStripButton7_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.mCRBreakingTickerRundownBindingSource.EndEdit();
            this.tableAdapterManager.MCRBreakingTickerRundownTableAdapter.Update(this.categoryTickerRundown);
        }

        private void mCRCategoryTickerRundownBindingSource_AddingNew(object sender, AddingNewEventArgs e)
        {
            var row = (mCRCategoryTickerRundownBindingSource.List as DataView).AddNew();
            row.Row["TickerId"] = 1;
            row.Row["CreationDate"] = DateTime.UtcNow;
            row.Row["SequenceNumber"] = 1;
            row.Row["CategoryId"] = 1;
            e.NewObject = row;
        }

        private void mCRBreakingTickerRundownBindingSource_AddingNew(object sender, AddingNewEventArgs e)
        {
            var row = (mCRBreakingTickerRundownBindingSource.List as DataView).AddNew();
            row.Row["TickerId"] = 1;
            row.Row["CreationDate"] = DateTime.UtcNow;
            row.Row["SequenceNumber"] = 1;
            e.NewObject = row;
        }

        private void mCRLatestTickerRundownBindingSource_AddingNew(object sender, AddingNewEventArgs e)
        {
            var row = (mCRLatestTickerRundownBindingSource.List as DataView).AddNew();
            row.Row["TickerId"] = 1;
            row.Row["CreationDate"] = DateTime.UtcNow;
            row.Row["SequenceNumber"] = 1;
            e.NewObject = row;
        }

        private void btnChangeConnection_Click(object sender, EventArgs e)
        {
            DBConnection connection = new DBConnection();
            connection.ShowDialog();
            Form1_Load(null, null);
        }

    }
}
