﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Win32;

namespace TickerManager
{
    public class RegistryOpps
    {
        public static string ApplicationName = "TickerManager";

        static RegistryKey rk = Registry.CurrentUser;
        public static bool Exist()
        {
            // Opening the registry key
            // Open a subKey as read-only
            RegistryKey sk1 = rk.OpenSubKey("TickerManager");
            // If the RegistrySubKey doesn't exist -> (null)
            return !(sk1 == null);
        }

        public static bool CreateConnection(string connection)
        {
            try
            {
                var protocol = rk.CreateSubKey("TickerManager");
                protocol.SetValue("ConnectionString", connection);
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        internal static bool UpdateConnection(string connection)
        {
            try
            {
                // Setting
                // I have to use CreateSubKey 
                // (create or open it if already exits), 
                // 'cause OpenSubKey open a subKey as read-only
                var protocol = rk.OpenSubKey("TickerManager", true);
                protocol.SetValue("ConnectionString", connection);
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        internal static string GetConnection()
        {
            try
            {
                if (Exist())
                {
                    var protocol = rk.OpenSubKey("TickerManager");
                    return protocol.GetValue("ConnectionString") as string;
                }
                else
                {
                    string connection="Data Source=10.3.18.173;Initial Catalog=NMS;User ID=sa;password=Axact123";
                    CreateConnection(connection);
                    return connection;
                }
            }
            catch (Exception e)
            {
                return "";
            }
        }
    }
}
