﻿using NMS.Core;
using NMS.Core.IService;
using NMS.ProcessThreads;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WMPLib;
using FFMPEGLib;

namespace testapp
{
    class Program
    {
        static void Main(string[] args)
        {

            //DeleteExtraMediaResources();
            //INewsFileService fileService = IoC.Resolve<INewsFileService>("NewsFileService");
            //ChannelVideoCreationThread thread = new ChannelVideoCreationThread();
            // ChannelVideoDataThread thread = new ChannelVideoDataThread();
            //ImportFPCMThread thread = new ImportFPCMThread();
            //ImportNewsGeneralThread thread = new ImportNewsGeneralThread();
            //DownloadExternalResourceThread thread = new DownloadExternalResourceThread();
            //  thread.Execute("{ \"FolderPath\":\"172.31.23.39/Resources\",\"SystemTypeId\":null}");
            // var abc= @"\\\\172.31.23.39\\Resources\"
            // DownloadMediaThread thread = new DownloadMediaThread();
            // FollowTwitterThread thread = new FollowTwitterThread();
            //thread.Execute("");
            // FollowTwitterThread thread = new FollowTwitterThread();
            // ImportTwitterNewsThread thread = new ImportTwitterNewsThread();
            ImportGoogleNewsThread thread = new ImportGoogleNewsThread();
            // DownloadExternalResourceThread thread = new DownloadExternalResourceThread();
            // DownloadMediaThread thread = new DownloadMediaThread();
            //thread.Execute("pakistan");
          //  NewsFeedsThread thread = new NewsFeedsThread();
            thread.Execute("pakistan");
            //DiscrepencyThread thread = new DiscrepencyThread();
            //thread.Execute("");

            //IChannelVideoService cserv = IoC.Resolve<IChannelVideoService>("ChannelVideoService");
            //var list = cserv.GetAllChannelVideo();
            //for(int i = 0; i < list.Count; i++)
            //{
            //    var item = list[i];
            //    if(item.ChannelVideoId > 3503)
            //    {
            //        continue;
            //    }
            //    else
            //    {
            //        System.IO.File.Delete(item.PhysicalPath);
            //        if (System.IO.File.Exists(item.PhysicalPath))
            //        { }else
            //        {
            //            cserv.DeleteChannelVideo(item.ChannelVideoId);
            //        }
            //    }
            //}
            //3503
            var t1 = GetDuration(@"\\10.3.18.14\Recording\Mux1\1002\BOL NEWS\1_$2016-12-09 02-00-55.ts");
            var t2 = GetDuration2(@"\\10.3.18.14\Recording\Mux1\1002\BOL NEWS\1_$2016-12-09 02-00-55.ts");
        }

        public static double GetDuration(string filename)
        {
            double ret = 0;
            try
            {
                var player = new WindowsMediaPlayer();
                var clip = player.newMedia(filename);
                ret = TimeSpan.FromSeconds(clip.duration).TotalSeconds;
                return ret;
            }
            catch (Exception)
            {
                return ret;
            }
        }

        public static double GetDuration2(string filename)
        {
            double ret = 0;
            try
            {
                ret = FFMPEGLib.FFMPEG.GetDuration(filename);
                //var player = new WindowsMediaPlayer();
                //var clip = player.newMedia(filename);
                //ret = TimeSpan.FromSeconds(clip.duration).TotalSeconds;
                return ret;
            }
            catch (Exception ex)
            {
                var Errors = new string[1];
                Errors[0] = ex.Message;
                ExceptionLogger.Log(ex);
                return ret;
            }
        }


        private static void DeleteExtraMediaResources()
        {
            string path = @"\\10.3.12.117\MediaResources\";
            path = "";


            DirectoryInfo dInfo = new DirectoryInfo(path);
            FileInfo[] files = dInfo.GetFiles().OrderByDescending(p => p.Length).ToArray();
            NMS.Repository.NewsResourceEditRepository nms = new NMS.Repository.NewsResourceEditRepository();
            var dbGuids = nms.GetAllNewsResourceEditWithURL();
            //var files = Directory.GetFiles(path);
            int singleFiles = 0;
            int copied = 0;
            int guidMatched = 0;
            for (int i=0;i<files.Length;i++)
            {
                var f = files[i];
                var nameWithoutExtension = Path.GetFileNameWithoutExtension(f.Name);
                var existingGuid = dbGuids.Where(x => x.ResourceGuid.Value.ToString() == nameWithoutExtension.ToUpper()).FirstOrDefault();
                if (existingGuid != null)
                {
                    Console.WriteLine("original guid");
                    guidMatched += 1;
                }
                if (i > 0)
                {
                    var lastF = files[i - 1];
                    if (f.Length == lastF.Length)
                    {
                        Console.WriteLine(nameWithoutExtension + "  -- same as above");
                        copied += 1;
                    }
                    else
                    {
                        Console.WriteLine(nameWithoutExtension);
                        singleFiles += 1;
                    }
                }
                else
                {
                    Console.WriteLine(nameWithoutExtension);
                    singleFiles += 1;
                }
            }
            Console.WriteLine("Original files : " + singleFiles + " , copied files : " + copied + " , GUID matched:" + guidMatched);

        }
    }
}
