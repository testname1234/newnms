USE [nms]
GO
--BOL_NMS_SelectNewsByTitleSM 212
	CREATE procedure [dbo].[BOL_NMS_SelectNewsByTitleSM]
		@S_News_ID int,
		@D_News_ID int=-1
		as
	BEGIN
		If(@D_News_ID = -1)
		BEGIN
			SELECT NewsId ,Title ,Description, 100*Score as Similarity_Prcentge FROM
			semanticsimilaritytable( News,*,@S_News_ID)
			INNER JOIN News
			ON NewsId = matched_document_key ORDER BY score DESC
		END
		ELSE
		BEGIN
			SELECT NewsId ,Title ,Description, 100*Score as Similarity_Prcentge FROM
			semanticsimilaritytable( News,*,@S_News_ID)
			INNER JOIN News
			ON NewsId = matched_document_key 

			where NewsId = @D_News_ID
			ORDER BY score DESC
		END
	END






GO
/****** Object:  StoredProcedure [dbo].[DeletetmpMigrationdata]    Script Date: 10/1/2016 12:38:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

 create procedure [dbo].[DeletetmpMigrationdata]

 as

 delete from NewsFilter

 where NewsFilterId in 

 (

 select max(NewsFilterId)  from NewsFilter (nolock)

 group by newsguid,FilterId

 having count(*) > 1

 )



  delete from NewsCategory

 where NewsCategoryId in 

 (

 select max(NewsCategoryId)  from NewsCategory (nolock)

 group by newsguid,CategoryId

 having count(*) > 1

 )



 delete from NewsLocation

 where NewsLocationId in 

 (

 select max(NewsLocationId)  from NewsLocation (nolock)

 group by newsguid,LocationId

 having count(*) > 1

 )

 

 delete from NewsTag

 where NewsTagId in 

 (

 select max(NewsTagId)  from NewsTag (nolock)

 group by newsguid,TagGuid

 having count(*) > 1

 )

 

 delete from NewsResource

 where NewsResourceId in 

 (

 select max(NewsResourceId)  from NewsResource (nolock)

 group by newsguid,ResourceGuid

 having count(*) > 1

 )



 delete from NewsStatistics

 where NewsStatisticsId in 

 (

 select max(NewsStatisticsId)  from NewsStatistics (nolock)

 group by newsguid

 having count(*) > 1

 )



 delete from bunchfilternews

 where bunchfilternewsId in 

 (

 select max(bunchfilternewsId)  from bunchfilternews (nolock)

 group by BunchGuid,Newsguid,FilterId

 having count(*) > 1

 )



 delete from bunch

 where bunchId in 

 (

 select max(bunchId)  from bunch (nolock)

 group by [Guid]

 having count(*) > 1

 )



 delete from BunchNews

 where BunchNewsId in 

 (

 select max(BunchNewsId)  from BunchNews (nolock)

 group by BunchGuid,NewsGuid

 having count(*) > 1

 )



  delete from Resource

 where ResourceId in 

 (

 select max(ResourceId)  from Resource (nolock)

 group by [Guid]

 having count(*) > 1

 )





GO
/****** Object:  StoredProcedure [dbo].[delte_caspertemplatesbyprogram]    Script Date: 10/1/2016 12:38:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[delte_caspertemplatesbyprogram]

as

declare @programid int

set @programid = 2478

delete from CasperTemplateItem where  flashtemplateid in (select CasperTemlateId from CasperTemplate where flashtemplateid in (select flashtemplateid from ScreenTemplate where ProgramId = @programid))

delete from CasperTemplate where flashtemplateid in (select flashtemplateid from ScreenTemplate where ProgramId = @programid)

delete from TemplateScreenElement where ScreenTemplateId in  (select ScreenTemplateId from ScreenTemplate where ProgramId = @programid)

delete from ScreenTemplatekey where ScreenTemplateId in  (select ScreenTemplateId from ScreenTemplate where ProgramId = @programid)

delete from SlotScreenTemplateCamera where SlotScreenTemplateId in(select SlotScreenTemplateId from slotScreenTemplate where ScreenTemplateId in(select ScreenTemplateId from ScreenTemplate where ProgramId = @programid))

delete from SlotScreenTemplateMic where SlotScreenTemplateId in(select SlotScreenTemplateId from slotScreenTemplate where ScreenTemplateId in(select ScreenTemplateId from ScreenTemplate where ProgramId = @programid))

delete from SlotScreenTemplateResource where SlotScreenTemplateId in(select SlotScreenTemplateId from slotScreenTemplate where ScreenTemplateId in(select ScreenTemplateId from ScreenTemplate where ProgramId = @programid))

delete from SlotTemplateScreenElement where SlotScreenTemplateId in(select SlotScreenTemplateId from slotScreenTemplate where ScreenTemplateId in(select ScreenTemplateId from ScreenTemplate where ProgramId = @programid))

delete from SlotScreenTemplatekey where SlotScreenTemplateId in(select SlotScreenTemplateId from slotScreenTemplate where ScreenTemplateId in(select ScreenTemplateId from ScreenTemplate where ProgramId = @programid))

delete from slotScreenTemplate where ScreenTemplateId in(select ScreenTemplateId from ScreenTemplate where ProgramId = @programid)

delete from ScreenTemplate where ProgramId = @programid
GO
/****** Object:  StoredProcedure [dbo].[ELMAH_GetErrorsXml]    Script Date: 10/1/2016 12:38:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[ELMAH_GetErrorsXml]
(
    @Application NVARCHAR(60),
    @PageIndex INT = 0,
    @PageSize INT = 15,
    @TotalCount INT OUTPUT
)
AS 

    SET NOCOUNT ON

    DECLARE @FirstTimeUTC DATETIME
    DECLARE @FirstSequence INT
    DECLARE @StartRow INT
    DECLARE @StartRowIndex INT

    SELECT 
        @TotalCount = COUNT(1) 
    FROM 
        [ELMAH_Error]
    WHERE 
        [Application] = @Application

    -- Get the ID of the first error for the requested page

    SET @StartRowIndex = @PageIndex * @PageSize + 1

    IF @StartRowIndex <= @TotalCount
    BEGIN

        SET ROWCOUNT @StartRowIndex

        SELECT  
            @FirstTimeUTC = [TimeUtc],
            @FirstSequence = [Sequence]
        FROM 
            [ELMAH_Error]
        WHERE   
            [Application] = @Application
        ORDER BY 
            [TimeUtc] DESC, 
            [Sequence] DESC

    END
    ELSE
    BEGIN

        SET @PageSize = 0

    END

    -- Now set the row count to the requested page size and get
    -- all records below it for the pertaining application.

    SET ROWCOUNT @PageSize

    SELECT 
        errorId     = [ErrorId], 
        application = [Application],
        host        = [Host], 
        type        = [Type],
        source      = [Source],
        message     = [Message],
        [user]      = [User],
        statusCode  = [StatusCode], 
        time        = CONVERT(VARCHAR(50), [TimeUtc], 126) + 'Z'
    FROM 
        [ELMAH_Error] error
    WHERE
        [Application] = @Application
    AND
        [TimeUtc] <= @FirstTimeUTC
    AND 
        [Sequence] <= @FirstSequence
    ORDER BY
        [TimeUtc] DESC, 
        [Sequence] DESC
    FOR
        XML AUTO


GO
/****** Object:  StoredProcedure [dbo].[ELMAH_GetErrorXml]    Script Date: 10/1/2016 12:38:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[ELMAH_GetErrorXml]
(
    @Application NVARCHAR(60),
    @ErrorId UNIQUEIDENTIFIER
)
AS

    SET NOCOUNT ON

    SELECT 
        [AllXml]
    FROM 
        [ELMAH_Error]
    WHERE
        [ErrorId] = @ErrorId
    AND
        [Application] = @Application


GO
/****** Object:  StoredProcedure [dbo].[ELMAH_LogError]    Script Date: 10/1/2016 12:38:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[ELMAH_LogError]
(
    @ErrorId UNIQUEIDENTIFIER,
    @Application NVARCHAR(60),
    @Host NVARCHAR(30),
    @Type NVARCHAR(100),
    @Source NVARCHAR(60),
    @Message NVARCHAR(500),
    @User NVARCHAR(50),
    @AllXml NTEXT,
    @StatusCode INT,
    @TimeUtc DATETIME
)
AS

    SET NOCOUNT ON

    INSERT
    INTO
        [ELMAH_Error]
        (
            [ErrorId],
            [Application],
            [Host],
            [Type],
            [Source],
            [Message],
            [User],
            [AllXml],
            [StatusCode],
            [TimeUtc]
        )
    VALUES
        (
            @ErrorId,
            @Application,
            @Host,
            @Type,
            @Source,
            @Message,
            @User,
            @AllXml,
            @StatusCode,
            @TimeUtc
        )


GO
/****** Object:  StoredProcedure [dbo].[Get_EpisodeMos]    Script Date: 10/1/2016 12:38:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Get_EpisodeMos] @casperTemplateId     INT, 
                                @SlotScreenTemplateId INT, 
                                @SlotId               INT 
AS 

	--	declare @SlotId int
	--declare @SlotScreenTemplateId int
	--declare @casperTemplateId int
	--set @SlotId = 2530
	--set @SlotScreenTemplateId= 3388
	--set @casperTemplateId= 72
	
	DELETE FROM episodepcrmos 
    WHERE  flashtemplateid = @casperTemplateId 

    INSERT INTO episodepcrmos 
                (itemname, 
                 [type], 
                 label, 
                 NAME, 
                 videolayer, 
                 isactive, 
                 caspertemplatitemid, 
                 flashtemplateid, 
                 flashtemplatewindowid, 
                 flashtemplatetypeid, 
                 templateid, 
                 flashtemplateguid, 
                 portnumber) 
    SELECT itemname              AS itemname, 
           [type]                AS [type], 
           label                 AS label, 
           NAME                  AS NAME, 
           videolayer            AS videolayer, 
           isactive              AS isactive, 
           caspertemplatitemid   AS caspertemplatitemid, 
           flashtemplateid       AS flashtemplateid, 
           flashtemplatewindowid AS flashtemplatewindowid, 
           flashtemplatetypeid   AS flashtemplatetypeid, 
           parentid              AS templateid, 
           flashtemplateguid     AS flashTemplateGuid, 
           portnumber            AS portnumber 
    FROM   [caspertemplateitem] WITH (nolock) 
    WHERE  flashtemplateid = @casperTemplateId and FlashWindowTypeId not in(2,5)
	
    ORDER  BY itemname DESC 



    DECLARE @tmptable AS TABLE 
      ( 
         id                   INT IDENTITY(1, 1), 
         resourcetypeid       INT, 
         slotscreentemplateid INT, 
         resourceguid         VARCHAR(200),
		 flashtemplatewindowid int,
		 VideoWallId int,
		 IsVideoWallTemplate bit		 
      ) 


    INSERT INTO @tmptable 


    SELECT CASE 
             WHEN  
			 case when isnull(stsr.resourcetypeid,0) = 0 then 
					case when isnull(stsr.Duration,0) <= 0 then 1 else 2 end 
				  else stsr.resourcetypeid end 	
			   = 1 THEN 5 

              WHEN  
			 case when isnull(stsr.resourcetypeid,0) = 0 then 
					case when isnull(stsr.Duration,0) <= 0 then 1 else 2 end 
				  else stsr.resourcetypeid end 	
			   = 2 THEN 2 

             ELSE 5 
           END AS ResourceTypeId, 
           stsr.slotscreentemplateid, 
           stsr.resourceguid,
		   flashtemplatewindowid,
		   st.VideoWallId,
		   st.IsVideoWallTemplate
    FROM   SlotTemplateScreenElementResource stsr (nolock)			
			inner join SlotTemplateScreenElement ste on ste.SlotTemplateScreenElementId = stsr.SlotTemplateScreenElementId			
			inner join SlotScreenTemplate st on st.SlotScreenTemplateId = stsr.slotscreentemplateid 	
    WHERE  stsr.slotscreentemplateid = @SlotScreenTemplateId and isnull(st.IsVideoWallTemplate,0) = 0




    INSERT INTO @tmptable 
    SELECT CASE 
             WHEN  
			 case when isnull(stsr.resourcetypeid,0) = 0 then 
					case when isnull(stsr.Duration,0) <= 0 then 1 else 2 end 
				  else stsr.resourcetypeid end 	
			   = 1 THEN 5 

              WHEN  
			 case when isnull(stsr.resourcetypeid,0) = 0 then 
					case when isnull(stsr.Duration,0) <= 0 then 1 else 2 end 
				  else stsr.resourcetypeid end 	
			   = 2 THEN 2 

             ELSE 5 
           END AS ResourceTypeId, 
           stsr.slotscreentemplateid, 
           stsr.resourceguid,
		   flashtemplatewindowid,
		   isnull(VideoWallId,sw.WallId) as WallId,
		   st.IsVideoWallTemplate	 			   
	from setwallmapping sw 		
	inner join SlotScreenTemplate sst on sst.SlotScreenTemplateId = @SlotScreenTemplateId
	inner join ScreenTemplate st on st.ScreenTemplateId = sst.ScreenTemplateId and sw.RatioTypeId = st.RatioTypeId and st.IsVideoWallTemplate = 1 and st.IsActive = 1
	inner join SlotTemplateScreenElement ste on ste.SlotScreenTemplateId = sst.SlotScreenTemplateId		
	inner join SlotTemplateScreenElementResource stsr (nolock) on ste.SlotTemplateScreenElementId = stsr.SlotTemplateScreenElementId							
	where sw.SetId in (
									select distinct sw.setid
									from setwallmapping sw 
									inner join ProgramSetMapping psm on psm.SetId = sw.SetId
									inner join Episode e on e.ProgramId = psm.ProgramId
									inner join Segment sg on sg.EpisodeId = e.EpisodeId
									inner join slot s on s.SegmentId = sg.SegmentId
									where s.SlotId = @SlotId and psm.IsActive = 1
					)

					declare @wallid int
					select top 1 @wallid = VideoWallId from @tmptable




    INSERT INTO episodepcrmos 
                (itemname, 
                 [type], 
                 label, 
                 NAME, 
                 videolayer, 
                 isactive, 
                 caspertemplatitemid, 
                 flashtemplateid, 
                 flashtemplatewindowid, 
                 flashtemplatetypeid, 
                 templateid, 
                 flashtemplateguid, 
                 portnumber,
				 Channel) 
    SELECT itemname              AS itemname, 
           [type]                AS [type], 
           label                 AS label, 
           t.resourceguid        AS NAME, 
           videolayer            AS videolayer, 
           0                     AS isactive, 
           caspertemplatitemid   AS caspertemplatitemid, 
           flashtemplateid       AS flashtemplateid, 
          c.flashtemplatewindowid as flashtemplatewindowid, 
		  -- case when isnull(t.flashtemplatewindowid,0) = 0 then c.flashtemplatewindowid else t.flashtemplatewindowid end as flashtemplatewindowid, 
           flashtemplatetypeid   AS flashtemplatetypeid, 
           parentid              AS templateid, 
           flashtemplateguid     AS flashTemplateGuid, 
           portnumber            AS portnumber,
		   isnull(VideoWallId,0) as VideoWallId
    FROM   [caspertemplateitem] c WITH (nolock) 
           INNER JOIN @tmptable t 
                   ON t.resourcetypeid = c.flashwindowtypeid 
    WHERE  flashtemplateid = @casperTemplateId and c.flashtemplatewindowid = t.flashtemplatewindowid


    UPDATE em 
    SET    em.flashtemplatetypeid = ct.templatetypeid	 
    FROM   episodepcrmos em (nolock) 
           INNER JOIN caspertemplate ct 
                   ON ct.flashtemplateid = em.flashtemplateid  


    UPDATE em 
    SET    Channel = @wallid
    FROM   episodepcrmos em (nolock) 
           INNER JOIN caspertemplate ct 
           ON ct.flashtemplateid = em.flashtemplateid  and ItemName like '%Template%'
				   



    SELECT * 
    FROM   episodepcrmos (nolock) 
    WHERE  flashtemplateid = @casperTemplateId 
    --or  (@casperTemplateId in (1029,1030) and caspertemplateid IN ( 2039, 2040 ))     
    ORDER  BY itemname DESC 








GO
/****** Object:  StoredProcedure [dbo].[Get_episodemos_bk]    Script Date: 10/1/2016 12:38:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Get_episodemos_bk] @casperTemplateId     INT, 

                                @SlotScreenTemplateId INT, 

                                @SlotId               INT 

AS 

    DELETE FROM episodepcrmos 

    WHERE  flashtemplateid = @casperTemplateId 



    INSERT INTO episodepcrmos 

                (itemname, 

                 [type], 

                 label, 

                 NAME, 

                 channel, 

                 videolayer, 

                 devicename, 

                 [delay], 

                 duration, 

                 creationdate, 

                 lastupdatedate, 

                 isactive, 

                 caspertemplateid, 

                 caspertemplatitemid, 

                 flashtemplateid, 

                 flashtemplatewindowid, 

                 flashtemplatetypeid, 

                 templateid, 

                 flashtemplateguid, 

                 portnumber) 

    SELECT itemname              AS itemname, 

           [type]                AS [type], 

           label                 AS label, 

           NAME                  AS NAME, 

           channel               AS channel, 

           videolayer            AS videolayer, 

           devicename            AS devicename, 

           [delay]               AS [delay], 

           duration              AS duration, 

           creationdate          AS creationdate, 

           lastupdatedate        AS lastupdatedate, 

           isactive              AS isactive, 

           caspertemplateid      AS caspertemplateid, 

           caspertemplatitemid   AS caspertemplatitemid, 

           flashtemplateid       AS flashtemplateid, 

           flashtemplatewindowid AS flashtemplatewindowid, 

           flashtemplatetypeid   AS flashtemplatetypeid, 

           parentid              AS templateid, 

           flashtemplateguid     AS flashTemplateGuid, 

           portnumber            AS portnumber 

    FROM   [caspertemplateitem] WITH (nolock) 

    WHERE  flashtemplateid = @casperTemplateId and @casperTemplateId <> 0 

    ORDER  BY itemname DESC 



    DECLARE @tmptable AS TABLE 

      ( 

         id                   INT IDENTITY(1, 1), 

         resourcetypeid       INT, 

         slotscreentemplateid INT, 

         resourceguid         VARCHAR(200) 

      ) 



    INSERT INTO @tmptable 

    SELECT CASE 

             WHEN resourcetypeid = 1 THEN 5 

             WHEN resourcetypeid = 2 THEN 2 

             ELSE 5 

           END AS ResourceTypeId, 

           slotscreentemplateid, 

           resourceguid 

    FROM   slotscreentemplateresource (nolock) 

    WHERE  slotscreentemplateid = @SlotScreenTemplateId 

           AND resourceguid NOT IN (SELECT resourceguid 

                                    FROM   slottemplatescreenelement (nolock) 

                                    WHERE 

               slotscreentemplateid = @SlotScreenTemplateId and ScreenElementId <> 22) 



    INSERT INTO episodepcrmos 

                (itemname, 

                 [type], 

                 label, 

                 NAME, 

                 channel, 

                 videolayer, 

                 devicename, 

                 [delay], 

                 duration, 

                 creationdate, 

                 lastupdatedate, 

                 isactive, 

                 caspertemplateid, 

                 caspertemplatitemid, 

                 flashtemplateid, 

                 flashtemplatewindowid, 

                 flashtemplatetypeid, 

                 templateid, 

                 flashtemplateguid, 

                 portnumber) 

    SELECT itemname              AS itemname, 

           [type]                AS [type], 

           label                 AS label, 

           t.resourceguid        AS NAME, 

           channel               AS channel, 

           videolayer            AS videolayer, 

           devicename            AS devicename, 

           [delay]               AS [delay], 

           duration              AS duration, 

           creationdate          AS creationdate, 

           lastupdatedate        AS lastupdatedate, 

           0                     AS isactive, 

           caspertemplateid      AS caspertemplateid, 

           caspertemplatitemid   AS caspertemplatitemid, 

           flashtemplateid       AS flashtemplateid, 

           flashtemplatewindowid AS flashtemplatewindowid, 

           flashtemplatetypeid   AS flashtemplatetypeid, 

           parentid              AS templateid, 

           flashtemplateguid     AS flashTemplateGuid, 

           portnumber            AS portnumber 

    FROM   [caspertemplateitem] c WITH (nolock) 

           INNER JOIN @tmptable t 

                   ON t.resourcetypeid = c.flashwindowtypeid 

    WHERE  flashtemplateid = @casperTemplateId  



    UPDATE em 

    SET    em.flashtemplatetypeid = ct.templatetypeid 

    FROM   episodepcrmos em (nolock) 

           INNER JOIN caspertemplate ct 

                   ON ct.flashtemplateid = em.flashtemplateid 



    SELECT * 

    FROM   episodepcrmos (nolock) 

    WHERE  flashtemplateid = @casperTemplateId 

    --or  (@casperTemplateId in (1029,1030) and caspertemplateid IN ( 2039, 2040 ))    

    ORDER  BY itemname DESC 
GO
/****** Object:  StoredProcedure [dbo].[GetRelatedBunch]    Script Date: 10/1/2016 12:38:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--GetRelatedBunch 6888,'551b0258102a15b3fc25fc8b'
CREATE procedure [dbo].[GetRelatedBunch]
@NewsID int,
@BunchGuid varchar(100)
as
BEGIN

--declare @NewsID int
--declare @BunchGuid varchar(100)
--set @NewsID =185360
--set @BunchGuid ='550d8ce01bbe5d2240001084'

	declare @tmpTable as table(id int identity(1,1),BunchID varchar(100))
	declare @NewstmpTable as table(id int identity(1,1),NewsID int)
	declare @ResulttmpTable as table(BunchID varchar(100))
	BEGIN
		
		insert into @tmpTable

		select distinct BunchGuid from semanticsimilaritytable( News,*,@NewsID)
		INNER JOIN News (nolock) ON NewsId = matched_document_key and score >= .3 where IsIndexed = 1		
		and BunchGuid <> @BunchGuid

		declare @RawRelatedCount int
		set  @RawRelatedCount = 0
		select @RawRelatedCount = count(*) from @tmpTable

		if(@RawRelatedCount >0)
		BEGIN
			print 1
			--Loop for bunch	
			declare @counter int
			declare @count int
			declare @BunchId varchar(100)
			set @counter=1	
			While(@counter<=@RawRelatedCount)
			BEGIN
				select @BunchId = BunchId from @tmpTable where id = @counter
				
					--Check news in Bunch
					declare @Newscounter int
					declare @Newscount int
					declare @RawNewsId int
					declare @MatchedCount int
					declare @TotalMatchedCount int
					set @Newscounter=1	
					set @MatchedCount = 0
					set @TotalMatchedCount = 0
					
					insert into @NewstmpTable
					select NewsId from news (nolock) where BunchGuid = @BunchId

					--select 'Bunch'+convert(varchar,@BunchId) 

					select @Newscount = count(*) from news (nolock) where BunchGuid = @BunchId

					while(@Newscounter<=@Newscount)
						BEGIN
							
							select @RawNewsId = NewsId from @NewstmpTable where id = @Newscounter							
							if(@RawNewsId <> @NewsID)
								BEGIN		
							select @MatchedCount = count(*) from semanticsimilaritytable( News,*,@NewsID)
							INNER JOIN News (nolock) ON NewsId = matched_document_key and score >= .3 where IsIndexed = 1
							and newsid = @RawNewsId							

							--select 'NewsCount=' + convert(varchar,@Newscount) + ' matchedcount'+convert(varchar,@MatchedCount) +convert(varchar,isnull(@RawNewsId,'')) +convert(varchar,isnull(@NewsID,''))
							set @TotalMatchedCount = @TotalMatchedCount + @MatchedCount
								END
							else
								BEGIN
							set @TotalMatchedCount = @TotalMatchedCount + 1
								END

							set @Newscounter = @Newscounter + 1
						END
						--select '@Newscount' +  convert(varchar,@TotalMatchedCount)
							if(@Newscount = @TotalMatchedCount)
							BEGIN
								insert into @ResulttmpTable
								select @BunchId
							END
						set @counter = @counter + 1
			END
		END	
	END
	select * from @ResulttmpTable
END







GO
/****** Object:  StoredProcedure [dbo].[GetrelatedbunchOnLoad]    Script Date: 10/1/2016 12:38:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--GetRelatedBunch 6888,'551b0258102a15b3fc25fc8b'

CREATE PROCEDURE [dbo].[GetrelatedbunchOnLoad] 

@NewsGuid    VARCHAR(100), 

@BunchGuid VARCHAR(100) 

AS 

  BEGIN 

      declare @NewsID int 

      --declare @BunchGuid varchar(100) 

      --set @NewsID =185360 

      --set @BunchGuid ='550d8ce01bbe5d2240001084' 

	  

	  select @NewsID = NewsID

	  from news (nolock)

	  where [Guid] = @NewsGuid



      DECLARE @tmpTable AS TABLE 

        ( 

           id      INT IDENTITY(1, 1), 

           bunchid VARCHAR(100) 

        ) 

      DECLARE @NewstmpTable AS TABLE 

        ( 

           id     INT IDENTITY(1, 1), 

           newsid INT 

        ) 

      DECLARE @ResulttmpTable AS TABLE 

        ( 

           bunchid VARCHAR(100) 

        ) 



      BEGIN 

          INSERT INTO @tmpTable 

          SELECT DISTINCT bunchguid 

          FROM   Semanticsimilaritytable(news, *, @NewsID) 

                 INNER JOIN news (nolock) 

                         ON newsid = matched_document_key 

                            AND score >= .3 

          WHERE  isindexed = 1 

                 AND bunchguid <> @BunchGuid 



          DECLARE @RawRelatedCount INT 



          SET @RawRelatedCount = 0 



          SELECT @RawRelatedCount = Count(*) 

          FROM   @tmpTable 



          IF( @RawRelatedCount > 0 ) 

            BEGIN 

                PRINT 1 



                --Loop for bunch   

                DECLARE @counter INT 

                DECLARE @count INT 

                DECLARE @BunchId VARCHAR(100) 



                SET @counter=1 



                WHILE( @counter <= @RawRelatedCount ) 

                  BEGIN 

                      SELECT @BunchId = bunchid 

                      FROM   @tmpTable 

                      WHERE  id = @counter 



                      --Check news in Bunch 

                      DECLARE @Newscounter INT 

                      DECLARE @Newscount INT 

                      DECLARE @RawNewsId INT 

                      DECLARE @MatchedCount INT 

                      DECLARE @TotalMatchedCount INT 



                      SET @Newscounter=1 

                      SET @MatchedCount = 0 

                      SET @TotalMatchedCount = 0 



                      INSERT INTO @NewstmpTable 

                      SELECT newsid 

                      FROM   news (nolock) 

                      WHERE  bunchguid = @BunchId 



                      --select 'Bunch'+convert(varchar,@BunchId)  

                      SELECT @Newscount = Count(*) 

                      FROM   news (nolock) 

                      WHERE  bunchguid = @BunchId 



                      WHILE( @Newscounter <= @Newscount ) 

                        BEGIN 

                            SELECT @RawNewsId = newsid 

                            FROM   @NewstmpTable 

                            WHERE  id = @Newscounter 



                            IF( @RawNewsId <> @NewsID ) 

                              BEGIN 

                                  SELECT @MatchedCount = Count(*) 

                                  FROM   Semanticsimilaritytable(news, *, 

                                         @NewsID) 

                                         INNER JOIN news (nolock) 

                                                 ON newsid = 

                                         matched_document_key 

                                                    AND score >= .1 

                                  WHERE  isindexed = 1 

                                         AND newsid = @RawNewsId 



                                  --select 'NewsCount=' + convert(varchar,@Newscount) + ' matchedcount'+convert(varchar,@MatchedCount) +convert(varchar,isnull(@RawNewsId,'')) +convert(varchar,isnull(@NewsID,''))

                                  SET @TotalMatchedCount = 

                                  @TotalMatchedCount + @MatchedCount 

                              END 

    ELSE 

                              BEGIN 

                                  SET @TotalMatchedCount = 

                                  @TotalMatchedCount + 1 

                              END 



                            SET @Newscounter = @Newscounter + 1 

                        END 



                      --select '@Newscount' +  convert(varchar,@TotalMatchedCount) 

                      IF( @Newscount = @TotalMatchedCount ) 

                        BEGIN 

                            INSERT INTO @ResulttmpTable 

                            SELECT @BunchId 

                        END 



                      SET @counter = @counter + 1 

                  END 

            END 

      END 



      SELECT * 

      FROM   @ResulttmpTable 

  END 























GO
/****** Object:  StoredProcedure [dbo].[getrelatednewstest]    Script Date: 10/1/2016 12:38:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create procedure [dbo].[getrelatednewstest]
as
DECLARE @COUNT INT
DECLARE @COUNTER INT
DECLARE @newsid INT
DECLARE @news varchar(max)

DECLARE @TMPInput AS TABLE(ID INT IDENTITY(1,1),NEWSId int,NEWSTITLE VARCHAR(MAX))

DECLARE @TMPOutput AS TABLE(ID INT IDENTITY(1,1),SOURCE_NEWS_TITLE VARCHAR(MAX),MATCHED_NEWS_TITLE VARCHAR(MAX),score decimal(18,2))

insert into @TMPInput
select NewsId,Title from news 

SELECT @COUNT = COUNT(*),@COUNTER=1 FROM @TMPInput 

WHILE @COUNTER <=@COUNT
begin

select @newsid = NEWSId,@news=NEWSTITLE from @TMPInput
where id =  @COUNTER


if(exists(select 1 from semanticsimilaritytable( News,*,@newsid) INNER JOIN News ON NewsId = matched_document_key and score >= .6))
BEGIN

	insert into @TMPOutput
	select @news,Title,score from 
	semanticsimilaritytable( News,*,@newsid) INNER JOIN News ON NewsId = matched_document_key and score >= .6

END

set @COUNTER = @COUNTER +1
end

select * from @TMPOutput
order by SOURCE_NEWS_TITLE


--delete from bunchnews


--delete  from news
--where newsid in 
--(
--select max(newsid) from news
--group by title
--having count(1) > 1
--)

--select * from news where title = 'Five shot dead in Karachi'

GO
/****** Object:  StoredProcedure [dbo].[GetSLotScreenTemplateByEpisodeId]    Script Date: 10/1/2016 12:38:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[GetSLotScreenTemplateByEpisodeId]
@EpisodeId int
as
select sct.* 
from SlotScreenTemplate sct (nolock)
inner join Slot s (nolock) on s.SlotId = sct.SlotId and isnull(IncludeInRundown,1) <> 0 
inner join segment sg (nolock) on sg.SegmentId = s.SegmentId
where sg.EpisodeId = @EpisodeId 		

GO
/****** Object:  StoredProcedure [dbo].[scrapdatechecking]    Script Date: 10/1/2016 12:38:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[scrapdatechecking]
as
begin
select * ,
CASE when datediff(hh,MaxUpdateDate,GETDATE()) >= 4  THEN 'false' ELSE 'true'  
END as Status 
from ScrapMaxDates
end
GO
/****** Object:  StoredProcedure [dbo].[Slot_GetByEpisodeId]    Script Date: 10/1/2016 12:38:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[Slot_GetByEpisodeId]
@EpisodeId int 
as
select s.* from slot (nolock) s
inner join Segment sg (nolock) on sg.SegmentId = s.SegmentId
where sg.EpisodeId = @EpisodeId


GO
/****** Object:  StoredProcedure [dbo].[sp_generate_inserts]    Script Date: 10/1/2016 12:38:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[sp_generate_inserts]      
(      
 @table_name varchar(176),    -- The table/view for which the INSERT statements will be generated using the existing data      
-- @target_table varchar(776) = NULL,  -- Use this parameter to specify a different table name into which the data will be inserted      
-- @include_column_list bit = 1,  -- Use this parameter to include/ommit column list in the generated INSERT statement      
 @from varchar(800) = NULL,   -- Use this parameter to filter the rows based on a filter condition (using WHERE)      
-- @include_timestamp bit = 0,   -- Specify 1 for this parameter, if you want to include the TIMESTAMP/ROWVERSION column's data in the INSERT statement      
 @ommit_identity bit = 0,  -- Use this parameter to ommit the identity columns      
 @debug_mode bit = 0,   -- If @debug_mode is set to 1, the SQL statements constructed by this procedure will be printed for later examination      
-- @owner varchar(64) = NULL,  -- Use this parameter if you are not the owner of the table      
-- @ommit_images bit = 0,   -- Use this parameter to generate INSERT statements by omitting the 'image' columns      
-- @top int = NULL,   -- Use this parameter to generate INSERT statements only for the TOP n rows      
 @cols_to_include varchar(1000) = NULL, -- List of columns to be included in the INSERT statement      
-- @cols_to_exclude varchar(8000) = NULL, -- List of columns to be excluded from the INSERT statement      
-- @disable_constraints bit = 0,  -- When 1, disables foreign key constraints and enables them after the INSERT statements      
 @ommit_computed_cols bit = 0  -- When 1, computed columns will not be included in the INSERT statement      
)      
AS      
BEGIN      
SET NOCOUNT ON      
IF ((@cols_to_include IS NOT NULL) AND (PATINDEX('''%''',@cols_to_include) = 0))      
 BEGIN      
  RAISERROR('Invalid use of @cols_to_include property',16,1)      
  PRINT 'Specify column names surrounded by single quotes and separated by commas'      
  PRINT 'Eg: EXEC sp_generate_inserts titles, @cols_to_include = "''title_id'',''title''"'      
  RETURN -1 --Failure. Reason: Invalid use of @cols_to_include property      
 END      
      
IF ((OBJECT_ID(@table_name,'U') IS NULL) AND (OBJECT_ID(@table_name,'V') IS NULL))       
 BEGIN      
  RAISERROR('User table or view not found.',16,1)      
  PRINT 'You may see this error, if you are not the owner of this table or view. In that case use @owner parameter to specify the owner name.'      
  PRINT 'Make sure you have SELECT permission on that table or view.'      
  RETURN -1 --Failure. Reason: There is no user table or view with this name      
 END      
      
DECLARE  @Column_ID int,         
  @Column_List varchar(8000),       
  @Column_Name varchar(128),       
  @Start_Insert varchar(786),       
  @Data_Type varchar(128),       
  @Actual_Values varchar(8000), --This is the string that will be finally executed to generate INSERT statements      
  @IDN varchar(128)  --Will contain the IDENTITY column's name in the table      
      
--Variable Initialization      
SET @IDN = ''      
SET @Column_ID = 0      
SET @Column_Name = ''      
SET @Column_List = ''      
SET @Actual_Values = ''      
      
      
SET @Start_Insert = 'INSERT INTO ' + '[' + @table_name + ']'       
      
 SELECT @Column_ID = MIN(ORDINAL_POSITION)        
 FROM INFORMATION_SCHEMA.COLUMNS (NOLOCK)       
 WHERE  TABLE_NAME = @table_name       
      
WHILE @Column_ID IS NOT NULL      
 BEGIN      
  SELECT  @Column_Name = QUOTENAME(COLUMN_NAME),      
    @Data_Type = DATA_TYPE       
  FROM  INFORMATION_SCHEMA.COLUMNS (NOLOCK)       
  WHERE  ORDINAL_POSITION = @Column_ID AND       
   TABLE_NAME = @table_name      
      
  IF @cols_to_include IS NOT NULL --Selecting only user specified columns      
  BEGIN      
   IF CHARINDEX( '''' + SUBSTRING(@Column_Name,2,LEN(@Column_Name)-2) + '''',@cols_to_include) = 0       
   BEGIN      
    GOTO SKIP_LOOP      
   END      
  END      
--  IF (SELECT COLUMNPROPERTY( OBJECT_ID(QUOTENAME(USER_NAME()) + '.' + @table_name),SUBSTRING(@Column_Name,2,LEN(@Column_Name) - 2),'IsIdentity')) = 1       
  IF (SELECT COLUMNPROPERTY( OBJECT_ID(@table_name),SUBSTRING(@Column_Name,2,LEN(@Column_Name) - 2),'IsIdentity')) = 1       
  BEGIN      
   IF (@ommit_identity = 0) --Determing whether to include or exclude the IDENTITY column      
    SET @IDN = @Column_Name      
   ELSE      
    GOTO SKIP_LOOP         
  END      
  --Making sure whether to output computed columns or not      
/*      
  IF @ommit_computed_cols = 1      
  BEGIN      
   IF (SELECT COLUMNPROPERTY( OBJECT_ID(QUOTENAME(USER_NAME()) + '.' + @table_name),SUBSTRING(@Column_Name,2,LEN(@Column_Name) - 2),'IsComputed')) = 1       
   BEGIN      
    GOTO SKIP_LOOP           
   END      
  END      
*/      
  SET @Actual_Values = @Actual_Values  +      
  CASE       
   WHEN @Data_Type IN ('char','varchar','nchar','nvarchar')       
    THEN       
     'COALESCE('''''''' + REPLACE(RTRIM(' + @Column_Name + '),'''''''','''''''''''')+'''''''',''NULL'')'      
   WHEN @Data_Type IN ('datetime','smalldatetime')       
    THEN       
     'COALESCE('''''''' + RTRIM(CONVERT(char,' + @Column_Name + ',109))+'''''''',''NULL'')'      
   WHEN @Data_Type IN ('uniqueidentifier')       
    THEN        
     'COALESCE('''''''' + REPLACE(CONVERT(char(255),RTRIM(' + @Column_Name + ')),'''''''','''''''''''')+'''''''',''NULL'')'      
   WHEN @Data_Type IN ('text','ntext')       
    THEN        
     'COALESCE('''''''' + REPLACE(CONVERT(char(8000),' + @Column_Name + '),'''''''','''''''''''')+'''''''',''NULL'')'           
   WHEN @Data_Type IN ('binary','varbinary')       
    THEN        
     'COALESCE(RTRIM(CONVERT(char,' + 'CONVERT(int,' + @Column_Name + '))),''NULL'')'        
/*   WHEN @Data_Type IN ('timestamp','rowversion')       
    THEN        
     CASE       
      WHEN @include_timestamp = 0       
       THEN       
        '''DEFAULT'''       
       ELSE       
        'COALESCE(RTRIM(CONVERT(char,' + 'CONVERT(int,' + @Column_Name + '))),''NULL'')'        
     END      
*/      
   WHEN @Data_Type IN ('float','real','money','smallmoney')      
    THEN      
     'COALESCE(LTRIM(RTRIM(' + 'CONVERT(char, ' +  @Column_Name  + ',2)' + ')),''NULL'')'       
   ELSE       
    'COALESCE(LTRIM(RTRIM(' + 'CONVERT(char, ' +  @Column_Name  + ')' + ')),''NULL'')'       
  END   + '+' +  ''',''' + ' + '      
      
      
  SET @Column_List = @Column_List +  @Column_Name + ','       
      
      
 SKIP_LOOP: --The label used in GOTO      
      
  SELECT  @Column_ID = MIN(ORDINAL_POSITION)       
  FROM  INFORMATION_SCHEMA.COLUMNS (NOLOCK)       
  WHERE  TABLE_NAME = @table_name AND       
  ORDINAL_POSITION > @Column_ID       
      
 ---------------- While Loop END      
 END      
      
        
 SET @Column_List = LEFT(@Column_List,len(@Column_List) - 1)      
 SET @Actual_Values = LEFT(@Actual_Values,len(@Actual_Values) - 6)      
      
      
      
      
 IF LTRIM(@Column_List) = ''       
 BEGIN      
  RAISERROR('No columns to select. There should at least be one column to generate the output',16,1)      
  RETURN -1 --Failure. Reason: Looks like all the columns are ommitted using the @cols_to_exclude parameter      
 END      
      
      
 if (@from is not null)      
 BEGIN      
  SET @Actual_Values =       
   'SELECT ' +        
   '''' + RTRIM(@Start_Insert) +       
   ' ''+' + '''(' + RTRIM(@Column_List) +  '''+' + ''')''' +       
   ' +''VALUES(''+ ' +  @Actual_Values  + '+'')''' + ' ' +       
   ' FROM ' +  '[' + rtrim(@table_name) + ']' + '(NOLOCK)'+ @from      
   --COALESCE(@from,' FROM ' +  '[' + rtrim(@table_name) + ']' + '(NOLOCK)')      
 END      
 else      
 BEGIN      
  SET @Actual_Values =       
   'SELECT ' +        
   '''' + RTRIM(@Start_Insert) +       
   ' ''+' + '''(' + RTRIM(@Column_List) +  '''+' + ''')''' +       
   ' +''VALUES(''+ ' +  @Actual_Values  + '+'')''' + ' ' +       
   ' FROM ' +  '[' + rtrim(@table_name) + ']' + '(NOLOCK)'      
   --COALESCE(@from,' FROM ' +  '[' + rtrim(@table_name) + ']' + '(NOLOCK)')      
 END      
      
      
-- print @Column_List      
-- print @Actual_Values      
      
IF @debug_mode =1      
 BEGIN      
  PRINT '/*****START OF DEBUG INFORMATION*****'      
  PRINT 'Beginning of the INSERT statement:'      
  PRINT @Start_Insert      
  PRINT ''      
  PRINT 'The column list:'      
  PRINT @Column_List      
  PRINT ''      
PRINT 'The SELECT statement executed to generate the INSERTs'      
  PRINT @Actual_Values      
  PRINT ''      
  PRINT '*****END OF DEBUG INFORMATION*****/'      
  PRINT ''      
 END      
        
PRINT '--INSERTs generated by ''sp_generate_inserts'' stored procedure written by Vyas'      
PRINT '--Build number: 22'      
PRINT '--Problems/Suggestions? Contact Vyas @ vyaskn@hotmail.com'      
PRINT '--http://vyaskn.tripod.com'      
PRINT ''      
PRINT 'SET NOCOUNT ON'      
PRINT ''      
      
      
 IF (@IDN <> '')      
 BEGIN      
  PRINT 'SET IDENTITY_INSERT ' + QUOTENAME(USER_NAME()) + '.' + QUOTENAME(@table_name) + ' ON'      
  PRINT 'GO'      
  PRINT ''      
 END      
 EXEC (@Actual_Values)      
 IF (@IDN <> '')      
 BEGIN      
  PRINT 'SET IDENTITY_INSERT ' + QUOTENAME(USER_NAME()) + '.' + QUOTENAME(@table_name) + ' OFF'      
  PRINT 'GO'      
 END      
SET NOCOUNT OFF      
-- RETURN 0 --Success. We are done!      
      
------Begin End      
END  

GO
/****** Object:  StoredProcedure [dbo].[Sp_GenerateRunDown]    Script Date: 10/1/2016 12:38:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[Sp_GenerateRunDown] @EpisodeId int AS --declare @EpisodeId int
 --set @EpisodeId = 228

DELETE
FROM MosActiveItem
WHERE EpisodeId = @EpisodeId if(NOT EXISTS
                                  (SELECT EpisodeId
                                   FROM MosActiveItem (nolock)
                                   WHERE EpisodeId = @EpisodeId)) BEGIN --select sstr.* from

  INSERT INTO MosActiveItem

  SELECT
		 @EpisodeId AS EpisodeId,
         cmi.Type,
         cmi.DeviceName,
         cmi.Label,
         cmi.Name,
         cmi.Channel,
         cmi.VideoLayer,
         cmi.Delay,
         cmi.Duration,
         cmi.AllowGpi,
         cmi.AllowRemoteTriggering,
         cmi.RemoteTriggerId,
         cmi.FlashLayer,
         cmi.Invoke,
         cmi.UseStoredData,
         cmi.useuppercasedata,
         cmi.color,
         cmi.transition,
         cmi.transitionDuration,
         cmi.tween,
         cmi.direction,
         cmi.seek,
         cmi.length,
         cmi.loop,
         cmi.freezeonload,
         cmi.triggeronnext,
         cmi.autoplay,
         cmi.timecode,
         getdate(),
         getdate(),
         'true' AS isactive,
         cmi.positionx,
         cmi.positiony,
         cmi.scalex,
         cmi.scaley,
         cmi.defer,
         cmi.device,
         cmi.format,
         cmi.showmask,
         cmi.blur,
         cmi.[key],
         cmi.spread,
         cmi.spill,
         cmi.threshold
  FROM ScreenTemplate (nolock) st
  INNER JOIN segment sg ON sg.EpisodeId = @EpisodeId
  INNER JOIN slot slt ON slt.SegmentId = sg.SegmentId
  INNER JOIN SlotScreenTemplate sst ON sst.ScreenTemplateId = st.ScreenTemplateId and slt.SlotId = sst.SlotId
  INNER JOIN CasperMosItem (nolock) cmi ON cmi.CasperMosItemId = st.AssetId WHERE isnull(st.AssetId,0) >0



  INSERT INTO MosActiveItem
  SELECT @EpisodeId AS EpisodeId,
         cmi.Type,
         cmi.DeviceName,
         cmi.Label,
         cmi.Name,
         cmi.Channel,
         cmi.VideoLayer,
         cmi.Delay,
         cmi.Duration,
         cmi.AllowGpi,
         cmi.AllowRemoteTriggering,
         cmi.RemoteTriggerId,
         cmi.FlashLayer,
         cmi.Invoke,
         cmi.UseStoredData,
         cmi.useuppercasedata,
         cmi.color,
         cmi.transition,
         cmi.transitionDuration,
         cmi.tween,
         cmi.direction,
         cmi.seek,
         cmi.length,
         cmi.loop,
         cmi.freezeonload,
         cmi.triggeronnext,
         cmi.autoplay,
         cmi.timecode,
         getdate(),
         getdate(),
         'true' AS isactive,
         cmi.positionx,
         cmi.positiony,
         cmi.scalex,
         cmi.scaley,
         cmi.defer,
         cmi.device,
         cmi.format,
         cmi.showmask,
         cmi.blur,
         cmi.[key],
         cmi.spread,
         cmi.spill,
         cmi.threshold
  FROM CasperMosItem (nolock) cmi WHERE cmi.CasperMosItemId in(3,5,7,9) END
  SELECT *
  FROM MosActiveItem (nolock) WHERE EpisodeId = @EpisodeId
GO
/****** Object:  StoredProcedure [dbo].[Sp_SlotScreenTemplateResourceByEpisode]    Script Date: 10/1/2016 12:38:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[Sp_SlotScreenTemplateResourceByEpisode]
@EpisodeId int
as
select sstr.* from 
Segment s (nolock)
inner join slot st on st.SegmentId = s.SegmentId
inner join SlotScreenTemplate sst on sst.SlotId = st.SlotId --and ScreenTemplateId in(17,18)
inner join SlotScreenTemplateResource sstr on sstr.SlotScreenTemplateId = sst.SlotScreenTemplateId
where s.EpisodeId = @EpisodeId

GO
/****** Object:  UserDefinedFunction [dbo].[resource_getMetabyType]    Script Date: 10/1/2016 12:38:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
    
create function [dbo].[resource_getMetabyType](@OgTable varchar(200),@CSV_Value varchar(100))
RETURNS VARCHAR(1000)
as
BEGIN
declare @MetaValue varchar(1000)


return @MetaValue
END



GO
/****** Object:  UserDefinedFunction [dbo].[udf_TitleCase]    Script Date: 10/1/2016 12:38:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE FUNCTION [dbo].[udf_TitleCase] (@InputString VARCHAR(4000) )

RETURNS VARCHAR(4000)

AS

BEGIN

DECLARE @Index INT

DECLARE @Char CHAR(1)

DECLARE @OutputString VARCHAR(255)

SET @OutputString = LOWER(@InputString)

SET @Index = 2

SET @OutputString =

STUFF(@OutputString, 1, 1,UPPER(SUBSTRING(@InputString,1,1)))

WHILE @Index <= LEN(@InputString)

BEGIN

SET @Char = SUBSTRING(@InputString, @Index, 1)

IF @Char IN (' ', ';', ':', '!', '?', ',', '.', '_', '-', '/', '&','''','(')

IF @Index + 1 <= LEN(@InputString)

BEGIN

IF @Char != ''''

OR

UPPER(SUBSTRING(@InputString, @Index + 1, 1)) != 'S'

SET @OutputString =

STUFF(@OutputString, @Index + 1, 1,UPPER(SUBSTRING(@InputString, @Index + 1, 1)))

END

SET @Index = @Index + 1

END

RETURN ISNULL(@OutputString,'')

END 

GO
/****** Object:  Table [dbo].[Alert]    Script Date: 10/1/2016 12:38:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Alert](
	[AlertId] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](max) NULL,
	[URL] [nvarchar](max) NULL,
	[ThumbUrl] [nvarchar](max) NULL,
	[CreationDate] [datetime] NULL,
	[LastUpdateDate] [datetime] NULL,
	[IsActive] [bit] NULL,
 CONSTRAINT [PK_AlertId] PRIMARY KEY CLUSTERED 
(
	[AlertId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Assignment]    Script Date: 10/1/2016 12:38:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Assignment](
	[AssignmentId] [int] IDENTITY(1,1) NOT NULL,
	[Slug] [nvarchar](max) NULL,
	[LocationId] [int] NULL,
	[CategoryId] [int] NULL,
	[AssignmentType] [int] NULL,
	[CreationDate] [datetime] NULL,
	[Picture] [bit] NULL,
	[Video] [bit] NULL,
	[Audio] [bit] NULL,
	[Document] [bit] NULL,
	[Barcode] [int] NULL,
 CONSTRAINT [PK_Assignment] PRIMARY KEY CLUSTERED 
(
	[AssignmentId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AssignmentResource]    Script Date: 10/1/2016 12:38:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AssignmentResource](
	[AssignmentResourceId] [int] IDENTITY(1,1) NOT NULL,
	[AssignmentId] [int] NULL,
	[Guid] [varchar](50) NULL,
	[CreationDate] [datetime] NULL,
	[LastUpdateDate] [datetime] NULL,
	[IsActive] [bit] NULL,
 CONSTRAINT [PK_AssignmentResource] PRIMARY KEY CLUSTERED 
(
	[AssignmentResourceId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Bunch]    Script Date: 10/1/2016 12:38:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Bunch](
	[BunchId] [int] IDENTITY(1,1) NOT NULL,
	[Guid] [varchar](50) NOT NULL,
	[CreationDate] [datetime] NOT NULL,
	[LastUpdateDate] [datetime] NOT NULL,
	[IsActive] [bit] NOT NULL,
 CONSTRAINT [PK_Bunch] PRIMARY KEY CLUSTERED 
(
	[BunchId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[BunchFilter]    Script Date: 10/1/2016 12:38:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BunchFilter](
	[BunchFilterId] [int] IDENTITY(1,1) NOT NULL,
	[FilterId] [int] NOT NULL,
	[BunchId] [int] NOT NULL,
	[CreationDate] [datetime] NOT NULL,
	[LastUpdateDate] [datetime] NOT NULL,
	[IsActive] [bit] NOT NULL,
 CONSTRAINT [PK_BunchFilter] PRIMARY KEY CLUSTERED 
(
	[BunchFilterId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[BunchFilterNews]    Script Date: 10/1/2016 12:38:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[BunchFilterNews](
	[BunchFilterNewsId] [int] IDENTITY(1,1) NOT NULL,
	[CreationDate] [datetime] NULL,
	[LastUpdateDate] [datetime] NULL,
	[Isactive] [bit] NULL,
	[Newsguid] [varchar](50) NULL,
	[BunchGuid] [varchar](50) NULL,
	[FilterId] [int] NULL,
 CONSTRAINT [PK_BunchFilterNews] PRIMARY KEY CLUSTERED 
(
	[BunchFilterNewsId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[BunchNews]    Script Date: 10/1/2016 12:38:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[BunchNews](
	[BunchNewsId] [int] IDENTITY(1,1) NOT NULL,
	[BunchId] [int] NULL,
	[NewsId] [int] NULL,
	[CreationDate] [datetime] NULL,
	[IsActive] [bit] NULL,
	[BunchGuid] [varchar](50) NULL,
	[NewsGuid] [varchar](50) NULL,
 CONSTRAINT [PK_BunchNews] PRIMARY KEY CLUSTERED 
(
	[BunchNewsId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[BunchResource]    Script Date: 10/1/2016 12:38:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BunchResource](
	[BunchResourceId] [int] IDENTITY(1,1) NOT NULL,
	[BunchId] [int] NOT NULL,
	[ResourceId] [int] NOT NULL,
	[CreationDate] [datetime] NOT NULL,
	[LastUpdateDate] [datetime] NOT NULL,
	[IsActive] [bit] NOT NULL,
 CONSTRAINT [PK_BunchResource] PRIMARY KEY CLUSTERED 
(
	[BunchResourceId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[BunchTag]    Script Date: 10/1/2016 12:38:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BunchTag](
	[BunchTagId] [int] IDENTITY(1,1) NOT NULL,
	[BunchId] [int] NOT NULL,
	[TagId] [int] NOT NULL,
	[CreationDate] [datetime] NOT NULL,
	[LastUpdateDate] [datetime] NOT NULL,
	[IsActive] [bit] NOT NULL,
 CONSTRAINT [PK_BunchTag] PRIMARY KEY CLUSTERED 
(
	[BunchTagId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[CameraType]    Script Date: 10/1/2016 12:38:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CameraType](
	[CameraTypeId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](255) NOT NULL,
 CONSTRAINT [PK_CameraType] PRIMARY KEY CLUSTERED 
(
	[CameraTypeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CasperItemTransformation]    Script Date: 10/1/2016 12:38:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CasperItemTransformation](
	[CasperItemTransformationId] [int] IDENTITY(1,1) NOT NULL,
	[CasperTemplateItemId] [int] NULL,
	[Type] [varchar](500) NULL,
	[Label] [varchar](1000) NULL,
	[Name] [varchar](1000) NULL,
	[Channel] [varchar](1000) NULL,
	[Videolayer] [int] NULL,
	[Devicename] [varchar](1000) NULL,
	[Allowgpi] [bit] NULL,
	[Allowremotetriggering] [bit] NULL,
	[Remotetriggerid] [int] NULL,
	[Tween] [varchar](500) NULL,
	[triggeronnext] [bit] NULL,
	[Positionx] [float] NULL,
	[Positiony] [float] NULL,
	[Scalex] [float] NULL,
	[Scaley] [float] NULL,
	[defer] [bit] NULL,
	[FlashWindowTypeId] [int] NULL,
	[Flashtemplatewindowid] [int] NULL,
 CONSTRAINT [PK_CasperItemTransformation] PRIMARY KEY CLUSTERED 
(
	[CasperItemTransformationId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CasperTemplate]    Script Date: 10/1/2016 12:38:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CasperTemplate](
	[CasperTemlateId] [int] IDENTITY(1,1) NOT NULL,
	[Template] [nchar](50) NOT NULL,
	[ItemCount] [int] NULL,
	[FlashTemplateId] [int] NULL,
	[TemplateTypeId] [int] NULL,
	[Width] [decimal](18, 2) NULL,
	[Height] [decimal](18, 2) NULL,
	[ResourceGuid] [varchar](100) NULL,
 CONSTRAINT [PK_CasperTemplate] PRIMARY KEY CLUSTERED 
(
	[CasperTemlateId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CasperTemplateItem]    Script Date: 10/1/2016 12:38:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CasperTemplateItem](
	[CasperTemplatItemId] [int] IDENTITY(1,1) NOT NULL,
	[casperTemplateId] [int] NULL,
	[ItemName] [nchar](50) NULL,
	[Type] [varchar](500) NULL,
	[Label] [varchar](1000) NULL,
	[Name] [varchar](1000) NULL,
	[Channel] [varchar](1000) NULL,
	[Videolayer] [int] NULL,
	[Devicename] [varchar](1000) NULL,
	[Delay] [int] NULL,
	[Duration] [int] NULL,
	[Allowgpi] [bit] NULL,
	[Allowremotetriggering] [bit] NULL,
	[Remotetriggerid] [int] NULL,
	[Flashlayer] [int] NULL,
	[Invoke] [int] NULL,
	[Usestoreddata] [bit] NULL,
	[Useuppercasedata] [bit] NULL,
	[Color] [varchar](500) NULL,
	[Transition] [varchar](500) NULL,
	[Transitionduration] [int] NULL,
	[Tween] [varchar](500) NULL,
	[Direction] [varchar](500) NULL,
	[Seek] [int] NULL,
	[Length] [int] NULL,
	[Loop] [bit] NULL,
	[Freezeonload] [bit] NULL,
	[Triggeronnext] [bit] NULL,
	[Autoplay] [bit] NULL,
	[Timecode] [varchar](500) NULL,
	[CreationDate] [datetime] NULL,
	[LastUpdateDate] [datetime] NULL,
	[IsActive] [bit] NULL,
	[Defer] [bit] NULL,
	[Device] [int] NULL,
	[Format] [varchar](500) NULL,
	[Showmask] [bit] NULL,
	[Blur] [int] NULL,
	[Key] [varchar](500) NULL,
	[Spread] [float] NULL,
	[Spill] [int] NULL,
	[Threshold] [float] NULL,
	[userType] [int] NULL,
	[casperTransformationId] [int] NULL,
	[parentid] [int] NULL,
	[flashtemplateid] [int] NULL,
	[flashtemplatewindowid] [int] NULL,
	[flashtemplatetypeid] [int] NULL,
	[videoid] [int] NULL,
	[sequenceorder] [int] NULL,
	[portnumber] [int] NULL,
	[flashTemplateGuid] [varchar](100) NULL,
	[flashTemplateName] [varchar](200) NULL,
	[FlashWindowTypeId] [int] NULL,
 CONSTRAINT [PK_CasperTemplateItem] PRIMARY KEY CLUSTERED 
(
	[CasperTemplatItemId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CasperTemplateKey]    Script Date: 10/1/2016 12:38:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CasperTemplateKey](
	[CasperTemplateKeyId] [int] IDENTITY(1,1) NOT NULL,
	[FlashTemplateId] [int] NULL,
	[FlashKey] [varchar](500) NULL,
	[CreationDate] [datetime] NULL,
	[FlashTemplateKeyId] [int] NULL,
 CONSTRAINT [PK_CasperTemplateKey] PRIMARY KEY CLUSTERED 
(
	[CasperTemplateKeyId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CasperTemplateType]    Script Date: 10/1/2016 12:38:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CasperTemplateType](
	[CasperTemplateTypeId] [int] IDENTITY(1,1) NOT NULL,
	[TemplateTypeId] [int] NULL,
	[Name] [varchar](500) NULL,
	[CreationDate] [datetime] NULL,
 CONSTRAINT [PK_CasperTemplateType] PRIMARY KEY CLUSTERED 
(
	[CasperTemplateTypeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CasperTemplateWindowType]    Script Date: 10/1/2016 12:38:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CasperTemplateWindowType](
	[CasperTemplateWindowTypeId] [int] IDENTITY(1,1) NOT NULL,
	[FlashWindowTypeId] [int] NULL,
	[FlashWindowType] [varchar](500) NULL,
	[CreationDate] [datetime] NULL,
 CONSTRAINT [PK_CasperTemplateWindowType] PRIMARY KEY CLUSTERED 
(
	[CasperTemplateWindowTypeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Category]    Script Date: 10/1/2016 12:38:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Category](
	[CategoryId] [int] IDENTITY(1,1) NOT NULL,
	[Category] [nvarchar](255) NOT NULL,
	[ParentId] [int] NULL,
	[CreationDate] [datetime] NOT NULL,
	[LastUpdateDate] [datetime] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[IsApproved] [bit] NULL,
	[CategoryInUrdu] [nvarchar](255) NULL,
 CONSTRAINT [PK_Category] PRIMARY KEY CLUSTERED 
(
	[CategoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[CategoryAlias]    Script Date: 10/1/2016 12:38:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CategoryAlias](
	[CategoryAliasId] [int] IDENTITY(1,1) NOT NULL,
	[CategoryId] [int] NOT NULL,
	[Alias] [nvarchar](100) NOT NULL,
	[CreationDate] [datetime] NOT NULL,
	[LastUpdateDate] [datetime] NOT NULL,
	[IsActive] [bit] NOT NULL,
 CONSTRAINT [PK_CategoryAlias] PRIMARY KEY CLUSTERED 
(
	[CategoryAliasId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Celebrity]    Script Date: 10/1/2016 12:38:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Celebrity](
	[CelebrityId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](255) NOT NULL,
	[ImageGuid] [uniqueidentifier] NULL,
	[Designation] [varchar](2000) NULL,
	[LocationId] [int] NULL,
	[CategoryId] [int] NULL,
	[CreationDate] [datetime] NULL,
	[LastUpdateDate] [datetime] NULL,
	[IsActive] [bit] NULL,
	[WordFrequency] [int] NULL,
	[taccount] [varchar](500) NULL,
	[AddedToRundown] [int] NULL,
	[OnAired] [int] NULL,
	[ExecutedOnOtherChannels] [int] NULL,
 CONSTRAINT [PK_Celebrity] PRIMARY KEY CLUSTERED 
(
	[CelebrityId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CelebrityStatistic]    Script Date: 10/1/2016 12:38:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[CelebrityStatistic](
	[CelebrityStatisticId] [int] IDENTITY(1,1) NOT NULL,
	[ProgramId] [int] NULL,
	[ProgramName] [varchar](500) NULL,
	[ChannelId] [int] NULL,
	[ChannelName] [varchar](250) NULL,
	[EpisodeId] [int] NULL,
	[EpisodeTime] [datetime] NULL,
	[StatisticType] [int] NULL,
	[CelebrityId] [int] NULL,
	[CreationDate] [datetime] NULL,
	[LastUpdatedDate] [datetime] NULL,
	[IsActive] [bit] NULL,
 CONSTRAINT [PK_CelebrityStatistic] PRIMARY KEY CLUSTERED 
(
	[CelebrityStatisticId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Channel]    Script Date: 10/1/2016 12:38:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Channel](
	[ChannelId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[CreationDate] [datetime] NOT NULL,
	[LastUpdateDate] [datetime] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[IsOtherChannel] [bit] NOT NULL,
	[VideosPath] [varchar](500) NULL,
	[ImportVideosPath] [varchar](500) NULL,
 CONSTRAINT [PK_Channel] PRIMARY KEY CLUSTERED 
(
	[ChannelId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ChannelVideo]    Script Date: 10/1/2016 12:38:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ChannelVideo](
	[ChannelVideoId] [int] IDENTITY(1,1) NOT NULL,
	[ChannelId] [int] NULL,
	[From] [datetime] NULL,
	[To] [datetime] NULL,
	[ProgramId] [int] NULL,
	[PhysicalPath] [varchar](1000) NULL,
	[Url] [varchar](1000) NULL,
	[IsProcessed] [bit] NOT NULL,
	[CreationDate] [datetime] NOT NULL,
	[LastUpdateDate] [datetime] NOT NULL,
	[IsActive] [bit] NOT NULL,
 CONSTRAINT [PK_ChannelVideo] PRIMARY KEY CLUSTERED 
(
	[ChannelVideoId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ChatMessage]    Script Date: 10/1/2016 12:38:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ChatMessage](
	[MessageId] [int] IDENTITY(1,1) NOT NULL,
	[From] [int] NOT NULL,
	[To] [int] NOT NULL,
	[Message] [nvarchar](max) NOT NULL,
	[CreationDate] [datetime] NOT NULL,
	[IsRecieved] [bit] NOT NULL,
	[IsSentToGroup] [bit] NULL,
 CONSTRAINT [PK_ChatMessage] PRIMARY KEY CLUSTERED 
(
	[MessageId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[CheckList]    Script Date: 10/1/2016 12:38:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CheckList](
	[ChecklistId] [int] IDENTITY(1,1) NOT NULL,
	[ChecklistName] [varchar](100) NULL,
	[Status] [bit] NULL,
	[ErrorMessage] [varchar](max) NULL,
	[CreationDate] [datetime] NULL,
	[Priority] [bit] NULL,
	[LastUpdatedDate] [datetime] NULL,
 CONSTRAINT [PK_CheckList] PRIMARY KEY CLUSTERED 
(
	[ChecklistId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Comment]    Script Date: 10/1/2016 12:38:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Comment](
	[Commentid] [int] IDENTITY(1,1) NOT NULL,
	[NewsGuid] [varchar](50) NULL,
	[UserId] [int] NULL,
	[CreationDate] [datetime] NULL,
	[LastUpdateDate] [datetime] NULL,
	[Isactive] [bit] NULL,
	[CommentTypeId] [int] NULL,
	[Guid] [varchar](50) NULL,
	[ReporterId] [int] NULL,
	[ResourceGuid] [varchar](50) NULL,
	[Comment] [nvarchar](max) NULL,
 CONSTRAINT [PK_Comment] PRIMARY KEY CLUSTERED 
(
	[Commentid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[DailyNewsPaper]    Script Date: 10/1/2016 12:38:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DailyNewsPaper](
	[DailyNewsPaperId] [int] IDENTITY(1,1) NOT NULL,
	[NewsPaperId] [int] NOT NULL,
	[Date] [datetime] NOT NULL,
	[CreationDate] [datetime] NOT NULL,
	[LastUpdateDate] [datetime] NOT NULL,
	[IsActive] [bit] NOT NULL,
 CONSTRAINT [PK_DailyNewsPaper] PRIMARY KEY CLUSTERED 
(
	[DailyNewsPaperId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ELMAH_Error]    Script Date: 10/1/2016 12:38:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ELMAH_Error](
	[ErrorId] [uniqueidentifier] NOT NULL,
	[Application] [nvarchar](60) NOT NULL,
	[Host] [nvarchar](100) NULL,
	[Type] [nvarchar](100) NOT NULL,
	[Source] [nvarchar](60) NOT NULL,
	[Message] [nvarchar](500) NOT NULL,
	[User] [nvarchar](50) NOT NULL,
	[StatusCode] [int] NOT NULL,
	[TimeUtc] [datetime] NOT NULL,
	[Sequence] [int] IDENTITY(1,1) NOT NULL,
	[AllXml] [ntext] NOT NULL,
 CONSTRAINT [PK_ELMAH_Error] PRIMARY KEY NONCLUSTERED 
(
	[ErrorId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Episode]    Script Date: 10/1/2016 12:38:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Episode](
	[EpisodeId] [int] IDENTITY(1,1) NOT NULL,
	[ProgramId] [int] NOT NULL,
	[From] [datetime] NOT NULL,
	[To] [datetime] NOT NULL,
	[CreationDate] [datetime] NOT NULL,
	[LastUpdateDate] [datetime] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[PreviewGuid] [uniqueidentifier] NULL,
 CONSTRAINT [PK_Episode] PRIMARY KEY CLUSTERED 
(
	[EpisodeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[EpisodePcrMos]    Script Date: 10/1/2016 12:38:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[EpisodePcrMos](
	[EpisodePcrMosId] [int] IDENTITY(1,1) NOT NULL,
	[ItemName] [nchar](50) NULL,
	[Type] [varchar](500) NULL,
	[Label] [varchar](1000) NULL,
	[Name] [varchar](1000) NULL,
	[Channel] [varchar](1000) NULL,
	[Videolayer] [int] NULL,
	[Devicename] [varchar](1000) NULL,
	[Delay] [int] NULL,
	[Duration] [int] NULL,
	[Allowgpi] [bit] NULL,
	[Allowremotetriggering] [bit] NULL,
	[Remotetriggerid] [int] NULL,
	[Flashlayer] [int] NULL,
	[Invoke] [int] NULL,
	[Usestoreddata] [bit] NULL,
	[Useuppercasedata] [bit] NULL,
	[Color] [varchar](500) NULL,
	[Transition] [varchar](500) NULL,
	[Transitionduration] [int] NULL,
	[Tween] [varchar](500) NULL,
	[Direction] [varchar](500) NULL,
	[Seek] [int] NULL,
	[Length] [int] NULL,
	[Loop] [bit] NULL,
	[Freezeonload] [bit] NULL,
	[Triggeronnext] [bit] NULL,
	[Autoplay] [bit] NULL,
	[Timecode] [varchar](500) NULL,
	[CreationDate] [datetime] NULL,
	[LastUpdateDate] [datetime] NULL,
	[IsActive] [bit] NULL,
	[Defer] [bit] NULL,
	[Device] [int] NULL,
	[Format] [varchar](500) NULL,
	[Showmask] [bit] NULL,
	[Blur] [int] NULL,
	[Key] [varchar](500) NULL,
	[Spread] [float] NULL,
	[Spill] [int] NULL,
	[Threshold] [float] NULL,
	[userType] [int] NULL,
	[casperTransformationId] [int] NULL,
	[flashtemplateid] [int] NULL,
	[templateid] [int] NULL,
	[flashtemplatewindowid] [int] NULL,
	[FlashTemplateTypeId] [int] NULL,
	[videoid] [int] NULL,
	[sequenceorder] [int] NULL,
	[casperTemplateId] [int] NULL,
	[CasperTemplatItemId] [int] NULL,
	[parentid] [int] NULL,
	[url] [varchar](1000) NULL,
	[portnumber] [int] NULL,
	[flashTemplateGuid] [varchar](100) NULL,
 CONSTRAINT [PK_EpisodePcrMos] PRIMARY KEY CLUSTERED 
(
	[EpisodePcrMosId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[EventInfo]    Script Date: 10/1/2016 12:38:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EventInfo](
	[EventInfoId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NOT NULL,
	[StartTime] [datetime] NOT NULL,
	[EndTime] [datetime] NOT NULL,
	[SearchTags] [nvarchar](max) NULL,
	[CreationDate] [datetime] NULL,
	[LastUpdateDate] [datetime] NULL,
	[IsActive] [bit] NULL,
 CONSTRAINT [PK_EventInfo] PRIMARY KEY CLUSTERED 
(
	[EventInfoId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[FacebookAccount]    Script Date: 10/1/2016 12:38:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FacebookAccount](
	[FacebookAccountId] [int] IDENTITY(1,1) NOT NULL,
	[UserName] [nvarchar](255) NOT NULL,
	[CelebrityId] [int] NULL,
	[Url] [nvarchar](500) NULL,
	[CreationDate] [datetime] NULL,
	[LastUpdateDate] [datetime] NULL,
	[IsActive] [bit] NULL,
	[IsFollowed] [bit] NULL,
	[Type] [nvarchar](50) NULL,
 CONSTRAINT [PK_FacebookAccount] PRIMARY KEY CLUSTERED 
(
	[FacebookAccountId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[FacebookCredential]    Script Date: 10/1/2016 12:38:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FacebookCredential](
	[FacebookCredentialID] [int] IDENTITY(1,1) NOT NULL,
	[ClientID] [nvarchar](255) NULL,
	[ClientSecret] [nvarchar](255) NULL,
	[AccessToken] [nvarchar](255) NOT NULL,
	[LastUpdateDate] [datetime] NOT NULL,
 CONSTRAINT [PK__Facebook__5B8A19DCF1576142] PRIMARY KEY CLUSTERED 
(
	[FacebookCredentialID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[FileCategory]    Script Date: 10/1/2016 12:38:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FileCategory](
	[FileCategoryId] [int] IDENTITY(1,1) NOT NULL,
	[NewsFileId] [int] NOT NULL,
	[CategoryId] [int] NOT NULL,
	[CreationDate] [datetime] NOT NULL,
 CONSTRAINT [PK_FileCategory] PRIMARY KEY CLUSTERED 
(
	[FileCategoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[FileDetail]    Script Date: 10/1/2016 12:38:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FileDetail](
	[FileDetailId] [int] IDENTITY(1,1) NOT NULL,
	[NewsFileId] [int] NOT NULL,
	[Text] [nvarchar](max) NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreationDate] [datetime] NOT NULL,
	[ReportedBy] [int] NOT NULL,
	[LastUpdateDate] [datetime] NOT NULL,
	[Slug] [nvarchar](500) NOT NULL,
	[CGValues] [nvarchar](max) NULL,
	[SlugId] [int] NULL,
	[Title] [nvarchar](500) NULL,
	[NewsDate] [datetime] NULL,
 CONSTRAINT [PK_FileDetail] PRIMARY KEY CLUSTERED 
(
	[FileDetailId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[FileFolderHistory]    Script Date: 10/1/2016 12:38:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FileFolderHistory](
	[FileFolderHistoryId] [int] IDENTITY(1,1) NOT NULL,
	[NewsFileId] [int] NOT NULL,
	[UserId] [int] NOT NULL,
	[FolderId] [int] NOT NULL,
	[StatusId] [int] NOT NULL,
	[CreationDate] [datetime] NOT NULL,
 CONSTRAINT [PK_FileHistory] PRIMARY KEY CLUSTERED 
(
	[FileFolderHistoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[FileLocation]    Script Date: 10/1/2016 12:38:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FileLocation](
	[FileLocationId] [int] IDENTITY(1,1) NOT NULL,
	[NewsFileId] [int] NOT NULL,
	[LocationId] [int] NOT NULL,
	[CreationDate] [datetime] NOT NULL,
 CONSTRAINT [PK_FileLocation] PRIMARY KEY CLUSTERED 
(
	[FileLocationId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[FileResource]    Script Date: 10/1/2016 12:38:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FileResource](
	[FileResourceId] [int] IDENTITY(1,1) NOT NULL,
	[NewsFileId] [int] NOT NULL,
	[ResourceTypeId] [int] NOT NULL,
	[Guid] [uniqueidentifier] NOT NULL,
	[Caption] [nvarchar](500) NULL,
	[Duration] [float] NULL,
	[CreationDate] [datetime] NOT NULL,
	[UserId] [int] NOT NULL,
	[LastUpdateDate] [datetime] NOT NULL,
	[IsActive] [bit] NOT NULL,
 CONSTRAINT [PK_FileResource] PRIMARY KEY CLUSTERED 
(
	[FileResourceId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[FileStatusHistory]    Script Date: 10/1/2016 12:38:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FileStatusHistory](
	[FileStatusHistoryId] [int] IDENTITY(1,1) NOT NULL,
	[NewsFileId] [int] NOT NULL,
	[StatusId] [int] NOT NULL,
	[UserId] [int] NOT NULL,
	[CreationDate] [datetime] NOT NULL,
 CONSTRAINT [PK_FileStatusHistory] PRIMARY KEY CLUSTERED 
(
	[FileStatusHistoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[FileTag]    Script Date: 10/1/2016 12:38:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FileTag](
	[FileTagId] [int] IDENTITY(1,1) NOT NULL,
	[NewsFileId] [int] NOT NULL,
	[TagId] [int] NOT NULL,
	[Rank] [float] NOT NULL,
	[CreationDate] [datetime] NOT NULL,
	[LastUpdateDate] [datetime] NOT NULL,
 CONSTRAINT [PK_FileTag] PRIMARY KEY CLUSTERED 
(
	[FileTagId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Filter]    Script Date: 10/1/2016 12:38:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Filter](
	[FilterId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[Value] [int] NULL,
	[FilterTypeId] [int] NOT NULL,
	[ParentId] [int] NULL,
	[CssClass] [varchar](50) NULL,
	[CreationDate] [datetime] NOT NULL,
	[LastUpdateDate] [datetime] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[IsApproved] [bit] NULL,
 CONSTRAINT [PK_Filter] PRIMARY KEY CLUSTERED 
(
	[FilterId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[FilterType]    Script Date: 10/1/2016 12:38:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[FilterType](
	[FilterTypeId] [int] IDENTITY(1,1) NOT NULL,
	[FilterType] [varchar](255) NOT NULL,
	[CreationDate] [datetime] NOT NULL,
	[LastUpdateDate] [datetime] NOT NULL,
	[IsActive] [bit] NOT NULL,
 CONSTRAINT [PK_FilterType] PRIMARY KEY CLUSTERED 
(
	[FilterTypeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Folder]    Script Date: 10/1/2016 12:38:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Folder](
	[FolderId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[CategoryId] [int] NULL,
	[LocationId] [int] NULL,
	[CreationDate] [datetime] NOT NULL,
	[ParentFolderId] [int] NULL,
	[IsRundown] [bit] NOT NULL,
	[ProgramId] [int] NULL,
	[IsPrivateFolder] [bit] NULL,
	[EpisodeId] [int] NULL,
	[StartTime] [datetime] NULL,
	[EndTime] [datetime] NULL,
 CONSTRAINT [PK_Folder] PRIMARY KEY CLUSTERED 
(
	[FolderId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Group]    Script Date: 10/1/2016 12:38:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Group](
	[GroupId] [int] IDENTITY(1,1) NOT NULL,
	[GroupName] [nvarchar](100) NULL,
 CONSTRAINT [PK_Group] PRIMARY KEY CLUSTERED 
(
	[GroupId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[GroupUser]    Script Date: 10/1/2016 12:38:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[GroupUser](
	[GroupId] [int] NULL,
	[UserID] [int] NULL,
	[GroupUserId] [int] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PK_GroupUser] PRIMARY KEY CLUSTERED 
(
	[GroupUserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Location]    Script Date: 10/1/2016 12:38:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Location](
	[LocationId] [int] IDENTITY(1,1) NOT NULL,
	[Location] [nvarchar](max) NULL,
	[ParentId] [int] NULL,
	[CreationDate] [datetime] NOT NULL,
	[LastUpdateDate] [datetime] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[IsApproved] [bit] NULL,
	[LocationOriginal] [nvarchar](max) NULL,
 CONSTRAINT [PK_Location] PRIMARY KEY CLUSTERED 
(
	[LocationId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[LocationAlias]    Script Date: 10/1/2016 12:38:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LocationAlias](
	[LocationAliasId] [int] IDENTITY(1,1) NOT NULL,
	[LocationId] [int] NOT NULL,
	[Alias] [nvarchar](100) NOT NULL,
	[CreationDate] [datetime] NOT NULL,
	[LastUpdateDate] [datetime] NOT NULL,
	[IsActive] [bit] NOT NULL,
 CONSTRAINT [PK_LocationAlias] PRIMARY KEY CLUSTERED 
(
	[LocationAliasId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[LogHistory]    Script Date: 10/1/2016 12:38:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[LogHistory](
	[LogHistoryId] [int] IDENTITY(1,1) NOT NULL,
	[TableId] [int] NOT NULL,
	[RefrenceId] [int] NOT NULL,
	[CreationDate] [date] NOT NULL,
	[Method] [varchar](100) NOT NULL,
	[UserId] [int] NOT NULL,
	[UserIp] [varchar](250) NOT NULL,
	[IsActive] [bit] NULL,
	[UserType] [varchar](100) NULL,
	[EntityJson] [nvarchar](max) NULL,
 CONSTRAINT [PK_LogHistory] PRIMARY KEY CLUSTERED 
(
	[LogHistoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[MCRAsimTickerRundown]    Script Date: 10/1/2016 12:38:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MCRAsimTickerRundown](
	[MCRAsimTickerRundownId] [int] IDENTITY(1,1) NOT NULL,
	[TickerId] [int] NOT NULL,
	[Text] [nvarchar](max) NULL,
	[CategoryName] [nvarchar](255) NOT NULL,
	[CategoryId] [int] NOT NULL,
	[SequenceNumber] [int] NOT NULL,
	[LanguageCode] [varchar](10) NOT NULL,
	[CreationDate] [datetime] NOT NULL,
	[TickerLineId] [int] NULL,
 CONSTRAINT [PK_MCRAsimTickerRundown_1] PRIMARY KEY CLUSTERED 
(
	[MCRAsimTickerRundownId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[MCRBreakingTickerRundown]    Script Date: 10/1/2016 12:38:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MCRBreakingTickerRundown](
	[MCRBreakingTickerRundownId] [int] IDENTITY(1,1) NOT NULL,
	[TickerId] [int] NULL,
	[Text] [nvarchar](max) NOT NULL,
	[SequenceNumber] [int] NULL,
	[LanguageCode] [varchar](10) NOT NULL,
	[CreationDate] [datetime] NULL,
	[TickerLineId] [int] NULL,
 CONSTRAINT [PK_MCRBreakingTickerRundown] PRIMARY KEY CLUSTERED 
(
	[MCRBreakingTickerRundownId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[MCRCategoryTickerRundown]    Script Date: 10/1/2016 12:38:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MCRCategoryTickerRundown](
	[MCRCategoryTickerRundownId] [int] IDENTITY(1,1) NOT NULL,
	[TickerId] [int] NOT NULL,
	[Text] [nvarchar](max) NOT NULL,
	[CategoryName] [nvarchar](255) NOT NULL,
	[CategoryId] [int] NOT NULL,
	[SequenceNumber] [int] NOT NULL,
	[LanguageCode] [varchar](10) NOT NULL,
	[CreationDate] [datetime] NOT NULL,
	[TickerLineId] [int] NULL,
 CONSTRAINT [PK_Table_1] PRIMARY KEY CLUSTERED 
(
	[MCRCategoryTickerRundownId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[MCRLatestTickerRundown]    Script Date: 10/1/2016 12:38:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MCRLatestTickerRundown](
	[MCRLatestTickerRundownId] [int] IDENTITY(1,1) NOT NULL,
	[TickerId] [int] NOT NULL,
	[Text] [nvarchar](1000) NOT NULL,
	[SequenceNumber] [int] NOT NULL,
	[LanguageCode] [varchar](10) NOT NULL,
	[CreationDate] [datetime] NOT NULL,
	[TickerLineId] [int] NULL,
 CONSTRAINT [PK_MCRLatestTickerRundownId] PRIMARY KEY CLUSTERED 
(
	[MCRLatestTickerRundownId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[MCRTickerRundownStatus]    Script Date: 10/1/2016 12:38:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MCRTickerRundownStatus](
	[MCRTickerRundownStatusId] [int] IDENTITY(1,1) NOT NULL,
	[StatusId] [int] NOT NULL,
	[LastUpdateDate] [datetime] NOT NULL,
 CONSTRAINT [PK_MCRTickerRundownStatus] PRIMARY KEY CLUSTERED 
(
	[MCRTickerRundownStatusId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[MCRTickerStatus]    Script Date: 10/1/2016 12:38:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MCRTickerStatus](
	[MCRTickerStatusId] [int] IDENTITY(1,1) NOT NULL,
	[Status] [varchar](255) NOT NULL,
 CONSTRAINT [PK_MCRTickerStatus] PRIMARY KEY CLUSTERED 
(
	[MCRTickerStatusId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Message]    Script Date: 10/1/2016 12:38:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Message](
	[MessageId] [int] IDENTITY(1,1) NOT NULL,
	[Message] [nvarchar](max) NOT NULL,
	[To] [int] NOT NULL,
	[From] [int] NOT NULL,
	[IsRecieved] [bit] NOT NULL,
	[CreationDate] [datetime] NOT NULL,
	[LastUpdateDate] [datetime] NOT NULL,
	[SlotScreenTemplateId] [int] NULL,
 CONSTRAINT [PK_Message] PRIMARY KEY CLUSTERED 
(
	[MessageId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[MigrationBunchTemp]    Script Date: 10/1/2016 12:38:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MigrationBunchTemp](
	[MigrationBunchTempId] [int] IDENTITY(1,1) NOT NULL,
	[BunchGuid] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
 CONSTRAINT [pk_MigrationBunchTempId] PRIMARY KEY CLUSTERED 
(
	[MigrationBunchTempId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[MosActiveEpisode]    Script Date: 10/1/2016 12:38:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MosActiveEpisode](
	[MosActiveEpisodeId] [int] IDENTITY(1,1) NOT NULL,
	[EpisodeId] [int] NULL,
	[StatusCode] [int] NULL,
	[CreationDate] [datetime] NULL,
	[LastUpdateDate] [datetime] NULL,
	[IsActive] [bit] NULL,
	[PCRTelePropterStatus] [bit] NULL,
	[PCRPlayoutStatus] [bit] NULL,
	[PCRFourWindowStatus] [bit] NULL,
	[StatusDescription] [varchar](max) NULL,
	[PCRTelepropterJson] [varchar](max) NULL,
	[PCRPlayoutJson] [varchar](max) NULL,
	[PCRFourWindowJson] [varchar](max) NULL,
	[IsAcknowledged] [int] NULL,
 CONSTRAINT [PK_MosActiveEpisode] PRIMARY KEY CLUSTERED 
(
	[MosActiveEpisodeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[MosActiveItem]    Script Date: 10/1/2016 12:38:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MosActiveItem](
	[MosActiveItemId] [int] IDENTITY(1,1) NOT NULL,
	[EpisodeId] [int] NULL,
	[type] [varchar](500) NULL,
	[devicename] [varchar](1000) NULL,
	[label] [varchar](1000) NULL,
	[name] [varchar](1000) NULL,
	[channel] [varchar](1000) NULL,
	[videolayer] [int] NULL,
	[delay] [int] NULL,
	[duration] [int] NULL,
	[allowgpi] [bit] NULL,
	[allowremotetriggering] [bit] NULL,
	[remotetriggerid] [int] NULL,
	[flashlayer] [int] NULL,
	[invoke] [int] NULL,
	[usestoreddata] [bit] NULL,
	[useuppercasedata] [bit] NULL,
	[color] [varchar](500) NULL,
	[transition] [varchar](500) NULL,
	[transitionduration] [int] NULL,
	[tween] [varchar](500) NULL,
	[direction] [varchar](500) NULL,
	[seek] [int] NULL,
	[length] [int] NULL,
	[loop] [bit] NULL,
	[freezeonload] [bit] NULL,
	[triggeronnext] [bit] NULL,
	[autoplay] [bit] NULL,
	[timecode] [varchar](500) NULL,
	[CreationDate] [datetime] NULL,
	[LastUpdateDate] [datetime] NULL,
	[IsActive] [bit] NULL,
	[positionx] [float] NULL,
	[positiony] [float] NULL,
	[scalex] [float] NULL,
	[scaley] [float] NULL,
	[defer] [bit] NULL,
	[device] [int] NULL
) ON [PRIMARY]
SET ANSI_PADDING OFF
ALTER TABLE [dbo].[MosActiveItem] ADD [format] [varchar](500) NULL
ALTER TABLE [dbo].[MosActiveItem] ADD [showmask] [bit] NULL
ALTER TABLE [dbo].[MosActiveItem] ADD [blur] [int] NULL
ALTER TABLE [dbo].[MosActiveItem] ADD [key] [varchar](500) NULL
ALTER TABLE [dbo].[MosActiveItem] ADD [spread] [float] NULL
ALTER TABLE [dbo].[MosActiveItem] ADD [spill] [int] NULL
ALTER TABLE [dbo].[MosActiveItem] ADD [threshold] [float] NULL
 CONSTRAINT [PK_MosActiveItem] PRIMARY KEY CLUSTERED 
(
	[MosActiveItemId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[News]    Script Date: 10/1/2016 12:38:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[News](
	[NewsId] [int] IDENTITY(1,1) NOT NULL,
	[Guid] [varchar](50) NOT NULL,
	[TranslatedDescription] [nvarchar](max) NULL,
	[TranslatedTitle] [nvarchar](500) NULL,
	[Title] [nvarchar](500) NULL,
	[BunchGuid] [varchar](50) NULL,
	[Description] [nvarchar](max) NULL,
	[LocationId] [int] NULL,
	[Author] [nvarchar](255) NULL,
	[UpdateTime] [datetime] NULL,
	[LanguageCode] [varchar](10) NULL,
	[LinkNewsId] [int] NULL,
	[ParentNewsId] [int] NULL,
	[PreviousVersionId] [int] NULL,
	[PublishTime] [datetime] NULL,
	[ThumbnailId] [varchar](50) NULL,
	[VersionNumber] [int] NULL,
	[IsIndexed] [bit] NULL,
	[NewsTypeId] [int] NOT NULL,
	[Source] [varchar](255) NULL,
	[SourceTypeId] [int] NULL,
	[SourceNewsUrl] [nvarchar](500) NULL,
	[SourceFilterId] [int] NULL,
	[CreationDate] [datetime] NOT NULL,
	[LastUpdateDate] [datetime] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[DescriptionText] [varchar](max) NULL,
	[NewsDirection] [varchar](20) NULL,
	[ShortDescription] [nvarchar](max) NULL,
	[Version] [int] NULL,
	[ReporterId] [int] NULL,
	[IsVerified] [bit] NULL,
	[IsAired] [bit] NULL,
	[IsSearchable] [bit] NULL,
	[HasResourceEdit] [bit] NULL,
	[ReferenceNewsId] [int] NULL,
	[IsCompleted] [bit] NULL,
	[HasNoResource] [bit] NULL,
	[AddedToRundownCount] [int] NULL,
	[OnAirCount] [int] NULL,
	[OtherChannelExecutionCount] [int] NULL,
	[NewsTickerCount] [int] NULL,
	[ReferenceNewsGuid] [varchar](50) NULL,
	[ParentNewsGuid] [varchar](50) NULL,
	[IsArchival] [bit] NULL,
	[Slug] [nvarchar](4000) NULL,
	[NewsPaperdescription] [nvarchar](max) NULL,
	[Suggestions] [nvarchar](max) NULL,
	[MigrationStatus] [int] NULL,
	[TranslatedSlug] [nvarchar](max) NULL,
	[SlugId] [int] NULL,
 CONSTRAINT [PK_News] PRIMARY KEY CLUSTERED 
(
	[NewsId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[NewsBucket]    Script Date: 10/1/2016 12:38:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[NewsBucket](
	[NewsBucketId] [int] IDENTITY(1,1) NOT NULL,
	[NewsGuid] [varchar](50) NULL,
	[Title] [nvarchar](500) NULL,
	[Description] [nvarchar](max) NULL,
	[ThumbGuid] [uniqueidentifier] NULL,
	[SequnceNumber] [int] NOT NULL,
	[CategoryId] [int] NULL,
	[TranslatedDescription] [nvarchar](max) NULL,
	[TranslatedTitle] [nvarchar](1000) NULL,
	[LanguageCode] [varchar](10) NULL,
	[PreviewGuid] [uniqueidentifier] NULL,
	[UserId] [int] NULL,
	[TickerId] [int] NULL,
	[ProgramId] [int] NULL,
	[CreationDate] [datetime] NOT NULL,
	[LastUpdateDate] [datetime] NOT NULL,
	[IsActive] [bit] NOT NULL,
 CONSTRAINT [PK_NewsBucket] PRIMARY KEY CLUSTERED 
(
	[NewsBucketId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[NewsCameraMan]    Script Date: 10/1/2016 12:38:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[NewsCameraMan](
	[NewsCameraManId] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NULL,
	[NewsGuid] [varchar](50) NULL,
	[CreationDate] [datetime] NULL,
	[LastUpdateDate] [datetime] NULL,
	[IsActive] [bit] NULL,
 CONSTRAINT [pk_NewsCameraManId] PRIMARY KEY CLUSTERED 
(
	[NewsCameraManId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[NewsCategory]    Script Date: 10/1/2016 12:38:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[NewsCategory](
	[NewsCategoryId] [int] IDENTITY(1,1) NOT NULL,
	[NewsId] [int] NOT NULL,
	[CategoryId] [int] NOT NULL,
	[CreationDate] [datetime] NOT NULL,
	[LastUpdateDate] [datetime] NOT NULL,
	[NewsGuid] [varchar](50) NULL,
 CONSTRAINT [PK_NewsCategory] PRIMARY KEY CLUSTERED 
(
	[NewsCategoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[NewsFile]    Script Date: 10/1/2016 12:38:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[NewsFile](
	[NewsFileId] [int] IDENTITY(1,1) NOT NULL,
	[SequenceNo] [int] NULL,
	[Slug] [nvarchar](500) NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[StatusId] [int] NOT NULL,
	[FolderId] [int] NOT NULL,
	[LocationId] [int] NULL,
	[CategoryId] [int] NULL,
	[CreationDate] [datetime] NOT NULL,
	[LastUpdateDate] [datetime] NOT NULL,
	[Title] [nvarchar](100) NULL,
	[NewsPaperDescription] [nvarchar](max) NULL,
	[LanguageCode] [varchar](10) NOT NULL,
	[PublishTime] [datetime] NOT NULL,
	[Source] [varchar](255) NULL,
	[SourceTypeId] [int] NULL,
	[SourceNewsUrl] [nvarchar](500) NULL,
	[SourceFilterId] [int] NULL,
	[ParentId] [int] NULL,
	[SlugId] [int] NULL,
	[ProgramId] [int] NULL,
	[AssignedTo] [int] NULL,
 CONSTRAINT [PK_File] PRIMARY KEY CLUSTERED 
(
	[NewsFileId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[NewsFilter]    Script Date: 10/1/2016 12:38:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[NewsFilter](
	[NewsFilterId] [int] IDENTITY(1,1) NOT NULL,
	[FilterId] [int] NOT NULL,
	[NewsId] [int] NOT NULL,
	[CreationDate] [datetime] NULL,
	[LastUpdateDate] [datetime] NULL,
	[IsActive] [bit] NULL,
	[NewsGuid] [varchar](50) NULL,
	[ReporterId] [int] NULL,
 CONSTRAINT [PK_NewsFilter] PRIMARY KEY CLUSTERED 
(
	[NewsFilterId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[NewsLocation]    Script Date: 10/1/2016 12:38:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[NewsLocation](
	[NewsLocationid] [int] IDENTITY(1,1) NOT NULL,
	[NewsId] [int] NULL,
	[LocationId] [int] NULL,
	[CreationDate] [datetime] NULL,
	[LastUpdateDate] [datetime] NULL,
	[Isactive] [bit] NULL,
	[NewsGuid] [varchar](50) NULL,
 CONSTRAINT [PK_NewsLocation] PRIMARY KEY CLUSTERED 
(
	[NewsLocationid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[NewsPaper]    Script Date: 10/1/2016 12:38:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[NewsPaper](
	[NewsPaperId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[CreationDate] [datetime] NOT NULL,
	[LastUpdateDate] [datetime] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[LanguageCode] [varchar](10) NULL,
 CONSTRAINT [PK_NewsPapper] PRIMARY KEY CLUSTERED 
(
	[NewsPaperId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[NewsPaperPage]    Script Date: 10/1/2016 12:38:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[NewsPaperPage](
	[NewsPaperPageId] [int] IDENTITY(1,1) NOT NULL,
	[DailyNewsPaperId] [int] NOT NULL,
	[ImageGuid] [uniqueidentifier] NULL,
	[PageTitle] [nvarchar](255) NOT NULL,
	[IsProcessed] [bit] NOT NULL,
	[CreationDate] [datetime] NOT NULL,
	[LastUpdateDate] [datetime] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[PageSection] [varchar](100) NULL,
 CONSTRAINT [PK_NewsPaperPage] PRIMARY KEY CLUSTERED 
(
	[NewsPaperPageId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[NewsPaperPagesPart]    Script Date: 10/1/2016 12:38:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NewsPaperPagesPart](
	[NewsPaperPagesPartId] [int] IDENTITY(1,1) NOT NULL,
	[NewsPaperPageId] [int] NULL,
	[PartNumber] [int] NULL,
	[Bottom] [int] NULL,
	[Top] [int] NULL,
	[Left] [int] NULL,
	[Right] [int] NULL,
	[CreationDate] [datetime] NULL,
	[LastUpdateDate] [datetime] NULL,
	[isActive] [bit] NULL,
 CONSTRAINT [PK_NewsPaperPagesPart] PRIMARY KEY CLUSTERED 
(
	[NewsPaperPagesPartId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[NewsPaperPagesPartsDetail]    Script Date: 10/1/2016 12:38:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[NewsPaperPagesPartsDetail](
	[NewsPaperPagesPartsDetailId] [int] IDENTITY(1,1) NOT NULL,
	[NewsPaperPagesPartId] [int] NULL,
	[ImageGuid] [varchar](50) NULL,
	[CreationDate] [datetime] NULL,
	[LastUpdateDate] [datetime] NULL,
	[isActive] [bit] NULL,
 CONSTRAINT [PK_NewsPaperPagesPartsDetail] PRIMARY KEY CLUSTERED 
(
	[NewsPaperPagesPartsDetailId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[NewsResource]    Script Date: 10/1/2016 12:38:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[NewsResource](
	[NewsResourceId] [int] IDENTITY(1,1) NOT NULL,
	[NewsId] [int] NOT NULL,
	[ResourceId] [int] NOT NULL,
	[CreationDate] [datetime] NOT NULL,
	[LastUpdateDate] [datetime] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[NewsGuid] [varchar](50) NULL,
	[ResourceGuid] [varchar](50) NULL,
 CONSTRAINT [PK_NewsResource] PRIMARY KEY CLUSTERED 
(
	[NewsResourceId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[NewsResourceEdit]    Script Date: 10/1/2016 12:38:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[NewsResourceEdit](
	[NewsResourceEditId] [int] IDENTITY(1,1) NOT NULL,
	[ResourceGuid] [uniqueidentifier] NULL,
	[NewsId] [int] NULL,
	[ResourceTypeId] [int] NULL,
	[FileName] [varchar](max) NULL,
	[Top] [float] NULL,
	[Left] [float] NULL,
	[Bottom] [float] NULL,
	[Right] [float] NULL,
	[Url] [varchar](max) NULL,
	[CreationDate] [datetime] NULL,
	[LastUpdateDate] [datetime] NULL,
	[isactive] [bit] NULL,
	[NewsGuid] [varchar](50) NULL,
 CONSTRAINT [PK_NewsResourceEdit] PRIMARY KEY CLUSTERED 
(
	[NewsResourceEditId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[NewsSource]    Script Date: 10/1/2016 12:38:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[NewsSource](
	[NewsSourceId] [int] IDENTITY(1,1) NOT NULL,
	[Source] [varchar](200) NULL,
	[FilterTypeId] [int] NULL,
	[FilterId] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[LastUpdateddate] [datetime] NULL,
	[IsActive] [bit] NULL,
	[languageCode] [varchar](20) NULL,
	[FolderPath] [varchar](5000) NULL,
 CONSTRAINT [NewsSourceId_Pk] PRIMARY KEY CLUSTERED 
(
	[NewsSourceId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[NewsStatistics]    Script Date: 10/1/2016 12:38:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[NewsStatistics](
	[NewsStatisticsId] [int] IDENTITY(1,1) NOT NULL,
	[ChannelName] [varchar](100) NULL,
	[ProgramId] [int] NULL,
	[ProgramName] [varchar](500) NULL,
	[EpisodeId] [int] NULL,
	[Time] [datetime] NULL,
	[Type] [int] NULL,
	[NewsGuid] [varchar](50) NULL,
 CONSTRAINT [pk_NewsStatisticsId] PRIMARY KEY CLUSTERED 
(
	[NewsStatisticsId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[NewsTag]    Script Date: 10/1/2016 12:38:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[NewsTag](
	[NewsTagId] [int] IDENTITY(1,1) NOT NULL,
	[Rank] [float] NULL,
	[TagId] [int] NOT NULL,
	[NewsId] [int] NOT NULL,
	[CreationDate] [datetime] NULL,
	[LastUpdateDate] [datetime] NULL,
	[NewsGuid] [varchar](50) NULL,
	[TagGuid] [varchar](50) NULL,
	[Tag] [nvarchar](max) NULL,
 CONSTRAINT [PK_NewsTag] PRIMARY KEY CLUSTERED 
(
	[NewsTagId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[NewsType]    Script Date: 10/1/2016 12:38:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[NewsType](
	[NewsTypeId] [int] IDENTITY(1,1) NOT NULL,
	[NewsType] [varchar](50) NOT NULL,
 CONSTRAINT [PK_NewsType] PRIMARY KEY CLUSTERED 
(
	[NewsTypeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[NewsUpdateHistory]    Script Date: 10/1/2016 12:38:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[NewsUpdateHistory](
	[NewsUpdateHistoryId] [int] IDENTITY(1,1) NOT NULL,
	[NewsGuid] [varchar](50) NULL,
	[NewsId] [int] NULL,
	[TranslatedDescription] [nvarchar](max) NULL,
	[TranslatedTitle] [nvarchar](1000) NULL,
	[Title] [nvarchar](1000) NULL,
	[BunchGuid] [varchar](50) NULL,
	[Description] [nvarchar](max) NULL,
	[DescriptionText] [nvarchar](max) NULL,
	[ShortDescription] [nvarchar](max) NULL,
	[LanguageCode] [varchar](10) NULL,
	[Author] [nvarchar](500) NULL,
	[ReporterId] [int] NULL,
	[IsVerified] [bit] NULL,
	[IsAired] [bit] NULL,
	[AddedToRundownCount] [int] NULL,
	[OnAirCount] [int] NULL,
	[OtherChannelExecutionCount] [int] NULL,
	[NewsTickerCount] [int] NULL,
	[Slug] [nvarchar](max) NULL,
	[NewsPaperdescription] [nvarchar](max) NULL,
	[Suggestions] [nvarchar](max) NULL,
	[Source] [varchar](200) NULL,
	[SourceTypeId] [int] NULL,
	[SourceNewsUrl] [nvarchar](1000) NULL,
	[SourceFilterId] [int] NULL,
	[ThumbnailId] [varchar](50) NULL,
	[NewsTypeId] [int] NULL,
	[CreationDate] [datetime] NULL,
	[LastUpdateDate] [datetime] NULL,
	[PublishTime] [datetime] NULL,
	[IsActive] [bit] NULL,
	[SlugId] [int] NULL,
 CONSTRAINT [PK_NewsUpdateHistory] PRIMARY KEY CLUSTERED 
(
	[NewsUpdateHistoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Notification]    Script Date: 10/1/2016 12:38:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Notification](
	[NotificationId] [int] IDENTITY(1,1) NOT NULL,
	[NotificationTypeId] [int] NOT NULL,
	[Title] [nvarchar](max) NOT NULL,
	[Argument] [nvarchar](max) NULL,
	[RecipientId] [int] NULL,
	[SenderId] [int] NULL,
	[IsRead] [bit] NULL,
	[CreationDate] [datetime] NULL,
	[LastUpdateDate] [datetime] NULL,
	[IsActive] [bit] NULL,
	[slotid] [int] NULL,
	[slotscreentemplateid] [int] NULL,
 CONSTRAINT [PK_NotificationId] PRIMARY KEY CLUSTERED 
(
	[NotificationId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[NotificationType]    Script Date: 10/1/2016 12:38:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NotificationType](
	[NotificationTypeId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](250) NOT NULL,
	[Template] [nvarchar](max) NULL,
	[Argument] [nvarchar](max) NULL,
 CONSTRAINT [PK_NotificationTypeId] PRIMARY KEY CLUSTERED 
(
	[NotificationTypeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[OnAirTicker]    Script Date: 10/1/2016 12:38:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[OnAirTicker](
	[TickerId] [int] IDENTITY(1,1) NOT NULL,
	[TickerGroupName] [nvarchar](max) NULL,
	[LocationId] [int] NULL,
	[CategoryId] [int] NULL,
	[SequenceId] [int] NULL,
	[TickerTypeId] [int] NULL,
	[IsShow] [bit] NULL,
	[NewsGuid] [varchar](max) NULL,
	[UserId] [int] NULL,
	[OnAiredTime] [datetime] NULL,
	[CreationDate] [datetime] NULL,
	[LastUpdatedDate] [datetime] NULL,
	[IsActive] [bit] NULL,
	[CreatedBy] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[TickerId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[OnAirTickerLine]    Script Date: 10/1/2016 12:38:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[OnAirTickerLine](
	[TickerLineId] [int] IDENTITY(1,1) NOT NULL,
	[Text] [nvarchar](max) NULL,
	[LanguageCode] [varchar](10) NULL,
	[SequenceId] [int] NULL,
	[TickerId] [int] NULL,
	[IsShow] [bit] NULL,
	[RepeatCount] [int] NULL,
	[Severity] [int] NULL,
	[Frequency] [int] NULL,
	[CreationDate] [datetime] NULL,
	[LastUpdatedDate] [datetime] NULL,
	[IsActive] [bit] NULL,
	[CreatedBy] [int] NULL,
	[OperatorNumber] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[TickerLineId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Program]    Script Date: 10/1/2016 12:38:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Program](
	[ProgramId] [int] NOT NULL,
	[Name] [varchar](500) NOT NULL,
	[ChannelId] [int] NULL,
	[CreationDate] [datetime] NOT NULL,
	[LastUpdateDate] [datetime] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[IsApproved] [bit] NOT NULL,
	[MinStoryCount] [int] NULL,
	[MaxStoryCount] [int] NULL,
	[ProgramTypeId] [int] NULL,
	[BucketID] [int] NULL,
	[FilterId] [int] NULL,
	[ApiKey] [varchar](50) NULL,
 CONSTRAINT [PK_Program] PRIMARY KEY CLUSTERED 
(
	[ProgramId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ProgramConfiguration]    Script Date: 10/1/2016 12:38:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[ProgramConfiguration](
	[ProgramConfigurationId] [int] IDENTITY(1,1) NOT NULL,
	[ProgramId] [int] NOT NULL,
	[SegmentTypeId] [int] NOT NULL,
	[Url] [varchar](500) NULL,
 CONSTRAINT [PK_ProgramConfiguration] PRIMARY KEY CLUSTERED 
(
	[ProgramConfigurationId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ProgramSchedule]    Script Date: 10/1/2016 12:38:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ProgramSchedule](
	[ProgramScheduleId] [int] IDENTITY(1,1) NOT NULL,
	[WeekDayId] [int] NOT NULL,
	[StartTime] [time](7) NOT NULL,
	[EndTime] [time](7) NOT NULL,
	[ProgramId] [int] NOT NULL,
	[RecurringTypeId] [int] NOT NULL,
	[CreationDate] [datetime] NOT NULL,
	[LastUpdateDate] [datetime] NOT NULL,
	[IsActive] [bit] NOT NULL,
 CONSTRAINT [PK_ProgramSchedule] PRIMARY KEY CLUSTERED 
(
	[ProgramScheduleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ProgramScreenElement]    Script Date: 10/1/2016 12:38:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ProgramScreenElement](
	[ProgramScreenElementId] [int] IDENTITY(1,1) NOT NULL,
	[ScreenElementId] [int] NOT NULL,
	[ImageGuid] [uniqueidentifier] NOT NULL,
	[ProgramId] [int] NOT NULL,
	[CreationDate] [datetime] NOT NULL,
	[LastUpdateDate] [datetime] NOT NULL,
	[IsActive] [bit] NOT NULL,
 CONSTRAINT [PK_ProgramScreenElement] PRIMARY KEY CLUSTERED 
(
	[ProgramScreenElementId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ProgramSegment]    Script Date: 10/1/2016 12:38:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ProgramSegment](
	[ProgramSegmentId] [int] IDENTITY(1,1) NOT NULL,
	[ProgramId] [int] NOT NULL,
	[SegmentTypeId] [int] NOT NULL,
	[SequenceNumber] [int] NOT NULL,
	[Duration] [float] NULL,
	[StoryCount] [int] NULL,
	[Name] [varchar](100) NULL,
	[CreationDate] [datetime] NOT NULL,
	[LastUpdatedDate] [datetime] NOT NULL,
	[IsActive] [bit] NOT NULL,
 CONSTRAINT [PK_ProgramSegment] PRIMARY KEY CLUSTERED 
(
	[ProgramSegmentId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ProgramSetMapping]    Script Date: 10/1/2016 12:38:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ProgramSetMapping](
	[ProgramSetMappingId] [int] NOT NULL,
	[ProgramId] [int] NOT NULL,
	[SetId] [int] NOT NULL,
	[CreationDate] [datetime] NULL,
	[LastUpdateDate] [datetime] NULL,
	[IsActive] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[ProgramSetMappingId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ProgramType]    Script Date: 10/1/2016 12:38:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[ProgramType](
	[ProgramTypeId] [int] IDENTITY(1,1) NOT NULL,
	[ProgramTypeName] [varchar](250) NULL,
 CONSTRAINT [PK_ProgramType] PRIMARY KEY CLUSTERED 
(
	[ProgramTypeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ProgramTypeMapping]    Script Date: 10/1/2016 12:38:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ProgramTypeMapping](
	[ProgramTypeMappingId] [int] IDENTITY(1,1) NOT NULL,
	[ProgramId] [int] NULL,
	[ProgramTypeId] [int] NULL,
	[CreationDate] [datetime] NULL,
	[LastUpdateDate] [datetime] NULL,
	[IsActive] [bit] NULL,
 CONSTRAINT [PK_ProgramTypeMapping] PRIMARY KEY CLUSTERED 
(
	[ProgramTypeMappingId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[RadioStation]    Script Date: 10/1/2016 12:38:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[RadioStation](
	[RadioStationId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[CreationDate] [datetime] NOT NULL,
	[LastUpdateDate] [datetime] NOT NULL,
	[IsActive] [bit] NOT NULL,
 CONSTRAINT [PK_RadioStation] PRIMARY KEY CLUSTERED 
(
	[RadioStationId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[RadioStream]    Script Date: 10/1/2016 12:38:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[RadioStream](
	[RadioStreamId] [int] IDENTITY(1,1) NOT NULL,
	[RadioStationId] [int] NOT NULL,
	[PhysicalPath] [varchar](500) NOT NULL,
	[ProgramId] [int] NOT NULL,
	[Url] [varchar](500) NOT NULL,
	[To] [datetime] NOT NULL,
	[From] [datetime] NOT NULL,
	[IsProcessed] [bit] NOT NULL,
	[CreationDate] [datetime] NOT NULL,
	[LastUpdateDate] [datetime] NOT NULL,
	[IsActive] [bit] NOT NULL,
 CONSTRAINT [PK_RadioStream] PRIMARY KEY CLUSTERED 
(
	[RadioStreamId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[RecurringType]    Script Date: 10/1/2016 12:38:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[RecurringType](
	[RecurringTypeId] [int] IDENTITY(1,1) NOT NULL,
	[Type] [varchar](50) NOT NULL,
 CONSTRAINT [PK_RecurringType] PRIMARY KEY CLUSTERED 
(
	[RecurringTypeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Reel]    Script Date: 10/1/2016 12:38:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Reel](
	[ReelId] [int] IDENTITY(1,1) NOT NULL,
	[ChannelId] [int] NOT NULL,
	[Path] [varchar](1000) NULL,
	[StatusId] [int] NOT NULL,
	[StartTime] [datetime] NOT NULL,
	[EndTime] [datetime] NOT NULL,
	[CreationDate] [datetime] NOT NULL,
	[OriginalPath] [varchar](1000) NULL,
 CONSTRAINT [PK_Reel] PRIMARY KEY CLUSTERED 
(
	[ReelId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ReelStatus]    Script Date: 10/1/2016 12:38:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ReelStatus](
	[ReelStatusId] [int] IDENTITY(1,1) NOT NULL,
	[Status] [varchar](50) NOT NULL,
 CONSTRAINT [PK_ReelStatus] PRIMARY KEY CLUSTERED 
(
	[ReelStatusId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Resource]    Script Date: 10/1/2016 12:38:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Resource](
	[ResourceId] [int] IDENTITY(1,1) NOT NULL,
	[Guid] [varchar](50) NOT NULL,
	[ResourceTypeId] [int] NOT NULL,
	[CreationDate] [datetime] NOT NULL,
	[LastUpdateDate] [datetime] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[Caption] [nvarchar](max) NULL,
	[Category] [varchar](500) NULL,
	[Location] [varchar](500) NULL,
	[Duration] [float] NULL,
 CONSTRAINT [PK_Resource] PRIMARY KEY CLUSTERED 
(
	[ResourceId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ResourceEditclipinfo]    Script Date: 10/1/2016 12:38:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ResourceEditclipinfo](
	[ResourceEditclipinfoId] [int] IDENTITY(1,1) NOT NULL,
	[NewsResourceEditId] [int] NULL,
	[From] [float] NULL,
	[To] [float] NULL,
	[CreationDate] [datetime] NULL,
	[LastUpdateDate] [datetime] NULL,
	[isactive] [bit] NULL,
 CONSTRAINT [PK_ResourceEditclipinfo] PRIMARY KEY CLUSTERED 
(
	[ResourceEditclipinfoId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ResourceType]    Script Date: 10/1/2016 12:38:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ResourceType](
	[ResourceTypeId] [int] IDENTITY(1,1) NOT NULL,
	[ResourceType] [varchar](50) NOT NULL,
 CONSTRAINT [PK_ResourceType] PRIMARY KEY CLUSTERED 
(
	[ResourceTypeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Role]    Script Date: 10/1/2016 12:38:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Role](
	[RoleId] [int] IDENTITY(1,1) NOT NULL,
	[Role] [varchar](50) NOT NULL,
	[IsActive] [bit] NOT NULL,
 CONSTRAINT [PK_Role] PRIMARY KEY CLUSTERED 
(
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[RunOrderLog]    Script Date: 10/1/2016 12:38:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RunOrderLog](
	[RunOrderLogId] [int] IDENTITY(1,1) NOT NULL,
	[groupId] [int] NULL,
	[messageId] [int] NULL,
	[mosId] [nvarchar](20) NULL,
	[ncsId] [nvarchar](20) NULL,
	[roStoriesCount] [int] NULL,
	[roStatus] [nchar](10) NULL,
	[roId] [int] NULL,
 CONSTRAINT [PK_RunOrderLog] PRIMARY KEY CLUSTERED 
(
	[RunOrderLogId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[RunOrderStories]    Script Date: 10/1/2016 12:38:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RunOrderStories](
	[RunOrderStoryId] [int] IDENTITY(1,1) NOT NULL,
	[RunOrderLogId] [int] NOT NULL,
	[ItemChannel] [nvarchar](20) NULL,
	[ItemId] [int] NULL,
	[ObjId] [int] NULL,
	[Status] [nchar](20) NULL,
	[StoryId] [int] NULL,
	[StoryStatus] [nchar](20) NULL,
	[SequenceId] [int] NULL,
	[Slug] [nvarchar](500) NULL,
 CONSTRAINT [PK_RunOrderStories] PRIMARY KEY CLUSTERED 
(
	[RunOrderStoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[RunOrderStoryDetail]    Script Date: 10/1/2016 12:38:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RunOrderStoryDetail](
	[RoStoryItemId] [int] IDENTITY(1,1) NOT NULL,
	[Instructions] [nvarchar](max) NULL,
	[Script] [nvarchar](max) NULL,
	[Play] [bit] NULL,
	[RoStoryId] [int] NULL,
	[Slug] [nvarchar](max) NULL,
	[SequenceId] [int] NULL,
 CONSTRAINT [PK_RunOrderStoryDetail] PRIMARY KEY CLUSTERED 
(
	[RoStoryItemId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[schema_info]    Script Date: 10/1/2016 12:38:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[schema_info](
	[version] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[version] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ScrapMaxDates]    Script Date: 10/1/2016 12:38:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ScrapMaxDates](
	[ScrapMaxDatesId] [int] IDENTITY(1,1) NOT NULL,
	[MaxUpdateDate] [datetime] NOT NULL,
	[SourceName] [varchar](500) NOT NULL,
 CONSTRAINT [PK_ScrapMaxDates] PRIMARY KEY CLUSTERED 
(
	[ScrapMaxDatesId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ScreenElement]    Script Date: 10/1/2016 12:38:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ScreenElement](
	[ScreenElementId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[ThumbGuid] [uniqueidentifier] NOT NULL,
	[ImageGuid] [uniqueidentifier] NULL,
	[AllowDisplay] [bit] NOT NULL,
	[CreationDate] [datetime] NOT NULL,
	[LastUpdateDate] [datetime] NOT NULL,
	[IsActive] [bit] NOT NULL,
 CONSTRAINT [PK_ScreenElement] PRIMARY KEY CLUSTERED 
(
	[ScreenElementId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ScreenTemplate]    Script Date: 10/1/2016 12:38:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ScreenTemplate](
	[ScreenTemplateId] [int] NOT NULL,
	[ProgramId] [int] NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[IsDefault] [bit] NOT NULL,
	[Html] [nvarchar](max) NOT NULL,
	[Description] [varchar](255) NULL,
	[Duration] [int] NULL,
	[ThumbGuid] [uniqueidentifier] NOT NULL,
	[GroupId] [varchar](50) NOT NULL,
	[AssetId] [varchar](50) NULL,
	[BackgroundColor] [varchar](50) NULL,
	[BackgroundImageUrl] [uniqueidentifier] NULL,
	[BackgroundRepeat] [varchar](50) NULL,
	[CreatonDate] [datetime] NOT NULL,
	[LastUpdateDate] [datetime] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[flashtemplateid] [int] NOT NULL,
	[RatioTypeId] [int] NULL,
	[IsVideoWallTemplate] [bit] NULL,
 CONSTRAINT [PK_ScreenTemplate] PRIMARY KEY CLUSTERED 
(
	[ScreenTemplateId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ScreenTemplatekey]    Script Date: 10/1/2016 12:38:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ScreenTemplatekey](
	[ScreenTemplatekeyId] [int] IDENTITY(1,1) NOT NULL,
	[ScreenTemplateId] [int] NULL,
	[FlashTemplateKeyId] [int] NULL,
	[KeyName] [nvarchar](500) NULL,
	[creationDate] [datetime] NULL,
	[updateddate] [datetime] NULL,
	[IsActive] [bit] NULL,
	[Top] [float] NULL,
	[Bottom] [float] NULL,
	[left] [float] NULL,
	[Right] [float] NULL,
 CONSTRAINT [PK_ScreenTemplatekey] PRIMARY KEY CLUSTERED 
(
	[ScreenTemplatekeyId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ScreenTemplateStatus]    Script Date: 10/1/2016 12:38:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ScreenTemplateStatus](
	[ScreenTemplateStatusId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](100) NULL,
 CONSTRAINT [PK_ScreenTemplateStatus] PRIMARY KEY CLUSTERED 
(
	[ScreenTemplateStatusId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Script]    Script Date: 10/1/2016 12:38:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[Script](
	[ScriptId] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NOT NULL,
	[Script] [text] NOT NULL,
	[IsExecuted] [bit] NOT NULL,
	[CreationDate] [datetime] NOT NULL,
	[LastUpdateDate] [datetime] NOT NULL,
	[Token] [varchar](50) NULL,
 CONSTRAINT [PK_Script] PRIMARY KEY CLUSTERED 
(
	[ScriptId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Segment]    Script Date: 10/1/2016 12:38:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Segment](
	[SegmentId] [int] IDENTITY(1,1) NOT NULL,
	[SegmentTypeId] [int] NOT NULL,
	[EpisodeId] [int] NOT NULL,
	[SequnceNumber] [int] NOT NULL,
	[Duration] [float] NULL,
	[StoryCount] [int] NULL,
	[CreationDate] [datetime] NOT NULL,
	[LastUpdateDate] [datetime] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[Name] [varchar](1000) NULL,
 CONSTRAINT [PK_Segment] PRIMARY KEY CLUSTERED 
(
	[SegmentId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[SegmentType]    Script Date: 10/1/2016 12:38:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SegmentType](
	[SegmentTypeId] [int] IDENTITY(1,1) NOT NULL,
	[Segment] [varchar](50) NOT NULL,
 CONSTRAINT [PK_SegmentType] PRIMARY KEY CLUSTERED 
(
	[SegmentTypeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[SetWallMapping]    Script Date: 10/1/2016 12:38:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SetWallMapping](
	[SetWallMappingId] [int] NOT NULL,
	[SetId] [int] NOT NULL,
	[WallId] [int] NOT NULL,
	[RatioTypeId] [int] NOT NULL,
	[CreationDate] [datetime] NOT NULL,
	[LastUpdateDate] [datetime] NOT NULL,
	[IsActive] [bit] NOT NULL,
 CONSTRAINT [PK_SetWallMapping] PRIMARY KEY CLUSTERED 
(
	[SetWallMappingId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Slot]    Script Date: 10/1/2016 12:38:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Slot](
	[SlotId] [int] IDENTITY(1,1) NOT NULL,
	[NewsGuid] [varchar](50) NULL,
	[Title] [nvarchar](500) NULL,
	[Description] [nvarchar](max) NULL,
	[ThumbGuid] [uniqueidentifier] NULL,
	[SequnceNumber] [int] NOT NULL,
	[SlotTypeId] [int] NOT NULL,
	[SegmentId] [int] NOT NULL,
	[CategoryId] [int] NULL,
	[CreationDate] [datetime] NOT NULL,
	[LastUpdateDate] [datetime] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[TranslatedDescription] [nvarchar](max) NULL,
	[TranslatedTitle] [nvarchar](1000) NULL,
	[LanguageCode] [varchar](10) NULL,
	[PreviewGuid] [uniqueidentifier] NULL,
	[UserId] [int] NULL,
	[TickerId] [int] NULL,
	[IncludeInRundown] [bit] NULL,
 CONSTRAINT [PK_Story] PRIMARY KEY CLUSTERED 
(
	[SlotId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[SlotScreenTemplate]    Script Date: 10/1/2016 12:38:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SlotScreenTemplate](
	[SlotScreenTemplateId] [int] IDENTITY(1,1) NOT NULL,
	[SlotId] [int] NOT NULL,
	[ScreenTemplateId] [int] NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[IsDefault] [bit] NOT NULL,
	[Html] [nvarchar](max) NOT NULL,
	[Description] [varchar](255) NULL,
	[Duration] [int] NULL,
	[ThumbGuid] [uniqueidentifier] NOT NULL,
	[GroupId] [varchar](50) NOT NULL,
	[AssetId] [varchar](50) NULL,
	[BackgroundColor] [varchar](50) NULL,
	[BackgroundImageUrl] [uniqueidentifier] NULL,
	[BackgroundRepeat] [varchar](50) NULL,
	[CreatonDate] [datetime] NOT NULL,
	[LastUpdateDate] [datetime] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[Script] [nvarchar](max) NULL,
	[SequenceNumber] [int] NULL,
	[IsAssignedToNLE] [bit] NULL,
	[IsAssignedToStoryWriter] [bit] NULL,
	[VideoDuration] [int] NULL,
	[ScriptDuration] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
SET ANSI_PADDING OFF
ALTER TABLE [dbo].[SlotScreenTemplate] ADD [Instructions] [varchar](max) NULL
ALTER TABLE [dbo].[SlotScreenTemplate] ADD [ParentId] [int] NULL
ALTER TABLE [dbo].[SlotScreenTemplate] ADD [VideoWallId] [int] NULL
ALTER TABLE [dbo].[SlotScreenTemplate] ADD [IsVideoWallTemplate] [bit] NULL
ALTER TABLE [dbo].[SlotScreenTemplate] ADD [StatusId] [int] NULL
ALTER TABLE [dbo].[SlotScreenTemplate] ADD [NLEStatusId] [int] NULL
ALTER TABLE [dbo].[SlotScreenTemplate] ADD [StoryWriterStatusId] [int] NULL
 CONSTRAINT [PK_StoryScreenTemplate] PRIMARY KEY CLUSTERED 
(
	[SlotScreenTemplateId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[SlotScreenTemplateCamera]    Script Date: 10/1/2016 12:38:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SlotScreenTemplateCamera](
	[SlotScreenTemplateCameraId] [int] IDENTITY(1,1) NOT NULL,
	[SlotScreenTemplateId] [int] NOT NULL,
	[CameraTypeId] [int] NOT NULL,
	[Name] [varchar](255) NOT NULL,
	[IsOn] [bit] NOT NULL,
	[CreationDate] [datetime] NOT NULL,
	[LastUpdateDate] [datetime] NOT NULL,
 CONSTRAINT [PK_SlotScreenTemplateCamera] PRIMARY KEY CLUSTERED 
(
	[SlotScreenTemplateCameraId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[SlotScreenTemplatekey]    Script Date: 10/1/2016 12:38:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[SlotScreenTemplatekey](
	[SlotScreenTemplatekeyId] [int] IDENTITY(1,1) NOT NULL,
	[ScreenTemplatekeyId] [int] NULL,
	[SlotScreenTemplateId] [int] NULL,
	[KeyName] [nvarchar](500) NULL,
	[KeyValue] [nvarchar](max) NULL,
	[creationDate] [datetime] NULL,
	[updateddate] [datetime] NULL,
	[IsActive] [bit] NULL,
	[languagecode] [varchar](100) NULL,
	[Top] [float] NULL,
	[Bottom] [float] NULL,
	[left] [float] NULL,
	[Right] [float] NULL,
 CONSTRAINT [PK_SlotScreenTemplatekey] PRIMARY KEY CLUSTERED 
(
	[SlotScreenTemplatekeyId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[SlotScreenTemplateMic]    Script Date: 10/1/2016 12:38:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SlotScreenTemplateMic](
	[SlotScreenTemplateMicId] [int] IDENTITY(1,1) NOT NULL,
	[SlotScreenTemplateId] [int] NOT NULL,
	[MicNo] [int] NOT NULL,
	[IsOn] [bit] NOT NULL,
	[CreationDate] [datetime] NOT NULL,
	[LastUpdateDate] [datetime] NOT NULL,
 CONSTRAINT [PK_SlotScreenTemplateMic] PRIMARY KEY CLUSTERED 
(
	[SlotScreenTemplateMicId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[SlotScreenTemplateResource]    Script Date: 10/1/2016 12:38:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SlotScreenTemplateResource](
	[SlotScreenTemplateResourceId] [int] IDENTITY(1,1) NOT NULL,
	[SlotScreenTemplateId] [int] NOT NULL,
	[ResourceGuid] [uniqueidentifier] NOT NULL,
	[Comment] [nvarchar](max) NULL,
	[IsUploadedFromNLE] [bit] NOT NULL,
	[CreationDate] [datetime] NOT NULL,
	[Caption] [nvarchar](max) NULL,
	[Category] [varchar](500) NULL,
	[Location] [varchar](500) NULL,
	[Duration] [decimal](18, 0) NULL,
	[ResourceTypeId] [int] NULL,
 CONSTRAINT [PK_SlotScreenTemplateResource] PRIMARY KEY CLUSTERED 
(
	[SlotScreenTemplateResourceId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[SlotTemplateScreenElement]    Script Date: 10/1/2016 12:38:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SlotTemplateScreenElement](
	[SlotTemplateScreenElementId] [int] IDENTITY(1,1) NOT NULL,
	[ScreenElementId] [int] NOT NULL,
	[SlotScreenTemplateId] [int] NOT NULL,
	[Top] [float] NOT NULL,
	[Bottom] [float] NOT NULL,
	[Left] [float] NOT NULL,
	[Right] [float] NOT NULL,
	[ZIndex] [float] NOT NULL,
	[AssetId] [varchar](50) NULL,
	[ResourceGuid] [uniqueidentifier] NULL,
	[CreationDate] [datetime] NOT NULL,
	[LastUpdateDate] [datetime] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[CelebrityId] [int] NULL,
	[flashtemplatewindowid] [int] NULL,
 CONSTRAINT [PK_StoryTemplateScreenElement] PRIMARY KEY CLUSTERED 
(
	[SlotTemplateScreenElementId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[SlotTemplateScreenElementResource]    Script Date: 10/1/2016 12:38:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SlotTemplateScreenElementResource](
	[SlotTemplateScreenElementResourceId] [int] IDENTITY(1,1) NOT NULL,
	[SlotTemplateScreenElementId] [int] NULL,
	[SlotScreenTemplateId] [int] NULL,
	[ResourceGuid] [varchar](100) NULL,
	[CreationDate] [datetime] NULL,
	[LastUpdateDate] [datetime] NULL,
	[IsActive] [bit] NULL,
	[Caption] [nvarchar](max) NULL,
	[Category] [varchar](500) NULL,
	[Location] [varchar](500) NULL,
	[Duration] [decimal](18, 2) NULL,
	[ResourceTypeId] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[SlotTemplateScreenElementResourceId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[SlotType]    Script Date: 10/1/2016 12:38:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SlotType](
	[SlotTypeId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](255) NOT NULL,
 CONSTRAINT [PK_SlotType] PRIMARY KEY CLUSTERED 
(
	[SlotTypeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Slug]    Script Date: 10/1/2016 12:38:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Slug](
	[SlugId] [int] IDENTITY(1,1) NOT NULL,
	[Slug] [nvarchar](max) NULL,
	[CreationDate] [datetime] NULL,
	[LastUpdateDate] [datetime] NULL,
	[IsActive] [bit] NULL,
 CONSTRAINT [PK_Slug] PRIMARY KEY CLUSTERED 
(
	[SlugId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[SocialMedia]    Script Date: 10/1/2016 12:38:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SocialMedia](
	[SocialMediaId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[CreationDate] [datetime] NOT NULL,
	[LastUpdateDate] [datetime] NOT NULL,
	[IsActive] [bit] NOT NULL,
 CONSTRAINT [PK_SocialMedia] PRIMARY KEY CLUSTERED 
(
	[SocialMediaId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[SocialMediaAccount]    Script Date: 10/1/2016 12:38:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SocialMediaAccount](
	[SocialMediaAccountId] [int] IDENTITY(1,1) NOT NULL,
	[UserName] [nvarchar](255) NULL,
	[CelebrityId] [int] NULL,
	[Url] [nvarchar](500) NULL,
	[CreationDate] [datetime] NULL,
	[LastUpdateDate] [datetime] NULL,
	[IsActive] [bit] NULL,
	[IsFollowed] [bit] NULL,
	[Type] [nvarchar](50) NULL,
	[SocialMediaType] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[SocialMediaAccountId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[SuggestedFlow]    Script Date: 10/1/2016 12:38:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[SuggestedFlow](
	[SuggestedFlowId] [int] IDENTITY(1,1) NOT NULL,
	[Key] [varchar](max) NULL,
	[ProgramId] [int] NULL,
	[EpisodeId] [int] NULL,
	[CreationDate] [datetime] NULL,
	[LastUpdatedDate] [datetime] NULL,
	[IsActive] [bit] NULL,
 CONSTRAINT [PK_SuggestedFlow] PRIMARY KEY CLUSTERED 
(
	[SuggestedFlowId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tag]    Script Date: 10/1/2016 12:38:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tag](
	[TagId] [int] IDENTITY(1,1) NOT NULL,
	[Rank] [float] NOT NULL,
	[Tag] [nvarchar](500) NOT NULL,
	[Guid] [varchar](50) NULL,
	[CreationDate] [datetime] NOT NULL,
	[LastUpdateDate] [datetime] NOT NULL,
	[IsActive] [bit] NOT NULL,
 CONSTRAINT [PK_Tag] PRIMARY KEY CLUSTERED 
(
	[TagId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TagResource]    Script Date: 10/1/2016 12:38:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TagResource](
	[TagResourceid] [int] IDENTITY(1,1) NOT NULL,
	[TagId] [int] NULL,
	[ResourceId] [int] NULL,
	[CreationDate] [datetime] NULL,
	[LastUpdateDate] [datetime] NULL,
	[isactive] [bit] NULL,
 CONSTRAINT [PK_TagResource] PRIMARY KEY CLUSTERED 
(
	[TagResourceid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Team]    Script Date: 10/1/2016 12:38:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Team](
	[TeamId] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NOT NULL,
	[WorkRoleId] [int] NOT NULL,
	[ProgramId] [int] NULL,
	[TeamRoleId] [int] NOT NULL,
	[CreationDate] [datetime] NOT NULL,
	[LastUpdateDate] [datetime] NOT NULL,
	[IsActive] [bit] NOT NULL,
 CONSTRAINT [PK_Team] PRIMARY KEY CLUSTERED 
(
	[TeamId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TeamRole]    Script Date: 10/1/2016 12:38:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TeamRole](
	[TeamRoleId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NOT NULL,
 CONSTRAINT [PK_TeamRole] PRIMARY KEY CLUSTERED 
(
	[TeamRoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TemplateScreenElement]    Script Date: 10/1/2016 12:38:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TemplateScreenElement](
	[TemplateScreenElementId] [int] IDENTITY(1,1) NOT NULL,
	[ScreenElementId] [int] NOT NULL,
	[Top] [float] NOT NULL,
	[Bottom] [float] NOT NULL,
	[Left] [float] NOT NULL,
	[Right] [float] NOT NULL,
	[ZIndex] [float] NOT NULL,
	[CreationDate] [datetime] NOT NULL,
	[LastUpdateDate] [datetime] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[ScreenTemplateId] [int] NULL,
	[flashtemplatewindowid] [int] NULL,
 CONSTRAINT [PK_TemplateScreenElement] PRIMARY KEY CLUSTERED 
(
	[TemplateScreenElementId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Ticker]    Script Date: 10/1/2016 12:38:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[Ticker](
	[TickerId] [int] IDENTITY(1,1) NOT NULL,
	[CategoryId] [int] NULL,
	[NewsGuid] [varchar](50) NULL,
	[UserId] [int] NULL,
	[OnAiredTime] [datetime] NULL,
	[SequenceId] [int] NULL,
	[CreationDate] [datetime] NULL,
	[LastUpdatedDate] [datetime] NULL,
	[IsActive] [bit] NULL,
	[CreatedBy] [int] NULL,
	[TickerGroupName] [nvarchar](max) NULL,
	[LocationId] [int] NULL,
	[OnAirRefId] [int] NULL,
 CONSTRAINT [PK_Ticker] PRIMARY KEY CLUSTERED 
(
	[TickerId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tickerbackup]    Script Date: 10/1/2016 12:38:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[tickerbackup](
	[TickerId] [int] IDENTITY(1,1) NOT NULL,
	[CategoryId] [int] NULL,
	[NewsGuid] [varchar](50) NULL,
	[UserId] [int] NULL,
	[OnAiredTime] [datetime] NULL,
	[SequenceId] [int] NULL,
	[CreationDate] [datetime] NULL,
	[LastUpdatedDate] [datetime] NULL,
	[IsActive] [bit] NULL,
	[CreatedBy] [int] NULL,
	[TickerGroupName] [nvarchar](max) NULL,
	[LocationId] [int] NULL,
 CONSTRAINT [PK_tickerbackup] PRIMARY KEY CLUSTERED 
(
	[TickerId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TickerCategory]    Script Date: 10/1/2016 12:38:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TickerCategory](
	[TickerCategoryId] [int] IDENTITY(1,1) NOT NULL,
	[CategoryId] [int] NULL,
	[Name] [nvarchar](510) NULL,
	[SequenceNumber] [int] NULL,
	[CreationDate] [datetime] NULL,
	[LastUpdateDate] [datetime] NULL,
	[IsActive] [bit] NULL,
 CONSTRAINT [PK_TickerCategory] PRIMARY KEY CLUSTERED 
(
	[TickerCategoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TickerLine]    Script Date: 10/1/2016 12:38:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[TickerLine](
	[TickerLineId] [int] IDENTITY(1,1) NOT NULL,
	[Text] [nvarchar](max) NULL,
	[LanguageCode] [varchar](50) NULL,
	[TickerStatusId] [int] NULL,
	[TickerId] [int] NULL,
	[CreationDate] [datetime] NULL,
	[LastUpdatedDate] [datetime] NULL,
	[IsActive] [bit] NULL,
	[SequenceId] [int] NULL,
	[BreakingStatusId] [int] NULL,
	[LatestStatusId] [int] NULL,
	[CategoryStatusId] [int] NULL,
	[BreakingSequenceId] [int] NULL,
	[LatestSequenceId] [int] NULL,
	[CategorySequenceId] [int] NULL,
	[RepeatCount] [int] NULL,
	[Severity] [int] NULL,
	[Frequency] [int] NULL,
	[CreatedBy] [int] NULL,
 CONSTRAINT [PK_TickerLine] PRIMARY KEY CLUSTERED 
(
	[TickerLineId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tickerlinebackup]    Script Date: 10/1/2016 12:38:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[tickerlinebackup](
	[TickerLineId] [int] IDENTITY(1,1) NOT NULL,
	[Text] [nvarchar](max) NULL,
	[LanguageCode] [varchar](50) NULL,
	[TickerStatusId] [int] NULL,
	[TickerId] [int] NULL,
	[CreationDate] [datetime] NULL,
	[LastUpdatedDate] [datetime] NULL,
	[IsActive] [bit] NULL,
	[SequenceId] [int] NULL,
	[BreakingStatusId] [int] NULL,
	[LatestStatusId] [int] NULL,
	[CategoryStatusId] [int] NULL,
	[BreakingSequenceId] [int] NULL,
	[LatestSequenceId] [int] NULL,
	[CategorySequenceId] [int] NULL,
	[RepeatCount] [int] NULL,
	[Severity] [int] NULL,
	[Frequency] [int] NULL,
	[CreatedBy] [int] NULL,
 CONSTRAINT [PK_tickerlinebackup] PRIMARY KEY CLUSTERED 
(
	[TickerLineId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TickerOnAirLog]    Script Date: 10/1/2016 12:38:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[TickerOnAirLog](
	[TickerOnAirLogId] [int] IDENTITY(1,1) NOT NULL,
	[TickerId] [varchar](50) NULL,
	[CreationDate] [datetime] NULL,
	[UserId] [int] NULL,
 CONSTRAINT [PK_TickerOnAirLog] PRIMARY KEY CLUSTERED 
(
	[TickerOnAirLogId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TickerStatus]    Script Date: 10/1/2016 12:38:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[TickerStatus](
	[TickerStatusId] [int] IDENTITY(1,1) NOT NULL,
	[TickerStatusName] [varchar](250) NULL,
 CONSTRAINT [PK_TickerStatus] PRIMARY KEY CLUSTERED 
(
	[TickerStatusId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TwitterAccount]    Script Date: 10/1/2016 12:38:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TwitterAccount](
	[TwitterAccountId] [int] IDENTITY(1,1) NOT NULL,
	[UserName] [nvarchar](255) NOT NULL,
	[CelebrityId] [int] NOT NULL,
	[Url] [varchar](500) NULL,
	[CreationDate] [datetime] NOT NULL,
	[LastUpdateDate] [datetime] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[IsFollowed] [bit] NULL,
 CONSTRAINT [PK_TwitterAccount] PRIMARY KEY CLUSTERED 
(
	[TwitterAccountId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[User]    Script Date: 10/1/2016 12:38:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[User](
	[UserId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](255) NOT NULL,
	[Login] [varchar](50) NOT NULL,
	[Password] [nvarchar](50) NOT NULL,
	[CreationDate] [datetime] NOT NULL,
	[LastUpdateDate] [datetime] NOT NULL,
	[IsActive] [bit] NOT NULL,
 CONSTRAINT [PK_User] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[UserRole]    Script Date: 10/1/2016 12:38:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserRole](
	[UserRoleId] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NOT NULL,
	[RoleId] [int] NOT NULL,
	[CreationDate] [datetime] NOT NULL,
	[LastUpdateDate] [datetime] NOT NULL,
	[IsActive] [bit] NOT NULL,
 CONSTRAINT [PK_UserRole] PRIMARY KEY CLUSTERED 
(
	[UserRoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[VideoCutterIp]    Script Date: 10/1/2016 12:38:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[VideoCutterIp](
	[VideoCutterIpId] [int] IDENTITY(1,1) NOT NULL,
	[UserIP] [varchar](100) NOT NULL,
	[Host] [varchar](max) NULL,
	[VideoCutterVersion] [varchar](20) NULL,
	[CreationDate] [datetime] NULL,
	[LastUpdateDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[VideoCutterIpId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Website]    Script Date: 10/1/2016 12:38:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Website](
	[WebsiteId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](255) NOT NULL,
	[Url] [varchar](255) NULL,
	[CreationDate] [datetime] NOT NULL,
	[LastUpdateDate] [datetime] NOT NULL,
	[IsActive] [bit] NOT NULL,
 CONSTRAINT [PK_Website] PRIMARY KEY CLUSTERED 
(
	[WebsiteId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Wire]    Script Date: 10/1/2016 12:38:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Wire](
	[WireId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[CreationDate] [datetime] NOT NULL,
	[LastUpdateDate] [datetime] NOT NULL,
	[IsActive] [bit] NOT NULL,
 CONSTRAINT [PK_Wire] PRIMARY KEY CLUSTERED 
(
	[WireId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[WorkRoleFolder]    Script Date: 10/1/2016 12:38:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[WorkRoleFolder](
	[WorkRoleFolderId] [int] IDENTITY(1,1) NOT NULL,
	[WorkRoleId] [int] NOT NULL,
	[FolderId] [int] NOT NULL,
	[AllowRead] [bit] NOT NULL,
	[AllowModify] [bit] NOT NULL,
	[AllowCreate] [bit] NOT NULL,
	[AllowCopy] [bit] NOT NULL,
	[AllowMove] [bit] NOT NULL,
	[AllowDelete] [bit] NOT NULL,
	[CreationDate] [datetime] NOT NULL,
	[LastUpdateDate] [datetime] NOT NULL,
	[MaxColor] [int] NOT NULL,
 CONSTRAINT [PK_WorkRoleFolder] PRIMARY KEY CLUSTERED 
(
	[WorkRoleFolderId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  View [dbo].[MCRBusinessTicker]    Script Date: 10/1/2016 12:38:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[MCRBusinessTicker]
AS
SELECT        MCRCategoryTickerRundownId, TickerId, Text, CategoryName, CategoryId, SequenceNumber, LanguageCode, CreationDate, TickerLineId
FROM            dbo.MCRCategoryTickerRundown
WHERE        (CategoryName = 'Business')

GO
/****** Object:  View [dbo].[MCRInternationalTicker]    Script Date: 10/1/2016 12:38:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[MCRInternationalTicker]
AS
SELECT        MCRCategoryTickerRundownId, TickerId, Text, CategoryName, CategoryId, SequenceNumber, LanguageCode, CreationDate, TickerLineId
FROM            dbo.MCRCategoryTickerRundown
WHERE        (CategoryName = 'International')

GO
/****** Object:  View [dbo].[MCRLocalTicker]    Script Date: 10/1/2016 12:38:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[MCRLocalTicker]
AS
SELECT        MCRCategoryTickerRundownId, TickerId, Text, CategoryName, CategoryId, SequenceNumber, LanguageCode, CreationDate, TickerLineId
FROM            dbo.MCRCategoryTickerRundown
WHERE        (CategoryName = 'Local')

GO
/****** Object:  View [dbo].[MCRSportsTicker]    Script Date: 10/1/2016 12:38:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[MCRSportsTicker]
AS
SELECT        MCRCategoryTickerRundownId, TickerId, Text, CategoryName, CategoryId, SequenceNumber, LanguageCode, CreationDate, TickerLineId
FROM            dbo.MCRCategoryTickerRundown
WHERE        (CategoryName = 'Sports')

GO
/****** Object:  View [dbo].[MCRWeatherTicker]    Script Date: 10/1/2016 12:38:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[MCRWeatherTicker]
AS
SELECT        MCRCategoryTickerRundownId, TickerId, Text, CategoryName, CategoryId, SequenceNumber, LanguageCode, CreationDate, TickerLineId
FROM            dbo.MCRCategoryTickerRundown
WHERE        (CategoryName = 'Weather')

GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_BunchFilterNews_Bunch_Filter_Date]    Script Date: 10/1/2016 12:38:49 AM ******/
CREATE NONCLUSTERED INDEX [IX_BunchFilterNews_Bunch_Filter_Date] ON [dbo].[BunchFilterNews]
(
	[BunchGuid] ASC,
	[FilterId] ASC,
	[LastUpdateDate] DESC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_News_BunchGuid]    Script Date: 10/1/2016 12:38:49 AM ******/
CREATE NONCLUSTERED INDEX [IX_News_BunchGuid] ON [dbo].[News]
(
	[BunchGuid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_News_Guid]    Script Date: 10/1/2016 12:38:49 AM ******/
CREATE NONCLUSTERED INDEX [IX_News_Guid] ON [dbo].[News]
(
	[Guid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_News_IsArchival]    Script Date: 10/1/2016 12:38:49 AM ******/
CREATE NONCLUSTERED INDEX [IX_News_IsArchival] ON [dbo].[News]
(
	[IsArchival] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_News_Last_UpdatedDate]    Script Date: 10/1/2016 12:38:49 AM ******/
CREATE NONCLUSTERED INDEX [IX_News_Last_UpdatedDate] ON [dbo].[News]
(
	[LastUpdateDate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_News_Source]    Script Date: 10/1/2016 12:38:49 AM ******/
CREATE NONCLUSTERED INDEX [IX_News_Source] ON [dbo].[News]
(
	[Source] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_NewsCategory_NewsGuid]    Script Date: 10/1/2016 12:38:49 AM ******/
CREATE NONCLUSTERED INDEX [IX_NewsCategory_NewsGuid] ON [dbo].[NewsCategory]
(
	[NewsGuid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_NewsFilter_NewsGuid]    Script Date: 10/1/2016 12:38:49 AM ******/
CREATE NONCLUSTERED INDEX [IX_NewsFilter_NewsGuid] ON [dbo].[NewsFilter]
(
	[NewsGuid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_NewsLocation_NewsGuid]    Script Date: 10/1/2016 12:38:49 AM ******/
CREATE NONCLUSTERED INDEX [IX_NewsLocation_NewsGuid] ON [dbo].[NewsLocation]
(
	[NewsGuid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_NewsResource_NewsGuid]    Script Date: 10/1/2016 12:38:49 AM ******/
CREATE NONCLUSTERED INDEX [IX_NewsResource_NewsGuid] ON [dbo].[NewsResource]
(
	[NewsGuid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_NewsResource_ResourcerGuid]    Script Date: 10/1/2016 12:38:49 AM ******/
CREATE NONCLUSTERED INDEX [IX_NewsResource_ResourcerGuid] ON [dbo].[NewsResource]
(
	[ResourceGuid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_NewsTag_NewsGuid]    Script Date: 10/1/2016 12:38:49 AM ******/
CREATE NONCLUSTERED INDEX [IX_NewsTag_NewsGuid] ON [dbo].[NewsTag]
(
	[NewsGuid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CasperTemplateItem] ADD  CONSTRAINT [DF_CasperTemplateItem_Duration]  DEFAULT ((0)) FOR [Duration]
GO
ALTER TABLE [dbo].[CasperTemplateItem] ADD  CONSTRAINT [DF_CasperTemplateItem_triggeronnext]  DEFAULT ((0)) FOR [Triggeronnext]
GO
ALTER TABLE [dbo].[CategoryAlias] ADD  CONSTRAINT [DF_CategoryAlias_CreationDate]  DEFAULT (getutcdate()) FOR [CreationDate]
GO
ALTER TABLE [dbo].[CategoryAlias] ADD  CONSTRAINT [DF_CategoryAlias_LastUpdateDate]  DEFAULT (getutcdate()) FOR [LastUpdateDate]
GO
ALTER TABLE [dbo].[CategoryAlias] ADD  CONSTRAINT [DF_CategoryAlias_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[ELMAH_Error] ADD  CONSTRAINT [DF_ELMAH_Error_ErrorId]  DEFAULT (newid()) FOR [ErrorId]
GO
ALTER TABLE [dbo].[MCRBreakingTickerRundown] ADD  CONSTRAINT [DF_MCRBreakingTickerRundown_LanguageCode]  DEFAULT ('ur') FOR [LanguageCode]
GO
ALTER TABLE [dbo].[News] ADD  CONSTRAINT [DF_News_IsIndexed]  DEFAULT ((0)) FOR [IsIndexed]
GO
ALTER TABLE [dbo].[News] ADD  CONSTRAINT [News_AddedToRundownCount_Default]  DEFAULT ((0)) FOR [AddedToRundownCount]
GO
ALTER TABLE [dbo].[News] ADD  CONSTRAINT [News_OnAirCount_Default]  DEFAULT ((0)) FOR [OnAirCount]
GO
ALTER TABLE [dbo].[News] ADD  CONSTRAINT [News_OtherChannelExecutionCount_Default]  DEFAULT ((0)) FOR [OtherChannelExecutionCount]
GO
ALTER TABLE [dbo].[News] ADD  CONSTRAINT [News_NewsTickerCount_Default]  DEFAULT ((0)) FOR [NewsTickerCount]
GO
ALTER TABLE [dbo].[News] ADD  CONSTRAINT [News_IsArchival_Default]  DEFAULT ((0)) FOR [IsArchival]
GO
ALTER TABLE [dbo].[NewsFilter] ADD  CONSTRAINT [DF_NewsFilter_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[BunchFilter]  WITH CHECK ADD  CONSTRAINT [FK_BunchFilter_Bunch] FOREIGN KEY([BunchId])
REFERENCES [dbo].[Bunch] ([BunchId])
GO
ALTER TABLE [dbo].[BunchFilter] CHECK CONSTRAINT [FK_BunchFilter_Bunch]
GO
ALTER TABLE [dbo].[BunchFilter]  WITH CHECK ADD  CONSTRAINT [FK_BunchFilter_Filter] FOREIGN KEY([FilterId])
REFERENCES [dbo].[Filter] ([FilterId])
GO
ALTER TABLE [dbo].[BunchFilter] CHECK CONSTRAINT [FK_BunchFilter_Filter]
GO
ALTER TABLE [dbo].[BunchNews]  WITH NOCHECK ADD  CONSTRAINT [FK_BunchNews_Bunch] FOREIGN KEY([BunchId])
REFERENCES [dbo].[Bunch] ([BunchId])
GO
ALTER TABLE [dbo].[BunchNews] CHECK CONSTRAINT [FK_BunchNews_Bunch]
GO
ALTER TABLE [dbo].[BunchNews]  WITH NOCHECK ADD  CONSTRAINT [FK_BunchNews_News] FOREIGN KEY([NewsId])
REFERENCES [dbo].[News] ([NewsId])
GO
ALTER TABLE [dbo].[BunchNews] CHECK CONSTRAINT [FK_BunchNews_News]
GO
ALTER TABLE [dbo].[BunchResource]  WITH CHECK ADD  CONSTRAINT [FK_BunchResource_BunchResource] FOREIGN KEY([BunchId])
REFERENCES [dbo].[Bunch] ([BunchId])
GO
ALTER TABLE [dbo].[BunchResource] CHECK CONSTRAINT [FK_BunchResource_BunchResource]
GO
ALTER TABLE [dbo].[BunchResource]  WITH CHECK ADD  CONSTRAINT [FK_BunchResource_Resource] FOREIGN KEY([ResourceId])
REFERENCES [dbo].[Resource] ([ResourceId])
GO
ALTER TABLE [dbo].[BunchResource] CHECK CONSTRAINT [FK_BunchResource_Resource]
GO
ALTER TABLE [dbo].[BunchTag]  WITH CHECK ADD  CONSTRAINT [FK_BunchTag_BunchTag] FOREIGN KEY([BunchId])
REFERENCES [dbo].[Bunch] ([BunchId])
GO
ALTER TABLE [dbo].[BunchTag] CHECK CONSTRAINT [FK_BunchTag_BunchTag]
GO
ALTER TABLE [dbo].[BunchTag]  WITH CHECK ADD  CONSTRAINT [FK_BunchTag_Tag] FOREIGN KEY([TagId])
REFERENCES [dbo].[Tag] ([TagId])
GO
ALTER TABLE [dbo].[BunchTag] CHECK CONSTRAINT [FK_BunchTag_Tag]
GO
ALTER TABLE [dbo].[CategoryAlias]  WITH CHECK ADD  CONSTRAINT [FK_CategoryAlias_Category] FOREIGN KEY([CategoryId])
REFERENCES [dbo].[Category] ([CategoryId])
GO
ALTER TABLE [dbo].[CategoryAlias] CHECK CONSTRAINT [FK_CategoryAlias_Category]
GO
ALTER TABLE [dbo].[Celebrity]  WITH CHECK ADD  CONSTRAINT [FK_Celebrity_Category] FOREIGN KEY([CategoryId])
REFERENCES [dbo].[Category] ([CategoryId])
GO
ALTER TABLE [dbo].[Celebrity] CHECK CONSTRAINT [FK_Celebrity_Category]
GO
ALTER TABLE [dbo].[CelebrityStatistic]  WITH CHECK ADD  CONSTRAINT [FK_Celebrity_CelebrityStatistic] FOREIGN KEY([CelebrityId])
REFERENCES [dbo].[Celebrity] ([CelebrityId])
GO
ALTER TABLE [dbo].[CelebrityStatistic] CHECK CONSTRAINT [FK_Celebrity_CelebrityStatistic]
GO
ALTER TABLE [dbo].[Comment]  WITH NOCHECK ADD  CONSTRAINT [FK_Comment_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[User] ([UserId])
GO
ALTER TABLE [dbo].[Comment] CHECK CONSTRAINT [FK_Comment_UserId]
GO
ALTER TABLE [dbo].[DailyNewsPaper]  WITH CHECK ADD  CONSTRAINT [FK_DailyNewsPaper_DailyNewsPaper] FOREIGN KEY([NewsPaperId])
REFERENCES [dbo].[NewsPaper] ([NewsPaperId])
GO
ALTER TABLE [dbo].[DailyNewsPaper] CHECK CONSTRAINT [FK_DailyNewsPaper_DailyNewsPaper]
GO
ALTER TABLE [dbo].[Episode]  WITH CHECK ADD  CONSTRAINT [FK_Episode_Program] FOREIGN KEY([ProgramId])
REFERENCES [dbo].[Program] ([ProgramId])
GO
ALTER TABLE [dbo].[Episode] CHECK CONSTRAINT [FK_Episode_Program]
GO
ALTER TABLE [dbo].[FacebookAccount]  WITH CHECK ADD  CONSTRAINT [FK_FacebookAccount_Celebrity] FOREIGN KEY([CelebrityId])
REFERENCES [dbo].[Celebrity] ([CelebrityId])
GO
ALTER TABLE [dbo].[FacebookAccount] CHECK CONSTRAINT [FK_FacebookAccount_Celebrity]
GO
ALTER TABLE [dbo].[FileCategory]  WITH CHECK ADD  CONSTRAINT [FK_FileCategory_Category] FOREIGN KEY([CategoryId])
REFERENCES [dbo].[Category] ([CategoryId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[FileCategory] CHECK CONSTRAINT [FK_FileCategory_Category]
GO
ALTER TABLE [dbo].[FileCategory]  WITH CHECK ADD  CONSTRAINT [FK_FileCategory_NewsFile] FOREIGN KEY([NewsFileId])
REFERENCES [dbo].[NewsFile] ([NewsFileId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[FileCategory] CHECK CONSTRAINT [FK_FileCategory_NewsFile]
GO
ALTER TABLE [dbo].[FileDetail]  WITH CHECK ADD  CONSTRAINT [FK_FileDetail_File] FOREIGN KEY([NewsFileId])
REFERENCES [dbo].[NewsFile] ([NewsFileId])
GO
ALTER TABLE [dbo].[FileDetail] CHECK CONSTRAINT [FK_FileDetail_File]
GO
ALTER TABLE [dbo].[FileDetail]  WITH CHECK ADD  CONSTRAINT [FK_FileDetail_Slug] FOREIGN KEY([SlugId])
REFERENCES [dbo].[Slug] ([SlugId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[FileDetail] CHECK CONSTRAINT [FK_FileDetail_Slug]
GO
ALTER TABLE [dbo].[FileFolderHistory]  WITH CHECK ADD  CONSTRAINT [FK_FileFolderHistory_File] FOREIGN KEY([NewsFileId])
REFERENCES [dbo].[NewsFile] ([NewsFileId])
GO
ALTER TABLE [dbo].[FileFolderHistory] CHECK CONSTRAINT [FK_FileFolderHistory_File]
GO
ALTER TABLE [dbo].[FileFolderHistory]  WITH CHECK ADD  CONSTRAINT [FK_FileFolderHistory_Folder] FOREIGN KEY([FolderId])
REFERENCES [dbo].[Folder] ([FolderId])
GO
ALTER TABLE [dbo].[FileFolderHistory] CHECK CONSTRAINT [FK_FileFolderHistory_Folder]
GO
ALTER TABLE [dbo].[FileFolderHistory]  WITH CHECK ADD  CONSTRAINT [FK_FileFolderHistory_Folder1] FOREIGN KEY([FolderId])
REFERENCES [dbo].[Folder] ([FolderId])
GO
ALTER TABLE [dbo].[FileFolderHistory] CHECK CONSTRAINT [FK_FileFolderHistory_Folder1]
GO
ALTER TABLE [dbo].[FileLocation]  WITH CHECK ADD  CONSTRAINT [FK_FileLocation_Location] FOREIGN KEY([LocationId])
REFERENCES [dbo].[Location] ([LocationId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[FileLocation] CHECK CONSTRAINT [FK_FileLocation_Location]
GO
ALTER TABLE [dbo].[FileLocation]  WITH CHECK ADD  CONSTRAINT [FK_FileLocation_NewsFile] FOREIGN KEY([NewsFileId])
REFERENCES [dbo].[NewsFile] ([NewsFileId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[FileLocation] CHECK CONSTRAINT [FK_FileLocation_NewsFile]
GO
ALTER TABLE [dbo].[FileResource]  WITH CHECK ADD  CONSTRAINT [FK_FileResource_File] FOREIGN KEY([NewsFileId])
REFERENCES [dbo].[NewsFile] ([NewsFileId])
GO
ALTER TABLE [dbo].[FileResource] CHECK CONSTRAINT [FK_FileResource_File]
GO
ALTER TABLE [dbo].[FileStatusHistory]  WITH CHECK ADD  CONSTRAINT [FK_FileStatusHistory_File] FOREIGN KEY([NewsFileId])
REFERENCES [dbo].[NewsFile] ([NewsFileId])
GO
ALTER TABLE [dbo].[FileStatusHistory] CHECK CONSTRAINT [FK_FileStatusHistory_File]
GO
ALTER TABLE [dbo].[FileTag]  WITH CHECK ADD  CONSTRAINT [FK_FileTag_NewsFile] FOREIGN KEY([NewsFileId])
REFERENCES [dbo].[NewsFile] ([NewsFileId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[FileTag] CHECK CONSTRAINT [FK_FileTag_NewsFile]
GO
ALTER TABLE [dbo].[FileTag]  WITH CHECK ADD  CONSTRAINT [FK_FileTag_Tag] FOREIGN KEY([TagId])
REFERENCES [dbo].[Tag] ([TagId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[FileTag] CHECK CONSTRAINT [FK_FileTag_Tag]
GO
ALTER TABLE [dbo].[Filter]  WITH CHECK ADD  CONSTRAINT [FK_Filter_FilterType] FOREIGN KEY([FilterTypeId])
REFERENCES [dbo].[FilterType] ([FilterTypeId])
GO
ALTER TABLE [dbo].[Filter] CHECK CONSTRAINT [FK_Filter_FilterType]
GO
ALTER TABLE [dbo].[LocationAlias]  WITH CHECK ADD  CONSTRAINT [FK_LocationAlias_Location] FOREIGN KEY([LocationId])
REFERENCES [dbo].[Location] ([LocationId])
GO
ALTER TABLE [dbo].[LocationAlias] CHECK CONSTRAINT [FK_LocationAlias_Location]
GO
ALTER TABLE [dbo].[MCRTickerRundownStatus]  WITH CHECK ADD  CONSTRAINT [FK_MCRTickerRundownStatus_MCRTickerStatus] FOREIGN KEY([StatusId])
REFERENCES [dbo].[MCRTickerStatus] ([MCRTickerStatusId])
GO
ALTER TABLE [dbo].[MCRTickerRundownStatus] CHECK CONSTRAINT [FK_MCRTickerRundownStatus_MCRTickerStatus]
GO
ALTER TABLE [dbo].[MosActiveEpisode]  WITH CHECK ADD  CONSTRAINT [FK_MosActiveEpisode_EpisodeId] FOREIGN KEY([EpisodeId])
REFERENCES [dbo].[Episode] ([EpisodeId])
GO
ALTER TABLE [dbo].[MosActiveEpisode] CHECK CONSTRAINT [FK_MosActiveEpisode_EpisodeId]
GO
ALTER TABLE [dbo].[MosActiveItem]  WITH CHECK ADD  CONSTRAINT [FK_MosActiveItem_EpisodeId] FOREIGN KEY([EpisodeId])
REFERENCES [dbo].[Episode] ([EpisodeId])
GO
ALTER TABLE [dbo].[MosActiveItem] CHECK CONSTRAINT [FK_MosActiveItem_EpisodeId]
GO
ALTER TABLE [dbo].[News]  WITH CHECK ADD  CONSTRAINT [FK_News_Location] FOREIGN KEY([LocationId])
REFERENCES [dbo].[Location] ([LocationId])
GO
ALTER TABLE [dbo].[News] CHECK CONSTRAINT [FK_News_Location]
GO
ALTER TABLE [dbo].[News]  WITH CHECK ADD  CONSTRAINT [FK_News_NewsType] FOREIGN KEY([NewsTypeId])
REFERENCES [dbo].[NewsType] ([NewsTypeId])
GO
ALTER TABLE [dbo].[News] CHECK CONSTRAINT [FK_News_NewsType]
GO
ALTER TABLE [dbo].[NewsCategory]  WITH NOCHECK ADD  CONSTRAINT [FK_NewsCategory_Category] FOREIGN KEY([CategoryId])
REFERENCES [dbo].[Category] ([CategoryId])
GO
ALTER TABLE [dbo].[NewsCategory] CHECK CONSTRAINT [FK_NewsCategory_Category]
GO
ALTER TABLE [dbo].[NewsCategory]  WITH NOCHECK ADD  CONSTRAINT [FK_NewsCategory_News] FOREIGN KEY([NewsId])
REFERENCES [dbo].[News] ([NewsId])
GO
ALTER TABLE [dbo].[NewsCategory] CHECK CONSTRAINT [FK_NewsCategory_News]
GO
ALTER TABLE [dbo].[NewsFile]  WITH CHECK ADD  CONSTRAINT [FK_File_Folder] FOREIGN KEY([FolderId])
REFERENCES [dbo].[Folder] ([FolderId])
GO
ALTER TABLE [dbo].[NewsFile] CHECK CONSTRAINT [FK_File_Folder]
GO
ALTER TABLE [dbo].[NewsFile]  WITH CHECK ADD  CONSTRAINT [FK_NewsFile_Program] FOREIGN KEY([ProgramId])
REFERENCES [dbo].[Program] ([ProgramId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[NewsFile] CHECK CONSTRAINT [FK_NewsFile_Program]
GO
ALTER TABLE [dbo].[NewsFile]  WITH CHECK ADD  CONSTRAINT [FK_NewsFile_Slug] FOREIGN KEY([SlugId])
REFERENCES [dbo].[Slug] ([SlugId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[NewsFile] CHECK CONSTRAINT [FK_NewsFile_Slug]
GO
ALTER TABLE [dbo].[NewsFilter]  WITH NOCHECK ADD  CONSTRAINT [FK_NewsFilter_Filter] FOREIGN KEY([FilterId])
REFERENCES [dbo].[Filter] ([FilterId])
GO
ALTER TABLE [dbo].[NewsFilter] CHECK CONSTRAINT [FK_NewsFilter_Filter]
GO
ALTER TABLE [dbo].[NewsLocation]  WITH NOCHECK ADD  CONSTRAINT [FK_NewsLocation_LocationId] FOREIGN KEY([LocationId])
REFERENCES [dbo].[Location] ([LocationId])
GO
ALTER TABLE [dbo].[NewsLocation] CHECK CONSTRAINT [FK_NewsLocation_LocationId]
GO
ALTER TABLE [dbo].[NewsLocation]  WITH NOCHECK ADD  CONSTRAINT [FK_NewsLocation_NewsId] FOREIGN KEY([NewsId])
REFERENCES [dbo].[News] ([NewsId])
GO
ALTER TABLE [dbo].[NewsLocation] CHECK CONSTRAINT [FK_NewsLocation_NewsId]
GO
ALTER TABLE [dbo].[NewsPaperPage]  WITH CHECK ADD  CONSTRAINT [FK_NewsPaperPage_DailyNewsPaper] FOREIGN KEY([DailyNewsPaperId])
REFERENCES [dbo].[DailyNewsPaper] ([DailyNewsPaperId])
GO
ALTER TABLE [dbo].[NewsPaperPage] CHECK CONSTRAINT [FK_NewsPaperPage_DailyNewsPaper]
GO
ALTER TABLE [dbo].[NewsPaperPagesPart]  WITH CHECK ADD  CONSTRAINT [FK_NewsPaperPage] FOREIGN KEY([NewsPaperPageId])
REFERENCES [dbo].[NewsPaperPage] ([NewsPaperPageId])
GO
ALTER TABLE [dbo].[NewsPaperPagesPart] CHECK CONSTRAINT [FK_NewsPaperPage]
GO
ALTER TABLE [dbo].[NewsPaperPagesPartsDetail]  WITH CHECK ADD  CONSTRAINT [FK_NewsPaperPagesParts] FOREIGN KEY([NewsPaperPagesPartId])
REFERENCES [dbo].[NewsPaperPagesPart] ([NewsPaperPagesPartId])
GO
ALTER TABLE [dbo].[NewsPaperPagesPartsDetail] CHECK CONSTRAINT [FK_NewsPaperPagesParts]
GO
ALTER TABLE [dbo].[NewsResource]  WITH NOCHECK ADD  CONSTRAINT [FK_NewsResource_NewsId] FOREIGN KEY([NewsId])
REFERENCES [dbo].[News] ([NewsId])
GO
ALTER TABLE [dbo].[NewsResource] CHECK CONSTRAINT [FK_NewsResource_NewsId]
GO
ALTER TABLE [dbo].[NewsResource]  WITH NOCHECK ADD  CONSTRAINT [FK_NewsResource_ResourceId] FOREIGN KEY([ResourceId])
REFERENCES [dbo].[Resource] ([ResourceId])
GO
ALTER TABLE [dbo].[NewsResource] CHECK CONSTRAINT [FK_NewsResource_ResourceId]
GO
ALTER TABLE [dbo].[NewsResourceEdit]  WITH CHECK ADD  CONSTRAINT [FK_NewsResourceEdit_NewsId] FOREIGN KEY([NewsId])
REFERENCES [dbo].[News] ([NewsId])
GO
ALTER TABLE [dbo].[NewsResourceEdit] CHECK CONSTRAINT [FK_NewsResourceEdit_NewsId]
GO
ALTER TABLE [dbo].[NewsTag]  WITH NOCHECK ADD  CONSTRAINT [FK_NewsTag_News] FOREIGN KEY([NewsId])
REFERENCES [dbo].[News] ([NewsId])
GO
ALTER TABLE [dbo].[NewsTag] CHECK CONSTRAINT [FK_NewsTag_News]
GO
ALTER TABLE [dbo].[NewsTag]  WITH NOCHECK ADD  CONSTRAINT [FK_NewsTag_Tag] FOREIGN KEY([TagId])
REFERENCES [dbo].[Tag] ([TagId])
GO
ALTER TABLE [dbo].[NewsTag] CHECK CONSTRAINT [FK_NewsTag_Tag]
GO
ALTER TABLE [dbo].[Notification]  WITH CHECK ADD  CONSTRAINT [FK_Notification_NotificationType] FOREIGN KEY([NotificationTypeId])
REFERENCES [dbo].[NotificationType] ([NotificationTypeId])
GO
ALTER TABLE [dbo].[Notification] CHECK CONSTRAINT [FK_Notification_NotificationType]
GO
ALTER TABLE [dbo].[OnAirTickerLine]  WITH CHECK ADD FOREIGN KEY([TickerId])
REFERENCES [dbo].[OnAirTicker] ([TickerId])
GO
ALTER TABLE [dbo].[Program]  WITH CHECK ADD  CONSTRAINT [FK_Program_Channel] FOREIGN KEY([ChannelId])
REFERENCES [dbo].[Channel] ([ChannelId])
GO
ALTER TABLE [dbo].[Program] CHECK CONSTRAINT [FK_Program_Channel]
GO
ALTER TABLE [dbo].[ProgramConfiguration]  WITH CHECK ADD  CONSTRAINT [FK_ProgramConfiguration_Program] FOREIGN KEY([ProgramId])
REFERENCES [dbo].[Program] ([ProgramId])
GO
ALTER TABLE [dbo].[ProgramConfiguration] CHECK CONSTRAINT [FK_ProgramConfiguration_Program]
GO
ALTER TABLE [dbo].[ProgramConfiguration]  WITH CHECK ADD  CONSTRAINT [FK_ProgramConfiguration_SegmentType] FOREIGN KEY([SegmentTypeId])
REFERENCES [dbo].[SegmentType] ([SegmentTypeId])
GO
ALTER TABLE [dbo].[ProgramConfiguration] CHECK CONSTRAINT [FK_ProgramConfiguration_SegmentType]
GO
ALTER TABLE [dbo].[ProgramSchedule]  WITH CHECK ADD  CONSTRAINT [FK_ProgramSchedule_Program] FOREIGN KEY([ProgramId])
REFERENCES [dbo].[Program] ([ProgramId])
GO
ALTER TABLE [dbo].[ProgramSchedule] CHECK CONSTRAINT [FK_ProgramSchedule_Program]
GO
ALTER TABLE [dbo].[ProgramSchedule]  WITH CHECK ADD  CONSTRAINT [FK_ProgramSchedule_ResourceType] FOREIGN KEY([RecurringTypeId])
REFERENCES [dbo].[RecurringType] ([RecurringTypeId])
GO
ALTER TABLE [dbo].[ProgramSchedule] CHECK CONSTRAINT [FK_ProgramSchedule_ResourceType]
GO
ALTER TABLE [dbo].[ProgramScreenElement]  WITH CHECK ADD  CONSTRAINT [FK_ProgramScreenElement_Program] FOREIGN KEY([ProgramId])
REFERENCES [dbo].[Program] ([ProgramId])
GO
ALTER TABLE [dbo].[ProgramScreenElement] CHECK CONSTRAINT [FK_ProgramScreenElement_Program]
GO
ALTER TABLE [dbo].[ProgramSegment]  WITH CHECK ADD  CONSTRAINT [FK_ProgramSegment_Program] FOREIGN KEY([ProgramId])
REFERENCES [dbo].[Program] ([ProgramId])
GO
ALTER TABLE [dbo].[ProgramSegment] CHECK CONSTRAINT [FK_ProgramSegment_Program]
GO
ALTER TABLE [dbo].[ProgramSegment]  WITH CHECK ADD  CONSTRAINT [FK_ProgramSegment_SegmentType] FOREIGN KEY([SegmentTypeId])
REFERENCES [dbo].[SegmentType] ([SegmentTypeId])
GO
ALTER TABLE [dbo].[ProgramSegment] CHECK CONSTRAINT [FK_ProgramSegment_SegmentType]
GO
ALTER TABLE [dbo].[ProgramTypeMapping]  WITH CHECK ADD  CONSTRAINT [FK_ProgramTypeMapping_Program] FOREIGN KEY([ProgramId])
REFERENCES [dbo].[Program] ([ProgramId])
GO
ALTER TABLE [dbo].[ProgramTypeMapping] CHECK CONSTRAINT [FK_ProgramTypeMapping_Program]
GO
ALTER TABLE [dbo].[ProgramTypeMapping]  WITH CHECK ADD  CONSTRAINT [FK_ProgramTypeMapping_ProgramType] FOREIGN KEY([ProgramTypeId])
REFERENCES [dbo].[ProgramType] ([ProgramTypeId])
GO
ALTER TABLE [dbo].[ProgramTypeMapping] CHECK CONSTRAINT [FK_ProgramTypeMapping_ProgramType]
GO
ALTER TABLE [dbo].[RadioStream]  WITH CHECK ADD  CONSTRAINT [FK_ProgramId_RadioStream] FOREIGN KEY([RadioStreamId])
REFERENCES [dbo].[Program] ([ProgramId])
GO
ALTER TABLE [dbo].[RadioStream] CHECK CONSTRAINT [FK_ProgramId_RadioStream]
GO
ALTER TABLE [dbo].[RadioStream]  WITH CHECK ADD  CONSTRAINT [FK_RadioStream_RadioStation] FOREIGN KEY([RadioStationId])
REFERENCES [dbo].[RadioStation] ([RadioStationId])
GO
ALTER TABLE [dbo].[RadioStream] CHECK CONSTRAINT [FK_RadioStream_RadioStation]
GO
ALTER TABLE [dbo].[Resource]  WITH NOCHECK ADD  CONSTRAINT [FK_Resource_ResourceType] FOREIGN KEY([ResourceTypeId])
REFERENCES [dbo].[ResourceType] ([ResourceTypeId])
GO
ALTER TABLE [dbo].[Resource] CHECK CONSTRAINT [FK_Resource_ResourceType]
GO
ALTER TABLE [dbo].[ResourceEditclipinfo]  WITH CHECK ADD  CONSTRAINT [FK_ResourceEditclipinfo_ResourceEditId] FOREIGN KEY([NewsResourceEditId])
REFERENCES [dbo].[NewsResourceEdit] ([NewsResourceEditId])
GO
ALTER TABLE [dbo].[ResourceEditclipinfo] CHECK CONSTRAINT [FK_ResourceEditclipinfo_ResourceEditId]
GO
ALTER TABLE [dbo].[RunOrderStories]  WITH CHECK ADD  CONSTRAINT [FK_RunOrderStories_RunOrderStories] FOREIGN KEY([RunOrderLogId])
REFERENCES [dbo].[RunOrderLog] ([RunOrderLogId])
GO
ALTER TABLE [dbo].[RunOrderStories] CHECK CONSTRAINT [FK_RunOrderStories_RunOrderStories]
GO
ALTER TABLE [dbo].[RunOrderStoryDetail]  WITH CHECK ADD  CONSTRAINT [FK_RunOrderStoryDetail_RunOrderStoryDetail] FOREIGN KEY([RoStoryId])
REFERENCES [dbo].[RunOrderStories] ([RunOrderStoryId])
GO
ALTER TABLE [dbo].[RunOrderStoryDetail] CHECK CONSTRAINT [FK_RunOrderStoryDetail_RunOrderStoryDetail]
GO
ALTER TABLE [dbo].[ScreenTemplate]  WITH CHECK ADD  CONSTRAINT [FK_ScreenTemplate_Program] FOREIGN KEY([ProgramId])
REFERENCES [dbo].[Program] ([ProgramId])
GO
ALTER TABLE [dbo].[ScreenTemplate] CHECK CONSTRAINT [FK_ScreenTemplate_Program]
GO
ALTER TABLE [dbo].[ScreenTemplatekey]  WITH CHECK ADD  CONSTRAINT [FK_ScreenTemplatekey_ScreenTemplate] FOREIGN KEY([ScreenTemplateId])
REFERENCES [dbo].[ScreenTemplate] ([ScreenTemplateId])
GO
ALTER TABLE [dbo].[ScreenTemplatekey] CHECK CONSTRAINT [FK_ScreenTemplatekey_ScreenTemplate]
GO
ALTER TABLE [dbo].[Segment]  WITH CHECK ADD  CONSTRAINT [FK_Segment_Episode] FOREIGN KEY([EpisodeId])
REFERENCES [dbo].[Episode] ([EpisodeId])
GO
ALTER TABLE [dbo].[Segment] CHECK CONSTRAINT [FK_Segment_Episode]
GO
ALTER TABLE [dbo].[Segment]  WITH CHECK ADD  CONSTRAINT [FK_Segment_SegmentType] FOREIGN KEY([SegmentTypeId])
REFERENCES [dbo].[SegmentType] ([SegmentTypeId])
GO
ALTER TABLE [dbo].[Segment] CHECK CONSTRAINT [FK_Segment_SegmentType]
GO
ALTER TABLE [dbo].[Slot]  WITH CHECK ADD  CONSTRAINT [FK_Slot_Category] FOREIGN KEY([CategoryId])
REFERENCES [dbo].[Category] ([CategoryId])
GO
ALTER TABLE [dbo].[Slot] CHECK CONSTRAINT [FK_Slot_Category]
GO
ALTER TABLE [dbo].[Slot]  WITH CHECK ADD  CONSTRAINT [FK_Slot_SlotType] FOREIGN KEY([SlotTypeId])
REFERENCES [dbo].[SlotType] ([SlotTypeId])
GO
ALTER TABLE [dbo].[Slot] CHECK CONSTRAINT [FK_Slot_SlotType]
GO
ALTER TABLE [dbo].[Slot]  WITH CHECK ADD  CONSTRAINT [FK_Story_Segment] FOREIGN KEY([SegmentId])
REFERENCES [dbo].[Segment] ([SegmentId])
GO
ALTER TABLE [dbo].[Slot] CHECK CONSTRAINT [FK_Story_Segment]
GO
ALTER TABLE [dbo].[SlotScreenTemplate]  WITH CHECK ADD  CONSTRAINT [FK_SlotScreenTemplate_ScreenTemplate] FOREIGN KEY([ScreenTemplateId])
REFERENCES [dbo].[ScreenTemplate] ([ScreenTemplateId])
GO
ALTER TABLE [dbo].[SlotScreenTemplate] CHECK CONSTRAINT [FK_SlotScreenTemplate_ScreenTemplate]
GO
ALTER TABLE [dbo].[SlotScreenTemplate]  WITH CHECK ADD  CONSTRAINT [FK_StoryScreenTemplate_Story] FOREIGN KEY([SlotId])
REFERENCES [dbo].[Slot] ([SlotId])
GO
ALTER TABLE [dbo].[SlotScreenTemplate] CHECK CONSTRAINT [FK_StoryScreenTemplate_Story]
GO
ALTER TABLE [dbo].[SlotScreenTemplateCamera]  WITH CHECK ADD  CONSTRAINT [FK_SlotScreenTemplateCamera_CameraType] FOREIGN KEY([CameraTypeId])
REFERENCES [dbo].[CameraType] ([CameraTypeId])
GO
ALTER TABLE [dbo].[SlotScreenTemplateCamera] CHECK CONSTRAINT [FK_SlotScreenTemplateCamera_CameraType]
GO
ALTER TABLE [dbo].[SlotScreenTemplateCamera]  WITH CHECK ADD  CONSTRAINT [FK_SlotScreenTemplateCamera_SlotScreenTemplate] FOREIGN KEY([SlotScreenTemplateId])
REFERENCES [dbo].[SlotScreenTemplate] ([SlotScreenTemplateId])
GO
ALTER TABLE [dbo].[SlotScreenTemplateCamera] CHECK CONSTRAINT [FK_SlotScreenTemplateCamera_SlotScreenTemplate]
GO
ALTER TABLE [dbo].[SlotScreenTemplatekey]  WITH CHECK ADD  CONSTRAINT [FK_SlotScreenTemplatekey_ScreenTemplatekey] FOREIGN KEY([ScreenTemplatekeyId])
REFERENCES [dbo].[ScreenTemplatekey] ([ScreenTemplatekeyId])
GO
ALTER TABLE [dbo].[SlotScreenTemplatekey] CHECK CONSTRAINT [FK_SlotScreenTemplatekey_ScreenTemplatekey]
GO
ALTER TABLE [dbo].[SlotScreenTemplatekey]  WITH CHECK ADD  CONSTRAINT [FK_SlotScreenTemplatekey_SlotScreenTemplate] FOREIGN KEY([SlotScreenTemplateId])
REFERENCES [dbo].[SlotScreenTemplate] ([SlotScreenTemplateId])
GO
ALTER TABLE [dbo].[SlotScreenTemplatekey] CHECK CONSTRAINT [FK_SlotScreenTemplatekey_SlotScreenTemplate]
GO
ALTER TABLE [dbo].[SlotScreenTemplateMic]  WITH CHECK ADD  CONSTRAINT [FK_SlotScreenTemplateMic_SlotScreenTemplate] FOREIGN KEY([SlotScreenTemplateId])
REFERENCES [dbo].[SlotScreenTemplate] ([SlotScreenTemplateId])
GO
ALTER TABLE [dbo].[SlotScreenTemplateMic] CHECK CONSTRAINT [FK_SlotScreenTemplateMic_SlotScreenTemplate]
GO
ALTER TABLE [dbo].[SlotScreenTemplateResource]  WITH CHECK ADD  CONSTRAINT [FK_SlotScreenTemplateResource_SlotScreenTemplate] FOREIGN KEY([SlotScreenTemplateId])
REFERENCES [dbo].[SlotScreenTemplate] ([SlotScreenTemplateId])
GO
ALTER TABLE [dbo].[SlotScreenTemplateResource] CHECK CONSTRAINT [FK_SlotScreenTemplateResource_SlotScreenTemplate]
GO
ALTER TABLE [dbo].[SlotTemplateScreenElement]  WITH CHECK ADD  CONSTRAINT [FK_Celebrity_SlotTemplateScreenElement] FOREIGN KEY([CelebrityId])
REFERENCES [dbo].[Celebrity] ([CelebrityId])
GO
ALTER TABLE [dbo].[SlotTemplateScreenElement] CHECK CONSTRAINT [FK_Celebrity_SlotTemplateScreenElement]
GO
ALTER TABLE [dbo].[SlotTemplateScreenElement]  WITH CHECK ADD  CONSTRAINT [FK_StoryTemplateScreenElement_StoryScreenTemplate] FOREIGN KEY([SlotScreenTemplateId])
REFERENCES [dbo].[SlotScreenTemplate] ([SlotScreenTemplateId])
GO
ALTER TABLE [dbo].[SlotTemplateScreenElement] CHECK CONSTRAINT [FK_StoryTemplateScreenElement_StoryScreenTemplate]
GO
ALTER TABLE [dbo].[SlotTemplateScreenElementResource]  WITH CHECK ADD FOREIGN KEY([SlotScreenTemplateId])
REFERENCES [dbo].[SlotScreenTemplate] ([SlotScreenTemplateId])
GO
ALTER TABLE [dbo].[SlotTemplateScreenElementResource]  WITH CHECK ADD FOREIGN KEY([SlotTemplateScreenElementId])
REFERENCES [dbo].[SlotTemplateScreenElement] ([SlotTemplateScreenElementId])
GO
ALTER TABLE [dbo].[SocialMediaAccount]  WITH CHECK ADD FOREIGN KEY([CelebrityId])
REFERENCES [dbo].[Celebrity] ([CelebrityId])
GO
ALTER TABLE [dbo].[TagResource]  WITH CHECK ADD  CONSTRAINT [FK_TagResource_ResourceId] FOREIGN KEY([ResourceId])
REFERENCES [dbo].[Resource] ([ResourceId])
GO
ALTER TABLE [dbo].[TagResource] CHECK CONSTRAINT [FK_TagResource_ResourceId]
GO
ALTER TABLE [dbo].[TagResource]  WITH CHECK ADD  CONSTRAINT [FK_TagResource_TagId] FOREIGN KEY([TagId])
REFERENCES [dbo].[Tag] ([TagId])
GO
ALTER TABLE [dbo].[TagResource] CHECK CONSTRAINT [FK_TagResource_TagId]
GO
ALTER TABLE [dbo].[Team]  WITH CHECK ADD  CONSTRAINT [FK_Team_Program] FOREIGN KEY([ProgramId])
REFERENCES [dbo].[Program] ([ProgramId])
GO
ALTER TABLE [dbo].[Team] CHECK CONSTRAINT [FK_Team_Program]
GO
ALTER TABLE [dbo].[Team]  WITH CHECK ADD  CONSTRAINT [FK_Team_TeamRole] FOREIGN KEY([TeamRoleId])
REFERENCES [dbo].[TeamRole] ([TeamRoleId])
GO
ALTER TABLE [dbo].[Team] CHECK CONSTRAINT [FK_Team_TeamRole]
GO
ALTER TABLE [dbo].[TemplateScreenElement]  WITH CHECK ADD  CONSTRAINT [FK_TemplateScreenElement_ScreenElement] FOREIGN KEY([ScreenElementId])
REFERENCES [dbo].[ScreenElement] ([ScreenElementId])
GO
ALTER TABLE [dbo].[TemplateScreenElement] CHECK CONSTRAINT [FK_TemplateScreenElement_ScreenElement]
GO
ALTER TABLE [dbo].[TemplateScreenElement]  WITH CHECK ADD  CONSTRAINT [FK_TemplateScreenElement_ScreenTemplate] FOREIGN KEY([ScreenTemplateId])
REFERENCES [dbo].[ScreenTemplate] ([ScreenTemplateId])
GO
ALTER TABLE [dbo].[TemplateScreenElement] CHECK CONSTRAINT [FK_TemplateScreenElement_ScreenTemplate]
GO
ALTER TABLE [dbo].[TickerLine]  WITH CHECK ADD  CONSTRAINT [FK_TickerLine_Ticker] FOREIGN KEY([TickerId])
REFERENCES [dbo].[Ticker] ([TickerId])
GO
ALTER TABLE [dbo].[TickerLine] CHECK CONSTRAINT [FK_TickerLine_Ticker]
GO
ALTER TABLE [dbo].[TwitterAccount]  WITH CHECK ADD  CONSTRAINT [FK_TwitterAccount_Celebrity] FOREIGN KEY([CelebrityId])
REFERENCES [dbo].[Celebrity] ([CelebrityId])
GO
ALTER TABLE [dbo].[TwitterAccount] CHECK CONSTRAINT [FK_TwitterAccount_Celebrity]
GO
ALTER TABLE [dbo].[UserRole]  WITH CHECK ADD  CONSTRAINT [FK_UserRole_Role] FOREIGN KEY([RoleId])
REFERENCES [dbo].[Role] ([RoleId])
GO
ALTER TABLE [dbo].[UserRole] CHECK CONSTRAINT [FK_UserRole_Role]
GO
ALTER TABLE [dbo].[UserRole]  WITH CHECK ADD  CONSTRAINT [FK_UserRole_User] FOREIGN KEY([UserId])
REFERENCES [dbo].[User] ([UserId])
GO
ALTER TABLE [dbo].[UserRole] CHECK CONSTRAINT [FK_UserRole_User]
GO
ALTER TABLE [dbo].[WorkRoleFolder]  WITH CHECK ADD  CONSTRAINT [FK_WorkRoleFolder_Folder] FOREIGN KEY([FolderId])
REFERENCES [dbo].[Folder] ([FolderId])
GO
ALTER TABLE [dbo].[WorkRoleFolder] CHECK CONSTRAINT [FK_WorkRoleFolder_Folder]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "MCRCategoryTickerRundown"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 135
               Right = 294
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'MCRBusinessTicker'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'MCRBusinessTicker'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "MCRCategoryTickerRundown"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 135
               Right = 294
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'MCRInternationalTicker'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'MCRInternationalTicker'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "MCRCategoryTickerRundown"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 135
               Right = 294
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'MCRLocalTicker'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'MCRLocalTicker'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "MCRCategoryTickerRundown"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 135
               Right = 294
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'MCRSportsTicker'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'MCRSportsTicker'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "MCRCategoryTickerRundown"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 135
               Right = 294
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'MCRWeatherTicker'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'MCRWeatherTicker'
GO
USE [master]
GO
ALTER DATABASE [NMS] SET  READ_WRITE 
GO
