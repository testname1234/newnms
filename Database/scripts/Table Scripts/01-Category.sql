USE NMS

GO  
-- SET IDENTITY_INSERT to ON.  
SET IDENTITY_INSERT dbo.Category ON;  
GO    
INSERT INTO [Category] ([CategoryId],[Category],[ParentId],[CreationDate],[LastUpdateDate],[IsActive],[IsApproved],[CategoryInUrdu])VALUES(1,'Politics',NULL,'May 19 2014  1:43:23:773PM','May 19 2014  1:43:23:773PM',1,1,NULL)
INSERT INTO [Category] ([CategoryId],[Category],[ParentId],[CreationDate],[LastUpdateDate],[IsActive],[IsApproved],[CategoryInUrdu])VALUES(2,'Sports',NULL,'May 19 2014  1:43:23:773PM','May 19 2014  1:43:23:773PM',1,1,NULL)
INSERT INTO [Category] ([CategoryId],[Category],[ParentId],[CreationDate],[LastUpdateDate],[IsActive],[IsApproved],[CategoryInUrdu])VALUES(3,'Cricket',2,'May 19 2014  1:43:23:773PM','May 19 2014  1:43:23:773PM',1,1,NULL)
INSERT INTO [Category] ([CategoryId],[Category],[ParentId],[CreationDate],[LastUpdateDate],[IsActive],[IsApproved],[CategoryInUrdu])VALUES(4,'Hockey',2,'May 19 2014  1:43:23:773PM','May 19 2014  1:43:23:773PM',1,1,NULL)
INSERT INTO [Category] ([CategoryId],[Category],[ParentId],[CreationDate],[LastUpdateDate],[IsActive],[IsApproved],[CategoryInUrdu])VALUES(5,'Football',2,'May 19 2014  1:43:23:773PM','May 19 2014  1:43:23:773PM',1,1,NULL)
INSERT INTO [Category] ([CategoryId],[Category],[ParentId],[CreationDate],[LastUpdateDate],[IsActive],[IsApproved],[CategoryInUrdu])VALUES(6,'Tennis',2,'May 19 2014  1:43:23:773PM','May 19 2014  1:43:23:773PM',1,1,NULL)
INSERT INTO [Category] ([CategoryId],[Category],[ParentId],[CreationDate],[LastUpdateDate],[IsActive],[IsApproved],[CategoryInUrdu])VALUES(7,'Badminton',2,'May 19 2014  1:43:23:773PM','May 19 2014  1:43:23:773PM',1,1,NULL)
INSERT INTO [Category] ([CategoryId],[Category],[ParentId],[CreationDate],[LastUpdateDate],[IsActive],[IsApproved],[CategoryInUrdu])VALUES(8,'Snooker',2,'May 19 2014  1:43:23:773PM','May 19 2014  1:43:23:773PM',1,1,NULL)
INSERT INTO [Category] ([CategoryId],[Category],[ParentId],[CreationDate],[LastUpdateDate],[IsActive],[IsApproved],[CategoryInUrdu])VALUES(10,'Newspaper',398,'May 21 2014  4:58:35:130PM','May 21 2014  4:58:35:130PM',1,1,NULL)
INSERT INTO [Category] ([CategoryId],[Category],[ParentId],[CreationDate],[LastUpdateDate],[IsActive],[IsApproved],[CategoryInUrdu])VALUES(12,'Government',NULL,'May 22 2014 12:17:45:960PM','May 22 2014 12:17:45:960PM',1,0,NULL)
INSERT INTO [Category] ([CategoryId],[Category],[ParentId],[CreationDate],[LastUpdateDate],[IsActive],[IsApproved],[CategoryInUrdu])VALUES(14,'International',NULL,'May 22 2014  1:12:37:247PM','May 22 2014  1:12:37:247PM',1,1,NULL)
INSERT INTO [Category] ([CategoryId],[Category],[ParentId],[CreationDate],[LastUpdateDate],[IsActive],[IsApproved],[CategoryInUrdu])VALUES(15,'Business',NULL,'May 22 2014  1:15:41:130PM','May 22 2014  1:15:41:130PM',1,1,NULL)
INSERT INTO [Category] ([CategoryId],[Category],[ParentId],[CreationDate],[LastUpdateDate],[IsActive],[IsApproved],[CategoryInUrdu])VALUES(19,'Entertainment',NULL,'May 29 2014 11:00:30:460PM','May 29 2014 11:00:30:460PM',1,1,NULL)
INSERT INTO [Category] ([CategoryId],[Category],[ParentId],[CreationDate],[LastUpdateDate],[IsActive],[IsApproved],[CategoryInUrdu])VALUES(20,'Health & Medicine',NULL,'May 29 2014 11:00:41:320PM','May 29 2014 11:00:41:320PM',1,1,NULL)
INSERT INTO [Category] ([CategoryId],[Category],[ParentId],[CreationDate],[LastUpdateDate],[IsActive],[IsApproved],[CategoryInUrdu])VALUES(22,'Technology',NULL,'May 29 2014 11:01:30:603PM','May 29 2014 11:01:30:603PM',1,1,NULL)
INSERT INTO [Category] ([CategoryId],[Category],[ParentId],[CreationDate],[LastUpdateDate],[IsActive],[IsApproved],[CategoryInUrdu])VALUES(23,'Weird',398,'May 29 2014 11:01:42:827PM','May 29 2014 11:01:42:827PM',1,1,NULL)
INSERT INTO [Category] ([CategoryId],[Category],[ParentId],[CreationDate],[LastUpdateDate],[IsActive],[IsApproved],[CategoryInUrdu])VALUES(27,'Weather
',NULL,'Jun  2 2014  4:34:30:233PM','Jun  2 2014  4:34:30:233PM',1,1,NULL)
INSERT INTO [Category] ([CategoryId],[Category],[ParentId],[CreationDate],[LastUpdateDate],[IsActive],[IsApproved],[CategoryInUrdu])VALUES(398,'Others',NULL,'Jun  2 2014  4:34:30:233PM','Jun  2 2014  4:34:30:233PM',1,1,NULL)
INSERT INTO [Category] ([CategoryId],[Category],[ParentId],[CreationDate],[LastUpdateDate],[IsActive],[IsApproved],[CategoryInUrdu])VALUES(402,'Education',NULL,'Jun  2 2014  4:34:30:233PM','Jun  2 2014  4:34:30:233PM',1,1,NULL)
INSERT INTO [Category] ([CategoryId],[Category],[ParentId],[CreationDate],[LastUpdateDate],[IsActive],[IsApproved],[CategoryInUrdu])VALUES(403,'Science',NULL,'Jun  2 2014  4:34:30:233PM','Jun  2 2014  4:34:30:233PM',1,1,NULL)
INSERT INTO [Category] ([CategoryId],[Category],[ParentId],[CreationDate],[LastUpdateDate],[IsActive],[IsApproved],[CategoryInUrdu])VALUES(404,'History',NULL,'Jun  2 2014  4:34:30:233PM','Jun  2 2014  4:34:30:233PM',1,0,NULL)
INSERT INTO [Category] ([CategoryId],[Category],[ParentId],[CreationDate],[LastUpdateDate],[IsActive],[IsApproved],[CategoryInUrdu])VALUES(405,'Religion',NULL,'Jun  2 2014  4:34:30:233PM','Jun  2 2014  4:34:30:233PM',1,1,NULL)
INSERT INTO [Category] ([CategoryId],[Category],[ParentId],[CreationDate],[LastUpdateDate],[IsActive],[IsApproved],[CategoryInUrdu])VALUES(406,'Travel',NULL,'Jun  2 2014  4:34:30:233PM','Jun  2 2014  4:34:30:233PM',1,1,NULL)
INSERT INTO [Category] ([CategoryId],[Category],[ParentId],[CreationDate],[LastUpdateDate],[IsActive],[IsApproved],[CategoryInUrdu])VALUES(407,'Food',NULL,'Jun  2 2014  4:34:30:233PM','Jun  2 2014  4:34:30:233PM',1,1,NULL)
INSERT INTO [Category] ([CategoryId],[Category],[ParentId],[CreationDate],[LastUpdateDate],[IsActive],[IsApproved],[CategoryInUrdu])VALUES(408,'Crime',NULL,'Jun  2 2014  4:34:30:233PM','Jun  2 2014  4:34:30:233PM',1,1,NULL)
INSERT INTO [Category] ([CategoryId],[Category],[ParentId],[CreationDate],[LastUpdateDate],[IsActive],[IsApproved],[CategoryInUrdu])VALUES(411,'Defence / Forces',NULL,'Jun  2 2014  4:34:30:233PM','Jun  2 2014  4:34:30:233PM',1,0,NULL)
INSERT INTO [Category] ([CategoryId],[Category],[ParentId],[CreationDate],[LastUpdateDate],[IsActive],[IsApproved],[CategoryInUrdu])VALUES(413,'Court',NULL,'Jun  2 2014  4:34:30:233PM','Jun  2 2014  4:34:30:233PM',1,1,NULL)
INSERT INTO [Category] ([CategoryId],[Category],[ParentId],[CreationDate],[LastUpdateDate],[IsActive],[IsApproved],[CategoryInUrdu])VALUES(425,'Economy',NULL,'Jun 12 2014 10:48:43:437AM','Jun 12 2014 10:48:43:437AM',1,1,NULL)
INSERT INTO [Category] ([CategoryId],[Category],[ParentId],[CreationDate],[LastUpdateDate],[IsActive],[IsApproved],[CategoryInUrdu])VALUES(426,'Artificial Intelligence',22,'Jun 12 2014 12:53:42:390PM','Jun 12 2014 12:53:42:390PM',1,1,NULL)
INSERT INTO [Category] ([CategoryId],[Category],[ParentId],[CreationDate],[LastUpdateDate],[IsActive],[IsApproved],[CategoryInUrdu])VALUES(427,'National',NULL,'Jun 12 2014  6:30:26:287PM','Jun 12 2014  6:30:26:287PM',1,1,NULL)
INSERT INTO [Category] ([CategoryId],[Category],[ParentId],[CreationDate],[LastUpdateDate],[IsActive],[IsApproved],[CategoryInUrdu])VALUES(428,'Miscellaneous',398,'Jun 17 2014  1:27:12:083PM','Jun 17 2014  1:27:12:083PM',1,1,NULL)
INSERT INTO [Category] ([CategoryId],[Category],[ParentId],[CreationDate],[LastUpdateDate],[IsActive],[IsApproved],[CategoryInUrdu])VALUES(429,'Top News',428,'Jun 17 2014  4:38:47:573PM','Jun 17 2014  4:38:47:573PM',1,1,NULL)
INSERT INTO [Category] ([CategoryId],[Category],[ParentId],[CreationDate],[LastUpdateDate],[IsActive],[IsApproved],[CategoryInUrdu])VALUES(430,'KP & FATA',427,'Jun 17 2014  4:39:35:357PM','Jun 17 2014  4:39:35:357PM',1,1,NULL)
INSERT INTO [Category] ([CategoryId],[Category],[ParentId],[CreationDate],[LastUpdateDate],[IsActive],[IsApproved],[CategoryInUrdu])VALUES(431,'Cycling',NULL,'Jun 17 2014  7:24:16:863PM','Jun 17 2014  7:24:16:863PM',1,1,NULL)
INSERT INTO [Category] ([CategoryId],[Category],[ParentId],[CreationDate],[LastUpdateDate],[IsActive],[IsApproved],[CategoryInUrdu])VALUES(432,'Athletics',NULL,'Jun 17 2014  7:45:16:320PM','Jun 17 2014  7:45:16:320PM',1,1,NULL)
INSERT INTO [Category] ([CategoryId],[Category],[ParentId],[CreationDate],[LastUpdateDate],[IsActive],[IsApproved],[CategoryInUrdu])VALUES(433,'Boxing',2,'Jun 18 2014  5:16:49:673PM','Jun 18 2014  5:16:49:673PM',1,1,NULL)
INSERT INTO [Category] ([CategoryId],[Category],[ParentId],[CreationDate],[LastUpdateDate],[IsActive],[IsApproved],[CategoryInUrdu])VALUES(434,'Science And Technology News',22,'Sep  8 2014 12:20:40:860PM','Sep  8 2014 12:20:40:860PM',1,1,NULL)
INSERT INTO [Category] ([CategoryId],[Category],[ParentId],[CreationDate],[LastUpdateDate],[IsActive],[IsApproved],[CategoryInUrdu])VALUES(435,'Life And Style',NULL,'Sep 15 2014 12:56:05:983PM','Sep 15 2014 12:56:05:983PM',1,1,NULL)
INSERT INTO [Category] ([CategoryId],[Category],[ParentId],[CreationDate],[LastUpdateDate],[IsActive],[IsApproved],[CategoryInUrdu])VALUES(436,'Amazing',NULL,'Sep 15 2014 12:58:04:593PM','Sep 15 2014 12:58:04:593PM',1,1,NULL)
INSERT INTO [Category] ([CategoryId],[Category],[ParentId],[CreationDate],[LastUpdateDate],[IsActive],[IsApproved],[CategoryInUrdu])VALUES(437,'Magazine',NULL,'Sep 15 2014  1:26:50:593PM','Sep 15 2014  1:26:50:593PM',1,1,NULL)
INSERT INTO [Category] ([CategoryId],[Category],[ParentId],[CreationDate],[LastUpdateDate],[IsActive],[IsApproved],[CategoryInUrdu])VALUES(438,'Tradition',NULL,'Sep 15 2014  2:06:51:780PM','Sep 15 2014  2:06:51:780PM',1,1,NULL)
INSERT INTO [Category] ([CategoryId],[Category],[ParentId],[CreationDate],[LastUpdateDate],[IsActive],[IsApproved],[CategoryInUrdu])VALUES(439,'Showbiz',NULL,'Sep 19 2014 10:10:45:313AM','Sep 19 2014 10:10:45:313AM',1,1,NULL)

GO  
-- SET IDENTITY_INSERT to ON.  
SET IDENTITY_INSERT dbo.Category OFF;  
GO    