USE [NMS]
GO
SET IDENTITY_INSERT [dbo].[Location] ON 

GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (1, N'Karachi', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, NULL)
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12590, N'Andorra', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Andorra')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12591, N'United Arab Emirates', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'United Arab Emirates')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12592, N'Afghanistan', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Afghanistan')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12593, N'Antigua And Barbuda', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Antigua And Barbuda')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12594, N'Anguilla', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Anguilla')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12595, N'Albania', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Albania')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12596, N'Armenia', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Armenia')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12597, N'Netherlands Antilles', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Netherlands Antilles')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12598, N'Angola', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Angola')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12599, N'Antarctica', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Antarctica')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12600, N'Argentina', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Argentina')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12601, N'American Samoa', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'American Samoa')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12602, N'Austria', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Austria')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12603, N'Australia', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Australia')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12604, N'Aruba', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Aruba')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12605, N'Aland Islands', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Aland Islands')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12606, N'Azerbaijan', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Azerbaijan')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12607, N'Bosnia And Herzegovina', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Bosnia And Herzegovina')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12608, N'Barbados', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Barbados')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12609, N'Bangladesh', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Bangladesh')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12610, N'Belgium', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Belgium')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12611, N'Burkina Faso', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Burkina Faso')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12612, N'Bulgaria', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Bulgaria')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12613, N'Bahrain', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Bahrain')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12614, N'Burundi', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Burundi')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12615, N'Benin', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Benin')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12616, N'Bermuda', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Bermuda')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12617, N'Brunei Darussalam', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Brunei Darussalam')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12618, N'Bolivia', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Bolivia')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12619, N'Brazil', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Brazil')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12620, N'Bahamas', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Bahamas')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12621, N'Bhutan', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Bhutan')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12622, N'Bouvet Island', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Bouvet Island')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12623, N'Botswana', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Botswana')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12624, N'Belarus', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Belarus')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12625, N'Belize', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Belize')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12626, N'Canada', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Canada')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12627, N'Cocos (Keeling) Islands', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Cocos (Keeling) Islands')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12628, N'Congo, The Democratic Republic Of The', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Congo, The Democratic Republic Of The')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12629, N'Central African Republic', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Central African Republic')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12630, N'Congo', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Congo')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12631, N'Switzerland', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Switzerland')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12632, N'Cote Divoire', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Cote Divoire')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12633, N'Cook Islands', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Cook Islands')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12634, N'Chile', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Chile')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12635, N'Cameroon', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Cameroon')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12636, N'China', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'China')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12637, N'Colombia', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Colombia')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12638, N'Costa Rica', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Costa Rica')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12639, N'Serbia And Montenegro', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Serbia And Montenegro')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12640, N'Cuba', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Cuba')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12641, N'Cape Verde', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Cape Verde')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12642, N'Christmas Island', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Christmas Island')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12643, N'Cyprus', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Cyprus')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12644, N'Czech Republic', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Czech Republic')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12645, N'Germany', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Germany')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12646, N'Djibouti', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Djibouti')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12647, N'Denmark', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Denmark')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12648, N'Dominica', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Dominica')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12649, N'Dominican Republic', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Dominican Republic')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12650, N'Algeria', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Algeria')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12651, N'Ecuador', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Ecuador')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12652, N'Estonia', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Estonia')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12653, N'Egypt', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Egypt')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12654, N'Western Sahara', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Western Sahara')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12655, N'Eritrea', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Eritrea')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12656, N'Spain', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Spain')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12657, N'Ethiopia', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Ethiopia')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12658, N'Finland', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Finland')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12659, N'Fiji', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Fiji')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12660, N'Falkland Islands (Malvinas)', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Falkland Islands (Malvinas)')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12661, N'Micronesia, Federated States Of', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Micronesia, Federated States Of')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12662, N'Faroe Islands', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Faroe Islands')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12663, N'France', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'France')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12664, N'Gabon', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Gabon')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12665, N'United Kingdom', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'United Kingdom')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12666, N'Grenada', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Grenada')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12667, N'Georgia', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Georgia')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12668, N'French Guiana', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'French Guiana')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12669, N'Ghana', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Ghana')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12670, N'Gibraltar', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Gibraltar')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12671, N'Greenland', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Greenland')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12672, N'Gambia', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Gambia')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12673, N'Guinea', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Guinea')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12674, N'Guadeloupe', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Guadeloupe')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12675, N'Equatorial Guinea', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Equatorial Guinea')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12676, N'Greece', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Greece')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12677, N'South Georgia And The South Sandwich Islands', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'South Georgia And The South Sandwich Islands')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12678, N'Guatemala', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Guatemala')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12679, N'Guam', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Guam')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12680, N'Guinea-Bissau', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Guinea-Bissau')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12681, N'Guyana', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Guyana')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12682, N'Hong Kong', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Hong Kong')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12683, N'Heard Island And Mcdonald Islands', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Heard Island And Mcdonald Islands')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12684, N'Honduras', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Honduras')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12685, N'Croatia', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Croatia')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12686, N'Haiti', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Haiti')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12687, N'Hungary', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Hungary')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12688, N'Indonesia', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Indonesia')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12689, N'Ireland', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Ireland')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12690, N'Israel', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Israel')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12691, N'India', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'India')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12692, N'British Indian Ocean Territory', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'British Indian Ocean Territory')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12693, N'Iraq', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Iraq')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12694, N'Iran, Islamic Republic Of', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Iran, Islamic Republic Of')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12695, N'Iceland', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Iceland')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12696, N'Italy', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Italy')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12697, N'Jamaica', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Jamaica')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12698, N'Jordan', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Jordan')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12699, N'Japan', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Japan')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12700, N'Kenya', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Kenya')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12701, N'Kyrgyzstan', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Kyrgyzstan')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12702, N'Cambodia', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Cambodia')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12703, N'Kiribati', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Kiribati')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12704, N'Comoros', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Comoros')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12705, N'Saint Kitts And Nevis', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Saint Kitts And Nevis')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12706, N'Korea, Democratic Peoples Republic Of', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Korea, Democratic Peoples Republic Of')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12707, N'Korea, Republic Of', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Korea, Republic Of')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12708, N'Kuwait', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Kuwait')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12709, N'Cayman Islands', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Cayman Islands')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12710, N'Kazakhstan', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Kazakhstan')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12711, N'Lao Peoples Democratic Republic', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Lao Peoples Democratic Republic')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12712, N'Lebanon', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Lebanon')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12713, N'Saint Lucia', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Saint Lucia')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12714, N'Liechtenstein', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Liechtenstein')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12715, N'Sri Lanka', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Sri Lanka')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12716, N'Liberia', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Liberia')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12717, N'Lesotho', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Lesotho')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12718, N'Lithuania', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Lithuania')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12719, N'Luxembourg', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Luxembourg')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12720, N'Latvia', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Latvia')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12721, N'Libyan Arab Jamahiriya', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Libyan Arab Jamahiriya')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12722, N'Morocco', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Morocco')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12723, N'Monaco', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Monaco')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12724, N'Moldova, Republic Of', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Moldova, Republic Of')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12725, N'Madagascar', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Madagascar')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12726, N'Marshall Islands', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Marshall Islands')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12727, N'Macedonia, The Former Yugoslav Republic Of', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Macedonia, The Former Yugoslav Republic Of')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12728, N'Mali', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Mali')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12729, N'Myanmar', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Myanmar')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12730, N'Mongolia', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Mongolia')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12731, N'Macao', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Macao')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12732, N'Northern Mariana Islands', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Northern Mariana Islands')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12733, N'Martinique', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Martinique')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12734, N'Mauritania', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Mauritania')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12735, N'Montserrat', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Montserrat')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12736, N'Malta', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Malta')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12737, N'Mauritius', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Mauritius')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12738, N'Maldives', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Maldives')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12739, N'Malawi', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Malawi')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12740, N'Mexico', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Mexico')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12741, N'Malaysia', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Malaysia')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12742, N'Mozambique', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Mozambique')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12743, N'Namibia', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Namibia')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12744, N'New Caledonia', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'New Caledonia')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12745, N'Niger', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Niger')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12746, N'Norfolk Island', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Norfolk Island')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12747, N'Nigeria', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Nigeria')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12748, N'Nicaragua', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Nicaragua')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12749, N'Netherlands', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Netherlands')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12750, N'Norway', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Norway')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12751, N'Nepal', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Nepal')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12752, N'Nauru', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Nauru')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12753, N'Niue', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Niue')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12754, N'New Zealand', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'New Zealand')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12755, N'Oman', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Oman')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12756, N'Panama', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Panama')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12757, N'Peru', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Peru')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12758, N'French Polynesia', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'French Polynesia')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12759, N'Papua New Guinea', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Papua New Guinea')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12760, N'Philippines', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Philippines')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12761, N'Pakistan', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Pakistan')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12762, N'Poland', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Poland')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12763, N'Saint Pierre And Miquelon', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Saint Pierre And Miquelon')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12764, N'Pitcairn', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Pitcairn')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12765, N'Puerto Rico', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Puerto Rico')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12766, N'Palestinian Territory, Occupied', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Palestinian Territory, Occupied')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12767, N'Portugal', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Portugal')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12768, N'Palau', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Palau')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12769, N'Paraguay', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Paraguay')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12770, N'Qatar', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Qatar')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12771, N'Reunion', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Reunion')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12772, N'Romania', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Romania')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12773, N'Russian Federation', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Russian Federation')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12774, N'Rwanda', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Rwanda')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12775, N'Saudi Arabia', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Saudi Arabia')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12776, N'Solomon Islands', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Solomon Islands')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12777, N'Seychelles', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Seychelles')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12778, N'Sudan', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Sudan')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12779, N'Sweden', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Sweden')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12780, N'Singapore', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Singapore')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12781, N'Saint Helena', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Saint Helena')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12782, N'Slovenia', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Slovenia')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12783, N'Svalbard And Jan Mayen', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Svalbard And Jan Mayen')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12784, N'Slovakia', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Slovakia')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12785, N'Sierra Leone', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Sierra Leone')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12786, N'San Marino', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'San Marino')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12787, N'Senegal', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Senegal')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12788, N'Somalia', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Somalia')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12789, N'Suriname', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Suriname')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12790, N'Sao Tome And Principe', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Sao Tome And Principe')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12791, N'El Salvador', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'El Salvador')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12792, N'Syrian Arab Republic', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Syrian Arab Republic')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12793, N'Swaziland', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Swaziland')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12794, N'Turks And Caicos Islands', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Turks And Caicos Islands')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12795, N'Chad', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Chad')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12796, N'French Southern Territories', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'French Southern Territories')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12797, N'Togo', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Togo')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12798, N'Thailand', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Thailand')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12799, N'Tajikistan', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Tajikistan')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12800, N'Tokelau', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Tokelau')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12801, N'Timor-Leste', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Timor-Leste')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12802, N'Turkmenistan', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Turkmenistan')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12803, N'Tunisia', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Tunisia')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12804, N'Tonga', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Tonga')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12805, N'Turkey', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Turkey')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12806, N'Trinidad And Tobago', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Trinidad And Tobago')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12807, N'Tuvalu', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Tuvalu')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12808, N'Taiwan, Province Of China', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Taiwan, Province Of China')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12809, N'Tanzania, United Republic Of', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Tanzania, United Republic Of')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12810, N'Ukraine', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Ukraine')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12811, N'Uganda', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Uganda')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12812, N'United States Minor Outlying Islands', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'United States Minor Outlying Islands')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12813, N'United States', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'United States')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12814, N'Uruguay', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Uruguay')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12815, N'Uzbekistan', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Uzbekistan')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12816, N'Holy See (Vatican City State)', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Holy See (Vatican City State)')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12817, N'Saint Vincent And The Grenadines', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Saint Vincent And The Grenadines')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12818, N'Venezuela', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Venezuela')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12819, N'Virgin Islands, British', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Virgin Islands, British')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12820, N'Virgin Islands, U.S.', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Virgin Islands, U.S.')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12821, N'Vietnam', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Vietnam')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12822, N'Vanuatu', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Vanuatu')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12823, N'Wallis And Futuna', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Wallis And Futuna')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12824, N'Samoa', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Samoa')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12825, N'Yemen', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Yemen')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12826, N'Mayotte', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Mayotte')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12827, N'South Africa', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'South Africa')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12828, N'Zambia', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Zambia')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12829, N'Zimbabwe', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Zimbabwe')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12830, N'Europe', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Europe')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12831, N'Anonymous Proxy', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Anonymous Proxy')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12832, N'Satellite Provider', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Satellite Provider')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12833, N'Curacao', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Curacao')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12834, N'Serbia', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Serbia')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12835, N'South Sudan', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'South Sudan')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12836, N'Jersey', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Jersey')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12837, N'Asia/Pacific Region', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Asia/Pacific Region')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12838, N'Montenegro', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Montenegro')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12839, N'Isle Of Man', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Isle Of Man')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12840, N'Guernsey', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Guernsey')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12841, N'Saint Barthelemy', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Saint Barthelemy')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12842, N'Bonaire, Saint Eustatius And Saba', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Bonaire, Saint Eustatius And Saba')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12843, N'Sint Maarten (Dutch Part)', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Sint Maarten (Dutch Part)')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12844, N'Saint Martin', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Saint Martin')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12845, N'Badin', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Badin')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12846, N'Dadu', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Dadu')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12847, N'Digri', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Digri')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12848, N'Diplo', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Diplo')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12849, N'Dokri', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Dokri')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12850, N'Ghotki', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Ghotki')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12851, N'Haala', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Haala')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12852, N'Hyderabad', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Hyderabad')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12853, N'Islamkot', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Islamkot')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12854, N'Jacobabad', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Jacobabad')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12855, N'Jamshoro', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Jamshoro')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12856, N'Jungshahi', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Jungshahi')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12857, N'Kandiaro', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Kandiaro')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12859, N'Kashmore', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Kashmore')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12860, N'Keti Bandar', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Keti Bandar')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12861, N'Khairpur', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Khairpur')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12862, N'Kotri', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Kotri')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12863, N'Larkana', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Larkana')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12864, N'Matiari', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Matiari')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12865, N'Mehar', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Mehar')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12866, N'Mirpur Khas', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Mirpur Khas')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12867, N'Mithani', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Mithani')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12868, N'Mithi', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Mithi')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12869, N'Mehrabpur', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Mehrabpur')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12870, N'Moro', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Moro')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12871, N'Nagarparkar', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Nagarparkar')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12872, N'Naudero', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Naudero')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12873, N'Naushahro Feroze', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Naushahro Feroze')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12874, N'Naushara', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Naushara')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12875, N'Nawabshah', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Nawabshah')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12876, N'Nazimabad', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Nazimabad')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12877, N'Ranipur', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Ranipur')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12878, N'Ratodero', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Ratodero')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12879, N'Rohri', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Rohri')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12880, N'Sakrand', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Sakrand')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12881, N'Sanghar', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Sanghar')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12882, N'Shahbandar', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Shahbandar')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12883, N'Shahdadkot', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Shahdadkot')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12884, N'Shahdadpur', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Shahdadpur')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12885, N'Shahpur Chakar', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Shahpur Chakar')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12886, N'Shikarpaur', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Shikarpaur')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12887, N'Sukkur', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Sukkur')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12888, N'Tando Adam Khan', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Tando Adam Khan')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12889, N'Tando Allahyar', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Tando Allahyar')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12890, N'Thatta', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Thatta')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12891, N'Umerkot', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Umerkot')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12892, N'Warah', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Warah')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12893, N'Ahmadpur East', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Ahmadpur East')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12894, N'Ahmed Nager Chatha', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Ahmed Nager Chatha')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12895, N'Alipur', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Alipur')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12896, N'Arifwala', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Arifwala')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12897, N'Attock', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Attock')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12898, N'Bhalwal', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Bhalwal')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12899, N'Bahawalnagar', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Bahawalnagar')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12900, N'Bahawalpur', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Bahawalpur')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12901, N'Bhakkar', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Bhakkar')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12902, N'Burewala', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Burewala')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12903, N'Chillianwala', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Chillianwala')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12904, N'Chakwal', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Chakwal')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12905, N'Chichawatni', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Chichawatni')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12906, N'Chiniot', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Chiniot')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12907, N'Daska', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Daska')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12908, N'Darya Khan', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Darya Khan')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12909, N'Dera Ghazi Khan', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Dera Ghazi Khan')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12910, N'Dhaular', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Dhaular')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12911, N'Dina', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Dina')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12912, N'Dinga', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Dinga')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12913, N'Dipalpur', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Dipalpur')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12914, N'Faisalabad', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Faisalabad')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12915, N'Fateh Jang', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Fateh Jang')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12916, N'Ghakhar Mandi', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Ghakhar Mandi')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12917, N'Gujranwala', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Gujranwala')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12918, N'Gujrat', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Gujrat')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12919, N'Gujar Khan', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Gujar Khan')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12920, N'Hafizabad', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Hafizabad')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12921, N'Haroonabad', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Haroonabad')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12922, N'Hasilpur', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Hasilpur')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12923, N'Haveli Lakha', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Haveli Lakha')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12924, N'Jampur', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Jampur')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12925, N'Jhang', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Jhang')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12926, N'Jhelum', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Jhelum')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12927, N'Kalabagh', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Kalabagh')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12928, N'Karor Lal Esan', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Karor Lal Esan')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12929, N'Kasur', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Kasur')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12930, N'Kamalia', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Kamalia')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12931, N'Kamoke', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Kamoke')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12932, N'Khanewal', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Khanewal')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12933, N'Khanpur', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Khanpur')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12934, N'Kharian', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Kharian')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12935, N'Khushab', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Khushab')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12936, N'Kot Adu', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Kot Adu')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12937, N'Jauharabad', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Jauharabad')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12938, N'Lahore', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Lahore')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12939, N'Lalamusa', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Lalamusa')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12940, N'Layyah', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Layyah')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12941, N'Liaquat Pur', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Liaquat Pur')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12942, N'Lodhran', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Lodhran')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12943, N'Mamoori', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Mamoori')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12944, N'Mandi Bahauddin', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Mandi Bahauddin')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12945, N'Mailsi', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Mailsi')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12946, N'Mian Channu', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Mian Channu')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12947, N'Mianwali', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Mianwali')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12948, N'Multan', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Multan')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12949, N'Murree', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Murree')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12950, N'Muridke', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Muridke')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12951, N'Muzaffargarh', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Muzaffargarh')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12952, N'Narowal', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Narowal')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12953, N'Okara', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Okara')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12954, N'Renala Khurd', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Renala Khurd')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12955, N'Pakpattan', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Pakpattan')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12956, N'Pattoki', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Pattoki')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12957, N'Pir Mahal', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Pir Mahal')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12958, N'Qila Didar Singh', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Qila Didar Singh')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12959, N'Rabwah', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Rabwah')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12960, N'Raiwind', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Raiwind')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12961, N'Rajanpur', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Rajanpur')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12962, N'Rahim Yar Khan', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Rahim Yar Khan')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12963, N'Rawalpindi', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Rawalpindi')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12964, N'Sadiqabad', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Sadiqabad')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12965, N'Safdarabad', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Safdarabad')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12966, N'Sahiwal', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Sahiwal')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12967, N'Sangla Hill', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Sangla Hill')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12968, N'Sarai Alamgir', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Sarai Alamgir')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12969, N'Sargodha', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Sargodha')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12970, N'Shakargarh', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Shakargarh')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12971, N'Sheikhupura', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Sheikhupura')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12972, N'Sialkot', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Sialkot')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12973, N'Sohawa', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Sohawa')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12974, N'Soianwala', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Soianwala')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12975, N'Talagang', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Talagang')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12976, N'Taxila', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Taxila')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12977, N'Toba Tek Singh', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Toba Tek Singh')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12978, N'Vehari', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Vehari')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12979, N'Wah Cantonment', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Wah Cantonment')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12980, N'Wazirabad', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Wazirabad')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12981, N'Quetta', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Quetta')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12982, N'Khuzdar', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Khuzdar')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12983, N'Turbat', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Turbat')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12984, N'Chaman', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Chaman')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12985, N'Hub', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Hub')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12986, N'Sibi', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Sibi')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12987, N'Zhob', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Zhob')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12988, N'Gwadar', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Gwadar')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12989, N'Dera Murad Jamali', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Dera Murad Jamali')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12990, N'Dera Allah Yar', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Dera Allah Yar')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12991, N'Usta Mohammad', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Usta Mohammad')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12992, N'Loralai', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Loralai')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12993, N'Pasni', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Pasni')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12994, N'Kharan', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Kharan')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12995, N'Mastung', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Mastung')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12996, N'Nushki', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Nushki')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12997, N'Kalat', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Kalat')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12998, N'Abbottabad', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Abbottabad')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (12999, N'Adezai', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Adezai')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (13000, N'Alpuri', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Alpuri')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (13001, N'Ayubia', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Ayubia')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (13002, N'Banda Daud Shah', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Banda Daud Shah')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (13003, N'Bannu', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Bannu')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (13004, N'Batkhela', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Batkhela')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (13005, N'Battagram', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Battagram')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (13006, N'Birote', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Birote')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (13007, N'Chakdara', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Chakdara')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (13008, N'Charsadda', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Charsadda')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (13009, N'Chitral', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Chitral')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (13010, N'Daggar', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Daggar')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (13011, N'Dargai', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Dargai')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (13012, N'Darya Khan', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Darya Khan')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (13013, N'Dera Ismail Khan', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Dera Ismail Khan')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (13014, N'Dir', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Dir')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (13015, N'Drosh', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Drosh')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (13016, N'Hangu', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Hangu')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (13017, N'Haripur', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Haripur')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (13018, N'Karak', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Karak')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (13019, N'Kohat', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Kohat')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (13020, N'Lakki Marwat', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Lakki Marwat')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (13021, N'Latamber', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Latamber')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (13022, N'Madyan', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Madyan')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (13023, N'Mansehra', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Mansehra')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (13024, N'Mardan', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Mardan')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (13025, N'Mastuj', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Mastuj')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (13026, N'Mingora', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Mingora')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (13027, N'Nowshera', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Nowshera')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (13028, N'Paharpur', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Paharpur')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (13029, N'Peshawar', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Peshawar')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (13030, N'Saidu Sharif', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Saidu Sharif')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (13031, N'Swabi', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Swabi')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (13032, N'Swat', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Swat')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (13033, N'Tangi', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Tangi')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (13034, N'Tank', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Tank')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (13035, N'Thall', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Thall')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (13036, N'Timergara', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Timergara')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (13037, N'Tordher', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Tordher')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (13038, N'Islamabad', 12761, CAST(N'2014-06-16 20:25:00.000' AS DateTime), CAST(N'2014-06-16 20:25:00.000' AS DateTime), 1, 1, N'Islamabad')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (13039, N'Scotland', NULL, CAST(N'2014-06-17 19:47:34.943' AS DateTime), CAST(N'2014-06-17 19:47:34.943' AS DateTime), 0, 1, NULL)
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (13040, N'London', 12665, CAST(N'2014-06-20 18:37:33.360' AS DateTime), CAST(N'2014-06-20 18:37:33.360' AS DateTime), 0, 1, NULL)
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (13041, N'Columbo', 12715, CAST(N'2014-08-18 15:07:40.667' AS DateTime), CAST(N'2014-08-18 15:07:40.667' AS DateTime), 0, 1, NULL)
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (13042, N'Jerusalem', 12690, CAST(N'2014-08-18 15:17:21.590' AS DateTime), CAST(N'2014-08-18 15:17:21.590' AS DateTime), 0, 1, NULL)
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (13043, N'Los Angeles', 12813, CAST(N'2014-08-18 22:45:12.610' AS DateTime), CAST(N'2014-08-18 22:45:12.610' AS DateTime), 0, 1, NULL)
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (13044, N'Delhi', 12691, CAST(N'2014-08-19 19:31:54.997' AS DateTime), CAST(N'2014-08-19 19:31:54.997' AS DateTime), 0, 1, NULL)
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (13045, N'Lisbon', 12767, CAST(N'2015-02-13 16:02:51.850' AS DateTime), CAST(N'2015-02-13 16:02:51.850' AS DateTime), 0, 1, NULL)
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (13046, N'Tokyo', 12699, CAST(N'2015-02-13 17:06:51.560' AS DateTime), CAST(N'2015-02-13 17:06:51.560' AS DateTime), 0, 1, NULL)
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (13047, N'Sydney', 12603, CAST(N'2015-02-14 14:06:26.100' AS DateTime), CAST(N'2015-02-14 14:06:26.100' AS DateTime), 0, 1, NULL)
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (13048, N'Washington', 12813, CAST(N'2015-02-14 14:12:50.597' AS DateTime), CAST(N'2015-02-14 14:12:50.597' AS DateTime), 0, 1, NULL)
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (13049, N'Tehran', 12694, CAST(N'2015-02-14 15:05:43.447' AS DateTime), CAST(N'2015-02-14 15:05:43.447' AS DateTime), 0, 1, NULL)
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (13050, N'Christchurch', 12754, CAST(N'2015-02-14 16:52:51.117' AS DateTime), CAST(N'2015-02-14 16:52:51.117' AS DateTime), 0, 1, NULL)
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (13051, N'Melbourne', 12603, CAST(N'2015-02-14 20:23:09.627' AS DateTime), CAST(N'2015-02-14 20:23:09.627' AS DateTime), 0, 1, NULL)
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (13052, N'New Dehli', 12691, CAST(N'2015-02-16 08:52:49.467' AS DateTime), CAST(N'2015-02-16 08:52:49.467' AS DateTime), 0, 1, NULL)
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (13053, N'Nelson', 12754, CAST(N'2015-02-16 09:08:15.717' AS DateTime), CAST(N'2015-02-16 09:08:15.717' AS DateTime), 0, 1, NULL)
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (13054, N'Mumbai', 12691, CAST(N'2015-02-16 10:50:54.417' AS DateTime), CAST(N'2015-02-16 10:50:54.417' AS DateTime), 0, 1, NULL)
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (13055, N'Mirpur, Azad Kashmir', 12761, CAST(N'2015-02-16 11:12:38.043' AS DateTime), CAST(N'2015-02-16 11:12:38.043' AS DateTime), 0, 1, NULL)
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (13056, N'Miranshah', 12761, CAST(N'2015-02-16 11:14:33.200' AS DateTime), CAST(N'2015-02-16 11:14:33.200' AS DateTime), 0, 1, NULL)
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (13057, N'Brisbane', 12603, CAST(N'2015-02-16 11:26:26.703' AS DateTime), CAST(N'2015-02-16 11:26:26.703' AS DateTime), 0, 1, NULL)
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (13058, N'San Francisco', 12813, CAST(N'2015-02-16 16:17:33.733' AS DateTime), CAST(N'2015-02-16 16:17:33.733' AS DateTime), 0, 1, NULL)
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (13059, N'Adelaide', 12603, CAST(N'2015-02-17 11:33:44.137' AS DateTime), CAST(N'2015-02-17 11:33:44.137' AS DateTime), 0, 1, NULL)
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (13060, N'Beirut', 12712, CAST(N'2015-02-17 11:54:24.043' AS DateTime), CAST(N'2015-02-17 11:54:24.043' AS DateTime), 0, 1, NULL)
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (13061, N'Niamey', 12745, CAST(N'2015-02-17 11:59:41.687' AS DateTime), CAST(N'2015-02-17 11:59:41.687' AS DateTime), 0, 1, NULL)
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (13062, N'Hamilton', 12754, CAST(N'2015-02-17 12:01:47.827' AS DateTime), CAST(N'2015-02-17 12:01:47.827' AS DateTime), 0, 1, NULL)
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (13063, N'Riyadh', 12775, CAST(N'2015-02-17 12:33:56.767' AS DateTime), CAST(N'2015-02-17 12:33:56.767' AS DateTime), 0, 1, NULL)
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (13064, N'Paris', 12663, CAST(N'2015-02-17 12:59:16.647' AS DateTime), CAST(N'2015-02-17 12:59:16.647' AS DateTime), 0, 1, NULL)
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (13065, N'New York', 12813, CAST(N'2015-02-17 13:16:35.180' AS DateTime), CAST(N'2015-02-17 13:16:35.180' AS DateTime), 0, 1, NULL)
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (13066, N'Dubai', NULL, CAST(N'2015-02-18 13:11:34.567' AS DateTime), CAST(N'2015-02-18 13:11:34.567' AS DateTime), 0, 1, NULL)
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (13067, N'Dhaka', 12609, CAST(N'2015-02-23 07:02:06.040' AS DateTime), CAST(N'2015-02-23 07:02:06.040' AS DateTime), 0, 1, NULL)
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (13068, N'Kabul', 12592, CAST(N'2015-02-23 08:06:01.263' AS DateTime), CAST(N'2015-02-23 08:06:01.263' AS DateTime), 0, 1, NULL)
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (13069, N'Beijing', 12636, CAST(N'2015-02-23 08:45:16.643' AS DateTime), CAST(N'2015-02-23 08:45:16.643' AS DateTime), 0, 1, NULL)
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (13070, N'Cairo', 12653, CAST(N'2015-02-23 09:08:04.570' AS DateTime), CAST(N'2015-02-23 09:08:04.570' AS DateTime), 0, 1, NULL)
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (13071, N'Dunedin', 12754, CAST(N'2015-02-26 13:46:41.263' AS DateTime), CAST(N'2015-02-26 13:46:41.263' AS DateTime), 0, 1, NULL)
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (13072, N'Sanaa', 12825, CAST(N'2015-02-26 14:04:05.237' AS DateTime), CAST(N'2015-02-26 14:04:05.237' AS DateTime), 0, 1, NULL)
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (13073, N'Wellington', 12754, CAST(N'2015-02-27 12:12:39.670' AS DateTime), CAST(N'2015-02-27 12:12:39.670' AS DateTime), 0, 1, NULL)
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (13074, N'Baghdad', 12693, CAST(N'2015-02-27 12:20:41.313' AS DateTime), CAST(N'2015-02-27 12:20:41.313' AS DateTime), 0, 1, NULL)
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (13075, N'Miami', 12813, CAST(N'2015-02-27 12:48:11.910' AS DateTime), CAST(N'2015-02-27 12:48:11.910' AS DateTime), 0, 1, NULL)
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (13076, N'Moscow', 12773, CAST(N'2015-03-04 17:25:03.927' AS DateTime), CAST(N'2015-03-04 17:25:03.927' AS DateTime), 0, 1, NULL)
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (13077, N'Canberra', 12603, CAST(N'2015-03-04 18:17:58.120' AS DateTime), CAST(N'2015-03-04 18:17:58.120' AS DateTime), 0, 1, NULL)
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (13078, N'Geneva', 12631, CAST(N'2015-03-04 18:29:28.077' AS DateTime), CAST(N'2015-03-04 18:29:28.077' AS DateTime), 0, 1, NULL)
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (13079, N'Napier', 12754, CAST(N'2015-03-04 18:52:19.063' AS DateTime), CAST(N'2015-03-04 18:52:19.063' AS DateTime), 0, 1, NULL)
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (13080, N'Brussels', 12610, CAST(N'2015-03-04 19:34:26.147' AS DateTime), CAST(N'2015-03-04 19:34:26.147' AS DateTime), 0, 1, NULL)
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (13081, N'Berlin', 12645, CAST(N'2015-03-05 11:40:09.727' AS DateTime), CAST(N'2015-03-05 11:40:09.727' AS DateTime), 0, 1, NULL)
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (13082, N'Madrid', 12656, CAST(N'2015-03-05 12:53:40.860' AS DateTime), CAST(N'2015-03-05 12:53:40.860' AS DateTime), 0, 1, NULL)
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (13083, N'Perth', 12603, CAST(N'2015-03-05 13:14:09.613' AS DateTime), CAST(N'2015-03-05 13:14:09.613' AS DateTime), 0, 1, NULL)
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (13084, N'Kiev', 12810, CAST(N'2015-03-05 13:35:35.180' AS DateTime), CAST(N'2015-03-05 13:35:35.180' AS DateTime), 0, 1, NULL)
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (13085, N'Istanbul', 12805, CAST(N'2015-03-05 14:03:53.383' AS DateTime), CAST(N'2015-03-05 14:03:53.383' AS DateTime), 0, 1, NULL)
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (13086, N'Rome', 12696, CAST(N'2015-03-05 14:25:21.027' AS DateTime), CAST(N'2015-03-05 14:25:21.027' AS DateTime), 0, 1, NULL)
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (13087, N'Seoul', 12707, CAST(N'2015-03-05 19:58:37.223' AS DateTime), CAST(N'2015-03-05 19:58:37.223' AS DateTime), 0, 1, NULL)
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (13088, N'Barcelona', 12656, CAST(N'2015-03-06 14:23:58.277' AS DateTime), CAST(N'2015-03-06 14:23:58.277' AS DateTime), 0, 1, NULL)
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (13089, N'Damascus', 12792, CAST(N'2015-03-06 16:50:55.807' AS DateTime), CAST(N'2015-03-06 16:50:55.807' AS DateTime), 0, 1, NULL)
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (13090, N'Auckland', 12603, CAST(N'2015-03-09 09:37:14.077' AS DateTime), CAST(N'2015-03-09 09:37:14.077' AS DateTime), 0, 1, NULL)
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (13091, N'Hobart', 12603, CAST(N'2015-03-11 17:11:26.843' AS DateTime), CAST(N'2015-03-11 17:11:26.843' AS DateTime), 0, 1, NULL)
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (13092, N'Athens', 12676, CAST(N'2015-03-12 11:12:49.187' AS DateTime), CAST(N'2015-03-12 11:12:49.187' AS DateTime), 0, 1, NULL)
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (13093, N'Colombo', 12715, CAST(N'2015-03-12 11:32:21.190' AS DateTime), CAST(N'2015-03-12 11:32:21.190' AS DateTime), 0, 1, NULL)
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (13094, N'Shanghai', 12636, CAST(N'2015-03-12 13:02:57.247' AS DateTime), CAST(N'2015-03-12 13:02:57.247' AS DateTime), 0, 1, NULL)
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (13095, N'Abuja', 12747, CAST(N'2015-03-12 13:39:52.143' AS DateTime), CAST(N'2015-03-12 13:39:52.143' AS DateTime), 0, 1, NULL)
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (13096, N'Kuala Lumpur', 12741, CAST(N'2015-03-12 19:35:17.780' AS DateTime), CAST(N'2015-03-12 19:35:17.780' AS DateTime), 0, 1, NULL)
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (13097, N'Milan', 12696, CAST(N'2015-03-13 10:26:55.743' AS DateTime), CAST(N'2015-03-13 10:26:55.743' AS DateTime), 0, 1, NULL)
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (13098, N'Ankara', 12805, CAST(N'2015-03-13 10:47:36.137' AS DateTime), CAST(N'2015-03-13 10:47:36.137' AS DateTime), 0, 1, NULL)
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (13099, N'Amman', 12698, CAST(N'2015-03-13 12:34:34.633' AS DateTime), CAST(N'2015-03-13 12:34:34.633' AS DateTime), 0, 1, NULL)
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (13100, N'Frankfurt', 12645, CAST(N'2015-03-19 17:29:26.957' AS DateTime), CAST(N'2015-03-19 17:29:26.957' AS DateTime), 0, 1, NULL)
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (13101, N'Srinagar', NULL, CAST(N'2015-03-27 12:28:13.310' AS DateTime), CAST(N'2015-03-27 12:28:13.310' AS DateTime), 0, 1, NULL)
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (13102, N'Indian Wells', 12813, CAST(N'2015-03-27 12:39:35.053' AS DateTime), CAST(N'2015-03-27 12:39:35.053' AS DateTime), 0, 1, NULL)
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (13103, N'Lausanne', 12631, CAST(N'2015-03-27 13:07:58.750' AS DateTime), CAST(N'2015-03-27 13:07:58.750' AS DateTime), 0, 1, NULL)
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (13104, N'Vienna', 12602, CAST(N'2015-03-27 13:32:26.283' AS DateTime), CAST(N'2015-03-27 13:32:26.283' AS DateTime), 0, 1, NULL)
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (13105, N'Manila', 12760, CAST(N'2015-03-27 18:10:12.377' AS DateTime), CAST(N'2015-03-27 18:10:12.377' AS DateTime), 0, 1, NULL)
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (13106, N'Lucknow', 12691, CAST(N'2015-03-27 18:56:22.367' AS DateTime), CAST(N'2015-03-27 18:56:22.367' AS DateTime), 0, 1, NULL)
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (13107, N'Other', NULL, CAST(N'2015-03-27 19:15:42.463' AS DateTime), CAST(N'2015-03-27 19:15:42.463' AS DateTime), 0, 1, NULL)
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (13108, N'Bangkok', 12798, CAST(N'2015-04-02 20:16:38.720' AS DateTime), CAST(N'2015-04-02 20:16:38.720' AS DateTime), 0, 1, NULL)
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (13109, N'Kathmandu', 12751, CAST(N'2015-05-02 19:02:01.473' AS DateTime), CAST(N'2015-05-02 19:02:01.473' AS DateTime), 0, 1, NULL)
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (13110, N'Misc', NULL, CAST(N'2014-06-16 20:16:53.487' AS DateTime), CAST(N'2014-06-16 20:16:53.487' AS DateTime), 1, 1, N'Misc')
GO
INSERT [dbo].[Location] ([LocationId], [Location], [ParentId], [CreationDate], [LastUpdateDate], [IsActive], [IsApproved], [LocationOriginal]) VALUES (13111, N'Misc', NULL, CAST(N'2015-05-18 22:25:30.220' AS DateTime), CAST(N'2015-05-18 22:25:30.220' AS DateTime), 1, NULL, NULL)
GO
SET IDENTITY_INSERT [dbo].[Location] OFF
GO
