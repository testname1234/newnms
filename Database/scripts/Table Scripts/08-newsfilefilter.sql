CREATE TABLE NewsFileFilter
(
NewsFileFilterId int IDENTITY(1,1) NOT NULL PRIMARY KEY,
FilterId int FOREIGN KEY REFERENCES Filter (FilterId),
NewsId int FOREIGN KEY REFERENCES News(NewsId),
IsActive bit,
CreationDate datetime,
LastUpdateDate datetime
)


ALTER TABLE NewsFile ADD DescriptionText varchar(max)
ALTER TABLE NewsFile ADD ResourceGuid varchar(max)
ALTER TABLE NewsFile ADD NewsStatus int

