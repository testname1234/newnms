USE [NMS]
GO
SET IDENTITY_INSERT [dbo].[TeamRole] ON 

INSERT [dbo].[TeamRole] ([TeamRoleId], [Name]) VALUES (1, N'Admin')
INSERT [dbo].[TeamRole] ([TeamRoleId], [Name]) VALUES (2, N'Report')
INSERT [dbo].[TeamRole] ([TeamRoleId], [Name]) VALUES (3, N'Field Reporter')
INSERT [dbo].[TeamRole] ([TeamRoleId], [Name]) VALUES (4, N'Verifier')
INSERT [dbo].[TeamRole] ([TeamRoleId], [Name]) VALUES (5, N'Producer')
INSERT [dbo].[TeamRole] ([TeamRoleId], [Name]) VALUES (20, N'NLE')
INSERT [dbo].[TeamRole] ([TeamRoleId], [Name]) VALUES (21, N'Story Writer')
INSERT [dbo].[TeamRole] ([TeamRoleId], [Name]) VALUES (52, N'Headline Producer')
INSERT [dbo].[TeamRole] ([TeamRoleId], [Name]) VALUES (1058, N'NMSReporter')
INSERT [dbo].[TeamRole] ([TeamRoleId], [Name]) VALUES (1059, N'NMSProducer')
INSERT [dbo].[TeamRole] ([TeamRoleId], [Name]) VALUES (1067, N'AllNewsProducer')
SET IDENTITY_INSERT [dbo].[TeamRole] OFF
