alter table tickercategory
add ParentTickerCategoryId int;
alter table tickercategory
add LanguageCode int;
alter table tickercategory
add TickerSize int not null default(0);
GO

insert into tickercategory (CategoryId,Name,SequenceNumber,CreationDate,LastUpdateDate,IsActive,ParentTickerCategoryId,LanguageCode, TickerSize)
values (NULL, 'Breaking News', 1, getdate(), getdate(), 1, NULL, 1, 20),
(NULL, 'Widgets', 3, getdate(), getdate(), 1, NULL, 1, 20),
(NULL, 'City Names', 4, getdate(), getdate(), 1, NULL, 1, 20),
(NULL, 'Entity Names', 5, getdate(), getdate(), 1, NULL, 1, 20),
(NULL, 'Popular Topics', 6, getdate(), getdate(), 1, NULL, 1, 20),
(NULL, 'Narrative Topics', 7, getdate(), getdate(), 1, NULL, 1, 20),
(NULL, '20 News A', 2, getdate(), getdate(), 1, NULL, 1, 20),
(NULL, '30 News A', 8, getdate(), getdate(), 1, NULL, 1, 20),
(NULL, '30 News B', 9, getdate(), getdate(), 1, NULL, 1, 20),
(NULL, '30 News C', 10, getdate(), getdate(), 1, NULL, 1, 20),
(NULL, 'Alert', 11, getdate(), getdate(), 1, NULL, 1, 20),
(NULL, 'Face Tickers', 12, getdate(), getdate(), 1, NULL, 1, 20),
(NULL, 'Breaking A', 1, getdate(), getdate(), 1, 1, 1, 20),
(NULL, 'Breaking B', 2, getdate(), getdate(), 1, 1, 1, 20),
(NULL, 'Breaking C', 3, getdate(), getdate(), 1, 1, 1, 20),
(NULL, 'Breaking D', 4, getdate(), getdate(), 1, 1, 1, 20),
(NULL, 'Business', 1, getdate(), getdate(), 1, 2, 1, 20),
(NULL, 'Sports', 2, getdate(), getdate(), 1, 2, 1, 20),
(NULL, 'Weather', 3, getdate(), getdate(), 1, 2, 1, 20)



GO

alter table ticker 
add NewsFileId int

alter table ticker 
add IsApproved bit NOT NULL default(0)

alter table ticker 
add ViewCount int NOT NULL default(0)

alter table ticker 
add [Text] nvarchar(max)
