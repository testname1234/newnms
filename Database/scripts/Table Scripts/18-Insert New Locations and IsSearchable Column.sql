alter table Location add IsSearchable bit not null default(0);
GO
insert into location (location,CreationDate,LastUpdateDate,IsActive,IsApproved,IsSearchable)
select 'P.E.C.H.S., Jamshed Town, Karachi',getdate(),getdate(),1,1,1 union
select 'Karachi Memon Society, Jamshed Town, Karachi',getdate(),getdate(),1,1,1 union
select 'Sindhi Muslim Housing Society, Jamshed Town, Karachi',getdate(),getdate(),1,1,1 union
select 'Karachi Administrative Society, Jamshed Town, Karachi',getdate(),getdate(),1,1,1 union
select 'Baloch Colony, Umer Colony, Jinnah Housing Society, Jamshed Town, Karachi',getdate(),getdate(),1,1,1 union
select 'Darul Aman Society, Jamshed Town, Karachi',getdate(),getdate(),1,1,1 union
select 'Catholic Colony, Pakistan Quarters, Jamshed Town, Karachi',getdate(),getdate(),1,1,1 union
select 'Akhtar Colony, Kashmir Colony, Jamshed Town, Karachi',getdate(),getdate(),1,1,1 union
select 'Binori Town, Jamshed Town, Karachi',getdate(),getdate(),1,1,1 union
select 'Amil Colony, Jamshed Town, Karachi',getdate(),getdate(),1,1,1 union
select 'Khudadad Colony, Shikarpur Colony, Jamshed Town, Karachi',getdate(),getdate(),1,1,1 union
select 'Manzoor Colony, Azam Basti, Jamshed Town, Karachi',getdate(),getdate(),1,1,1 union
select 'People''s Colony, Islam Nagar, Jamshed Town, Karachi',getdate(),getdate(),1,1,1 union
select 'Soldier Bazar, Jamshed Town, Karachi',getdate(),getdate(),1,1,1 union
select 'Garden East, Jamshed Town, Karachi',getdate(),getdate(),1,1,1 union
select 'Chanesar Goth, Masoom Shah Colony, Jamshed Town, Karachi',getdate(),getdate(),1,1,1 union
select 'Martin Quarters, Jamshed Town, Karachi',getdate(),getdate(),1,1,1 union
select 'Muhammadi Colony, Mustafa Colony, Jamshed Town, Karachi',getdate(),getdate(),1,1,1 union
select 'Hyderabad Colony, Jamshed Quarter, Jamshed Town, Karachi',getdate(),getdate(),1,1,1 union
select 'Chapra Society, Jamshed Town, Karachi',getdate(),getdate(),1,1,1 union
select 'Abyssinia Lines, Jamshed Town, Karachi',getdate(),getdate(),1,1,1 union
select 'Lines Area, Jamshed Town, Karachi',getdate(),getdate(),1,1,1 union
select 'Jut Lane Lines, Jacob Lines, Jinnah Colony, Jamshed Town, Karachi',getdate(),getdate(),1,1,1 union
select 'Mehmoodabad, Liaquat Ashraf Colony, Jamshed Town, Karachi',getdate(),getdate(),1,1,1 union
select 'Bihar Colony, Allama Iqbal Colony, Lyari Town, Karachi',getdate(),getdate(),1,1,1 union
select 'Gulistan Colony, Lyari Town, Karachi',getdate(),getdate(),1,1,1 union
select 'Kutchi Colony, Chakiwara, Ghazi Nagar, Rexer Lane, Lyari Town, Karachi',getdate(),getdate(),1,1,1 union
select 'Baghdadi, Lyari Town, Karachi',getdate(),getdate(),1,1,1 union
select 'Saeedabad, Saifi Lane, Lyari Town, Karachi',getdate(),getdate(),1,1,1 union
select 'Darya Abad, Nawaabad, Khada Memon Society  , Jinnahabad, Rasoolabad, Lyari Khadda, Lyari Town, Karachi',getdate(),getdate(),1,1,1 union
select 'Baloch Para, Ragiwara, Singo Lane, Moosa Lane, Kumbhar Wara, Zikri Para, Juma Kalri, Lyari Town, Karachi',getdate(),getdate(),1,1,1 union
select 'Agra Taj Colony, Lyari Town, Karachi',getdate(),getdate(),1,1,1 union
select 'Shah Baig Line, Hingorabad, Usmanabad, Bakra Pirhi, Liaquat Colony, Miran Naka, Lyari Town, Karachi',getdate(),getdate(),1,1,1 union
select 'Clifton, Saddar Town, Karachi',getdate(),getdate(),1,1,1 union
select 'PIDC, Saddar Town, Karachi',getdate(),getdate(),1,1,1 union
select 'I.I. Chunrigarh, Saddar Town, Karachi',getdate(),getdate(),1,1,1 union
select 'Tower, Saddar, Saddar Town, Karachi',getdate(),getdate(),1,1,1 union
select 'Pakistan Quarters, Saddar Town, Karachi',getdate(),getdate(),1,1,1 union
select 'Garden West, Saddar Town, Karachi',getdate(),getdate(),1,1,1 union
select 'Arambagh, Saddar Town, Karachi',getdate(),getdate(),1,1,1 union
select 'Timber Market, Saddar Town, Karachi',getdate(),getdate(),1,1,1 union
select 'Bohri Bazar, Saddar Town, Karachi',getdate(),getdate(),1,1,1 union
select 'Urdu Bazar, Saddar Town, Karachi',getdate(),getdate(),1,1,1 union
select 'Gazdarabad, Saddar Town, Karachi',getdate(),getdate(),1,1,1 union
select 'Kharadar, Saddar Town, Karachi',getdate(),getdate(),1,1,1 union
select 'Jubilee Market, Saddar Town, Karachi',getdate(),getdate(),1,1,1 union
select 'Railway City Colony, Civil Lines, Old Haji Camp, Saddar Town, Karachi',getdate(),getdate(),1,1,1 union
select 'Chakiwara, Saddar Town, Karachi',getdate(),getdate(),1,1,1 union
select 'Dhobi Ghat, Millat Nagar/Islam Pura, Dhramsi Wara, Saddar Town, Karachi',getdate(),getdate(),1,1,1 union
select 'Bhimpura, Hijrat Colony, Saddar Town, Karachi',getdate(),getdate(),1,1,1 union
select 'Ramswami, Ranchhor Lane, Ghanchi Para, Saddar Town, Karachi',getdate(),getdate(),1,1,1 union
select 'Ghazi Nagar, Saddar Town, Karachi',getdate(),getdate(),1,1,1 union
select 'Gulshan-e-Iqbal, Gulshan Town, Karachi',getdate(),getdate(),1,1,1 union
select 'Gulistan-e-Johar, Gulshan Town, Karachi',getdate(),getdate(),1,1,1 union
select 'Bahadurabad, Sharfabad, Gulshan Town, Karachi',getdate(),getdate(),1,1,1 union
select 'Mohammad Ali Society, Adamji Nagar, Dhoraji Colony, Gulshan Town, Karachi',getdate(),getdate(),1,1,1 union
select 'KDA Scheme 1, Gulshan Town, Karachi',getdate(),getdate(),1,1,1 union
select 'KDA Officers Society, Gulshan Town, Karachi',getdate(),getdate(),1,1,1 union
select ' Civic Centre, Gulshan Town, Karachi',getdate(),getdate(),1,1,1 union
select 'Memon Nagar, NED Staff Colony, Gulshan Town, Karachi',getdate(),getdate(),1,1,1 union
select 'Jamali Colony, Delhi Mercentile Society, Gulshan Town, Karachi',getdate(),getdate(),1,1,1 union
select 'Abbas Town, Karachi University Employees Society, Federal Govt. Employees Society, Darul Uloom Society,  Gulzar-E-Hijri, Gulshan Town, Karachi',getdate(),getdate(),1,1,1 union
select 'Metroville Society, Gulshan Town, Karachi',getdate(),getdate(),1,1,1 union
select ' P.I.B. Colony, Gulshan Town, Karachi',getdate(),getdate(),1,1,1 union
select ' Pehlwan Goth,  Safurah Goth, Gulshan Town, Karachi',getdate(),getdate(),1,1,1 union
select 'Mohammad Khan Goth, Gulshan Town, Karachi',getdate(),getdate(),1,1,1 union
select 'Shafi Colony, Shafi Colony, Gulshan Town, Karachi',getdate(),getdate(),1,1,1 union
select ' Shanti Nagar,  Jamil Colony, Gulshan Town, Karachi',getdate(),getdate(),1,1,1 union
select 'Mausamyat,  Essa Nagri,  Gillani Railway Station, Gulshan Town, Karachi',getdate(),getdate(),1,1,1 union
select 'Mujahid Colony, Gulshan Town, Karachi',getdate(),getdate(),1,1,1 union
select 'Zia ul Haq Colony, Gulshan Town, Karachi',getdate(),getdate(),1,1,1 union
select 'Korangi,  Korangi Town, Karachi',getdate(),getdate(),1,1,1 union
select 'Nishtar Colony, Hasrat Mohani Colony,  Korangi Town, Karachi',getdate(),getdate(),1,1,1 union
select ' Chakra Goth,  100 Quarters,  Gulzar Colony, Darussalam Town, Mehran Town, Ghosia Colony, P&T Colony, Bilawal Colony,  Korangi Town, Karachi',getdate(),getdate(),1,1,1 union
select ' Mustafa Taj Colony,  Korangi Town, Karachi',getdate(),getdate(),1,1,1 union
select 'Sharifabad,  Korangi Town, Karachi',getdate(),getdate(),1,1,1 union
select ' Bilal Colony,  Nasir Colony, Usman Town, Zaman Town, Christian Colony,  Korangi Town, Karachi',getdate(),getdate(),1,1,1 union
select 'Sweeper Colony, Noorani Basti,  Korangi Town, Karachi',getdate(),getdate(),1,1,1 union
select 'Korangi,  Landhi Town, Karachi',getdate(),getdate(),1,1,1 union
select 'Area 36-B, 36-G Area, Landhi Small Industry,  Khawaja Ajmer Nagri, Landhi,  Landhi Town, Karachi',getdate(),getdate(),1,1,1 union
select 'Zamanabad Housing Society,  Landhi Town, Karachi',getdate(),getdate(),1,1,1 union
select 'Ahsanabad,  Landhi Town, Karachi',getdate(),getdate(),1,1,1 union
select 'Sector 21,  Landhi Town, Karachi',getdate(),getdate(),1,1,1 union
select 'Future Colony, K-Area, Sharif Colony, Metroville 2, Goth Lalabad, Sharafi Goth,  Bhutto Nagar, Awami Colony,  Burmi Colony, Sherabad,  Landhi Town, Karachi',getdate(),getdate(),1,1,1 union
select 'Labour Colony, Majeed Colony, New Muzaffarabad Colony, Bakhtawar Goth, Sherpao Colony,  Muslimabad,  Landhi Town, Karachi',getdate(),getdate(),1,1,1 union
select 'Rehri Goth, Shahbaz Colony, Goth Nek Mohammad,  Landhi Town, Karachi',getdate(),getdate(),1,1,1 union
select 'Mansehra Colony,  Muzaffarabad Colony,  Daud Colony,  Landhi Town, Karachi',getdate(),getdate(),1,1,1 union
select 'Green Town, Golden Town,  Shah Faisal Town, Karachi',getdate(),getdate(),1,1,1 union
select ' Natha Khan Goth, PIA Staff Colony, SFC Block 5, Al Flah Society, Millat Town, Gulfishan Society, Pak Saadat Colony,  Al Falah Society,  Shah Faisal Town, Karachi',getdate(),getdate(),1,1,1 union
select 'Shamsi Society, Hinsabad, Goth Juma, Salman Farsi Town, Azeem Pura, Morio Khan Goth,  Drigh Colony,  Rifah Aam Society,  Shah Faisal Town, Karachi',getdate(),getdate(),1,1,1 union
select ' Reta Plot,  Shah Faisal Town, Karachi',getdate(),getdate(),1,1,1 union
select 'F.B. Area, Gulberg Town, Karachi',getdate(),getdate(),1,1,1 union
select 'Nasirabad 14, Gulberg Town, Karachi',getdate(),getdate(),1,1,1 union
select 'Karimabad 2+3, Gulberg Town, Karachi',getdate(),getdate(),1,1,1 union
select 'Azizabad, Gulberg Town, Karachi',getdate(),getdate(),1,1,1 union
select 'Yaseenabad 9, Gulberg Town, Karachi',getdate(),getdate(),1,1,1 union
select 'Ancholi 17+20, Gulberg Town, Karachi',getdate(),getdate(),1,1,1 union
select 'Gulberg 12+13, Gulberg Town, Karachi',getdate(),getdate(),1,1,1 union
select 'Samanabad 18+19, Gulberg Town, Karachi',getdate(),getdate(),1,1,1 union
select 'Shafeeque Mill Colony 22, Gulberg Town, Karachi',getdate(),getdate(),1,1,1 union
select 'Musa Colony, Gulberg Town, Karachi',getdate(),getdate(),1,1,1 union
select 'Nazimabad, Liaquatabad Town, Karachi',getdate(),getdate(),1,1,1 union
select 'Gulbahar, C Area, C-1 Area, E Area, Qasimabad, Liaquatabad Town, Karachi',getdate(),getdate(),1,1,1 union
select 'Rizvia Society, Liaquatabad Town, Karachi',getdate(),getdate(),1,1,1 union
select 'Punjab Colony, Liaquatabad Town, Karachi',getdate(),getdate(),1,1,1 union
select 'Jahangirabad, Liaquatabad Town, Karachi',getdate(),getdate(),1,1,1 union
select 'Hussainabad, Sharifabad, Liaquatabad Town, Karachi',getdate(),getdate(),1,1,1 union
select 'Jamhuria colony, Nital Colony, F.C. Area, Siraj Colony, Jalalabad, Farooqabad, B-1 Area, Firdous Colony, Super Market, Liaquatabad Town, Karachi',getdate(),getdate(),1,1,1 union
select 'Dak Khana, Liaquatabad Town, Karachi',getdate(),getdate(),1,1,1 union
select 'Bandhani Colony, Bandhani Colony, Liaquatabad Town, Karachi',getdate(),getdate(),1,1,1 union
select 'Gharibabad, Liaquatabad Town, Karachi',getdate(),getdate(),1,1,1 union
select 'Musarrat Colony, Sikandarabad, Siddiqabad, Mujahid Colony, Liaquatabad Town, Karachi',getdate(),getdate(),1,1,1 union
select 'North Karachi, North Karachi Town, Karachi',getdate(),getdate(),1,1,1 union
select 'North Nazimabad, North Nazimabad Town, Karachi',getdate(),getdate(),1,1,1 union
select 'Shadman Town, North Nazimabad Town, Karachi',getdate(),getdate(),1,1,1 union
select 'Paposh Nagar, North Nazimabad Town, Karachi',getdate(),getdate(),1,1,1 union
select 'Nusrat Bhutto Colony, North Nazimabad Town, Karachi',getdate(),getdate(),1,1,1 union
select 'Qayam Khani Colony, Gulshan-E-Ghazi, Ittehad Town, Islam Nagar, Swat Colony, Muslim Mujahid Colony, Muhajir Camp, Dehli Macca Colony, Rasheedabad, Baldia Town, Karachi',getdate(),getdate(),1,1,1 union
select 'Mohammad Khan Colony, Baldia Town, Karachi',getdate(),getdate(),1,1,1 union
select 'Dawood Goth, Sajjan Colony, Faqir Colony, Qaddafi Colony, Nai Abadi, Saeedabad, Kumhar Wara, Baldia Town, Karachi',getdate(),getdate(),1,1,1 union
select 'Saadullah Goth, Baldia Town, Karachi',getdate(),getdate(),1,1,1 union
select 'West Wharf, Kiamari Town, Karachi',getdate(),getdate(),1,1,1 union
select 'East Wharf, Kiamari Town, Karachi',getdate(),getdate(),1,1,1 union
select 'Bhutta Village, Kiamari Town, Karachi',getdate(),getdate(),1,1,1 union
select 'Kiamari Town, Kiamari Town, Karachi',getdate(),getdate(),1,1,1 union
select 'Sultanabad, Ghulam Colony, Kiamari Town, Karachi',getdate(),getdate(),1,1,1 union
select 'Goth Raees Ilyas, Kiamari Town, Karachi',getdate(),getdate(),1,1,1 union
select 'Moach, Paracha Labour Colony, Kiamari Town, Karachi',getdate(),getdate(),1,1,1 union
select 'Shershah Colony, Kiamari Town, Karachi',getdate(),getdate(),1,1,1 union
select 'Kaka Pir, Machhar Colony, Kiamari Town, Karachi',getdate(),getdate(),1,1,1 union
select 'Maripur Colony, Gondapas, Goth Ahmed Khan, Kiamari Town, Karachi',getdate(),getdate(),1,1,1 union
select 'Mochko, Moach Goth, Kiamari Town, Karachi',getdate(),getdate(),1,1,1 union
select 'Baba Bhit, Kiamari Town, Karachi',getdate(),getdate(),1,1,1 union
select 'Sumar Goth, Kachela Goth, Kiamari Town, Karachi',getdate(),getdate(),1,1,1 union
select 'Goth Wiro, Gabo Pat, Kiamari Town, Karachi',getdate(),getdate(),1,1,1 union
select 'Raees Amrohvi Colony, Aligarh Colony, Orangi Town, Karachi',getdate(),getdate(),1,1,1 union
select 'Yaqoobabad, Tori Bangash Colony, Al Khizra Society, Sir Syed Colony, Ali Nagar, Bilal Colony, Gulzar-e-Habib Colony, Gabol Colony, Fareed Colony, Hazarvi Colony, Bilal Colony, Gabol Town, Data Nagar, Orangi Town, Karachi',getdate(),getdate(),1,1,1 union
select 'Baloch Goth, Orangi Town, Karachi',getdate(),getdate(),1,1,1 union
select 'Gulshan-e-Zia, Mansoor Nagar, Gulshan-e-Bihar, Shah Waliullah Nagar, Abuzar Ghaffari Colony, Ittehad Colony, Raja Tanveer Colony, Muslim Nagar, Shamsi Colony, Gulzar-e-Mohammad Colony, Salimabad, New Islam Nagar, Imam Colony, Gharib Nawaz Colony, Mujahidabad, Aziz Nagar, Baloch Para, Mohammad Farooq Colony, Zia ul Haq Colony, Mohammad Pur, Fateh Colony, Ghosia Colony, Elahiabad, Hanifabad, Usmania Colony, Khyber Colony, Bismillah Colony, Faqirabad, Azad Nagar, Zia Colony, Mominabad, Haryana Colony, Hanifabad, Madina Colony, Ghaziabad, Chishti Nagar, Iqbal Baloch Colony, Mujahidabad, Orangi Town, Karachi',getdate(),getdate(),1,1,1 union
select 'Bewa Quarters, Tauheed Colony, Mohammad Nagar, Afridi Colony, Orangi Town, Karachi',getdate(),getdate(),1,1,1 union
select ', Site Town, Karachi',getdate(),getdate(),1,1,1 union
select 'Hasrat Mohani Colony, Site Town, Karachi',getdate(),getdate(),1,1,1 union
select 'Old Golimar, Site Town, Karachi',getdate(),getdate(),1,1,1 union
select 'Haroonanad, Jahanabad, Site Town, Karachi',getdate(),getdate(),1,1,1 union
select 'Pak Colony, Madina Basti, Metroville, Site Town, Karachi',getdate(),getdate(),1,1,1 union
select 'Banaras Colony, Qasba Colony, Islamia Colony, Site Town, Karachi',getdate(),getdate(),1,1,1 union
select 'Pathan Colony, Asif Colony, Shah Wali Colony, Frontier Colony, Site Town, Karachi',getdate(),getdate(),1,1,1 union
select 'Muslim Town, Elahi Colony, Labour Colony, Ali Village, Mian Wali Colony, Site Town, Karachi',getdate(),getdate(),1,1,1 union
select 'Gul Bai, Site Town, Karachi',getdate(),getdate(),1,1,1 union
select 'Steel Mills, Bin Qaism Town, Karachi',getdate(),getdate(),1,1,1 union
select 'Steel Town, Bin Qaism Town, Karachi',getdate(),getdate(),1,1,1 union
select 'Port Qasim Staff Colony, Shah Latif Town, Bin Qaism Town, Karachi',getdate(),getdate(),1,1,1 union
select 'Radio Pakistan Colony, Bin Qaism Town, Karachi',getdate(),getdate(),1,1,1 union
select 'Gulshan-e-Hadeed, Bin Qaism Town, Karachi',getdate(),getdate(),1,1,1 union
select 'Yousuf Goth, Goth Jumman, Goth Musa Jokhio, Goth Lashari, Goth Razzaqabad, Mehmood Kalimi Goth, Mohammadi Colony, Goth Rehri, Ibraheem Hyderi, Haji Daud Goth, Chishma Goth, Jumma Goth, Arkanabad, Umer Goth, Bin Qaism Town, Karachi',getdate(),getdate(),1,1,1 union
select 'Master Goth, Bin Qaism Town, Karachi',getdate(),getdate(),1,1,1 union
select 'Quaidabad, Bin Qaism Town, Karachi',getdate(),getdate(),1,1,1 union
select 'Gulshan-e-Maymar, Gadap Town, Karachi',getdate(),getdate(),1,1,1 union
select 'Surjani Town, Gadap Town, Karachi',getdate(),getdate(),1,1,1 union
select 'Hamza Society, Gadap Town, Karachi',getdate(),getdate(),1,1,1 union
select 'Ishrat Town, Gadap Town, Karachi',getdate(),getdate(),1,1,1 union
select 'Abdullah Nagar, Gulshan-e-Shiraz, Darakhshan Society, Gadap Town, Karachi',getdate(),getdate(),1,1,1 union
select 'Ahsanabad, Gadap Town, Karachi',getdate(),getdate(),1,1,1 union
select 'High Court Employees Housing Society, Azimabad, Gadap Town, Karachi',getdate(),getdate(),1,1,1 union
select 'Khuda Ki Basti, Al Asif Square, Goth Abdullah, Gadap Town, Karachi',getdate(),getdate(),1,1,1 union
select 'Al Azam Town, Gadap Town, Karachi',getdate(),getdate(),1,1,1 union
select 'Kazimabad, Faizabad Society, Soorti Society, Gulshan-e-Amna, Darakhshan Society, Shadman Town, Khurshid Begum Town, Madina Colony, Malir Town, Karachi',getdate(),getdate(),1,1,1 union
select 'Moinabad, Nishtarabad, Model Colony, Kala Board, Musarrat Colony, Pak Kausar Town, Kausar Town, Saudabad, Khokarapar, Gilanabad, Muzaffarabad, Nazir Town, Mehran Town, Qasimabad, Goth Khaskheli, Goth Bachal, Rahim Khan Goth, Lilly Town, Mohammadi Colony, Aasu Goth, Gulshan-e-Qadri, Sukhio Goth, Jafar-e-Tayyar Society, Ghulam Mohammad Goth, Gharibabad Goth, Goth Rasool Baksh Jokhio, Ghazi Brohi Goth, Malir Town, Karachi',getdate(),getdate(),1,1,1 union
select 'D.H.A., Clifton Cantonment, Karachi',getdate(),getdate(),1,1,1 union
select ', Clifton Cantonment, Karachi',getdate(),getdate(),1,1,1 union
select ', Clifton Cantonment, Karachi',getdate(),getdate(),1,1,1 union
select 'Khadda Market, Clifton Cantonment, Karachi',getdate(),getdate(),1,1,1 union
select 'Gizri Colony, Clifton Cantonment, Karachi',getdate(),getdate(),1,1,1 union
select 'Askari 3, Clifton Cantonment, Karachi',getdate(),getdate(),1,1,1 union
select 'Railway Colony, Clifton Cantonment, Karachi',getdate(),getdate(),1,1,1 union
select 'Punjab Colony, Clifton Cantonment, Karachi',getdate(),getdate(),1,1,1 union
select 'Saddar, Clifton Cantonment, Karachi',getdate(),getdate(),1,1,1 union
select 'Dehli Colony, Clifton Cantonment, Karachi',getdate(),getdate(),1,1,1 union
select 'P&T Colony, Clifton Cantonment, Karachi',getdate(),getdate(),1,1,1 union
select 'Shah Rasool Colony, Clifton Cantonment, Karachi',getdate(),getdate(),1,1,1 union
select 'Gulistan-e-Johar, Faisal Contonment, Karachi',getdate(),getdate(),1,1,1 union
select ', Faisal Contonment, Karachi',getdate(),getdate(),1,1,1 union
select 'Askari 4, Faisal Contonment, Karachi',getdate(),getdate(),1,1,1 union
select 'Gulshan-e-Jamal, Faisal Contonment, Karachi',getdate(),getdate(),1,1,1 union
select 'Dada Bhai Colony, Faisal Contonment, Karachi',getdate(),getdate(),1,1,1 union
select 'Defence Officer Housing Society, Malir Contonment, Karachi',getdate(),getdate(),1,1,1 union
select 'Sadi Town, Islamia Town, Jinnah Terminal, Malir Contonment, Karachi',getdate(),getdate(),1,1,1 union
select 'Gulshan-e-Mehran, Gulshan-e-Rahim, Malir Contonment, Karachi',getdate(),getdate(),1,1,1 union
select 'Global City, Malir Contonment, Karachi',getdate(),getdate(),1,1,1 union
select 'Goth Haji Soomaro, Hashimabad Society, Malir Contonment, Karachi',getdate(),getdate(),1,1,1 union
select 'Islamabad',getdate(),getdate(),1,1,1 union
select 'Sector-F, Islamabad',getdate(),getdate(),1,1,1 union
select 'Sector-E, Islamabad',getdate(),getdate(),1,1,1 union
select 'Sector-G, Islamabad',getdate(),getdate(),1,1,1 union
select 'Sector-I, Islamabad',getdate(),getdate(),1,1,1 union
select 'Sector-H, Islamabad',getdate(),getdate(),1,1,1 union
select 'SOAN GARDEN, Islamabad',getdate(),getdate(),1,1,1 union
select 'DHA, Islamabad',getdate(),getdate(),1,1,1 union
select 'DIPLOMATIC ENCLAVE, Islamabad',getdate(),getdate(),1,1,1 union
select 'BAHRIA, Islamabad',getdate(),getdate(),1,1,1 union
select 'PWD COLONY, JINNAH GARDEN, NAVAL ANCHORAGE, BANI GALA, CBR, Islamabad',getdate(),getdate(),1,1,1 union
select 'RIVER GARDEN, Islamabad',getdate(),getdate(),1,1,1 union
select 'BURMA TOWN, RAWAL TOWN, CHAK SHAHZAD TOWN, Islamabad',getdate(),getdate(),1,1,1 union
select 'GHORI TOWN, MEDIA TOWN, POLICE LANE, DOCTORS TOWN, Islamabad',getdate(),getdate(),1,1,1 union
select 'TARLAI, MARGALLA TWON, THANDA PANI, SULTANA FOUNDATION, Islamabad',getdate(),getdate(),1,1,1 union
select 'KORANG TOWN, PAKISTAN TOWN, Islamabad',getdate(),getdate(),1,1,1 union
select 'MAL PUR, Islamabad',getdate(),getdate(),1,1,1 union
select 'RAWAT TOWN, Islamabad',getdate(),getdate(),1,1,1 union
select 'LOHI BHER, Islamabad',getdate(),getdate(),1,1,1 union
select 'SIHALA, Islamabad',getdate(),getdate(),1,1,1 union
select 'DHOK SOHAN, FARASH TWON, ALI PUR FRASH TWON, CHATTA BAKHTAWAR, ZIA MASJID EAST, JILANIA, TARAMARI, Kachha Stop, Islamabad',getdate(),getdate(),1,1,1 union
select 'IMAM BARI, ATTOMIC ENERGY COMMISSION, NILOR, KHANNA EAST, GOLRA TOWN, Islamabad',getdate(),getdate(),1,1,1 union
select 'BARA KAHU, Islamabad',getdate(),getdate(),1,1,1 union
select 'KRL, Islamabad',getdate(),getdate(),1,1,1 union
select '17 MEEL, Islamabad',getdate(),getdate(),1,1,1 union
select 'SHAH ALLAH DITTA, Habib Colony, Mohalla Loharan, Islamabad',getdate(),getdate(),1,1,1 union
select 'Lahore',getdate(),getdate(),1,1,1 union
select 'Dubban Pura, Kot Kamboh, IQBAL TOWN, Lahore',getdate(),getdate(),1,1,1 union
select 'Johar Town Phase I, Johar Town Phase II, IQBAL TOWN, Lahore',getdate(),getdate(),1,1,1 union
select 'Judicial Colony, IQBAL TOWN, Lahore',getdate(),getdate(),1,1,1 union
select 'Thokar Niaz Baig, IQBAL TOWN, Lahore',getdate(),getdate(),1,1,1 union
select 'PECO Factory, IQBAL TOWN, Lahore',getdate(),getdate(),1,1,1 union
select 'NESPAK, IQBAL TOWN, Lahore',getdate(),getdate(),1,1,1 union
select 'Ali Town, IQBAL TOWN, Lahore',getdate(),getdate(),1,1,1 union
select 'West Wood Colony, IQBAL TOWN, Lahore',getdate(),getdate(),1,1,1 union
select 'Central park PCSIR Phase 2, IQBAL TOWN, Lahore',getdate(),getdate(),1,1,1 union
select 'Wapda Town, IQBAL TOWN, Lahore',getdate(),getdate(),1,1,1 union
select 'Sabzazar Scheme, IQBAL TOWN, Lahore',getdate(),getdate(),1,1,1 union
select 'Thokar Chowk, IQBAL TOWN, Lahore',getdate(),getdate(),1,1,1 union
select 'GECH, IQBAL TOWN, Lahore',getdate(),getdate(),1,1,1 union
select 'Nawab Town, Ittefaq Town, IQBAL TOWN, Lahore',getdate(),getdate(),1,1,1 union
select 'Town Ship, Sector A2, IQBAL TOWN, Lahore',getdate(),getdate(),1,1,1 union
select 'Khumbe, IQBAL TOWN, Lahore',getdate(),getdate(),1,1,1 union
select 'Town Ship Industrial Estate, Town Ship Sector A1, IQBAL TOWN, Lahore',getdate(),getdate(),1,1,1 union
select 'Wafaqi Colony, IQBAL TOWN, Lahore',getdate(),getdate(),1,1,1 union
select 'B.O.R Society, IQBAL TOWN, Lahore',getdate(),getdate(),1,1,1 union
select 'Awan Town, IQBAL TOWN, Lahore',getdate(),getdate(),1,1,1 union
select 'Sunfort gardens, IQBAL TOWN, Lahore',getdate(),getdate(),1,1,1 union
select 'Hanjarwal, IQBAL TOWN, Lahore',getdate(),getdate(),1,1,1 union
select 'Satto Katla, IQBAL TOWN, Lahore',getdate(),getdate(),1,1,1 union
select 'Taragarh, RAVI TOWN, Lahore',getdate(),getdate(),1,1,1 union
select 'Badami Bagh, RAVI TOWN, Lahore',getdate(),getdate(),1,1,1 union
select 'Shahdara, RAVI TOWN, Lahore',getdate(),getdate(),1,1,1 union
select 'Walled City, RAVI TOWN, Lahore',getdate(),getdate(),1,1,1 union
select 'Qazi Park, Bhamma, Jhuggian Jodha, RAVI TOWN, Lahore',getdate(),getdate(),1,1,1 union
select 'Naulkha, RAVI TOWN, Lahore',getdate(),getdate(),1,1,1 union
select 'Data Nagar, RAVI TOWN, Lahore',getdate(),getdate(),1,1,1 union
select 'Ravi Park, Sadaat Colony, Hanif Park, RAVI TOWN, Lahore',getdate(),getdate(),1,1,1 union
select 'Garhi Shahu, SHALIMAR TOWN, Lahore',getdate(),getdate(),1,1,1 union
select 'Madni Park, SHALIMAR TOWN, Lahore',getdate(),getdate(),1,1,1 union
select 'Singhpura, SHALIMAR TOWN, Lahore',getdate(),getdate(),1,1,1 union
select 'Baghban Pura, SHALIMAR TOWN, Lahore',getdate(),getdate(),1,1,1 union
select 'Misri Shah, Bhagat Pura, Kot Khawaja Saeed, Makhan Pura, SHALIMAR TOWN, Lahore',getdate(),getdate(),1,1,1 union
select 'Chah Miran, Begum Pura, SHALIMAR TOWN, Lahore',getdate(),getdate(),1,1,1 union
select 'Gawal Mandi, DATA GUNJ BAKHSH TOWN, Lahore',getdate(),getdate(),1,1,1 union
select 'Rizwan Garden, DATA GUNJ BAKHSH TOWN, Lahore',getdate(),getdate(),1,1,1 union
select 'Mozang, DATA GUNJ BAKHSH TOWN, Lahore',getdate(),getdate(),1,1,1 union
select 'Shah Jamal, DATA GUNJ BAKHSH TOWN, Lahore',getdate(),getdate(),1,1,1 union
select 'GOR-I, DATA GUNJ BAKHSH TOWN, Lahore',getdate(),getdate(),1,1,1 union
select 'GOR-III, DATA GUNJ BAKHSH TOWN, Lahore',getdate(),getdate(),1,1,1 union
select 'Sanda, DATA GUNJ BAKHSH TOWN, Lahore',getdate(),getdate(),1,1,1 union
select 'Raj Garh, DATA GUNJ BAKHSH TOWN, Lahore',getdate(),getdate(),1,1,1 union
select 'Shadman-I, Shadman-II, DATA GUNJ BAKHSH TOWN, Lahore',getdate(),getdate(),1,1,1 union
select 'Mayo Garden, DATA GUNJ BAKHSH TOWN, Lahore',getdate(),getdate(),1,1,1 union
select 'Bilal Gunj, DATA GUNJ BAKHSH TOWN, Lahore',getdate(),getdate(),1,1,1 union
select 'Old Anarkali, DATA GUNJ BAKHSH TOWN, Lahore',getdate(),getdate(),1,1,1 union
select 'Qila Gujjar Singh, DATA GUNJ BAKHSH TOWN, Lahore',getdate(),getdate(),1,1,1 union
select 'Birdwood Barracks, DATA GUNJ BAKHSH TOWN, Lahore',getdate(),getdate(),1,1,1 union
select 'Ahatta Mol Chand, DATA GUNJ BAKHSH TOWN, Lahore',getdate(),getdate(),1,1,1 union
select 'Krishan Nagar, Sant Nagar, DATA GUNJ BAKHSH TOWN, Lahore',getdate(),getdate(),1,1,1 union
select 'Ameen Park, Karim Park, DATA GUNJ BAKHSH TOWN, Lahore',getdate(),getdate(),1,1,1 union
select 'Muhammad Nagar, DATA GUNJ BAKHSH TOWN, Lahore',getdate(),getdate(),1,1,1 union
select 'Alama Iqbal Town, SAMANABAD TOWN, Lahore',getdate(),getdate(),1,1,1 union
select 'Ichhar, SAMANABAD TOWN, Lahore',getdate(),getdate(),1,1,1 union
select 'Canal View, TECH Society, SAMANABAD TOWN, Lahore',getdate(),getdate(),1,1,1 union
select 'Jinnah Block, SAMANABAD TOWN, Lahore',getdate(),getdate(),1,1,1 union
select 'Rehman Pura, SAMANABAD TOWN, Lahore',getdate(),getdate(),1,1,1 union
select 'Wahdat Colony, SAMANABAD TOWN, Lahore',getdate(),getdate(),1,1,1 union
select 'Mustafa Town, SAMANABAD TOWN, Lahore',getdate(),getdate(),1,1,1 union
select 'Karim Block, SAMANABAD TOWN, Lahore',getdate(),getdate(),1,1,1 union
select 'Samanabad, SAMANABAD TOWN, Lahore',getdate(),getdate(),1,1,1 union
select 'Kamran Block, SAMANABAD TOWN, Lahore',getdate(),getdate(),1,1,1 union
select 'Nishtar Block, Umar Block, Nizam Block, SAMANABAD TOWN, Lahore',getdate(),getdate(),1,1,1 union
select 'Pakki Thathi, SAMANABAD TOWN, Lahore',getdate(),getdate(),1,1,1 union
select 'Sodhiwal, SAMANABAD TOWN, Lahore',getdate(),getdate(),1,1,1 union
select 'Gulshan-e-Ravi, SAMANABAD TOWN, Lahore',getdate(),getdate(),1,1,1 union
select 'Raza Block, SAMANABAD TOWN, Lahore',getdate(),getdate(),1,1,1 union
select 'Babu Sabu, SAMANABAD TOWN, Lahore',getdate(),getdate(),1,1,1 union
select 'New Mozang, SAMANABAD TOWN, Lahore',getdate(),getdate(),1,1,1 union
select 'Garden Town, Christian Colony, GULBERG TOWN, Lahore',getdate(),getdate(),1,1,1 union
select 'Faisal Town, GULBERG TOWN, Lahore',getdate(),getdate(),1,1,1 union
select 'Falcon Complex, GULBERG TOWN, Lahore',getdate(),getdate(),1,1,1 union
select 'Askari V, GULBERG TOWN, Lahore',getdate(),getdate(),1,1,1 union
select 'GECH Phase II, PCSIR, GULBERG TOWN, Lahore',getdate(),getdate(),1,1,1 union
select 'Gopal Nagar, GULBERG TOWN, Lahore',getdate(),getdate(),1,1,1 union
select 'Sabzi Mandi, GULBERG TOWN, Lahore',getdate(),getdate(),1,1,1 union
select 'Naseerabad, GULBERG TOWN, Lahore',getdate(),getdate(),1,1,1 union
select 'Kot Lakhpat, GULBERG TOWN, Lahore',getdate(),getdate(),1,1,1 union
select 'Bhabra, GULBERG TOWN, Lahore',getdate(),getdate(),1,1,1 union
select 'Bhatti Colony, GULBERG TOWN, Lahore',getdate(),getdate(),1,1,1 union
select 'Mochi Pura, GULBERG TOWN, Lahore',getdate(),getdate(),1,1,1 union
select 'Dilkusha, GULBERG TOWN, Lahore',getdate(),getdate(),1,1,1 union
select 'Garden Town, GULBERG TOWN, Lahore',getdate(),getdate(),1,1,1 union
select 'Gulberg, GULBERG TOWN, Lahore',getdate(),getdate(),1,1,1 union
select ', GULBERG TOWN, Lahore',getdate(),getdate(),1,1,1 union
select 'Model Town, GULBERG TOWN, Lahore',getdate(),getdate(),1,1,1 union
select 'Taj Bagh Scheme, AZIZ BHATTI TOWN, Lahore',getdate(),getdate(),1,1,1 union
select 'Mustafa Abad, AZIZ BHATTI TOWN, Lahore',getdate(),getdate(),1,1,1 union
select 'Mughalpura, AZIZ BHATTI TOWN, Lahore',getdate(),getdate(),1,1,1 union
select 'Tajpura, AZIZ BHATTI TOWN, Lahore',getdate(),getdate(),1,1,1 union
select 'Harbanspura, AZIZ BHATTI TOWN, Lahore',getdate(),getdate(),1,1,1 union
select 'Ghaziabad, AZIZ BHATTI TOWN, Lahore',getdate(),getdate(),1,1,1 union
select 'Fateh Garh, Haji Pura, Hameed Pura, AZIZ BHATTI TOWN, Lahore',getdate(),getdate(),1,1,1 union
select 'Jalal Colony, AZIZ BHATTI TOWN, Lahore',getdate(),getdate(),1,1,1 union
select 'Gulistan Colony, Gulshan Colony, Theat, NISHTAR TOWN, Lahore',getdate(),getdate(),1,1,1 union
select 'Gawala Colony, NISHTAR TOWN, Lahore',getdate(),getdate(),1,1,1 union
select 'NFC Housing Society, NISHTAR TOWN, Lahore',getdate(),getdate(),1,1,1 union
select 'DHA Rahbar, NISHTAR TOWN, Lahore',getdate(),getdate(),1,1,1 union
select 'Engineer''s Town, UET Housing Society, NISHTAR TOWN, Lahore',getdate(),getdate(),1,1,1 union
select 'Nasheman-e-Iqbal Housing Society, NISHTAR TOWN, Lahore',getdate(),getdate(),1,1,1 union
select 'Valencia, NISHTAR TOWN, Lahore',getdate(),getdate(),1,1,1 union
select 'Pak-Arab Housing Scheme, NISHTAR TOWN, Lahore',getdate(),getdate(),1,1,1 union
select 'Wapda Colony, NISHTAR TOWN, Lahore',getdate(),getdate(),1,1,1 union
select 'Audit and Accounts Housing, Green Acres, T & T Aabpara Housing Society, NISHTAR TOWN, Lahore',getdate(),getdate(),1,1,1 union
select 'Naz Town, NISHTAR TOWN, Lahore',getdate(),getdate(),1,1,1 union
select 'Javed Colony, NISHTAR TOWN, Lahore',getdate(),getdate(),1,1,1 union
select 'Kamahan, Nishtar Colony, Sitara Colony, NISHTAR TOWN, Lahore',getdate(),getdate(),1,1,1 union
select 'Sadhoke, Green Town, Ameerpur, NISHTAR TOWN, Lahore',getdate(),getdate(),1,1,1 union
select 'Aashiana Housing Scheme, NISHTAR TOWN, Lahore',getdate(),getdate(),1,1,1 union
select 'Gujjar Colony, NISHTAR TOWN, Lahore',getdate(),getdate(),1,1,1 union
select 'Muftpura, NISHTAR TOWN, Lahore',getdate(),getdate(),1,1,1 union
select 'Tariq Gardens, Chandri, NISHTAR TOWN, Lahore',getdate(),getdate(),1,1,1 union
select 'Bagharian, NISHTAR TOWN, Lahore',getdate(),getdate(),1,1,1 union
select 'Atari Saroba, NISHTAR TOWN, Lahore',getdate(),getdate(),1,1,1 union
select 'D.H.A, WAHGA TOWN, Lahore',getdate(),getdate(),1,1,1 union
select 'Askari XI, WAHGA TOWN, Lahore',getdate(),getdate(),1,1,1 union
select 'State Life Housing Society, Sui Gas Society, WAHGA TOWN, Lahore',getdate(),getdate(),1,1,1 union
select 'Walton Cantt., WAHGA TOWN, Lahore',getdate(),getdate(),1,1,1 union
select 'Nishat Colony, WAHGA TOWN, Lahore',getdate(),getdate(),1,1,1 union
select 'DHA Phase IV, DHA Phase III, DHA Phase V, DHA Phase I, DHA Phase II, WAHGA TOWN, Lahore',getdate(),getdate(),1,1,1 union
select 'Qainchi, WAHGA TOWN, Lahore',getdate(),getdate(),1,1,1 union
select 'Qadri Colony, WAHGA TOWN, Lahore',getdate(),getdate(),1,1,1 union
select 'Punjab Society, WAHGA TOWN, Lahore',getdate(),getdate(),1,1,1 union
select 'Charrar, WAHGA TOWN, Lahore',getdate(),getdate(),1,1,1 union
select 'Gulshan Ali Colony, WAHGA TOWN, Lahore',getdate(),getdate(),1,1,1 union
select 'Fort Villa, Bank Officers Colony, WAHGA TOWN, Lahore',getdate(),getdate(),1,1,1 union
select 'Noor Colony, Shadipura, WAHGA TOWN, Lahore',getdate(),getdate(),1,1,1 union
select 'Salamat Pura, WAHGA TOWN, Lahore',getdate(),getdate(),1,1,1 union
select 'PEL Factory, WAHGA TOWN, Lahore',getdate(),getdate(),1,1,1 union
select 'Mushtaq Colony, WAHGA TOWN, Lahore',getdate(),getdate(),1,1,1 union
select 'Baowala, WAHGA TOWN, Lahore',getdate(),getdate(),1,1,1 union
select 'Nawaz Shariff Colony, WAHGA TOWN, Lahore',getdate(),getdate(),1,1,1 union
select 'Amar Sidhu, WAHGA TOWN, Lahore',getdate(),getdate(),1,1,1 union
select 'Gohawa, Khuda Bux Colony, WAHGA TOWN, Lahore',getdate(),getdate(),1,1,1 union
select 'Darogha Wala, Mominpura, WAHGA TOWN, Lahore',getdate(),getdate(),1,1,1 union
select 'Chung Khurad, WAHGA TOWN, Lahore',getdate(),getdate(),1,1,1 union
select 'Lakhudher, WAHGA TOWN, Lahore',getdate(),getdate(),1,1,1 union
select 'Keer, WAHGA TOWN, Lahore',getdate(),getdate(),1,1,1 union
select 'Askari Flats, Askari I, Lahore Cantonment, Lahore',getdate(),getdate(),1,1,1 union
select 'Askari X, Lahore Cantonment, Lahore',getdate(),getdate(),1,1,1 union
select 'Cavalry Ground, Lahore Cantonment, Lahore',getdate(),getdate(),1,1,1 union
select 'PAF Colony, Lahore Cantonment, Lahore',getdate(),getdate(),1,1,1 union
select 'Bridge Colony, CMA Colony, Lahore Cantonment, Lahore',getdate(),getdate(),1,1,1 union
select 'Park View Housing, Lahore Cantonment, Lahore',getdate(),getdate(),1,1,1 union
select 'Askari Villas, Lahore Cantonment, Lahore',getdate(),getdate(),1,1,1 union
select 'Divine Gardens, Lahore Cantonment, Lahore',getdate(),getdate(),1,1,1 union
select 'Nisar Colony, Lahore Cantonment, Lahore',getdate(),getdate(),1,1,1 union
select 'Qasim Pura, Lahore Cantonment, Lahore',getdate(),getdate(),1,1,1 union
select 'Nadirabad, Lahore Cantonment, Lahore',getdate(),getdate(),1,1,1 union
select 'Eden Avrnue, Lahore Cantonment, Lahore',getdate(),getdate(),1,1,1 union
select 'Lal Kurti, Lahore Cantonment, Lahore',getdate(),getdate(),1,1,1 union
select 'PAF Officers Colony, Lahore Cantonment, Lahore',getdate(),getdate(),1,1,1 union
select 'New Officers Colony, Old Officers Colony, Lahore Cantonment, Lahore',getdate(),getdate(),1,1,1 union
select 'Rawalpindi',getdate(),getdate(),1,1,1 union
select 'Bahria Town, Rawalpindi',getdate(),getdate(),1,1,1 union
select 'DHA Phase 1, Executive Lodges, Rawalpindi',getdate(),getdate(),1,1,1 union
select 'Khayaban-e-Faisal, Rawalpindi',getdate(),getdate(),1,1,1 union
select 'Satellite Town, Rawalpindi',getdate(),getdate(),1,1,1 union
select 'Police Foundation, Rawalpindi',getdate(),getdate(),1,1,1 union
select 'Askari VIII, Rawalpindi',getdate(),getdate(),1,1,1 union
select 'Askari VII, Safari Villas, Askari XI, New Gulzar-e-Quaid Housing Colony, Rawalpindi',getdate(),getdate(),1,1,1 union
select 'Residential & Commercial Area of Khayaban e Sir Syed, Afandi Colony, Rawalpindi',getdate(),getdate(),1,1,1 union
select 'Naseerabad, Rawalpindi',getdate(),getdate(),1,1,1 union
select 'Gulrez, Rawalpindi',getdate(),getdate(),1,1,1 union
select 'PIA Colony, Airport Housing Society, Morgah, Rawalpindi',getdate(),getdate(),1,1,1 union
select 'Jhanda Chichi, Marir Hassan, Civil Lines, Rawalpindi',getdate(),getdate(),1,1,1 union
select 'COBB Line, Rawalpindi',getdate(),getdate(),1,1,1 union
select 'Shah Khalid Colony, Chah Sultan, Tali Mori, Bilal Colony, Misrial Road, Ilyas Colony, Dhoak Elahi Bakhsh, Pindora Chungi, Mohan Pura, Allahbad, Sher Zaman Colony, Kalyal Sharief, Amer Pura Mohalla, Dhoke Raja Muhammad Khan, Fazal Town Phase II, Fazal Town Phase I, Chaklala Railway Scheme I, Nawaz Colony, Kotha Kalan, Jamhara Nai Abadi, Army Officer Colony, ARL Colony, Rawalpindi',getdate(),getdate(),1,1,1 union
select 'Chour Harpal, Rawalpindi',getdate(),getdate(),1,1,1 union
select 'Media Town, Rawalpindi',getdate(),getdate(),1,1,1 union
select 'Raja Bazar, Dhoak Ratta, Dhoak Ali Akbar, Khokar Market, Professor Colony, Jehandad Town, Nussah Town, Shaheen Phase II, Azeem Town, Sangar Town, Rawalpindi',getdate(),getdate(),1,1,1 union
select 'Sadiqabad, Rawalpindi',getdate(),getdate(),1,1,1 union
select 'Muslim Town, Khurrum Colony, Rawalpindi',getdate(),getdate(),1,1,1 union
select 'Naya Muhalla, Nanak Pura, Koh-i-Noor Colony, Chistianabad, Ratta Amral, Rawalpindi',getdate(),getdate(),1,1,1 union
select 'Magistrate Colony, Rawalpindi',getdate(),getdate(),1,1,1 union
select 'Dhoke Munshi Khan, Rawalpindi',getdate(),getdate(),1,1,1 union
select 'Chamanabad, Javed Sultan Shaheed Flats, Hazara Colony, Azizabad, Rawalpindi',getdate(),getdate(),1,1,1 union
select 'Waris Khan, Rawalpindi',getdate(),getdate(),1,1,1 union
select 'Colonel Yousaf Colony, Behari Colony, Muhammadi Colony, Rawalpindi',getdate(),getdate(),1,1,1 union
select 'Ganj Mandi, New katarian, Mehmood Abad, Ameen Town, Rawalpindi',getdate(),getdate(),1,1,1 union
select 'Awan Town, Ittehad Colony, Rawalpindi',getdate(),getdate(),1,1,1 union
select 'Narala, Rawalpindi',getdate(),getdate(),1,1,1 union
select 'Dhoak Mangtal, Dheri Hassanabad, Gulshanabad, BethSaida Colony, Dhoke Chiragh Din, Gulshan-e-Fatima, Nun, Friends Colony, Millat Colony, Committee Mohalla, Khayaban-e-Shifa, Dhok Kak, Dhok Kalas, Sheikhpur, Rawalpindi',getdate(),getdate(),1,1,1 union
select 'Baber Colony, Allama Iqbal Colony, Mangral Town, Rawalpindi',getdate(),getdate(),1,1,1 union
select 'Ibrahim Nagar, Daryabad, Rawalpindi',getdate(),getdate(),1,1,1 union
select 'Dhoak Paracha, Rawalpindi',getdate(),getdate(),1,1,1 union
select 'T & T Colony, Wireless Residential Colony, Rawalpindi',getdate(),getdate(),1,1,1 union
select 'Shakriyal West, Quid e Azam Colony, Rawalpindi',getdate(),getdate(),1,1,1 union
select 'Zeeshan Colony, Shadman Town, Rawalpindi',getdate(),getdate(),1,1,1 union
select 'Kalyamabad, Rawalpindi',getdate(),getdate(),1,1,1 union
select 'Abid Basti, Gorakkhpur, Samar Zar Housing Society, Gulshan-e-Shamal, Shahpur, Garden Villas, Jhang, Dahgal, Tarbela Colony, Dhoke Abdullah, Dhoke Maria Jarahi, Mohra Chhappar, Malik Colony, Christian Colony, Lakhu, FOECHS, Rupa, Kolian, Dhok Saparaswali, Muqadmewali, Kohala, Rawalpindi',getdate(),getdate(),1,1,1 union
select 'Lalarukh Colony, Rawalpindi',getdate(),getdate(),1,1,1 union
select 'Fauji Colony, Rawalpindi',getdate(),getdate(),1,1,1 union
select 'Pir Wadai, Rawalpindi',getdate(),getdate(),1,1,1 union
select 'Dhoak Babu Irfan, Rawalpindi',getdate(),getdate(),1,1,1 union
select 'Lalazar Old & New, Rawalpindi',getdate(),getdate(),1,1,1 union
select 'Lal Kurti, Rawalpindi',getdate(),getdate(),1,1,1 union
select 'Hyderabad',getdate(),getdate(),1,1,1 union
select 'Qasimabad, Hyderabad',getdate(),getdate(),1,1,1 union
select 'Old Wahdat Colony, Hyderabad',getdate(),getdate(),1,1,1 union
select 'Sahrish Nagar, Hyderabad',getdate(),getdate(),1,1,1 union
select 'Khalid Society, Hyderabad',getdate(),getdate(),1,1,1 union
select 'Sheedi Goth, Hyderabad',getdate(),getdate(),1,1,1 union
select 'AlMustafa Town, Hyderabad',getdate(),getdate(),1,1,1 union
select 'Nasim Nagar, Hyderabad',getdate(),getdate(),1,1,1 union
select 'Momin Nagar, Hyderabad',getdate(),getdate(),1,1,1 union
select 'Qasim Town, Hyderabad',getdate(),getdate(),1,1,1 union
select 'Gulshan e Karim, Hyderabad',getdate(),getdate(),1,1,1 union
select 'Bhittai Town, Hyderabad',getdate(),getdate(),1,1,1 union
select 'Samanabad, Hyderabad',getdate(),getdate(),1,1,1 union
select 'Gulistan e Sajjad  , Hyderabad',getdate(),getdate(),1,1,1 union
select 'Goth Karan Khan Shoro, Hyderabad',getdate(),getdate(),1,1,1 union
select 'Diplai Memon Cooperative Housing Society, Hyderabad',getdate(),getdate(),1,1,1 union
select 'Ammar City, Hyderabad',getdate(),getdate(),1,1,1 union
select 'Army Quarters, Hyderabad',getdate(),getdate(),1,1,1 union
select 'Gulistan e Fatima Phase 2, Hyderabad',getdate(),getdate(),1,1,1 union
select 'Mubarak Housing Society, Hyderabad',getdate(),getdate(),1,1,1 union
select 'Ghumman Abad, Hyderabad',getdate(),getdate(),1,1,1 union
select 'Bachal Solangi, Hyderabad',getdate(),getdate(),1,1,1 union
select 'Latifabad, Hyderabad',getdate(),getdate(),1,1,1 union
select 'Gulshan Colony, Hyderabad',getdate(),getdate(),1,1,1 union
select 'Mir Fazal Town, Hyderabad',getdate(),getdate(),1,1,1 union
select 'Madina Colony, Hyderabad',getdate(),getdate(),1,1,1 union
select 'Nashtar Colony, Hyderabad',getdate(),getdate(),1,1,1 union
select 'Jinnah Colony, Hyderabad',getdate(),getdate(),1,1,1 union
select 'Iqbal Colony, Hyderabad',getdate(),getdate(),1,1,1 union
select 'Ferozabad , Hyderabad',getdate(),getdate(),1,1,1 union
select 'SITE Area, Hyderabad',getdate(),getdate(),1,1,1 union
select 'Gulshan-e-Zealpak Cooperative Housing Society, Hyderabad',getdate(),getdate(),1,1,1 union
select 'Hussainabad, Hyderabad',getdate(),getdate(),1,1,1 union
select 'Kohisar Colony, Hyderabad',getdate(),getdate(),1,1,1 union
select 'Kohisar Extension, Hyderabad',getdate(),getdate(),1,1,1 union
select 'Gulistan e Sarmast, Hyderabad',getdate(),getdate(),1,1,1 union
select 'Goth Mihrani, Hyderabad',getdate(),getdate(),1,1,1 union
select 'Pakka Qilla, Hyderabad',getdate(),getdate(),1,1,1 union
select 'Choti Ghiti, Hyderabad',getdate(),getdate(),1,1,1 union
select 'Tando Agha, Hyderabad',getdate(),getdate(),1,1,1 union
select 'Talpur Compound, Hyderabad',getdate(),getdate(),1,1,1 union
select 'Islamabad, Hyderabad',getdate(),getdate(),1,1,1 union
select 'Goods Naka, Hyderabad',getdate(),getdate(),1,1,1 union
select 'Guru Nagar, Hyderabad',getdate(),getdate(),1,1,1 union
select 'Phuleli, Hyderabad',getdate(),getdate(),1,1,1 union
select 'Pareetabad, Hyderabad',getdate(),getdate(),1,1,1 union
select 'AL Fazal Town, Hyderabad',getdate(),getdate(),1,1,1 union
select 'Noorani Basti, Hyderabad',getdate(),getdate(),1,1,1 union
select 'Basti Lashari, Hyderabad',getdate(),getdate(),1,1,1 union
select 'Defence, Hyderabad',getdate(),getdate(),1,1,1 union
select 'Hyderabad',getdate(),getdate(),1,1,1 union
select 'Bengali Colony, Hyderabad',getdate(),getdate(),1,1,1 union
select 'Tando Jhanian, Hyderabad',getdate(),getdate(),1,1,1 union
select 'Cantonment, Hyderabad',getdate(),getdate(),1,1,1 union
select 'Doctors Colony, Hyderabad',getdate(),getdate(),1,1,1 union
select 'Khokar Mohalla, Hyderabad',getdate(),getdate(),1,1,1 union
select 'Shahi Bazar, Hyderabad',getdate(),getdate(),1,1,1 union
select 'Kaccha Qilla, Hyderabad',getdate(),getdate(),1,1,1 union
select 'Hyder Bux Jatoi, Hyderabad',getdate(),getdate(),1,1,1 union
select 'Gulshan e hali, Hyderabad',getdate(),getdate(),1,1,1 union
select 'American Quaters, Hyderabad',getdate(),getdate(),1,1,1 union
select 'G.O.R Colony, Hyderabad',getdate(),getdate(),1,1,1 union
select 'Gujrati Para, Hyderabad',getdate(),getdate(),1,1,1 union
select 'Tando Yousif, Hyderabad',getdate(),getdate(),1,1,1 union
select 'Tilak Incline, Hyderabad',getdate(),getdate(),1,1,1 union
select 'Sarfaraz Incline, Hyderabad',getdate(),getdate(),1,1,1 union
select 'Pathan Colony, Hyderabad',getdate(),getdate(),1,1,1 union
select 'Heerabad, Hyderabad',getdate(),getdate(),1,1,1 union
select 'Liaqat Colony, Hyderabad',getdate(),getdate(),1,1,1 union
select 'Sukkur',getdate(),getdate(),1,1,1 union
select 'Bachal Shah Miani, Sukkur',getdate(),getdate(),1,1,1 union
select 'Sukkur Town Ship, Sukkur',getdate(),getdate(),1,1,1 union
select 'Basheerabad, Sukkur',getdate(),getdate(),1,1,1 union
select 'Behari Colony, Sukkur',getdate(),getdate(),1,1,1 union
select 'Shah Khalid Colony, Sukkur',getdate(),getdate(),1,1,1 union
select 'Sindhi Cooperative Housing Society, Sukkur',getdate(),getdate(),1,1,1 union
select 'Bhitai Nagar, Sukkur',getdate(),getdate(),1,1,1 union
select 'Millat Cooperative Housing Society, Sukkur',getdate(),getdate(),1,1,1 union
select 'Govt Employees Cooperative Housing Society, Sukkur',getdate(),getdate(),1,1,1 union
select 'Jaffria Cooperative Housing Society, Sukkur',getdate(),getdate(),1,1,1 union
select 'Sukkur Cooperative Housing Society, Sukkur',getdate(),getdate(),1,1,1 union
select 'Pak Cooperative Housing Society, Sukkur',getdate(),getdate(),1,1,1 union
select 'Sindh Cooperative Housing Society, Sukkur',getdate(),getdate(),1,1,1 union
select 'Delhi Muslim Housing Society, Sukkur',getdate(),getdate(),1,1,1 union
select 'Soarth Memon Housing Society, Sukkur',getdate(),getdate(),1,1,1 union
select 'Pak Memon Housing Society, Sukkur',getdate(),getdate(),1,1,1 union
select 'Arain, Sukkur',getdate(),getdate(),1,1,1 union
select 'Shahpur, Sukkur',getdate(),getdate(),1,1,1 union
select 'Abad Lakha, Sukkur',getdate(),getdate(),1,1,1 union
select 'Lakhi Town, Sukkur',getdate(),getdate(),1,1,1 union
select 'Barrage Colony, Sukkur',getdate(),getdate(),1,1,1 union
select 'Memon Mohalla, Sukkur',getdate(),getdate(),1,1,1 union
select 'Nusrat Colony, Sukkur',getdate(),getdate(),1,1,1 union
select 'Railway Banglows, Sukkur',getdate(),getdate(),1,1,1 union
select 'Sindh Muslim Cooperative Housing Society, Sukkur',getdate(),getdate(),1,1,1 union
select 'Nawa Goth, Sukkur',getdate(),getdate(),1,1,1 union
select 'Latifabad, Sukkur',getdate(),getdate(),1,1,1 union
select 'New Pind, Sukkur',getdate(),getdate(),1,1,1 union
select 'Golimar, Sukkur',getdate(),getdate(),1,1,1 union
select 'Old Sukkar, Sukkur',getdate(),getdate(),1,1,1 union
select 'Thermal Colony, Sukkur',getdate(),getdate(),1,1,1 union
select 'S.I.T.E, Sukkur',getdate(),getdate(),1,1,1 union
select 'Bukkar Island, Sukkur',getdate(),getdate(),1,1,1 union
select 'Multan',getdate(),getdate(),1,1,1 union
select 'Nazimabad, Multan',getdate(),getdate(),1,1,1 union
select 'Abid Colony, Multan',getdate(),getdate(),1,1,1 union
select 'Shahmasabad, Multan',getdate(),getdate(),1,1,1 union
select 'WAPDA Colony, Multan',getdate(),getdate(),1,1,1 union
select 'Akhtar Colony, Multan',getdate(),getdate(),1,1,1 union
select 'Hassan Abad, Multan',getdate(),getdate(),1,1,1 union
select 'Peoples Colony, Multan',getdate(),getdate(),1,1,1 union
select 'Shah Rukn-e-Alam Colony, Multan',getdate(),getdate(),1,1,1 union
select 'Ghous Pura, Multan',getdate(),getdate(),1,1,1 union
select 'Writer Colony, Multan',getdate(),getdate(),1,1,1 union
select 'Shahrif Pura, Multan',getdate(),getdate(),1,1,1 union
select 'Manzoorabad, Multan',getdate(),getdate(),1,1,1 union
select 'Jamal Pura, Multan',getdate(),getdate(),1,1,1 union
select 'Khawaja Farid Colony, Multan',getdate(),getdate(),1,1,1 union
select 'Behani, Multan',getdate(),getdate(),1,1,1 union
select 'Piran Ghaib, Multan',getdate(),getdate(),1,1,1 union
select 'Banjal Wala, Multan',getdate(),getdate(),1,1,1 union
select 'Lother, Multan',getdate(),getdate(),1,1,1 union
select 'Tatay Pur, Multan',getdate(),getdate(),1,1,1 union
select 'Dhumra, Multan',getdate(),getdate(),1,1,1 union
select 'Kotal Meharam, Multan',getdate(),getdate(),1,1,1 union
select 'Multani Wala, Multan',getdate(),getdate(),1,1,1 union
select 'Budhla Sannat, Multan',getdate(),getdate(),1,1,1 union
select 'Khotay Wala, Multan',getdate(),getdate(),1,1,1 union
select 'Gharyala Wala, Multan',getdate(),getdate(),1,1,1 union
select 'Per Colony, Multan',getdate(),getdate(),1,1,1 union
select ' Ashraf Colony, Multan',getdate(),getdate(),1,1,1 union
select ' Timber Market, Multan',getdate(),getdate(),1,1,1 union
select ' Latif Abad, Multan',getdate(),getdate(),1,1,1 union
select ' Glass Factory, Multan',getdate(),getdate(),1,1,1 union
select ' Gulnar Colony, Multan',getdate(),getdate(),1,1,1 union
select ' Abbas Colony, Multan',getdate(),getdate(),1,1,1 union
select ' Khanqah Inayat Shah, Multan',getdate(),getdate(),1,1,1 union
select ' Hasan Perwana, Multan',getdate(),getdate(),1,1,1 union
select ' Gul Din Colony, Multan',getdate(),getdate(),1,1,1 union
select ' Qadeer Abad , Multan',getdate(),getdate(),1,1,1 union
select ' Shadman Colony, Multan',getdate(),getdate(),1,1,1 union
select ' Rangeel Pur, Multan',getdate(),getdate(),1,1,1 union
select ' Kayan Pur, Multan',getdate(),getdate(),1,1,1 union
select ' Muzzafarabad, Multan',getdate(),getdate(),1,1,1 union
select ' Bakhar Arbi, Multan',getdate(),getdate(),1,1,1 union
select ' Bili Wala, Multan',getdate(),getdate(),1,1,1 union
select ' Kabir Pur, Multan',getdate(),getdate(),1,1,1 union
select ' Lar, Multan',getdate(),getdate(),1,1,1 union
select ' Qasba Maral, Multan',getdate(),getdate(),1,1,1 union
select ' Ayyazabad Maral, Multan',getdate(),getdate(),1,1,1 union
select ' Khokhar, Multan',getdate(),getdate(),1,1,1 union
select ' Hamid Pur Kanora, Multan',getdate(),getdate(),1,1,1 union
select ' Sher Shah, Multan',getdate(),getdate(),1,1,1 union
select 'Dewan Bagh, Multan',getdate(),getdate(),1,1,1 union
select 'Gulgasht, Multan',getdate(),getdate(),1,1,1 union
select 'Officer Colony, Multan',getdate(),getdate(),1,1,1 union
select 'Meherban Colony, Multan',getdate(),getdate(),1,1,1 union
select 'Sadiq Colony, Multan',getdate(),getdate(),1,1,1 union
select 'Tibba Masood Pur, Multan',getdate(),getdate(),1,1,1 union
select 'Alamdi Sura, Multan',getdate(),getdate(),1,1,1 union
select 'Taraf Mubarak Doem, Multan',getdate(),getdate(),1,1,1 union
select 'Neel kot, Multan',getdate(),getdate(),1,1,1 union
select 'Durana Langana, Multan',getdate(),getdate(),1,1,1 union
select 'Jahangirabad, Multan',getdate(),getdate(),1,1,1 union
select 'Binda Sandeela, Multan',getdate(),getdate(),1,1,1 union
select 'Nawabpur, Multan',getdate(),getdate(),1,1,1 union
select 'Saleh Mahay, Multan',getdate(),getdate(),1,1,1 union
select 'Punjkuha, Multan',getdate(),getdate(),1,1,1 union
select 'Lutfabad, Multan',getdate(),getdate(),1,1,1 union
select 'Bosan, Multan',getdate(),getdate(),1,1,1 union
select 'Alam Pur, Multan',getdate(),getdate(),1,1,1 union
select 'Abbas Pur, Multan',getdate(),getdate(),1,1,1 union
select 'Matti Tal, Multan',getdate(),getdate(),1,1,1 union
select 'Quadirpur Ran, Multan',getdate(),getdate(),1,1,1 union
select 'Qasim Bela, Multan',getdate(),getdate(),1,1,1 union
select 'Garden Town, Multan',getdate(),getdate(),1,1,1 union
select 'Jakhar Pur, Multan',getdate(),getdate(),1,1,1 union
select ' Jalalpur City, Multan',getdate(),getdate(),1,1,1 union
select ' Drab Pur, Multan',getdate(),getdate(),1,1,1 union
select ' Khanbela, Multan',getdate(),getdate(),1,1,1 union
select ' Bait Katch, Multan',getdate(),getdate(),1,1,1 union
select ' Enayyat Pur, Multan',getdate(),getdate(),1,1,1 union
select 'Ghazi Pur, Multan',getdate(),getdate(),1,1,1 union
select ' Ali Pur, Multan',getdate(),getdate(),1,1,1 union
select 'Jahan Pur, Multan',getdate(),getdate(),1,1,1 union
select ' Mian Pur Belay Walla, Multan',getdate(),getdate(),1,1,1 union
select ' Karam Ali Walla, Multan',getdate(),getdate(),1,1,1 union
select 'Lalwah, Multan',getdate(),getdate(),1,1,1 union
select ' Nauraja Bhutta, Multan',getdate(),getdate(),1,1,1 union
select ' Kotla Chakar, Multan',getdate(),getdate(),1,1,1 union
select ' Bahadar Pur, Multan',getdate(),getdate(),1,1,1 union
select 'Saleem Colony, Multan',getdate(),getdate(),1,1,1 union
select 'Iqbal Nagar, Multan',getdate(),getdate(),1,1,1 union
select 'Niazabad, Multan',getdate(),getdate(),1,1,1 union
select 'Mumtazabad, Multan',getdate(),getdate(),1,1,1 union
select 'Panjnand Colony, Multan',getdate(),getdate(),1,1,1 union
select 'Qasbapura, Multan',getdate(),getdate(),1,1,1 union
select 'Gulshan-e-Faiz, Multan',getdate(),getdate(),1,1,1 union
select 'Gulzaib Colony, Multan',getdate(),getdate(),1,1,1 union
select 'Shah Risal, Multan',getdate(),getdate(),1,1,1 union
select 'New Nizamabad, Multan',getdate(),getdate(),1,1,1 union
select 'Abbaspura, Multan',getdate(),getdate(),1,1,1 union
select 'Mohalla Kamangran, Multan',getdate(),getdate(),1,1,1 union
select 'Shah Gardez, Multan',getdate(),getdate(),1,1,1 union
select 'Hakeem Wala, Multan',getdate(),getdate(),1,1,1 union
select 'Dera Budhu Malik, Multan',getdate(),getdate(),1,1,1 union
select 'Jhok Lasharpur, Multan',getdate(),getdate(),1,1,1 union
select 'Bootay Wala, Multan',getdate(),getdate(),1,1,1 union
select 'Makhdoom Rashid, Multan',getdate(),getdate(),1,1,1 union
select 'Khanpur Maral, Multan',getdate(),getdate(),1,1,1 union
select '18-MR, Multan',getdate(),getdate(),1,1,1 union
select 'Chah 5-Faiz, Multan',getdate(),getdate(),1,1,1 union
select 'Qadirpur Lar, Multan',getdate(),getdate(),1,1,1 union
select 'Basti Malook, Multan',getdate(),getdate(),1,1,1 union
select 'Shujjabad City, Multan',getdate(),getdate(),1,1,1 union
select 'Ponta, Multan',getdate(),getdate(),1,1,1 union
select 'Chah R.S., Multan',getdate(),getdate(),1,1,1 union
select 'Sikandarabad, Multan',getdate(),getdate(),1,1,1 union
select 'Shahpur Ubha, Multan',getdate(),getdate(),1,1,1 union
select 'Gajju Hatta, Multan',getdate(),getdate(),1,1,1 union
select 'Basti Mithu, Multan',getdate(),getdate(),1,1,1 union
select 'Raja Ram, Multan',getdate(),getdate(),1,1,1 union
select 'Kotli Nijabar, Multan',getdate(),getdate(),1,1,1 union
select 'Rasool Pur, Multan',getdate(),getdate(),1,1,1 union
select 'Matotli, Multan',getdate(),getdate(),1,1,1 union
select 'That Ghalwan, Multan',getdate(),getdate(),1,1,1 union
select 'Punjani, Multan',getdate(),getdate(),1,1,1 union
select 'Jalalpur Khakhi, Multan',getdate(),getdate(),1,1,1 union
select 'Gardazpur, Multan',getdate(),getdate(),1,1,1 union
select 'Mahra, Multan',getdate(),getdate(),1,1,1 union
select 'Faisalabad',getdate(),getdate(),1,1,1 union
select 'Sahibnagar, Faisalabad',getdate(),getdate(),1,1,1 union
select 'Fort Defence, Faisalabad',getdate(),getdate(),1,1,1 union
select 'Four Seasons Housing, Faisalabad',getdate(),getdate(),1,1,1 union
select 'Sehgal City, Faisalabad',getdate(),getdate(),1,1,1 union
select 'Chenab Gardens, Faisalabad',getdate(),getdate(),1,1,1 union
select 'Chak 235 RB Nia Mauna, Faisalabad',getdate(),getdate(),1,1,1 union
select 'Chak 225 RB Malkhanwala, Faisalabad',getdate(),getdate(),1,1,1 union
select 'Chak 226 RB, Faisalabad',getdate(),getdate(),1,1,1 union
select 'Ilahi Abad, Faisalabad',getdate(),getdate(),1,1,1 union
select 'Waris Pura, Faisalabad',getdate(),getdate(),1,1,1 union
select 'Barkat pura, Faisalabad',getdate(),getdate(),1,1,1 union
select 'Yaseenabad, Faisalabad',getdate(),getdate(),1,1,1 union
select 'Ameen Park, Faisalabad',getdate(),getdate(),1,1,1 union
select 'D-Type Colony, Faisalabad',getdate(),getdate(),1,1,1 union
select 'Tech Town, Faisalabad',getdate(),getdate(),1,1,1 union
select 'Allam Iqbal Colony, Faisalabad',getdate(),getdate(),1,1,1 union
select 'Zulfiqar colony, Faisalabad',getdate(),getdate(),1,1,1 union
select '224 Fateh Din, Faisalabad',getdate(),getdate(),1,1,1 union
select 'Karim Town, Faisalabad',getdate(),getdate(),1,1,1 union
select 'Rabbani Colonuy, Faisalabad',getdate(),getdate(),1,1,1 union
select 'Younas town, Faisalabad',getdate(),getdate(),1,1,1 union
select 'Rachna Town, Faisalabad',getdate(),getdate(),1,1,1 union
select 'Al fayaz Colony, Faisalabad',getdate(),getdate(),1,1,1 union
select 'Sarfaraz colony, Faisalabad',getdate(),getdate(),1,1,1 union
select 'Peoples Colony, Faisalabad',getdate(),getdate(),1,1,1 union
select 'Suhailabad, Faisalabad',getdate(),getdate(),1,1,1 union
select 'Jilani pura, Faisalabad',getdate(),getdate(),1,1,1 union
select 'Batala Colony, Faisalabad',getdate(),getdate(),1,1,1 union
select 'Shadman Colony, Faisalabad',getdate(),getdate(),1,1,1 union
select 'Al Najaf Colony, Faisalabad',getdate(),getdate(),1,1,1 union
select 'Muhammad Nagar, Faisalabad',getdate(),getdate(),1,1,1 union
select 'Gulzar Colony, Faisalabad',getdate(),getdate(),1,1,1 union
select 'Rasool Nagar, Faisalabad',getdate(),getdate(),1,1,1 union
select 'Akbar Colony, Faisalabad',getdate(),getdate(),1,1,1 union
select 'Abbas Nagar, Faisalabad',getdate(),getdate(),1,1,1 union
select 'Usmanabad, Faisalabad',getdate(),getdate(),1,1,1 union
select 'Fareed Town, Faisalabad',getdate(),getdate(),1,1,1 union
select 'Ali Town, Faisalabad',getdate(),getdate(),1,1,1 union
select 'Madina Abad, Faisalabad',getdate(),getdate(),1,1,1 union
select 'Kehkashan colony # 1, Faisalabad',getdate(),getdate(),1,1,1 union
select 'Chak 215 RB Nethari, Faisalabad',getdate(),getdate(),1,1,1 union
select 'Satellite town , Faisalabad',getdate(),getdate(),1,1,1 union
select 'Rehman Garden, Faisalabad',getdate(),getdate(),1,1,1 union
select 'Punjab Government Servants Housing Colony, Faisalabad',getdate(),getdate(),1,1,1 union
select 'Awan Wala, Faisalabad',getdate(),getdate(),1,1,1 union
select 'Medina Town, Faisalabad',getdate(),getdate(),1,1,1 union
select 'Kohinoor City, Faisalabad',getdate(),getdate(),1,1,1 union
select 'Masoodabad, Faisalabad',getdate(),getdate(),1,1,1 union
select 'Umer Garden, Faisalabad',getdate(),getdate(),1,1,1 union
select 'Hassan pura, Faisalabad',getdate(),getdate(),1,1,1 union
select 'National Colony, Faisalabad',getdate(),getdate(),1,1,1 union
select 'Kehkashan colony # 3, Faisalabad',getdate(),getdate(),1,1,1 union
select 'Himat pura, Faisalabad',getdate(),getdate(),1,1,1 union
select 'Dudhi wala Sharqi, Faisalabad',getdate(),getdate(),1,1,1 union
select 'Umar Town, Faisalabad',getdate(),getdate(),1,1,1 union
select 'Bilal Garden, Faisalabad',getdate(),getdate(),1,1,1 union
select 'Al Rehmat Villas, Faisalabad',getdate(),getdate(),1,1,1 union
select 'Hamayon Town, Faisalabad',getdate(),getdate(),1,1,1 union
select 'Bukhari Town , Faisalabad',getdate(),getdate(),1,1,1 union
select 'Islam Pura, Faisalabad',getdate(),getdate(),1,1,1 union
select 'Faisal Town, Faisalabad',getdate(),getdate(),1,1,1 union
select 'Hana Town, Faisalabad',getdate(),getdate(),1,1,1 union
select 'Chak 215 RB Kakuana, Faisalabad',getdate(),getdate(),1,1,1 union
select 'Rasool Park, Faisalabad',getdate(),getdate(),1,1,1 union
select 'Khayaban Colony, Faisalabad',getdate(),getdate(),1,1,1 union
select 'Eden Garden, Faisalabad',getdate(),getdate(),1,1,1 union
select 'Officer Colony, Faisalabad',getdate(),getdate(),1,1,1 union
select 'Hassan Villas, Faisalabad',getdate(),getdate(),1,1,1 union
select 'Chak 208 RB Dogran wala, Faisalabad',getdate(),getdate(),1,1,1 union
select 'Green Town , Faisalabad',getdate(),getdate(),1,1,1 union
select 'Zia Colony, Faisalabad',getdate(),getdate(),1,1,1 union
select 'Chak 204 RB, Faisalabad',getdate(),getdate(),1,1,1 union
select 'Amir Town, Faisalabad',getdate(),getdate(),1,1,1 union
select 'Gut Wala 199, Faisalabad',getdate(),getdate(),1,1,1 union
select 'New Fakharabad, Faisalabad',getdate(),getdate(),1,1,1 union
select 'Fakharabad 199 RB, Faisalabad',getdate(),getdate(),1,1,1 union
select 'Forest Colony, Faisalabad',getdate(),getdate(),1,1,1 union
select 'Abdullah Pur, Faisalabad',getdate(),getdate(),1,1,1 union
select 'Tariqabad, Faisalabad',getdate(),getdate(),1,1,1 union
select 'Hajveri Town, Faisalabad',getdate(),getdate(),1,1,1 union
select 'Civil Lines, Faisalabad',getdate(),getdate(),1,1,1 union
select 'Mustaffabad, Faisalabad',getdate(),getdate(),1,1,1 union
select 'Rehman pura, Faisalabad',getdate(),getdate(),1,1,1 union
select 'Nighaban pura, Faisalabad',getdate(),getdate(),1,1,1 union
select 'Mansoorabad, Faisalabad',getdate(),getdate(),1,1,1 union
select 'Farooq Abad, Faisalabad',getdate(),getdate(),1,1,1 union
select 'Taj Colony, Faisalabad',getdate(),getdate(),1,1,1 union
select 'Gulistan Colony # 1, Faisalabad',getdate(),getdate(),1,1,1 union
select 'Sheeraz Park, Faisalabad',getdate(),getdate(),1,1,1 union
select 'Abbas Park, Faisalabad',getdate(),getdate(),1,1,1 union
select 'Amin Town, Faisalabad',getdate(),getdate(),1,1,1 union
select 'Bholay di Jugi, Faisalabad',getdate(),getdate(),1,1,1 union
select 'Mughal Pura, Faisalabad',getdate(),getdate(),1,1,1 union
select 'Nishat abad, Faisalabad',getdate(),getdate(),1,1,1 union
select 'Malik pur, Faisalabad',getdate(),getdate(),1,1,1 union
select 'Steam Power Station Colony, Faisalabad',getdate(),getdate(),1,1,1 union
select 'Qamaqr Garden, Faisalabad',getdate(),getdate(),1,1,1 union
select 'Faisal Town, Faisalabad',getdate(),getdate(),1,1,1 union
select 'Faisal Gardens, Faisalabad',getdate(),getdate(),1,1,1 union
select 'Civic Center, Faisalabad',getdate(),getdate(),1,1,1 union
select 'Paradise Valley, Faisalabad',getdate(),getdate(),1,1,1 union
select 'Judicial Employees Cooperative Housing , Faisalabad',getdate(),getdate(),1,1,1 union
select 'Haneef Garden, Faisalabad',getdate(),getdate(),1,1,1 union
select 'Yousafabad, Faisalabad',getdate(),getdate(),1,1,1 union
select 'Main Bazar, Faisalabad',getdate(),getdate(),1,1,1 union
select 'Sabina Town, Faisalabad',getdate(),getdate(),1,1,1 union
select 'Manawala, Faisalabad',getdate(),getdate(),1,1,1 union
select 'Wapda Colony, Faisalabad',getdate(),getdate(),1,1,1 union
select 'Jamilabad, Faisalabad',getdate(),getdate(),1,1,1 union
select 'Hajiabad, Faisalabad',getdate(),getdate(),1,1,1 union
select 'Aqsa Town, Faisalabad',getdate(),getdate(),1,1,1 union
select 'Office Block, Faisalabad',getdate(),getdate(),1,1,1 union
select 'Noorpur, Faisalabad',getdate(),getdate(),1,1,1 union
select 'Usman Town, Faisalabad',getdate(),getdate(),1,1,1 union
select 'Muslim Town, Faisalabad',getdate(),getdate(),1,1,1 union
select 'Abu Bakar Saddique Block, Faisalabad',getdate(),getdate(),1,1,1 union
select 'Green Town, Faisalabad',getdate(),getdate(),1,1,1 union
select 'Khayaban Gardens , Faisalabad',getdate(),getdate(),1,1,1 union
select 'Nanak pur, Faisalabad',getdate(),getdate(),1,1,1 union
select 'Gokhowal, Faisalabad',getdate(),getdate(),1,1,1 union
select 'Jilani Park, Faisalabad',getdate(),getdate(),1,1,1 union
select 'Chak 202 RB bhai wala, Faisalabad',getdate(),getdate(),1,1,1 union
select 'Gatti, Faisalabad',getdate(),getdate(),1,1,1 union
select 'Chak 198 RB Muniawala, Faisalabad',getdate(),getdate(),1,1,1 union
select 'Chak Bahga , Faisalabad',getdate(),getdate(),1,1,1 union
select 'Wapda City, Faisalabad',getdate(),getdate(),1,1,1 union
select 'Chak 201 RB Taragarh, Faisalabad',getdate(),getdate(),1,1,1 union
select 'Chak 201 RB Talawan, Faisalabad',getdate(),getdate(),1,1,1 union
select 'Muloani Harian, Faisalabad',getdate(),getdate(),1,1,1 union
select 'Sultan Nagar, Faisalabad',getdate(),getdate(),1,1,1 union
select 'Chak 190 RB Karari Kalan, Faisalabad',getdate(),getdate(),1,1,1 union
select 'Chak 195 RB jandan wala, Faisalabad',getdate(),getdate(),1,1,1 union
select 'Chak 116 JB Rara Taali, Faisalabad',getdate(),getdate(),1,1,1 union
select 'Chak 117 JB Dhanola, Faisalabad',getdate(),getdate(),1,1,1 union
select 'Chak 118 JB bathan, Faisalabad',getdate(),getdate(),1,1,1 union
select 'Rasul pur, Faisalabad',getdate(),getdate(),1,1,1 union
select 'FDA City, Faisalabad',getdate(),getdate(),1,1,1 union
select 'Samaana, Faisalabad',getdate(),getdate(),1,1,1 union
select 'University Town, Faisalabad',getdate(),getdate(),1,1,1 union
select 'Ghona East, Faisalabad',getdate(),getdate(),1,1,1 union
select 'Ghona West, Faisalabad',getdate(),getdate(),1,1,1 union
select 'Chak 296 RB, Faisalabad',getdate(),getdate(),1,1,1 union
select 'Chak 233, Faisalabad',getdate(),getdate(),1,1,1 union
select 'Sitara Colony, Faisalabad',getdate(),getdate(),1,1,1 union
select 'Muzaffar Colony, Faisalabad',getdate(),getdate(),1,1,1 union
select 'Nawaban wala, Faisalabad',getdate(),getdate(),1,1,1 union
select 'Gulshan e Rafique, Faisalabad',getdate(),getdate(),1,1,1 union
select 'Satellite town, Faisalabad',getdate(),getdate(),1,1,1 union
select 'Abdullah Colony, Faisalabad',getdate(),getdate(),1,1,1 union
select 'Samman Abad, Faisalabad',getdate(),getdate(),1,1,1 union
select 'Factory Area, Faisalabad',getdate(),getdate(),1,1,1 union
select 'Sir Syed Town, Faisalabad',getdate(),getdate(),1,1,1 union
select 'Khalid abad, Faisalabad',getdate(),getdate(),1,1,1 union
select 'Nazim abad, Faisalabad',getdate(),getdate(),1,1,1 union
select 'Partab nagar, Faisalabad',getdate(),getdate(),1,1,1 union
select 'Sardar Pura, Faisalabad',getdate(),getdate(),1,1,1 union
select 'AARI Housing Colony, Faisalabad',getdate(),getdate(),1,1,1 union
select 'Ayub Colony, Faisalabad',getdate(),getdate(),1,1,1 union
select 'Police lines, Faisalabad',getdate(),getdate(),1,1,1 union
select 'Model Town, Faisalabad',getdate(),getdate(),1,1,1 union
select 'Jinnah Colony, Faisalabad',getdate(),getdate(),1,1,1 union
select 'Diglus pura, Faisalabad',getdate(),getdate(),1,1,1 union
select 'Santpura, Faisalabad',getdate(),getdate(),1,1,1 union
select 'Muhalla Fareed Shakar Ganj, Faisalabad',getdate(),getdate(),1,1,1 union
select 'Gulberg, Faisalabad',getdate(),getdate(),1,1,1 union
select 'Afghanabad, Faisalabad',getdate(),getdate(),1,1,1 union
select 'Liaqat Town, Faisalabad',getdate(),getdate(),1,1,1 union
select 'Sheikh Colony, Faisalabad',getdate(),getdate(),1,1,1 union
select 'Gulfishan Colony, Faisalabad',getdate(),getdate(),1,1,1 union
select 'International Housing Society, Faisalabad',getdate(),getdate(),1,1,1 union
select 'Akbarabad, Faisalabad',getdate(),getdate(),1,1,1 union
select 'Hamza Block, Faisalabad',getdate(),getdate(),1,1,1 union
select 'Green View Colony, Faisalabad',getdate(),getdate(),1,1,1 union
select 'Jameel Town, Faisalabad',getdate(),getdate(),1,1,1 union
select 'Kanak Basti, Faisalabad',getdate(),getdate(),1,1,1 union
select 'Yong wala, Faisalabad',getdate(),getdate(),1,1,1 union
select 'Madan pura, Faisalabad',getdate(),getdate(),1,1,1 union
select 'Raza abad, Faisalabad',getdate(),getdate(),1,1,1 union
select 'Muneer Abad, Faisalabad',getdate(),getdate(),1,1,1 union
select 'Shahbaz town, Faisalabad',getdate(),getdate(),1,1,1 union
select 'Ali housing Society, Faisalabad',getdate(),getdate(),1,1,1 union
select 'Shadab Colony, Faisalabad',getdate(),getdate(),1,1,1 union
select 'Saifabad, Faisalabad',getdate(),getdate(),1,1,1 union
select 'Farid Town, Faisalabad',getdate(),getdate(),1,1,1 union
select 'Amin Town, Faisalabad',getdate(),getdate(),1,1,1 union
select 'Rasheedabad, Faisalabad',getdate(),getdate(),1,1,1 union
select 'Babuwala, Faisalabad',getdate(),getdate(),1,1,1 union
select 'Air Avenue, Faisalabad',getdate(),getdate(),1,1,1 union
select 'lasani Garden, Faisalabad',getdate(),getdate(),1,1,1 union
select 'Chak 123 JB Sidhupura, Faisalabad',getdate(),getdate(),1,1,1 union
select 'Ismail Town, Faisalabad',getdate(),getdate(),1,1,1 union
select 'Ghulam Muhammad abad, Faisalabad',getdate(),getdate(),1,1,1 union
select 'Murad abad, Faisalabad',getdate(),getdate(),1,1,1 union
select 'Kothi Sadat, Faisalabad',getdate(),getdate(),1,1,1 union
select 'Siddique Akbar Town, Faisalabad',getdate(),getdate(),1,1,1 union
select 'Kaleem Shaheed Colony, Faisalabad',getdate(),getdate(),1,1,1 union
select 'Aftabnagar, Faisalabad',getdate(),getdate(),1,1,1 union
select 'Chak 66 JB Dhandharan, Faisalabad',getdate(),getdate(),1,1,1 union
select 'Zeenat Town, Faisalabad',getdate(),getdate(),1,1,1 union
select 'Chak 7 JB Panjwar, Faisalabad',getdate(),getdate(),1,1,1 union
select 'Chak 6 JB Panjwar, Faisalabad',getdate(),getdate(),1,1,1 union
select 'Miranwala, Faisalabad',getdate(),getdate(),1,1,1 union
select 'Chak 100 JB, Faisalabad',getdate(),getdate(),1,1,1 union
select 'Gujranwala',getdate(),getdate(),1,1,1 union
select 'Chand Da Qila, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Kashmir Colony # 1, Gujranwala',getdate(),getdate(),1,1,1 union
select 'G. Magnolia Park Housing Scheme, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Abadi Talabi Wali, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Pind Pipliwala Goraya, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Green Town, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Nazir Colony Outskirts, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Miran-Je Town, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Kangniwala, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Rana Colony, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Nabi Pura, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Shadman Colony/Town, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Biahar Colony, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Asghar Colony, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Siddique Colony, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Allah Baksh Colony, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Nazir Colony, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Peoples Colony, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Basir Colony, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Mujahid Pura, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Islam Pura, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Rashid Colony, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Shamsabad, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Shehzada Shaheed Colony, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Wahdat Colony, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Mohalla Chaman Shah, Gujranwala',getdate(),getdate(),1,1,1 union
select 'New Muslim Colony, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Farid Town, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Faqirpura, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Muhammadpura, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Chicherwali, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Majid Colony, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Salim Colony, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Civil Lines, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Chatha Colony, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Agriculture Colony, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Fatomand, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Kacha Fatomand, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Mustafa Colony, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Gulshan Town, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Bilal Town, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Malik Park, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Gulberg City, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Faisal Town, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Ghosia Colony, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Faiz Alam Town, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Ghosiabad, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Mohalla Aliabad, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Gulshan-E-Iyaz Town, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Fazal Town, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Mohalla Quaid-a-Azam, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Rajput Colony, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Usman Colony, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Gulshan Aziz Colony, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Lohianwala, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Mufti Colony, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Garden Town, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Professors Colony, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Bhullewala, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Moaifiwala, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Bismillah Colony, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Al-badar Town, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Nawan Pind, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Bhinderanwala, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Kikriwala, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Satellite Town, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Sardar Town, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Mohalla Pondawala, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Police Lines, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Kashmir Colony, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Khokhar Ke, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Abu Bakar Town, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Sharif Colony, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Rehman Colony, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Moazzam Colony, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Tariq Abad, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Popular Nursery Town, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Qasim Town, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Madina Colony, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Tanveer Town, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Bhekopur, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Mohalla Noorpur, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Aziz Colony, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Gulzar Colony, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Raja Colony, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Ikram Town, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Allah Rakha Town, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Ratta Bajwa, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Jagna, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Chak Jagna, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Islampura, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Bilal Colony, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Kalim Colony, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Park Royal Town, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Waniawala, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Mustafabad, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Sardar Town, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Nagri Ahmad Shah, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Muzaffarabad, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Kot Baqar, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Korotana, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Kot Mamin, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Kamran Colony, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Canal View, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Kohlo Wala, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Wapda Town, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Jalil Town, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Faisal Colony, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Muhafiz Town, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Industrial Estate 2, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Muhammad Nagar, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Khiali, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Mohalla Mustafa, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Khiali Shahpura, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Sardar Colony, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Qaiser Colony, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Gulzar Colony, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Asad Colony, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Shadman Town, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Judicial Housing Society, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Kashmir Colony # 2, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Umar Farooq Colony, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Ifhan ji Town, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Muhammadi Town, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Mohalla Shama-ul-Arfin, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Sarfaraz Colony, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Madina Colony, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Ittifaq Colony, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Jinnah Colony, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Khalid Colony, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Highway Staff Colony, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Old Town, Gujranwala',getdate(),getdate(),1,1,1 union
select 'BaghbanPura, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Mehr Wazir, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Chah Chuhanwala, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Gulshanabad, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Chah Habib, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Mubarak Colony, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Shahrukh Colony, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Mian Mir Town, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Baghdad Town, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Sharif Pura, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Hajweri Town, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Muhammad Nagar, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Data Ganj Baksh, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Mominabad, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Mubarik Colony, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Wasan Pura, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Nowshera Sansi, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Qila Sundar Singh, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Awan Chowk, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Nawan Pind, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Bakhtay wala, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Mohalla Miran Shah, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Islampura, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Nazir Park, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Mian Sansi, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Gulistan Colony, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Rehman Colony, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Iftikhar Colony, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Kashif Park, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Shamsabad, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Ilyas Colony, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Nusrat Colony, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Hyderi Colony, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Francisabad, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Mohalla Noor Baba, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Krishan Nagar, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Abadi Mir Muzaffar, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Guru Nanakpura, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Gobandgarh, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Model Town, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Numanpura, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Rehmatpura, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Rehmanpura, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Rasulpura, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Dhule, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Shaheenabad, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Mughal Pura, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Siddique Akbar Town, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Fazal Pura, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Barkat Colony, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Zahid Colony, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Nurpura, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Mustafa Colony, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Sui Gas Road , Gujranwala',getdate(),getdate(),1,1,1 union
select 'Baghwala, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Tajpura, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Raza Abad, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Kotli Rustam, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Haider Colony, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Arif Colony, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Batth Colony, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Hasanpura, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Garjakh, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Ali Ji Town, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Muhammad Pura, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Rajkot, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Pasban Colony, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Ghulam Muhammad Town, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Samnabad, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Shalimar Town, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Pindi Bypass Grw, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Gulshan Colony, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Mehar Colony, Gujranwala',getdate(),getdate(),1,1,1 union
select 'Quetta',getdate(),getdate(),1,1,1 union
select 'Hazar Ganji, Quetta',getdate(),getdate(),1,1,1 union
select 'Kamalloo, Quetta',getdate(),getdate(),1,1,1 union
select 'Berro, Quetta',getdate(),getdate(),1,1,1 union
select 'Kech Baig, Quetta',getdate(),getdate(),1,1,1 union
select 'Shahwani, Quetta',getdate(),getdate(),1,1,1 union
select 'New Hazara Town, Quetta',getdate(),getdate(),1,1,1 union
select 'Hazara Colony, Quetta',getdate(),getdate(),1,1,1 union
select 'Qumbrani, Quetta',getdate(),getdate(),1,1,1 union
select 'Grid Station Colony, Quetta',getdate(),getdate(),1,1,1 union
select 'Sumalani, Quetta',getdate(),getdate(),1,1,1 union
select 'Khalq Lehri, Quetta',getdate(),getdate(),1,1,1 union
select 'Green Town, Quetta',getdate(),getdate(),1,1,1 union
select 'Hazara town, Quetta',getdate(),getdate(),1,1,1 union
select 'Salim Town, Quetta',getdate(),getdate(),1,1,1 union
select 'Sheikhan, Quetta',getdate(),getdate(),1,1,1 union
select 'Essa Nagri, Quetta',getdate(),getdate(),1,1,1 union
select 'Kharot Abad, Quetta',getdate(),getdate(),1,1,1 union
select 'Pashtun Bagh, Quetta',getdate(),getdate(),1,1,1 union
select 'Badizai, Quetta',getdate(),getdate(),1,1,1 union
select 'Khaizi, Quetta',getdate(),getdate(),1,1,1 union
select 'Khujalzai, Quetta',getdate(),getdate(),1,1,1 union
select 'Killi Gul Muhammad, Quetta',getdate(),getdate(),1,1,1 union
select 'Bazai, Quetta',getdate(),getdate(),1,1,1 union
select 'Qesco Colony, Quetta',getdate(),getdate(),1,1,1 union
select 'Chiltan Housing Society, Quetta',getdate(),getdate(),1,1,1 union
select 'Garden town, Quetta',getdate(),getdate(),1,1,1 union
select 'Killi Dumaran, Quetta',getdate(),getdate(),1,1,1 union
select 'Killi Malak Aman Kasi, Quetta',getdate(),getdate(),1,1,1 union
select 'Kali Kakozi, Quetta',getdate(),getdate(),1,1,1 union
select 'Sariab, Quetta',getdate(),getdate(),1,1,1 union
select 'New Baloch Colony, Quetta',getdate(),getdate(),1,1,1 union
select 'Shahnawaz, Quetta',getdate(),getdate(),1,1,1 union
select 'Malghani Mohallah, Quetta',getdate(),getdate(),1,1,1 union
select 'Mehmood Khan Kakar Town, Quetta',getdate(),getdate(),1,1,1 union
select 'Rodhi Alizai Town, Quetta',getdate(),getdate(),1,1,1 union
select 'Pirkani Abad, Quetta',getdate(),getdate(),1,1,1 union
select 'Mangal Abad, Quetta',getdate(),getdate(),1,1,1 union
select 'Rind Gar, Quetta',getdate(),getdate(),1,1,1 union
select 'New Pashtun Abad, Quetta',getdate(),getdate(),1,1,1 union
select 'Noorzai, Quetta',getdate(),getdate(),1,1,1 union
select 'Satellite Town, Quetta',getdate(),getdate(),1,1,1 union
select 'Pashtunabad, Quetta',getdate(),getdate(),1,1,1 union
select 'Mariabad, Quetta',getdate(),getdate(),1,1,1 union
select 'Railway Housing Society, Quetta',getdate(),getdate(),1,1,1 union
select 'Railway Colony, Quetta',getdate(),getdate(),1,1,1 union
select 'Wahdat colony, Quetta',getdate(),getdate(),1,1,1 union
select 'Lehri Abad, Quetta',getdate(),getdate(),1,1,1 union
select 'Gulshan Town, Quetta',getdate(),getdate(),1,1,1 union
select 'Durrani Town, Quetta',getdate(),getdate(),1,1,1 union
select 'Tarkha Kasi, Quetta',getdate(),getdate(),1,1,1 union
select 'Killi Deba, Quetta',getdate(),getdate(),1,1,1 union
select 'Hori Nala , Quetta',getdate(),getdate(),1,1,1 union
select 'Killi Sheikh Hussain, Quetta',getdate(),getdate(),1,1,1 union
select 'Hudda, Quetta',getdate(),getdate(),1,1,1 union
select 'Killi Ismail, Quetta',getdate(),getdate(),1,1,1 union
select 'Jinnah Town, Quetta',getdate(),getdate(),1,1,1 union
select 'Killi, Quetta',getdate(),getdate(),1,1,1 union
select 'Shahboo, Quetta',getdate(),getdate(),1,1,1 union
select 'Shaheed Haji Arsala Mohallah, Quetta',getdate(),getdate(),1,1,1 union
select 'Khanan, Quetta',getdate(),getdate(),1,1,1 union
select 'Khilji Abad, Quetta',getdate(),getdate(),1,1,1 union
select 'Loni, Quetta',getdate(),getdate(),1,1,1 union
select 'Gulberg, Quetta',getdate(),getdate(),1,1,1 union
select 'Modern Colony, Quetta',getdate(),getdate(),1,1,1 union
select 'City Area, Quetta',getdate(),getdate(),1,1,1 union
select 'Gulistan Town, Quetta',getdate(),getdate(),1,1,1 union
select 'Napier lInes, Quetta',getdate(),getdate(),1,1,1 union
select 'Quetta Cantonment, Quetta',getdate(),getdate(),1,1,1 union
select 'Nawai Killi Bhittani, Quetta',getdate(),getdate(),1,1,1 union
select 'Main Khel Abad, Quetta',getdate(),getdate(),1,1,1 union
select 'Labor Colony, Quetta',getdate(),getdate(),1,1,1 union
select 'Mizolo, Quetta',getdate(),getdate(),1,1,1 union
select 'Wapda Colony, Quetta',getdate(),getdate(),1,1,1 union
select 'Peshawar',getdate(),getdate(),1,1,1 union
select 'Tajabad, Peshawar',getdate(),getdate(),1,1,1 union
select 'Board Bazar, Peshawar',getdate(),getdate(),1,1,1 union
select 'Nasir Bagh, Peshawar',getdate(),getdate(),1,1,1 union
select 'PTCL Colony, Peshawar',getdate(),getdate(),1,1,1 union
select 'Polic Colony, Peshawar',getdate(),getdate(),1,1,1 union
select 'Askari, Peshawar',getdate(),getdate(),1,1,1 union
select 'Canal Town Colony, Peshawar',getdate(),getdate(),1,1,1 union
select 'Academy Town, Peshawar',getdate(),getdate(),1,1,1 union
select 'Danishabad, Peshawar',getdate(),getdate(),1,1,1 union
select 'Islamia College Colony, Peshawar',getdate(),getdate(),1,1,1 union
select 'Agriculture Colony, Peshawar',getdate(),getdate(),1,1,1 union
select 'Forest Colony, Peshawar',getdate(),getdate(),1,1,1 union
select 'Peshawar University Colony, Peshawar',getdate(),getdate(),1,1,1 union
select 'Professors Colony, Peshawar',getdate(),getdate(),1,1,1 union
select 'Engineering University Colony, Peshawar',getdate(),getdate(),1,1,1 union
select 'Lalazar Colony, Peshawar',getdate(),getdate(),1,1,1 union
select 'Rahatabad, Peshawar',getdate(),getdate(),1,1,1 union
select 'Palosy, Peshawar',getdate(),getdate(),1,1,1 union
select 'Shaheen Town, Peshawar',getdate(),getdate(),1,1,1 union
select 'Jahangirabad, Peshawar',getdate(),getdate(),1,1,1 union
select 'University Town, Peshawar',getdate(),getdate(),1,1,1 union
select 'Tahkal Bala, Peshawar',getdate(),getdate(),1,1,1 union
select 'Tehkal Payan, Peshawar',getdate(),getdate(),1,1,1 union
select 'Karkhano Market, Peshawar',getdate(),getdate(),1,1,1 union
select 'Warsak Road, Peshawar',getdate(),getdate(),1,1,1 union
select 'Christian Colony, Peshawar',getdate(),getdate(),1,1,1 union
select 'Aabshar Colony, Peshawar',getdate(),getdate(),1,1,1 union
select 'WAPDA Colony, Peshawar',getdate(),getdate(),1,1,1 union
select 'Hassan Garhi, Peshawar',getdate(),getdate(),1,1,1 union
select 'Wazeer Bagh, Peshawar',getdate(),getdate(),1,1,1 union
select 'Tauheedabad, Peshawar',getdate(),getdate(),1,1,1 union
select 'Faqeerabad, Peshawar',getdate(),getdate(),1,1,1 union
select 'Faqeerabad, Peshawar',getdate(),getdate(),1,1,1 union
select 'Bilal Town, Peshawar',getdate(),getdate(),1,1,1 union
select 'Zaryab Colony, Peshawar',getdate(),getdate(),1,1,1 union
select 'Momin Town, Peshawar',getdate(),getdate(),1,1,1 union
select 'Amjad Colony, Peshawar',getdate(),getdate(),1,1,1 union
select 'Kazi Town, Peshawar',getdate(),getdate(),1,1,1 union
select 'Faisal Colony, Peshawar',getdate(),getdate(),1,1,1 union
select 'Muncipal Corporation Colony, Peshawar',getdate(),getdate(),1,1,1 union
select 'Bashirabad, Peshawar',getdate(),getdate(),1,1,1 union
select 'Saeedabad, Peshawar',getdate(),getdate(),1,1,1 union
select 'Afridi Garhi, Peshawar',getdate(),getdate(),1,1,1 union
select 'Malikabad, Peshawar',getdate(),getdate(),1,1,1 union
select 'Karim Pura, Peshawar',getdate(),getdate(),1,1,1 union
select 'Andar Sher, Peshawar',getdate(),getdate(),1,1,1 union
select 'Raiti Bazar, Peshawar',getdate(),getdate(),1,1,1 union
select 'Ashraf Road, Peshawar',getdate(),getdate(),1,1,1 union
select 'Hasht Nagri, Peshawar',getdate(),getdate(),1,1,1 union
select 'Firdous, Peshawar',getdate(),getdate(),1,1,1 union
select 'Gulbahar, Peshawar',getdate(),getdate(),1,1,1 union
select 'Gulbahar, Peshawar',getdate(),getdate(),1,1,1 union
select 'Goal Gathri, Peshawar',getdate(),getdate(),1,1,1 union
select 'Yaqatoot, Peshawar',getdate(),getdate(),1,1,1 union
select 'Kohati, Peshawar',getdate(),getdate(),1,1,1 union
select 'Brasko, Peshawar',getdate(),getdate(),1,1,1 union
select 'Ramdas, Peshawar',getdate(),getdate(),1,1,1 union
select 'Garhi Khana, Peshawar',getdate(),getdate(),1,1,1 union
select 'Gunj Gate, Peshawar',getdate(),getdate(),1,1,1 union
select 'Haider Colony, Peshawar',getdate(),getdate(),1,1,1 union
select 'Marvia Muhalla, Peshawar',getdate(),getdate(),1,1,1 union
select 'Shiekhabad, Peshawar',getdate(),getdate(),1,1,1 union
select 'New Islamabad Town, Peshawar',getdate(),getdate(),1,1,1 union
select 'Nishterabad, Peshawar',getdate(),getdate(),1,1,1 union
select 'Namak Mandi, Peshawar',getdate(),getdate(),1,1,1 union
select 'Khyber Bazar, Peshawar',getdate(),getdate(),1,1,1 union
select 'Qissa Khwani, Peshawar',getdate(),getdate(),1,1,1 union
select 'Meena Bazar, Peshawar',getdate(),getdate(),1,1,1 union
select 'Kochi Bazar, Peshawar',getdate(),getdate(),1,1,1 union
select 'Piple Mandi, Peshawar',getdate(),getdate(),1,1,1 union
select 'Chowk Nasir Khan, Peshawar',getdate(),getdate(),1,1,1 union
select 'Dhakki Nal Bandi, Peshawar',getdate(),getdate(),1,1,1 union
select 'Bachaa Garhey, Peshawar',getdate(),getdate(),1,1,1 union
select 'Muhalla Khudadad, Peshawar',getdate(),getdate(),1,1,1 union
select 'Kashkal, Peshawar',getdate(),getdate(),1,1,1 union
select 'Dabgari, Peshawar',getdate(),getdate(),1,1,1 union
select 'Dalazak Road, Peshawar',getdate(),getdate(),1,1,1 union
select 'Charsadda Road, Peshawar',getdate(),getdate(),1,1,1 union
select 'Eid Gah, Peshawar',getdate(),getdate(),1,1,1 union
select 'Haji Camp, Peshawar',getdate(),getdate(),1,1,1 union
select 'Baksho Pul, Peshawar',getdate(),getdate(),1,1,1 union
select 'Imamia Colony GT Road, Peshawar',getdate(),getdate(),1,1,1 union
select 'Bacha Khan Chowk, Peshawar',getdate(),getdate(),1,1,1 union
select 'Kohat Road, Peshawar',getdate(),getdate(),1,1,1 union
select 'Arbab Landi, Peshawar',getdate(),getdate(),1,1,1 union
select 'Qaziabad, Peshawar',getdate(),getdate(),1,1,1 union
select 'Badabair, Peshawar',getdate(),getdate(),1,1,1 union
select 'Sodazai, Peshawar',getdate(),getdate(),1,1,1 union
select 'Masterzai, Peshawar',getdate(),getdate(),1,1,1 union
select 'Chamkani, Peshawar',getdate(),getdate(),1,1,1 union
select 'Phandu, Peshawar',getdate(),getdate(),1,1,1 union
select 'Baghban, Peshawar',getdate(),getdate(),1,1,1 union
select 'Sarband, Peshawar',getdate(),getdate(),1,1,1 union
select 'Achini Bala, Peshawar',getdate(),getdate(),1,1,1 union
select 'Achini Payan, Peshawar',getdate(),getdate(),1,1,1 union
select 'Raigee, Peshawar',getdate(),getdate(),1,1,1 union
select 'Matani, Peshawar',getdate(),getdate(),1,1,1 union
select 'Taru Jabbad, Peshawar',getdate(),getdate(),1,1,1 union
select 'Bahadar Kalay, Peshawar',getdate(),getdate(),1,1,1 union
select 'Hayatabad, Peshawar',getdate(),getdate(),1,1,1 union
select 'Hayatabad, Peshawar',getdate(),getdate(),1,1,1 union
select 'Hayatabad, Peshawar',getdate(),getdate(),1,1,1 union
select 'Hayatabad, Peshawar',getdate(),getdate(),1,1,1 union
select 'Hayatabad, Peshawar',getdate(),getdate(),1,1,1 union
select 'Hayatabad, Peshawar',getdate(),getdate(),1,1,1 union
select 'Hayatabad, Peshawar',getdate(),getdate(),1,1,1 union
select 'Hayatabad Industrial Estate, Peshawar',getdate(),getdate(),1,1,1 union
select 'Defence Officers Colony, Peshawar',getdate(),getdate(),1,1,1 union
select 'Askari, Peshawar',getdate(),getdate(),1,1,1 union
select 'Mall Road, Peshawar',getdate(),getdate(),1,1,1 union
select 'Airport, Peshawar',getdate(),getdate(),1,1,1 union
select 'Saddar, Peshawar',getdate(),getdate(),1,1,1 union
select 'Governer House, Peshawar',getdate(),getdate(),1,1,1 union
select 'CM House, Peshawar',getdate(),getdate(),1,1,1 union
select 'IG House, Peshawar',getdate(),getdate(),1,1,1 union
select 'Speaker House, Peshawar',getdate(),getdate(),1,1,1 union
select 'PTV Peshawar Station, Peshawar',getdate(),getdate(),1,1,1 union
select 'PAF Head Quarter, Peshawar',getdate(),getdate(),1,1,1 union
select 'Sawati Phatak, Peshawar',getdate(),getdate(),1,1,1 union
select 'Army Employee Colony, Peshawar',getdate(),getdate(),1,1,1 union
select 'Army Flates, Peshawar',getdate(),getdate(),1,1,1 union
select 'Shami Road, Peshawar',getdate(),getdate(),1,1,1 union
select 'Core Commander House, Peshawar',getdate(),getdate(),1,1,1 union
select 'PC Hotel, Peshawar',getdate(),getdate(),1,1,1 union
select 'Peshawar Assembly, Peshawar',getdate(),getdate(),1,1,1 union
select 'PTCL Head Quarter, Peshawar',getdate(),getdate(),1,1,1 union
select 'Edward College Colony, Peshawar',getdate(),getdate(),1,1,1 union
select 'Khyber Super Market, Peshawar',getdate(),getdate(),1,1,1;

delete from locationalias where locationid in (select max(LocationId) from location group by Location having count(1)>1)
delete from location where locationid in (select max(LocationId) from location group by Location having count(1)>1)

