use nms
--set identity_insert program on
insert into program (ProgramId,name,ChannelId, CreationDate,LastUpdateDate,IsActive,IsApproved,MinStoryCount,MaxStoryCount,ProgramTypeId,BucketID,FilterId,ApiKey)values (868,'Breaking News',37,GETUTCDATE(),GETUTCDATE(),1,1,null,null,null,null,null,null)
insert into program (ProgramId,name,ChannelId, CreationDate,LastUpdateDate,IsActive,IsApproved,MinStoryCount,MaxStoryCount,ProgramTypeId,BucketID,FilterId,ApiKey)values (4720,'News Update',37,GETUTCDATE(),GETUTCDATE(),1,1,null,null,null,null,null,null)
--set identity_insert program off

set identity_insert folder on
insert into folder (Folderid,name,programid,CreationDate,IsRundown)values (56,'Breaking News',868,GETUTCDATE(),0)
insert into folder (Folderid,name,programid,CreationDate,IsRundown)values (57,'News Update',4720,GETUTCDATE(),0)
set identity_insert folder off