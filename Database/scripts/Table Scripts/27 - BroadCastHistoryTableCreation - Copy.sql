USE [NMS]
GO

/****** Object:  Table [dbo].[Alert]    Script Date: 10/27/2016 7:57:40 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[NewsFileBroadcastedHistory](
	[NewsFileBroadcastedHistoryId] [int] IDENTITY(1,1) NOT NULL,
	[NewsFileId] int not null,
	[Highlights] [nvarchar](max) NULL,
	[Text] [nvarchar](max) NULL,
	[CreationDate] [datetime] NOT NULL default(getdate()),
 CONSTRAINT [NewsFileBroadcastedHistoryId] PRIMARY KEY CLUSTERED 
(
	[NewsFileBroadcastedHistoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

