use nms

create TABLE DescrepencyNewsFile
(
	DescrepencyNewsFileId INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
	Title nvarchar(200),
	Source nvarchar(200),
	DescrepencyValue nvarchar(100),
	DescrepencyType int,
	CreationDate DateTime,
	LastUpdateDate DateTime,
	DiscrepencyStatusCode int,
	IsInserted bit,
	RawNews nvarchar(MAX),
        NewsFileId int,
	CategoryId int
);


