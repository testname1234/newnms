use NMS
CREATE TABLE NewsFileOrganization
(
NewsFileOrganizationId int IDENTITY(1,1) NOT NULL PRIMARY KEY,
organizationId int NOT NULL,
NewsFileId int NOT NULL,
IsActive bit NOT NULL DEFAULT(1),
CreationDate datetime,
LastUpdateDate datetime
)

CREATE TABLE Organization
(
OrganizationId int IDENTITY(1,1) NOT NULL PRIMARY KEY,
Name varchar(500) NOT NULL,
IsActive bit NOT NULL DEFAULT(1),
CreationDate datetime,
LastUpdateDate datetime
)
