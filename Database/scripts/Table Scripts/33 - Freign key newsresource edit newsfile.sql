Use NMS
GO
ALTER TABLE [dbo].[NewsResourceEdit]  WITH CHECK ADD  CONSTRAINT [FK_NewsResource_NewsFile]
 FOREIGN KEY([NewsID])
REFERENCES [dbo].[NewsFile] ([NewsFileID])
GO