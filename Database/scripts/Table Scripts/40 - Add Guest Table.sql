use NMS
CREATE TABLE Guest
(
GuestId int IDENTITY(1,1) NOT NULL PRIMARY KEY,
SlotId int NOT NULL,
LocationType int NOT NULL,
Question nvarchar(max),
CreationDate datetime,
LastUpdateDate datetime,
IsActive bit not null default(1)
)
