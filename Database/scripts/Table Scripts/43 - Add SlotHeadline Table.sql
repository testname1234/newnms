use nms
CREATE TABLE SlotHeadline
(
SlotHeadlineId int IDENTITY(1,1) NOT NULL PRIMARY KEY,
SlotId int FOREIGN KEY REFERENCES slot(SlotId) NOT NULL,
Headline nvarchar(1000) NOT NULL,
LanguageCode varchar(10),
CreationDate datetime,
LastUpdateDate datetime,
IsActive bit default(0),
)