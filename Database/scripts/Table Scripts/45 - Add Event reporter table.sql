use NMS
CREATE TABLE EventReporter
(
EventReporterId int IDENTITY(1,1) NOT NULL PRIMARY KEY,
ReporterId int NOT NULL,
NewsFileId int FOREIGN KEY REFERENCES NEWSFILE(NEWSFILEID),
CreationDate datetime,
LastUpdateDate datetime,
IsActive bit not null default(1)
)