use NMS
CREATE TABLE EventResource
(
EventResourceId int IDENTITY(1,1) NOT NULL PRIMARY KEY,
EventResourceType int NOT NULL,
NewsFileId int FOREIGN KEY REFERENCES newsfile(newsfileid),
CreationDate datetime,
LastUpdateDate datetime,
IsActive bit not null default(1)
)
