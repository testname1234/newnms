use NMS
CREATE TABLE EventType
(
EventTypeId int IDENTITY(1,1) NOT NULL PRIMARY KEY,
Name varchar(200) NOT NULL,
CreationDate datetime,
LastUpdateDate datetime,
IsActive bit not null default(1),
IsApproved bit not null default(0)
)

Insert into EventType (Name,CreationDate,LastUpdateDate,IsApproved) values('Press Conference',GETUTCDATE(),GETUTCDATE(),1)
Insert into EventType (Name,CreationDate,LastUpdateDate,IsApproved) values('Exhibition',GETUTCDATE(),GETUTCDATE(),1)
Insert into EventType (Name,CreationDate,LastUpdateDate,IsApproved) values('Interview',GETUTCDATE(),GETUTCDATE(),1)
Insert into EventType (Name,CreationDate,LastUpdateDate,IsApproved) values('Rally',GETUTCDATE(),GETUTCDATE(),1)
