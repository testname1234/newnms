use NMS
go
alter table tickercategory
add TicketCategoryTypeId int 
alter table tickercategory
add EntityIds text
alter table tickercategory
add TagIds text
alter table tickercategory
add LocationId int 
go

USE [NMS]
GO


CREATE TABLE [dbo].[KeyValue](
	[KeyValueId] [int] NOT NULL Identity(1,1),
	[Key] [varchar](225) NULL,
	[Value] [nvarchar](2000) NULL,
PRIMARY KEY CLUSTERED 
(
	[KeyValueId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO