use NMS
CREATE TABLE NewsFileHistory
(
NewsFileHistoryId int IDENTITY(1,1) NOT NULL PRIMARY KEY,
NewsFileId int NOT NULL FOREIGN KEY REFERENCES NEWSFILE(NEWSFILEID),
NewsFileJson nvarchar(max),
CreationDate datetime,
)