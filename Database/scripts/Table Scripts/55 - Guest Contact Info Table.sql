use NMS
CREATE TABLE GuestContactInfo
(
GuestContactInfoId int IDENTITY(1,1) NOT NULL PRIMARY KEY,
ContactInfoType int NOT NULL,
Value varchar(200) NOT NULL,
CelebrityId int FOREIGN KEY REFERENCES Celebrity(celebrityid),
CreationDate datetime,
LastUpdateDate datetime,
IsActive bit not null default(1)
)
