use NMS
CREATE TABLE McrTickerHistory
(
McrTickerHistoryId int IDENTITY(1,1) NOT NULL PRIMARY KEY,
TickerId int FOREIGN KEY REFERENCES Ticker(Tickerid),
[Text] nvarchar(max) NOT NULL,
TickerCategoryId int FOREIGN KEY REFERENCES TickerCategory(TickerCategoryid),
CreationDate datetime
)


