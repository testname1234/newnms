use NMS
CREATE TABLE McrTickerBroadcasted
(
McrTickerBroadcastedId int IDENTITY(1,1) NOT NULL PRIMARY KEY,
[Hour] int NOT NULL,
TickerCategoryId int NOT NULL FOREIGN KEY REFERENCES TickerCategory(TickerCategoryid),
[Limit] int NOT NULL,
Broadcasted int,
CreationDate datetime,
)