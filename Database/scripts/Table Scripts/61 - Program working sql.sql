USE [NMS]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[CelebrityType](
	CelebrityTypeId [int] IDENTITY(1,1) NOT NULL,
	Name varchar(1000) not NULL,
	IsActive bit not null,
CreationDate datetime not null,
LastUpdateDate datetime null
 CONSTRAINT [PK_CelebrityType] PRIMARY KEY CLUSTERED 
(
	CelebrityTypeId ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]



USE [NMS]
GO

/****** Object:  Table [dbo].[BunchNews]    Script Date: 12/16/2016 8:42:52 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[ChannelCelebrity](
	ChannelCelebrityId [int] IDENTITY(1,1) NOT NULL,
	ChannelId [int] not NULL,
	CelebrityId [int] not NULL,
	CelebrityTypeId [int] not NULL,
	IsActive bit not null,
CreationDate datetime not null,
LastUpdateDate datetime null
 CONSTRAINT [PK_ChannelCelebrity] PRIMARY KEY CLUSTERED 
(
	ChannelCelebrityId ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[ChannelCelebrity]  WITH NOCHECK ADD  CONSTRAINT [FK_ChannelCelebrity_Celebrity] FOREIGN KEY([CelebrityId])
REFERENCES [dbo].[Celebrity] ([CelebrityId])
GO


ALTER TABLE [dbo].[ChannelCelebrity]  WITH NOCHECK ADD  CONSTRAINT [FK_ChannelCelebrity_Channel] FOREIGN KEY([ChannelId])
REFERENCES [dbo].[Channel] ([ChannelId])
GO

ALTER TABLE [dbo].[ChannelCelebrity]  WITH NOCHECK ADD  CONSTRAINT [FK_ChannelCelebrity_CelebrityType] FOREIGN KEY([CelebrityTypeId])
REFERENCES [dbo].[CelebrityType] ([CelebrityTypeId])
GO


USE [NMS]
GO

/****** Object:  Table [dbo].[BunchNews]    Script Date: 12/16/2016 8:42:52 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[ProgramAnchor](
	ProgramAnchorId [int] IDENTITY(1,1) NOT NULL,
	ProgramId [int] not NULL,
	CelebrityId [int] not NULL,
	IsActive bit not null,
CreationDate datetime not null,
LastUpdateDate datetime null
 CONSTRAINT [PK_ProgramAnchor] PRIMARY KEY CLUSTERED 
(
	ProgramAnchorId ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[ProgramAnchor]  WITH NOCHECK ADD  CONSTRAINT [FK_ProgramAnchor_Celebrity] FOREIGN KEY([CelebrityId])
REFERENCES [dbo].[Celebrity] ([CelebrityId])
GO


ALTER TABLE [dbo].[ProgramAnchor]  WITH NOCHECK ADD  CONSTRAINT [FK_ProgramAnchor_Program] FOREIGN KEY([ProgramId])
REFERENCES [dbo].[Program] ([ProgramId])
GO

insert into CelebrityType ( name, IsActive, CreationDate)
values ('Anchor', 1, getdate())

go
alter table Slot
add AnchorId int

alter table Slot
add Script nvarchar(4000)
go