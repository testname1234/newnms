use NMS
CREATE TABLE McrTickerHistoryLog
(
McrTickerHistoryLogId int IDENTITY(1,1) NOT NULL PRIMARY KEY,
JsonObject nvarchar(max),
CreationDate datetime default(getutcdate())
)

