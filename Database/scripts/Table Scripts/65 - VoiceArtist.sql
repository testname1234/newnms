USE [NMS]
GO

/****** Object:  Table [dbo].[VoiceArtist]    Script Date: 1/4/2017 6:26:28 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[VoiceArtist](
	[VoiceArtistId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
	[CreationDate] [datetime] NOT NULL,
	[LastUpdateDate] [datetime] NULL,
	[IsActive] [bit] NULL,
 CONSTRAINT [PK_VoiceArtist] PRIMARY KEY CLUSTERED 
(
	[VoiceArtistId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[VoiceArtist] ADD  CONSTRAINT [DF_VoiceArtist_IsActive]  DEFAULT ((0)) FOR [IsActive]
GO


