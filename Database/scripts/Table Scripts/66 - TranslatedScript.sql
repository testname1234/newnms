USE [NMS]
GO

/****** Object:  Table [dbo].[TranslatedScript]    Script Date: 1/9/2017 5:15:39 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[TranslatedScript](
	[TranslatedScriptId] [int] IDENTITY(1,1) NOT NULL,
	[NewsFileId] [int] NULL,
	[Title] [nvarchar](700) NULL,
	[VoiceOver] [nvarchar](700) NULL,
	[CreationDate] [datetime] NULL,
	[LastUpdateDate] [datetime] NULL,
	[CreatedBy] [int] NULL,
	[Status] [int] NULL,
	[LanguageCode] [int] NULL,
 CONSTRAINT [PK_TranslatedScript] PRIMARY KEY CLUSTERED 
(
	[TranslatedScriptId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[TranslatedScript]  WITH CHECK ADD  CONSTRAINT [FK_TranslatedScript_NewsFile] FOREIGN KEY([NewsFileId])
REFERENCES [dbo].[NewsFile] ([NewsFileId])
GO

ALTER TABLE [dbo].[TranslatedScript] CHECK CONSTRAINT [FK_TranslatedScript_NewsFile]
GO


