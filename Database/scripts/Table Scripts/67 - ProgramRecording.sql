USE [NMS]
GO

/****** Object:  Table [dbo].[ProgramRecording]    Script Date: 1/9/2017 5:16:09 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ProgramRecording](
	[ProgramRecordingId] [int] IDENTITY(1,1) NOT NULL,
	[ProgramId] [int] NULL,
	[Path] [nvarchar](100) NULL,
	[Status] [int] NULL,
	[From] [datetime] NULL,
	[To] [datetime] NULL,
	[CreationDate] [datetime] NULL,
	[LastUpdateDate] [datetime] NULL,
 CONSTRAINT [PK_ProgramRecording] PRIMARY KEY CLUSTERED 
(
	[ProgramRecordingId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[ProgramRecording]  WITH CHECK ADD  CONSTRAINT [FK_ProgramRecording_Program] FOREIGN KEY([ProgramId])
REFERENCES [dbo].[Program] ([ProgramId])
GO

ALTER TABLE [dbo].[ProgramRecording] CHECK CONSTRAINT [FK_ProgramRecording_Program]
GO


