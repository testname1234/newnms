USE [NMS]
GO

/****** Object:  Table [dbo].[RecordingClips]    Script Date: 1/9/2017 5:14:56 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[RecordingClips](
	[RecordingClipId] [int] IDENTITY(1,1) NOT NULL,
	[Script] [nvarchar](700) NULL,
	[CelebrityId] [int] NULL,
	[ProgramRecordingId] [int] NULL,
	[From] [varchar](100) NULL,
	[To] [varchar](100) NULL,
	[CreatedBy] [int] NULL,
	[CreationDate] [datetime] NULL,
	[LastUpdateDate] [datetime] NULL,
	[Status] [int] NULL,
	[LanguageId] [int] NULL,
 CONSTRAINT [PK_RecordingClips] PRIMARY KEY CLUSTERED 
(
	[RecordingClipId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[RecordingClips]  WITH CHECK ADD  CONSTRAINT [FK_RecordingClips_Celebrity] FOREIGN KEY([CelebrityId])
REFERENCES [dbo].[Celebrity] ([CelebrityId])
GO

ALTER TABLE [dbo].[RecordingClips] CHECK CONSTRAINT [FK_RecordingClips_Celebrity]
GO

ALTER TABLE [dbo].[RecordingClips]  WITH CHECK ADD  CONSTRAINT [FK_RecordingClips_ProgramRecording] FOREIGN KEY([ProgramRecordingId])
REFERENCES [dbo].[ProgramRecording] ([ProgramRecordingId])
GO

ALTER TABLE [dbo].[RecordingClips] CHECK CONSTRAINT [FK_RecordingClips_ProgramRecording]
GO


