USE [NMS]
GO

/****** Object:  Table [dbo].[CelebrityVoiceArtist]    Script Date: 1/4/2017 6:35:19 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[CelebrityVoiceArtist](
	[CelebrityVoiceArtistId] [int] IDENTITY(1,1) NOT NULL,
	[CelebrityId] [int] NULL,
	[VoiceArtistId] [int] NULL,
	[Language] [nvarchar](10) NULL,
	[CreationDate] [datetime] NULL,
	[LastUpdateDate] [datetime] NULL,
 CONSTRAINT [PK_CelebrityVoiceArtist] PRIMARY KEY CLUSTERED 
(
	[CelebrityVoiceArtistId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[CelebrityVoiceArtist]  WITH CHECK ADD  CONSTRAINT [FK_CelebrityVoiceArtist_Celebrity] FOREIGN KEY([CelebrityId])
REFERENCES [dbo].[Celebrity] ([CelebrityId])
GO

ALTER TABLE [dbo].[CelebrityVoiceArtist] CHECK CONSTRAINT [FK_CelebrityVoiceArtist_Celebrity]
GO

ALTER TABLE [dbo].[CelebrityVoiceArtist]  WITH CHECK ADD  CONSTRAINT [FK_CelebrityVoiceArtist_VoiceArtist] FOREIGN KEY([VoiceArtistId])
REFERENCES [dbo].[VoiceArtist] ([VoiceArtistId])
GO

ALTER TABLE [dbo].[CelebrityVoiceArtist] CHECK CONSTRAINT [FK_CelebrityVoiceArtist_VoiceArtist]
GO


