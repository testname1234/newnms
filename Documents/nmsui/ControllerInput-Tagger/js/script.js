//(function () {

    var app = angular.module('nmsApp', ['ngScrollbars','ngMaterial','ngRoute','ngAnimate','ngMessages','ngSanitize','ui.select2','daterangepicker'])
        .config(function (ScrollBarsProvider) {
            ScrollBarsProvider.defaults = {
                axis: 'y', // enable 2 axis scrollbars by default
                autoHideScrollbar: false,
                theme: 'minimal',
                advanced:{
                    updateOnContentResize: true
                },
            };
        })
        .controller('AppCtrl', AppCtrl)
    ;

//})(); // End Main Function



// Directives Start----------------------
app.directive('dateRangePick', function() {
    return {
        restrict: 'E',
        templateUrl: 'directives/_dateRangePicker.html'
    };
}).directive('dateSinglePick', function() {
    return {
        restrict: 'E',
        templateUrl: 'directives/_dateSinglePicker.html'
    };
}).directive('dateSingleTimePick', function() {
    return {
        restrict: 'E',
        templateUrl: 'directives/_dateSingleTimePicker.html'
    };
}).directive('newsList', function() {
    return {
        restrict: 'AE',
        templateUrl: 'directives/_newsList.html',
        scope: {listData: '='},
        link: function(scope, element, attribute){
            //console.log(scope.listData);
        }
    };
});
// Directives Ends----------------------



function alldata($scope){
    $scope.newslist_a = [
        {
            id: 0,
            tags: ['international','others'],
            news:{
                title: 'Bombardment by rebels devastates strategic town in Ukraine',
                dpimage: '../assets/images/img1.jpg',
                source: 'Dawn Epaper',
                time: '2 years ago',
                description: '<strong>DONETSK:</strong> Ferocious fighting raged in Ukraine on Saturday., threating a ceasefire deal as Kiev and the US accused Russia of fuelling a rebel onslaught to grab territory hours before the truce began. Kiev-loyal regional police chief Vyacheslav Abroskin said constant artillery bombardments were razing the strategic railway hub of Debaltseve...'
            },
            attachment: [
                {
                    image: '../assets/images/img1.jpg',
                    type: 'img',
                    link: '../assets/images/img1.jpg'
                },
                {
                    image: '../assets/images/img2.jpg',
                    type: 'img',
                    link: '../assets/images/img2.jpg'
                },
                {
                    image: '../assets/images/img3.jpg',
                    type: 'vid',
                    link: '../assets/images/img3.jpg'
                },
                {
                    image: '../assets/images/img4.jpg',
                    type: 'img',
                    link: '../assets/images/img4.jpg'
                },
                {
                    image: '../assets/images/img5.jpg',
                    type: 'img',
                    link: '../assets/images/img5.jpg'
                },
                {
                    image: '../assets/images/img6.jpg',
                    type: 'vid',
                    link: '../assets/images/img6.jpg'
                },
                {
                    image: '../assets/images/img7.jpg',
                    type: 'img',
                    link: '../assets/images/img7.jpg'
                },
                {
                    image: '',
                    type: 'placeHolder',
                    link: ''
                }
            ]
        },
        {
            id: 1,
            tags: ['business','national','politics','others'],
            news:{
                title: 'Pakistan about to secure deal with Qatar over LNG supply',
                dpimage: '../assets/images/img2.jpg',
                source: 'Dawn Epaper',
                time: '2 years ago',
                description: '<strong>ISLAMABAD:</strong> A top Pakistani energy official has revealed that Islamabad is expected to soon secure an extensive deal with Qatar for the import of liquefied natural gas (LNG) to help alleviate the country’s energy crisis by fuelling its currently dormant power stations.'
            },
            attachment: [
                {
                    image: '../assets/images/img8.jpg',
                    type: 'img',
                    link: '../assets/images/img8.jpg'
                },
                {
                    image: '../assets/images/img9.jpg',
                    type: 'img',
                    link: '../assets/images/img9.jpg'
                },
                {
                    image: '../assets/images/img10.jpg',
                    type: 'vid',
                    link: '../assets/images/img10.jpg'
                },
                {
                    image: '../assets/images/img11.jpg',
                    type: 'img',
                    link: '../assets/images/img11.jpg'
                },
                {
                    image: '../assets/images/img12.jpg',
                    type: 'img',
                    link: '../assets/images/img12.jpg'
                },
                {
                    image: '../assets/images/img13.jpg',
                    type: 'vid',
                    link: '../assets/images/img13.jpg'
                },
                {
                    image: '',
                    type: 'placeHolder',
                    link: ''
                }
            ]
        },
        {
            id: 2,
            tags: ['national','politics'],
            news:{
                title: 'Did Farooq Sattar already know about Nine Zero raid?',
                dpimage: '../assets/images/img3.jpg',
                source: 'Dawn Epaper',
                time: '2 years ago',
                description: '<strong>DONETSK:</strong> Ferocious fighting raged in Ukraine on Saturday., threating a ceasefire deal as Kiev and the US accused Russia of fuelling a rebel onslaught to grab territory hours before the truce began. Kiev-loyal regional police chief Vyacheslav Abroskin said constant artillery bombardments were razing the strategic railway hub of Debaltseve...'
            },
            attachment: [
                {
                    image: '../assets/images/img14.jpg',
                    type: 'img',
                    link: '../assets/images/img14.jpg'
                },
                {
                    image: '../assets/images/img15.jpg',
                    type: 'img',
                    link: '../assets/images/img15.jpg'
                },
                {
                    image: '../assets/images/img16.jpg',
                    type: 'vid',
                    link: '../assets/images/img16.jpg'
                },
                {
                    image: '../assets/images/img17.jpg',
                    type: 'img',
                    link: '../assets/images/img17.jpg'
                },
                {
                    image: '../assets/images/img18.jpg',
                    type: 'img',
                    link: '../assets/images/img18.jpg'
                },
                {
                    image: '../assets/images/img19.jpg',
                    type: 'vid',
                    link: '../assets/images/img19.jpg'
                },
                {
                    image: '../assets/images/img20.jpg',
                    type: 'img',
                    link: '../assets/images/img20.jpg'
                },
                {
                    image: '',
                    type: 'placeHolder',
                    link: ''
                }
            ]
        }
    ]
    $scope.newsSelction = [
        {
            id: 0,
            date: 'June 19, 2016',
            newsList: [
                {
                    title: 'Did Farooq Sattar already know about Nine Zero raid?',
                    dpimage: '../assets/images/img14.jpg',
                    category: 'Politics',
                    time: '2 mins ago',
                    source: 'Bureau'
                },
                {
                    title: '1st ODI: Pakistan facing England today',
                    dpimage: '../assets/images/img15.jpg',
                    category: 'Sports',
                    time: '10 mins ago',
                    source: 'Reuters'
                },
                {
                    title: 'Elections for heads of local bodies underway in Sindh',
                    dpimage: '../assets/images/img16.jpg',
                    category: 'Government',
                    time: '11 mins ago',
                    source: 'Bureau'
                },
                {
                    title: 'Will Karachi’s new mayor run the city from prison?',
                    dpimage: '../assets/images/img17.jpg',
                    category: 'Politics',
                    time: '15 mins ago',
                    source: 'Samaa TV'
                },
                {
                    title: 'One dead, dozens wounded in Thai car bombing',
                    dpimage: '../assets/images/img18.jpg',
                    category: 'Crime',
                    time: '18 mins ago',
                    source: 'Yahoo'
                },
                {
                    title: 'I am not a traitor, ‘heartbroken’ Aamir Liaquat says',
                    dpimage: '../assets/images/img19.jpg',
                    category: 'Politics',
                    time: '20 mins ago',
                    source: 'Social Media'
                },
                {
                    title: 'ایم کیوایم کے کارکنان کا اے آر وائی کے دفتر پر حملہ',
                    lang: 'urdu',
                    dpimage: '../assets/images/img20.jpg',
                    category: 'Government',
                    time: '23 mins ago',
                    source: 'Bureau'
                },
                {
                    title: 'Elections for heads of local bodies underway in Sindh',
                    dpimage: '../assets/images/img13.jpg',
                    category: 'Government',
                    time: '11 mins ago',
                    source: 'Bureau'
                }
            ]
        }
    ]
}



// jQuery Code

$(function(){

    $('body').on('click','.jq-link01 .newsRow',function(e){
        //console.log(e.target);
        if(!$(e.target).parents('button').is('button') && !$(e.target).is('button')){
            if($(this).hasClass('active')){
                $(this).removeClass('active').parents('.newsSection').removeClass('openSide');
            } else {
                if($(this).siblings().hasClass('active')){
                    $(this).addClass('active').siblings().removeClass('active');
                } else{
                    $(this).addClass('active').parents('.newsSection').addClass('openSide');
                }
            }
        }
    });

    $('body').on('click','.jq-verify .newsRow .verifButs>button',function() {
        var $this = $(this);
        $this.parents('.newsRow').addClass('verified');
        if($this.parents('.newsRow').hasClass('newsMarked')){
            setTimeout(function(){
                $this.parents('.newsRow').slideUp();
            },1000);
        }
    }).on('click','.jq-verify .newsRow .buts>button',function(){
        var $this = $(this),
            typ = $(this).attr('data-news-type');
        $this.addClass('active').parents('.newsRow').addClass('newsMarked type-'+typ);
        if($this.parents('.newsRow').hasClass('verified')){
            setTimeout(function(){
                $this.parents('.newsRow').slideUp();
            },1000);
        }
    });

    //$(window).on('load',function(){onLoad();});
    //$(window).resize(function(){onResize();});


}); // Main Function Ends




function onLoad(){
}
function onResize(){
}



















