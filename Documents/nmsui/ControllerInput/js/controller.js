
function AppCtrl($scope, $window, $timeout){

    // Custom Scroll Update
    updateScroll();
    function updateScroll(){
        $timeout(function(){
            $scope.updateScrollbar('update');
            $scope.updateScrollbar1 ? $scope.updateScrollbar1('update') : '';
            $scope.updateScrollbar2 ? $scope.updateScrollbar2('update') : '';
            $scope.updateScrollbar3 ? $scope.updateScrollbar3('update') : '';
            $scope.updateScrollbar4 ? $scope.updateScrollbar4('update') : '';
        },300);
    }
    angular.element($window).on('resize', function(){
        //console.log('Angular Resize');
        $timeout(function(){updateScroll();});
    });
    // Custom Scroll Update



    // Md-Menu Starts--------------------
    $scope.$on("$mdMenuOpen", function(e) {
        //$scope.menuScrollbar('update');
        $timeout(function(){
            customScroll('body > div > md-menu-content .scrollDv');
        }, 300);
    });
    $scope.$on("$mdMenuClose", function(e) {
        customScroll('body > div > md-menu-content .scrollDv','destroy');
    });
    // Md-Menu Ends--------------------




    // Select2 Starts--------------------
    $scope.select2Options = {
        maximumSelectionLength: 1
    };

    $scope.select3Options = {
        maximumSelectionLength: 20,
        placeholder: "Enter Tags...",
        tags: true,
    };
    // Select2 Ends----------------------



    // on Routing Change Succesfuly
    $scope.$on("$routeChangeSuccess", function(event, next, current) {
        $timeout(function(){resethtml();},100);
        activeLink(next);
        //console.log(event, next, current);
    });


    alldata($scope);



    // DatePicker Start--------------------
    //$scope.singleDate = moment();
    $scope.dateRange = {
        startDate: moment().subtract(1, "days"),
        endDate: moment()
    };
    $scope.dateRangeOpts = {
        applyClass: '',
        cancelClass: '',
        buttonClasses: '',
        opens: "left",
        locale: {
            applyLabel: "Apply",
            fromLabel: "From",
            format: "YYYY-MM-DD",
            toLabel: "To",
            cancelLabel: 'Cancel',
            customRangeLabel: 'Custom range',
        },
        eventHandlers: {
            'show.daterangepicker': function(ev, picker) {
                console.log('show.daterangepicker', ev, picker);
            },
            'hide.daterangepicker': function(ev, picker) {
                console.log('hide.daterangepicker', ev, picker);
            },
            'showCalendar.daterangepicker': function(ev, picker) {
                console.log('showCalendar.daterangepicker', ev, picker);
            },
            'hideCalendar.daterangepicker': function(ev, picker) {
                console.log('hideCalendar.daterangepicker', ev, picker);
            },
            'apply.daterangepicker': function(ev, picker) {
                console.log('apply.daterangepicker', ev, picker);
            },
            'cancel.daterangepicker': function(ev, picker) {
                console.log('cancel.daterangepicker', ev, picker);
            }
        },

        ranges: {
            'Today': [moment().subtract(6, 'days'), moment()],
            'Yesterday': [moment().subtract(29, 'days'), moment()],
            'This Week': [moment().subtract(6, 'days'), moment()],
            'Last Week': [moment().subtract(29, 'days'), moment()],
            'This Month': [moment().subtract(6, 'days'), moment()],
            'Last Month': [moment().subtract(29, 'days'), moment()],
            'This Weekdays': [moment().subtract(29, 'days'), moment()],
            'Last Weekdays': [moment().subtract(29, 'days'), moment()],
            'This Weekend': [moment().subtract(29, 'days'), moment()],
            'Last Weekend': [moment().subtract(29, 'days'), moment()]
        }
    };

    $scope.dateSingle = {
        //startDate: moment().subtract(1, "days"),
        //endDate: moment()
    };
    $scope.dateSingleOpts = {
        applyClass: '',
        cancelClass: '',
        buttonClasses: '',
        opens: "left",
        singleDatePicker: true,
        locale: {
            applyLabel: "Apply",
            fromLabel: "From",
            format: "YYYY-MM-DD",
            toLabel: "To",
            cancelLabel: 'Cancel',
            customRangeLabel: 'Custom range',
        },
        eventHandlers: {
            'show.daterangepicker': function(ev, picker) {
                console.log('show.daterangepicker', ev, picker);
            },
            'hide.daterangepicker': function(ev, picker) {
                console.log('hide.daterangepicker', ev, picker);
            },
            'showCalendar.daterangepicker': function(ev, picker) {
                console.log('showCalendar.daterangepicker', ev, picker);
            },
            'hideCalendar.daterangepicker': function(ev, picker) {
                console.log('hideCalendar.daterangepicker', ev, picker);
            },
            'apply.daterangepicker': function(ev, picker) {
                console.log('apply.daterangepicker', ev, picker);
            },
            'cancel.daterangepicker': function(ev, picker) {
                console.log('cancel.daterangepicker', ev, picker);
            }
        }
    };

    $scope.dateSingleTime = {
        //startDate: moment().subtract(1, "days"),
        //endDate: moment()
    };
    $scope.dateSingleTimeOpts = {
        applyClass: '',
        cancelClass: '',
        buttonClasses: '',
        opens: "left",
        timePicker: true,
        singleDatePicker: true,
        locale: {
            applyLabel: "Apply",
            fromLabel: "From",
            format: "YYYY-MM-DD h:mm A",
            toLabel: "To",
            cancelLabel: 'Cancel',
            customRangeLabel: 'Custom range',
        },
        eventHandlers: {
            'show.daterangepicker': function(ev, picker) {
                console.log('show.daterangepicker', ev, picker);
            },
            'hide.daterangepicker': function(ev, picker) {
                console.log('hide.daterangepicker', ev, picker);
            },
            'showCalendar.daterangepicker': function(ev, picker) {
                console.log('showCalendar.daterangepicker', ev, picker);
            },
            'hideCalendar.daterangepicker': function(ev, picker) {
                console.log('hideCalendar.daterangepicker', ev, picker);
            },
            'apply.daterangepicker': function(ev, picker) {
                console.log('apply.daterangepicker', ev, picker);
            },
            'cancel.daterangepicker': function(ev, picker) {
                console.log('cancel.daterangepicker', ev, picker);
            }
        }
    };
    // DatePicker End--------------------






} // AppCtrl



