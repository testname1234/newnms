app.config(function($routeProvider) {
    $routeProvider
        .when("/", {
            templateUrl: "views/home.html"
        })
        .when("/news-reviewed", {
            templateUrl: "views/news-reviewed.html"
        });
});



function activeLink(next){
    //console.log(next, next.loadedTemplateUrl);
    switch (next.loadedTemplateUrl) {
        case 'views/news-reviewed.html':
            $('[href="#news-reviewed"]').parent().addClass('active').siblings().removeClass('active');
            break;
        default:
            $('[href="#/"]').parent().addClass('active').siblings().removeClass('active');
            break;
    }
}