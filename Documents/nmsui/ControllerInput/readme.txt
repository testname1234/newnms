Using Less Mixins:

.wd-per(1 - 100) => place width in Percentage
eg:
.wd-per6 	-> width : 6%;
.wd-per25 	-> width : 25%;
.wd-per50 	-> width : 50%;
.wd-per95 	-> width : 95%;




.wd-px(1 - 100) => place width (x 10) in pixels
eg:
.wd-per6 	-> width : 60px;
.wd-per25 	-> width : 250px;
.wd-per50 	-> width : 500px;
.wd-per95 	-> width : 950px;


Pre Defined Vars:

@baseTxtColor
@baseTxtSize
@baseFontFamily

@baseLink
@baseLinkHover
@headColor1
@headColor2


.ellipsis; => will controll text overflow using three dots at the end

.clearafter; => just like .clearfix in bootstrap

.icocenter; => make centeralize single icon in a box

.fxdcenter('realative' / 'fixed'); => will make centralize according to position

.para('color', 'font-size', 'margin', 'padding') => can be use for basic values of paragraph or headings


.trans(speed) => will allow elements to chnage there states through animate by css3 if possible
eg:
.trans();	-> animation duration is default
.trans(.2s);	-> animation duration is .2 seconds
.trans(.4s);	-> animation duration is .4 seconds


.placeholder('color') => will make all form elemnts placeholder color change inside parent container


.xtooltip-color('color') => if using xtooltip, this will allow you to chnage tooltip color



Using javascript function:

goToByScroll('target_element'); => will scroll page to given elemnt with animation

<div data-position="x-position y-position"></div> => this will place background position on runtime

* every first and last element in 'li' and 'td' will have 'first' and 'last' classes respectively
* every 2nd 'tr' element will have 'alter' class
* every element having 'href="#"' will change to 'href="javascript:;"'














