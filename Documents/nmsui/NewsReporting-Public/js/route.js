app.config(function($routeProvider) {
    $routeProvider
        .when("/", {
            templateUrl: "views/report-news-a.html"
        })
        .when("/news-list", {
            templateUrl: "views/newsList.html"
        });
});



function activeLink(next){
    //console.log(next, next.loadedTemplateUrl);
    switch (next.loadedTemplateUrl) {
        case 'views/newsList.html':
            $('[href="#news-list"]').parent().addClass('active').siblings().removeClass('active');
            break;
        default:
            $('[href="#/"]').parent().addClass('active').siblings().removeClass('active');
            break;
    }
}