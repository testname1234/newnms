app.config(function($routeProvider) {
    $routeProvider
        .when("/", {
            templateUrl: "views/report-news-a.html"
        })
        .when("/news-list", {
            templateUrl: "views/newsList.html"
        }).when("/report-program", {
            templateUrl: "views/report-program.html"
        });
});



function activeLink(next){
    //console.log(next, next.loadedTemplateUrl);
    switch (next.loadedTemplateUrl) {
        case 'views/newsList.html':
            $('[href="#news-list"]').parent().addClass('active').siblings().removeClass('active');
            break;
        case 'views/report-program.html':
            $('[href="#report-program"]').parent().addClass('active').siblings().removeClass('active');
            break;
        default:
            $('[href="#/"]').parent().addClass('active').siblings().removeClass('active');
            break;
    }
}