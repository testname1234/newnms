//(function () {

    var app = angular.module('nmsApp', ['ngScrollbars','ngMaterial','ngRoute','ngAnimate','ngMessages','ngSanitize','ui.select2','daterangepicker'])
        .config(function (ScrollBarsProvider) {
            ScrollBarsProvider.defaults = {
                axis: 'y', // enable 2 axis scrollbars by default
                autoHideScrollbar: false,
                theme: 'minimal',
                advanced:{
                    updateOnContentResize: true
                },
            };
        })
        .controller('AppCtrl', AppCtrl)
    ;

//})(); // End Main Function


// Directives Start----------------------
app.directive('dateRangePick', function() {
    return {
        restrict: 'E',
        templateUrl: 'directives/_dateRangePicker.html'
    };
}).directive('dateSinglePick', function() {
    return {
        restrict: 'E',
        templateUrl: 'directives/_dateSinglePicker.html'
    };
}).directive('dateSingleTimePick', function() {
    return {
        restrict: 'E',
        templateUrl: 'directives/_dateSingleTimePicker.html'
    };
}).directive('newsList', function() {
    return {
        restrict: 'AE',
        templateUrl: 'directives/_newsList.html',
        scope: {listData: '='},
        link: function(scope, element, attribute){
            //console.log(scope.listData);
        }
    };
}).directive('newsEditorBox', function() {
    return {
        restrict: 'AE',
        templateUrl: 'directives/_newsEditorBox.html',
        scope: {editingInside: '='},
        link: function(scope, element, attribute){
            //console.log(scope.editingInside);
        }
    };
});
// Directives Ends----------------------



function alldata($scope){
    $scope.newslist_a = [
        {
            id: 0,
            tags: ['international','others'],
            news:{
                title: 'Bombardment by rebels devastates strategic town in Ukraine',
                dpimage: '../assets/images/img1.jpg',
                source: 'Dawn Epaper',
                time: '2 years ago',
                description: '<strong>DONETSK:</strong> Ferocious fighting raged in Ukraine on Saturday., threating a ceasefire deal as Kiev and the US accused Russia of fuelling a rebel onslaught to grab territory hours before the truce began. Kiev-loyal regional police chief Vyacheslav Abroskin said constant artillery bombardments were razing the strategic railway hub of Debaltseve...'
            },
            attachment: [
                {
                    image: '../assets/images/img1.jpg',
                    type: 'img',
                    link: '../assets/images/img1.jpg'
                },
                {
                    image: '../assets/images/img2.jpg',
                    type: 'img',
                    link: '../assets/images/img2.jpg'
                },
                {
                    image: '../assets/images/img3.jpg',
                    type: 'vid',
                    link: '../assets/images/img3.jpg'
                },
                {
                    image: '../assets/images/img4.jpg',
                    type: 'img',
                    link: '../assets/images/img4.jpg'
                },
                {
                    image: '../assets/images/img5.jpg',
                    type: 'img',
                    link: '../assets/images/img5.jpg'
                },
                {
                    image: '../assets/images/img6.jpg',
                    type: 'vid',
                    link: '../assets/images/img6.jpg'
                },
                {
                    image: '../assets/images/img7.jpg',
                    type: 'img',
                    link: '../assets/images/img7.jpg'
                },
                {
                    image: '',
                    type: 'placeHolder',
                    link: ''
                }
            ]
        },
        {
            id: 1,
            tags: ['business','national','politics','others'],
            news:{
                title: 'Pakistan about to secure deal with Qatar over LNG supply',
                dpimage: '../assets/images/img2.jpg',
                source: 'Dawn Epaper',
                time: '2 years ago',
                description: '<strong>ISLAMABAD:</strong> A top Pakistani energy official has revealed that Islamabad is expected to soon secure an extensive deal with Qatar for the import of liquefied natural gas (LNG) to help alleviate the country’s energy crisis by fuelling its currently dormant power stations.'
            },
            attachment: [
                {
                    image: '../assets/images/img8.jpg',
                    type: 'img',
                    link: '../assets/images/img8.jpg'
                },
                {
                    image: '../assets/images/img9.jpg',
                    type: 'img',
                    link: '../assets/images/img9.jpg'
                },
                {
                    image: '../assets/images/img10.jpg',
                    type: 'vid',
                    link: '../assets/images/img10.jpg'
                },
                {
                    image: '../assets/images/img11.jpg',
                    type: 'img',
                    link: '../assets/images/img11.jpg'
                },
                {
                    image: '../assets/images/img12.jpg',
                    type: 'img',
                    link: '../assets/images/img12.jpg'
                },
                {
                    image: '../assets/images/img13.jpg',
                    type: 'vid',
                    link: '../assets/images/img13.jpg'
                },
                {
                    image: '',
                    type: 'placeHolder',
                    link: ''
                }
            ]
        },
        {
            id: 2,
            tags: ['national','politics'],
            news:{
                title: 'Did Farooq Sattar already know about Nine Zero raid?',
                dpimage: '../assets/images/img3.jpg',
                source: 'Dawn Epaper',
                time: '2 years ago',
                description: '<strong>DONETSK:</strong> Ferocious fighting raged in Ukraine on Saturday., threating a ceasefire deal as Kiev and the US accused Russia of fuelling a rebel onslaught to grab territory hours before the truce began. Kiev-loyal regional police chief Vyacheslav Abroskin said constant artillery bombardments were razing the strategic railway hub of Debaltseve...'
            },
            attachment: [
                {
                    image: '../assets/images/img14.jpg',
                    type: 'img',
                    link: '../assets/images/img14.jpg'
                },
                {
                    image: '../assets/images/img15.jpg',
                    type: 'img',
                    link: '../assets/images/img15.jpg'
                },
                {
                    image: '../assets/images/img16.jpg',
                    type: 'vid',
                    link: '../assets/images/img16.jpg'
                },
                {
                    image: '../assets/images/img17.jpg',
                    type: 'img',
                    link: '../assets/images/img17.jpg'
                },
                {
                    image: '../assets/images/img18.jpg',
                    type: 'img',
                    link: '../assets/images/img18.jpg'
                },
                {
                    image: '../assets/images/img19.jpg',
                    type: 'vid',
                    link: '../assets/images/img19.jpg'
                },
                {
                    image: '../assets/images/img20.jpg',
                    type: 'img',
                    link: '../assets/images/img20.jpg'
                },
                {
                    image: '',
                    type: 'placeHolder',
                    link: ''
                }
            ]
        }
    ]
    $scope.newsSelction = [
        {
            id: 0,
            date: 'June 19, 2016',
            newsList: [
                {
                    title: 'Did Farooq Sattar already know about Nine Zero raid?',
                    dpimage: '../assets/images/img14.jpg',
                    category: 'Politics',
                    time: '2 mins ago',
                    source: 'Bureau'
                },
                {
                    title: '1st ODI: Pakistan facing England today',
                    dpimage: '../assets/images/img15.jpg',
                    category: 'Sports',
                    time: '10 mins ago',
                    source: 'Reuters'
                },
                {
                    title: 'Elections for heads of local bodies underway in Sindh',
                    dpimage: '../assets/images/img16.jpg',
                    category: 'Government',
                    time: '11 mins ago',
                    source: 'Bureau'
                },
                {
                    title: 'Will Karachi’s new mayor run the city from prison?',
                    dpimage: '../assets/images/img17.jpg',
                    category: 'Politics',
                    time: '15 mins ago',
                    source: 'Samaa TV'
                },
                {
                    title: 'One dead, dozens wounded in Thai car bombing',
                    dpimage: '../assets/images/img18.jpg',
                    category: 'Crime',
                    time: '18 mins ago',
                    source: 'Yahoo'
                },
                {
                    title: 'I am not a traitor, ‘heartbroken’ Aamir Liaquat says',
                    dpimage: '../assets/images/img19.jpg',
                    category: 'Politics',
                    time: '20 mins ago',
                    source: 'Social Media'
                },
                {
                    title: 'ایم کیوایم کے کارکنان کا اے آر وائی کے دفتر پر حملہ',
                    lang: 'urdu',
                    dpimage: '../assets/images/img20.jpg',
                    category: 'Government',
                    time: '23 mins ago',
                    source: 'Bureau'
                },
                {
                    title: 'Elections for heads of local bodies underway in Sindh',
                    dpimage: '../assets/images/img13.jpg',
                    category: 'Government',
                    time: '11 mins ago',
                    source: 'Bureau'
                }
            ]
        }
    ]

    $scope.editorText = {
        urdu: {
            title: 'شدت پسند تنظیم داعش نے دھماکے کی ذمہ داری قبول کرلی ہے۔',
            oneLiner: 'بغداد: عراق کے دارالحکومت بغداد میں خودکش دھماکے کے نتیجے میں 55 افراد ہلاک جب کہ درجنوں زخمی ہوگئے ہیں۔',
            detail: 'غیر ملکی خبررساں ادارے کے مطابق عراق کے دارالحکومت بغداد میں درجنوں لوگ مذہبی رسومات ادا کرنے کے لیے جمع ہوئے تھے کہ اس دوران خودکش بمبار نے ان کے درمیان آکر خود کو دھماکے سے اڑادیا جس کے نتیجے میں 55 افراد ہلاک جب کہ 30 سے زائد زخمی ہوگئے جنہیں فوری طور پر قریبی اسپتال منتقل کردیا گیا۔\n واضح رہے عراقی فوج نے ملک کے اکثر شہروں سے داعش کا قبضہ ختم کروالیا ہے تاہم صرف شمالی عراق کے شہر موصل میں داعش کا وجود باقی  ہے'
        },
        english:{
            title: 'Islamic militant group has claimed responsibility for the blast.',
            oneLiner: 'BAGHDAD that dozens were injured when a suicide blast kills 55 in Baghdad, capital of Iraq.',
            detail: 'Cooperation of the Iraqi capital Baghdad, dozens of people gathered to pay rituals that the bomber came blown his suicide, killing 55 people between them, more than 30 injured when they were rushed to a nearby hospital. \n\nThe Iraqi army is tantamount over control of most parts of the country, but only the ISIS ISIS presence in Mosul in northern Iraq'
        }
    }
}




// jQuery Code

$(function(){

    $('body').on('click','.jq-link01 .newsRow',function(){
        if($(this).hasClass('active')){
            $(this).removeClass('active').parents('.newsSection').removeClass('openSide');
        } else {
            if($(this).siblings().hasClass('active')){
                $(this).addClass('active').siblings().removeClass('active');
            } else{
                $(this).addClass('active').parents('.newsSection').addClass('openSide');
            }
        }
    });

    $('body').on('click', '.tabController li', function(){
        $('.tabController li').removeClass('active');
        $(this).addClass('active');
        var target = $(this).data('target');
        $('div[class^="box-"]').hide();
        $('.'+target).show();
    });

    $('body').on('focus','news-editor-box .inputRow input, news-editor-box .inputRow textarea',function(){
        $(this).parents('news-editor-box').addClass('togl');
    }).on('blur','news-editor-box .inputRow input, news-editor-box .inputRow textarea',function(){
        $(this).parents('news-editor-box').removeClass('togl');
    });



    //$(window).on('load',function(){onLoad();});
    //$(window).resize(function(){onResize();});


}); // Main Function Ends




function onLoad(){
}
function onResize(){
}












